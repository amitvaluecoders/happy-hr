import { HappyHrPage } from './app.po';

describe('happy-hr App', () => {
  let page: HappyHrPage;

  beforeEach(() => {
    page = new HappyHrPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
