import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DataTableModule} from 'angular2-datatable';
import { FileNotesRoutingModule } from './file-notes.routing';
import { FileNotesService } from './file-notes.service';
import { FileNoteListingComponent } from './file-note-listing/file-note-listing.component';
import { AddEditFileNoteComponent } from './add-edit-file-note/add-edit-file-note.component';
import { FileNoteDetailsComponent } from './file-note-details/file-note-details.component';
import { MaterialModule} from '@angular/material';
import { FormsModule} from '@angular/forms';
import { SharedModule } from '../../shared/shared.module';
import { AngularMultiSelectModule } from 'angular2-multiselect-checkbox-dropdown/angular2-multiselect-dropdown';
import { FileNotesGuardService } from './file-notes-guard.service';

@NgModule({
  imports: [
    SharedModule,
    MaterialModule,
    DataTableModule,
    CommonModule,
    FormsModule,
    AngularMultiSelectModule,
    FileNotesRoutingModule
  ],
  declarations: [FileNoteListingComponent, AddEditFileNoteComponent, FileNoteDetailsComponent],
  providers:[FileNotesService,FileNotesGuardService],
  entryComponents:[FileNoteDetailsComponent]
})
export class FileNotesModule { }
