import { Injectable } from '@angular/core';
import {CanActivate,Router,RouterStateSnapshot,ActivatedRouteSnapshot} from "@angular/router";
import {CoreService} from '../../shared/services/core.service';
import {Observable} from 'rxjs/Rx'; 
import 'rxjs/add/operator/map';

@Injectable()
export class FileNotesGuardService {

  public module = "note";
  
        constructor(private _core: CoreService, private router: Router){ }
  
        canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {  
            let url: string;
            if(route['_routeConfig']['path'])
            url= route['_routeConfig']['path']
            else url = state.url;
            if(this.checkHavePermission(url)){
              return true;
            }else{
              this.router.navigate(['/extra/unauthorized']);
              return false;
            }
        }
  
        private checkHavePermission(path) {
            return true;
            // switch (path) {
            //     case "/app/company-admin/file-notes":
            //         return this._core.getModulePermission(this.module, 'view');
            //     case "list":
            //         return this._core.getModulePermission(this.module, 'view');
            //     case "create-note":
            //         return this._core.getModulePermission(this.module, 'view');
            //     default:
            //         return false;
            // }
        }
}


