import { Component, OnInit } from '@angular/core';
import { FileNotesService } from '../file-notes.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { CoreService } from '../../../shared/services/core.service';

@Component({
  selector: 'app-file-note-details',
  templateUrl: './file-note-details.component.html',
  styleUrls: ['./file-note-details.component.scss']
})
export class FileNoteDetailsComponent implements OnInit {
  
  public appConfig;
  public isProcessing:Boolean=false;
  public id:any;
  public notesDetails={
    noteId: "",
    title:"",
    description:"",
    noteFor: {name:"",imageUrl:"",designation:""},
    noteBy:  {name:"",imageUrl:"",designation:""}
  };
  permission;

  constructor(public fileNotesService:FileNotesService,
               public router:Router,public activatedRoute:ActivatedRoute,
               public coreService:CoreService) { }

  ngOnInit() {
    this.getFilesNotes();
    this.permission=this.coreService.getNonRoutePermission();
  }

  getFilesNotes(){
     this.isProcessing=true;       
     this.fileNotesService.getFileNoteDetails(this.id).subscribe(
          d => {
              this.isProcessing=false;
              if(d.code == "200") this.notesDetails=d.data;  
              else this.coreService.notify("Unsuccessful",d.message, 0); 
          },
          error =>{
            this.coreService.notify("Unsuccessful", "Error while getting notes", 0);
            this.isProcessing=false
          } 
      )
  }

}
