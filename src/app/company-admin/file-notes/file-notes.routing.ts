import {  ModuleWithProviders  } from '@angular/core';
import {  RouterModule,Routes  } from '@angular/router';
import { FileNoteListingComponent } from './file-note-listing/file-note-listing.component';
import { AddEditFileNoteComponent } from './add-edit-file-note/add-edit-file-note.component';
import { FileNoteDetailsComponent } from './file-note-details/file-note-details.component';
import { FileNotesGuardService } from './file-notes-guard.service';

export const FileNotesPagesRoutes: Routes = [
    {
        path: '',
        canActivate:[],
        children: [      
            { path: '', component: FileNoteListingComponent,canActivate:[FileNotesGuardService]},
            { path: 'list', component: FileNoteListingComponent,canActivate:[FileNotesGuardService]},                 
            { path: 'create-note', component: AddEditFileNoteComponent,canActivate:[FileNotesGuardService]},                 
        ]
    }
];

export const FileNotesRoutingModule = RouterModule.forChild(FileNotesPagesRoutes);
