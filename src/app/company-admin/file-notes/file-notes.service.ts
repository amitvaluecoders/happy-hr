import { Injectable } from '@angular/core';
import { RestfulLaravelService} from '../../shared/services/restful-laravel.service';

@Injectable()
export class FileNotesService {

  constructor(public restfulLaravelService:RestfulLaravelService) { }

  getFileNotes(reqBody){
    return this.restfulLaravelService.get(`get-note-list/${encodeURIComponent(reqBody)}`);
  }

  getFileNoteDetails(reqBody){
    return this.restfulLaravelService.get(`get-note-detail/${reqBody}`);
  }

  getEmployeeList(){
    return this.restfulLaravelService.get(`show-all-employee`);
  }

  createNote(reqBody){
    return this.restfulLaravelService.post(`create-note`,reqBody);
  }

}
