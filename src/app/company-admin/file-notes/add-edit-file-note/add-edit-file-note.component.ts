import { Component, OnInit } from '@angular/core';
import { FileNotesService } from '../file-notes.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { CoreService } from '../../../shared/services/core.service';
import { APPCONFIG } from '../../../config';

@Component({
  selector: 'app-add-edit-file-note',
  templateUrl: './add-edit-file-note.component.html',
  styleUrls: ['./add-edit-file-note.component.scss']
})
export class AddEditFileNoteComponent implements OnInit {

  public isProcessing:Boolean=false;
  public searchKey="";
  public selectedUser="";
  public isCreateProcessing:Boolean=false;
  public isButtonClicked=false;
  public counter = 0;

  dropdownListEmployees = [];
  selectedItems = [];
  dropdownSettings = {};

  public note={
    userId:"",
    title:"",
    description:""
  }

  constructor(public fileNotesService:FileNotesService,
               public router:Router,public activatedRoute:ActivatedRoute,
               public coreService:CoreService) { }

  ngOnInit() {
    this.getEmployeeeList();
    APPCONFIG.heading="Create file note";

        this.dropdownSettings = {
          singleSelection: true, 
          text:"Select name",
          enableSearchFilter: true,
          classes:"myclass assmng"
        }; 
  }

    getEmployeeeList() {
     this.isProcessing = true;
     this.fileNotesService.getEmployeeList().subscribe(
          d => {
              if (d.code == "200") {
                  this.dropdownListEmployees=d.data.filter(item =>{
                      item['id'] = item.userId;
                      item['itemName'] = item.name;
                      return item;
                  })
                this.isProcessing = false; 
              }
              else this.coreService.notify("Unsuccessful", d.message, 0);
          },
          error => this.coreService.notify("Unsuccessful", "Error while getting employee list", 0),
          () => { }
      )
  }

  onselectUser(user){
    this.note.userId=user.userId;
    this.selectedUser=user.name;
  }

  onCreateNote(isValid){
    this.isButtonClicked=true;
    if(isValid){
     this.isCreateProcessing=true;
     this.note.userId=this.selectedItems[0].id;
    this.fileNotesService.createNote(this.note).subscribe(
          d => {
              this.isCreateProcessing=true;    
              if(d.code == "200") {
                this.router.navigate([`/app/company-admin/file-notes`]);
                this.coreService.notify("Successful",d.message, 1); 
              } 
              else this.coreService.notify("Unsuccessful",d.message, 0); 
          },
          error =>{
            this.coreService.notify("Unsuccessful", "Error while adding note", 0);
            this.isProcessing=false
          } 
      )
    }

  }

  onCancel(){

  }

    attactImageAndRolesDropDownList(){

      if(this.counter == 0){
        console.log(this.dropdownListEmployees)
    this.dropdownListEmployees.filter(function(item, i){
        $('.assmng ul li').eq(i).prepend(`<img src="${item.imageUrl}" alt="" />`);
        $('.assmng ul li').eq(i).children('label').append('<span>' + item.designation+ '</span>');
    });
    this.dropdownListEmployees.filter(function(item, i){
        $('.thirdParty ul li').eq(i).prepend(`<img src="${item.imageUrl}" alt="" />`);
        $('.thirdParty ul li').eq(i).children('label').append('<span>' + item.designation || '-'  + '</span>');
    });
      }
      this.counter++;
  }

}
