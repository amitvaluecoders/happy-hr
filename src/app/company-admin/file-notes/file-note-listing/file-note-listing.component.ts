import { Component, OnInit } from '@angular/core';
import { MdSnackBar, MdDialog, MdDialogRef } from '@angular/material';
import {FileNoteDetailsComponent} from "../file-note-details/file-note-details.component";
import {AddEditFileNoteComponent} from "../add-edit-file-note/add-edit-file-note.component";
import { FileNotesService } from '../file-notes.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { CoreService } from '../../../shared/services/core.service';
import { APPCONFIG } from '../../../config';

@Component({
  selector: 'app-file-note-listing',
  templateUrl: './file-note-listing.component.html',
  styleUrls: ['./file-note-listing.component.scss']
})
export class FileNoteListingComponent implements OnInit {
  
  public toggleFilter:boolean;
  public employment="";
  public position="";
  public notesList=[];
  public isProcessing:Boolean;
  public unsubscriber:any;
  public serchFor='';
  public serchOwner='';
  public isApplyBtnClicked=false;
  permission;
  tempReqBody={
    "search":"",
    "filter":{
      "ownerUsers":[],
      "forUsers":[],
      "createdBy":{
        "int0":false,
        "int1":false,
        "int7":false,
        "int30":false
      }
    }
  }

  reqBody={
    "search":"",
    "filter":{
      "ownerUserIDs":{
      },
      "forUserIDs":{
      },
      "createdBy":{
        "int0":false,
        "int1":false,
        "int7":false,
        "int30":false
      }
    }
  }

  constructor(public fileNotesService:FileNotesService,public dialog: MdDialog,
               public router:Router,public activatedRoute:ActivatedRoute,
               public coreService:CoreService) { }

  ngOnInit() {
    this.toggleFilter=true;
    this.getFileNotesList();
    APPCONFIG.heading="File notes";
    this.permission=this.coreService.getNonRoutePermission();
  }

    getFileNotesList(){
     this.isProcessing=true;    
     this.unsubscriber = this.fileNotesService.getFileNotes(JSON.stringify(this.reqBody)).subscribe(
          d => {
              this.isProcessing=false;
              if (d.code == "200"){
                this.notesList=d.data; 
                if(d.data.length){  
                d.data.forEach(item =>{
                  item.noteBy['isChecked']=false;
                  item.noteFor['isChecked']=false;
                  this.tempReqBody.filter.ownerUsers.push(item.noteBy);
                  this.tempReqBody.filter.forUsers.push(item.noteFor);     
                })
                  this.tempReqBody.filter.ownerUsers = this.tempReqBody.filter.ownerUsers.filter((thing, index, self) => self.findIndex(t => t.userID === thing.userID) === index)
                  this.tempReqBody.filter.forUsers = this.tempReqBody.filter.forUsers.filter((thing, index, self) => self.findIndex(t => t.userID === thing.userID) === index)
                }
              } 
              else this.coreService.notify("Unsuccessful",d.message, 0); 
          },
          error =>{
            this.coreService.notify("Unsuccessful", error.message, 0);
            this.isProcessing=false
          } 
      )
  }

  onApplyFilters(){
    for(let i of this.tempReqBody.filter.ownerUsers){
      this.reqBody.filter.ownerUserIDs[i.userID]=false;
      if(i.isChecked) this.reqBody.filter.ownerUserIDs[i.userID]=true;
    }
    for(let i of this.tempReqBody.filter.forUsers){
      this.reqBody.filter.forUserIDs[i.userID]=false;
      if(i.isChecked) this.reqBody.filter.forUserIDs[i.userID]=true;
    }
    this.reqBody.filter.createdBy=this.tempReqBody.filter.createdBy;
    this.getFileNotesList();
  }

  onSearch(){
    this.unsubscriber.unsubscribe();
    this.reqBody.search=encodeURIComponent(this.tempReqBody.search);
    this.getFileNotesList();
  }

  onShowDetails(id){
         let dialogRef = this.dialog.open(FileNoteDetailsComponent,{width:"1000px"});  
          dialogRef.componentInstance.id=id;
          dialogRef.afterClosed().subscribe(result => {});   
  }

  onAddNote(){
       this.router.navigate([`/app/company-admin/file-notes/create-note`]);
  }


}



