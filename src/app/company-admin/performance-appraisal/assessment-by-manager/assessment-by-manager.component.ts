import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { CoreService } from '../../../shared/services/core.service';
import { PerformanceAppraisalService } from '../performance-appraisal.service';
import { APPCONFIG } from '../../../config';
import { IDatePickerConfig } from 'ng2-date-picker';
import * as moment from "moment";
import { MdSnackBar, MdDialog, MdDialogRef } from '@angular/material';
import { ActionsOnEmployeePopupComponent } from "../../../shared/components/actions-on-employee-popup/actions-on-employee-popup.component";

@Component({
  selector: 'app-assessment-by-manager',
  templateUrl: './assessment-by-manager.component.html',
  styleUrls: ['./assessment-by-manager.component.scss']
})
export class AssessmentByManagerComponent implements OnInit {

  public progress;
  public collapse = [];
  public isProcessing = false;
  public isSubmitted = false;
  public noResult = true;
  public performaceAppraisalID = "";
  public pa = {
    "performaceAppraisalID": "", "title": "", "assessmentDate": "",
    "managerID": { "userID": "", "name": "", "imageUrl": "", "position": "" },
    "userID": { "userID": "", "name": "", "imageUrl": "", "position": "" },
    "performanceIndicator": [], "question": [], "thirdParty": []
  }

  public managerResponse = {
    "performaceAppraisalID": "", "nextAssessmentDate": "", "reminderDate": "",
    "generalManagerNote": "", "performanceIndicator": [], "question": [], "thirdParty": []
  }

  config: IDatePickerConfig = {};
  reminderDate; nextAssessmentDate;
  itemList = [];
  modelName = '';
  directAssessment = false;

  constructor(public coreService: CoreService, private router: Router, private route: ActivatedRoute,
    public companyService: PerformanceAppraisalService, public dialog: MdDialog) {

    this.config.locale = "en";
    this.config.format = "DD MMM, YYYY hh:mm A";
    this.config.showMultipleYearsNavigation = true;
    this.config.weekDayFormat = 'dd';
    this.config.disableKeypress = true;
    this.config.monthFormat = "MMM YYYY";
    this.config.min = moment();
  }

  ngOnInit() {
    localStorage.removeItem('PaUserData');
    localStorage.removeItem('PaKpi');
    localStorage.removeItem('PaCallbackUrl');
    APPCONFIG.heading = "Performance appraisal";
    let type = this.route.snapshot.url[0].path;
    this.directAssessment = (type == 'direct-assessment');
    if (this.directAssessment) {
      // APPCONFIG.heading = "Direct assessment";
      this.directAssessmentProgressBarSteps();
    } else {
      // APPCONFIG.heading = "Assessment by manager";
      this.progressBarSteps();
    }

    if (this.route.snapshot.params['id']) {
      this.performaceAppraisalID = this.route.snapshot.params['id'];
      this.getPerformanceAppraisal();
    }
    this.getEmployee();
  }

  progressBarSteps() {
    this.progress = {
      progress_percent_value: 75, //progress percent
      total_steps: 4,
      current_step: 3,
      circle_container_width: 25, //circle container width in percent
      steps: [
        // {num:1,data:"Select Role"},
        { num: 1, data: "Create Performance appraisal" },
        { num: 2, data: "Assement done by employee" },
        { num: 3, data: "Assessment by manager", active: true },
        { num: 4, data: "Result" }
      ]
    }
  }

  directAssessmentProgressBarSteps() {
    this.progress = {
      progress_percent_value: 66, //progress percent
      total_steps: 3,
      current_step: 2,
      circle_container_width: 25, //circle container width in percent
      steps: [
        { num: 1, data: "Create Performance appraisal" },
        { num: 2, data: "Direct assessment", active: true },
        { num: 3, data: "Result" }
      ]
    }
  }

  getEmployee() {
    this.companyService.getEmployeeList().subscribe(
      res => {
        if (res.code == 200) {
          this.itemList = res.data;
          this.itemList.filter((item, i) => {
            item['itemName'] = item.name; item['id'] = item.userId; item['position'] = item.designation;
          });
        }
        else return this.coreService.notify("Unsuccessful", res.message, 0);
      },
      err => {
        this.coreService.notify("Unsuccessful", err.message, 0);
      })
  }

  formatDate(date) {
    return (date && moment(date).isValid()) ? moment(date).format('DD MMM YYYY') : 'Not specified';
  }

  getPerformanceAppraisal() {
    this.isProcessing = true;
    this.companyService.getPerformanceAppraisalDetail(this.performaceAppraisalID).subscribe(
      res => {
        this.isProcessing = false;
        if (res.code == 200) {
          this.noResult = false;
          this.pa = res.data;
          this.managerResponse.generalManagerNote = res.data.generalManagerNote;
          this.managerResponse.performanceIndicator = this.pa.performanceIndicator;
          //console.log("performance indicator------->>",this.managerResponse.performanceIndicator)
          this.managerResponse.performanceIndicator.filter(item => { item['result'] = !(item['result']) ? null : item.result });
          this.managerResponse.question = this.directAssessment ? [] : this.pa.question;
          this.managerResponse.question.filter(item => { item['result'] = !(item['result']) ? null : item.result });
        }
        else return this.coreService.notify("Unsuccessful", res.message, 0);
      },
      err => {
        this.isProcessing = false;
        this.coreService.notify("Unsuccessful", err.message, 0);
      }
    )
  }

  onAction(actionType, category, index) {
    let route = ['/app/company-admin/performance-appraisal/result/', this.performaceAppraisalID];
    switch (actionType) {
      case 'PIP':
        route = ['/app/company-admin/performance-improvement-plan/create', this.pa.userID.userID];
        if (category == 'survey') {
          this.managerResponse.question[index]['isProcessingResult'] = true;
          this.managerResponse.question[index].result = actionType;
          this.saveManagerResponse(route);
        } else if (category == 'kpi') {
          this.managerResponse.performanceIndicator[index]['isProcessingResult'] = true;
          this.managerResponse.performanceIndicator[index].result = actionType;
          localStorage.setItem('PaKpi', JSON.stringify(this.managerResponse.performanceIndicator[index]));
          this.saveManagerResponse(route);
        }
        break;

      case 'BPIP':
        route = ['/app/company-admin/behavioral-performance-improvement-plan/create', this.pa.userID.userID];
        if (category == 'survey') {
          this.managerResponse.question[index]['isProcessingResult'] = true;
          this.managerResponse.question[index].result = actionType;
        } else if (category == 'kpi') {
          this.managerResponse.performanceIndicator[index]['isProcessingResult'] = true;
          this.managerResponse.performanceIndicator[index].result = actionType;
        }
        this.saveManagerResponse(route);
        break;

      case 'Disciplinary Action':
        route = ['/app/company-admin/disciplinary-meeting/create', this.pa.userID.userID];
        if (category == 'survey') {
          this.managerResponse.question[index]['isProcessingResult'] = true;
          this.managerResponse.question[index].result = actionType;
        } else if (category == 'kpi') {
          this.managerResponse.performanceIndicator[index]['isProcessingResult'] = true;
          this.managerResponse.performanceIndicator[index].result = actionType;
        }
        this.saveManagerResponse(route);
        break;

      case 'Development plan':
        route = ['/app/company-admin/development-plan/create', this.pa.userID.userID];
        if (category == 'survey') {
          this.managerResponse.question[index]['isProcessingResult'] = true;
          this.managerResponse.question[index].result = actionType;
        } else if (category == 'kpi') {
          this.managerResponse.performanceIndicator[index]['isProcessingResult'] = true;
          this.managerResponse.performanceIndicator[index].result = actionType;
        }
        this.saveManagerResponse(route);
        break;

      default:
        let info = { type: actionType, userID: this.pa.userID.userID, subject: "", body: '' }
        if (category == 'survey') {
          info['performanceAppraisalQuestionID'] = this.managerResponse.question[index].questionID;
        } else if (category == 'kpi') {
          info['performanceAppraisalIndicatorID'] = this.managerResponse.performanceIndicator[index].performanceID;
        }

        let dialogRef = this.dialog.open(ActionsOnEmployeePopupComponent);
        dialogRef.componentInstance.actionInfo = info;
        dialogRef.afterClosed().subscribe(r => {
          if (r) {
            if (category == 'survey') {
              this.managerResponse.question[index].result = actionType;
            } else if (category == 'kpi') {
              this.managerResponse.performanceIndicator[index].result = actionType;
            }
            this.saveManagerResponse(route);
          }
        });
        break;
    }
  }

  isValidData(data) {
    return !(data instanceof Array);
  }

  isValid() {
    let pi = true; let qsn = true;

    for (let i = 0; i < this.managerResponse.performanceIndicator.length; i++) {
      if (!this.managerResponse.performanceIndicator[i].result) {
        pi = false; break;
      }
    }
    for (let i = 0; i < this.managerResponse.question.length; i++) {
      if (!this.managerResponse.question[i].result) {
        qsn = false; break;
      }
    }
    console.log("dfkjh",pi,qsn);
    return pi && qsn;
  }

  saveManagerResponse(route?: any[]) {
    this.managerResponse.thirdParty = JSON.parse(JSON.stringify(this.itemList.filter(i => i['isSelected'])));
    this.managerResponse.thirdParty.filter(i => {
      if (i['external']) {
        i['type'] = 'external'; i['email'] = i.itemName;
      }
      else {
        i['type'] = 'internal';
        i['userID'] = i.userId;
      }
    });
    this.managerResponse['status'] = this.isValid() ? 'Complete' : 'Under Assessment';
    this.managerResponse['directAssessment'] = this.directAssessment;
    this.managerResponse.performaceAppraisalID = this.performaceAppraisalID;
    this.managerResponse.nextAssessmentDate = this.nextAssessmentDate ? moment(this.nextAssessmentDate).format('YYYY-MM-DD HH:mm:ss') : '';
    this.managerResponse.reminderDate = this.reminderDate ? moment(this.reminderDate).format('YYYY-MM-DD HH:mm:ss') : '';
    this.managerResponse.question = this.directAssessment ? [] : this.managerResponse.question

    console.log(this.managerResponse);
    this.isSubmitted = true;
    this.companyService.saveManagerResponse(this.managerResponse).subscribe(
      res => {
        this.isSubmitted = false;
        if (res.code == 200) {
          this.coreService.notify("Successful", res.message, 1);
          if (route && route.length) {
            localStorage.setItem('PaUserData', JSON.stringify(this.pa.userID));
            if (this.directAssessment) {
              localStorage.setItem('PaCallbackUrl', `/app/company-admin/performance-appraisal/direct-assessment/${this.performaceAppraisalID}`);
            } else {
              localStorage.setItem('PaCallbackUrl', `/app/company-admin/performance-appraisal/assessment-by-manager/${this.performaceAppraisalID}`);
            }
            this.router.navigate(route);
          } else {
            if (this.directAssessment) this.router.navigate(['/app/company-admin/performance-appraisal/direct-assessment-result/', this.performaceAppraisalID])
            else this.router.navigate(['/app/company-admin/performance-appraisal/result/', this.performaceAppraisalID])
          }
        }
        else return this.coreService.notify("Unsuccessful", res.message, 0);
      },
      err => {
        this.isSubmitted = false;
        this.coreService.notify("Unsuccessful", err.message, 0);
      }
    )
  }

}
