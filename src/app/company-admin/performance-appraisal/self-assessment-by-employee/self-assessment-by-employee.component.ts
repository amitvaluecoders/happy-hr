import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { CoreService } from '../../../shared/services/core.service';
import { PerformanceAppraisalService } from '../performance-appraisal.service';
import { APPCONFIG } from '../../../config';
import { IDatePickerConfig } from 'ng2-date-picker';
import * as moment from "moment";

@Component({
  selector: 'app-self-assessment-by-employee',
  templateUrl: './self-assessment-by-employee.component.html',
  styleUrls: ['./self-assessment-by-employee.component.scss']
})
export class SelfAssessmentByEmployeeComponent implements OnInit {
  public progress;
  public collapse = {};
  public isProcessing = false;
  public noResult = true;
  public performaceAppraisalID = "";
  public pa = {
    "performaceAppraisalID": "", "title": "", "assessmentDate": "",
    "managerID": { "userID": "", "name": "", "imageUrl": "", "position": "" },
    "userID": { "userID": "", "name": "", "imageUrl": "", "position": "" },
    "performanceIndicator": [], "question": [], "thirdParty": []
  }

  constructor(public coreService: CoreService, private router: Router, private route: ActivatedRoute,
    public companyService: PerformanceAppraisalService) { }

  ngOnInit() {
    APPCONFIG.heading = "Performance appraisal";
    this.progressBarSteps();

    if (this.route.snapshot.params['id']) {
      this.performaceAppraisalID = this.route.snapshot.params['id'];
      this.getPerformanceAppraisal();
    }
  }
  progressBarSteps() {
    this.progress = {
      progress_percent_value: 50, //progress percent
      total_steps: 4,
      current_step: 2,
      circle_container_width: 25, //circle container width in percent
      steps: [
        // {num:1,data:"Select Role"},
        { num: 1, data: "Create performance appraisal" },
        { num: 2, data: "Assessment done by employee", active: true },
        { num: 3, data: "Assessment done by manager" },
        { num: 4, data: "Result" }
      ]
    }
  }

  formatDate(date) {
    return (date && moment(date).isValid()) ? moment(date).format('DD MMM YYYY') : 'Not specified';
  }

  getPerformanceAppraisal() {
    this.isProcessing = true;
    this.companyService.getPerformanceAppraisalDetail(this.performaceAppraisalID).subscribe(
      res => {
        this.isProcessing = false;
        if (res.code == 200) {
          this.pa = res.data;
          this.noResult = false;
        }
        else return this.coreService.notify("Unsuccessful", res.message, 0);
      },
      err => {
        this.isProcessing = false;
        this.coreService.notify("Unsuccessful", err.message, 0);
      }
    )
  }
}
