import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { CoreService } from '../../../shared/services/core.service';
import { PerformanceAppraisalService } from '../performance-appraisal.service';
import { APPCONFIG } from '../../../config';
import { IDatePickerConfig } from 'ng2-date-picker';
import * as moment from "moment";

@Component({
  selector: 'app-create-performance-appraisal',
  templateUrl: './create-performance-appraisal.component.html',
  styleUrls: ['./create-performance-appraisal.component.scss']
})
export class CreatePerformanceAppraisalComponent implements OnInit {
  public progress;
  public isProcessing = false;
  public isSubmiting = false;
  public dropdownListEmployees = [];
  public addquestionTextarea = "";
  public pa = {
    "performaceAppraisalID": "", "title": "", "assessmentDate": "", "managerID": null, "thirdParty": [],
    "performanceIndicator": [], "question": [], "userID": null
  }
  userID = '';
  performaceAppraisalID = '';
  noResult = true;
  active = false;
  questionBox = { editMode: false, questionID: '', questionTitle: '', surveyQuetionnerID: '' };

  managerList = [];
  selectedManager = [];
  dropdownSettings2 = {};
  saQuestions = [];
  selectedQuestions = [];
  dropdownSettings = {};
  submitted = false;

  userData = null;
  thirdParty = []
  modelName = '';
  itemList = [];
  config: IDatePickerConfig = {};
  assessmentDate;
  formType = 'Create';

  constructor(public coreService: CoreService, private router: Router, private route: ActivatedRoute,
    public companyService: PerformanceAppraisalService) {

    this.config.locale = "en";
    this.config.format = "DD MMM, YYYY";
    this.config.showMultipleYearsNavigation = true;
    this.config.weekDayFormat = 'dd';
    this.config.disableKeypress = true;
    this.config.monthFormat = "MMM YYYY";
    this.config.min = moment();
  }

  ngOnInit() {
    APPCONFIG.heading = "Performance appraisal";
    this.progressBarSteps();

    this.dropdownSettings = {
      singleSelection: true,
      text: "Choose a question",
      enableSearchFilter: true,
      classes: "myclass"
    };
    this.dropdownSettings2 = {
      singleSelection: true,
      text: "Select a manager",
      enableSearchFilter: true,
      classes: "myclass"
    };

    if (this.route.snapshot.params['uid']) {
      this.userID = this.route.snapshot.params['uid'];
      this.getPerformanceIndicators();
      this.getManagers();
    }
    else if (this.route.snapshot.params['id']) {
      this.formType = 'Edit';
      this.performaceAppraisalID = this.route.snapshot.params['id'];
      this.getPerformanceAppraisal();
    }

    this.getEmployee();
    this.getSurveyQuestions()
  }

  progressBarSteps() {
    this.progress = {
      progress_percent_value: 25, //progress percent
      total_steps: 4,
      current_step: 1,
      circle_container_width: 25, //circle container width in percent
      steps: [
        // {num:1,data:"Select Role"},
        { num: 1, data: "Create performance appraisal", active: true },
        { num: 2, data: "Assessment done by employee" },
        { num: 3, data: "Assessment done by manager" },
        { num: 4, data: "Result" }
      ]
    }
  }

  getPerformanceAppraisal() {
    this.isProcessing = true;
    this.companyService.getPerformanceAppraisalDetail(this.performaceAppraisalID).subscribe(
      res => {
        this.isProcessing = false;
        if (res.code == 200) {
          this.pa = res.data;
          this.noResult = false;
          this.userID = this.pa.userID.userID;
          this.userData = this.pa.userID;
          this.thirdParty = this.pa.thirdParty;
          this.thirdParty.filter(i => {
            if (!i.userID) {
              i['type'] = 'external'
            } else {
              i['type'] = 'internal';
              i['userData'] = i.userID;
              i['userID'] = i.userID.userID;
            }
          })
          this.assessmentDate = moment(this.pa.assessmentDate);
          this.getManagers();
        }
        else return this.coreService.notify("Unsuccessful", res.message, 0);
      },
      err => {
        this.isProcessing = false;
        this.coreService.notify("Unsuccessful", err.message, 0);
      }
    )
  }

  getEmployee() {
    this.companyService.getEmployeeList().subscribe(
      res => {
        if (res.code == 200) {
          this.itemList = res.data;
          this.itemList.filter((item, i) => {
            item['itemName'] = item.name; item['id'] = item.userId; item['position'] = item.designation;
          });
        }
        else return this.coreService.notify("Unsuccessful", res.message, 0);
      },
      err => {
        this.coreService.notify("Unsuccessful", err.message, 0);
      })
  }

  getManagers() {
    this.companyService.getManagerList(this.route.snapshot.params['uid']).subscribe(
      res => {
        if (res.code == 200) {
          this.managerList = res.data;
          this.managerList.filter((item, i) => {
            item['itemName'] = item.name; item['id'] = item.userId;
          });
          this.selectedManager = !this.pa.managerID ? [] : this.managerList.filter(i => i.id == this.pa.managerID.userID);
        }
        else return this.coreService.notify("Unsuccessful", res.message, 0);
      },
      err => {
        this.coreService.notify("Unsuccessful", err.message, 0);
      })
  }

  getPerformanceIndicators() {
    let params = { "userID": this.userID };
    this.isProcessing = true;
    this.companyService.getPerformanceIndicators(JSON.stringify(params)).subscribe(
      res => {
        this.isProcessing = false;
        if (res.code == 200) {
          this.pa.performanceIndicator = res.data.performanceIndicator;
          this.noResult = false;
          this.userData = res.data.userID;
        }
        else return this.coreService.notify("Unsuccessful", res.message, 0);
      },
      err => {
        this.isProcessing = false;
        this.coreService.notify("Unsuccessful", err.message, 0);
      }
    )
  }

  getSurveyQuestions() {
    this.companyService.getSueveyQuestions().subscribe(
      res => {
        if (res.code == 200) {
          this.saQuestions = res.data.questionData;
          this.saQuestions.filter((item, i) => { item['itemName'] = item.title; item['id'] = item.surveyQuetionnerID; });
        }
        else return this.coreService.notify("Unsuccessful", res.message, 0);
      },
      err => {
        this.coreService.notify("Unsuccessful", err.message, 0);
      }
    )
  }

  resetQuestionBox(editMode) {
    this.selectedQuestions = [];
    this.questionBox = {
      editMode: editMode,
      questionID: "",
      surveyQuetionnerID: "",
      questionTitle: ""
    };
  }

  onItemSelected(e) {
    console.log(e, this.itemList);
  }

  onSelectQuestion(item) {
    this.questionBox.questionID = null;
    this.questionBox.surveyQuetionnerID = item.surveyQuetionnerID;
    this.questionBox.questionTitle = item.title;
  }

  onDeSelectQuestion(item) {
    this.resetQuestionBox(true);
  }

  onChangeSwitch(event) {
    this.active = event.target.checked;
    if (!this.active) this.itemList.filter(i => i['isSelected'] = false);
  }

  saveQuestion() {
    if (!this.questionBox.questionTitle || !this.questionBox.questionTitle.trim().length) return;

    this.pa.question.push({
      "questionID": this.questionBox.questionID, "surveyQuetionnerID": this.questionBox.surveyQuetionnerID,
      "questionTitle": this.questionBox.questionTitle
    });
    this.resetQuestionBox(false);
  }

  isItemSelected() {
    return this.itemList.filter(i => i['isSelected']).length > 0;
  }

  createPerformanceAppraisal() {
    if (!this.assessmentDate || !this.selectedManager.length || !this.pa.question.length) return;
    if (this.active && !this.isItemSelected()) return;

    this.pa.thirdParty = !this.active ? [] : JSON.parse(JSON.stringify(this.itemList.filter(i => i['isSelected'])));
    this.pa.thirdParty.filter(i => {
      if (i['external']) {
        i['type'] = 'external'; i['email'] = i.itemName;
      }
      else {
        i['type'] = 'internal';
        i['userID'] = i.userId;
      }
    });

    this.pa.thirdParty.push(...this.thirdParty);
    this.pa.userID = this.userID;
    this.pa.managerID = this.selectedManager[0].id;
    this.pa.assessmentDate = !this.assessmentDate ? '' : moment(this.assessmentDate).format('YYYY-MM-DD');

    console.log(this.pa);
    this.isSubmiting = true;
    this.companyService.createPerformanceAppraisal(this.pa).subscribe(
      res => {
        this.isSubmiting = false;
        if (res.code == 200) {
          this.coreService.notify("Successful", res.message, 1);
          // this.router.navigate(['/app/company-admin/performance-appraisal/assessment-by-employee', res.data.performaceAppraisalID]);
          this.router.navigate(['/app/company-admin/listing-page/performance-appraisal-listing']);
        }
        else return this.coreService.notify("Unsuccessful", res.message, 0);
      },
      err => {
        this.isSubmiting = false;
        this.coreService.notify("Unsuccessful", err.message, 0);
      }
    )
  }
}
