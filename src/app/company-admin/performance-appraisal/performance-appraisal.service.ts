import { Injectable } from '@angular/core';
import { RestfulLaravelService } from "../../shared/services/restful-laravel.service";

@Injectable()
export class PerformanceAppraisalService {

  constructor(private restfulService: RestfulLaravelService) { }

  createPerformanceAppraisal(reqBody) {
    return this.restfulService.post('create-update-performance-appraisal', reqBody);
  }

  updatePerformanceAppraisal(reqBody) {
    return this.restfulService.post('create-update-performance-appraisal', reqBody);
  }

  getSueveyQuestions() {
    return this.restfulService.get('detail-survey-category/2');
  }

  getPerformanceIndicators(params) { //{"userID":23}
    return this.restfulService.get(`position-performance-indicator/${params}`);
  }

  getPerformanceAppraisalList(params) { //{"search":"","filter":{"status":{"Awaiting Employee Acceptance":false,"Complete":false},"userID":[],"managerID":[32]}}
    return this.restfulService.get(`list-performance-appraisal/${params}`);
  }

  getPerformanceAppraisalDetail(id) {
    return this.restfulService.get(`performance-appraisal-detail/${id}`);
  }

  getEmployeeList() {
    return this.restfulService.get('show-all-employee');
  }

  getManagerList(id) {
    if(id==null){
      return this.restfulService.get('show-manager-and-ceo-and-fm');      
    }
    else{
      return this.restfulService.get(`show-manager-and-ceo-and-fm/${id}`);    
    }
  }
  

  saveManagerResponse(reqBody) {
    return this.restfulService.post('save-manager-performance-response', reqBody);
  }
}
