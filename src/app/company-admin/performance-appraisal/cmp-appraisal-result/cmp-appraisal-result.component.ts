import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { CoreService } from '../../../shared/services/core.service';
import { PerformanceAppraisalService } from '../performance-appraisal.service';
import { APPCONFIG } from '../../../config';
import { IDatePickerConfig } from 'ng2-date-picker';
import * as moment from "moment";

@Component({
  selector: 'app-cmp-appraisal-result',
  templateUrl: './cmp-appraisal-result.component.html',
  styleUrls: ['./cmp-appraisal-result.component.scss']
})
export class CmpAppraisalResultComponent implements OnInit {
  public progress;
  public collapse = [];
  public isProcessing = false;
  public isSubmitted = false;
  public noResult = true;
  public performaceAppraisalID = "";
  public pa = {
    "performaceAppraisalID": "", "title": "", "assessmentDate": "", "generalManagerNote": "",
    "managerID": { "userID": "", "name": "", "imageUrl": "", "position": "" },
    "userID": { "userID": "", "name": "", "imageUrl": "", "position": "" },
    "performanceIndicator": [], "question": [], "thirdParty": []
  }

  // config: IDatePickerConfig = {};
  // reminderDate; nextAssessmentDate;
  // itemList = [];
  // modelName = '';
  directAssessment = false;

  constructor(public coreService: CoreService, private router: Router, private route: ActivatedRoute,
    public companyService: PerformanceAppraisalService) {

    // this.config.locale = "en";
    // this.config.format = "DD MMM, YYYY hh:mm A";
    // this.config.showMultipleYearsNavigation = true;
    // this.config.weekDayFormat = 'dd';
    // this.config.disableKeypress = true;
    // this.config.monthFormat = "MMM YYYY";
    // this.config.min = moment();
  }

  ngOnInit() {
    APPCONFIG.heading = "Performance appraisal";
    let type = this.route.snapshot.url[0].path;
    this.directAssessment = (type == 'direct-assessment-result');
    if (this.directAssessment) {
      this.directAssessmentProgressBarSteps();
    } else {
      this.progressBarSteps();
    }

    if (this.route.snapshot.params['id']) {
      this.performaceAppraisalID = this.route.snapshot.params['id'];
      this.getPerformanceAppraisal();
    }
    // this.getEmployee();
  }

  progressBarSteps() {
    this.progress = {
      progress_percent_value: 100, //progress percent
      total_steps: 4,
      current_step: 4,
      circle_container_width: 25, //circle container width in percent
      steps: [
        // {num:1,data:"Select Role"},
        { num: 1, data: "Create Performance appraisal" },
        { num: 2, data: "Assement done by employee" },
        { num: 3, data: "Assessment by manager" },
        { num: 4, data: "Result", active: true }
      ]
    }
  }

  directAssessmentProgressBarSteps() {
    this.progress = {
      progress_percent_value: 100, //progress percent
      total_steps: 3,
      current_step: 3,
      circle_container_width: 25, //circle container width in percent
      steps: [
        { num: 1, data: "Create Performance appraisal" },
        { num: 2, data: "Direct assessment" },
        { num: 3, data: "Result", active: true }
      ]
    }
  }

  // getEmployee() {
  //   this.companyService.getEmployeeList().subscribe(
  //     res => {
  //       if (res.code == 200) {
  //         this.itemList = res.data;
  //         this.itemList.filter((item, i) => {
  //           item['itemName'] = item.name; item['id'] = item.userId; item['position'] = item.designation;
  //         });
  //       }
  //       else return this.coreService.notify("Unsuccessful", res.message, 0);
  //     },
  //     err => {
  //       this.coreService.notify("Unsuccessful", err.message, 0);
  //     })
  // }

  isValidData(data) {
    return !(data instanceof Array);
  }

  formatDate(date) {
    return (date && moment(date).isValid()) ? moment(date).format('DD MMM YYYY') : 'Not specified';
  }

  getPerformanceAppraisal() {
    this.isProcessing = true;
    this.companyService.getPerformanceAppraisalDetail(this.performaceAppraisalID).subscribe(
      res => {
        this.isProcessing = false;
        if (res.code == 200) {
          this.pa = res.data;
          this.noResult = false;
        }
        else return this.coreService.notify("Unsuccessful", res.message, 0);
      },
      err => {
        this.isProcessing = false;
        this.coreService.notify("Unsuccessful", err.message, 0);
      }
    )
  }

  // saveManagerResponse(route?: any[]) {
  //   let managerResponse = {
  //     "performaceAppraisalID": this.performaceAppraisalID,
  //     "nextAssessmentDate": this.nextAssessmentDate ? moment(this.nextAssessmentDate).format('YYYY-MM-DD HH:mm:ss') : '',
  //     "reminderDate": this.reminderDate ? moment(this.reminderDate).format('YYYY-MM-DD HH:mm:ss') : '',
  //     "generalManagerNote": this.pa.generalManagerNote, "performanceIndicator": this.pa.performanceIndicator,
  //     "question": this.directAssessment ? [] : this.pa.question, "thirdParty": []
  //   }
  //   // managerResponse['status'] = this.isValid() ? 'Complete' : 'Under Assessment';
  //   managerResponse['directAssessment'] = this.directAssessment;
  //   managerResponse.thirdParty = JSON.parse(JSON.stringify(this.itemList.filter(i => i['isSelected'])));
  //   managerResponse.thirdParty.filter(i => {
  //     if (i['external']) {
  //       i['type'] = 'external'; i['email'] = i.itemName;
  //     }
  //     else {
  //       i['type'] = 'internal';
  //       i['userID'] = i.userId;
  //     }
  //   });

  //   console.log(managerResponse);
  //   this.isSubmitted = true;
  //   this.companyService.saveManagerResponse(managerResponse).subscribe(
  //     res => {
  //       this.isSubmitted = false;
  //       if (res.code == 200) {
  //         this.coreService.notify("Successful", res.message, 1);
  //         if (route && route.length) {
  //           this.router.navigate(route);
  //         } else {
  //           this.router.navigate(['/app/company-admin/performance-appraisal/result/', this.performaceAppraisalID])
  //         }
  //       }
  //       else return this.coreService.notify("Unsuccessful", res.message, 0);
  //     },
  //     err => {
  //       this.isSubmitted = false;
  //       this.coreService.notify("Unsuccessful", err.message, 0);
  //     }
  //   )
  // }
}
