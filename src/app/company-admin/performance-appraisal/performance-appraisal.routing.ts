import { ModuleWithProviders } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CreatePerformanceAppraisalComponent } from './create-performance-appraisal/create-performance-appraisal.component';
import { SelfAssessmentByEmployeeComponent } from './self-assessment-by-employee/self-assessment-by-employee.component';
import { AssessmentByManagerComponent } from './assessment-by-manager/assessment-by-manager.component';
import { CmpAppraisalResultComponent } from './cmp-appraisal-result/cmp-appraisal-result.component';

export const PerformanceAppraisalPagesRoutes: Routes = [
    { path: 'create/:uid', component: CreatePerformanceAppraisalComponent },
    { path: 'edit/:id', component: CreatePerformanceAppraisalComponent },
    { path: 'assessment-by-employee/:id', component: SelfAssessmentByEmployeeComponent },
    { path: 'assessment-by-manager/:id', component: AssessmentByManagerComponent },
    { path: 'result/:id', component: CmpAppraisalResultComponent },

    { path: 'direct-assessment/:id', component: AssessmentByManagerComponent },
    { path: 'direct-assessment-result/:id', component: CmpAppraisalResultComponent }
];

export const PerformanceAppraisalRoutingModule = RouterModule.forChild(PerformanceAppraisalPagesRoutes);
