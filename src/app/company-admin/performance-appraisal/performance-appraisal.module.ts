import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { MaterialModule } from '@angular/material';
import { SharedModule } from '../../shared/shared.module';
import { PerformanceAppraisalRoutingModule } from './performance-appraisal.routing';
import { PerformanceAppraisalService } from './performance-appraisal.service';
import { CreatePerformanceAppraisalComponent } from './create-performance-appraisal/create-performance-appraisal.component';
import { SelfAssessmentByEmployeeComponent } from './self-assessment-by-employee/self-assessment-by-employee.component';
import { AssessmentByManagerComponent } from './assessment-by-manager/assessment-by-manager.component';
import { CmpAppraisalResultComponent } from './cmp-appraisal-result/cmp-appraisal-result.component';
import { PerformanceAppraisalGuardService } from './performance-appraisal-guard.service';
import { AngularMultiSelectModule } from 'angular2-multiselect-checkbox-dropdown/angular2-multiselect-dropdown';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    SharedModule,
    PerformanceAppraisalRoutingModule,
    AngularMultiSelectModule,
    MaterialModule
  ],
  declarations: [CreatePerformanceAppraisalComponent, SelfAssessmentByEmployeeComponent, AssessmentByManagerComponent, CmpAppraisalResultComponent],
  providers: [PerformanceAppraisalService, PerformanceAppraisalGuardService]
})
export class PerformanceAppraisalModule { }
