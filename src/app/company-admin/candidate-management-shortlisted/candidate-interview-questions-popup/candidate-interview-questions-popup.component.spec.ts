import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CandidateInterviewQuestionsPopupComponent } from './candidate-interview-questions-popup.component';

describe('CandidateInterviewQuestionsPopupComponent', () => {
  let component: CandidateInterviewQuestionsPopupComponent;
  let fixture: ComponentFixture<CandidateInterviewQuestionsPopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CandidateInterviewQuestionsPopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CandidateInterviewQuestionsPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
