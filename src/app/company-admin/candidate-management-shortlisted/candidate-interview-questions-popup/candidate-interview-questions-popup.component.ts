import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { CoreService } from '../../../shared/services/core.service';
import { CandidateManagementShortlistedService } from '../candidate-management-shortlisted.service';
import { ConfirmService } from '../../../shared/services/confirm.service';
import { APPCONFIG } from '../../../config';
import { MdSnackBar, MdDialog, MdDialogRef } from '@angular/material';

@Component({
  selector: 'app-candidate-interview-questions-popup',
  templateUrl: './candidate-interview-questions-popup.component.html',
  styleUrls: ['./candidate-interview-questions-popup.component.scss']
})
export class CandidateInterviewQuestionsPopupComponent implements OnInit {

  public candidateID:any;
  public PopulatedData:any;
  public isProcessing:Boolean=false;
  public isActionProcessing:Boolean=false;
  public interviewQuestions:any;
  public isButtonClicked=false;
  public addsID:any;
  public isquestionOnly=false;
  public refrenceType:any;
  public requestType:any;
  constructor(public router: Router, public activatedRoute: ActivatedRoute,
    public dialog: MdDialog,public dialogRef:MdDialogRef<CandidateInterviewQuestionsPopupComponent>,
    public coreService: CoreService, public confirmService: ConfirmService, public viewContainerRef: ViewContainerRef,
    public candidateManagementShortlistedService: CandidateManagementShortlistedService) { }

  ngOnInit() {
     if( this.requestType == 'questions'){
      this.isquestionOnly=true;
      this.getInterviewQuestions();
    }  
    else  this.getInterviewQuestionsAndAnswers();
  }

  getInterviewQuestions(){
      this.isProcessing = true;
      this.candidateManagementShortlistedService.getCandidateInterviewQuestions(this.addsID).subscribe(
      d => {
        this.isProcessing = false;
        if (d.code == "200") {
          this.interviewQuestions=d.data;
        }
        else this.coreService.notify("Unsuccessful", d.message, 0);
      },
      error => {
        this.coreService.notify("Unsuccessful", error.message, 0);
        this.isProcessing = false
      }
    )
  }

  getInterviewQuestionsAndAnswers(){
     this.isProcessing = true;
      this.candidateManagementShortlistedService.getCandidateInterviewQuestionsAndAnswers(this.candidateID).subscribe(
      d => {
        this.isProcessing = false;
        if (d.code == "200") {
          this.interviewQuestions=d.data;
        }
        else this.coreService.notify("Unsuccessful", d.message, 0);
      },
      error => {
        this.coreService.notify("Unsuccessful", error.message, 0);
        this.isProcessing = false
      }
    )
  }

  onSave(isvalid){
    this.isButtonClicked=true;
    if(isvalid){
            this.isActionProcessing = true;    
            let reqBody={
              candidateID:this.candidateID,
              data:this.interviewQuestions
            }  
          this.candidateManagementShortlistedService.saveInterviewQuestions(reqBody).subscribe(
          d => {
            this.isActionProcessing = false;
            if (d.code == "200") {
              this.dialogRef.close(true);
            }
            else {
            this.isActionProcessing = false;
            this.coreService.notify("Unsuccessful", d.message, 0); 
            }
          },
          error => {
            this.coreService.notify("Unsuccessful", error.message, 0);
            this.isActionProcessing = false
          }
        )
    }
  }

}
