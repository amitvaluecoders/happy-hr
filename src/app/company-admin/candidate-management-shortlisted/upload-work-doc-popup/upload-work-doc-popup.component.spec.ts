import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UploadWorkDocPopupComponent } from './upload-work-doc-popup.component';

describe('UploadWorkDocPopupComponent', () => {
  let component: UploadWorkDocPopupComponent;
  let fixture: ComponentFixture<UploadWorkDocPopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UploadWorkDocPopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UploadWorkDocPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
