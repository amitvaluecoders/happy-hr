import { Component, OnInit } from '@angular/core';
import { RestfulLaravelService } from '../../../shared/services/restful-laravel.service';
import { MdDialogRef } from '@angular/material';

@Component({
  selector: 'app-upload-work-doc-popup',
  templateUrl: './upload-work-doc-popup.component.html',
  styleUrls: ['./upload-work-doc-popup.component.scss']
})
export class UploadWorkDocPopupComponent implements OnInit {

  public candidateID:any;
  public fileName:any;
  public title='';
  public isActionProcessing=false;
  public isProcessing=false;
  public docPath='';
  public docError=false;
  constructor(public restfulWebService:RestfulLaravelService,public dialog: MdDialogRef<UploadWorkDocPopupComponent>) { }

  ngOnInit() {
  }

  fileChange(event) {
    this.isProcessing=true;
    this.docError=false;
    this.fileName='';
    this.docPath='';
    let fileList: FileList = event.target.files;
    if(fileList.length > 0) {
        let file: File = fileList[0];
        let formData:FormData = new FormData();
        formData.append('eligibilityDoc',file);
        this.restfulWebService.post('file-upload',formData).subscribe(
          d=>{ 
            if(d.code="200"){
              this.isProcessing=false;
                this.docPath=d.data.eligibilityDoc;
                this.fileName=file.name;
            }
            else {this.docError=true;this.isProcessing=false;}
          },
          error=>{this.docError=true;this.isProcessing=false;}
          )
    }
}

  saveDoc(){
    if(this.docPath){
      this.isActionProcessing=true;
        let reqBody={
        candidateID:this.candidateID,
        eligibilityDoc:this.docPath,
        title:this.title 
        }
        this.restfulWebService.post('candidate-doc-upload',reqBody).subscribe(
          d=>{ 
            if(d.code="200"){
               this.dialog.close(d.data.workDoc);
            }
            else {
              this.isActionProcessing=false;
            }
          },
          error=>{ this.isActionProcessing=false;}
          )
  }
}

}
