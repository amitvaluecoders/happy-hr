import { Injectable } from '@angular/core';
import { RestfulLaravelService} from '../../shared/services/restful-laravel.service';

@Injectable()
export class CandidateManagementShortlistedService {

  constructor(public restfulLaravelService:RestfulLaravelService) { }

  getcandidatesFilterData(){
    return this.restfulLaravelService.get(`get-candidate-filter-data`);
    
  }

  getEmployeeList(){
    return this.restfulLaravelService.get(`show-all-employee`);
  }

  createNote(reqBody){
    return this.restfulLaravelService.post(`create-note`,reqBody);
  }

  getcandidatesList(reqBody){
        return this.restfulLaravelService.get(`get-candidate-list/${encodeURIComponent(reqBody)}`)

  }
  saveAction(reqBody):any{
    return this.restfulLaravelService.put(`candidate-change-status-action`,reqBody);
    
  }

  getcandidateRejectionNote():any{
    return this.restfulLaravelService.get(`candidate-rejection-template`);
  }

  getCandidateInterviewQuestions(id):any{
    return this.restfulLaravelService.get(`get-advertisement-interview-question/${id}`);    
  }

  saveInterviewQuestions(reqBody):any{
    return this.restfulLaravelService.post(`candidate-save-interview-answer`,reqBody);    
  } 
  
  getCandidateInterviewQuestionsAndAnswers(reqBody):any{
    return this.restfulLaravelService.get(`get-candidate-interview-answer/${reqBody}`);    
  }

  getCandidateRefrenceQuestions(id):any{
    return this.restfulLaravelService.get(`get-advertisement-reference-question/${id}`);    
  }

  getCandidateRefrenceQuestionsandAnswers(reqBody):any{
    return this.restfulLaravelService.get(`get-candidate-reference-answer/${JSON.stringify(reqBody)}`);    
  }

  saveRefrenceQuestions(reqBody):any{
    return this.restfulLaravelService.post(`candidate-save-reference-answer`,reqBody);    
  }
}
