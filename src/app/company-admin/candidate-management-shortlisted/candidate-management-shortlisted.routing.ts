import {  ModuleWithProviders  } from '@angular/core';
import {  RouterModule,Routes  } from '@angular/router';

import { CandidateManagementShortlistedComponent } from './candidate-management-shortlisted/candidate-management-shortlisted.component';

export const CandidateManagementShortlistedPagesRoutes: Routes = [
    {
        path: '',
        canActivate:[],
        children: [
           { path: 'list/:id', component: CandidateManagementShortlistedComponent},
         ]
    }
];

export const CandidateManagementShortlistedRoutingModule = RouterModule.forChild(CandidateManagementShortlistedPagesRoutes);
