import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RejectAllCcandidatesPopupComponent } from './reject-all-ccandidates-popup.component';

describe('RejectAllCcandidatesPopupComponent', () => {
  let component: RejectAllCcandidatesPopupComponent;
  let fixture: ComponentFixture<RejectAllCcandidatesPopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RejectAllCcandidatesPopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RejectAllCcandidatesPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
