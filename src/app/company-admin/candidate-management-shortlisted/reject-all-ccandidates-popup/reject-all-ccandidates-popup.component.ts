import { Component, OnInit } from '@angular/core';
import { MdDialogRef } from '@angular/material';

@Component({
  selector: 'app-reject-all-ccandidates-popup',
  templateUrl: './reject-all-ccandidates-popup.component.html',
  styleUrls: ['./reject-all-ccandidates-popup.component.scss']
})
export class RejectAllCcandidatesPopupComponent implements OnInit {

   constructor(public dialogRef: MdDialogRef<RejectAllCcandidatesPopupComponent>) {}

  ngOnInit() {
  }

}
