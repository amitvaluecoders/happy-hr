import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { CoreService } from '../../../shared/services/core.service';
import { CandidateManagementShortlistedService } from '../candidate-management-shortlisted.service';
import { ConfirmService } from '../../../shared/services/confirm.service';
import { APPCONFIG } from '../../../config';
import { MdSnackBar, MdDialog, MdDialogRef } from '@angular/material';
import { UploadWorkDocPopupComponent } from '../upload-work-doc-popup/upload-work-doc-popup.component';
import { CandidateRejectionNotePopupComponent} from '../candidate-rejection-note-popup/candidate-rejection-note-popup.component';
import { CandidateInterviewQuestionsPopupComponent } from '../candidate-interview-questions-popup/candidate-interview-questions-popup.component';
import { CandidateRefrenceQuestionsComponent } from '../candidate-refrence-questions/candidate-refrence-questions.component';
import { RejectAllCcandidatesPopupComponent } from '../reject-all-ccandidates-popup/reject-all-ccandidates-popup.component';

@Component({
  selector: 'app-candidate-management-shortlisted',
  templateUrl: './candidate-management-shortlisted.component.html',
  styleUrls: ['./candidate-management-shortlisted.component.scss']
})
export class CandidateManagementShortlistedComponent implements OnInit {
  public toggleFilter: boolean=false;
  public isProcessing = false;
  public isSelectAll=false;
  public isApplyBtnClicked=false;
  public isFilterDataProcessing = false;
  public unsubscriber: any;
  public candidates = [];
  tempReqBody = {
    "search": "",
    "filter": {
      "status": [],
    }
  }
  reqBody = {
    "search": "",
    "filter": {
      "status": {},
    }
  }

  constructor(public router: Router, public activatedRoute: ActivatedRoute,public dialog: MdDialog,
    public coreService: CoreService, public confirmService: ConfirmService, public viewContainerRef: ViewContainerRef,
    public candidateManagementShortlistedService: CandidateManagementShortlistedService) { }

  ngOnInit() {
    this.activatedRoute.snapshot.params['id'];
    this.getCandidateList();
    APPCONFIG.heading = "Candidate management";
    this.getFilterData();
    this.toggleFilter = true;
  }

  getFilterData() {
    this.isFilterDataProcessing = true;
    this.candidateManagementShortlistedService.getcandidatesFilterData().subscribe(
      d => {
        this.isFilterDataProcessing = false;
        if (d.code == "200") {
          this.tempReqBody.filter = d.data;
        }
        else this.coreService.notify("Unsuccessful", d.message, 0);
      },
      error => {
        this.coreService.notify("Unsuccessful", error.message, 0);
        this.isFilterDataProcessing = false
      }
    )
  }

  onSearch() {
    this.reqBody.search = encodeURIComponent(this.tempReqBody.search);
    this.getCandidateList();
  }


  // method to set filter data for getting filtered list. 
  onApplyFilters() {
    this.reqBody.filter.status = {};
    for (let i of this.tempReqBody.filter.status) {
      if (i.isChecked) this.reqBody.filter.status[i.title] = true;
    }
     this.getCandidateList();
  }

  //method to get candidate list .
  getCandidateList() {
    this.isProcessing = true;
    this.reqBody['advertisementID'] = this.activatedRoute.snapshot.params['id'];
    this.candidateManagementShortlistedService.getcandidatesList(JSON.stringify(this.reqBody)).subscribe(
      d => {
        this.isProcessing = false;
        if (d.code == "200") {
          this.candidates = d.data;
          this.candidates = this.candidates.filter(item=>{
            item['isActionProcessing']=false;
            item['isSelected']=false;
            return item;
          })
        }
        else this.coreService.notify("Unsuccessful", d.message, 0);
      },
      error => {
        this.coreService.notify("Unsuccessful", error.message, 0);
        this.isProcessing = false
      }
    )
  }

  onSelectAll(isSelected){
    this.candidates = this.candidates.filter(item=>{
            item['isSelected']=!isSelected;
            return item;
          })
  }

  // method to perform action to all selected candidates
  onActionAll(action){
    // let ids=[];
    // for(let i of this.candidates){
    //   if(i.isSelected) {
    //     ids.push(i.candidateID);
    //     i['isActionProcessing']=true;;
    //   }
    // }
    //     let reqBody = {
    //     candidateIDs: ids,
    //     action: action
    //   }
    // if(action == 'Rejected')  this.appendRejectionNoteToReqBody(reqBody,);
    //   else this.onSaveAction(reqBody); 

  }

 // method to perform action to all selected candidates
  onActionSelected(i,action){
    let ids=[]; 
       ids.push(i.candidateID);
       i['isActionProcessing']=true;
        let reqBody = {
        candidateIDs: ids,
        action: action
      }
      if(action == 'Rejected')  this.appendRejectionNoteToReqBody(reqBody,i);
      else this.onSaveAction(reqBody,i); 

  }

  appendRejectionNoteToReqBody(reqBody,i){
        
        let dialogRef = this.dialog.open(RejectAllCcandidatesPopupComponent); 
                
              dialogRef.afterClosed().subscribe(r => {
              if(r){
                  if(r=='yes'){
                       let ids=[];
                        for(let i of this.candidates){
                          if(i.status=='Not Suitable') {
                            ids.push(i.candidateID);
                          }
                        }
                        reqBody['candidateIDs']=ids;
                        this.submitRejectNote(reqBody);
                  }
                  if(r=='no') this.submitRejectNote(reqBody);
              }
            })
 
  }

  submitRejectNote(reqBody){
    let dialogRef = this.dialog.open(CandidateRejectionNotePopupComponent); 
              dialogRef.componentInstance.reqBody=reqBody;
                    dialogRef.afterClosed().subscribe(r => {
                    if(r){
                          this.candidates = this.candidates.filter(item=>{
                      item['isActionProcessing']=false;
                      if( reqBody.candidateIDs.indexOf(item.candidateID) != -1 )item['status']=reqBody.action; 
                      item['isSelected']=false;
                      return item;
                    })
                    this.isProcessing = false;  
                    this.isSelectAll = false; 
                    }
                  })
  }


  onSaveAction(reqBody,i){
    this.isProcessing = true;    
     this.candidateManagementShortlistedService.saveAction(reqBody).subscribe(
      d => {
        if (d.code == "200") {    
          this.coreService.notify("Successful", d.message, 1);
          this.candidates = this.candidates.filter(item=>{
            item['isActionProcessing']=false;
            if( reqBody.candidateIDs.indexOf(item.candidateID) != -1 )item['status']=reqBody.action; 
            item['isSelected']=false;
            return item;
          })

          if(reqBody['action'] == 'Offered') this.router.navigate([`/app/company-admin/organisation-chart/cid/${i.candidateID}`]);
 
           this.isProcessing = false;  
           this.isSelectAll = false;     
        }
        else{
           this.isProcessing = false;              
          this.coreService.notify("Unsuccessful", d.message, 0);
        } 
      },
      error => {
        this.isProcessing = false;            
        this.coreService.notify("Unsuccessful", error.message, 0);
      }
    )
  }

  onUploadDoc(c){
        let dialogRef = this.dialog.open(UploadWorkDocPopupComponent); 
          dialogRef.componentInstance.candidateID=c.candidateID; 
          dialogRef.afterClosed().subscribe(r => {
          if(r){
              c['workDoc']={
                  name:r.name,
                  path:r.path,
                  insertTime:r.insertTime
              }
          }
        })
  }

  onConductInterview(c,type){
       let dialogRef = this.dialog.open(CandidateInterviewQuestionsPopupComponent,{height:"600px" ,width:"1000px"}); 
          dialogRef.componentInstance.candidateID=c.candidateID;  
          dialogRef.componentInstance.requestType=type;          
          // dialogRef.componentInstance.refrenceType=(index==0)?'First':'Second';
          dialogRef.componentInstance.addsID=this.activatedRoute.snapshot.params['id']; 
          dialogRef.afterClosed().subscribe(r => {
          if(r){
            this.getCandidateList();
            //  c['interviewGuide'].heading='Interview Conducted';
            //  c['interviewGuide'].interviewDate=new Date();
          }
        })
  }

  onConductRefrenceCheck(c,index,type){
       let dialogRef = this.dialog.open(CandidateRefrenceQuestionsComponent,{height:"600px" ,width:"1000px"}); 
          dialogRef.componentInstance.candidateID=c.candidateID;  
          let d= new Date();
          let userInfo=JSON.parse(localStorage.getItem('happyhr-userInfo'));          
          let tempData ={
            candidateName:c.employeeData.name,
            date:d.getFullYear()+"-"+d.getMonth()+"-"+d.getDate(),
            refrenceCheckBy:userInfo.fullName
          }
          dialogRef.componentInstance.populatedData=tempData;          
          dialogRef.componentInstance.refrenceType=(index==0)?'First':'Second';
          dialogRef.componentInstance.requestType=type;
          dialogRef.componentInstance.addsID=this.activatedRoute.snapshot.params['id']; 
          dialogRef.afterClosed().subscribe(r => {
          if(r){
            this.getCandidateList();            
            //  c['referenceCheck'].heading='Reference Conducted';
            //  c['referenceCheck'].interviewDate=new Date();
  
          }
        })
  }
  noCoverPopUp(){
    this.coreService.notify("Unsuccessful","The candidate has not uploaded the cover letter yet.", 0);    
  }
  noResumePopUp(){
    this.coreService.notify("Unsuccessful","The candidate has not uploaded the resume yet.", 0);        
  }

 
}
