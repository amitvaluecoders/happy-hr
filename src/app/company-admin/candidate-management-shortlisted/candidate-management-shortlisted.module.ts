import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CandidateManagementShortlistedRoutingModule } from './candidate-management-shortlisted.routing';
import { CandidateManagementShortlistedService } from './candidate-management-shortlisted.service';
import { CandidateManagementShortlistedComponent } from './candidate-management-shortlisted/candidate-management-shortlisted.component';
import { FormsModule } from '@angular/forms';
import { DataTableModule } from 'angular2-datatable';
import { SharedModule } from '../../shared/shared.module';
import { MaterialModule } from '@angular/material';
import { UploadWorkDocPopupComponent } from './upload-work-doc-popup/upload-work-doc-popup.component';
import { RestfulLaravelService } from '../../shared/services/restful-laravel.service';
import { CandidateRejectionNotePopupComponent } from './candidate-rejection-note-popup/candidate-rejection-note-popup.component';
import { CandidateInterviewQuestionsPopupComponent } from './candidate-interview-questions-popup/candidate-interview-questions-popup.component';
import { CandidateRefrenceQuestionsComponent } from './candidate-refrence-questions/candidate-refrence-questions.component';
import { RejectAllCcandidatesPopupComponent } from './reject-all-ccandidates-popup/reject-all-ccandidates-popup.component';


@NgModule({
  imports: [
    MaterialModule,
    SharedModule,
    DataTableModule,
    FormsModule,
    CommonModule,
    CandidateManagementShortlistedRoutingModule
  ],
  declarations: [CandidateManagementShortlistedComponent, UploadWorkDocPopupComponent, CandidateRejectionNotePopupComponent, CandidateInterviewQuestionsPopupComponent, CandidateRefrenceQuestionsComponent, RejectAllCcandidatesPopupComponent],
  providers:[CandidateManagementShortlistedService,RestfulLaravelService],
  entryComponents:[RejectAllCcandidatesPopupComponent,UploadWorkDocPopupComponent,CandidateRefrenceQuestionsComponent,CandidateRejectionNotePopupComponent,CandidateInterviewQuestionsPopupComponent]
})
export class CandidateManagementShortlistedModule { }
