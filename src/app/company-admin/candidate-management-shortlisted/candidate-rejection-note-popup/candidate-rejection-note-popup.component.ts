import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { CoreService } from '../../../shared/services/core.service';
import { CandidateManagementShortlistedService } from '../candidate-management-shortlisted.service';
import { ConfirmService } from '../../../shared/services/confirm.service';
import { APPCONFIG } from '../../../config';
import { MdSnackBar, MdDialog, MdDialogRef } from '@angular/material';

@Component({
  selector: 'app-candidate-rejection-note-popup',
  templateUrl: './candidate-rejection-note-popup.component.html',
  styleUrls: ['./candidate-rejection-note-popup.component.scss']
})
export class CandidateRejectionNotePopupComponent implements OnInit {
    public isButtonClicked=false;
    public isProcessing=false;
    public isActionProcessing=false;
    rejectionNote="";
    public reqBody:any;
    constructor(public router: Router, public activatedRoute: ActivatedRoute,public dialog: MdDialog,
    public dialogRef: MdDialogRef<CandidateRejectionNotePopupComponent>,
    public coreService: CoreService, public confirmService: ConfirmService, public viewContainerRef: ViewContainerRef,
    public candidateManagementShortlistedService: CandidateManagementShortlistedService) { }

  ngOnInit() {
    this.getRejectionNote();
  }

  getRejectionNote(){
       this.isProcessing = true;
    this.candidateManagementShortlistedService.getcandidateRejectionNote().subscribe(
      d => {
        this.isProcessing = false;
        if (d.code == "200") {
          this.rejectionNote = d.data.body;
        }
        else{
          this.isProcessing = false
          this.coreService.notify("Unsuccessful", d.message, 0);
        } 
      },
      error => {
        this.coreService.notify("Unsuccessful", error.message, 0);
        this.isProcessing = false
      }
    )
  }

  onSave(isValid){
    this.isButtonClicked=true;
    if(isValid){
      this.isActionProcessing=true;
      this.reqBody['rejectionNote']=this.rejectionNote;
       this.candidateManagementShortlistedService.saveAction(this.reqBody).subscribe(
      d => {
        if (d.code == "200") {    
          this.coreService.notify("Successful", d.message, 1);
           this.dialogRef.close(true);
        }
        else{
           this.isActionProcessing=false;                
          this.coreService.notify("Unsuccessful", d.message, 0);
        } 
      },
      error => {
           this.isActionProcessing=false;                               
        this.coreService.notify("Unsuccessful", error.message, 0);
      }
    )
    }
  }

}
