import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CandidateRejectionNotePopupComponent } from './candidate-rejection-note-popup.component';

describe('CandidateRejectionNotePopupComponent', () => {
  let component: CandidateRejectionNotePopupComponent;
  let fixture: ComponentFixture<CandidateRejectionNotePopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CandidateRejectionNotePopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CandidateRejectionNotePopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
