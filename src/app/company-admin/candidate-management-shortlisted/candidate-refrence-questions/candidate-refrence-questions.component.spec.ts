import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CandidateRefrenceQuestionsComponent } from './candidate-refrence-questions.component';

describe('CandidateRefrenceQuestionsComponent', () => {
  let component: CandidateRefrenceQuestionsComponent;
  let fixture: ComponentFixture<CandidateRefrenceQuestionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CandidateRefrenceQuestionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CandidateRefrenceQuestionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
