import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { CoreService } from '../../../shared/services/core.service';
import { CandidateManagementShortlistedService } from '../candidate-management-shortlisted.service';
import { ConfirmService } from '../../../shared/services/confirm.service';
import { APPCONFIG } from '../../../config';
import { MdSnackBar, MdDialog, MdDialogRef } from '@angular/material';

@Component({
  selector: 'app-candidate-refrence-questions',
  templateUrl: './candidate-refrence-questions.component.html',
  styleUrls: ['./candidate-refrence-questions.component.scss']
})
export class CandidateRefrenceQuestionsComponent implements OnInit {

 public populatedData:any;
  public candidateID:any;
  public isProcessing:Boolean=false;
  public isActionProcessing:Boolean=false;
  public questions:any;
  public isButtonClicked=false;
  public isquestionOnly=false;
  public addsID:any;
  public RefrenceNote='';
  public refrenceType:any; 
  public requestType:any;
  public isStatement:'';
  constructor(public router: Router, public activatedRoute: ActivatedRoute,
    public dialog: MdDialog,public dialogRef:MdDialogRef<CandidateRefrenceQuestionsComponent>,
    public coreService: CoreService, public confirmService: ConfirmService, public viewContainerRef: ViewContainerRef,
    public candidateManagementShortlistedService: CandidateManagementShortlistedService) { }

  ngOnInit() {
    if( this.requestType == 'questions'){
      this.isquestionOnly=true;
      this.getrefrenceQuestions();
    }  
    else this.getrefrenceQuestionsAndAnswers();
  }

  getrefrenceQuestions(){
      this.isProcessing = true;
      this.candidateManagementShortlistedService.getCandidateRefrenceQuestions(this.addsID).subscribe(
      d => {
        this.isProcessing = false;
        if (d.code == "200") {
          this.questions=d.data;
          this.questions=this.questions.filter(item=>{
            if(item.referenceID=='1'){
              item.data[0].answer=this.populatedData.refrenceCheckBy;
              item.data[1].answer=this.populatedData.date;
              item.data[2].answer=this.populatedData.candidateName;
            }
            return item;
          });

          console.log("questionssssssssssss",this.questions)
        }
        else this.coreService.notify("Unsuccessful", d.message, 0);
      },
      error => {
        this.coreService.notify("Unsuccessful", error.message, 0);
        this.isProcessing = false
      }
    )
  }

    getrefrenceQuestionsAndAnswers(){
      this.isProcessing = true;
      let reqBody={
       candidateID:this.candidateID,
       referenceType:this.refrenceType
      }
      this.candidateManagementShortlistedService.getCandidateRefrenceQuestionsandAnswers(reqBody).subscribe(
      d => {
        this.isProcessing = false;
        if (d.code == "200") {
          this.questions=d.data;
        }
        else this.coreService.notify("Unsuccessful", d.message, 0);
      },
      error => {
        this.coreService.notify("Unsuccessful", error.message, 0);
        this.isProcessing = false
      }
    )
  }

  onSave(isvalid){
    this.isButtonClicked=true;
    if(isvalid){
            this.isActionProcessing = true;  
            let reqBody:any;
            if(this.isStatement == new String("yes")){
               reqBody={
              candidateID:this.candidateID,
              data:this.questions
            }
            }
             if(this.isStatement == new String("no")){
               reqBody={
              candidateID:this.candidateID,
              data:{
                referenceNote:this.RefrenceNote
              }
            }
            }    
            
          this.candidateManagementShortlistedService.saveRefrenceQuestions(reqBody).subscribe(
          d => {
            this.isActionProcessing = false;
            if (d.code == "200") {
              this.dialogRef.close(true);
            }
            else {
            this.isActionProcessing = false;
            this.coreService.notify("Unsuccessful", d.message, 0); 
            }
          },
          error => {
            this.coreService.notify("Unsuccessful", error.message, 0);
            this.isActionProcessing = false
          }
        )
    }
  }


}
