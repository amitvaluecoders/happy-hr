import { Component, OnInit } from '@angular/core';
import { APPCONFIG } from '../../../config';

@Component({
  selector: 'app-cmp-upcoming-payment',
  templateUrl: './cmp-upcoming-payment.component.html',
  styleUrls: ['./cmp-upcoming-payment.component.scss']
})
export class CmpUpcomingPaymentComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    APPCONFIG.heading="Sub company upcoming payments";
  }

}
