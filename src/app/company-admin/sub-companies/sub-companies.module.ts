import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SubCompaniesRoutingModule } from './sub-companies.routing';
import { SubCompaniesService } from './sub-companies.service';
import { SubCompanyListComponent } from './sub-company-list/sub-company-list.component';
import { CmpUpcomingPaymentComponent } from './cmp-upcoming-payment/cmp-upcoming-payment.component';
import { SubCompaniesGuardService } from './sub-companies-guard.service';
import {FormsModule} from '@angular/forms';
import { MaterialModule } from '@angular/material';
import { SharedModule } from '../../shared/shared.module';
import { InviteSubCompanyComponent } from './invite-sub-company/invite-sub-company.component';
import { DataTableModule } from 'angular2-datatable';
@NgModule({
  imports: [
    FormsModule,
    MaterialModule,
    CommonModule,
    SubCompaniesRoutingModule,
    SharedModule,
    DataTableModule
  ],
  declarations: [SubCompanyListComponent, CmpUpcomingPaymentComponent, InviteSubCompanyComponent],
  providers:[SubCompaniesService,SubCompaniesGuardService]
})
export class SubCompaniesModule { }
