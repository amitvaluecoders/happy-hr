import { Injectable } from '@angular/core';
import { RestfulLaravelService } from "../../shared/services/restful-laravel.service";

@Injectable()
export class SubCompaniesService {

  constructor(private restfulService: RestfulLaravelService) { }


  register(reqBody){
      return this.restfulService.post('create-sub-company', reqBody);
  }

  listSubCompany(){
      return this.restfulService.get('list-sub-company');
  }

  deleteSubCompany(subId){
      return this.restfulService.delete(`delete-company/${ subId }`);
  }

  incognito(id){
    return this.restfulService.get(`login-as-sub-company/${id}`);    
  }

}
