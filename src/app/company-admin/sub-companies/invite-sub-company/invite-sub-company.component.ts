import { Component, OnInit } from '@angular/core';
import { APPCONFIG } from '../../../config';
import { Router, ActivatedRoute } from '@angular/router';
import { CoreService } from "../../../shared/services/core.service";
import { SubCompaniesService  } from "./../sub-companies.service"

@Component({
  selector: 'app-invite-sub-company',
  templateUrl: './invite-sub-company.component.html',
  styleUrls: ['./invite-sub-company.component.scss']
})
export class InviteSubCompanyComponent implements OnInit {
  
  public progress;
  constructor(public coreService: CoreService, public router: Router , public activatedRoute : ActivatedRoute
  ,public subCompaniesService : SubCompaniesService) { }
  public isProcessing : Boolean;
   public isButtonClicked=false;
  public payment = "company";
  public invite = {
          "firstName":"",
          "lastName":"",
          "companyName":"",
          "ABN":"",
          "streetAddress":"",
          "suburb":"",
          "postalCode":"",
          "email":"",
          "nodeID": "",
 }

  ngOnInit() {
    APPCONFIG.heading="Invite a sub company";
   if(!this.activatedRoute.snapshot.params['nodeID']){
      return this.coreService.notify("Unsuccessful", "Somthing going wrong ! Try Again", 0);
   }
    this.progressBarSteps(); 
  }

  register(form){
      if(form.form.valid){
           this.isProcessing = true;
           this.invite.nodeID = this.activatedRoute.snapshot.params['nodeID'];
          
           this.subCompaniesService.register(this.invite).subscribe(
                      d => {
                        this.isButtonClicked = false;
                         this.isProcessing = false;
                        if (d.code == "200") {
                          if(this.payment == "company") this.router.navigate([`/user/sub-company-registration/payments/${d.data.companyID}`]);
                          else {
                            this.isButtonClicked=false;
                            form.reset();  
                            this.router.navigate(['app/company-admin/organisation-chart']);    
                            this.coreService.notify("Successful", d.message, 1);                    
                          }
                         
                        }
                        else this.coreService.notify("Unsuccessful", d.message, 0);
                      },
                      error => {
                          this.isButtonClicked = false;
                         this.isProcessing = false;
                         if(error.code == "400"){
                              return this.coreService.notify("Unsuccessful", error.message , 0);
                         }
                          else return this.coreService.notify("Unsuccessful", "Error while getting data", 0);
                      },
                      () => { }
              );
      }
  }


   progressBarSteps() {
      this.progress={
          progress_percent_value: 33, //progress percent
          total_steps: 3,
          current_step: 1,
          circle_container_width: 33, //circle container width in percent
        steps: [
          { num: 1, data: "Sign up", active: true },
          { num: 2, data: "Pick a plan" },
          { num: 3, data: "Confirmation" }
        ]
       }         
  }
}
