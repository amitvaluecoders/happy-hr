import {  ModuleWithProviders  } from '@angular/core';
import {  RouterModule,Routes  } from '@angular/router';
import {  SubCompanyListComponent } from './sub-company-list/sub-company-list.component';
import {  CmpUpcomingPaymentComponent } from './cmp-upcoming-payment/cmp-upcoming-payment.component';
import { InviteSubCompanyComponent } from './invite-sub-company/invite-sub-company.component';
export const SubCompaniesPagesRoutes: Routes = [
    {
        path: '',
        canActivate:[],
        children: [
           
        { path: '', component: SubCompanyListComponent},
        { path:'payments', component: CmpUpcomingPaymentComponent },
        { path:'invite/:nodeID', component: InviteSubCompanyComponent }     
            
        ]
    }
];

export const SubCompaniesRoutingModule = RouterModule.forChild(SubCompaniesPagesRoutes);
