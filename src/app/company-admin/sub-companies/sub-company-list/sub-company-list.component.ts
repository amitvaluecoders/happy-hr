import { Component, OnInit,ViewContainerRef } from '@angular/core';
import { APPCONFIG } from '../../../config';
import { CoreService } from '../../../shared/services/core.service';
import { Router, ActivatedRoute } from '@angular/router';
import { SubCompaniesService  } from "./../sub-companies.service"
import { Cookie } from 'ng2-cookies/ng2-cookies';
import {Location} from '@angular/common';
import { ConfirmService } from '../../../shared/services/confirm.service';
@Component({
  selector: 'app-sub-company-list',
  templateUrl: './sub-company-list.component.html',
  styleUrls: ['./sub-company-list.component.scss']
})
export class SubCompanyListComponent implements OnInit {
  public loginData:any;
   public SubCompanyList = [];
    public isProcessing;
    public role:any;

  constructor(public confirmService:ConfirmService,public coreService: CoreService , public router:Router, 
    public viewContainerRef:ViewContainerRef,
    public location:Location,public subCompaniesService : SubCompaniesService) { }

  ngOnInit() {
    APPCONFIG.heading="List of sub company";
     this.getSubCompanyList();
  }

  getSubCompanyList(){
    this.isProcessing = true;
    this.subCompaniesService.listSubCompany().subscribe(
      res => {
        this.isProcessing = false;
        if (res.code == 200) {
          this.SubCompanyList = res.data.reverse();
        }
        else return this.coreService.notify("Unsuccessful", res.message, 0);
      },
      err => {
        this.isProcessing = false;
        if (err.code == 400) {
          return this.coreService.notify("Unsuccessful", err.message, 0);
        }
        this.coreService.notify("Unsuccessful", "Error while getting data.", 0)
      }
    );
    
     // list-sub-company
  }


  deleteSub(sub){
    this.confirmService.confirm(this.confirmService.tilte, this.confirmService.message, this.viewContainerRef)
    .subscribe(res => {
      let result = res;
      if (result) {
        this.confirmService.confirm(this.confirmService.tilte, "Do you really want to delete this item?", this.viewContainerRef)
        .subscribe(res => {
          let result = res;
          if (result) {
              this.deleteSubCompany(sub);
            
          }});
      }});
  }

  deleteSubCompany(sub){

    this.isProcessing = true;
    this.subCompaniesService.deleteSubCompany(sub.companyID).subscribe(
      res => {
        this.isProcessing = false;
        if (res.code == 200) {
          this.coreService.notify("Successful", res.message, 1);
          this.SubCompanyList.splice(this.SubCompanyList.indexOf(sub) , 1)
        }
        else return this.coreService.notify("Unsuccessful", res.message, 0);
      },
      err => {
        this.isProcessing = false;
        if (err.code == 400) {
          return this.coreService.notify("Unsuccessful", err.message, 0);
        }
        this.coreService.notify("Unsuccessful", "Error while getting data.", 0)
      }
    );
  }

  onIncognito(sub){
    this.subCompaniesService.incognito(sub.companyID).subscribe(
      d => {
          if(d.code=="200"){
            let url;
            console.log("dataaaaaaaa",d)
            Cookie.deleteAll();
            localStorage.clear();
            this.loginData=d.data;
            Cookie.set('happyhr-token',d.token);
            this.coreService.setRole(d.data.userRole)
            // Cookie.set('happyhr-role',d.data.userRole);
            Cookie.set('happyhr-userInfo',JSON.stringify(this.loginData));                 
           this.coreService.setPermission(d.data.modules); 
           Cookie.set('happyhr-permission',JSON.stringify(this.coreService.permission));          
            localStorage.setItem('happyhr-permission',JSON.stringify(this.coreService.permission));
            localStorage.setItem('happyhr-userInfo',JSON.stringify(this.loginData));
            if(d.data.userRole=="companyAdmin"){
              this.role="companyAdmin";
              this.coreService.setRole(this.role);
              url='/app/company-admin';
            }          
            this.coreService.setRole(this.role);
            this.location.replaceState(url);
            window.location.reload();              
        }
      },e=>{          
        this.coreService.notify("Unsuccessful",e.message,0)          
      }
    ); 
  }

}
