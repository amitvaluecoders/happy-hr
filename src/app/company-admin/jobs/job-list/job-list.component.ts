import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-job-list',
  templateUrl: './job-list.component.html',
  styleUrls: ['./job-list.component.scss']
})
export class JobListComponent implements OnInit {
  classification = [];
  classificationItems = [];
  // setting
  dropdownSettings = {};
  // setting ends
  location =[];
  locationItems=[];
  minSalary=[];
  minSalaryItems=[];
  maxSalary=[];
  maxSalaryItems=[];
  EmployeeType=[];
  EmployeeTypeItems=[];
  constructor() { }

  ngOnInit() {
  this.classificationItems =  ['Web Developer','Software Developer','HTML Developer','Sales Men','Humen Resource'];
  this.locationItems=['Delhi','Gurugram','pune','Noida','Banglore'];
  this.minSalaryItems=[12000,15000,20000,25000,30000,45000,60000];
  this.maxSalaryItems=[12000,15000,20000,25000,30000,45000,60000];
  this.EmployeeTypeItems=['Full time','Part time', 'contract'];
  
  }

}
