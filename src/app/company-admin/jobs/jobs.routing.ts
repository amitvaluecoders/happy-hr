import {  ModuleWithProviders  } from '@angular/core';
import {  RouterModule,Routes  } from '@angular/router';
import {  JobListComponent } from './job-list/job-list.component';

export const JobsPagesRoutes: Routes = [
    {path: '', component:JobListComponent}
];

export const JobsRoutingModule = RouterModule.forChild(JobsPagesRoutes);
