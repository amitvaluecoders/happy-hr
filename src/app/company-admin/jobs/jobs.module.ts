import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { JobsRoutingModule } from './jobs.routing';
import { JobsService } from './jobs.service';
import { JobListComponent } from './job-list/job-list.component';
import { AngularMultiSelectModule } from 'angular2-multiselect-checkbox-dropdown/angular2-multiselect-dropdown';
import { FormsModule} from '@angular/forms';
import { JobsGuardService } from './jobs-guard.service'; 

@NgModule({
  imports: [
    CommonModule,
    JobsRoutingModule,
    AngularMultiSelectModule,
    FormsModule 
  ],
  declarations: [JobListComponent],
  providers:[JobsService,JobsGuardService]
})
export class JobsModule { }
