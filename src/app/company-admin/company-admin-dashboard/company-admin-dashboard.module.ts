import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { CmpDashboardComponent } from './cmp-dashboard/cmp-dashboard.component';
import { dashboardRouterModule } from './company-admin-dashboard.routing';
import { CompanyActivitiesAndApprovalsComponent,KPIwithNote } from './company-activities-and-approvals/company-activities-and-approvals.component';
// import {CalendarComponent} from "ap-angular2-fullcalendar/src/calendar/calendar";
import { SharedModule } from '../../shared/shared.module';
import { MaterialModule } from '@angular/material';
import { CompanyAdminDashboardService } from "./company-admin-dashboard.service";
import { DataTableModule } from 'angular2-datatable';
// import { EmployeeListingPopupComponent } from '../company-module-listing/employee-listing-popup/employee-listing-popup.component';
import { AngularMultiSelectModule } from 'angular2-multiselect-checkbox-dropdown/angular2-multiselect-dropdown';
import { CompanyModuleListingService } from "../company-module-listing/company-module-listing.service";
import { ConfirmService } from '../../shared/services/confirm.service';
import { warningPop } from './company-activities-and-approvals/company-activities-and-approvals.component';
import { ShootOutComponent } from './shoot-out/shoot-out.component';
import { UploadDocCompleteListComponent } from './upload-doc-complete-list/upload-doc-complete-list.component';
import { MandatoryDocumentListingComponent } from './mandatory-document-listing/mandatory-document-listing.component';
// import { SlimScrollDirective } from '../../shared/slim-scroll.directive';

@NgModule({
  imports: [
    MaterialModule,
    SharedModule,
    CommonModule,
    FormsModule,
    dashboardRouterModule,
    DataTableModule,
    AngularMultiSelectModule
    
  ],
  declarations: [CmpDashboardComponent,KPIwithNote, CompanyActivitiesAndApprovalsComponent, warningPop, ShootOutComponent, UploadDocCompleteListComponent, MandatoryDocumentListingComponent],
  entryComponents: [warningPop ,KPIwithNote, ShootOutComponent],
  providers: [CompanyAdminDashboardService, CompanyModuleListingService, ConfirmService]

})
export class CompanyAdminDashboardModule { }
