import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { APPCONFIG } from '../../../config';
import { CoreService } from '../../../shared/services/core.service';
import { IDatePickerConfig } from 'ng2-date-picker';
import * as moment from "moment";
import { CompanyAdminDashboardService } from "../company-admin-dashboard.service";
import { MdDialogRef, MdDialog, MdSnackBar, HIDE_ANIMATION } from '@angular/material';
import { EmployeeListingPopupComponent } from '../../company-module-listing/employee-listing-popup/employee-listing-popup.component';
import { ConfirmService } from '../../../shared/services/confirm.service';
import { TrophyService } from "../../trophy/trophy.service";
@Component({
  selector: 'app-company-activities-and-approvals',
  templateUrl: './company-activities-and-approvals.component.html',
  styleUrls: ['./company-activities-and-approvals.component.scss']
})
export class CompanyActivitiesAndApprovalsComponent implements OnInit {

  public isProcessing = false;
  isDone=false;
  dropdownListEmployees = [];
  selectedItems = [];
  selectedUserID: any;
  public isEmployeeListProcessing = false;
  dropdownSettings = {};
  public collapse = {};
  public collapseNew = {};
  public isKpiListingProcessing = false;
  kpiList = [];
  public isPending = {};
  public isValid = {};
  public actions = {};
  public performanceIndicators=[];
  public current = [];
  public activities = [];
  isManager = false;
  userData:any;

  constructor(public companyService: CompanyAdminDashboardService, public coreService: CoreService, private router: Router,
    private route: ActivatedRoute, public dialog: MdDialog, public confirmService: ConfirmService, public viewContainerRef: ViewContainerRef) { }

  ngOnInit() {
    APPCONFIG.heading = "Activities and approvals";
    // this.route.snapshot.params['id']
    
    this.selectedUserID = this.route.snapshot.params['id'];
    this.userData = JSON.parse(localStorage.getItem("happyhr-userInfo"));    
    this.getActivities();
    this.getKPIListing();
    this.getEmployeeeList();
    this.dropdownSettings = {
      singleSelection: false,
      text: "Select employee",
      enableSearchFilter: true,
      classes: "myclass assmng"
    };

    this.isManager = this.coreService.isManager();
  }

  isValidToApproveOrReject(list){
    if(list.module=='newEmployeeApproval'){
      if(this.userData.subRole){
        console.log("called")
        if(this.userData.subRole!='genericCeo'){return false;}
        else{
          return true;
        } 
      }
    }
    else{
        return true;  
    }
  }
    onViewKPI(k) {
    let dialogRef = this.dialog.open(KPIwithNote);
    dialogRef.componentInstance.kpi = k;
    dialogRef.afterClosed().subscribe(result => {
      if (result) {

      }
    });
  }

  getKPIListing(){
    this.isKpiListingProcessing = true;
    let isID = (this.selectedItems && this.selectedItems.length>0 ) ? true : false;
    this.companyService.getActivitiesKPI({ isID: isID, ids: this.selectedItems }).subscribe(
      res => {
        if (res.code == 200){
           for(let item of res.data){
              if(item.module == "performanceIndicator"){
           this.performanceIndicators = item.data||[];
                 this.isPending['performancIndicator'] =item.piPending;
              } 
              
            }
            res.data.forEach(item=>{  
            
                  item.data.forEach(item2=>{
                    item2.kpis['valid']= false;                                           
                        item2.kpis.Current.find(item3=>{
                          if(item3.lapseDuration==true){
                            item2.kpis['valid']= true;
                            this.isValid['performancIndicator'] = item2.kpis.valid; 
                            } 
                      })
                      
                    })
            })

            
             this.performanceIndicators = this.performanceIndicators.filter(item=>{
              item['isCollapsed'] = true;
              return item;
            })
             
             this.chengeOrderPI();     
        }
        else return this.coreService.notify("Unsuccessful", res.message, 0);
        // this.isKpiListingProcessing = false;
      },
      err => {
        this.isKpiListingProcessing = false;
      }
    );
  }


  chengeOrderPI(){

      var pindicator = $('#box2');
      console.log(pindicator);
      var prevelement = $('#box1 .col-md-12:nth-child(18)');
      $(pindicator).insertAfter(prevelement); 
      this.isKpiListingProcessing = false;
  }

  sendReminder(user,k){

   k.isActionProcessing = true;
    let reqBody = {
      "companyPositionPerformanceIndicatorID":k.companyPositionPerformanceIndicatorID,
      "employeeUserID":user.employee.userID  
    }
    this.companyService.sendReminderforKpi(reqBody).subscribe(
      res => {
        if (res.code == 200) this.coreService.notify("Successful", res.message, 1);
        else return this.coreService.notify("Unsuccessful", res.message, 0);
        k.isActionProcessing = false;
      },
      err => {
        k.isActionProcessing = false;
      }
    );
  }

  // method to get all employee listing for manger and third pary listing 
  getEmployeeeList() {
    this.isEmployeeListProcessing = true;
    this.companyService.getAllUsersUnderLoggedinUser().subscribe(
      d => {
        if (d.code == "200") {
          this.dropdownListEmployees = d.data.filter(item => {
            item['id'] = item.userId;
            item['itemName'] = item.name;
            item['isSelected'] = false;
            return item;
          })
          this.isEmployeeListProcessing = false;

        }
        else this.coreService.notify("Unsuccessful", d.message, 0);
      },
      error => this.coreService.notify("Unsuccessful", error.message, 0),
      () => { }
    )
  }

  onItemSelect(e, selected) {
    this.getActivities();
    this.getKPIListing();    
  }

  getActivities() {
    this.isProcessing = true;
    let isID = (this.selectedItems && this.selectedItems.length>0 ) ? true : false;
    this.companyService.getActivities({ isID: isID, ids: this.selectedItems }).subscribe(
      res => {
        this.isProcessing = false;
        if (res.code == 200) {
          this.activities = res.data;
            // for(let item of res.data){
            //   if(item.module == "performanceIndicator")  this.performanceIndicators = item.data;
            // }
            //  this.performanceIndicators = this.performanceIndicators.filter(item=>{
            //   item['isCollapsed'] = true;
            //   return item;
            // })
           
          this.setHeaderColor();
          this.setAction();
        }
        else return this.coreService.notify("Unsuccessful", res.message, 0);
      },
      err => {
        this.isProcessing = false;
        this.coreService.notify("Unsuccessful", err.message, 0);
      }
    )
  }

  formatDateTime(date) {
    return moment(date).isValid() ? moment(date).format('DD MMM YYYY hh:mm A') : 'Not specified';
  }

  formatDate(date) {
    return moment(date).isValid() ? moment(date).format('DD MMM YYYY') : 'Not specified';
  }

  isValidateColumn(arr, columnName) {
    for (let i = 0; i < arr.length; i++) {
      if (arr[i].hasOwnProperty(columnName)) return true;
    }
    return false;
  }

  isCreateButton(list) {
    let adminCreateButton = ['360Survey', 'bpip', 'contract', 'developmentPlan', 'disciplinaryAction', 'performanceAppraisal', 'pip', 'policyDocument', 'trophy','ohs','warning'];
    let managerCreateButton = ['360Survey', 'bpip', 'developmentPlan', 'disciplinaryAction', 'performanceAppraisal', 'pip', 'trophy','ohs'];

    return this.isManager ? managerCreateButton.indexOf(list.module) >= 0 : adminCreateButton.indexOf(list.module) >= 0;
  }

  setHeaderColor() {
   
    for (let list of this.activities) {
       this.isPending[list.module] = false;
      for (let data of list.data) {
        // if (list.module == 'trainingPlan') {
        //   if (data.status && data.status == 'Awaiting Trainee Acceptance') return this.isPending[list.module] = true;
        // }
        // else if (list.module == 'grievance' || list.module == "360Survey" || list.module == 'ohs' || list.module == 'trophy') {
        //   if (data.status && data.status == 'Pending') return this.isPending[list.module] = true;
        // }
        if (data.status && (data.status == 'Awaiting Employee Acceptance' || data.status == 'Pending' || data.status == 'Awaiting Trainee Acceptance')) this.isPending[list.module] = true;
        if(list.modelu =='policyDocument' && data.status == 'Pending') this.isPending[list.module] = true;
        if (list.module == 'employeeSignedDocument' && (moment(data.expiryDate).diff(moment(), 'days') < 30)) this.isPending[list.module] = true;
        if (list.module == 'mandatoryDocuments' && (moment(data.expiryDate).diff(moment(), 'days') < 30)) this.isPending[list.module] = true;
        if ( (list.module == 'onboardchecklist' ||  list.module == 'offboardchecklist' ) && data.status && (data.status == 'Incomplete')) this.isPending[list.module] = true;
        if ( (list.module == 'ohs') && data.status && (data.status == 'Under Investigation')) this.isPending[list.module] = true;

      }
    }
  }

  showActionButton(list, data) {
    // console.log(list.module, ' ===> ', $(list.module).children());
    return true;
  }

  // onCancel(list,data){
  //   this.companyService.cancelProbationaryMeating({probationaryMeetingID:data.probationaryPeriodMeetingID})
  //   .subscribe(
  //     res => {
  //       if (res.code == 200) {
  //         this.coreService.notify("Success", res.message, 1);
  //        data['status']='Cancelled'; 
  //       }
  //       else return this.coreService.notify("Unsuccessful", res.message, 0);
  //     },
  //     err => {
  //       if (err.code == 400) {
  //         return this.coreService.notify("Unsuccessful", err.message, 0);
  //       }
  //       this.coreService.notify("Unsuccessful", "Problem while performing action.", 0)
  //     }
  //   );
  // }

  setAction() {
    for (let list of this.activities) {
      let moduleName = list.module;
      switch (moduleName) {
        case 'trophy':
          this.actions[moduleName] = { edit: true, delete: true, createNomination: true, result: true }; break;
        case "trophy-certificate":
          this.actions[moduleName] = { viewDetail: true }; break;
        case 'leaveApproval':
          this.actions[moduleName] = { approve: true, reject: true, viewDetail: true,delete: true }; break;
        case '360Survey':
          this.actions[moduleName] = { edit: true, delete: true, viewEmployeeResponse: true, viewSurveyResponse: true }; break;
        case 'trainingPlan':
          this.actions[moduleName] = { viewDetail: true, edit: true }; break;
        case 'contract':
          this.actions[moduleName] = { acceptOrReject: true , viewDetail: true }; break;
        case 'policyDocument':
          this.actions[moduleName] = { acceptOrReject: true , viewDetail: true }; break;
        case 'pip':
          this.actions[moduleName] = { performAction: true, delete: true }; break;
        case 'developmentPlan':
          this.actions[moduleName] = { performAction: true, delete: true }; break;
        case 'bpip':
          this.actions[moduleName] = { performAction: true, delete: true }; break;
        case 'warning':
          this.actions[moduleName] = { viewDetail: true }; break;
        case 'grievance':
          this.actions[moduleName] = { investigate: true, result: true }; break;
        case 'disciplinaryAction':
          this.actions[moduleName] = { viewDetail: true, reSchedule: true, delete: true }; break;
        case 'performanceAppraisal':
          this.actions[moduleName] = { edit: true, viewEmployeeResponse: true, directAsessment: true, asessment: true, viewDetail: true }; break;
        case 'induction':
          // this.actions[moduleName] = { archive: true, delete: true }; 
          break;
        case 'offbording':
          // this.actions[moduleName] = { edit: true, delete: true }; 
          break;
        case 'offboardchecklist':
          this.actions[moduleName] = { performAction : true, viewDetail: true, delete :true  }; break;
        case 'onboardchecklist':
          this.actions[moduleName] = { performAction : true, viewDetail: true, delete :true , cancel: true }; break;
        case 'performanceIndicator':
          this.actions[moduleName] = { createPerformanceAppraisal: true, directAsessment: true }; break;
        case 'newEmployeeApproval':
          this.actions[moduleName] = { approve: true, reject: true }; break;
        case 'assetsApproval':
          this.actions[moduleName] = { approve: true, reject: true, edit: true, delete: true }; break;
        case 'exitSurvey':
          this.actions[moduleName] = { viewResponse: true , delete :true }; break;
        case 'ohs':
          this.actions[moduleName] = {investigate: true, viewDetail: true, downloadReport: true}; break;
        case 'probationaryMeeting':
          this.actions[moduleName] = { cancel: true, delete: true }; 
          break;
        case 'employeeSignedDocument':
          this.actions[moduleName] = { documentUrl: true, delete :true , archive :true}; break;
        case 'mandatoryDocuments':
          this.actions[moduleName] = { documentUrl: true, delete :true , archive :true}; break;
        case 'pending contract' :
          this.actions[moduleName] = {viewDetail : true}
      }
    }
  }

  onDownloadReport(list, data) {
    this.companyService.downloadPdf(list, data);
  }

  onAcceptOrReject(list, data) {
    switch (list.module) {
      case 'policyDocument': return this.router.navigate(['/app/company-admin/policy-documents/accept/reject/change/', data.policyDocumentID]);
      case 'contract': return this.router.navigate(['/app/company-admin/contract/change-cotract', data.contractID]);
    }
  }

  openEmployeeListPopup(moduleName){
    let dialogRef = this.dialog.open(EmployeeListingPopupComponent);
    dialogRef.componentInstance.moduleName = moduleName;
    dialogRef.afterClosed().subscribe(result => {
      if (result) {

      }
    });
  }

  onCreate(list) {
    let moduleName = list.module;
    let createByPopup = ['developmentPlan', 'disciplinaryAction', 'pip', 'bpip', 'performanceAppraisal','ohs','warning'];
    if (createByPopup.indexOf(moduleName) >= 0) {
      this.openEmployeeListPopup(moduleName);
    } else {
      switch (moduleName) {
        case 'trophy':
          return this.router.navigate(['/app/company-admin/trophy/create-trophy']);
        case 'contract':
          return this.router.navigate(['app/company-admin/contract/create-new']);
        case '360Survey':
          return this.router.navigate(['/app/company-admin/survey/create-survey']);
        case 'policyDocument':
          return this.router.navigate(['/app/company-admin/policy-documents/create']);
      }
    }
  }

  onViewAll(list) {
    let moduleName = list.module;
    switch (moduleName) {
      case 'trophy':
        return this.router.navigate(['/app/company-admin/trophy']);
      case 'leaveApproval':
        return this.router.navigate(['/app/company-admin/leaves']);
      case '360Survey':
        return this.router.navigate(['/app/company-admin/survey']);
      case 'trainingPlan':
        return this.router.navigate(['/app/company-admin/training-day']);
      case 'contract':
        return this.router.navigate(['/app/company-admin/contract']);
      case 'policyDocument':
        return this.router.navigate(['/app/company-admin/policy-documents']);
      case 'pip':
        return this.router.navigate(['/app/company-admin/listing-page']);
      case 'developmentPlan':
        return this.router.navigate(['/app/company-admin/listing-page/development-plan-list']);
      case 'bpip':
        return this.router.navigate(['/app/company-admin/listing-page/behavior-performance-improvement-listing']);
      case 'warning':
        return this.router.navigate(['/app/company-admin/listing-page/warning-list']);
      case 'grievance':
        return this.router.navigate(['/app/company-admin/listing-page/grievance-listing']);
      case 'disciplinaryAction':
        return this.router.navigate(['/app/company-admin/listing-page/disciplinary-action-listing']);
      case 'performanceAppraisal':
        return this.router.navigate(['/app/company-admin/listing-page/performance-appraisal-listing']);
      case 'induction':
        return this.router.navigate(['/app/company-admin/listing-page/induction-onboarding-listing']);
      case 'offbording':
        return this.router.navigate(['/app/company-admin/listing-page/off-boarding-listing']);
      case 'performance':
        return this.router.navigate(['/app/company-admin/listing-page/performance-indicator-listing']);
      case 'newEmployeeApproval':
        return this.router.navigate(['/app/company-admin/listing-page/new-emloyee-approval']);
      case 'assetsApproval':
        return this.router.navigate(['/app/company-admin/listing-page/assets-approval']);
      case 'exitSurvey':
        return this.router.navigate(['/app/company-admin/listing-page/exit-survey']);
      case 'ohs':
        return this.router.navigate(['/app/company-admin/listing-page/ohs-listing']);
      case 'probationaryMeeting':
        return this.router.navigate(['/app/company-admin/listing-page/probationary-meeting']);
      case 'onboardchecklist':
        return this.router.navigate(['/app/company-admin/listing-page/induction-onboarding-listing']);
      case 'employeeSignedDocument':
        return this.router.navigate(['/app/company-admin/docs-uploaded']);
      case 'mandatoryDocuments':
        return this.router.navigate(['/app/company-admin/docs-mandatory']);
      case "trophy-certificate":
        return this.router.navigate(['/app/company-admin/listing-page/trophy-certificates']);
        
      case 'offboardchecklist':
        return this.router.navigate(['/app/company-manager/listing-page/off-boarding-listing']) ;
      case 'pending contract':
        return this.router.navigate(['/app/company-manager/listing-page/contract-update'])

    }
  }

  isValidEdit(list, data) {
    switch (list.module) {
      case '360Survey': return data.status == 'Unused';
      case 'performanceAppraisal': return data.status == 'Awaiting Employee Acceptance';
      case 'trainingPlan': return data.status !== 'Completed';

      default: return true;
    }
  }

  onEdit(list, data) {
    let moduleName = list.module;
    switch (moduleName) {
      case 'trophy':
        return this.router.navigate(['/app/company-admin/trophy/edit-trophy', data.trophyID]);
      case '360Survey':
        return this.router.navigate(['/app/company-admin/survey/edit', data.surveyID]);
      case 'trainingPlan':
        return this.router.navigate(['/app/company-admin/training-day/master-training-plan/edit', data.trainingPlanID]);
      case 'performanceAppraisal':
        return this.router.navigate(['/app/company-admin/performance-appraisal/edit', data.performanceAppraisalID]);
      case 'assetsApproval':
        return this.router.navigate([`/app/company-admin/listing-page/assets-approve/edit/${data.assetApprovalID}`])
      case 'trainingPlan':
        return this.router.navigate([`/app/company-admin/training-day/training-plan/edit/${data.trainingPlanID}`])
    }
  }

  onDelete(list, data, index) {

    let message = "Do you want to delete this " + list.title + " ?";
    if(list.module ==  'leaveApproval') message =`Do you want to delete this item?<br>
     You need to delete the same in integrated APIs(XERO,Myob,keypay,deputy,tanda) if connected.`;
    this.confirmService.confirm("Delete confirmation", message, this.viewContainerRef).subscribe(
      confirm => {
        if (confirm) {
          data['isProcessing'] = true;
          this.companyService.deleteData(list, data).subscribe(
            res => {
              data['isProcessing'] = false;
              if (res.code == 200) {
                list.data.splice(index, 1);
                this.coreService.notify("Successful", res.message, 1);
              }
              else return this.coreService.notify("Unsuccessful", res.message, 0);
            },
            error => {
              data['isProcessing'] = false;
              this.coreService.notify("Unsuccessful", error.message, 0);
            }
          )
        }
      }
    )
  }

  onCancel(list, data, index) {

          data['isProcessing'] = true;
          this.companyService.cancelData(list, data).subscribe(
            res => {
              data['isProcessing'] = false;
              if (res.code == 200) {
                // list.data.splice(index, 1);
                data['status']='Cancelled'; 
                this.coreService.notify("Successful", res.message, 1);
              }
              else return this.coreService.notify("Unsuccessful", res.message, 0);
            },
            error => {
              data['isProcessing'] = false;
              this.coreService.notify("Unsuccessful", error.message, 0);
            }
          )
      
  }

  onArchive(list, data, index) {
          data['isProcessing'] = true;
          data['type']='archive';
          this.companyService.archiveData(list, data).subscribe(
            res => {
              data['isProcessing'] = false;
              if (res.code == 200) {
                list.data.splice(index, 1);
                this.coreService.notify("Successful", res.message, 1);
              }
              else return this.coreService.notify("Unsuccessful", res.message, 0);
            },
            error => {
              data['isProcessing'] = false;
              this.coreService.notify("Unsuccessful", error.message, 0);
            }
          )
  }

  isValidResult(list, data) {
    switch (list.module) {
      case 'trophy': return data.nominatedStatus == 'Yes';
      case 'grievance': return data.status == 'Resolved' || data.status == 'Unresolved' || data.status == 'Terminated';
      case 'performanceAppraisal': return data.status == 'Complete';

      default: return true;
    }
  }

  onResult(list, data) {
    let moduleName = list.module;
    switch (moduleName) {
      case 'trophy': return this.router.navigate(['/app/company-admin/trophy/trophy-progress', data.trophyID]);
      case 'grievance': return this.router.navigate(['/app/company-admin/grievance/result', data.grievanceID]);
      case 'performanceAppraisal':
        if (data.directAssessment == 'Yes') {
          return this.router.navigate(['/app/company-admin/performance-appraisal/direct-assessment-result', data.performaceAppraisalID]);
        } else {
          return this.router.navigate(['/app/company-admin/performance-appraisal/result', data.performaceAppraisalID]);
        }
    }
  }

  isValidViewResponse(list, data) {
    switch (list.module) {
      case '360Survey': return data.status !== 'Unused';
      case 'exitSurvey': return data.status == 'Done';
      case 'performanceAppraisal': return data.status == 'Awaiting Manager Response';
      case 'warning': return data.title != null;

      default: return true;
    }
  }

  onViewEmployeeResponse(list, data) {
    let moduleName = list.module;
    switch (moduleName) {
      case '360Survey': return this.router.navigate(['/app/company-admin/survey/detail', data.surveyID]);
      case 'exitSurvey': return this.router.navigate(['/app/company-admin/exit-survey/view-response', data.exitSurveyID]);
      case 'performanceAppraisal': return this.router.navigate(['/app/company-admin/performance-appraisal/assessment-by-employee', data.performaceAppraisalID]);
    }
  }

  isValidViewDetail(list, data) {
    // (data.status == 'Resolved' || data.status == 'Unresolved' || data.status == 'Complete'|| data.status == 'Approved')
    switch (list.module) {
      case 'ohs': return data.status !== 'Pending';
      case 'performanceAppraisal': return data.status == 'Complete' || data.status == 'Terminated';
      case 'trainingPlan': return data.status !== 'Pending' && data.trainingTime && moment().isAfter(moment(data.trainingTime));
      case 'disciplinaryAction': return data.status!=='Terminated';
      default: return true;
    }
  }

  onViewDetail(list, data) {
    let moduleName = list.module;
    switch (moduleName) {
      case 'leaveApproval': return this.onPopUp1('leaveDetail', list, data);
      case 'ohs': return this.router.navigate(['/app/company-admin/ohs/result', data.ohsID]);
      case 'contract': 
          if(data.status=='Pending'){
            return this.router.navigate([`/app/company-admin/contract/contract-details/own/${data.contractID}`]);  
          }
          else{
            return this.router.navigate([`/app/company-admin/contract/contract-details/own/${data.companyContractID}`]);
          }
      case 'policyDocument':{ 
        let url="/app/company-admin/policy-documents/detail/"+data.policyDocumentID;
          if(data.companyPolicyDocumentID) url="/app/company-admin/policy-documents/detail/own/"+data.companyPolicyDocumentID;
          return this.router.navigate([url]);
        //return this.router.navigate([`/app/company-admin/policy-documents/detail/${data.policyDocumentID}`]);
      }
      case 'disciplinaryAction': return this.router.navigate([`/app/company-admin/disciplinary-meeting/view/${data.disciplinaryActionID}`]);
      case 'performanceAppraisal':{
        if(data.isDirectAssessment != "Yes") return this.router.navigate([`/app/company-admin/performance-appraisal/result/${data.performanceAppraisalID}`]);
        return this.router.navigate([`/app/company-admin/performance-appraisal/direct-assessment-result/${data.performanceAppraisalID}`]);
      } 
      case 'warning': return this.onPopUp1('warning', list, data);
      case 'offboardchecklist': return this.router.navigate(['/app/company-admin/checklist/view/off-boarding', data.employeeChecklistID]);
      case 'onboardchecklist': return this.router.navigate(['/app/company-admin/checklist/view/on-boarding', data.employeeChecklistID]);
      case 'trainingPlan': return this.router.navigate(['/app/company-admin/training-day/training-plan/detail', data.trainingPlanID]);
      case 'trophy-certificate': return this.onPopUp1('trophy', list, data);
      case 'pending contract' : return this.router.navigate([`/app/company-admin/listing-page/contract-detail/${data.companyContractID}/${data.employeeCompanyContractID}`]);
    }
  }

  onPerformAction(list, data) {
    let moduleName = list.module;
    switch (moduleName) {
      case 'pip': return this.router.navigate([`/app/company-admin/performance-improvement-plan/training/${data.pipID}`]);
      case 'bpip': return this.router.navigate([`/app/company-admin/behavioral-performance-improvement-plan/training/${data.bpipID}`]);
      case 'developmentPlan': return this.router.navigate([`/app/company-admin/development-plan/reassessment-assessment/${data.developmentPlanID}`]);
      case 'onboardchecklist': return this.router.navigate([`/app/company-admin/checklist/edit/on-boarding/${data.employeeChecklistID}`])
      case 'offboardchecklist': return this.router.navigate([`/app/company-admin/checklist/edit/off-boarding/${data.employeeChecklistID}`])  
  }
  }

  isValidInvestigate(list, data) {
    switch (list.module) {
      case 'grievance': return !(data.status == 'Resolved' || data.status == 'Unresolved');
      case 'ohs': return !(data.status == 'Resolved' || data.status == 'Unresolved');

      default: return true;
    }
  }

  onInvestigate(list, data) {
    let moduleName = list.module;
    switch (moduleName) {
      case 'grievance':
        if (!data.stage) return this.router.navigate(['/app/company-admin/grievance/meeting-complainer', data.grievanceID]);
        switch (data.stage) {
          case '': return this.router.navigate(['/app/company-admin/grievance/meeting-complainer', data.grievanceID]);
          case '1': return this.router.navigate(['/app/company-admin/grievance/meeting-grievance-against', data.grievanceID]);
          case '2': return this.router.navigate(['/app/company-admin/grievance/meeting-both-parties', data.grievanceID]);
          case '3': return this.router.navigate(['/app/company-admin/grievance/result', data.grievanceID]);
        }
      case 'ohs':
        if (data.status == 'Under Investigation') {
          if (!data.injuryDetail) {
            return this.router.navigate(['/app/company-admin/ohs/company-admin-notified', data.ohsID]);
          } else if (!data.correctiveAction || !data.correctiveAction.length) {
            return this.router.navigate(['/app/company-admin/ohs/corrective-actions', data.ohsID]);
          } else {
            return this.router.navigate(['/app/company-admin/ohs/repair-assessment', data.ohsID]);
          }
        } else if (data.status == 'Pending') {
          return this.router.navigate(['/app/company-admin/ohs/incident-assessment', data.ohsID]);
        } else {
          return this.router.navigate(['/app/company-admin/ohs/result', data.ohsID]);
        }
    }
  }

  // createOhs(list, data){
  //   this.router.navigate(['/app/company-admin/listing-page/create-oh&s', data.userID]);
  // }


  isDirectAsessment(list, data) {
    switch (list.module) {
      case 'performanceAppraisal': return data.status != 'Complete';

      default: return true;
    }
  }

  onDirectAsessment(list, data) {
    switch (list.module) {
      case 'performanceAppraisal': return this.router.navigate(['/app/company-admin/performance-appraisal/direct-assessment', data.performanceAppraisalID]);
      case 'performanceIndicator': return this.router.navigate(['/app/company-admin/performance-indicator/direct-assessment', data.employeeUser.userID]);
    }
  }

  newEmployeeStatusChange(data, status) {
    data['isProcessing'] = true;
    let reqBody = { "employeeInvitedID": data.employeeInvitedID, "status": status };
    this.companyService.changeNewEmployeeStatus(reqBody).subscribe(
      res => {
        data['isProcessing'] = false;
        if (res.code == 200) {
          this.coreService.notify("Successful", res.message, 1);
          data.status = status;
        }
        else return this.coreService.notify("Unsuccessful", res.message, 0);
      },
      err => {
        data['isProcessing'] = false;
        this.coreService.notify("Unsuccessful", err.message, 0);
      }
    )
  }

  onApprove(list, data) {
    switch (list.module) {
      case 'leaveApproval':
        data['isApprove'] = true;
        return this.onPopUp1('leaveDetail', list, data);

      case 'newEmployeeApproval': return this.newEmployeeStatusChange(data, 'Approved');

      default: return this.onPopUp1('Approved', list, data);
    }
  }

  onReject(list, data) {
    switch (list.module) {
      case 'newEmployeeApproval': return this.newEmployeeStatusChange(data, 'Rejected');

      default: return this.onPopUp1('Rejected', list, data);
    }
  }

  onPopUp1(disp_type, list, data) {
    
    let dialogRef = this.dialog.open(warningPop);
    dialogRef.componentInstance.list = list;
    dialogRef.componentInstance.data = data;
    dialogRef.componentInstance.displayType = disp_type;
    dialogRef.afterClosed().subscribe(res => {
      console.log(res);
      if(disp_type=='leaveDetail') disp_type='Approved';
      if (disp_type == 'Approved' || disp_type == 'Rejected') {
        if (res.title == 'Successful') 
              data.status = disp_type;
        this.coreService.notify(res.title, res.message, res.type);
      }
    });
  }
}


@Component({
  selector: 'warning-pop',
  template: `
          <div *ngIf="displayType=='trophy'">
            <div class="TrophyCertOuter" [style.background-image]="'url(' + data?.trophyIcon + ')'" >
              <div class="TrophyCertInner">
                <div class="TrophyCertImage" [style.background-image]="'url(' + data?.trophyIcon + ')'">
                  <i class="fa fa-trophy"></i>
                </div>
                <div class="TrophyCertInfo">
                  <h4>Certificate of Achievement</h4>
                  <h5>Trophy Award certificate</h5>
                  <p>Award category</p>
                  <h4>{{data?.title}}</h4>
                  <p>This certificate is presented to</p>
                  <h2>{{data?.employeeUser?.name}}</h2>
                  <p>For excellent performance in {{data?.title}} trophy competition.</p>
                  <p class="dateRow">Date: {{formatDate(data.insertTime)}}</p>
                </div>
              </div>
            </div> 
          </div>
          <div *ngIf="displayType=='warning'">
              <div [innerHTML]="data?.reason"></div>
            </div>
            <div *ngIf="displayType=='leaveDetail'">
              <h4 class="heading">Leave detail
                  <div class="status color-danger pull-right">{{data?.status}}</div>
              </h4>
              <div class="row admin-panel">
                <div class="col-sm-12 form-group">
                    <div class="lftinpt10">
                        <label style="margin:0px 0px;" class="form-inline">Title:</label>
                    </div>
                    <div class="rghtinput10">
                        {{data?.title || 'Not specified'}}
                    </div>
                </div>
                <div class="col-sm-12 form-group">
                    <div class="lftinpt10">
                        <label style="margin:0px 0px;" class="form-inline">Selected leave type:</label>
                    </div>
                    <div class="rghtinput10">
                        {{data?.leaveType || 'Not specified'}}
                    </div>
                </div>
                <div class="col-sm-12 form-group">
                    <div class="lftinpt10">
                        <label style="margin:0px 0px;" class="form-inline">Start date:</label>
                    </div>
                    <div class="rghtinput10">
                        {{formatDate(data?.startDate)}}
                    </div>
                </div>
                <div class="col-sm-12 form-group">
                    <div class="lftinpt10">
                        <label style="margin:0px 0px;" style="margin:0px 0px;" class="form-inline">End date:</label>
                    </div>
                    <div class="rghtinput10">
                        {{formatDate(data?.endDate)}}
                    </div>
                </div>
                <div class="col-sm-12 form-group">
                    <div class="lftinpt10">
                        <label style="margin:0px 0px;" class="form-inline">Comments:</label>
                    </div>
                    <div class="rghtinput10">
                        {{data?.comment || 'Not specified'}}
                    </div>
                </div>
                <div class="col-sm-12 text-center">
                  <md-progress-spinner *ngIf="isProcessing" mode="indeterminate" color=""></md-progress-spinner>
                  <button *ngIf="!isProcessing && data?.status=='Pending'" class="btn btn-primary btn-lg" type="submit" (click)="onSubmit(displayType)">Approve</button>
                </div>
              </div>
            </div>
            <div *ngIf="(displayType=='Approved')||(displayType=='Rejected')">
              <h5>Write response note</h5>
              <textarea style="min-width:300px;margin-bottom:20px;" [(ngModel)]="note" placeholder="Response note" class="form-control"></textarea>
              <md-progress-spinner *ngIf="isProcessing" mode="indeterminate" color=""></md-progress-spinner>
              <button *ngIf="!isProcessing" class="btn btn-primary btn-lg text-center" type="submit" (click)="onSubmit(displayType)">Submit</button>
            </div>
            `,
  styles: [
    `
    .TrophyCertOuter {max-width: 700px;min-width: 700px;background-size: cover;background-repeat: repeat;padding:15px;float:left;}
    .TrophyCertInner{width:100%;float:left;display:table;}
    .TrophyCertImage, .TrophyCertInfo{display: table-cell;vertical-align: middle;width:60%;text-align:center;}
    .TrophyCertImage{background-size: cover;background-repeat: repeat;width: 40%;box-shadow: inset 0px 0px 60px #000000;-webkit-box-shadow: inset 0px 0px 60px #000000;-moz-box-shadow: inset 0px 0px 60px #000000;-o-box-shadow: inset 0px 0px 60px #000000;-ms-box-shadow: inset 0px 0px 60px #000000;}
    .TrophyCertImage i.fa-trophy{font-size:200px;color:#ffffff;opacity: 0.8;}
    .TrophyCertInfo{background:#ffffff;padding: 20px;}
    .TrophyCertInfo h2{text-transform: uppercase;text-decoration: underline;font-size: 30px;font-weight: 600;}     
    .TrophyCertInfo h4{margin: 0 0 15px 0;border: none;padding: 0;font-weight: 600;}
    .TrophyCertInfo h5{margin: 0 0 15px 0;font-weight: 600;font-size: 18px;padding: 0;border: none;}
    .TrophyCertInfo p{ margin: 5px 0;line-height: 20px;}
    .TrophyCertInfo p.dateRow{margin: 20px 0;}
              `
  ]
})
export class warningPop {
  public data: any;
  public list: any;
  public displayType: string;
  note = '';
  isProcessing = false;

  constructor(public companyService: CompanyAdminDashboardService, public dialog: MdDialogRef<warningPop>) { }

  formatDate(date) {
    return moment(date).isValid() ? moment(date).format('DD MMM YYYY') : 'Not specified';
  }

  onSubmit(actionType) {
    let reqBody;
    if (this.list.module == "assetsApproval") {
      reqBody = { "assetApprovalID": this.data.assetApprovalID, "responseNote": this.note, "status": actionType };
    } else if (this.list.module == "leaveApproval") {
        if(actionType=='leaveDetail'){
            
            reqBody = { leavesID: this.data.leaveID, note: this.note, status: 'Approved' };        
        }
        else{
          reqBody = { leavesID: this.data.leaveID, note: this.note, status: actionType };          
        }
    }
    this.isProcessing = true;
    this.companyService.changeStatus(reqBody, this.list.module).subscribe(
      res => {
        this.isProcessing = true;
        if (res.code == 200) {
          this.dialog.close({ title: 'Successful', message: res.message, type: 1 });
        }
        else this.dialog.close({ title: 'Unsuccessful', message: res.message, type: 0 });
      },
      error => {
        this.isProcessing = true;
        this.dialog.close({ title: 'Unsuccessful', message: error.message, type: 0 });
      }
    )
  }
}

@Component({
  selector: 'app-kpi-with-note',
  templateUrl: './Kpi-details.html',
  styles: [``]
})
export class KPIwithNote implements OnInit {
  public kpi:any;
  constructor() { }
  ngOnInit() {}
}
