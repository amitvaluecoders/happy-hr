import { Routes, RouterModule } from '@angular/router';
import { CmpDashboardComponent } from './cmp-dashboard/cmp-dashboard.component';
import { CompanyActivitiesAndApprovalsComponent } from './company-activities-and-approvals/company-activities-and-approvals.component';
import { UploadDocCompleteListComponent } from './upload-doc-complete-list/upload-doc-complete-list.component';
import { MandatoryDocumentListingComponent } from './mandatory-document-listing/mandatory-document-listing.component';

const dashboardRoutes:Routes=[
    {path:'', component:CmpDashboardComponent},
    {path:'activities', component:CompanyActivitiesAndApprovalsComponent},
    {path:'activities/:id', component:CompanyActivitiesAndApprovalsComponent},
    {path:'docs-uploaded', component:UploadDocCompleteListComponent},
    {path:'docs-mandatory', component:MandatoryDocumentListingComponent}
];

export const dashboardRouterModule=RouterModule.forChild(dashboardRoutes);