import { Component, OnInit } from '@angular/core';
import { MdSnackBar, MdDialog, MdDialogRef } from '@angular/material';
import { CoreService } from '../../../shared/services/core.service';
import { CompanyAdminDashboardService } from "../company-admin-dashboard.service";

@Component({
  selector: 'app-shoot-out',
  templateUrl: './shoot-out.component.html',
  styleUrls: ['./shoot-out.component.scss']
})
export class ShootOutComponent implements OnInit {

  public shootout ={
        "title":"",
        "reason":"",
        "userID": ""
  };
  public dropdownSettings = {};
  public isProcessing : boolean;
  public dropdownListEmployees = [];
  public selectedItems = [];
  public isButtonClicked : boolean;

  constructor( public dialogRef: MdDialogRef<ShootOutComponent> , 
   public coreService: CoreService,
  public companyAdminDashboardService : CompanyAdminDashboardService) { }

  ngOnInit() {
     this.getEmployeeeList();
    this.dropdownListEmployees = [];
    this.dropdownSettings = {
      singleSelection: true,
      text: "Select employee",
      enableSearchFilter: true,
      classes: "myclass assmng"
    };
  }



  

  getEmployeeeList() {
    this.isProcessing = true;
    
    this.companyAdminDashboardService.getAllEmployee().subscribe(
      d => {
        this.isProcessing = false;
        if (d.code == "200") {
          // this.dropdownListEmployees = d.data;
          this.dropdownListEmployees = d.data.filter(item => {
            item['id'] = item.userId;
            item['itemName'] = item.name;
            return item;
          })
        }
        else this.coreService.notify("Unsuccessful", d.message, 0);
      },
      error => this.coreService.notify("Unsuccessful", "Error while getting data", 0),
      () => { }
    )
  }



  submitShootout(){
      this.isProcessing = true;
      this.shootout.userID = this.selectedItems[0].id;
    this.companyAdminDashboardService.setShootout(this.shootout).subscribe(
      d => {
        this.isProcessing = false;
        this.isButtonClicked = false;
        if (d.code == "200") {
           this.dialogRef.close();
         this.coreService.notify("Successful", d.message, 1);
        }
        else this.coreService.notify("Unsuccessful", d.message, 0);
      },
      error => {
         this.isProcessing = false;
        this.isButtonClicked = false;
        this.coreService.notify("Unsuccessful", "Error while getting data", 0)
      },
      () => { }
    )
  }

}
