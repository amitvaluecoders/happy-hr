import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UploadDocCompleteListComponent } from './upload-doc-complete-list.component';

describe('UploadDocCompleteListComponent', () => {
  let component: UploadDocCompleteListComponent;
  let fixture: ComponentFixture<UploadDocCompleteListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UploadDocCompleteListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UploadDocCompleteListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
