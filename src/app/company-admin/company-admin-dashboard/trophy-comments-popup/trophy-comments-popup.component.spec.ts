import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TrophyCommentsPopupComponent } from './trophy-comments-popup.component';

describe('TrophyCommentsPopupComponent', () => {
  let component: TrophyCommentsPopupComponent;
  let fixture: ComponentFixture<TrophyCommentsPopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TrophyCommentsPopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TrophyCommentsPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
