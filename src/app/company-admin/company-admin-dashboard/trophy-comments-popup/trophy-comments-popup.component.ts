import { Component, OnInit, ElementRef,ViewContainerRef, ViewChild, Renderer2 } from '@angular/core';
import { RestfulLaravelService } from "../../../shared/services/restful-laravel.service";
import { CoreService } from "../../../shared/services/core.service";
import { icsFormatter } from './../../../../assets/vendor_js/icsFormatter';
import { MdSnackBar, MdDialog, MdDialogRef } from '@angular/material';
import { Cookie } from 'ng2-cookies/ng2-cookies';
import { ConfirmService } from "../../../shared/services/confirm.service";

@Component({
  selector: 'app-trophy-comments-popup',
  templateUrl: './trophy-comments-popup.component.html',
  styleUrls: ['./trophy-comments-popup.component.scss']
})
export class TrophyCommentsPopupComponent implements OnInit {

  comments: any;
  tempComments: any;
  isProcessing: any;
  editedComment: any;
  reqBody: any;
  loggedInUser: any;
  constructor(public rendrer: Renderer2,
      public viewContainerRef:ViewContainerRef,
    public confirmService: ConfirmService,
    public restfulLaravelService: RestfulLaravelService, public coreService: CoreService
    , public dialog: MdDialogRef<TrophyCommentsPopupComponent>) { }

  ngOnInit() {
    this.loggedInUser = JSON.parse(Cookie.get('happyhr-userInfo')) || JSON.parse(localStorage.getItem('happyhr-userInfo'));
    this.tempComments = JSON.parse(JSON.stringify(this.comments));
  }

  onSendComment(isValid, commentForm) {
    this.isProcessing = true;
    this.restfulLaravelService.post('shoutout-comment-add', this.reqBody).subscribe(
      d => {
        if (d.code == "200") {
          this.saveComment(d, commentForm);
          this.coreService.notify("Successful", d.message, 1);
        }
        else this.coreService.notify("Unsuccessful", d.message, 0);
        this.isProcessing = false;
      },
      error => this.coreService.notify("Unsuccessful", error.message, 0),
      () => { this.isProcessing = false; }
    )
  }
  onTime(time){
    return new Date(time);
  }

  onEdit(c) {
    this.reqBody = JSON.parse(JSON.stringify(c));
  }

  saveComment(d, commentForm) {
    this.comments = d.comments;
    this.tempComments = d.comments;
    this.coreService.setTrophyAndShoutouts(d.data);
    commentForm.reset();
  }

  onDelete(c) {
    this.confirmService.confirm(this.confirmService.tilte, this.confirmService.message, this.viewContainerRef)
      .subscribe(res => {
        let result = res;
        if (result) {
          c['isProcessing'] = true;
          this.restfulLaravelService.delete(`shoutout-comment-delete/${c.shoutoutCommentID}`).subscribe(
            d => {
              if (d.code == "200") {
                this.coreService.notify("Successful", d.message, 1);
                this.comments = d.comments;
                this.tempComments = d.comments;
                this.coreService.setTrophyAndShoutouts(d.data);
              }
              else this.coreService.notify("Unsuccessful", d.message, 0);
              c['isProcessing'] = false;
            },
            error => this.coreService.notify("Unsuccessful", error.message, 0),
            () => { c['isProcessing'] = false; }
          )

        }
      })

  }

}
