import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { APPCONFIG } from '../../../config';
import { CoreService } from '../../../shared/services/core.service';
import { CompanyAdminDashboardService } from "../company-admin-dashboard.service";
import { ConfirmService } from '../../../shared/services/confirm.service';
import * as moment from 'moment';

@Component({
  selector: 'app-mandatory-document-listing',
  templateUrl: './mandatory-document-listing.component.html',
  styleUrls: ['./mandatory-document-listing.component.scss']
})
export class MandatoryDocumentListingComponent implements OnInit {
 isProcessing = false;
  unsubscriber:any;
  type='';
  docs=[];
  isDeleteProcessing=false;
  requestBody ={
    search:'',
    status:''
  }
  filter={
    search:'',
    status:''
  }

  constructor(public companyService: CompanyAdminDashboardService, public coreService: CoreService, 
              private router: Router,private route: ActivatedRoute, public confirmService: ConfirmService, public viewContainerRef:ViewContainerRef ) { }

  ngOnInit() {
    APPCONFIG.heading = "Mandatory documents";
    this.getDocList();
  }
  formatDate(v){
    return moment(v).isValid()? moment(v).format('DD MMM YYYY') : 'Not specified';
  }
  onSearch(){
    this.unsubscriber.unsubscribe();
    this.requestBody.search=encodeURIComponent(this.filter.search);
    this.getDocList();
  }

  onstatusTypeChange(e){
    console.log("eeeee",e)
    this.requestBody.status =e;
    this.getDocList();
  }

  getDocList() {
    this.isProcessing = true;
    this.unsubscriber = this.companyService.getAllMandatoryDocs(JSON.stringify(this.requestBody)).subscribe(
      d => {
        if (d.code == "200") {
          this.docs=d.data;
          this.isProcessing = false;
        }
        else this.coreService.notify("Unsuccessful", d.message, 0);
      },
      error => this.coreService.notify("Unsuccessful", error.message, 0),
      () => { }
    )
  }

  onViewDoc(link){
    console.log("url",link);
    window.open(link);
  }

  onArchieve(data, index){
    let item={module:''};
    item.module='mandatoryDocument';
          data["isDeleteProcessing"] = true;
          data["type"]='archive';
          this.companyService.archiveData(item, data).subscribe(
            res => {
              data["isDeleteProcessing"]= false;
              if (res.code == 200) {
                this.docs.splice(index, 1);
                this.coreService.notify("Successful", res.message, 1);
              }
              else return this.coreService.notify("Unsuccessful", res.message, 0);
            },
            error => {
              data["isDeleteProcessing"]= false;
              this.coreService.notify("Unsuccessful", error.message, 0);
            }
          )
        
  }

    onMarkCurrent(data,index){
    let item={module:''};
    item.module='mandatoryDocument';
          data["isDeleteProcessing"] = true;
          data["type"]='archive';
          this.companyService.onMarkCurrent(item, data).subscribe(
            res => {
              data["isDeleteProcessing"]= false;
              if (res.code == 200) {
                this.docs.splice(index, 1);
                this.coreService.notify("Successful", res.message, 1);
              }
              else return this.coreService.notify("Unsuccessful", res.message, 0);
            },
            error => {
              data["isDeleteProcessing"]= false;
              this.coreService.notify("Unsuccessful", error.message, 0);
            }
          )
        
  }

  onDelete(data, index) {
    let item={title:data.title,module:''};
    item.module='mandatoryDocument';
    this.confirmService.confirm("Delete confirmation", "Do you want to delete this " + item.title + " ?", this.viewContainerRef).subscribe(
      confirm => {
        if (confirm) {
          data["isDeleteProcessing"] = true;
          this.companyService.deleteData(item, data).subscribe(
            res => {
              data["isDeleteProcessing"]= false;
              if (res.code == 200) {
                this.docs.splice(index, 1);
                this.coreService.notify("Successful", res.message, 1);
              }
              else return this.coreService.notify("Unsuccessful", res.message, 0);
            },
            error => {
              data["isDeleteProcessing"]= false;
              this.coreService.notify("Unsuccessful", error.message, 0);
            }
          )
        }
      }
    )
  }

}
