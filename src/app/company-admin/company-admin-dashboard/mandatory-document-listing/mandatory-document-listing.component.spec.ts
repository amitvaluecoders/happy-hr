import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MandatoryDocumentListingComponent } from './mandatory-document-listing.component';

describe('MandatoryDocumentListingComponent', () => {
  let component: MandatoryDocumentListingComponent;
  let fixture: ComponentFixture<MandatoryDocumentListingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MandatoryDocumentListingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MandatoryDocumentListingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
