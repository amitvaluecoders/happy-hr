import { Injectable } from '@angular/core';
import { RestfulLaravelService } from "../../shared/services/restful-laravel.service";

@Injectable()
export class CompanyAdminDashboardService {
  public trophyAndShoutouts:any=[];
  constructor(private restfulService: RestfulLaravelService) { 

  }

  getActivities(reqBody) {
    if (reqBody.isID) return this.restfulService.post(`dashboard-activity-employee`,{userIDs:reqBody.ids})
    return this.restfulService.get(`dashboard-activity`);
  }

  getActivitiesKPI(reqBody) {
    if (reqBody.isID) return this.restfulService.post(`dashboard-activity-employee-pi`,{userIDs:reqBody.ids})
    return this.restfulService.get(`dashboard-activity-pi`);
  }

  deleteData(list, data) {
    let moduleName = list.module;
    switch (moduleName) {  
      case 'trophy':
        return this.restfulService.delete(`delete-trophy/${data.trophyID}`);
      case '360Survey':
        return this.restfulService.delete(`delete-company-survey/${data.surveyID}`);
      case 'exitSurvey':
        return this.restfulService.delete(`delete-employee-exit-survey/${data.employeeExitSurveyID}`);
      case 'pip':
        return this.restfulService.delete(`pip-delete/${data.pipID}`);
      case 'developmentPlan':
        return this.restfulService.delete(`development-plan-delete/${data.developmentPlanID}`);
      case 'bpip':
        return this.restfulService.delete(`bpip-delete/${data.bpipID}`);
      case 'disciplinaryAction':
        return this.restfulService.delete(`delete-disciplinary-action/${data.disciplinaryActionID}`);
      case 'probationaryMeeting':
        return this.restfulService.delete(`probationary-meeting-delete/${data.probationaryPeriodMeetingID}`);
      case 'assetsApproval':
        let reqBody = { "assetApprovalID": data.assetApprovalID, "status": "Deleted", "responseNote": "" };
        return this.restfulService.put(`change-asset-status`, reqBody);
      case 'employeeSignedDocument':
      return this.restfulService.delete(`uploaded-document-delete/${data.employeeDocumentSignedID}`);
      case 'mandatoryDocument':
      return this.restfulService.delete(`mandatory-document-delete/${data.mandatoryDocumentID}`);
      case 'mandatoryDocuments':
      return this.restfulService.delete(`mandatory-document-delete/${data.mandatoryDocumentID}`);
      case 'onboardchecklist':
      return this.restfulService.delete(`delete-checklist/${data.employeeChecklistID}`);
      case 'offboardchecklist':
      return this.restfulService.delete(`delete-checklist/${data.employeeChecklistID}`);
      case 'leaveApproval':
        return this.restfulService.delete(`deleteLeaves/${data.leaveID}`);
    }
  }

    cancelData(list, data) {
    let moduleName = list.module;
    switch (moduleName) {  
      case 'onboardchecklist':
        return this.restfulService.put(`checklist-cancel`,{employeeChecklistID:data.employeeChecklistID});
      case 'offboardchecklist':
        return this.restfulService.put(`checklist-cancel`,{employeeChecklistID:data.employeeChecklistID});

      case 'probationaryMeeting':
        return this.restfulService.put(`probationary-meeting-cancel`, {probationaryMeetingID:data.probationaryPeriodMeetingID});
      // case '360Survey':
      //   return this.restfulService.delete(`delete-company-survey/${data.surveyID}`);
      // case 'pip':
      //   return this.restfulService.delete(`pip-delete/${data.pipID}`);
      // case 'developmentPlan':
      //   return this.restfulService.delete(`development-plan-delete/${data.developmentPlanID}`);
      // case 'bpip':
      //   return this.restfulService.delete(`bpip-delete/${data.bpipID}`);
      // case 'disciplinaryAction':
      //   return this.restfulService.delete(`delete-disciplinary-action/${data.disciplinaryActionID}`);
      // case 'probationaryMeeting':
      //   return this.restfulService.delete(`probationary-meeting-delete/${data.probationaryPeriodMeetingID}`);
      // case 'assetsApproval':
      //   let reqBody = { "assetApprovalID": data.assetApprovalID, "status": "Deleted", "responseNote": "" };
      //   return this.restfulService.put(`change-asset-status`, reqBody);
      // case 'employeeSignedDocument':
      // return this.restfulService.delete(`uploaded-document-delete/${data.employeeDocumentSignedID}`);
    }
  }

  archiveData(list, data) {
    let moduleName = list.module;
    switch (moduleName) {  
      
    case 'employeeSignedDocument':
    return this.restfulService.put(`uploaded-document-mark-archive`,{"employeeDocumentSignedID":data.employeeDocumentSignedID});
    case 'mandatoryDocuments':
    return this.restfulService.put(`mandatory-document-mark-archive`,{"mandatoryDocumentID":data.mandatoryDocumentID});
     case 'mandatoryDocument':
    return this.restfulService.put(`mandatory-document-mark-archive`,{"mandatoryDocumentID":data.mandatoryDocumentID});
    }
  }

   onMarkCurrent(list, data) {
    let moduleName = list.module;
    switch (moduleName) {  
      
    case 'employeeSignedDocument':
    return this.restfulService.put(`uploaded-document-mark-current`,{employeeDocumentSignedID:data.employeeDocumentSignedID});
    case 'mandatoryDocuments':
    return this.restfulService.put(`mandatory-document-mark-current`,{mandatoryDocumentID:data.mandatoryDocumentID});
     case 'mandatoryDocument':
    return this.restfulService.put(`mandatory-document-mark-current`,{mandatoryDocumentID:data.mandatoryDocumentID});
    }
  }

  changeStatus(reqBody, moduleName) {
    if (moduleName == "assetsApproval") {
      return this.restfulService.put(`change-asset-status`, reqBody);
    } else if (moduleName == "leaveApproval") {
      return this.restfulService.put(`accept-reject-leave`, reqBody);
    }
  }

  getAllUploadedDocs(reqBody){
    return this.restfulService.get(`uploaded-document-list/${encodeURIComponent(reqBody)}`);
  }

  getAllMandatoryDocs(reqBody){
    return this.restfulService.get(`mandatory-document-list/${encodeURIComponent(reqBody)}`);
  }
  
    getAllUsersUnderLoggedinUser() {
    return this.restfulService.get(`show-all-employee`);  
    // return this.restfulService.get(`get-employee-under`);
  }

  getAllEmployee() {
    return this.restfulService.get('show-all-employee');
  }

  setShootout(reqBody) {
    return this.restfulService.post(`shoutout-add`, reqBody);
  }

  changeNewEmployeeStatus(reqBody) {
    return this.restfulService.post(`employee-invited-reject-by-company-admin`, reqBody);
  }

  downloadPdf(list, data) {
    switch (list.module) {
      case 'ohs':
        window.open(`${this.restfulService.baseUrl}generate-ohs-pdf/${data.ohsID}`); break;
    }
  }

  sendReminderforKpi(reqBody){
    return this.restfulService.put(`kpi-send-reminder`,reqBody);    
  }

  cancelProbationaryMeating(probationData) {
    return this.restfulService.put(`probationary-meeting-cancel`, probationData);
  }

  deleteProbationMeeting(id) {
    return this.restfulService.delete(`probationary-meeting-delete/${id}`);
  }

}
