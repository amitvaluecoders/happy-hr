import { Component, OnInit,ElementRef,ViewChild,Renderer2} from '@angular/core';
import { APPCONFIG } from '../../../config';
import { RestfulLaravelService } from "../../../shared/services/restful-laravel.service";
import { CoreService } from "../../../shared/services/core.service";
import { icsFormatter }  from './../../../../assets/vendor_js/icsFormatter';
import { MdSnackBar, MdDialog, MdDialogRef } from '@angular/material';
import { ShootOutComponent } from '../shoot-out/shoot-out.component';
import { TrophyCommentsPopupComponent } from '../trophy-comments-popup/trophy-comments-popup.component';
import { CompanyAdminDashboardService } from '../company-admin-dashboard.service';

@Component({
  selector: 'app-cmp-dashboard',
  templateUrl: './cmp-dashboard.component.html',
  styleUrls: ['./cmp-dashboard.component.scss']
})
export class CmpDashboardComponent implements OnInit {

  @ViewChild('calander') calanderRef :ElementRef;
  public isProcessing=false;
  public activityCollapse = false;
  public actStatusCollapse = false;
  public staffCollapse = false;
  public expDocCollapse = false;
  public userID='';
  linkchk:any;
  public dashBoard:any;
  dropdownListEmployees = [];
  userData:any;
  selectedItems = [];
  public isEmployeeListProcessing=false;
  dropdownSettings = {};  
  public calendarOptions = {
    height: '10%',
    width:'70%',
    eventColor: '#64c3fb',
    header: {
      left: 'prev,next today',
      center: 'title',
      right: 'month,agendaWeek,agendaDay'
      },
    fixedWeekCount : false,
    defaultDate: new Date(),
    eventLimit: true, 
    editable: true,
    events: [],
    eventRender: function( event, element, view ) { var title = element.find('.fc-title, .fc-list-item-title'); title.html(title.text());   },
    eventClick: function(calEvent, jsEvent, view) {
        var calEntry = icsFormatter();

        var title = calEvent.description.substr(0 , calEvent.description.search('\n'));
        var place = '';
        var begin = new Date(calEvent.start.utc()).toString();
        var end = new Date(calEvent.end.utc()).toString();

        var description =  calEvent.description.substr(calEvent.description.search('\n')+ 1 ,calEvent.description.length);

        calEntry.addEvent(title,description, place, begin, end);
        calEntry.download('vcart', '.ics');
 }
    
  };
 
   constructor(public service:CompanyAdminDashboardService,public rendrer:Renderer2,public restfulLaravelService:RestfulLaravelService,public coreService:CoreService
  ,public dialog: MdDialog) { }


  ngOnInit() {
    APPCONFIG.heading="Dashboard";
    this.userData = JSON.parse(localStorage.getItem("happyhr-userInfo"));
    this.getDashBoardDeatils();
    this.getEmployeeeList();
    this.dropdownSettings = {
      singleSelection: true, 
      text:"Select employee",
      enableSearchFilter: true,
      classes:"myclass assmng"
   }; 
  }

  createShootout(){
     let dialogRef = this.dialog.open(ShootOutComponent, { width: '600px' });

        dialogRef.afterClosed().subscribe(result => {
            if (result) {
                
            }
        });
  }

   addCommentPopup(data){
     let dialogRef = this.dialog.open(TrophyCommentsPopupComponent, { width: '600px' });
     dialogRef.componentInstance.comments =   data.comments ; 
     dialogRef.componentInstance.reqBody =  {shoutoutID:data.shoutoutID,note:''} ; 
        dialogRef.afterClosed().subscribe(result => {
            let temp = this.coreService.getTrophyAndShoutouts();
            if(temp && temp.length>0)this.dashBoard.trophyAndShoutouts = this.coreService.getTrophyAndShoutouts(); 
        });
  }

  // method to get all employee listing for manger and third pary listing 
  getEmployeeeList() {
    this.isEmployeeListProcessing = true;
    this.restfulLaravelService.get('show-all-employee').subscribe(
         d => {
             if (d.code == "200") {
                 this.dropdownListEmployees=d.data.filter(item =>{
                     item['id'] = item.userId;
                     item['itemName'] = item.name;
                     item['isSelected'] = false;            
                     return item;
                 })
               this.isEmployeeListProcessing = false; 
              
             }
             else this.coreService.notify("Unsuccessful", d.message, 0);
         },
         error => this.coreService.notify("Unsuccessful", error.message, 0),
         () => { }
     )
 }

    onItemSelect(e){
      this.userID=e.id;
      this.getDashBoardDeatils();

    }

    OnItemDeSelect(e){
      this.userID = null;
      this.getDashBoardDeatils();
    }

   getDashBoardDeatils(){
      this.isProcessing = true; 
      let url='dashboard';
      if(this.userID) url += `/${this.userID}`;
      this.restfulLaravelService.get(url).subscribe(
        res => { 
          if (res.code == 200) {
           this.dashBoard=res.data;
           this.setCalanderData();
           this.dashBoard['trophyAndShoutouts']=this.setTrophyANdSoutouts(res.data) || [];
           this.isProcessing = false;
          }
          else return this.coreService.notify("Unsuccessful", res.message, 0);
        },
        error => {
          this.isProcessing = false;
          if (error.code == 400) {
            return this.coreService.notify("Unsuccessful", error.message, 0);
          }
          this.coreService.notify("Unsuccessful", "Error", 0)
        }
      ) 
   }

   onViewProfile(id){
    var newWindow = window.open(`/#/app/company-admin/employee-management/profile/${id}`); 
   }


   setTrophyANdSoutouts(data){
    let tempArray = data.shoutout.concat(data.trophy);
    tempArray.sort(function(a:any,b:any){
      let firstValue:any = new Date(b.insertTime);
      let secondValue:any = new Date(a.insertTime);
      return  firstValue - secondValue ;
    });
    return tempArray;
   }


   formatActivites(){
     this.linkchk ="pip is ctreate by {{ajay}} for {{dines-h}} on 23. "
    //  let re= /\{\{(\w+)\}\}/gi;
     let re = /\{\{([a-zA-Z0-9-]+)\}\}/gi
          let key =this.linkchk.match(re);
          console.log(key,"ddd")
          for(let c of key){
        this.linkchk=this.linkchk.replace(c, "<a target='_blank' href='www.google.com' >dfg</a>");
          }
          
     for(let i in this.dashBoard.activityLog ){
      //  console.log("th",this.dashBoard.activityLog[i])
      for(let j of this.dashBoard.activityLog[i]){
        let original='';  
          // let tempString = j.template;    
          // let re= /\{\{(\w+)\}\}/gi;
          // let key =tempString.match(re);
          // console.log(key,"ddd")
        
      }
     }
   }

   onRoute(){
     console.log("dasdasdasdasdasdasdasdasd---");
   }

   setCalanderData(){
    this.calendarOptions.events=[];
      this.dashBoard.calendarData.filter(d=>{
        let users='';
        for(let c of d.employeeUserNames){users += users?","+c:c }
        let color='yellow';
        // if(d.status=='Cancelled') color='orange';
        // if(d.status=='Approved') color='lightgreen';
        // if(d.status=='Rejected') color='red';
          this.calendarOptions.events.push({
              title:"<div class='myEvent'>"+d.title+ "<div class='footer clearfix'> <span class='user'>" +users+"</span> <span class='abb'>" +d.moduleAbbreviation+"</span></div>  </div>",
              start:d.startTime,
              end:d.endTime,
              backgroundColor:'#ebebebe',
              description : d.title + '\n Users:' +  users
          })
        

        // if(d.reminderTime){
        //   this.calendarOptions.events.push({
        //     title:"<div class='myEvent'>"+" Reminder "+" Performance appraisal "+ "<div class='footer clearfix'> <span class='user'>" +users+"</span> <span class='abb'>" +d.moduleAbbreviation+"</span></div>  </div>",
        //     start:d.reminderTime,
        //     end:'',
        //     backgroundColor:'#ebebebe',
        //     description : d.title + '\n Users:' +  users
        //   })
        // }
       
      })

      // this.rendrer.setAttribute(this.calanderRef.nativeElement.querySelector('.fc-scroller'),'mySlimScroll','')
  }

}
