import { Routes, RouterModule }  from '@angular/router';
import { InviteSubcontractorComponent } from './invite-subcontractor/invite-subcontractor.component';
 
export const SubcontractorAdminPagesRoutes: Routes = [
    {
        path: '',
        children: [
            // { path: '', component: MasterOnBoardingChecklistComponent },
            { path: 'invite/:nodeID', component: InviteSubcontractorComponent }
        ]
    }
];

export const SubcontractorAdminPagesRoutingModule = RouterModule.forChild(SubcontractorAdminPagesRoutes);
