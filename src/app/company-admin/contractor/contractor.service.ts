import { Injectable } from '@angular/core';
import { RestfulwebService } from '../../shared/services/restfulweb.service';
import { RestfulLaravelService } from '../../shared/services/restful-laravel.service';


@Injectable()
export class SubContractorService {

 constructor(
    private restfulWebService: RestfulwebService,
    private restfulLaravelService: RestfulLaravelService, 
  ){}


registerSubcontractor(reqBody){
  return this.restfulLaravelService.post('contractor-invite',reqBody);
}
getOwnContract(){
  return this.restfulLaravelService.get('get-own-types-present');    
}
getOwnContractForSubContractor(){
  return this.restfulLaravelService.get('get-own-types-present/contractor');    
}




}
