import { Component, OnInit ,ViewContainerRef} from "@angular/core";
import { APPCONFIG } from "../../../config";
import { Router, ActivatedRoute } from "@angular/router";
import { CoreService } from "../../../shared/services/core.service";
import { ConfirmService } from "../../../shared/services/confirm.service";
import { SubContractorService } from "../contractor.service";
import { RestfulLaravelService } from '../../../shared/services/restful-laravel.service';
import { CountryStateService } from "../../../shared/services/country-state.service";
import { MdDialog } from '@angular/material';
import { MandatoryDocumentsPopupComponent } from '../../../shared/components/mandatory-documents-popup/mandatory-documents-popup.component';

@Component({
  selector: "app-invite-subcontractor",
  templateUrl: "./invite-subcontractor.component.html",
  styleUrls: ["./invite-subcontractor.component.scss"]
})
export class InviteSubcontractorComponent implements OnInit {
  disableState:boolean;
  public progress;
  constructor(
    public viewContainerRef:ViewContainerRef,
    public confirmService:ConfirmService,
    public dialog:MdDialog,
    public countryService: CountryStateService,
    public restfulWebService:RestfulLaravelService,
    public coreService: CoreService,
    public router: Router,
    public activatedRoute: ActivatedRoute,
    public service: SubContractorService
  ) {}
  public isProcessing: Boolean;
  public isButtonClicked = false;
  public payment = "company";
  public stateList =[];
  ownContract:any={};
  public ownContractList = [];
  contractTypeID:any;
  public LoggedInUser:any;
  public role:any;
  public invite = {
    companyName: "",
    ABN: "",
    address: "",
    suburb: "",
    stateName: "",
    email: "",
    phoneNumber: "",
    firstName: "",
    lastName: "",
    rateType: "",
    rateValue: "",
    scope: "",
    invoicePaid: "",
    postalCode:"",
    abnContractType: "",
    invoicePaidOther:"",
    contractFileName:"",
    nodeID:"",
    mandatoryDocuments:[]
  };

  public docPath='';
  public docError=false;
  public fileName:any;
  
  dropdownSettings={};

  listRateType=[];
  selectedRateType=[];

  listInvoiceType=[];
  selectedInvoiceType=[];

  listContractType=[];
  selectedContractType=[];
  selectedOwnContractType=[];
  

  ngOnInit() {
    this.LoggedInUser = JSON.parse(localStorage.getItem("happyhr-userInfo"));
    //this.role = this.LoggedInUser.userRole;
    console.log("logged in user",this.LoggedInUser);
    console.log("role.........",this.role);
    this.stateList = this.countryService.getState(13);
    APPCONFIG.heading = "Invite a sub-contractor";
    if (!this.activatedRoute.snapshot.params["nodeID"]) {
      return this.coreService.notify(
        "Unsuccessful",
        "Somthing going wrong ! Try Again",
        0
      );
    }
    this.setDropdownItems();
    this.dropdownSettings = {
        singleSelection: true, 
        text:"Select",
        enableSearchFilter: true,
        classes:"myclass"
    }; 
    this.getOwnContractList();
  }


  setDropdownItems(){
    this.listRateType = [
      { id : 0, itemName : "Rate per hour including GST" , value : "Per hour" },
      { id : 1, itemName : "Rate per hour +GST" , value : "Per hour GST" },
      { id : 2, itemName : "Rate per day including GST" , value : "Per day" },
      { id : 3, itemName : "Rate per day +GST" , value : "Per day GST" },
      { id : 4, itemName : "Project fee including GST" , value : "Project"},
      { id : 5, itemName : "Project fee + GST" , value : "Project GST"},
      { id : 6, itemName : "Percentage of sales" , value : "Percentage"},
      { id : 7, itemName : "Percentage of consultation", value : "Percentage consulation"},
      { id : 8, itemName : "Percentage of GP", value : "Percentage GP"},
      // { id : 8, itemName : "Percentage of GP", value : "Percentage GP"},

    ]

    this.listInvoiceType = [
      { id : 0, itemName : "Day of invoice" , value : "0" },
      { id : 1, itemName : "7 Days from invoice date" , value : "7" },
      { id : 2, itemName : "14 Days from invoice date" , value : "14" },
      { id : 3, itemName : "30 Days from invoice date" , value : "30" },
      { id : 4, itemName : "45 Days from invoice date" , value : "45" },
      { id : 5, itemName : "60 Days from invoice date" , value : "60" },
      { id : 6, itemName : "Other days from invoice date" , value : "-1" },
    ]

    this.listContractType = [
      { id : 0, itemName : "Use Happy HR ABN contract" , value : "HHR" },
      { id : 1, itemName : "Use own ABN contract" , value : "Own" },
      { id : 2, itemName : "Upload of current contract" , value : "Upload" },
      { id : 3, itemName : "Own Contract" , value: 'x'},
    ]
  }

  fileChange(event) {
    this.isProcessing=true;
    this.docError=false;
    this.fileName='';
    this.docPath='';
    let fileList: FileList = event.target.files;
    if(fileList.length > 0) {
        let file: File = fileList[0];
        let formData:FormData = new FormData();
        formData.append('contractorContract',file);
        this.restfulWebService.post('file-upload',formData).subscribe(
          d=>{ 
            if(d.code="200"){
              this.isProcessing=false;
                this.docPath=d.data.contractorContract;
                this.fileName=d.data.contractorContract;
            }
            else {this.docError=true;this.isProcessing=false;}
          },
          error=>{this.docError=true;this.isProcessing=false;}
          )
    }
}

register(form){
  if(form.form.valid){

      this.invite.rateType = this.selectedRateType[0].value;
      this.invite.invoicePaid = this.selectedInvoiceType[0].value;
      this.invite.abnContractType=(this.contractTypeID=='x') ? this.selectedOwnContractType[0].value : this.selectedContractType[0].value;
      // this.invite.abnContractType = this.selectedContractType[0].value;  
      this.invite.contractFileName =  this.fileName;
      this.invite.nodeID = this.activatedRoute.snapshot.params["nodeID"];
      let reqBody = JSON.parse(JSON.stringify(this.invite)) 
       reqBody['mandatoryDocuments']=[];
       this.invite.mandatoryDocuments.filter(l=>{
          reqBody['mandatoryDocuments'].push(l.name);
      });
      this.isProcessing = true;
      this.service.registerSubcontractor(reqBody).subscribe(
        d=>{
          if(d.code =='200'){
            this.router.navigate(['/app/company-admin/organisation-chart']);
          this.coreService.notify("Successful",d.message,1);          
            this.isProcessing = false;
          }
        },
        err=>{
          this.isProcessing = false;
          this.coreService.notify("Unsuccessful",err.message,0);
        }
      )

  }
}
getOwnContractList(){
  this.isProcessing = true;
  this.service.getOwnContractForSubContractor().subscribe(
    res => {
      this.isProcessing = false;
      if (res.code == 200) {
        this.ownContract = res.data;
        this.ownContract.types.forEach((item,i)=>{
            if(item){
                  if(item=="Own"){
                    let data = {"id" : i,"itemName":item+"\t"+"contract","value":"Own_1"}
                    this.ownContractList.push(data);
                    console.log("list",this.ownContractList);  
                  }
                  else{
                    let name= item.split("_");
                    let data = {"id" : i,"itemName":name[0]+"\t"+"contract"+"\t"+name[1],"value":"Own"+"_" + name[1]}
                    this.ownContractList.push(data);
                  }
            }
            
        })
      }
    },
    err => {
      this.isProcessing = false;
    }
  )

}
onSelectContractType(item, selected) {
  this.contractTypeID = (selected) ? item.value : null;
}

openMandatoryDocPopup(type,item,key){
  let dialogRef = this.dialog.open(MandatoryDocumentsPopupComponent);
    dialogRef.componentInstance.document = JSON.parse(JSON.stringify(item));
  var mandatoryDocs=[];
  dialogRef.afterClosed().subscribe(response => {
    if(response){
    if(type=='add')this.invite.mandatoryDocuments.push(response);
    else {
      this.invite.mandatoryDocuments = this.invite.mandatoryDocuments.map((item,index)=>{
        if(index == key) item = response;
        return item;
      })
    }
    }
  });
}

onDeleteDoc(i){
   this.confirmService.confirm(this.confirmService.tilte,this.confirmService.message,this.viewContainerRef)
      .subscribe(res => {
      let result = res;
      if (result)  this.invite.mandatoryDocuments.splice(i,1);
    })
    
}

}
