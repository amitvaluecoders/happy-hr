import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InviteSubcontractorComponent } from './invite-subcontractor/invite-subcontractor.component';
import { MaterialModule } from '@angular/material';
import { FormsModule } from '@angular/forms';
import { SharedModule } from '../../shared/shared.module';
import { SubContractorService } from './contractor.service';
import { SubcontractorAdminPagesRoutingModule } from './contractor.routing';
import { AngularMultiSelectModule } from 'angular2-multiselect-checkbox-dropdown/angular2-multiselect-dropdown';
import { CountryStateService } from "../../shared/services/country-state.service";

@NgModule({
  imports: [
    AngularMultiSelectModule,
    MaterialModule,
    FormsModule,
    SharedModule,
    CommonModule,
    SubcontractorAdminPagesRoutingModule
  ],
  providers:[SubContractorService,CountryStateService],
  declarations: [InviteSubcontractorComponent]
})
export class ContractorModule { }
