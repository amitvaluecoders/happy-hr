import { Component, OnInit } from '@angular/core';
import { APPCONFIG } from '../../../config';
import { IDatePickerConfig } from 'ng2-date-picker';

@Component({
  selector: 'app-employee-invite-third-party',
  templateUrl: './employee-invite-third-party.component.html',
  styleUrls: ['./employee-invite-third-party.component.scss']
})
export class EmployeeInviteThirdPartyComponent implements OnInit {
  
  public progress;
  birthday: any;
  config: IDatePickerConfig = {};

  constructor() { 
     this.config.locale = "en";
    this.config.format = "YYYY-MM-DD hh:mm A";
    this.config.showMultipleYearsNavigation = true;
    this.config.weekDayFormat = 'dd';
    this.config.disableKeypress = true;
    this.config.monthFormat="MMM YYYY";
  }

  ngOnInit() {
     APPCONFIG.heading="Invite third party";
     this.progressBarSteps();
  }
 progressBarSteps() {
      this.progress={
          progress_percent_value:50, //progress percent
          total_steps:4,
          current_step:2,
          circle_container_width:20, //circle container width in percent
          steps:[
               {num:1,data:"Create disciplinary meeting"},
               {num:2,data:"Employee: Invite third party",active:true},
               {num:3,data:"Formal disciplinary meeting"},
               {num:4,data:"Result"},
          ]  
       }         
   }
}
