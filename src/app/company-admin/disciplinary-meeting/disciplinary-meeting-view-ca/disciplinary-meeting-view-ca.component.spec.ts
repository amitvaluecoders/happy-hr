import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DisciplinaryMeetingViewCaComponent } from './disciplinary-meeting-view-ca.component';

describe('DisciplinaryMeetingViewCaComponent', () => {
  let component: DisciplinaryMeetingViewCaComponent;
  let fixture: ComponentFixture<DisciplinaryMeetingViewCaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DisciplinaryMeetingViewCaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DisciplinaryMeetingViewCaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
