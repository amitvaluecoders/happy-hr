import { Component, OnInit,ViewContainerRef } from '@angular/core';
import { APPCONFIG } from '../../../config';
import { IDatePickerConfig } from 'ng2-date-picker';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { DisciplinaryMeetingService } from '../disciplinary-meeting.service';
import { Cookie } from 'ng2-cookies/ng2-cookies';
import { CoreService } from '../../../shared/services/core.service';
import * as moment from 'moment';
import { ConfirmService } from '../../../shared/services/confirm.service';
import { MdSnackBar, MdDialog, MdDialogRef } from '@angular/material';
import { CreateDisciplinaryActionStatementPopupComponent } from './create-disciplinary-action-statement-popup/create-disciplinary-action-statement-popup.component';
import { ActionsOnEmployeePopupComponent } from '../../../shared/components/actions-on-employee-popup/actions-on-employee-popup.component';

@Component({
  selector: 'app-results',
  templateUrl: './results.component.html',
  styleUrls: ['./results.component.scss']
})
export class ResultsComponent implements OnInit {

    public damID:any;
    public heading='';
    public isResultProcessing=false;
    public progress={
          progress_percent_value:66, //progress percent
          total_steps:3,
          current_step:2,
          circle_container_width:20, //circle container width in percent
          steps:[
               {num:1,data:"Create disciplinary meeting",active:false},
               {num:2,data:"Formal disciplinary meeting",active:true},
               {num:3,data:"Result",active:false},
          ]  
       };
    birthday: any;
    config: IDatePickerConfig = {};
    disciplinaryMeeting={
      disciplinaryActionID:'',
      employee:{
        userID:''
      },
      manager:{},
      thirdParty:[],
      statements:{
        Employee:[],
        ThirdParty:[],
        Manager:[]
      }
    };
    isProcessing=false;

    constructor(public  disciplinaryMeetingService: DisciplinaryMeetingService, public confirmService: ConfirmService,
    public router: Router, public activatedRoute: ActivatedRoute,public dialog: MdDialog,public viewContainerRef: ViewContainerRef,
    public coreService: CoreService){
    this.config.locale = "en";
    this.config.format = "YYYY-MM-DD hh:mm A";
    this.config.showMultipleYearsNavigation = true;
    this.config.weekDayFormat = 'dd';
    this.config.disableKeypress = true;
    this.config.monthFormat="MMM YYYY";
    }

  ngOnInit() {
    this.disciplinaryMeeting['Manager']=[];
    this.disciplinaryMeeting['Employee']=[];
    this.disciplinaryMeeting['ThirdParty']=[];
     APPCONFIG.heading="";
    //  this.progressBarSteps(); 
     this.damID=this.activatedRoute.snapshot.params['damID'];
     this.getDisciplinaryDetails();
  }
//  progressBarSteps(percent,currentStep,) {
//       this.progress={
//           progress_percent_value:100, //progress percent
//           total_steps:3,
//           current_step:3,
//           circle_container_width:20, //circle container width in percent
//           steps:[
//                {num:1,data:"Create disciplinary meeting",active:false},
//                {num:2,data:"Formal disciplinary meeting",active:false},
//                {num:3,data:"Result",active:false},
//           ]  
//        }         
//    }
   onEdit(){
      this.router.navigate([`/app/company-admin/disciplinary-meeting/update/${this.damID}`]);
   }

// method to get details of disciplinary ations. 
  getDisciplinaryDetails(){ 
    this.isProcessing=true;       
      this.disciplinaryMeetingService.getDisciplinaryMeetingDetails(this.damID).subscribe(
          d => {
        if (d.code == "200") {
            this.setProgressBar(d.data); 
            this.disciplinaryMeeting=d.data;       
            // if(this.disciplinaryMeeting.statements[''])this.disciplinaryMeeting.statements['ThirdParty']=this.disciplinaryMeeting.statements[''];
            for(let i of this.disciplinaryMeeting.statements.Employee){i['isDeleteProcessing']=false;}
            for(let i of this.disciplinaryMeeting.statements.Manager){i['isDeleteProcessing']=false;}
            for(let i of this.disciplinaryMeeting.statements.ThirdParty){i['isDeleteProcessing']=false;}
            this.isProcessing=false; 
        }
        else this.coreService.notify("Unsuccessful", d.message, 0);
    },
    error => this.coreService.notify("Unsuccessful", error.message, 0),
    () => { }                
    )     
  }

  setProgressBar(data){
   
    APPCONFIG.heading='Formal disciplinary meeting';
      if(data.statements.Employee || data.statements.Manager || data.statements.ThirdParty){
        this.progress.progress_percent_value=66;        
        this.progress.current_step=2;
        this.progress.steps[1].active=true;
        APPCONFIG.heading="Formal disciplinary meeting";
      }
       if(data.result){
        this.progress.progress_percent_value=100;
        this.progress.current_step=3;
        this.progress.steps[1].active=true;
        this.progress.steps[2].active=true;
        APPCONFIG.heading="Result";     
      }
      this.heading=APPCONFIG.heading;     
  }  


  onResult(result){
        if ((result == 'Terminate') || (result == 'Warning') || (result == 'Final Warning') || (result == 'Stand down') || (result == 'Serious Disciplinary Action')  ) {
      let info = { disciplinaryActionID: this.disciplinaryMeeting.disciplinaryActionID, type: result, userID: this.disciplinaryMeeting['employee'].userID, subject: "", body: '' }
      let dialogRef = this.dialog.open(ActionsOnEmployeePopupComponent);
      dialogRef.componentInstance.actionInfo = info;
      dialogRef.afterClosed().subscribe(r => {
        if (r) {
          this.submitResult(result);
        }
      })
    }
    else this.submitResult(result);
  }


  submitResult(result){
        this.isResultProcessing=true; 
        let reqBody={
          disciplinaryActionID:this.disciplinaryMeeting.disciplinaryActionID,
          result:result
        }      
      this.disciplinaryMeetingService.setResult(reqBody).subscribe(
          d => {
        if (d.code == "200") {
          let userID=this.disciplinaryMeeting['employee'].userID;
          this.coreService.notify("Successful", d.message, 1);
            switch(result) {
                  case 'PIP':
                    this.router.navigate([`/app/company-admin/performance-improvement-plan/create/${userID}`]);          
                      break;
                  case 'BPIP':
                      this.router.navigate([`/app/company-admin/behavioral-performance-improvement-plan/create/${userID}`]); 
                      break;
                  case 'Development Plan':
                      this.router.navigate([`/app/company-admin/development-plan/create/${userID}`]); 
                      break;
                  case 'Training Plan':
                      this.router.navigate([`/app/company-admin/training-day/master-training-plan/add`]); 
                      break;
                  default:
                      this.router.navigate([`/app/company-admin/listing-page/disciplinary-action-listing`]);  
              }
                    
            this.isResultProcessing=false; 
        }
        else this.coreService.notify("Unsuccessful", d.message, 0);
    },
    error => this.coreService.notify("Unsuccessful", error.message, 0),
    () => { }                
    ) 
  }



  onCreateStatement(statementFor,st){
           let reqBody={
            disciplinaryStatementID:st?st.disciplinaryStatementID:'',
            disciplinaryStatement:st?st.disciplinaryStatement:'',
            disciplinaryActionID:this.disciplinaryMeeting.disciplinaryActionID,
            from:statementFor, 
            thirdPartyID:''
          }
          let dialogRef = this.dialog.open(CreateDisciplinaryActionStatementPopupComponent);
          dialogRef.componentInstance.reqBody=reqBody;
          dialogRef.componentInstance.statementBy=st.userDetails;
          if(statementFor=='ThirdParty') dialogRef.componentInstance.thirdParty=JSON.parse(JSON.stringify(this.disciplinaryMeeting.thirdParty)); 
            
        dialogRef.afterClosed().subscribe(result => {
          if(result){
            if(!st){
            if(statementFor=='ThirdParty') this.disciplinaryMeeting.statements['ThirdParty'].push(result);
            else if(statementFor=='Manager') this.disciplinaryMeeting.statements['Manager'].push(result);
            else if(statementFor=='Employee') {
              result['userDetails']=this.disciplinaryMeeting.employee;
              this.disciplinaryMeeting.statements['Employee'].push(result);
          }
            }
            else{

              if(statementFor=='ThirdParty'){
                  st.disciplinaryStatement=result.disciplinaryStatement;
                  st.disciplinaryStatementID=result.disciplinaryStatementID;
              }
              else{
              st.disciplinaryStatement=result.disciplinaryStatement;
              st.disciplinaryStatementID=result.disciplinaryStatementID;
              st.userDetails.imageUrl=result['userDetails'].imageUrl;
              st.userDetails.name=result['userDetails'].name;
              st.userDetails.thirdPartyID=result['userDetails'].thirdPartyID;
              st.userDetails.userID=result['userDetails'].userID;
              }
            }           
          }
        }); 

 
  }

  onDeleteStatement(statementFor, st) {
    this.confirmService.confirm(this.confirmService.tilte, this.confirmService.message, this.viewContainerRef)
      .subscribe(res => {
        let result = res;
        if (result) {
          st.isDeleteProcessing = true;
          let reqBody = {
            disciplinaryStatementID: st.disciplinaryStatementID,
            from: statementFor
          }
          this.disciplinaryMeetingService.deletemeetingStatement(reqBody).subscribe(
            d => {
              if (d.code == "200") {
                this.coreService.notify("Successful", d.message, 1);
                this.disciplinaryMeeting.statements[statementFor] = this.disciplinaryMeeting.statements[statementFor].filter(function (item) {
                  if (item.disciplinaryStatementID == st.disciplinaryStatementID) return null;
                  else return item;
                })
              }
              else this.coreService.notify("Unsuccessful", d.message, 0);
            },
            error => this.coreService.notify("Unsuccessful", error.message, 0)
          )
        }
      })

  }

}
