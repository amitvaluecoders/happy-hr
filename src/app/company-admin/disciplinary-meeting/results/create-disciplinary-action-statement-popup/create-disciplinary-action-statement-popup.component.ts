import { Component, OnInit } from '@angular/core';
import { CoreService } from '../../../../shared/services/core.service';
import { MdDialogRef } from '@angular/material';
import { DisciplinaryMeetingService } from '../../disciplinary-meeting.service';
import { Cookie } from 'ng2-cookies/ng2-cookies';


@Component({
  selector: 'app-create-disciplinary-action-statement-popup',
  templateUrl: './create-disciplinary-action-statement-popup.component.html',
  styleUrls: ['./create-disciplinary-action-statement-popup.component.scss']
})
export class CreateDisciplinaryActionStatementPopupComponent implements OnInit {
  public isButtonClicked=false;
  public selectedThirdParty=[];
  public thirdParty:any;
  public dropdownSettings={};
  public statement='';
  public isProcessing=false;
  public reqBody:any;
  public statementBy:any;
  public  tempSelectedThirdParty:any;
 constructor(public  disciplinaryMeetingService: DisciplinaryMeetingService,
    public dialog: MdDialogRef<CreateDisciplinaryActionStatementPopupComponent>,public coreService: CoreService) { }

  ngOnInit() {
    // console.log("by",this.statementBy);
    if(this.thirdParty){
      for(let k of this.thirdParty){
        k['itemName']=k.name?k.name:k.email;
        k['id']=k.thirdPartyID;
      }
    }
      this.dropdownSettings = {
          singleSelection: true, 
          text:"Select thirdparty",
          enableSearchFilter: true,
          classes:"myclass assmng"
        };

  }

  onCreate(isvalid){
    this.isButtonClicked=true;
    if(isvalid){
             this.isProcessing=true; 
          // if(this.thirdParty)this.reqBody.thirdPartyID=this.selectedThirdParty[0].id;
            
      this.disciplinaryMeetingService.addmeetingStatement(this.reqBody).subscribe(
          d => {
        if (d.code == "200") {          
            this.isProcessing=false; 
            let response=d.data;
            let userInfo=JSON.parse(Cookie.get('happyhr-userInfo')) || JSON.parse(localStorage.getItem('happyhr-userInfo'));
            response['disciplinaryStatement']=d.data.note;
            response['userDetails']={
             name:userInfo.fullName,
             position:userInfo.position?userInfo.positio:'',
             imageUrl:userInfo.imageUrl?userInfo.imageUrl:'',
             userID:userInfo.userID
            }
            this.coreService.notify("Successful", d.message, 1);
            if(this.tempSelectedThirdParty)response['userDetails']=this.tempSelectedThirdParty;
            this.dialog.close(response) ;
        }
        else this.coreService.notify("Unsuccessful", d.message, 0);
    },
    error => this.coreService.notify("Unsuccessful", error.message, 0),
    () => { }                
    ) 

    }
  }

  onItemSelect(e){
    this.tempSelectedThirdParty=e;
    // console.log(e)
    this.reqBody['thirdPartyID']=e.id;
  }
  

}
