import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateDisciplinaryActionStatementPopupComponent } from './create-disciplinary-action-statement-popup.component';

describe('CreateDisciplinaryActionStatementPopupComponent', () => {
  let component: CreateDisciplinaryActionStatementPopupComponent;
  let fixture: ComponentFixture<CreateDisciplinaryActionStatementPopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateDisciplinaryActionStatementPopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateDisciplinaryActionStatementPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
