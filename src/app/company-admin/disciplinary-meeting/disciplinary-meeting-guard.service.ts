import { Injectable } from '@angular/core';
import { CanActivate, Router, RouterStateSnapshot, ActivatedRouteSnapshot } from "@angular/router";
import { CoreService } from '../../shared/services/core.service';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';

@Injectable()
export class DisciplinaryMeetingGuardService {

    public module = "disciplinaryAction";

    constructor(private _core: CoreService, private router: Router) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        let url: string;
        if (route['_routeConfig']['path'])
            url = route['_routeConfig']['path']
        else url = state.url;
        if (this.checkHavePermission(url)) {
            return true;
        } else {
            this.router.navigate(['/extra/unauthorized']);
            return false;
        }
    }

    private checkHavePermission(path) {
        switch (path) {
            case "disciplinary-action-listing":
                return this._core.getModulePermission(this.module, 'view');
            case "/app/company-admin/disciplinary-meeting":
                return this._core.getModulePermission(this.module, 'view');
            case "create/:userID":
                return this._core.getModulePermission(this.module, 'add');
            case "create/:userID/pip/:pipID":
                return this._core.getModulePermission(this.module, 'add');
            case "create/:userID/bpip/:bpipID":
                return this._core.getModulePermission(this.module, 'add');
            case "update/:damID":
                return this._core.getModulePermission(this.module, 'edit');
            case "view/:damID":
                return this._core.getModulePermission(this.module, 'view');
            case "employee/invite-thirdparty":
                return this._core.getModulePermission(this.module, 'view');
            case "formal":
                return this._core.getModulePermission(this.module, 'view');
            case "result":
                return this._core.getModulePermission(this.module, 'view');
            default:
                return false;
        }
    }
}

