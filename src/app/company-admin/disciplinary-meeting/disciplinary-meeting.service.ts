import { Injectable } from '@angular/core';
import { RestfulLaravelService} from '../../shared/services/restful-laravel.service';

@Injectable()
export class DisciplinaryMeetingService {
  
 constructor(public restfulWebService:RestfulLaravelService) { }

   getAllEmployee():any{
   return this.restfulWebService.get('show-all-employee')
  }

  getAllDisciplinaryActions(reqBody):any{
   return this.restfulWebService.get(`get-disciplinary-action-list/${encodeURIComponent(reqBody)}`)
  }

  getDataForFilters():any{
   return this.restfulWebService.get('get-disciplinary-action-fliter-data')    
  }

 createPLan(reqBody):any{
   return this.restfulWebService.post('disciplinary-action-save',reqBody)
 }

 getDisciplinaryMeetingDetails(reqBody):any{
   return this.restfulWebService.get(`disciplinary-action-detail/${reqBody}`)   
 }

 setResult(reqBody):any{
   return this.restfulWebService.post(`disciplinary-action-result`,reqBody)   
 }
 
 addmeetingStatement(reqBody):any{
   return this.restfulWebService.post(`disciplinary-statement-save`,reqBody)   
 }
 
 deletemeetingStatement(reqBody):any{
   return this.restfulWebService.delete(`disciplinary-statement-delete/${JSON.stringify(reqBody)}`)   
 }

deleteDisciplinaryAction(reqBody):any{
   return this.restfulWebService.delete(`delete-disciplinary-action/${reqBody}`)   
  
}
 
 

}
