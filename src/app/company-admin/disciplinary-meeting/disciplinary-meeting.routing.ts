import {  ModuleWithProviders  } from '@angular/core';
import {  RouterModule,Routes  } from '@angular/router';

import { DisciplinaryMeetingService } from './disciplinary-meeting.service';
import { CreateDisciplinaryMeetingComponent } from './create-disciplinary-meeting/create-disciplinary-meeting.component';
import { EmployeeInviteThirdPartyComponent } from './employee-invite-third-party/employee-invite-third-party.component';
import { FormalDisciplinaryMeetingComponent } from './formal-disciplinary-meeting/formal-disciplinary-meeting.component';
import { ResultsComponent } from './results/results.component';
import { DisciplinaryMeetingViewCaComponent } from './disciplinary-meeting-view-ca/disciplinary-meeting-view-ca.component';

export const DisciplinaryMeetingPagesRoutes: Routes = [
    {
        path: '',
        canActivate:[],
        children: [
           
           { path: 'create/:userID', component: CreateDisciplinaryMeetingComponent},
           { path: 'create/:userID/pip/:pipID', component: CreateDisciplinaryMeetingComponent},
           { path: 'create/:userID/bpip/:bpipID', component: CreateDisciplinaryMeetingComponent},
           { path: 'update/:damID', component: CreateDisciplinaryMeetingComponent},//disciplinary action meating ID
           { path: 'view/:damID', component: ResultsComponent},//disciplinary action meating ID         
           { path: 'employee/invite-thirdparty', component: EmployeeInviteThirdPartyComponent},
           { path: 'formal', component: FormalDisciplinaryMeetingComponent}, 
           { path: 'result', component: ResultsComponent}, 
         ]
    }
];

export const DisciplinaryMeetingRoutingModule = RouterModule.forChild(DisciplinaryMeetingPagesRoutes);
