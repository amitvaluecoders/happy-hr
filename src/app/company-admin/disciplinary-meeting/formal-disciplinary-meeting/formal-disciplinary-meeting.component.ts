import { Component, OnInit } from '@angular/core';
import { APPCONFIG } from '../../../config';
import { IDatePickerConfig } from 'ng2-date-picker';

@Component({
  selector: 'app-formal-disciplinary-meeting',
  templateUrl: './formal-disciplinary-meeting.component.html',
  styleUrls: ['./formal-disciplinary-meeting.component.scss']
})
export class FormalDisciplinaryMeetingComponent implements OnInit {
  
  public progress;
  birthday: any;
  config: IDatePickerConfig = {};

  public invitelistCollapse = false;

  constructor() { 
     this.config.locale = "en";
    this.config.format = "YYYY-MM-DD hh:mm A";
    this.config.showMultipleYearsNavigation = true;
    this.config.weekDayFormat = 'dd';
    this.config.disableKeypress = true;
    this.config.monthFormat="MMM YYYY";
  }

  ngOnInit() {
     APPCONFIG.heading="Formal disciplinary meeting";
     this.progressBarSteps();
  }
  progressBarSteps() {
      this.progress={
          progress_percent_value:75, //progress percent
          total_steps:4,
          current_step:3,
          circle_container_width:20, //circle container width in percent
          steps:[
               {num:1,data:"Create disciplinary meeting"},
               {num:2,data:"Employee: Invite third party"},
               {num:3,data:"Formal disciplinary meeting",active:true},
               {num:4,data:"Result"},
          ]  
       }         
   }

}
