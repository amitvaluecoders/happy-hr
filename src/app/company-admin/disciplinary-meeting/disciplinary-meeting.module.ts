import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DisciplinaryMeetingRoutingModule } from './disciplinary-meeting.routing';
import { DisciplinaryMeetingService } from './disciplinary-meeting.service';
import { CreateDisciplinaryMeetingComponent } from './create-disciplinary-meeting/create-disciplinary-meeting.component';
import { EmployeeInviteThirdPartyComponent } from './employee-invite-third-party/employee-invite-third-party.component';
import { FormalDisciplinaryMeetingComponent } from './formal-disciplinary-meeting/formal-disciplinary-meeting.component';
import { ResultsComponent } from './results/results.component';
import { MaterialModule } from '@angular/material';
import {FormsModule} from '@angular/forms'
import { SharedModule } from '../../shared/shared.module';
import { AngularMultiSelectModule } from 'angular2-multiselect-checkbox-dropdown/angular2-multiselect-dropdown';
import { DisciplinaryMeetingViewCaComponent } from './disciplinary-meeting-view-ca/disciplinary-meeting-view-ca.component';
import { CreateDisciplinaryActionStatementPopupComponent } from './results/create-disciplinary-action-statement-popup/create-disciplinary-action-statement-popup.component';
import { DisciplinaryMeetingGuardService } from './disciplinary-meeting-guard.service'; 
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    MaterialModule,
    DisciplinaryMeetingRoutingModule,
    SharedModule,
    AngularMultiSelectModule
  ],
  declarations: [CreateDisciplinaryMeetingComponent, EmployeeInviteThirdPartyComponent, FormalDisciplinaryMeetingComponent, ResultsComponent, DisciplinaryMeetingViewCaComponent, CreateDisciplinaryActionStatementPopupComponent],
  providers:[DisciplinaryMeetingService,DisciplinaryMeetingGuardService],
  entryComponents:[CreateDisciplinaryActionStatementPopupComponent,]
})
export class DisciplinaryMeetingModule { }
