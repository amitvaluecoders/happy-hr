import { Routes, RouterModule }  from '@angular/router';
import { MasterOnBoardingChecklistComponent } from './master-on-boarding-checklist/master-on-boarding-checklist.component';
import { MasterOffBoardingChecklistComponent } from './master-off-boarding-checklist/master-off-boarding-checklist.component';
import { CreateCategoryComponent } from './create-category/create-category.component';
import { CreateListComponent } from './create-list/create-list.component';
import { EditMasterOnBoardCheckListComponent } from './edit-master-on-board-check-list/edit-master-on-board-check-list.component';
import { ViewMasterOnBoardChecklistComponent } from './view-master-on-board-checklist/view-master-on-board-checklist.component';
import { EditMasterOffBoardCheckListComponent } from './edit-master-off-board-check-list/edit-master-off-board-check-list.component';
import { ViewMasterOffBoardChecklistComponent } from './view-master-off-board-checklist/view-master-off-board-checklist.component';

export const ChecklistPagesRoutes: Routes = [
    {
        path: '',
        children: [
            { path: '', component: MasterOnBoardingChecklistComponent },
            { path: 'offBoarding', component: MasterOffBoardingChecklistComponent },
            { path: 'create-list', component: CreateListComponent },
            { path: 'add-category', component: CreateCategoryComponent },
            { path: 'edit/on-boarding/:id', component: EditMasterOnBoardCheckListComponent },
            { path: 'view/on-boarding/:id', component: ViewMasterOnBoardChecklistComponent },
            { path: 'edit/off-boarding/:id', component: EditMasterOffBoardCheckListComponent },
            { path: 'view/off-boarding/:id', component: ViewMasterOffBoardChecklistComponent }
          
        ]
    }
];

export const ChecklistPagesRoutingModule = RouterModule.forChild(ChecklistPagesRoutes);
