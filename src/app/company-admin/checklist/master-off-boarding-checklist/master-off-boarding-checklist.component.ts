import {APPCONFIG } from '../../../config';
import { AddItemComponent } from '../add-item/add-item.component';
import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { CoreService} from '../../../shared/services';
import { ChecklistService } from '../checklist.service';
import { MdSnackBar, MdDialog, MdDialogRef } from '@angular/material';
import { CreateListComponent } from '../create-list/create-list.component';
import { ConfirmService } from '../../../shared/services/confirm.service';

@Component({
  selector: 'app-master-off-boarding-checklist',
  templateUrl: './master-off-boarding-checklist.component.html',
  styleUrls: ['./master-off-boarding-checklist.component.scss']
})
export class MasterOffBoardingChecklistComponent implements OnInit {

  collapse = {};
  
  
    ngOnInit() {
      this.getCheckListDetails();
      APPCONFIG.heading="Master off boarding checklist"
    }
    
  public collapsebox=[];
    public opencollapse;
    
    public checkList:any;
    public typeID='';
    public isProcessing=false;
    public addColumnProcessing=false;
    public oldChecklist=[];
    permission;
    constructor(public coreService:CoreService,
                    public checkListService:ChecklistService,
                    public snackBar: MdSnackBar,
                    public confirmService: ConfirmService,
                    public viewContainerRef: ViewContainerRef, 
                    public dialog: MdDialog,) { }
  
    
  getCheckListDetails(){
    this.isProcessing=true;
    //let reqBody=JSON.stringify({type:this.checkList.type});
    this.checkListService.getCompanyCheckListOffBoardDetails().subscribe(
      d=>{
        if(d.code=="200"){
          console.log("data",d)
          this.checkList=d.data;
          this.oldChecklist=JSON.parse(JSON.stringify(this.checkList));
          this.typeID=d.data.companyChecklistID;
          this.isProcessing=false;
          console.log("typeIDDD",this.typeID)
          console.log("checkList",this.checkList);
        }else {this.isProcessing=false;this.coreService.notify("Unsuccessful",d.message,0)}
      },
      e=>{this.isProcessing=false;this.coreService.notify("Unsuccessful",e.message,0)})
  }
  
  // onAddNewCategory(){
  //     let dialogRef = this.dialog.open(CreateNewCategoryComponent, {
  //           width: '400px'
  //       });
  //       dialogRef.afterClosed().subscribe(result => {
  //           if (result) {
  //             //this.checkList.categories.push({id:this.checkList.categories.length+1,name:result,items:[],coloumns:[]})
  //           }
  //       });
  // }
  
  onDeleteCheckListCategory(categoryID){
    this.confirmService.confirm(
      this.confirmService.tilte,
       this.confirmService.message,
       this.viewContainerRef
     )
     .subscribe(res => {
       let result = res;
       if (result) {
         this.isProcessing=true;
    this.checkListService.deleteCheckListCategory(categoryID).subscribe(
      d=>{
        if(d.code=="200"){
        // for(let c in  this.checkList.categories){
        //   if(this.checkList.categories[c].id==categoryId) {
        //     this.checkList.categories[c].coloumns = this.checkList.categories[c].coloumns.filter(function(element) {
        //       if(element.id != columnID)  return element;
        //     });;
        //   } 
        // }
        
        
        //     this.checkList=d.data;
  
           this.getCheckListDetails();
        
          this.coreService.notify("Successful",d.message,1)
        }else this.coreService.notify("Unsuccessful",d.message,0)
      },
      e=>{this.isProcessing=false;this.coreService.notify("Unsuccessful","error while deleteing column",0)})
    }})
  }
  
  // on deletiong column
  onDeleteColumns(columnID){
    
    // let reqBody=JSON.stringify({checkListType:this.checkList.type,categoryId:categoryId,columnId:columnID}) 
    this.confirmService.confirm(
      this.confirmService.tilte,
       this.confirmService.message,
       this.viewContainerRef
     )
     .subscribe(res => {
       let result = res;
       if (result) {  
    this.checkListService.deleteCheckListCategoryColumn(columnID).subscribe(
      d=>{
        if(d.code=="200"){
        // for(let c in  this.checkList.categories){
        //   if(this.checkList.categories[c].id==categoryId) {
        //     this.checkList.categories[c].coloumns = this.checkList.categories[c].coloumns.filter(function(element) {
        //       if(element.id != columnID)  return element;
        //     });;
        //   } 
        // }
  
         this.getCheckListDetails();
        
          this.coreService.notify("Successful",d.message,1)
        }else this.coreService.notify("Unsuccessful",d.message,0)
      },
      e=>this.coreService.notify("Unsuccessful",e.message,0))
    }})
  }
  
  // on deleting list item
  onDeleteListitems(rowID){
  
    //let reqBody=JSON.stringify({checkListType:this.checkList.type,categoryId:categoryId,itemId:itemId}) 
    this.confirmService.confirm(
              this.confirmService.tilte,
               this.confirmService.message,
               this.viewContainerRef
             )
             .subscribe(res => {
               //let result = res;
               if (res) {
                 this.isProcessing=true;
      this.checkListService.deleteCheckListCategoryItem(rowID).subscribe(
      d=>{
        if(d.code=="200"){
             //this.checkList=d.data;
             this.getCheckListDetails();
            // for(let c in this.checkList.categories){
            //   if(this.checkList.categories[c].id==categoryId) {
            //     this.checkList.categories[c].items = this.checkList.categories[c].items.filter(function(element) {
            //       if(element.id != itemId)  return element;
            //     });
            //   } 
            // }
  
            // this.getCheckListDetails();
  
          this.coreService.notify("Successful",d.message,1)
        }else this.coreService.notify("Unsuccessful",d.message,0)
      },
      e=>{this.isProcessing=false;this.coreService.notify("Unsuccessful",e.message,0)})
    }})
  }
  
  // for adding columns in category
  // onAddColumn(categoryId){  
  //   console.log("category ID",categoryId);
  //    let dialogRef = this.dialog.open(ColumnComponent, {
  //           width: '600px'
  //         });
  //         dialogRef.componentInstance.type="1";
  //         dialogRef.componentInstance.categoryId=categoryId;
  //         //this.getCheckListDetails();
  //         dialogRef.afterClosed().subscribe(result => {
  //             if(result){
  //               this.addColumnProcessing=true;
  //               this.getCheckListDetails();
  //               // for(let c in  this.checkList['columnsItems']){
  //               //     if(this.checkList['columnsItems'][c]['companyChecklistCategoryID']==categoryId) {
  //               //       this.checkList['columnsItems'][c].push({columnID:this.checkList['columnsItems'][c]['columnID'],title:result});
  //               //     } 
  //               //   }
  //                 this.addColumnProcessing=false;
  //                 console.log("column checklist",this.checkList);
  //             }
  //         });
  // }
  
  onAddItem(categoryId){
     let dialogRef = this.dialog.open(CreateListComponent, {
            width: '600px'
          });
          dialogRef.componentInstance.type="onboard";
          dialogRef.componentInstance.categoryId=categoryId;
        //  dialogRef.componentInstance.item=rowData;
          dialogRef.afterClosed().subscribe(result => {
              if(result){
                console.log("RESULTTTTTT",result);
                for(let c of this.checkList["categories"]){
                  if(c["companyChecklistCategoryID"]==result["companyChecklistCategoryID"]){
                console.log("TRUEUEUUEUE");c["rows"].push(result);
                console.log("AFTER PUSH",this.checkList)
                  }
              }
                //console.log("AFTER PUSH",this.checkList)
              }
          });
  }
  
  onEditItem(categoryId,rowData){
    let dialogRef = this.dialog.open(CreateListComponent, {
           width: '600px'
         });
         dialogRef.componentInstance.type="onboard";
         dialogRef.componentInstance.categoryId=categoryId;
         dialogRef.componentInstance.rowData=rowData;
         dialogRef.afterClosed().subscribe(result => {
             if(result){
               console.log("RESULTTTTTT",result);
                for(let c of this.checkList["categories"]){
                  if(c["companyChecklistCategoryID"]==result["companyChecklistCategoryID"]){
                for(let r of c["rows"]){
                if(r["companyChecklistCategoryRowID"]==result["companyChecklistCategoryRowID"]){
                r=result;
                console.log("RRRRRRRRR",r);
                console.log("AFTER PUSH",this.checkList)
                  }
                }
                  }
              }
            //  /  this.checkList["categories"]["rows"]["companyChecklistCategoryRowID"]
               //this.getCheckListDetails();
               // for(let c in  this.checkList.categories){
               //     if(this.checkList.categories[c].id==categoryId) {
               //       this.checkList.categories[c].items.push({id:this.checkList.categories[c].items.length+1,name:result});
               //     } 
               //   }
             }
         });
  }
  
  onHeadingChange(headData){
    //console.log("cc",categoryId,"dd",categoryName);
    // this.checkListService.updateCategoryHeading()
    let category={
      companyChecklistID:this.typeID,
      companyChecklistCategoryID:headData.companyChecklistCategoryID,
      title:headData.title
  }
    // let category={
    //   title: headData.title,
    //   sortOrder: 1,
    //   companyChecklistID:this.typeID,
    //   companyChecklistCategoryID:headData.companyChecklistCategoryID
    // }
    if(headData.title){
    console.log("CATEGORYYYY",category)
    this.checkListService.addCategory(category).subscribe( d=>{
        if(d.code=="200"){
         this.oldChecklist=JSON.parse(JSON.stringify(this.checkList));        
         this.coreService.notify("Successful",d.message,1);
         
        }else this.coreService.notify("Unsuccessful",d.message,0)
      },
      e=>this.coreService.notify("Unsuccessful",e.message,0))
    }else{
      for(let c of this.oldChecklist["categories"]){
        if(headData.companyChecklistCategoryID==c.companyChecklistCategoryID){
         // categoryName=c.title;
         headData.title=c.title;
      this.coreService.notify("Unsuccessful","Category title is required",0)
          
        }
      }
      // this.checkList=this.oldChecklist;
    }
  }
  
  onColumnChange(columnData){
  //   let column={
  //         title: columnName,
  //         sortOrder: 1,
  //         companyChecklistCategoryID:categoryId,
  //         companyChecklistCategoryColumnID:columnId
  // }
  
  let column={
          columnID:columnData.columns.columnID,
          columnA:columnData.columns.columnA,
          columnB:columnData.columns.columnB
  }
  if(columnData.columns.columnA&&columnData.columns.columnB){
    this.checkListService.addCheckListCategoryColumn(column).subscribe(
          d=>{
            if(d.code="200") {
          this.oldChecklist=JSON.parse(JSON.stringify(this.checkList));
              
              this.coreService.notify("Successful",d.message,1);
            }
            else this.coreService.notify("Unsuccessful",d.message,0);
          },
          e=>this.coreService.notify("Unsuccessful",e.message,0))
        }else{
          
          for(let c of this.oldChecklist["categories"]){
            if(c["columns"]){
        if(columnData.columns.columnID==c.columns.columnID){
         // categoryName=c.title;
         columnData.columns.columnA=c.columns.columnA;
         columnData.columns.columnB=c.columns.columnB;
         
      this.coreService.notify("Unsuccessful","Column title is required",0)
        }
        }
      }
        }
  
  
  }
  
  onItemChange(categoryID,rowData){
    let item={
          companyChecklistCategoryID:categoryID,
          columnA:rowData.columnA,
          columnB:"",
          columnC:"",
          companyChecklistCategoryRowID:rowData.companyChecklistCategoryRowID
  }
  if(rowData.columnA){
     this.checkListService.addCheckListCategoryItem(item).subscribe(
        d=>{
          if(d.code="200"){
          this.oldChecklist=JSON.parse(JSON.stringify(this.checkList));
            
           // this.dialogRef.close(this.item.title);
            this.coreService.notify("Successful",d.message,1)
          }
          else this.coreService.notify("Unsuccessful",d.message,0)
        },
        e=>this.coreService.notify("Unsuccessful",e.message,0)
        )   
  }else{
    
          for(let category of this.oldChecklist["categories"]){
            if(category["rows"].length){
            for(let r of category["rows"] ){
              console.log("INSIDE category ROWS",r)
              console.log("ROW DATATAATTATAT",rowData);
              console.log("CONDITION",(rowData.companyChecklistCategoryRowID==r.companyChecklistCategoryRowID))
        if(rowData.companyChecklistCategoryRowID==r.companyChecklistCategoryRowID){
         // categoryName=c.title;
         console.log("TRUEUUEU CONDition")
         rowData.columnA=r.columnA;       
      this.coreService.notify("Unsuccessful","Item title is required",0)
        }
            }
        }
      }
  }
  }

}
