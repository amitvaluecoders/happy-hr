import { Component, OnInit } from '@angular/core';
import { ChecklistService } from '../checklist.service';
import { MdSnackBar, MdDialog, MdDialogRef } from '@angular/material';
import { CoreService} from '../../../shared/services';

@Component({
  selector: 'app-create-list',
  templateUrl: './create-list.component.html',
  styleUrls: ['./create-list.component.scss']
})
export class CreateListComponent implements OnInit {
 public item={
  companyChecklistCategoryID:null,
  companyChecklistCategoryRowID:"",
  columnA:"",
  columnB:"",
  columnC: ""
}

  public categoryId:String;
  public rowData:any;
  public type:String;
  public listName:String;
  public isProcessing=false;
  title="Create new question";
  buttonText="Add";

  constructor(public dialog: MdDialog,public checkListService: ChecklistService,
              public coreService:CoreService,
              public dialogRef: MdDialogRef<CreateListComponent>) { }

  ngOnInit() {
   if(this.categoryId) {this.item.companyChecklistCategoryID=this.categoryId;}
   if(this.rowData) {
    this.item.columnA=this.rowData.columnA;
    this.item.columnB=this.rowData.columnB;
    this.item.columnC=this.rowData.columnC;
    this.item.companyChecklistCategoryRowID=this.rowData.companyChecklistCategoryRowID;
     this.title="Edit question";
     this.buttonText="Save";
   }
  }

  // for adding list item  
  onAdd(isvalid){
    if(isvalid){
      this.isProcessing=true;
      console.log("item",this.item);
    this.checkListService.addCheckListCategoryItem(this.item).subscribe(
      d=>{
        if(d.code="200"){
          this.isProcessing=false; 
          console.log("data",d);         
          this.dialogRef.close(d.data);
          this.coreService.notify("Successful",d.message,1)
        }
        else this.coreService.notify("Unsuccessful",d.message,0)
      },
      e=>{
        this.isProcessing=false;        
        this.coreService.notify("Unsuccessful",e.message,0)}
      )
    }
  }

}
