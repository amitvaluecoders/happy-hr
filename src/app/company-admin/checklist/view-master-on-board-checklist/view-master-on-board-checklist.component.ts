import { APPCONFIG } from '../../../config';
import { AddItemComponent } from '../add-item/add-item.component';
import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { CoreService } from '../../../shared/services';
import { ChecklistService } from '../checklist.service';
import { MdSnackBar, MdDialog, MdDialogRef } from '@angular/material';
import { CreateListComponent } from '../create-list/create-list.component';
import { ConfirmService } from '../../../shared/services/confirm.service';
import { Router, ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-view-master-on-board-checklist',
  templateUrl: './view-master-on-board-checklist.component.html',
  styleUrls: ['./view-master-on-board-checklist.component.scss']
})
export class ViewMasterOnBoardChecklistComponent implements OnInit {

  public isProcessing = false;
  public checkList: any;
  public oldChecklist = [];
  // public typeID = '';
  public collapse = {};
  public actions=[
    {value:"Pending",action:"Pending"},
    {value:"Not Applicable",action:"Not Applicable"},
    {value:"Complete",action:"Complete"}
  ];

  constructor(public coreService: CoreService,
    public checkListService: ChecklistService,
    public snackBar: MdSnackBar,
    public confirmService: ConfirmService,
    public viewContainerRef: ViewContainerRef,
    public dialog: MdDialog,
    private router:Router,
    private activatedRoute:ActivatedRoute
  ) { }

  ngOnInit() {
    
    if(this.activatedRoute.snapshot.params['id']){
      this.getCheckListDetail(this.activatedRoute.snapshot.params['id']);
    }
    // this.getCheckListDetails();
    APPCONFIG.heading = "On boarding checklist"
  }

  // getCheckListDetails() {
  //   this.isProcessing = true;
  //   //let reqBody=JSON.stringify({type:this.checkList.type});
  //   this.checkListService.getCompanyCheckListOnBoardDetails().subscribe(
  //     d => {
  //       if (d.code == "200") {
  //         this.checkList = d.data;
  //         this.oldChecklist = JSON.parse(JSON.stringify(this.checkList));
  //         this.typeID = d.data.companyChecklistID;
  //         this.isProcessing = false;
  //       } else { this.isProcessing = false; this.coreService.notify("Unsuccessful", d.message, 0) }
  //     },
  //     e => { this.isProcessing = false; this.coreService.notify("Unsuccessful", e.message, 0) })
  // }

  getCheckListDetail(listId){
    this.isProcessing = true;
    this.checkListService.getParticularOnBoardList(listId)
      .subscribe(
        d => {
        if (d.code == "200") {
          this.checkList = d.data;
          this.oldChecklist = JSON.parse(JSON.stringify(this.checkList));
          // this.typeID = d.data.companyChecklistID;
          this.isProcessing = false;
        } else { this.isProcessing = false; this.coreService.notify("Unsuccessful", d.message, 0) }
      },
      e => { this.isProcessing = false; this.coreService.notify("Unsuccessful", e.message, 0) }
      );
  }

}
