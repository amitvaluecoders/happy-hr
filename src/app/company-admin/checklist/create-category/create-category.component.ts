import { Component, OnInit } from '@angular/core';
import {APPCONFIG } from '../../../config';
import { MdSnackBar, MdDialog, MdDialogRef } from '@angular/material';
import { ChecklistService } from '../checklist.service';
import { CoreService} from '../../../shared/services';
@Component({
  selector: 'app-create-category',
  templateUrl: './create-category.component.html',
  styleUrls: ['./create-category.component.scss']
})
export class CreateCategoryComponent implements OnInit {

  public types=[];
  public category={
      companyChecklistID:"",
      companyChecklistCategoryID:"",
      title:""
}
  

  public isProcessing=false;
  public addProcessing=false;

  constructor(
              public checkListService:ChecklistService,
              public coreServices:CoreService) { }

  ngOnInit() {
    this.getCategoryTypes();
    APPCONFIG.heading="Create category"
  }

  getCategoryTypes(){
    this.isProcessing=true;
  this.checkListService.getCategoryTypes().subscribe( d=>{
      if(d.code=="200"){
        this.types=d.data;
        console.log("Types",this.types);
       this.isProcessing=false;
      }else {this.isProcessing=false;this.coreServices.notify("Unsuccessful",d.message,0)}
    },
    e=>{this.isProcessing=false;this.coreServices.notify("Unsuccessful",e.message,0)}) 
  }

  onSave(isvalid,form){
    this.addProcessing=true
    if(isvalid){
      console.log("catrgory on ",this.category)
      this.checkListService.addCategory(this.category).subscribe( d=>{
      if(d.code=="200"){
        form.reset();
        this.addProcessing=false;
       this.coreServices.notify("Successful",d.message,1);
      // this.dialogRef.close(this.category.title);
       
      }else {this.addProcessing=false;this.coreServices.notify("Unsuccessful",d.message,0)}
    },
    e=>{this.addProcessing=false;this.coreServices.notify("Unsuccessful",e.message,0)})      
    }
  }

  onCancel(form){
    form.reset();
  }
}
