import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditMasterOffBoardCheckListComponent } from './edit-master-off-board-check-list.component';

describe('EditMasterOffBoardCheckListComponent', () => {
  let component: EditMasterOffBoardCheckListComponent;
  let fixture: ComponentFixture<EditMasterOffBoardCheckListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditMasterOffBoardCheckListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditMasterOffBoardCheckListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
