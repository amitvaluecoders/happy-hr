import { APPCONFIG } from '../../../config';
import { AddItemComponent } from '../add-item/add-item.component';
import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { CoreService } from '../../../shared/services';
import { ChecklistService } from '../checklist.service';
import { MdSnackBar, MdDialog, MdDialogRef } from '@angular/material';
import { CreateListComponent } from '../create-list/create-list.component';
import { ConfirmService } from '../../../shared/services/confirm.service';
import { Router, ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-edit-master-off-board-check-list',
  templateUrl: './edit-master-off-board-check-list.component.html',
  styleUrls: ['./edit-master-off-board-check-list.component.scss']
})
export class EditMasterOffBoardCheckListComponent implements OnInit {

  collapse = {};
  ngOnInit() {
    if(this.activatedRoute.snapshot.params['id']){
      this.getCheckListDetail(this.activatedRoute.snapshot.params['id']);
    }
    APPCONFIG.heading = "Off boarding checklist"
  }

  public collapsebox = [];
  public opencollapse;

  public checkList: any;
  public typeID = '';
  public isProcessing = false;
  public addColumnProcessing = false;
  public oldChecklist = [];
  permission;
  public actions=[
    {value:"Not Applicable",action:"Not Applicable"},
    {value:"Complete",action:"Complete"}
  ];
  constructor(public coreService: CoreService,
    public checkListService: ChecklistService,
    public snackBar: MdSnackBar,
    public confirmService: ConfirmService,
    public viewContainerRef: ViewContainerRef,
    public dialog: MdDialog,
    private router:Router,
    private activatedRoute:ActivatedRoute 
  ) { }
  
  getCheckListDetail(listId){
    console.log(listId);
    this.isProcessing = true;
    this.checkListService.getParticularOnBoardList(listId)
      .subscribe(
        d => {
          console.log(d)
        if (d.code == "200") {
          this.checkList = d.data;
          this.oldChecklist = JSON.parse(JSON.stringify(this.checkList));
          // this.typeID = d.data.companyChecklistID;
          this.isProcessing = false;
        } else { this.isProcessing = false; this.coreService.notify("Unsuccessful", d.message, 0) }
      },
      e => { this.isProcessing = false; this.coreService.notify("Unsuccessful", e.message, 0) }
      );
  }


  //ON SAVE LIST CHANGES
  onSave(){
    let saveDta = [];
    this.checkList.forEach(d => {
      if(d.categories && d.categories.columns && d.categories.columns.rows.length){
      for(let k of d.categories.columns.rows){
           var data = {
        employeeChecklistResponceID: k.employeeChecklistResponseID,
        columnB: (k.columnB)?k.columnB : '',
        columnC: (k.columnC)?k.columnC : ''
      };
      saveDta.push(data);
      }
      }
     
    });
    console.log('data...',saveDta);    
    //if(saveDta.length == this.checkList.length){
      var sData={"responce":saveDta}
      this.checkListService.saveBoardChanges(sData)
        .subscribe(data=>{
          console.log(data)
          if(data.code ==200){
            this.coreService.notify("Successful", data.message, 1);
            this.router.navigate(['/app/company-admin/listing-page/off-boarding-listing']);
          }
        },
          error=>{
            console.log(error)
            this.coreService.notify("Error", error.message, 0)
          }
        );
    }
    
  //}

}
