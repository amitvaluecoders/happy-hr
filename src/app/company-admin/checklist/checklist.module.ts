import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChecklistPagesRoutingModule } from './checklist.routing';
import { ChecklistService } from './checklist.service';
import { MasterOnBoardingChecklistComponent } from './master-on-boarding-checklist/master-on-boarding-checklist.component';
import { MasterOffBoardingChecklistComponent } from './master-off-boarding-checklist/master-off-boarding-checklist.component';
import { CreateCategoryComponent } from './create-category/create-category.component';
import { CreateListComponent } from './create-list/create-list.component';
import { AddItemComponent } from './add-item/add-item.component';
import { MaterialModule } from '@angular/material';
import { InlineEditorModule} from 'ng2-inline-editor';
import { FormsModule } from '@angular/forms';
import { SharedModule } from '../../shared/shared.module'; 
import { ChecklistGuardService } from './checklist-guard.service';
import { EditMasterOnBoardCheckListComponent } from './edit-master-on-board-check-list/edit-master-on-board-check-list.component';
import { EditMasterOffBoardCheckListComponent } from './edit-master-off-board-check-list/edit-master-off-board-check-list.component';
import { ViewMasterOffBoardChecklistComponent } from './view-master-off-board-checklist/view-master-off-board-checklist.component';
import { ViewMasterOnBoardChecklistComponent } from './view-master-on-board-checklist/view-master-on-board-checklist.component';

@NgModule({
  imports: [
    CommonModule,
    ChecklistPagesRoutingModule,
    MaterialModule,
    InlineEditorModule,FormsModule,
    SharedModule
  ],
  entryComponents: [AddItemComponent],
  declarations: [MasterOnBoardingChecklistComponent, MasterOffBoardingChecklistComponent, CreateCategoryComponent, CreateListComponent, AddItemComponent, EditMasterOnBoardCheckListComponent, EditMasterOffBoardCheckListComponent, ViewMasterOffBoardChecklistComponent, ViewMasterOnBoardChecklistComponent],
  providers:[ChecklistService,ChecklistGuardService]
})
export class ChecklistModule { }
