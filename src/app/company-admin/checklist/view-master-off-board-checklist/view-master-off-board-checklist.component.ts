import { APPCONFIG } from '../../../config';
import { AddItemComponent } from '../add-item/add-item.component';
import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { CoreService } from '../../../shared/services';
import { ChecklistService } from '../checklist.service';
import { MdSnackBar, MdDialog, MdDialogRef } from '@angular/material';
import { CreateListComponent } from '../create-list/create-list.component';
import { ConfirmService } from '../../../shared/services/confirm.service';
import { Router, ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-view-master-off-board-checklist',
  templateUrl: './view-master-off-board-checklist.component.html',
  styleUrls: ['./view-master-off-board-checklist.component.scss']
})
export class ViewMasterOffBoardChecklistComponent implements OnInit {

  public isProcessing = false;
  public checkList: any;
 // public rowId:any;
  public oldChecklist = [];
  // public typeID = '';
  public collapse = {};
  public actions=[
    {value:"Pending",action:"Pending"},
    {value:"Not Applicable",action:"Not Applicable"},
    {value:"Complete",action:"Complete"}
  ];

  constructor(public coreService: CoreService,
    public checkListService: ChecklistService,
    public snackBar: MdSnackBar,
    public confirmService: ConfirmService,
    public viewContainerRef: ViewContainerRef,
    public dialog: MdDialog,
    private router:Router,
    private activatedRoute:ActivatedRoute
  ) { }

  ngOnInit() {
    
    if(this.activatedRoute.snapshot.params['id']){
      this.getCheckListDetail(this.activatedRoute.snapshot.params['id']);
    }
    APPCONFIG.heading = "Off boarding checklist"
  }

  getCheckListDetail(listId){
    this.isProcessing = true;
    this.checkListService.getParticularOnBoardList(listId)
      .subscribe(
        d => {
        if (d.code == "200") {
          this.checkList = d.data;
          this.oldChecklist = JSON.parse(JSON.stringify(this.checkList));
          // this.typeID = d.data.companyChecklistID;
          this.isProcessing = false;
        } else { this.isProcessing = false; this.coreService.notify("Unsuccessful", d.message, 0) }
      },
      e => { this.isProcessing = false; this.coreService.notify("Unsuccessful", e.message, 0) }
      );
  }

  onDelete(rowId){
    this.confirmService.confirm(
      this.confirmService.tilte,
       this.confirmService.message,
       this.viewContainerRef
     )
     .subscribe(res => {
       let result = res;
       if (result) {
        this.isProcessing=true;

          this.checkListService.deleteCheckListCategoryItem(rowId).subscribe(
            res => {
              this.checkList['isProcessing'] = false;
              if (res.code == 200) {
                this.coreService.notify("Successful", res.message, 1);
                this.getCheckListDetail(this.activatedRoute.snapshot.params['id']);
              }
              else return this.coreService.notify("Unsuccessful", res.message, 0);
            },
            error => {
              this.checkList['isProcessing'] = false;
              this.coreService.notify("Unsuccessful", error.message, 0);
            }
          )
        }
      }
    )


  }



}
