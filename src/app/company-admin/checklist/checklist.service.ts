import { Injectable } from '@angular/core';
import { RestfulwebService } from '../../shared/services/restfulweb.service';
import { RestfulLaravelService } from '../../shared/services/restful-laravel.service';


@Injectable()
export class ChecklistService {

 constructor(
    private restfulWebService: RestfulwebService,
    private restfulLaravelService: RestfulLaravelService, 
  ){}


//  getCheckListDetails(reqBody){
//   return this.restfulWebService.get(`checklist/${reqBody}`);
//  }

// getCheckListDetails(reqBody){
//   return this.restfulWebService.get(`checklist/${reqBody}`);
//  }
getCategoryTypes(){
  return this.restfulLaravelService.get('get-checklist-list');
}

getCompanyCheckListOnBoardDetails(){
  return this.restfulLaravelService.get('onBoard-checklist');
 }

 getCompanyCheckListOffBoardDetails(){
  return this.restfulLaravelService.get('offBoard-checklist');
 }

//  addCategory(reqBody){
//   return this.restfulWebService.post(`addCheckListCategory`,reqBody);
//  }

addCategory(reqBody){
  return this.restfulLaravelService.post(`create-update-checklist-category`,reqBody);
 }

//  deleteCheckListCategoryColumn(reqBody){
//     return this.restfulWebService.delete(`checkListCategoryColumn/${reqBody}`);
//  }

 deleteCheckListCategoryColumn(reqBody){
 return this.restfulLaravelService.delete(`admin/master-checklist-category-delete/${reqBody}`);
 }

//  deleteCheckListCategoryItem(reqBody){
//    return this.restfulWebService.delete(`checkListCategoryItem/${reqBody}`);
//  }

 deleteCheckListCategoryItem(reqBody){
   return this.restfulLaravelService.delete(`delete-checklist-row/${reqBody}`);
 }

 deleteCheckListCategory(reqBody){
   return this.restfulLaravelService.delete(`delete-checklist-category/${reqBody}`);
 }

//  addCheckListCategoryColumn(reqBody){
//     return this.restfulWebService.post(`checkListCategoryColumn`,reqBody);
//  }

//  addCheckListCategoryColumn(reqBody){
//    console.log("REq BOdy column",reqBody)
//     return this.restfulLaravelService.post(`admin/master-checklist-category-column-addEdit`,reqBody);
//  }

addCheckListCategoryColumn(reqBody){
   console.log("REq BOdy column",reqBody)
    return this.restfulLaravelService.put(`update-checklist-category-coloumn`,reqBody);
 }

//  addCheckListCategoryItem(reqBody){
//    return this.restfulWebService.post(`checkListCategoryItem`,reqBody);
//  }

// addCheckListCategoryItem(reqBody){
//    return this.restfulLaravelService.post(`admin/master-checklist-category-row-first-column-addEdit`,reqBody);
//  }

 addCheckListCategoryItem(reqBody){
  return this.restfulLaravelService.post(`update-checklist-category-row`,reqBody);
} 

//GET PARTICULAR ON BOARD CHECKLIST DATA
getParticularOnBoardList(listId){
  return this.restfulLaravelService.get(`get-employee-checklist-response/${listId}`);
}

//SAVE ON BOARD CHANGES
saveBoardChanges(changes){
  return this.restfulLaravelService.post(`save-employee-manager-checklist-response`,changes);
}


}
