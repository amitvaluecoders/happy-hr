import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditMasterOnBoardCheckListComponent } from './edit-master-on-board-check-list.component';

describe('EditMasterOnBoardCheckListComponent', () => {
  let component: EditMasterOnBoardCheckListComponent;
  let fixture: ComponentFixture<EditMasterOnBoardCheckListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditMasterOnBoardCheckListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditMasterOnBoardCheckListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
