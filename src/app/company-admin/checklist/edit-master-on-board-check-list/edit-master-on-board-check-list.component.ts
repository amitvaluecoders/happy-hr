import { APPCONFIG } from '../../../config';
import { AddItemComponent } from '../add-item/add-item.component';
import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { CoreService } from '../../../shared/services';
import { ChecklistService } from '../checklist.service';
import { MdSnackBar, MdDialog, MdDialogRef } from '@angular/material';
import { CreateListComponent } from '../create-list/create-list.component';
import { ConfirmService } from '../../../shared/services/confirm.service';
import { Router, ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-edit-master-on-board-check-list',
  templateUrl: './edit-master-on-board-check-list.component.html',
  styleUrls: ['./edit-master-on-board-check-list.component.scss']
})
export class EditMasterOnBoardCheckListComponent implements OnInit {

  collapse = {};
  public checkList = [];
  public isProcessing = false;
  public isProcessingSave = false;
  public actions = ["Not Applicable", "Complete"];
  permission;

  constructor(public coreService: CoreService, public checkListService: ChecklistService, public snackBar: MdSnackBar, public confirmService: ConfirmService,
    public viewContainerRef: ViewContainerRef, public dialog: MdDialog, private router: Router, private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    APPCONFIG.heading = "On boarding checklist"
    if (this.activatedRoute.snapshot.params['id']) {
      this.getCheckListDetail(this.activatedRoute.snapshot.params['id']);
    }
  }

  getCheckListDetail(listId) {
    this.isProcessing = true;
    this.checkListService.getParticularOnBoardList(listId).subscribe(
      d => {
        this.isProcessing = false;
        if (d.code == "200") {
          this.checkList = d.data;
        }
        else return this.coreService.notify("Unsuccessful", d.message, 0);
      },
      e => {
        this.isProcessing = false;
        this.coreService.notify("Unsuccessful", e.message, 0)
      }
    );
  }

  //ON SAVE LIST CHANGES
  onSave() {
    let saveDta = [];
    this.checkList.forEach(d => {
      if(d.categories && d.categories.columns && d.categories.columns.rows.length){
      for(let k of d.categories.columns.rows){
           var data = {
        employeeChecklistResponceID: k.employeeChecklistResponseID,
        columnB: (k.columnB)?k.columnB : '',
        columnC: (k.columnC)?k.columnC : ''
      };
      saveDta.push(data);
      }
      }
     
    });

      let sData = { "responce": saveDta };
      this.isProcessingSave = true;
      this.checkListService.saveBoardChanges(sData).subscribe(
        data => {
          this.isProcessingSave = false;
          if (data.code == 200) {
            this.coreService.notify("Successful", data.message, 1);
            this.router.navigate(['/app/company-admin/listing-page/induction-onboarding-listing']);
          }
        },
        error => {
          this.isProcessingSave = false;
          this.coreService.notify("Error", error.message, 0)
        }
      );
    

  }
}
