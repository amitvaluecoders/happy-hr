import { Injectable } from '@angular/core';
import { CanActivate, Router, RouterStateSnapshot, ActivatedRouteSnapshot } from "@angular/router";
import { CoreService } from '../../shared/services/core.service';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';

@Injectable()
export class BehaviouralPerformanceImprovementPlanGuardService {

    public module = "bpip";

    constructor(private _core: CoreService, private router: Router) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        let url: string;
        if (route['_routeConfig']['path'])
            url = route['_routeConfig']['path']
        else url = state.url;
        if (this.checkHavePermission(url)) {
            return true;
        } else {
            this.router.navigate(['/extra/unauthorized']);
            return false;
        }
    }

    private checkHavePermission(path) {
        return true;
        // switch (path) {
        //     case "behavior-performance-improvement-listing":
        //         return this._core.getModulePermission(this.module, 'view');
        //     case "create/:id":
        //         return this._core.getModulePermission(this.module, 'add');
        //     case "training/:id":
        //         return this._core.getModulePermission(this.module, 'view');
        //     case "details/:id":
        //         return this._core.getModulePermission(this.module, 'view');
        //     case "update/:id":
        //         return this._core.getModulePermission(this.module, 'edit');
        //     case "create/:info/:id":
        //         return this._core.getModulePermission(this.module, 'add');
        //     default:
        //         return false;
        // }
    }
}


