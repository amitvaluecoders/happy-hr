import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditBpipNoteComponent } from './edit-bpip-note.component';

describe('EditBpipNoteComponent', () => {
  let component: EditBpipNoteComponent;
  let fixture: ComponentFixture<EditBpipNoteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditBpipNoteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditBpipNoteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
