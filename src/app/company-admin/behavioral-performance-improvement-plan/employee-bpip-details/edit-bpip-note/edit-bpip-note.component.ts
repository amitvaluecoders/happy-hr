import { Component, OnInit } from '@angular/core';
import { BehavioralPerformanceImprovementPlanService } from '../../behavioral-performance-improvement-plan.service';
import { CoreService } from '../../../../shared/services/core.service';
import { MdDialogRef } from '@angular/material';

@Component({
  selector: 'app-edit-bpip-note',
  templateUrl: './edit-bpip-note.component.html',
  styleUrls: ['./edit-bpip-note.component.scss']
})
export class EditBpipNoteComponent implements OnInit {

 public noteDetails:any;
  public isProcessing =false;
  public isButtonClicked=false;
  public mode:any;

  constructor( public bpipService: BehavioralPerformanceImprovementPlanService,
               public coreService: CoreService,public dialog: MdDialogRef<EditBpipNoteComponent>) { }

  ngOnInit() {
    
  }

  onUpdate(isValid){
    this.isButtonClicked=true;
    if(isValid){
      this.isProcessing=true;
     this.bpipService.updateDevelopmentPlanNotes(this.noteDetails).subscribe(
          d => {
            this.isProcessing = false;  
              if (d.code == "200"){
                this.coreService.notify("Successful", d.message, 1);
                this.dialog.close({data:d.data,mode:this.mode});
              } 
              else this.coreService.notify("Unsuccessful", d.message, 0);
          },
          error => {
            this.coreService.notify("Unsuccessful", "Error while updating.", 0), 
            this.isProcessing = false;  
          }
      )
  }
  }


}
