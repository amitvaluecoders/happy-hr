import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployeeBpipDetailsComponent } from './employee-bpip-details.component';

describe('EmployeeBpipDetailsComponent', () => {
  let component: EmployeeBpipDetailsComponent;
  let fixture: ComponentFixture<EmployeeBpipDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmployeeBpipDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployeeBpipDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
