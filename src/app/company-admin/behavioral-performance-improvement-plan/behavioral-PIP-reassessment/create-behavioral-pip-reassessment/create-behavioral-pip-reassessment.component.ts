import { Component, OnInit } from '@angular/core';
import { APPCONFIG } from '../../../../config';
import { Router} from '@angular/router';

@Component({
  selector: 'app-create-behavioral-pip-reassessment',
  templateUrl: './create-behavioral-pip-reassessment.component.html',
  styleUrls: ['./create-behavioral-pip-reassessment.component.scss']
})
export class CreateBehavioralPIPReassessmentComponent implements OnInit {

   public progress;

  dropdownListEmployees = [];
  selectedItems = [];
  inviteSelectedItems = [];  
  dropdownSettings = {};
  thirdPartyList = [];
  selectedParty = [];
  
  invitedropdownListEmployees = [];
  invitedropdownSettings = {};

    public invitelistCollapse = false;
    public originalPIPToggle = false;

   constructor(public router:Router ) { }

  ngOnInit() {

     APPCONFIG.heading="Re-assess behaviour performance improvement plan";
     this.progressBarSteps();

     this.dropdownListEmployees = [
            { "id": 1, "itemName": "John", "img": "g1.jpg" },
            { "id": 2, "itemName": "Raj", "img": "eway.png" },
            { "id": 3, "itemName": "Peter", "img": "g1.jpg" },
            { "id": 4, "itemName": "Roy", "img": "g1.jpg" },
        ];

        this.invitedropdownListEmployees = [
            { "id": 1, "itemName": "John", "img": "g1.jpg" },
            { "id": 2, "itemName": "Raj", "img": "eway.png" },
            { "id": 3, "itemName": "Peter", "img": "g1.jpg" },
            { "id": 4, "itemName": "Roy", "img": "g1.jpg" },
        ];
      
     this.dropdownSettings = {
        singleSelection: false, 
        text:"Select Manager",
        enableSearchFilter: true,
        classes:"myclass"
     }; 
    this.invitedropdownSettings = {
        singleSelection: false, 
        text:"Select Third Party",
        enableSearchFilter: true,
        classes:"myclass"
    };
    $('angular2-multiselect:eq(0) .list-area ul').addClass('listItems');
    $('angular2-multiselect:eq(1) .list-area ul').addClass('inviteListItems');
 }

ngAfterViewInit() {
      this.invitedropdownListEmployees.filter(function (item, i) {
            $('.inviteListItems li').eq(i).prepend(`<img src="assets/images/${item.img}" alt="" />`);
            $('.inviteListItems li').eq(i).children('label').append('<span>' + item.role + '</span>');
        });
        this.dropdownListEmployees.filter(function (item, i) {
            $('.listItems li').eq(i).prepend(`<img src="assets/images/${item.img}" alt="" />`);
            $('.listItems li').eq(i).children('label').append('<span>' + item.role + '</span>');
        });
       
        
    }

    onItemSelect(item: any) {
        console.log(item);
        console.log(this.selectedItems);
    }
    OnItemDeSelect(item: any) {
        console.log(item);
        console.log(this.selectedItems);
    }
    onSelectAll(items: any) {
        console.log(items);
    }
    onDeSelectAll(items: any) {
        console.log(items);
    }
  
progressBarSteps() {
      this.progress={
          progress_percent_value:25, //progress percent
          total_steps:4,
          current_step:1,
          circle_container_width:25, //circle container width in percent
          steps:[
              // {num:1,data:"Select Role"},
               {num:1,data:"Re-Assess BPIP",active:true},
               {num:2,data:"Training"},
               {num:3,data:"Re-Assessment"},
               {num:4,data:"Result"}
          ]  
       }         
  }

}
