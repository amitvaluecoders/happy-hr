import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateBehavioralPIPReassessmentComponent } from './create-behavioral-pip-reassessment.component';

describe('CreateBehavioralPIPReassessmentComponent', () => {
  let component: CreateBehavioralPIPReassessmentComponent;
  let fixture: ComponentFixture<CreateBehavioralPIPReassessmentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateBehavioralPIPReassessmentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateBehavioralPIPReassessmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
