import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { MdSnackBar, MdDialog, MdDialogRef } from '@angular/material';
import { APPCONFIG } from '../../../../config';

@Component({
  selector: 'app-create-warning',
  templateUrl: './create-warning.component.html',
  styleUrls: ['./create-warning.component.scss']
})
export class CreateWarningComponent implements OnInit {

  constructor(public dialog: MdDialog) { }

  ngOnInit() {

     APPCONFIG.heading="Create warning";
     
}

onClick() {
    let dialogRef = this.dialog.open(ActionDialog, { width: '600px'});
    let award;
    dialogRef.afterClosed().subscribe(result => {

    });
  }
}

@Component({
    selector: 'action-dialog',
    template: ` <h4 class="heading">Written warning <span class="edit"><i class="fa fa-edit"></i></span></h4>
                <div class="form-group">
                   <textarea [readonly]="readOnly" class="form-control">Lorem ipsum dolor sit amet, eu mel augue veritus, usu no option ancillae, vim ea erant salutatus. Nihil everti salutandi nec at, ius id iisque argumentum. Qui option mandamus te, libris facilis pro no. Ornatus tractatos definitiones eam ut, te mel quot graeci. Pri quis probatus incorrupte an, ex habeo tacimates quaerendum duo.</textarea>
                </div>
                <div class="text-center">
                  <button class="btn btn-lg btn-primary">Send</button>
                  <button class="btn btn-lg btn-default">Cancel</button>
                </div>`,
        styles:[`
            h4{ margin-top:0px;}
            .form-group textarea{height:160px;resize:none;}
            .edit{ float:right;}
            .btn-primary{margin-right:10px;}
        `]
})
export class ActionDialog {
  public readOnly=true;
   constructor(public dialogRef: MdDialogRef<ActionDialog>) {}
}
