import { Component, OnInit } from '@angular/core';
import { APPCONFIG } from '../../../../config';
import { Router} from '@angular/router';

@Component({
  selector: 'app-reassessment-result',
  templateUrl: './reassessment-result.component.html',
  styleUrls: ['./reassessment-result.component.scss']
})
export class ReassessmentResultComponent implements OnInit {

   public progress;

  constructor() { }

  ngOnInit() {
    APPCONFIG.heading="BPIP re-assessment result";
     this.progressBarSteps();
  }
  invitelistCollapse=true;

  progressBarSteps() {
      this.progress={
          progress_percent_value:100, //progress percent
          total_steps:4,
          current_step:4,
          circle_container_width:25, //circle container width in percent
          steps:[
              // {num:1,data:"Select Role"},
               {num:1,data:"Re-assess BPIP"},
               {num:2,data:"Training"},
               {num:3,data:"Re-assessment"},
               {num:4,data:"Result",active:true}
          ]  
       }         
  }

}
