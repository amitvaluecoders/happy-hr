import { Component, OnInit } from '@angular/core';
import { APPCONFIG } from '../../../../config';
import { Router} from '@angular/router';

@Component({
  selector: 'app-behavioral-pip-reassessment-training',
  templateUrl: './behavioral-pip-reassessment-training.component.html',
  styleUrls: ['./behavioral-pip-reassessment-training.component.scss']
})
export class BehavioralPIPReassessmentTrainingComponent implements OnInit {

  public progress;
    public invitelistCollapse = false;
    public originalPIPToggle = false;

   constructor(public router:Router ) { }

  ngOnInit() {

     APPCONFIG.heading="BPIP re-assessment training notes";
     this.progressBarSteps();

     
}
progressBarSteps() {
      this.progress={
          progress_percent_value:50, //progress percent
          total_steps:4,
          current_step:2,
          circle_container_width:25, //circle container width in percent
          steps:[
              // {num:1,data:"Select Role"},
               {num:1,data:"Re-assess PIP"},
               {num:2,data:"Training",active:true},
               {num:3,data:"Re-assessment"},
               {num:4,data:"Result"}
          ]  
       }         
  }

}

