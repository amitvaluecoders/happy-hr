import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { MyDatePickerModule } from 'mydatepicker';
import { BehavioralPerformanceImprovementPlanRoutingModule } from './behavioral-performance-improvement-plan.routing';
import { BehavioralPerformanceImprovementPlanService } from './behavioral-performance-improvement-plan.service';
import { CreateBehavioralPerformanceImprovementPlanComponent } from './create-behavioral-performance-improvement-plan/create-behavioral-performance-improvement-plan.component';
import { TrainingComponent } from './training/training.component';
import { BehaviouralPIPAssessmentComponent } from './behavioural-pip-assessment/behavioural-pip-assessment.component';
import { ResultComponent } from './result/result.component';
import { CreateBehavioralPIPReassessmentComponent } from './behavioral-PIP-reassessment/create-behavioral-pip-reassessment/create-behavioral-pip-reassessment.component';
import { BehavioralPIPReassessmentTrainingComponent } from './behavioral-PIP-reassessment/behavioral-pip-reassessment-training/behavioral-pip-reassessment-training.component';
import { BehavioralPIPReassessmentComponent } from './behavioral-PIP-reassessment/behavioral-pip-reassessment/behavioral-pip-reassessment.component';
import { CreateDisciplinaryComponent } from './behavioral-PIP-reassessment/create-disciplinary/create-disciplinary.component';
import { CreateWarningComponent, ActionDialog} from './behavioral-PIP-reassessment/create-warning/create-warning.component';
import { CreateTerminationLetterComponent, terminationDialog} from './behavioral-PIP-reassessment/create-termination-letter/create-termination-letter.component';
import { ReassessmentResultComponent } from './behavioral-PIP-reassessment/reassessment-result/reassessment-result.component';
import { SharedModule } from '../../shared/shared.module';
import { AngularMultiSelectModule } from 'angular2-multiselect-checkbox-dropdown/angular2-multiselect-dropdown';
import {MaterialModule} from '@angular/material';
import { EmployeeBpipDetailsComponent } from './employee-bpip-details/employee-bpip-details.component';
import { EditBpipNoteComponent } from './employee-bpip-details/edit-bpip-note/edit-bpip-note.component';
import { EditBpipEmployeeComponent } from './edit-bpip-employee/edit-bpip-employee.component';
import { DisciplinaryMeetingService } from '../disciplinary-meeting/disciplinary-meeting.service';
import { DisciplinaryMeetingAsActionComponent } from './disciplinary-meeting-as-action/disciplinary-meeting-as-action.component';
import { BehaviouralPerformanceImprovementPlanGuardService } from './behavioural-performance-improvement-plan-guard.service';
@NgModule({
  imports: [
    MaterialModule,
    CommonModule,
    SharedModule,
    FormsModule,
    MyDatePickerModule,
    AngularMultiSelectModule,
    BehavioralPerformanceImprovementPlanRoutingModule
  ],
  declarations: [CreateBehavioralPerformanceImprovementPlanComponent, TrainingComponent, BehaviouralPIPAssessmentComponent, ResultComponent, CreateBehavioralPIPReassessmentComponent, BehavioralPIPReassessmentTrainingComponent, BehavioralPIPReassessmentComponent, CreateDisciplinaryComponent, CreateWarningComponent, CreateTerminationLetterComponent, ReassessmentResultComponent, ActionDialog, terminationDialog, EmployeeBpipDetailsComponent, EditBpipNoteComponent, EditBpipEmployeeComponent, DisciplinaryMeetingAsActionComponent],
  providers:[BehavioralPerformanceImprovementPlanService,DisciplinaryMeetingService,BehaviouralPerformanceImprovementPlanGuardService],
  entryComponents:[ActionDialog, terminationDialog,EditBpipNoteComponent,DisciplinaryMeetingAsActionComponent]
})
export class BehavioralPerformanceImprovementPlanModule { }
