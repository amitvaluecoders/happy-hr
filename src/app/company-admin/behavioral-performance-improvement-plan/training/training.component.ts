import { Component, OnInit } from '@angular/core';
import { APPCONFIG } from '../../../config';
import { BehavioralPerformanceImprovementPlanService } from '../behavioral-performance-improvement-plan.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { CoreService } from '../../../shared/services/core.service';
import { MdSnackBar, MdDialog, MdDialogRef } from '@angular/material';
import { EditBpipNoteComponent } from '../employee-bpip-details/edit-bpip-note/edit-bpip-note.component';
import { ActionsOnEmployeePopupComponent } from '../../../shared/components/actions-on-employee-popup/actions-on-employee-popup.component';
import { DisciplinaryMeetingAsActionComponent } from '../disciplinary-meeting-as-action/disciplinary-meeting-as-action.component'
import { Cookie } from 'ng2-cookies/ng2-cookies';

@Component({
  selector: 'app-training',
  templateUrl: './training.component.html',
  styleUrls: ['./training.component.scss']
})
export class TrainingComponent implements OnInit {

  public progress;
  public trainingNoteAddPermission = true;
  public assessmentNoteAddPermission = true;
  public tempDP: any;
  public isTraCreateProcessing = false;
  public isTraButtonClicked = false;
  public heading = '';
  public serviceDetails: any;
  dropdownListEmployees = [];
  selectedParty = [];
  public originalPIPToggle = [];
  public unsubscriber:any;
  public invitelistCollapse: Boolean;
  invitedropdownSettings = {};
  public plan: any;
  public loggedInUser: any;
  public planNew = {
    insertTime: '',
    status: '',
    developmentFor: { imageUrl: '', employee: '', employeeProfile: '' },
    assinedBy: { imageUrl: '', employee: '', employeeProfile: '' },
    thirdParty: [],
    result: '',
    title: '',
    reason: '',
    objective: '',
    actionPlan: '',
    trainingDate: new Date(),
    trainingTime: '',
    assessmentDate: new Date(),
    assessmentTime: '',
    trainingNotes: [],
    assessmentNotes: [],
    employeeNotes: []
  };
  public isButtonClicked = false;
  public isCreateProcessing = false;
  public isResultProcessing = false;
  public isProcessing = false;
  public counter = 0;
  public pipManagerNote = {
    bpipID: "",
    assessmentNote: "",
    managerNote: "",
    objectiveMet: "Yes",
    managerTraNote: ''
  };
  public reqBody = {
    search:"",
    filter:{
      result:{},
      employeeIDs:[],
      managerIDs:[],
      due:[],
      status:{}
    }
  }

  constructor(public bpipService: BehavioralPerformanceImprovementPlanService,
    public router: Router, public activatedRoute: ActivatedRoute,
    public coreService: CoreService, public dialog: MdDialog) { }


  ngOnInit() {
    this.loggedInUser = JSON.parse(Cookie.get('happyhr-userInfo')) || JSON.parse(localStorage.getItem('happyhr-userInfo'));
    console.log("cookuie",this.loggedInUser);
    //'Awaiting Employee Acceptance','Accepted','Under Assessment','Under Training','Complete','Re Assessment','Reschedule Requested','Awaiting Result'
    //this.serviceDetails = this.coreService.getPIP();
    this.getPlans();
    //console.log("asdgfjh",this.serviceDetails);
      
  }
  onTime(time){
    return new Date(time);
  }

  getPlans() {
    this.isProcessing = true;
    this.bpipService.getAllPlans(JSON.stringify(this.reqBody)).subscribe(
         d => {
             if (d.code == "200") {
               this.serviceDetails=d.data;
                d.data.filter(item=>{
                  if(item.bpipID==this.activatedRoute.snapshot.params['id'])
                  {
                    this.serviceDetails= item;

                  }
                })
                if (!this.serviceDetails) {
                  this.router.navigate([`/app/company-admin/listing-page/behavior-performance-improvement-listing`]);
                }
                else {
                  this.progressBarSteps(this.serviceDetails);
                  this.getpipDetails();
                }
                //console.log("data",this.serviceDetails); 
               
               this.isProcessing = false;    
             }
             else this.coreService.notify("Unsuccessful", d.message, 0);
         },
         error => this.coreService.notify("Unsuccessful", error.message, 0),
         () => { }
     )
     //return this.serviceDetails;
 }


  progressBarSteps(data) {
    this.tempDP = data;
    APPCONFIG.heading = "Create";
    this.progress = {
      progress_percent_value: 25, //progress percent
      total_steps: 4,
      current_step: 1,
      circle_container_width: 25, //circle container width in percent
      steps: [
        { num: 1, data: (data.reaccess == 'No') ? "Create BPIP" : "Re-assess: Create BPIP", active: false },
        { num: 2, data: "Training", active: false },
        { num: 3, data: "Assessment", active: false },
        { num: 4, data: "Result", active: false }
      ]
    }

    if ((data.status == 'Under Training') || (data.status == 'Awaiting employee acceptance') || (data.status == 'Accepted')) {
      APPCONFIG.heading = "Training";
      this.progress.progress_percent_value = 50;
      this.progress.current_step = 2;
      this.progress.steps[1].active = true;
    }
    if (data.status == 'Under Assessment') {
      APPCONFIG.heading = "Assessment";
      this.progress.progress_percent_value = 75;
      this.progress.current_step = 3;
      this.progress.steps[2].active = true;
    }
    if (data.status == 'Complete') {
      APPCONFIG.heading = "Result";
      this.progress.progress_percent_value = 100;
      this.progress.current_step = 4;
      this.progress.steps[3].active = true;
    }
    this.heading = APPCONFIG.heading;
  }

  getpipDetails() {
    this.isProcessing = true;
    this.bpipService.getPipPlanDetails(this.activatedRoute.snapshot.params['id']).subscribe(
      d => {
        if (d.code == "200") {
          this.planNew = d.data;
          this.pipManagerNote.bpipID = this.planNew['bpipID'];
          this.plan = this.planNew['oldBpip'].reverse();
          for (let i in this.plan) { this.originalPIPToggle[i] = false }
          this.isProcessing = false;
          for (let k of this.planNew['thirdParty']) {
            k['id'] = k['employeeID'];
            k['itemName'] = k['employee'];
            this.selectedParty.push(k);
          }

          for (let c of this.planNew.trainingNotes) {
            if (c.noteByUser.userID == this.loggedInUser.userID) this.trainingNoteAddPermission = false;
          }
          for (let c of this.planNew.assessmentNotes) {
            if (c.noteByUser.userID == this.loggedInUser.userID) this.assessmentNoteAddPermission = false;
          }

        }

        else this.coreService.notify("Unsuccessful", d.message, 0);
      },
      error => this.coreService.notify("Unsuccessful", "Error while getting development details", 0),
      () => { }
    )
  }

  onAddResult(id, result) {
    if ((result == 'Terminate') || (result == 'Warning') || (result == 'Final Warning')) {
      let info = { bpipID: this.planNew['bpipID'], type: result, userID: this.planNew['employee'].userID, subject: "", body: '' }
      let dialogRef = this.dialog.open(ActionsOnEmployeePopupComponent);
      dialogRef.componentInstance.actionInfo = info;
      dialogRef.afterClosed().subscribe(r => {
        if (r) {
          this.submitResult(id, result);
        }
      })
    }
    else if (result == 'Disciplinary') {
      this.router.navigate([`/app/company-admin/disciplinary-meeting/create/${this.planNew['employee'].userID}/bpip/${id}`]);

    }
    else this.submitResult(id, result);

  }
  onAddResultNotPassed(id, result) {
    // if ((result == 'Terminate') || (result == 'Warning') || (result == 'Final Warning')) {
    //   let info = { bpipID: this.planNew['bpipID'], type: result, userID: this.planNew['employee'].userID, subject: "", body: '' }
    //   let dialogRef = this.dialog.open(ActionsOnEmployeePopupComponent);
    //   dialogRef.componentInstance.actionInfo = info;
    //   dialogRef.afterClosed().subscribe(r => {
    //     if (r) {
    //       this.submitResult(id, result);
    //     }
    //   })
    // }
    // else if (result == 'Disciplinary') {
    //   this.router.navigate([`/app/company-admin/disciplinary-meeting/create/${this.planNew['employee'].userID}/bpip/${id}`]);

    // }
    // else this.submitResult(id, result);

  }


  submitResult(id, result) {
    this.isResultProcessing = true;
    let reqBody = { bpipID: id, result: result }
    this.bpipService.onAddResult(reqBody).subscribe(
      d => {
        if (d.code == "200") {
          this.coreService.notify("Successful", d.message, 1);
          // if(result=='Disciplinary') this.router.navigate([`/app/company-admin/disciplinary-meeting/create/${this.planNew['employee'].userID}`]);
          if (result == 'Re-Access') {
            this.planNew.result = result;
            this.planNew.status = 'Complete';
          }
          else this.router.navigate([`/app/company-admin/listing-page/behavior-performance-improvement-listing`]);
          this.isResultProcessing = false;
        }
        else {
          this.coreService.notify("Unsuccessful", d.message, 0);
          this.isResultProcessing = false;
        }
      },
      error => {
        this.coreService.notify("Unsuccessful", error.message, 0);
        this.isResultProcessing = false;
      }
    )
  }


  onEditNotes(note, mode) {
    let dialogRef = this.dialog.open(EditBpipNoteComponent);
    dialogRef.componentInstance.mode = mode;
    dialogRef.componentInstance.noteDetails = JSON.parse(JSON.stringify(note));
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        console.log("in", result)
        if (result.mode == 'training') {
          for (let n of this.planNew.trainingNotes) {
            if (n.noteID == result.data.noteID) {
              n.note = result.data.note;
              n.time = result.data.time;
              n.noteByUser.name = result.data.noteByUser.name;
            }
          }
        }
        else {
          for (let n of this.planNew.assessmentNotes) {
            if (n.noteID == result.data.noteID) {
              n.note = result.data.note;
              n.time = result.data.time;
              n.noteByUser.name = result.data.noteByUser.name;
              n.objectiveMet = result.data.objectiveMet;
            }
          }
        }
      }
    });

  }

  onAddManagerTrainingNote(trainingNoteForm) {
    this.isTraButtonClicked = true;
    if (trainingNoteForm.form.valid) {
      let reqBody = {
        bpipID: this.pipManagerNote.bpipID,
        trainingNote: this.pipManagerNote.managerTraNote
      };

      this.isTraCreateProcessing = true;
      this.bpipService.onAddManagerNote(reqBody).subscribe(
        d => {
          if (d.code == "200") {
            this.coreService.notify("Successful", d.message, 1);
            this.router.navigate([`/app/company-admin/listing-page/behavior-performance-improvement-listing`]);
            this.planNew.trainingNotes.push(d.data);
            this.pipManagerNote.managerNote = '';
            this.isTraCreateProcessing = false;
            this.isTraButtonClicked = false;
            this.tempDP['status'] = 'Under Training';
            trainingNoteForm.reset();
          }
          else this.coreService.notify("Unsuccessful", d.message, 0);
        },
        error => {
          this.coreService.notify("Unsuccessful", error.message, 0);
          this.isTraCreateProcessing = false;
        }

      )
    }
  }

  onAddManagerAssessmentNote(form) {
    this.isButtonClicked = true;
    if (form.form.valid) {
      this.isCreateProcessing = true;
      this.pipManagerNote.assessmentNote = this.pipManagerNote.managerNote;
      this.bpipService.onAssessment(this.pipManagerNote).subscribe(
        d => {
          this.isCreateProcessing = false;
          if (d.code == "200") {
            this.coreService.notify("Successful", d.message, 1);
            // this.router.navigate([`/app/company-admin/listing-page/behavior-performance-improvement-listing`]);
            this.assessmentNoteAddPermission = false;
            this.planNew.assessmentNotes.push(d.data);
            this.pipManagerNote.managerNote = '';
            this.isCreateProcessing = false;
            this.isButtonClicked = false;
            this.tempDP['status'] = 'Under Assessment';
            form.reset();
          }
          else this.coreService.notify("Unsuccessful", d.message, 0);
        },
        error => {
          this.coreService.notify("Unsuccessful", error.message, 0);
          this.isCreateProcessing = false;
        }

      )
    }
  }


  onReassessemnt(id) {
    this.router.navigate([`/app/company-admin/behavioral-performance-improvement-plan/create/re/${id}`]);
  }

  onEditPIP(bpipID) {
    this.router.navigate([`/app/company-admin/behavioral-performance-improvement-plan/update/${bpipID}`]);
  }



}
