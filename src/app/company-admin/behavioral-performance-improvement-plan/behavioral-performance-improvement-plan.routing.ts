import {  ModuleWithProviders  } from '@angular/core';
import {  RouterModule,Routes  } from '@angular/router';

import { CreateBehavioralPerformanceImprovementPlanComponent } from './create-behavioral-performance-improvement-plan/create-behavioral-performance-improvement-plan.component';
import { TrainingComponent } from './training/training.component';
import { BehaviouralPIPAssessmentComponent } from './behavioural-pip-assessment/behavioural-pip-assessment.component';
import { ResultComponent } from './result/result.component';
import { CreateBehavioralPIPReassessmentComponent } from './behavioral-PIP-reassessment/create-behavioral-pip-reassessment/create-behavioral-pip-reassessment.component';
import { BehavioralPIPReassessmentTrainingComponent } from './behavioral-PIP-reassessment/behavioral-pip-reassessment-training/behavioral-pip-reassessment-training.component';
import { BehavioralPIPReassessmentComponent } from './behavioral-PIP-reassessment/behavioral-pip-reassessment/behavioral-pip-reassessment.component';
import { CreateDisciplinaryComponent } from './behavioral-PIP-reassessment/create-disciplinary/create-disciplinary.component';
import { CreateWarningComponent } from './behavioral-PIP-reassessment/create-warning/create-warning.component';
import { CreateTerminationLetterComponent } from './behavioral-PIP-reassessment/create-termination-letter/create-termination-letter.component';
import { ReassessmentResultComponent } from './behavioral-PIP-reassessment/reassessment-result/reassessment-result.component';
import { EmployeeBpipDetailsComponent } from './employee-bpip-details/employee-bpip-details.component';
import { EditBpipEmployeeComponent } from './edit-bpip-employee/edit-bpip-employee.component';

export const BehavioralPerformanceImprovementPlanPagesRoutes: Routes = [
    {
        path: '',
        canActivate:[],
        children: [
           
            { path: 'create/:id', component: CreateBehavioralPerformanceImprovementPlanComponent},
            { path: 'training/:id', component: TrainingComponent},
            { path: 'details/:status/:id', component: EmployeeBpipDetailsComponent},
            { path: 'update/:id', component: EditBpipEmployeeComponent},
            { path: 'create/:info/:id', component: CreateBehavioralPerformanceImprovementPlanComponent},

            // { path: 'assessment', component: BehaviouralPIPAssessmentComponent},
            // { path: 'result', component: ResultComponent},
            // { path: 'reassessment/create', component: CreateBehavioralPIPReassessmentComponent},
            // { path: 'reassessment/training', component: BehavioralPIPReassessmentTrainingComponent},
            // { path: 'reassessment', component: BehavioralPIPReassessmentComponent},
            // { path: 'reassessment/disciplinary/create', component: CreateDisciplinaryComponent},
            // { path: 'reassessment/warning/create', component: CreateWarningComponent},
            // { path: 'reassessment/termination-letter/create', component: CreateTerminationLetterComponent},
            // { path: 'reassessment/result', component: ReassessmentResultComponent},
            
            
            
            
            
            
            
            
            
            
            
        ]
    }
];

export const BehavioralPerformanceImprovementPlanRoutingModule = RouterModule.forChild(BehavioralPerformanceImprovementPlanPagesRoutes);
