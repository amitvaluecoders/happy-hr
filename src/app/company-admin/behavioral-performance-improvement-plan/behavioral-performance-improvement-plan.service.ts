import { Injectable } from '@angular/core';
import { RestfulLaravelService} from '../../shared/services/restful-laravel.service';

@Injectable()
export class BehavioralPerformanceImprovementPlanService {

   constructor(public restfulWebService:RestfulLaravelService) { }


    getAllEmployee():any{
   //return this.restfulWebService.get('show-manager-and-ceo')
   return this.restfulWebService.get('show-all-employee');
  }
  getAllManager(id):any{
    if(id==null){
       return this.restfulWebService.get('show-manager-and-ceo-and-fm')        
    }
    else{
      return this.restfulWebService.get(`show-manager-and-ceo-and-fm/${id}`)    
    }
    //return this.restfulWebService.get('show-all-employee');
   }

  createPLan(reqBody):any{
   return this.restfulWebService.post('bpip-create-update',reqBody); 
  }

  getDataForFilters():any{
   return this.restfulWebService.get(`bpip-filter-data`)    
  }

  getAllPlans(reqBody):any{
   return this.restfulWebService.get(`bpip-list/${encodeURIComponent(reqBody)}`)    
  }

  getPipPlanDetails(reqBody):any{
   return this.restfulWebService.get(`bpip-detail/${reqBody}`)    
  }

  onAddResult(reqBody):any{
   return this.restfulWebService.post('bpip-result',reqBody);     
  }

  onAddManagerNote(reqBody):any{
   return this.restfulWebService.post('bpip-training',reqBody);         
  }

   onAssessment(reqBody):any{
   return this.restfulWebService.post('bpip-assessment',reqBody);         
  }

  updateDevelopmentPlanNotes(reqBody):any{
       return this.restfulWebService.put('bpip-note-edit',reqBody)
  }

  deleteBpip(reqBody):any{
    return this.restfulWebService.delete(`bpip-delete/${reqBody}`)
  }

}
