import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateBehavioralPerformanceImprovementPlanComponent } from './create-behavioral-performance-improvement-plan.component';

describe('CreateBehavioralPerformanceImprovementPlanComponent', () => {
  let component: CreateBehavioralPerformanceImprovementPlanComponent;
  let fixture: ComponentFixture<CreateBehavioralPerformanceImprovementPlanComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateBehavioralPerformanceImprovementPlanComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateBehavioralPerformanceImprovementPlanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
