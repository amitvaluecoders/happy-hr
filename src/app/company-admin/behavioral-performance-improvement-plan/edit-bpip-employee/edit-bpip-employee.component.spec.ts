import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditBpipEmployeeComponent } from './edit-bpip-employee.component';

describe('EditBpipEmployeeComponent', () => {
  let component: EditBpipEmployeeComponent;
  let fixture: ComponentFixture<EditBpipEmployeeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditBpipEmployeeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditBpipEmployeeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
