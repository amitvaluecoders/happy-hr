import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DisciplinaryMeetingAsActionComponent } from './disciplinary-meeting-as-action.component';

describe('DisciplinaryMeetingAsActionComponent', () => {
  let component: DisciplinaryMeetingAsActionComponent;
  let fixture: ComponentFixture<DisciplinaryMeetingAsActionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DisciplinaryMeetingAsActionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DisciplinaryMeetingAsActionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
