import { Component, OnInit } from '@angular/core';
import { APPCONFIG } from '../../../config';
import { DisciplinaryMeetingService } from '../../disciplinary-meeting/disciplinary-meeting.service';
import { Cookie } from 'ng2-cookies/ng2-cookies';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { CoreService } from '../../../shared/services/core.service';
import * as moment from 'moment';
import { IDatePickerConfig } from 'ng2-date-picker';
import { MdDialogRef } from '@angular/material';

@Component({
  selector: 'app-disciplinary-meeting-as-action',
  templateUrl: './disciplinary-meeting-as-action.component.html',
  styleUrls: ['./disciplinary-meeting-as-action.component.scss']
})
export class DisciplinaryMeetingAsActionComponent implements OnInit {

  //in case of result in PIP,BPIP
  public actionInfo:any;

  public isCreateProcessing=false;
  public progress;
  public isProcessing=false;
  public isButtonClicked=false;
  public birthday: any;
  public config: IDatePickerConfig = {};
  public tempDateAndTime:any;
  public damID:any;
  public tempThirdParty=[];
  public heading='';

  // variables for third party invite . 
  public isThirdPartyAlreadyExist = false;
  public isEnterPressed = false;
  public thirdPartyName = ''; 
  public thirdPartyList = [];
  public dropdownListEmployees = [];
  public invitelistCollapse = false;
  public createdBy:any;
 public userInfo={
     name:''
 }
 public disciplinayMeeting = {
        disciplinaryActionID:'',
        title:'',
        reason:'',
        employeeID:'',
        meetingTime:'',
        managerID:'',
        thirdParty:[]

}

    constructor(public  disciplinaryMeetingService: DisciplinaryMeetingService,
        public router: Router, public activatedRoute: ActivatedRoute,
        public dialog: MdDialogRef<DisciplinaryMeetingAsActionComponent>,
        public coreService: CoreService)  { 
        this.config.locale = "en";
        this.config.format = "YYYY-MM-DD hh:mm A";
        this.config.showMultipleYearsNavigation = true;
        this.config.weekDayFormat = 'dd';
        this.config.min=moment();
        this.config.disableKeypress = true;
        this.config.monthFormat="MMM YYYY";
    }

  ngOnInit() {
      this.damID=this.activatedRoute.snapshot.params['damID'];
      this.createdBy=JSON.parse(Cookie.get('happyhr-userInfo'));
     this.heading=this.damID?"Update":"Create";
     if(!this.actionInfo)APPCONFIG.heading= this.heading+" disciplinary meeting";
     this.progressBarSteps();
     this.getEmployeeeList();
 }
 progressBarSteps() {
      this.progress={
          progress_percent_value:25, //progress percent
          total_steps:4,
          current_step:1,
          circle_container_width:20, //circle container width in percent
          steps:[
              // {num:1,data:"Select Role"},
               {num:1,data:"Create disciplinary meeting",active:true},
               {num:2,data:"Employee: invite third party"},
               {num:3,data:"Formal disciplinary meeting"},
               {num:4,data:"Result"},
          ]  
       }         
   }

     // method to get all employee listing for manger and third pary listing 
  getEmployeeeList() {
     this.isProcessing = true;
     this.disciplinaryMeetingService.getAllEmployee().subscribe(
          d => {
              if (d.code == "200") {
                    this.isProcessing = false; 
                  if(this.damID) this.getDisciplinaryMeetingdetails();
                  this.dropdownListEmployees=d.data.filter(item =>{
                      if((!this.activatedRoute.snapshot.params['damID']) && ( (item.userId==this.activatedRoute.snapshot.params['userID'] ) || (item.userId ==this.actionInfo.userID) ) ) {
                          this.userInfo=item;
                          this.disciplinayMeeting.employeeID=item.userId;
                        } 
                      item['id'] = item.userId;
                      item['itemName'] = item.name;
                      item['isSelected'] = false;
                      return item;
                  })

                  this.thirdPartyList=JSON.parse(JSON.stringify(this.dropdownListEmployees));
               
              }
              else this.coreService.notify("Unsuccessful", "Error while getting contract types", 0);
          },
          error => this.coreService.notify("Unsuccessful", "Error while getting contract types", 0),
          () => { }
      )
  }

        getDisciplinaryMeetingdetails(){
            this.isProcessing=true;
            this.disciplinaryMeetingService.getDisciplinaryMeetingDetails(this.damID).subscribe(
                d => {
              if (d.code == "200") {
                 this.isProcessing=false; 
                this.userInfo=d.data.employee;
                this.createdBy=d.data.manager
                 this.disciplinayMeeting=d.data;
                 this.tempThirdParty=JSON.parse(JSON.stringify(d.data.thirdParty));
                 this.disciplinayMeeting.employeeID=this.userInfo['userID'];
                 this.tempDateAndTime=moment(d.data.meetingtime).format("YYYY-MM-DD hh:mm:ss");  
             
              }
              else this.coreService.notify("Unsuccessful", d.message, 0);
          },
          error => this.coreService.notify("Unsuccessful", error.message, 0),
          () => { }                )     
        }

     onEnterExternalThirdParty(isvalid){
          if(this.thirdPartyName){
      this.isThirdPartyAlreadyExist=false;
      this.isEnterPressed=true;
      if(isvalid){
            for(let c of this.thirdPartyList ){
                if(c.email==this.thirdPartyName)this.isThirdPartyAlreadyExist=true;
            }
            for(let c of this.disciplinayMeeting.thirdParty ){
                if(c.email==this.thirdPartyName || c.name==this.thirdPartyName)this.isThirdPartyAlreadyExist=true;
            }
            if(!this.isThirdPartyAlreadyExist){
                this.thirdPartyList.splice(0,0,{
                    isSelected:true,
                    name:this.thirdPartyName,
                    email:this.thirdPartyName,
                    external:true,
                    imageUrl:"http://happyhrapi.n1.iworklab.com/public/user-profile-image/user.png"
                })
                this.setForTempThirsParty();

                this.thirdPartyName='';
                this.isEnterPressed=false;
            } 
      }
          }

  }

// set third parties while updating the disciplinary meeting.
  setForTempThirsParty(){
      for(let c of this.tempThirdParty ){
                if(c.email==this.tempThirdParty)this.isThirdPartyAlreadyExist=true;
            }
            if(!this.isThirdPartyAlreadyExist){
                this.tempThirdParty.splice(0,0,{
                    isSelected:true,
                    name:this.thirdPartyName,
                    email:this.thirdPartyName,
                    external:true,
                    imageUrl:"http://happyhrapi.n1.iworklab.com/public/user-profile-image/user.png"
                })
            }
  }


    onCreate(isValid){
        this.isButtonClicked=true;
        if(isValid){
              for(let i of this.thirdPartyList){
               if(i.isSelected){
                 this.disciplinayMeeting.thirdParty.push({
                     type:(i.external)?"External":"Internal",
                     userID:(!i.external)?i.userId:'',
                     email:(i.external)?i.email:''
                });  
               }
           }
           this.disciplinayMeeting.managerID=this.createdBy.userID;
           this.disciplinayMeeting.meetingTime = moment(this.tempDateAndTime).format("YYYY-MM-DD hh:mm:ss");  
            console.log(this.disciplinayMeeting);
            this.saveDisciplinaryAction();
        }
    }

    saveDisciplinaryAction(){
         this.isCreateProcessing=true;
            if(this.actionInfo){
                if(this.actionInfo['bpipID']) this.disciplinayMeeting["bpipID"]=this.actionInfo['bpipID'];
                if(this.actionInfo['pipID']) this.disciplinayMeeting["pipID"]=this.actionInfo['pipID'];
                if(this.actionInfo['userID']) this.disciplinayMeeting["userID"]=this.actionInfo['userID'];
            }
        this.disciplinaryMeetingService.createPLan(this.disciplinayMeeting).subscribe(
          d => {
              if (d.code == "200") {
                  this.coreService.notify("Successful",d.message, 1);
                  this.dialog.close(true);
                
              }
              else this.coreService.notify("Unsuccessful",d.message, 0);
               this.isCreateProcessing = false; 
          },
          error => {
              this.coreService.notify("Unsuccessful", error.message, 0);
              this.isCreateProcessing = false; 
          }
          
      )
    }


}
