import { Injectable } from '@angular/core';
import {CanActivate,Router,RouterStateSnapshot,ActivatedRouteSnapshot} from "@angular/router";
import {CoreService} from '../../shared/services/core.service';
import {Observable} from 'rxjs/Rx'; 
import 'rxjs/add/operator/map';

@Injectable()
export class OhsGuideService {

  public module = "ohs";
  
        constructor(private _core: CoreService, private router: Router){ }
  
        canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {  
            let url: string;
            if(route['_routeConfig']['path'])
            url= route['_routeConfig']['path']
            else url = state.url;
            if(this.checkHavePermission(url)){
              return true;
            }else{
              this.router.navigate(['/extra/unauthorized']);
              return false;
            }
        }
  
        private checkHavePermission(path) {
            switch (path) {
                case "/app/company-admin/ohs":
                    return this._core.getModulePermission(this.module, 'view');
                case 'incident-assessment/:id':
                    return this._core.getModulePermission(this.module, 'view');      
                case 'company-admin-notified/:id':
                    return this._core.getModulePermission(this.module, 'view'); 
                case 'warning-email/:id':
                    return this._core.getModulePermission(this.module, 'view');            
                case 'corrective-actions/:id':
                    return this._core.getModulePermission(this.module, 'view'); 
                case 'repair-assessment/:id':
                    return this._core.getModulePermission(this.module, 'view'); 
                case 'result/:id':
                    return this._core.getModulePermission(this.module, 'view'); 
                default:
                    return false;
            }
        }
}

