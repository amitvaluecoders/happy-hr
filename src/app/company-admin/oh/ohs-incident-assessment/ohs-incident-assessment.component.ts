import { Component, OnInit } from '@angular/core';
import { APPCONFIG } from '../../../config';
import { Router, ActivatedRoute } from '@angular/router';
import { CoreService } from '../../../shared/services/core.service';
import { OhService } from "../oh.service";
import { Ohs } from "../ohs";
import * as moment from 'moment';

@Component({
  selector: 'app-ohs-incident-assessment',
  templateUrl: './ohs-incident-assessment.component.html',
  styleUrls: ['./ohs-incident-assessment.component.scss']
})
export class OhsIncidentAssessmentComponent implements OnInit {
  public progress;
  isProcessing: boolean;
  noResult = true;
  ohs = new Ohs;

  constructor(public companyService: OhService, public coreService: CoreService, private router: Router,
    private route: ActivatedRoute) { }

  ngOnInit() {
    APPCONFIG.heading = "Incident reported";
    this.progressBarSteps();

    if (this.route.snapshot.params['id']) {
      this.ohs.ohsID = this.route.snapshot.params['id'];
      this.getIncidentDetail();
    }
  }

  progressBarSteps() {
    this.progress = {
      progress_percent_value: 20, //progress percent
      total_steps: 5,
      current_step: 1,
      circle_container_width: 20, //circle container width in percent
      steps: [
        // {num:1,data:"Select Role"},
        { num: 1, data: "Incident reported", active: true },
        { num: 2, data: "OH&S or company admin is notified" },
        { num: 3, data: "Warning email" },
        { num: 4, data: "Corrective actions" },
        { num: 5, data: "Repair assessment" }
      ]
    }
  }

  getIncidentDetail() {
    this.isProcessing = true;
    this.companyService.getOhsDetail(this.ohs.ohsID).subscribe(
      res => {
        this.isProcessing = false;
        if (res.code == 200) {
          this.ohs = res.data;
          this.noResult = false;
        }
        else return this.coreService.notify("Unsuccessful", res.message, 0);
      },
      err => {
        this.isProcessing = false;
        if (err.code == 400) {
          return this.coreService.notify("Unsuccessful", err.message, 0);
        }
        this.coreService.notify("Unsuccessful", "Error while getting data.", 0)
      }
    );
  }

  formatDateTime(date) {
    return moment(date).isValid() ? moment(date).format('DD MMM YYYY hh:mm A') : 'Not specified';
  }

}
