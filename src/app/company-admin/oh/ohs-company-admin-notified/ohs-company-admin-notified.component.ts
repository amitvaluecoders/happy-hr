import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { MdSnackBar, MdDialog, MdDialogRef } from '@angular/material';
import { APPCONFIG } from '../../../config';
import { Router, ActivatedRoute } from '@angular/router';
import { CoreService } from '../../../shared/services/core.service';
import { OhService } from "../oh.service";
import { Ohs, InjuryDetail } from "../ohs";
import * as moment from 'moment';

@Component({
    selector: 'app-ohs-company-admin-notified',
    templateUrl: './ohs-company-admin-notified.component.html',
    styleUrls: ['./ohs-company-admin-notified.component.scss']
})
export class OhsCompanyAdminNotifiedComponent implements OnInit {
    public progress;
    isProcessing: boolean;
    isSaving: boolean;
    submitted = false;
    invalidMarker = false;
    noResult = true;
    ohs: Ohs;
    assessment = new InjuryDetail;

    employeeList = [];
    selectedEmployee = [];
    dropdownSettings = {};
    options = [];
    addNote = { add: false, index: null, note: '' };

    actions = [{ id: 1, action: 'Employee resumed duties after treatment' }, { id: 2, action: 'Employee sent home' },
    { id: 3, action: 'Employee taken to doctor' }, { id: 4, action: 'Ambulance called' }, { id: 5, action: 'Accident site preserved' }, { id: 6, action: 'Other' }]

    @ViewChild('markerNote') markerNote: ElementRef;
    constructor(public companyService: OhService, public coreService: CoreService, private router: Router,
        private route: ActivatedRoute, public dialog: MdDialog) { this.ohs = new Ohs; }

    ngOnInit() {
        let home = this;
        $("#container").click(function (e) {
            e.preventDefault();

            if (e.target.classList.contains('marker')) {
                console.log(e);
                // home.resetNote();
                // return $('#' + e.target.id).remove();
                let index = e.target.id.split('-').pop();
                home.addNote.add = true;
                home.addNote.index = index;
                home.addNote.note = home.assessment.marker[index].markerNote;
                return;
            }
            else if (home.addNote.add == true) {
                return home.markerNote.nativeElement.focus();
                // home.assessment.marker.splice(home.addNote.index, 1);
                // $('#mark-' + home.addNote.index).remove();
                // home.resetNote();
            }
            let x = e.offsetX - 5;
            let y = e.offsetY - 5;
            let mark = $('<i></i>');
            mark.addClass('fa fa-circle marker');
            mark.css('top', y);
            mark.css('left', x);
            mark.css('position', 'absolute');
            mark.css('color', '#ff8383');
            mark.attr('id', 'mark-' + home.assessment.marker.length);
            mark.appendTo('#container');

            home.assessment.marker.push({ posX: x, posY: y, markerNote: '' });
            home.addNote.add = true;
            home.addNote.index = home.assessment.marker.length - 1;
        });

        APPCONFIG.heading = "OH&S or company admin is notified";
        this.progressBarSteps();

        this.dropdownSettings = {
            singleSelection: true,
            text: "Select user",
            enableSearchFilter: true,
            classes: "myclass"
        };

        if (this.route.snapshot.params['id']) {
            this.ohs.ohsID = this.route.snapshot.params['id'];
            this.getIncidentDetail();
        }
        this.getEmployee();
        // $('angular2-multiselect:eq(0) .list-area ul').addClass('listItems');
        // $('angular2-multiselect:eq(1) .list-area ul').addClass('inviteListItems');
    }

    progressBarSteps() {
        this.progress = {
            progress_percent_value: 40, //progress percent
            total_steps: 5,
            current_step: 2,
            circle_container_width: 20, //circle container width in percent
            steps: [
                // {num:1,data:"Select Role"},
                { num: 1, data: "Incident reported" },
                { num: 2, data: "OH&S or company admin is notified", active: true },
                { num: 3, data: "Warning email" },
                { num: 4, data: "Corrective actions" },
                { num: 5, data: "Repair assessment" }
            ]
        }
    }

    getEmployee() {
        this.companyService.getEmployee().subscribe(
            res => {
                if (res.code == 200) {
                    if (res.data instanceof Array) {
                        this.employeeList = res.data;
                        this.employeeList.filter((item, i) => { item['itemName'] = item.name; item['id'] = item.userId; });
                        // setTimeout(() => {
                        //     this.employeeList.filter(function (item, i) {
                        //         $('.listItems li').eq(i).prepend(`<img src="assets/images/${item.imageUrl}" alt=" />`);
                        //         $('.listItems li').eq(i).children('label').append('<span>' + item.role + '</span>');
                        //     });
                        // }, 0);
                    }
                }
                else return this.coreService.notify("Unsuccessful", res.message, 0);
            },
            err => {
                if (err.code == 400) {
                    return this.coreService.notify("Unsuccessful", err.message, 0);
                }
                this.coreService.notify("Unsuccessful", "Error while getting data.", 0)
            }
        );
    }

    getIncidentDetail() {
        this.isProcessing = true;
        this.companyService.getOhsDetail(this.ohs.ohsID).subscribe(
            res => {
                this.isProcessing = false;
                if (res.code == 200) {
                    this.ohs = res.data;
                    this.noResult = false;
                    if (this.ohs.injuryDetail) {
                        this.setMarker();
                    }
                }
                else return this.coreService.notify("Unsuccessful", res.message, 0);
            },
            err => {
                this.isProcessing = false;
                if (err.code == 400) {
                    return this.coreService.notify("Unsuccessful", err.message, 0);
                }
                this.coreService.notify("Unsuccessful", "Error while getting data.", 0)
            }
        );
    }

    formatDateTime(date) {
        return moment(date).isValid() ? moment(date).format('DD MMM YYYY hh:mm A') : 'Not specified';
    }

    getName(url: string) {
        return url.split('/').pop();
    }

    setMarker() {
        let marker = this.ohs.injuryDetail.marker;
        if (marker.length) {
            for (let i = 0; i < marker.length; i++) {
                let mark = $('<i></i>');
                mark.addClass('fa fa-circle marker');
                mark.css('top', marker[i].posY);
                mark.css('left', marker[i].posX);
                mark.css('position', 'absolute');
                mark.css('color', '#ff8383');
                mark.attr('id', 'mark-' + i);
                mark.appendTo('#container');
            }
        }
    }

    onCheckOption(event, item) {
        let ind = this.options.findIndex(i => i.id == item.id);

        if (event.target.checked) {
            (ind >= 0) ? null : this.options.push(item);
        } else {
            (ind >= 0) ? this.options.splice(ind, 1) : null;
        }
    }

    clearMarker() {
        this.assessment.marker = [];
        $(".marker").remove();
        this.resetNote();
    }

    submitNote() {
        if (!this.addNote.note.trim().length) { this.invalidMarker = true; return; }
        this.invalidMarker = false;
        this.assessment.marker[this.addNote.index].markerNote = this.addNote.note;
        this.resetNote();
    }

    deleteMarkerAndNote() {
        this.assessment.marker.splice(this.addNote.index, 1);
        $('#mark-' + this.addNote.index).remove();
        this.resetNote();
    }

    resetNote() {
        this.addNote = { add: false, index: null, note: '' };
    }

    isValidMarker() {
        if (!this.assessment.marker.length) { this.invalidMarker = true; return false };
        for (let i = 0; i < this.assessment.marker.length; i++) {
            if (!this.assessment.marker[i].markerNote.trim().length) {
                this.invalidMarker = true;
                return false;
            }
        }
        this.invalidMarker = false;
        return true;
    }

    onClick() {
        if (!this.options.length) return;
        this.assessment['ohsID'] = this.ohs.ohsID;
        this.assessment.furtherAction = this.options.length ? this.options.map(i => { return i.action }) : [];
        this.assessment.takenToDoctoruserID = this.selectedEmployee.length ? this.selectedEmployee[0].id : '';
        console.log(this.assessment);

        let dialogRef = this.dialog.open(OhsCompanyAdminNotifiedDialog, { width: '500px' });
        dialogRef.componentInstance.ohs = this.ohs;
        dialogRef.afterClosed().subscribe(result => {
            if (result) {
                this.isSaving = true;
                this.companyService.saveInjuriDetail(this.assessment).subscribe(
                    res => {
                        this.isSaving = false;
                        if (res.code == 200) {
                            this.coreService.notify("Successful", res.message, 1);
                            if (result == 1) this.router.navigate(['/app/company-admin/ohs/warning-email', this.ohs.ohsID]);
                            else if (result == 2) this.router.navigate(['/app/company-admin/ohs/corrective-actions', this.ohs.ohsID]);
                            // else this.router.navigate(['/app/company-admin/ohs/corrective-actions', this.ohs.ohsID]);
                        }
                        else return this.coreService.notify("Unsuccessful", res.message, 0);
                    },
                    err => {
                        this.isSaving = false;
                        if (err.code == 400) {
                            return this.coreService.notify("Unsuccessful", err.message, 0);
                        }
                        this.coreService.notify("Unsuccessful", "Error while saving question.", 0);
                    }
                )
            }
        })
    }

}

@Component({
    selector: 'ohs-company-admin-notified',
    template: `<div md-dialog-content style="padding-bottom: 20px;">
                  <div class="correctiveActions text-center">
                    <ul>
                      <li><button class="btn btn-primary" (click)="dialogRef.close(1);">Warning email to all employees</button></li>
                      <li><button class="btn btn-primary" (click)="dialogRef.close(2);">Corrective actions</button></li>
                      <li><button class="btn btn-primary" (click)="downloadPDF()">Download report</button></li>
                    </ul>
                  </div>
               </div>`,
    styleUrls: ['./ohs-company-admin-notified.component.scss']
})
export class OhsCompanyAdminNotifiedDialog implements OnInit {
    ohs: Ohs;
    constructor(public dialogRef: MdDialogRef<OhsCompanyAdminNotifiedDialog>, private companyService: OhService) { this.ohs = new Ohs; }

    ngOnInit() { }

    downloadPDF() {
        this.dialogRef.close(3);
        this.companyService.downloadPdf(this.ohs.ohsID);
    }
}
