import { ModuleWithProviders } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { OhsIncidentAssessmentComponent } from './ohs-incident-assessment/ohs-incident-assessment.component';
import { OhsCompanyAdminNotifiedComponent } from './ohs-company-admin-notified/ohs-company-admin-notified.component';
import { WarningEmailComponent } from './warning-email/warning-email.component';
import { CorrectiveActionsComponent } from './corrective-actions/corrective-actions.component';
import { RepairAssessmentComponent } from './repair-assessment/repair-assessment.component';
import { ResultComponent } from './result/result.component';

export const OhPagesRoutes: Routes = [
    { path: 'incident-assessment/:id', component: OhsIncidentAssessmentComponent },
    { path: 'company-admin-notified/:id', component: OhsCompanyAdminNotifiedComponent },
    { path: 'warning-email/:id', component: WarningEmailComponent },
    { path: 'corrective-actions/:id', component: CorrectiveActionsComponent },
    { path: 'repair-assessment/:id', component: RepairAssessmentComponent },
    { path: 'result/:id', component: ResultComponent }
];

export const OhRoutingModule = RouterModule.forChild(OhPagesRoutes);
