import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from '@angular/material';
import { FormsModule } from '@angular/forms';
import { OhRoutingModule } from './oh.routing';
import { OhService } from './oh.service';
import { OhsIncidentAssessmentComponent } from './ohs-incident-assessment/ohs-incident-assessment.component';
import { OhsCompanyAdminNotifiedComponent, OhsCompanyAdminNotifiedDialog } from './ohs-company-admin-notified/ohs-company-admin-notified.component';
import { WarningEmailComponent } from './warning-email/warning-email.component';
import { CorrectiveActionsComponent } from './corrective-actions/corrective-actions.component';
import { RepairAssessmentComponent, ResolvedMailDialog } from './repair-assessment/repair-assessment.component';
import { ResultComponent } from './result/result.component';

import { NguiDatetimePickerModule } from '@ngui/datetime-picker';
import { SharedModule } from '../../shared/shared.module';
import { AngularMultiSelectModule } from 'angular2-multiselect-checkbox-dropdown/angular2-multiselect-dropdown';
import { OhsGuideService } from './ohs-guide.service';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    OhRoutingModule,
    NguiDatetimePickerModule,
    SharedModule,
    AngularMultiSelectModule,
    MaterialModule
  ],
  entryComponents: [OhsCompanyAdminNotifiedDialog, ResolvedMailDialog],
  declarations: [OhsIncidentAssessmentComponent, OhsCompanyAdminNotifiedComponent, WarningEmailComponent,
    CorrectiveActionsComponent, RepairAssessmentComponent, ResultComponent, OhsCompanyAdminNotifiedDialog,
    ResolvedMailDialog],
  providers: [OhService,OhsGuideService]
})
export class OhModule { }
