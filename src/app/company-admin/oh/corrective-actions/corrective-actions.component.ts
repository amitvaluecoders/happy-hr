import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { APPCONFIG } from '../../../config';
import { CoreService } from '../../../shared/services/core.service';
import { OhService } from "../oh.service";
import { IDatePickerConfig } from 'ng2-date-picker';
import * as moment from "moment";

@Component({
  selector: 'app-corrective-actions',
  templateUrl: './corrective-actions.component.html',
  styleUrls: ['./corrective-actions.component.scss']
})
export class CorrectiveActionsComponent implements OnInit {

  public progress;
  // isProcessing = false;
  isProcessingForm = false;
  noResult = true;
  // ohs = {};
  action = {
    "ohsID": "", "title": "", "measuresTaken": "", "booking": "", "supply": "", "actionTime": "", "reminderTime": ""
  }
  // actionTime;
  reminderTime;
  config: IDatePickerConfig = {};
  remConfig: IDatePickerConfig = {};

  constructor(public companyService: OhService, public coreService: CoreService, private router: Router,
    private route: ActivatedRoute) {

    this.remConfig.locale = this.config.locale = "en";
    this.remConfig.format = this.config.format = "DD MMM, YYYY hh:mm A";
    this.remConfig.showMultipleYearsNavigation = this.config.showMultipleYearsNavigation = true;
    this.remConfig.weekDayFormat = this.config.weekDayFormat = 'dd';
    this.remConfig.disableKeypress = this.config.disableKeypress = true;
    this.remConfig.monthFormat = this.config.monthFormat = "MMM YYYY";
    this.config.max = moment();
    this.remConfig.min = moment();
  }

  ngOnInit() {
    APPCONFIG.heading = "Corrective actions";
    this.progressBarSteps();

    if (this.route.snapshot.params['id']) {
      this.action.ohsID = this.route.snapshot.params['id'];
      // this.getCorrectiveActionDetail();
    }
  }

  progressBarSteps() {
    this.progress = {
      progress_percent_value: 80, //progress percent
      total_steps: 5,
      current_step: 4,
      circle_container_width: 20, //circle container width in percent
      steps: [
        // {num:1,data:"Select Role"},
        { num: 1, data: "Incident reported" },
        { num: 2, data: "OH&S or company admin is notified" },
        { num: 3, data: "Warning email" },
        { num: 4, data: "Corrective actions", active: true },
        { num: 5, data: "Repair assessment" }
      ]
    }
  }

  // getCorrectiveActionDetail() {
  //   this.isProcessing = true;
  //   this.companyService.getOhsDetail(this.ohs['ohsID']).subscribe(
  //     res => {
  //       this.isProcessing = false;
  //       if (res.code == 200) {
  //         this.ohs = res.data;
  //         if (res.data.correctiveAction.length) {
  //           this.action = res.data.correctiveAction[0];
  //           this.action.actionTime = !this.action.actionTime ? '' : moment(this.action.actionTime).format('DD MMM, YYYY hh:mm A');
  //           this.action.reminderTime = !this.action.reminderTime ? '' : moment(this.action.reminderTime).format('DD MMM, YYYY hh:mm A');
  //         }
  //         this.noResult = false;
  //       }
  //       else return this.coreService.notify("Unsuccessful", res.message, 0);
  //     },
  //     err => {
  //       this.isProcessing = false;
  //       if (err.code == 400) {
  //         return this.coreService.notify("Unsuccessful", err.message, 0);
  //       }
  //       this.coreService.notify("Unsuccessful", "Error while getting data.", 0)
  //     }
  //   )
  // }

  submitAction() {
    // this.action.actionTime = this.actionTime && moment(this.actionTime).isValid() ? moment(this.actionTime).format('YYYY-MM-DD HH:mm:ss') : '';
    this.action.reminderTime = this.reminderTime && moment(this.reminderTime).isValid() ? moment(this.reminderTime).format('YYYY-MM-DD HH:mm:ss') : '';
    console.log(this.action);
    this.isProcessingForm = true;
    this.companyService.takeCorrectiveAction(this.action).subscribe(
      res => {
        this.isProcessingForm = false;
        if (res.code == 200) {
          this.coreService.notify("Successful", res.message, 1);
          this.router.navigate(['/app/company-admin/ohs/repair-assessment', this.action.ohsID]);
        }
        else return this.coreService.notify("Unsuccessful", res.message, 0);
      },
      err => {
        this.isProcessingForm = false;
        if (err.code == 400) {
          return this.coreService.notify("Unsuccessful", err.message, 0);
        }
        this.coreService.notify("Unsuccessful", "Error while getting data.", 0)
      }
    )
  }

}