import { Injectable } from '@angular/core';
import { RestfulLaravelService } from "../../shared/services/restful-laravel.service";

@Injectable()
export class OhService {

  constructor(private restfulWebService: RestfulLaravelService) { }

  getOhsList(params) {
    return this.restfulWebService.get(`list-ohs/${params}`);
  }

  getOhsDetail(id) {
    return this.restfulWebService.get(`ohs-detail/${id}`);
  }

  getEmployee() {
    return this.restfulWebService.get(`show-all-employee`);
  }

  saveInjuriDetail(reqBody) {
    return this.restfulWebService.post('save-injury-detail', reqBody);
  }

  sendWarningMail(reqBody) {
    return this.restfulWebService.post('ohs-warning-mail', reqBody);
  }

  sendResolvedMail(reqBody) {
    return this.restfulWebService.post('ohs-resolved-mail', reqBody);
  }

  takeCorrectiveAction(reqBody) {
    return this.restfulWebService.post('save-corrective-action', reqBody);
  }

  getCorrectiveActionDetail(id) {
    return this.restfulWebService.get(`corrective-action-detail/${id}`);
  }

  repairAssessment(reqBody) {
    return this.restfulWebService.post('save-repair-assessment', reqBody);
  }

  downloadPdf(id) {
    window.open(`${this.restfulWebService.baseUrl}generate-ohs-pdf/${id}`);
  }

  //GET PROBATIONARY LIST
  getProbationaryList() {
    return this.restfulWebService.get(`get-probationary-meeting`);
  }

  //GET PARTICULAR PROBATIONARY DETAIL
  getProbationDetail(id) {
    return this.restfulWebService.get(`probationary-meeting-detail/${id}`);
  }

  //PROBATION ACTION
  probationAction(probationData) {
    return this.restfulWebService.post(`save-probationary-meeting-result`, probationData);
  }

  //CREATE PROBATION
  createProbationMeeting(probationData) {
    return this.restfulWebService.post(`craete-update-probationary-meeting`, probationData);
  }

  //ADD PROBATION NOTES
  addProbationNotes(probationData) {
    return this.restfulWebService.post(`save-probationary-meeting-note`, probationData);
  }

  //ADD PROBATION NOTES
  cancelProbationMeeting(probationData) {
    return this.restfulWebService.put(`probationary-meeting-cancel`, probationData);
  }

  deleteProbationMeeting(id) {
    return this.restfulWebService.delete(`probationary-meeting-delete/${id}`);
  }

}
