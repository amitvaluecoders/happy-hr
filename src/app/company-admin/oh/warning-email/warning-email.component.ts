import { Component, OnInit } from '@angular/core';
// import { MdSnackBar, MdDialog, MdDialogRef } from '@angular/material';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { APPCONFIG } from '../../../config';
import { CoreService } from '../../../shared/services/core.service';
import { OhService } from "../oh.service";

@Component({
  selector: 'app-warning-email',
  templateUrl: './warning-email.component.html',
  styleUrls: ['./warning-email.component.scss']
})
export class WarningEmailComponent implements OnInit {

  public progress;
  isProcessing: boolean;
  ohs = { ohsID: '', emailBody: '', riskType: '' };

  constructor(public companyService: OhService, public coreService: CoreService, private router: Router,
    private route: ActivatedRoute) { }

  ngOnInit() {
    APPCONFIG.heading = "Warning email";
    this.progressBarSteps();


    if (this.route.snapshot.params['id']) {
      this.ohs.ohsID = this.route.snapshot.params['id'];
    }
  }

  progressBarSteps() {
    this.progress = {
      progress_percent_value: 60, //progress percent
      total_steps: 5,
      current_step: 3,
      circle_container_width: 20, //circle container width in percent
      steps: [
        // {num:1,data:"Select Role"},
        { num: 1, data: "Incident reported" },
        { num: 2, data: "OH&S or company admin is notified" },
        { num: 3, data: "Warning email", active: true },
        { num: 4, data: "Corrective actions" },
        { num: 5, data: "Repair assessment" }
      ]
    }
  }

  sendWarningMail() {
    if (!this.ohs.emailBody.trim().length) return;
    this.isProcessing = true;
    this.companyService.sendWarningMail(this.ohs).subscribe(
      res => {
        this.isProcessing = false;
        if (res.code == 200) {
          this.coreService.notify("Successful", res.message, 1);
          this.router.navigate(['/app/company-admin/ohs/corrective-actions', this.ohs.ohsID]);
        }
        else return this.coreService.notify("Unsuccessful", res.message, 0);
      },
      err => {
        this.isProcessing = false;
        if (err.code == 400) {
          return this.coreService.notify("Unsuccessful", err.message, 0);
        }
        this.coreService.notify("Unsuccessful", "Error while getting data.", 0)
      }
    )
  }
}
