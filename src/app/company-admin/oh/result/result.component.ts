import { Component, OnInit } from '@angular/core';
import { APPCONFIG } from '../../../config';
import { Router, ActivatedRoute } from '@angular/router';
import { CoreService } from '../../../shared/services/core.service';
import { OhService } from "../oh.service";
import { Ohs } from "../ohs";
import * as moment from 'moment';

@Component({
    selector: 'app-result',
    templateUrl: './result.component.html',
    styleUrls: ['./result.component.scss']
})
export class ResultComponent implements OnInit {

    public progress;
    isProcessing: boolean;
    noResult = true;
    ohs = new Ohs;

    constructor(public companyService: OhService, public coreService: CoreService, private router: Router,
        private route: ActivatedRoute) { }

    ngOnInit() {
        APPCONFIG.heading = "OH&S result";
        this.progressBarSteps();

        if (this.route.snapshot.params['id']) {
            this.ohs.ohsID = this.route.snapshot.params['id'];
            this.getIncidentDetail();
        }
    }
    formateDate(v){
        return moment(v).calendar()? moment(v).format('DD MMM YYYY') : 'Not specified';
    }

    progressBarSteps() {
        this.progress = {
            progress_percent_value: 100, //progress percent
            total_steps: 5,
            current_step: 5,
            circle_container_width: 20, //circle container width in percent
            steps: [
                // {num:1,data:"Select Role"},
                { num: 1, data: "Incident reported" },
                { num: 2, data: "OH&S or company admin is notified" },
                { num: 3, data: "Warning email" },
                { num: 4, data: "Corrective actions" },
                { num: 5, data: "Repair assessment", active: true }
            ]
        }
    }

    getIncidentDetail() {
        this.isProcessing = true;
        this.companyService.getOhsDetail(this.ohs.ohsID).subscribe(
            res => {
                this.isProcessing = false;
                if (res.code == 200) {
                    this.ohs = res.data;
                    this.noResult = false;
                    if (this.ohs.injuryDetail) {
                        this.setMarker();
                    }
                }
                else return this.coreService.notify("Unsuccessful", res.message, 0);
            },
            err => {
                this.isProcessing = false;
                if (err.code == 400) {
                    return this.coreService.notify("Unsuccessful", err.message, 0);
                }
                this.coreService.notify("Unsuccessful", "Error while getting data.", 0)
            }
        );
    }

    // formatDateTime(date) {
    //     return moment(date).isValid() ? moment(date).format('YYYY-MM-DD hh:mm A') : 'Not specified';
    // }

    getName(url: string) {
        return url.split('/').pop();
    }

    setMarker() {
        let marker = this.ohs.injuryDetail.marker;
        if (marker.length) {
            for (let i = 0; i < marker.length; i++) {
                let mark = $('<i></i>');
                mark.addClass('fa fa-circle marker');
                mark.css('top', marker[i].posY);
                mark.css('left', marker[i].posX);
                mark.css('position', 'absolute');
                mark.css('color', '#ff8383');
                mark.attr('id', 'mark-' + i);

                let span = $('<span></span>');
                span.css('color', '#000');
                span.html((i + 1).toString());

                mark.append(span);
                mark.appendTo('#container');
            }
        }
    }

    downloadPDF() {
        this.companyService.downloadPdf(this.ohs.ohsID);
    }
}
