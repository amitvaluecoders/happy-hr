import { Component, OnInit } from '@angular/core';
import { MdSnackBar, MdDialog, MdDialogRef } from '@angular/material';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { APPCONFIG } from '../../../config';
import { CoreService } from '../../../shared/services/core.service';
import { OhService } from "../oh.service";
import { Ohs } from "../ohs";
import { IDatePickerConfig } from 'ng2-date-picker';
import * as moment from "moment";

@Component({
  selector: 'app-repair-assessment',
  templateUrl: './repair-assessment.component.html',
  styleUrls: ['./repair-assessment.component.scss']
})
export class RepairAssessmentComponent implements OnInit {

  public progress;
  isProcessing = false;
  isProcessingForm = false;
  noResult = true;
  ohs = new Ohs;
  config: IDatePickerConfig = {};
  // assessmentTime;
  repair = {
    "correctiveActionID": "", "ohsID": "", "assessmentNote": "", "assessmentTime": "", "assessmentStatus": ""
  }

  constructor(public companyService: OhService, public coreService: CoreService, private router: Router,
    private route: ActivatedRoute, public dialog: MdDialog) {
    this.config.locale = "en";
    this.config.format = "DD MMM, YYYY hh:mm A";
    this.config.showMultipleYearsNavigation = true;
    this.config.weekDayFormat = 'dd';
    this.config.disableKeypress = true;
    this.config.monthFormat = "MMM YYYY";
    this.config.max = moment();
  }

  ngOnInit() {
    APPCONFIG.heading = "Corrective ctions";
    this.progressBarSteps();

    if (this.route.snapshot.params['id']) {
      this.ohs['ohsID'] = this.route.snapshot.params['id'];
      this.getCorrectiveActionDetail();
    }
  }

  progressBarSteps() {
    this.progress = {
      progress_percent_value: 100, //progress percent
      total_steps: 5,
      current_step: 5,
      circle_container_width: 20, //circle container width in percent
      steps: [
        { num: 1, data: "Incident reported" },
        { num: 2, data: "OH&S or company admin is notified" },
        { num: 3, data: "Warning email" },
        { num: 4, data: "Corrective actions" },
        { num: 5, data: "Repair assessment", active: true }
      ]
    }
  }

  getCorrectiveActionDetail() {
    this.isProcessing = true;
    this.companyService.getOhsDetail(this.ohs['ohsID']).subscribe(
      res => {
        this.isProcessing = false;
        if (res.code == 200) {
          this.ohs = res.data;
          this.noResult = false;
        }
        else return this.coreService.notify("Unsuccessful", res.message, 0);
      },
      err => {
        this.isProcessing = false;
        if (err.code == 400) {
          return this.coreService.notify("Unsuccessful", err.message, 0);
        }
        this.coreService.notify("Unsuccessful", "Error while getting data.", 0)
      }
    )
  }

  submitAssessment() {
    this.repair.ohsID = this.ohs['ohsID'];
    // this.repair.assessmentTime = this.assessmentTime && moment(this.assessmentTime).isValid() ? moment(this.assessmentTime).format('DD-MM-YYYY HH:mm:ss') : '';
    this.repair.assessmentStatus = 'Fixed';

    this.isProcessingForm = true;
    this.companyService.repairAssessment(this.repair).subscribe(
      res => {
        this.isProcessingForm = false;
        if (res.code == 200) {
          this.coreService.notify("Successful", res.message, 1);
          this.router.navigate(['/app/company-admin/ohs/result', this.ohs['ohsID']])
        }
        else return this.coreService.notify("Unsuccessful", res.message, 0);
      },
      err => {
        this.isProcessingForm = false;
        if (err.code == 400) {
          return this.coreService.notify("Unsuccessful", err.message, 0);
        }
        this.coreService.notify("Unsuccessful", "Error while submitting data.", 0)
      }
    )
  }

  onFixed(isValid) {
    if (!isValid) return;
    let dialogRef = this.dialog.open(ResolvedMailDialog, { width: '500px' });
    dialogRef.componentInstance.ohsID = this.ohs['ohsID'];
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.submitAssessment();
      }
    })
  }

}


@Component({
  selector: 'resolved-mail-dialog',
  template: `
    <div md-dialog-content style="padding-bottom: 20px;">
      <div class="correctiveActions text-center">
        <form name="mailform" #mailform="ngForm" (ngSubmit)="sendResolvedMail()">
          <div class="col-sm-12 form-group">
             <h4 class="heading">Email to everyone</h4>
              
              <div>
                  <textarea name="message" #message="ngModel" [(ngModel)]="emailBody" class="form-control"></textarea>
                 <!-- <div *ngIf="message.dirty || message.touched || mailform.submitted">
                      <div class="alert alert-danger" [hidden]="message.valid">Please enter a message.</div>
                  </div> -->
              </div>
          </div>
          <md-progress-spinner *ngIf="isProcessing" mode="indeterminate" color=""></md-progress-spinner>
          <div *ngIf="!isProcessing" class="col-md-12 form-group">
              <div class="btnwrap text-center">
                  <button type="submit" class="btn btn-lg btn-primary">Send</button>
                  <div class="bold text-center" style="margin: 11px 0 10px;"> <strong>OR</strong> </div>
                  <button type="submit" class="btn btn-lg btn-primary">Complete</button>
              </div>
          </div>
        </form>
      </div>
    </div>
               `,
  styleUrls: ['../ohs-company-admin-notified/ohs-company-admin-notified.component.scss']
})
export class ResolvedMailDialog implements OnInit {

  ohsID = "";
  emailBody = '';
  isProcessing = false;

  constructor(public companyService: OhService, public coreService: CoreService, public dialogRef: MdDialogRef<ResolvedMailDialog>) { }

  ngOnInit() { }

  sendResolvedMail() {
    if (!this.emailBody.trim().length) return this.dialogRef.close(true);
    this.isProcessing = true;
    let reqBody = { ohsID: this.ohsID, emailBody: this.emailBody };
    this.companyService.sendResolvedMail(reqBody).subscribe(
      res => {
        this.isProcessing = false;
        this.dialogRef.close(true);
        if (res.code == 200) {
          this.coreService.notify("Successful", res.message, 1);
        }
        else return this.coreService.notify("Unsuccessful", res.message, 0);
      },
      err => {
        this.isProcessing = false;
        this.dialogRef.close(false);
        if (err.code == 400) {
          return this.coreService.notify("Unsuccessful", err.message, 0);
        }
        this.coreService.notify("Unsuccessful", "Error while sending email.", 0)
      }
    )
  }
}