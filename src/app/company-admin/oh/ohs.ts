export class Ohs {
    "ohsID" = ""; "title" = ""; "issueType" = ""; "description" = ""; "location" = ""; "issueDate" = ""; "status" = "";
    "witness" = []; "document" = []; "personInvolved" = []; "correctiveAction" = []; "emails" = []; "issueOtherType" = "";
    "injuryDetail" = new InjuryDetail; reportedBy = new User;
}


export class InjuryDetail {
    "firstAid" = ""; "furtherAction" = []; "marker" = []; "nature" = ""; "requiringFirstAid" = ""; "servityHazard" = "";
    "workTimeOff" = ""; "takenToDoctoruserID" = new User;
}

export class User {
    "name": ''; "imageUrl": ''; "position": ''
}