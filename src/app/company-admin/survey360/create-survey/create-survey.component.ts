import { Component, OnInit, ViewChild, ElementRef, ViewContainerRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { CoreService } from '../../../shared/services/core.service';
import { SurveyService } from '../survey.service';
import { APPCONFIG } from '../../../config';
import { IDatePickerConfig } from 'ng2-date-picker';
import * as moment from "moment";
import { ConfirmService } from '../../../shared/services/confirm.service';

@Component({
    selector: 'app-create-survey',
    templateUrl: './create-survey.component.html',
    styleUrls: ['./create-survey.component.scss']
})
export class CreateSurveyComponent implements OnInit {
    employeeList = [];
    collapse = {};
    selectedEmployee = [];
    dropdownSettings = {};
    selectedQuestion = [];
    dropdownSettings2 = {};
    survey = {
        companySurveyID: "", title: "", status: "", question: [], employeeInvite: []
    };
    surveyID: string = null;
    // surveyQuestions = [];
    superAdminQuestions = [];
    questionBox = { editMode: false, questionID: '', questionTitle: '', surveyQuestionerID: '' };
    isProcessing = false;
    isSaving = false;
    anonymous=false;
    isSending = false;
    editable = {};
    options = [];
    completionDate: any = "";
    active = false;
    noRecord: boolean = false;
    config: IDatePickerConfig = {};
    heading = 'Create 360 degree survey';

    @ViewChild('surveyName') surveyName: ElementRef;
    constructor(
        public coreService: CoreService, private router: Router, private route: ActivatedRoute,
        public surveyService: SurveyService, public confirmService: ConfirmService, public viewContainerRef: ViewContainerRef) {

        this.config.locale = "en";
        this.config.format = "DD MMM, YYYY";
        this.config.showMultipleYearsNavigation = true;
        this.config.weekDayFormat = 'dd';
        this.config.disableKeypress = true;
        this.config.monthFormat = "MMM YYYY";
        this.config.min = moment();
    }

    ngOnInit() {
        this.getQuestions();
        this.getEmployee();

        if (this.route.snapshot.params['id']) {
            this.heading = 'Edit 360 degree survey';
            this.surveyID = this.route.snapshot.params['id'];
            this.getSurveyDetail();
        }
        APPCONFIG.heading = "360 degree survey";
        this.dropdownSettings2 = {
            singleSelection: true,
            text: "Choose a question",
            enableSearchFilter: true,
            classes: "myclass"
        };
        this.dropdownSettings = {
            singleSelection: false,
            text: "Select user",
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            enableSearchFilter: true,
            classes: "myclass"
        };
        $('.list-area ul').addClass('listItems');
        $('.listItems').height('150px');
    }

    getQuestions() {
        // this.isProcessing = true;
        this.surveyService.getQuestions().subscribe(
            res => {
                // this.isProcessing = false;
                if (res.code == 200) {
                    this.superAdminQuestions = res.data;
                    this.superAdminQuestions.filter((item, i) => { item['itemName'] = item.title; item['id'] = item.surveyQuetionnerID; });
                }
                else return this.coreService.notify("Unsuccessful", res.message, 0);
            },
            err => {
                // this.isProcessing = false;
                if (err.code == 400) {
                    return this.coreService.notify("Unsuccessful", err.message, 0);
                }
                this.coreService.notify("Unsuccessful", "Error while getting questions.", 0);
            }
        )
    }

    getSurveyDetail() {
        this.isProcessing = true;
        this.surveyService.getSurveyDetail(this.surveyID).subscribe(
            res => {
                this.isProcessing = false;
                if (res.code == 200) {
                    this.survey = res.data;
                    this.anonymous= (res.data.anonymous=='Yes') ? true:false;
                    this.survey.question = res.data.question;
                    this.noRecord = this.survey.question.length == 0;
                }
                else return this.coreService.notify("Unsuccessful", res.message, 0);
            },
            err => {
                this.isProcessing = false;
                if (err.code == 400) {
                    return this.coreService.notify("Unsuccessful", err.message, 0);
                }
                this.coreService.notify("Unsuccessful", "Error while getting data.", 0);
            }
        )
    }

    onSaveQuestion() {
        if (!this.questionBox.questionTitle || !this.questionBox.questionTitle.trim().length) return;
        this.survey.question.push(this.questionBox);
        this.resetQuestionBox(false);
    }

    // onSave(newQuestion, data, isSaving) {
    //     if (this.survey.title.trim() == '') {
    //         this.coreService.notify("Warning!", "Please enter the survey name", 0);
    //         return this.surveyName.nativeElement.focus();
    //     }

    //     if (!data.questionTitle || data.questionTitle.trim() == "") return;

    //     let qsn = [];
    //     if (data) {
    //         if (newQuestion) {
    //             qsn.push({ questionID: '', questionTitle: data.questionTitle, surveyQuestionerID: data.questionID });
    //         } else {
    //             qsn.push({ questionID: data.questionID, questionTitle: data.questionTitle, surveyQuestionerID: '' });
    //         }
    //     }
    //     this.saveSurvey(qsn, false);
    // }

    saveSurvey() {
        if (!this.survey.title || !this.survey.title.trim().length) {
            this.coreService.notify("Warning!", "Please enter the survey name", 0);
            return this.surveyName.nativeElement.focus();
        }

        this.isSaving = true;
        // let ano;
        // if(this.anonymous==true){
        //     ano='Yes';
        // }
        // else{
        //     ano='No';
        // }
        let reqBody = {
            "companySurveyID": (!this.surveyID) ? this.survey.companySurveyID : this.surveyID,
            "title": this.survey.title, "question": this.survey.question,
            "anonymous" :(this.anonymous==true) ? 'Yes' : 'No',
        }
        this.surveyService.createSurvey(reqBody).subscribe(
            res => {
                this.isSaving = false;
                if (res.code == 200) {
                    this.coreService.notify("Successful", res.message, 1);
                    this.router.navigate(['/app/company-admin/survey']);

                    // this.survey = this.coreService.copyObject(this.survey, res.data);
                    // this.surveyID = (!this.survey.companySurveyID) ? this.surveyID : this.survey.companySurveyID;
                    // this.noRecord = this.survey.question.length == 0;
                    // this.resetQuestionBox(false);
                }
                else return this.coreService.notify("Unsuccessful", res.message, 0);
            },
            err => {
                this.isSaving = false;
                this.coreService.notify("Unsuccessful", err.message, 0);
            }
        )
    }

    resetQuestionBox(editMode) {
        this.selectedQuestion = [];
        this.questionBox = {
            editMode: editMode,
            questionID: "",
            questionTitle: "",
            surveyQuestionerID: ""
        };
    }

    onSelectQuestion(item) {
        // let qsn = this.superAdminQuestions.filter(i => i.surveyQuetionnerID == surveyQuetionnerID).shift();
        this.questionBox.questionID = '';
        this.questionBox.questionTitle = item.title;
        this.questionBox.surveyQuestionerID = item.surveyQuetionnerID
    }

    onDeSelectQuestion(item) {
        this.resetQuestionBox(true);
    }

    deleteQuestion(qsn, index) {
        this.confirmService.confirm("Delete Confirmation", "Do you want to delete this question?", this.viewContainerRef).subscribe(
            confirm => {
                if (confirm) {
                    qsn['isProcessing'] = true;
                    let params = { "questionIDs": [this.survey.question[index].questionID], "companySurveyID": this.surveyID }
                    this.surveyService.deleteQuestion(JSON.stringify(params)).subscribe(
                        res => {
                            qsn['isProcessing'] = false;
                            if (res.code == 200) {
                                this.survey.question.splice(index, 1);
                                this.coreService.notify('Successful', res.message, 1);
                            }
                        },
                        err => {
                            qsn['isProcessing'] = false;
                            this.coreService.notify("Unsuccessful", err.message, 0);
                        }
                    )
                }
            }
        )
    }

    onChangeSwitch(event) {
        this.active = event.target.checked;
        this.completionDate = moment();
    }

    getEmployee() {
        this.surveyService.getEmployee().subscribe(
            res => {
                if (res.code == 200) {
                    this.employeeList = res.data;
                    this.employeeList.filter((item, i) => { item['itemName'] = item.name; item['id'] = i; });
                    setTimeout(() => {
                        this.employeeList.filter(function (item, i) {
                            $('.listItems li').eq(i).prepend(`<img src="${item.imageUrl}" alt="" />`);
                            $('.listItems li').eq(i).children('label').append('<span>' + item.role + '</span>');
                        });
                    }, 0);
                }
                else return this.coreService.notify("Unsuccessful", res.message, 0);
            },
            err => {
                this.coreService.notify("Unsuccessful", err.message, 0);
            })
    }

    sendSurvey() {
        if (this.selectedEmployee.length == 0) return;
        if (!this.surveyID) return this.coreService.notify("Unsuccessful", "No survey is selected.", 0);
        let empIds = this.selectedEmployee.map((i) => { return i.userId });
        let reqBody = {
            "active": this.active, "companySurveyID": this.surveyID,
            "completionDate": this.active ? moment(this.completionDate).format('YYYY-MM-DD') : null,
            "inviteEmployee": empIds,
        };
        console.log("req-->",reqBody);
        this.isSending = true;
        this.surveyService.sendSurvey(reqBody).subscribe(
            res => {
                this.isSending = false;
                if (res.code == 200) {
                    this.coreService.notify("Successful", res.message, 1);
                    this.router.navigate(['/app/company-admin/survey']);
                }
                else return this.coreService.notify("Unsuccessful", res.message, 0);
            },
            err => {
                this.isSending = false;
                if (err.code == 400) {
                    return this.coreService.notify("Unsuccessful", err.message, 0);
                }
                this.coreService.notify("Unsuccessful", "Error while sending survey.", 0);
            }
        )
    }
    toggle(){
        this.anonymous = !this.anonymous;
     }

}
