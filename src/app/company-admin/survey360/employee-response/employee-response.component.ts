import { Component, OnInit } from '@angular/core';
import { APPCONFIG } from '../../../config';
import { Router, ActivatedRoute } from "@angular/router";
import { SurveyService } from "../survey.service";
import { CoreService } from "../../../shared/services/core.service";

@Component({
  selector: 'app-employee-response',
  templateUrl: './employee-response.component.html',
  styleUrls: ['./employee-response.component.scss']
})
export class EmployeeResponseComponent implements OnInit {
  isProcessing: boolean;
  empResponse = {
    "data": [],
    "assignedBy": { "userID": "", "name": "", "imageUrl": "", "position": "" },
    "assigndTo": { "userID": "", "name": "", "imageUrl": "", "position": "" },
  };
  companySurveyID = '';
  employeeID = '';
  noResult = true;

  constructor(public surveyService: SurveyService, public router: Router,
    public route: ActivatedRoute, public coreService: CoreService) { }

  ngOnInit() {
    APPCONFIG.heading = "360 degree survey";
    if (this.route.snapshot.params['sid'] && this.route.snapshot.params['eid']) {
      this.companySurveyID = this.route.snapshot.params['sid'];
      this.employeeID = this.route.snapshot.params['eid'];
      this.getEmployeeResponse();
    }
  }

  getEmployeeResponse() {
    this.isProcessing = true;
    let params = { "companySurveyID": this.companySurveyID, "employeeID": this.employeeID }
    this.surveyService.getEmployeeResponse(JSON.stringify(params)).subscribe(
      res => {
        this.isProcessing = false;
        if (res.code == 200) {
          if (!res.data) return this.noResult = true;
          this.empResponse = res.data;
          this.noResult = false;
        }
        else return this.coreService.notify("Unsuccessful", res.message, 0);
      },
      err => {
        this.isProcessing = false;
        if (err.code == 400) {
          return this.coreService.notify("Unsuccessful", err.message, 0);
        }
        this.coreService.notify("Unsuccessful", "Error while getting data.", 0)
      }
    )
  }

}
