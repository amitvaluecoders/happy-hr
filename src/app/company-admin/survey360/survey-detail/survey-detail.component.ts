import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { CoreService } from '../../../shared/services/core.service';
import { SurveyService } from '../survey.service';
import { APPCONFIG } from '../../../config';
import { IDatePickerConfig } from 'ng2-date-picker';
import * as moment from "moment";

@Component({
    selector: 'app-survey-detail',
    templateUrl: './survey-detail.component.html',
    styleUrls: ['./survey-detail.component.scss']
})
export class SurveyDetailComponent implements OnInit {
    employeeList = [];
    selectedEmployee = [];
    dropdownSettings = {};
    survey = {
        companySurveyID: "", title: "", status: "", question: [], employeeInvite: []
    };
    surveyID: string = null;
    surveyList = [];
    isProcessing: boolean;
    isSending: boolean;
    // options = [];
    completionDate: any = "";
    active = false;
    noRecord: boolean = false;
    config: IDatePickerConfig = {};
    permission;
    surveyResponce=[];

    constructor(
        public coreService: CoreService,
        private router: Router,
        private route: ActivatedRoute,
        public surveyService: SurveyService) {

        this.config.locale = "en";
        this.config.format = "DD MMM, YYYY";
        this.config.showMultipleYearsNavigation = true;
        this.config.weekDayFormat = 'dd';
        this.config.disableKeypress = true;
        this.config.monthFormat = "MMM YYYY";
        this.config.min = moment();
    }

    ngOnInit() {
        APPCONFIG.heading = "360 degree survey";
        this.permission = this.coreService.getNonRoutePermission();

        if (this.route.snapshot.params['id']) {
            this.surveyID = this.route.snapshot.params['id'];
            this.getSurveyDetail();
        }
        this.getEmployee()
        this.dropdownSettings = {
            singleSelection: false,
            text: "Select Name",
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            enableSearchFilter: true,
            classes: "myclass"
        };
        $('.list-area ul').addClass('listItems');
    }

    getSurveyDetail() {
        this.isProcessing = true;
        this.surveyService.getSurveyDetail(this.surveyID).subscribe(
            res => {
                this.isProcessing = false;
                if (res.code == 200) {
                    this.survey = res.data;
                    this.survey.employeeInvite.filter(item=>{
                        if(item.employeeResponse.length>0){
                            this.surveyResponce.push(item);
                        }
                    })
                    this.noRecord = this.survey.question.length == 0 ? true : false;
                }
                else return this.coreService.notify("Unsuccessful", res.message, 0);
            },
            err => {
                this.isProcessing = false;
                if (err.code == 400) {
                    return this.coreService.notify("Unsuccessful", err.message, 0);
                }
                this.coreService.notify("Unsuccessful", "Error while getting data.", 0)
            });
    }

    // onCheckQuestion(event, id) {
    //     let ind = this.options.indexOf(id);

    //     if (event.target.checked) {
    //         (ind >= 0) ? null : this.options.push(id);
    //     } else {
    //         (ind >= 0) ? this.options.splice(this.options.indexOf(id), 1) : null;
    //     }
    //     console.log(event.target.checked, ind, this.options)
    // }

    // deleteQuestion() {
    //     this.isProcessing = true;
    //     let params = { "questionIDs": this.options, "companySurveyID": this.surveyID }
    //     this.surveyService.deleteQuestion(JSON.stringify(params)).subscribe(
    //         res => {
    //             this.isProcessing = false;
    //             if (res.code == 200) {
    //                 for (let i = 0; i < this.options.length; i++) {
    //                     let index = this.survey.question.findIndex(k => k.questionID == this.options[i])
    //                     this.survey.question.splice(index, 1);
    //                 }
    //                 this.options = []
    //                 this.coreService.notify('Successful', 'question is deleted successfully', 1);
    //             }
    //             else return this.coreService.notify("Unsuccessful", res.message, 0);
    //         },
    //         err => {
    //             this.isProcessing = false;
    //             if (err.code == 400) {
    //                 return this.coreService.notify("Unsuccessful", err.message, 0);
    //             }
    //             this.coreService.notify('Unsuccessful', 'Error while deleting question', 0)
    //         })

    // }

    onChangeSwitch(event) {
        this.active = event.target.checked;
        this.completionDate = moment();
    }

    getEmployee() {
        this.surveyService.getEmployee().subscribe(
            res => {
                if (res.code == 200) {
                    this.employeeList = res.data;
                    this.employeeList.filter((item, i) => { item['itemName'] = item.name; item['id'] = i; });
                    setTimeout(() => {
                        this.employeeList.filter(function (item, i) {
                            $('.listItems li').eq(i).prepend(`<img src="${item.imageUrl}" alt="" />`);
                            $('.listItems li').eq(i).children('label').append('<span>' + item.role + '</span>');
                        });
                    }, 0);
                }
                else return this.coreService.notify("Unsuccessful", res.message, 0);
            },
            err => {
                if (err.code == 400) {
                    return this.coreService.notify("Unsuccessful", err.message, 0);
                }
                this.coreService.notify("Unsuccessful", "Error while getting employee.", 0);
            })
    }

    sendSurvey() {
        if (!this.surveyID || this.selectedEmployee.length == 0) return;
        let empIds = this.selectedEmployee.map((i) => { return i.userId });
        let reqBody = {
            "active": this.active, "companySurveyID": this.surveyID,
            "completionDate": this.active ? moment(this.completionDate).format('YYYY-MM-DD') : '',
            "inviteEmployee": empIds
        };

        this.isSending = true;
        this.surveyService.sendSurvey(reqBody).subscribe(
            res => {
                this.isSending = false;
                if (res.code == 200) {
                    this.coreService.notify("Successful", res.message, 1);
                    this.router.navigate(['/app/company-admin/survey']);
                }
                else return this.coreService.notify("Unsuccessful", res.message, 0);
            },
            err => {
                this.isSending = false;
                if (err.code == 400) {
                    return this.coreService.notify("Unsuccessful", err.message, 0);
                }
                this.coreService.notify("Unsuccessful", "Error while sending survey.", 0);
            }
        )
    }
}
