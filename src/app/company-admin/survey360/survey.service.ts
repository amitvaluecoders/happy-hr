import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { RestfulLaravelService } from '../../shared/services';

@Injectable()
export class SurveyService {

  constructor(private restfulLaravelService: RestfulLaravelService) { }

  getQuestions() {
    // return this.restfulLaravelService.get('admin/list-survey-quetionner/1');
    return this.restfulLaravelService.get('list-admin-survey-quetionner/1');
  }

  getSurveyList() {
    return this.restfulLaravelService.get('list-survey');
  }

  getSurveyDetail(id: string) {
    return this.restfulLaravelService.get(`survey-detail/${id}`);
  }

  deleteSurvey(id: string) {
    return this.restfulLaravelService.delete(`delete-company-survey/${id}`);
  }

  deleteQuestion(param) {
    return this.restfulLaravelService.delete(`delete-company-survey-question/${param}`);
  }

  createSurvey(reqBody) {
    return this.restfulLaravelService.post('create-360survey', reqBody);
  }

  sendSurvey(reqBody) {
    return this.restfulLaravelService.post('save-employee-survey', reqBody);
  }

  getEmployee() {
    return this.restfulLaravelService.get('show-all-employee');
  }

  duplicateSurvey(reqBody) {
    return this.restfulLaravelService.post('duplicate-360survey', reqBody);
  }

  getEmployeeResponse(params) {
    return this.restfulLaravelService.get(`employee-response/${params}`);
  }

  getAllEmployeeResponse(params) {
    return this.restfulLaravelService.get(`all-employee-response/${params}`);
  }

}
