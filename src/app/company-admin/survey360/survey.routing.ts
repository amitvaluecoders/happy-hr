import { ModuleWithProviders } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SurveyListComponent } from './survey-list/survey-list.component';
import { SurveyDetailComponent } from './survey-detail/survey-detail.component';
import { EmployeeResponseComponent } from './employee-response/employee-response.component';
import { AllEmployeeResponseComponent } from './all-employee-response/all-employee-response.component';
import { CreateSurveyComponent } from './create-survey/create-survey.component';
import { Survey360GuardService } from "./survey360-guard.service";

export const SurveyPagesRoutes: Routes = [
    {
        path: '',
        canActivate: [],
        children: [

            { path: '', component: SurveyListComponent, canActivate: [Survey360GuardService] },
            { path: 'detail/:id', component: SurveyDetailComponent, canActivate: [Survey360GuardService] },
            { path: 'employee-response/:sid/:eid', component: EmployeeResponseComponent, canActivate: [Survey360GuardService] },
            { path: 'all-employee-response/:sid', component: AllEmployeeResponseComponent, canActivate: [Survey360GuardService] },
            { path: 'create-survey', component: CreateSurveyComponent, canActivate: [Survey360GuardService] },
            { path: 'edit/:id', component: CreateSurveyComponent, canActivate: [Survey360GuardService] }
        ]
    }
];

export const SurveyRoutingModule = RouterModule.forChild(SurveyPagesRoutes);
