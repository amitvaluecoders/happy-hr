import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { MyDatePickerModule } from 'mydatepicker';
import { SurveyRoutingModule } from './survey.routing';
import { DataTableModule } from "angular2-datatable";
import { SurveyService } from './survey.service';
import { SurveyListComponent, AddValueDialog } from './survey-list/survey-list.component';
import { SurveyDetailComponent } from './survey-detail/survey-detail.component';
import { AngularMultiSelectModule } from 'angular2-multiselect-checkbox-dropdown/angular2-multiselect-dropdown';
import { EmployeeResponseComponent } from './employee-response/employee-response.component';
import { AllEmployeeResponseComponent } from './all-employee-response/all-employee-response.component';
import { CreateSurveyComponent } from './create-survey/create-survey.component';
import { SharedModule } from '../../shared/shared.module';
import { MaterialModule } from '@angular/material';
import { Survey360GuardService } from './survey360-guard.service';
// import { InlineEditorModule } from 'ng2-inline-editor';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    SurveyRoutingModule,
    MyDatePickerModule,
    AngularMultiSelectModule,
    SharedModule,
    MaterialModule,
    DataTableModule,
    // InlineEditorModule
  ],
  entryComponents: [AddValueDialog],
  declarations: [SurveyListComponent, SurveyDetailComponent, EmployeeResponseComponent, AllEmployeeResponseComponent, CreateSurveyComponent, AddValueDialog],
  providers: [SurveyService,Survey360GuardService]
})
export class SurveyModule { }
