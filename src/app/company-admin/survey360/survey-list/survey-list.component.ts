import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { Router } from '@angular/router';
import { CoreService } from '../../../shared/services/core.service';
import { SurveyService } from '../survey.service';
import { APPCONFIG } from '../../../config';
import { MdSnackBar, MdDialog, MdDialogRef } from '@angular/material';
import { ConfirmService } from '../../../shared/services/confirm.service';
import * as moment from 'moment';

@Component({
  selector: 'app-survey-list',
  templateUrl: './survey-list.component.html',
  styleUrls: ['./survey-list.component.scss']
})
export class SurveyListComponent implements OnInit {
  surveyList = [];
  isProcessing: boolean;
  permission;

  constructor(public coreService: CoreService, private router: Router, public surveyService: SurveyService, public dialog: MdDialog,
    public confirmService: ConfirmService, public viewContainerRef: ViewContainerRef) { }

  ngOnInit() {
    APPCONFIG.heading = "360 degree survey";
    this.permission = this.coreService.getNonRoutePermission();
    this.getSurveyList();
  }

  getSurveyList() {
    this.isProcessing = true;
    this.surveyService.getSurveyList().subscribe(
      res => {
        if (res.code == 200) {
          this.surveyList = res.data;
          this.isProcessing = false;
        }
        else return this.coreService.notify("Unsuccessful", res.message, 0);
      },
      error => {
        this.isProcessing = false;
        if (error.code == 400) {
          return this.coreService.notify("Unsuccessful", error.message, 0);
        }
        this.coreService.notify("Unsuccessful", "Error while getting data.", 0)
      })
  }

  duplicateSurvey(survey) {
    let dialogRef = this.dialog.open(AddValueDialog, { width: '400px' });
    dialogRef.afterClosed().subscribe(duplicateSurveyName => {
      if (duplicateSurveyName) {
        this.isProcessing = true;
        let reqBody = { "companySurveyID": survey.companySurveyID, "duplicateSurveyName": duplicateSurveyName }
        this.surveyService.duplicateSurvey(reqBody).subscribe(
          res => {
            if (res.code == 200) {
              console.log(res);
              // this.surveyList.push(res.data);
              this.surveyList = res.data
              this.isProcessing = false;
              this.coreService.notify('Successful', "Duplicate survey has been created.", 1);
            }
            else return this.coreService.notify("Unsuccessful", res.message, 0);
          },
          error => {
            this.isProcessing = false;
            if (error.code == 400) {
              return this.coreService.notify("Unsuccessful", error.message, 0);
            }
            this.coreService.notify('Unsuccessful', "Error while creating duplicate.", 0);
          })
      }
    })
  }

  editSurvey(survey) {
    this.router.navigate([`/app/company-admin/survey/edit/${survey.companySurveyID}`]);
  }

  deleteSurvey(survey) {
    this.confirmService.confirm("Delete Confirmation", "Do you want to delete this survey?", this.viewContainerRef).subscribe(
      confirm => {
        if (confirm) {
          this.isProcessing = true;
          this.surveyService.deleteSurvey(survey.companySurveyID).subscribe(
            res => {
              if (res.code == 200) {
                let index = this.surveyList.findIndex(i => i.companySurveyID == survey.companySurveyID);
                this.surveyList.splice(index, 1);
                this.isProcessing = false;
                this.coreService.notify('Successful', res.message, 1);
              }
              else return this.coreService.notify("Unsuccessful", res.message, 0);
            },
            error => {
              this.isProcessing = false;
              if (error.code == 400) {
                return this.coreService.notify("Unsuccessful", error.message, 0);
              }
              this.coreService.notify("Unsuccessful", "Error while deleting survey.", 0);
            })
        }
      })
  }
  formatDate(date) {
    return moment(date).isValid() ? moment(date).format('DD MMM YYYY') : 'Not specified';
  }

  // formatDate(d) {
  //   let date = new Date(d);
  //   if (Object.prototype.toString.call(date) === "[object Date]") {
  //     // it is a date
  //     if (isNaN(date.getTime())) {
  //       return '--';
  //     }
  //     else {
  //       return new Date(date).toDateString();
  //     }
  //   }
  //   else {
  //     return '--';
  //   }
  // }

}


@Component({
  selector: 'add-value-dialog',
  template: `<section class="admin-panel"> <h3 md-dialog-title>Duplicate survey name</h3>
        <div md-dialog-content style="padding-bottom: 20px;">
         <form #qsnForm="ngForm" name="material_signup_form" class="md-form-auth form-validation" (ngSubmit)="qsnForm.valid && onSubmit(value)">  
            <div class="full-width form-group">
              <input mdInput  required rows="4" name="name" class="form-control" [(ngModel)]="value" #name="ngModel" placeholder="Enter name">
            </div> 
            <div class="alert alert-danger" *ngIf="name.invalid && qsnForm.submitted">
                Name is required !
            </div>
          </form>       
        </div>
        <div class="buttonWrap">
            <button style="margin-right:7px;" class="btn btn-primary" type="submit" [disabled]="qsnForm.invalid" (click)="qsnForm.valid && onSubmit(value)">Submit</button>
            <button class="btn btn-default" type="button" (click)="dialogRef.close(null)">Close</button>
        </div></section>`,
})
export class AddValueDialog implements OnInit {

  public value = "";
  constructor(public dialogRef: MdDialogRef<AddValueDialog>) { }

  ngOnInit() { }

  onSubmit(value) {
    this.dialogRef.close(value);
  }
}