import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AllEmployeeResponseComponent } from './all-employee-response.component';

describe('AllEmployeeResponseComponent', () => {
  let component: AllEmployeeResponseComponent;
  let fixture: ComponentFixture<AllEmployeeResponseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AllEmployeeResponseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AllEmployeeResponseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
