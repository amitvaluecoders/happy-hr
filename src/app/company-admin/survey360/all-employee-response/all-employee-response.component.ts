import { Component, OnInit } from '@angular/core';
import { APPCONFIG } from '../../../config';
import { Router, ActivatedRoute } from "@angular/router";
import { SurveyService } from "../survey.service";
import { CoreService } from "../../../shared/services/core.service";

@Component({
  selector: 'app-all-employee-response',
  templateUrl: './all-employee-response.component.html',
  styleUrls: ['./all-employee-response.component.scss']
})
export class AllEmployeeResponseComponent implements OnInit {
  isProcessing: boolean;
  empResponse = { "companySurveyID": '', "data": [] };

  constructor(public surveyService: SurveyService, public router: Router,
    public route: ActivatedRoute, public coreService: CoreService) { }

  ngOnInit() {
    APPCONFIG.heading = "360 degree survey response";
    if (this.route.snapshot.params['sid']) {
      this.empResponse.companySurveyID = this.route.snapshot.params['sid'];
      this.getEmployeeResponse();
    }
  }

  getEmployeeResponse() {
    this.isProcessing = true;
    this.surveyService.getAllEmployeeResponse(this.empResponse.companySurveyID).subscribe(
      res => {
        this.isProcessing = false;
        if (res.code == 200) {
          this.empResponse = res.data;
          this.empResponse.data.forEach((item,i)=>{
                item.answer.filter((item2,j)=>{
                   item.employeeInvite.filter((item3,k)=>{
                      if(item3.employee.userID==item2.assigndTo.userID){
                          item2.assigndTo['anonymous']= item3.anonymous;
                      }
                   })
                })
          })   
        }
        else return this.coreService.notify("Unsuccessful", res.message, 0);
      },
      err => {
        this.isProcessing = false;
        if (err.code == 400) {
          return this.coreService.notify("Unsuccessful", err.message, 0);
        }
        this.coreService.notify("Unsuccessful", "Error while getting data.", 0)
      }
    )
  }

}
