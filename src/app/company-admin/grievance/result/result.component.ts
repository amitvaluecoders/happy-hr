import { Component, OnInit } from '@angular/core';
import { APPCONFIG } from '../../../config';
import { Router, ActivatedRoute } from '@angular/router';
import { CoreService } from '../../../shared/services/core.service';
import { GrievanceService } from "../grievance.service";
import { Grievance } from "../grievance";
import * as moment from 'moment';

@Component({
  selector: 'app-result',
  templateUrl: './result.component.html',
  styleUrls: ['./result.component.scss']
})
export class ResultComponent implements OnInit {
  public progress;
  isProcessing: boolean;
  grievance: Grievance;
  noResult = true;

  constructor(public companyService: GrievanceService, public coreService: CoreService, private router: Router,
    private route: ActivatedRoute) { this.grievance = new Grievance; }

  ngOnInit() {
    APPCONFIG.heading = "Result";
    this.progressBarSteps();

    if (this.route.snapshot.params['id']) {
      this.grievance.grievanceID = this.route.snapshot.params['id'];
      this.getGrievanceDetail();
    }
  }

  progressBarSteps() {
    this.progress = {
      progress_percent_value: 100, //progress percent
      total_steps: 5,
      current_step: 5,
      circle_container_width: 20, //circle container width in percent
      steps: [
        // {num:1,data:"Select Role"},
        { num: 1, data: "Report grievance" },
        { num: 2, data: "Meeting with complainer" },
        { num: 3, data: "Meeting with grievance against" },
        { num: 4, data: "Meeting with both parties" },
        { num: 5, data: "Result", active: true }
      ]
    }
  }

  getGrievanceDetail() {
    this.isProcessing = true;
    this.companyService.getGrievanceDetail(this.grievance.grievanceID).subscribe(
      res => {
        this.isProcessing = false;
        if (res.code == 200) {
          this.grievance = res.data;
          this.noResult = false;
        }
        else return this.coreService.notify("Unsuccessful", res.message, 0);
      },
      err => {
        this.isProcessing = false;
        if (err.code == 400) {
          return this.coreService.notify("Unsuccessful", err.message, 0);
        }
        this.coreService.notify("Unsuccessful", "Error while getting data.", 0)
      }
    );
  }

  formatDateTime(date) {
    return moment(date).isValid() ? moment(date).format('DD MMM, YYYY hh:mm A') : 'Not specified';
  }
}
