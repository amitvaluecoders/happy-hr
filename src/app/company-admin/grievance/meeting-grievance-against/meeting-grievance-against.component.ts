import { Component, OnInit } from '@angular/core';
import { APPCONFIG } from '../../../config';
import { Router, ActivatedRoute } from '@angular/router';
import { CoreService } from '../../../shared/services/core.service';
import { GrievanceService } from "../grievance.service";
import { Grievance } from "../grievance";
import * as moment from 'moment';

@Component({
  selector: 'app-meeting-grievance-against',
  templateUrl: './meeting-grievance-against.component.html',
  styleUrls: ['./meeting-grievance-against.component.scss']
})
export class MeetingGrievanceAgainstComponent implements OnInit {
  public progress;
  isProcessing: boolean;
  isSubmitting: boolean;
  grievance: Grievance;
  managerNote = '';
  noResult = true;
  // datetime = '';
  permission;

  constructor(public companyService: GrievanceService, public coreService: CoreService, private router: Router,
    private route: ActivatedRoute) { this.grievance = new Grievance; }

  ngOnInit() {
    APPCONFIG.heading = "Second meeting with employee that has grievance against them";
    this.permission = this.coreService.getNonRoutePermission();
    this.progressBarSteps();

    // setInterval(() => {
    //   this.datetime = moment().format('DD MMM, YYYY HH:mm:ss');
    // }, 1000);

    if (this.route.snapshot.params['id']) {
      this.grievance.grievanceID = this.route.snapshot.params['id'];
      this.getGrievanceDetail();
    }
  }

  progressBarSteps() {
    this.progress = {
      progress_percent_value: 60, //progress percent
      total_steps: 5,
      current_step: 3,
      circle_container_width: 20, //circle container width in percent
      steps: [
        { num: 1, data: "Report grievance" },
        { num: 2, data: "Meeting with complainer" },
        { num: 3, data: "Meeting with grievance against", active: true },
        { num: 4, data: "Meeting with both parties" },
        { num: 5, data: "Result" }
      ]
    }
  }

  getGrievanceDetail() {
    this.isProcessing = true;
    this.companyService.getGrievanceDetail(this.grievance.grievanceID).subscribe(
      res => {
        this.isProcessing = false;
        if (res.code == 200) {
          this.grievance = res.data;
          this.noResult = false;
        }
        else return this.coreService.notify("Unsuccessful", res.message, 0);
      },
      err => {
        this.isProcessing = false;
        if (err.code == 400) {
          return this.coreService.notify("Unsuccessful", err.message, 0);
        }
        this.coreService.notify("Unsuccessful", "Error while getting data.", 0)
      }
    );
  }

  formatDateTime(date) {
    return moment(date).isValid() ? moment(date).format('DD MMM, YYYY hh:mm A') : 'Not specified';
  }

  submitNote() {
    this.isSubmitting = true;
    let reqBody = {
      grievanceID: this.grievance.grievanceID, stage: '2', notes: this.managerNote,
      noteForUser: this.grievance.raisedAgainst[0].userID, complainantNote: '', defendantNote: '',
      status: 'Under Investigation'
    };

    this.companyService.submitNote(reqBody).subscribe(
      res => {
        this.isSubmitting = false;
        if (res.code == 200) {
          this.coreService.notify("Successful", res.message, 1);
          this.router.navigate(['/app/company-admin/grievance/meeting-both-parties', this.grievance.grievanceID]);
        }
        else return this.coreService.notify("Unsuccessful", res.message, 0);
      },
      err => {
        this.isSubmitting = false;
        if (err.code == 400) {
          return this.coreService.notify("Unsuccessful", err.message, 0);
        }
        this.coreService.notify("Unsuccessful", "Error while submit.", 0)
      }
    )
  }

}
