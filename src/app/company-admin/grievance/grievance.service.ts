import { Injectable } from '@angular/core';
import { RestfulLaravelService } from "../../shared/services/restful-laravel.service";

@Injectable()
export class GrievanceService {

  constructor(private restfulWebService: RestfulLaravelService) { }

  getGrievanceList(params) {
    return this.restfulWebService.get(`list-grievance/${params}`);
  }

  getGrievanceDetail(id) {
    return this.restfulWebService.get(`grievance-detail/${id}`);
  }

  submitNote(reqBody) {
    return this.restfulWebService.post('post-meeting', reqBody)
  }

  getEmployee() {
    return this.restfulWebService.get(`show-all-employee`);
  }

  getManagers() {
    return this.restfulWebService.get(`show-manager-and-ceo`);
  }

  downloadPdf(id) {
    window.open(`${this.restfulWebService.baseUrl}generate-grievance-pdf/${id}`);
  }
}
