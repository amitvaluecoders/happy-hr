import { Injectable } from '@angular/core';
import { CanActivate, Router, RouterStateSnapshot, ActivatedRouteSnapshot } from "@angular/router";
import { CoreService } from '../../shared/services/core.service';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';

@Injectable()
export class GrievanceGuardService {

    public module = "grievance";

    constructor(private _core: CoreService, private router: Router) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        let url: string;
        if (route['_routeConfig']['path'])
            url = route['_routeConfig']['path']
        else url = state.url;
        if (this.checkHavePermission(url)) {
            return true;
        } else {
            this.router.navigate(['/extra/unauthorized']);
            return false;
        }
    }

    private checkHavePermission(path) {
        switch (path) {
            case "grievance-listing":
                return this._core.getModulePermission(this.module, 'view');
            case "/app/company-admin/grievance":
                return this._core.getModulePermission(this.module, 'view');
            case "detail/:id":
                return this._core.getModulePermission(this.module, 'view');
            case "meeting-complainer/:id":
                return this._core.getModulePermission(this.module, 'view');
            case "meeting-grievance-against/:id":
                return this._core.getModulePermission(this.module, 'view');
            case "meeting-both-parties/:id":
                return this._core.getModulePermission(this.module, 'view');
            case "result/:id":
                return this._core.getModulePermission(this.module, 'view');
            default:
                return false;
        }
    }
}

