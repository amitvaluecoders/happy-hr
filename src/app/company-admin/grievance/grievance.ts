
export class User {
    "userID" = "";
    "name" = "";
    "imageUrl" = "";
    "position" = "";
}

export class Grievance {
    "grievanceID" = "";
    "title" = "";
    "date" = "";
    "status" = "";
    "result" = "";
    "complainerNote" = "";
    "raisedBy" = new User;
    "raisedAgainst" = [];
    "manager" = [];
    "evidence" = [];
    "meeting" = [];
    "witness" = [];
}