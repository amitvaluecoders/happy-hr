import { Component, OnInit } from '@angular/core';
import { APPCONFIG } from '../../../config';
import { Router, ActivatedRoute } from '@angular/router';
import { CoreService } from '../../../shared/services/core.service';
import { GrievanceService } from "../grievance.service";
import { Grievance } from "../grievance";
import * as moment from 'moment';

@Component({
  selector: 'app-meeting-complainer',
  templateUrl: './meeting-complainer.component.html',
  styleUrls: ['./meeting-complainer.component.scss']
})
export class MeetingComplainerComponent implements OnInit {
  public progress;
  isProcessing: boolean;
  isSubmitting: boolean;
  grievance: Grievance;
  managerNote = '';
  noResult = true;
  // datetime = '';
  permission;

  constructor(public companyService: GrievanceService, public coreService: CoreService, private router: Router,
    private route: ActivatedRoute) { this.grievance = new Grievance; }

  ngOnInit() {
    APPCONFIG.heading = "Initial meeting with complainer";
    this.permission = this.coreService.getNonRoutePermission();
    this.progressBarSteps();

    // setInterval(() => {
    //   this.datetime = moment().format('DD MMM, YYYY HH:mm:ss');
    // }, 1000);

    if (this.route.snapshot.params['id']) {
      this.grievance.grievanceID = this.route.snapshot.params['id'];
      this.getGrievanceDetail();
    }
  }

  progressBarSteps() {
    this.progress = {
      progress_percent_value: 40, //progress percent
      total_steps: 5,
      current_step: 2,
      circle_container_width: 20, //circle container width in percent
      steps: [
        { num: 1, data: "Report grievance" },
        { num: 2, data: "Meeting with complainer", active: true },
        { num: 3, data: "Meeting with grievance against" },
        { num: 4, data: "Meeting with both parties" },
        { num: 5, data: "Result" }
      ]
    }
  }

  getGrievanceDetail() {
    this.isProcessing = true;
    this.companyService.getGrievanceDetail(this.grievance.grievanceID).subscribe(
      res => {
        this.isProcessing = false;
        if (res.code == 200) {
          this.grievance = res.data;
          this.noResult = false;
        }
        else return this.coreService.notify("Unsuccessful", res.message, 0);
      },
      err => {
        this.isProcessing = false;
        if (err.code == 400) {
          return this.coreService.notify("Unsuccessful", err.message, 0);
        }
        this.coreService.notify("Unsuccessful", "Error while getting data.", 0)
      }
    );
  }

  formatDateTime(date) {
    return moment(date).isValid() ? moment(date).format('DD MMM, YYYY hh:mm A') : 'Not specified';
  }

  submitNote(type) {
    this.isSubmitting = true;
    let reqBody = {
      grievanceID: this.grievance.grievanceID, stage: '1', notes: this.managerNote,
      noteForUser: this.grievance.raisedBy.userID, complainantNote: '', defendantNote: '',
      status: type == 'Resolved' ? type : 'Under Investigation'
    };

    this.companyService.submitNote(reqBody).subscribe(
      res => {
        this.isSubmitting = false;
        if (res.code == 200) {
          this.coreService.notify("Successful", res.message, 1);
          if (type == 'Resolved') {
            return this.router.navigate(['/app/company-admin/listing-page/grievance-listing']);
          }
          this.router.navigate(['/app/company-admin/grievance/meeting-grievance-against', this.grievance.grievanceID]);
        }
        else return this.coreService.notify("Unsuccessful", res.message, 0);
      },
      err => {
        this.isSubmitting = false;
        if (err.code == 400) {
          return this.coreService.notify("Unsuccessful", err.message, 0);
        }
        this.coreService.notify("Unsuccessful", "Error while submit.", 0)
      }
    )
  }

}
