import { Component, OnInit } from '@angular/core';
import { APPCONFIG } from '../../../config';
import { Router, ActivatedRoute } from '@angular/router';
import { CoreService } from '../../../shared/services/core.service';
import { GrievanceService } from "../grievance.service";
import { Grievance } from "../grievance";
import * as moment from 'moment';

@Component({
  selector: 'app-grievance',
  templateUrl: './grievance.component.html',
  styleUrls: ['./grievance.component.scss']
})
export class GrievanceComponent implements OnInit {
  isProcessing: boolean;
  grievance: Grievance;
  noResult = true;

  constructor(public companyService: GrievanceService, public coreService: CoreService, private router: Router,
    private route: ActivatedRoute) { this.grievance = new Grievance; }

  ngOnInit() {
    APPCONFIG.heading = "Grievance detail";

    if (this.route.snapshot.params['id']) {
      this.grievance.grievanceID = this.route.snapshot.params['id'];
      this.getGrievanceDetail();
    }
  }

  getGrievanceDetail() {
    this.isProcessing = true;
    this.companyService.getGrievanceDetail(this.grievance.grievanceID).subscribe(
      res => {
        this.isProcessing = false;
        if (res.code == 200) {
          this.grievance = res.data;
          this.noResult = false;
        }
        else return this.coreService.notify("Unsuccessful", res.message, 0);
      },
      err => {
        this.isProcessing = false;
        if (err.code == 400) {
          return this.coreService.notify("Unsuccessful", err.message, 0);
        }
        this.coreService.notify("Unsuccessful", "Error while getting data.", 0)
      }
    );
  }

  formatDateTime(date) {
    return moment(date).isValid() ? moment(date).format('DD MMM, YYYY hh:mm A') : 'Not specified';
  }

  downloadPDF(){
    this.companyService.downloadPdf(this.grievance.grievanceID);
  }

}
