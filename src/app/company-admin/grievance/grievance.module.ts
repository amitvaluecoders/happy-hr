import { NgModule } from '@angular/core';
import { MaterialModule } from '@angular/material';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { GrievanceRoutingModule } from './grievance.routing';
import { GrievanceService } from './grievance.service';
import { GrievanceComponent } from './grievance/grievance.component';
// import { StepsComponent } from '../../shared/components/steps/steps.component';
import { SharedModule } from '../../shared/shared.module';
import { AngularMultiSelectModule } from 'angular2-multiselect-checkbox-dropdown/angular2-multiselect-dropdown';
import { MeetingComplainerComponent } from './meeting-complainer/meeting-complainer.component';
import { MeetingGrievanceAgainstComponent } from './meeting-grievance-against/meeting-grievance-against.component';
import { MeetingBothPartiesComponent } from './meeting-both-parties/meeting-both-parties.component';
import { ResultComponent } from './result/result.component';
import { NguiDatetimePickerModule } from '@ngui/datetime-picker';
// import { GrievanceListingComponent } from "../company-module-listing/grievance-listing/grievance-listing.component";
import { GrievanceGuardService } from './grievance-guard.service';

@NgModule({
  imports: [
    CommonModule,
    MaterialModule,
    GrievanceRoutingModule,
    SharedModule,
    FormsModule,
    AngularMultiSelectModule,
    NguiDatetimePickerModule
  ],
  declarations: [GrievanceComponent, MeetingComplainerComponent, MeetingGrievanceAgainstComponent,
    MeetingBothPartiesComponent, ResultComponent],
  providers: [GrievanceService,GrievanceGuardService],
})
export class GrievanceModule { }
