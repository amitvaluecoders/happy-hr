import { ModuleWithProviders } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { GrievanceComponent } from './grievance/grievance.component';
import { MeetingComplainerComponent } from './meeting-complainer/meeting-complainer.component';
import { MeetingGrievanceAgainstComponent } from './meeting-grievance-against/meeting-grievance-against.component';
import { MeetingBothPartiesComponent } from './meeting-both-parties/meeting-both-parties.component';
import { ResultComponent } from './result/result.component';
import { GrievanceGuardService } from "./grievance-guard.service";

export const GrievancePagesRoutes: Routes = [
    { path: 'detail/:id', component: GrievanceComponent, canActivate: [GrievanceGuardService] },
    { path: 'meeting-complainer/:id', component: MeetingComplainerComponent, canActivate: [GrievanceGuardService] },
    { path: 'meeting-grievance-against/:id', component: MeetingGrievanceAgainstComponent, canActivate: [GrievanceGuardService] },
    { path: 'meeting-both-parties/:id', component: MeetingBothPartiesComponent, canActivate: [GrievanceGuardService] },
    { path: 'result/:id', component: ResultComponent, canActivate: [GrievanceGuardService] }
];

export const GrievanceRoutingModule = RouterModule.forChild(GrievancePagesRoutes);
