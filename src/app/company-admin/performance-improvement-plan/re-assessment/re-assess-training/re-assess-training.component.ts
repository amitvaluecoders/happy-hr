import { Component, OnInit } from '@angular/core';
import { APPCONFIG } from '../../../../config';
import { Router} from '@angular/router';

@Component({
  selector: 'app-re-assess-training',
  templateUrl: './re-assess-training.component.html',
  styleUrls: ['./re-assess-training.component.scss']
})
export class ReAssessTrainingComponent implements OnInit {

public progress;
    public invitelistCollapse = false;
    public originalPIPToggle = false;

   constructor(public router:Router ) { }

  ngOnInit() {

     APPCONFIG.heading="Re-Assess Training";
     this.progressBarSteps();

     
}
progressBarSteps() {
      this.progress={
          progress_percent_value:50, //progress percent
          total_steps:4,
          current_step:2,
          circle_container_width:25, //circle container width in percent
          steps:[
              // {num:1,data:"Select Role"},
               {num:1,data:"Re-Assess PIP"},
               {num:2,data:"Training",active:true},
               {num:3,data:"Assessment"},
               {num:4,data:"Result"}
          ]  
       }         
  }

}

