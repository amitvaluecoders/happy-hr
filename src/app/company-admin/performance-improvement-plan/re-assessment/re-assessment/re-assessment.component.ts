import { Component, OnInit } from '@angular/core';
import { APPCONFIG } from '../../../../config';
import { Router} from '@angular/router';

@Component({
  selector: 'app-re-assessment',
  templateUrl: './re-assessment.component.html',
  styleUrls: ['./re-assessment.component.scss']
})
export class ReAssessmentComponent implements OnInit {

public progress;
    public invitelistCollapse = false;
    public originalPIPToggle = false;

   public dropdropdownListActions=[];
    public selectedItemsActions = [];
    public dropdownSettingsActions={};

    constructor(public router:Router ) { }

  ngOnInit() {

     APPCONFIG.heading="Re-Assessment";
     this.progressBarSteps();

     this.dropdropdownListActions=[
      { "id": 1, "itemName": "Re-Assess Performance Improvement Plan" },
      { "id": 2, "itemName": "Warning" },
      { "id": 3, "itemName": "Final Warning" },
      { "id": 4, "itemName": "Terminate" },
      { "id": 5, "itemName": "Disciplinary Action" },
      { "id": 6, "itemName": "Volunteer" },
    ]
    this.dropdownSettingsActions={
     singleSelection: true,
     text: "Action"
   }
 }
// on select user discount type
    onActionsUserSelect(obj){
      console.log("OBKJJBJBJBJBJBJJBJB",obj);
      if(obj.itemName=='Re-Assess Performance Improvement Plan')
      this.router.navigate(['app/company-admin/performance-improvement-plan/reassessment/disciplinary/create']);
     else if(obj.itemName=='Warning')
     this.router.navigate(['app/company-admin/performance-improvement-plan/reassessment/warning/create']);
     else if(obj.itemName=='Final Warning')
     this.router.navigate(['app/company-admin/performance-improvement-plan/reassessment/warning/create']);
     else if(obj.itemName=='Terminate')
     this.router.navigate(['app/company-admin/performance-improvement-plan/reassessment/termination-letter/create']);
     else if(obj.itemName=='Disciplinary Action')
     this.router.navigate(['app/company-admin/performance-improvement-plan/reassessment/disciplinary/create']);
    }
 
 // on de-select user discount type
    onActionsUserDeSelect(obj){
     // this.subscriptionPlan.userDiscoutType='';
    }
progressBarSteps() {
      this.progress={
          progress_percent_value:75, //progress percent
          total_steps:4,
          current_step:3,
          circle_container_width:25, //circle container width in percent
          steps:[
              // {num:1,data:"Select Role"},
               {num:1,data:"Re-Assess PIP"},
               {num:2,data:"Training"},
               {num:3,data:"Assessment",active:true},
               {num:4,data:"Result"}
          ]  
       }         
  }

}