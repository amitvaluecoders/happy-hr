import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditPerformanceImprovementPlanComponent } from './edit-performance-improvement-plan.component';

describe('EditPerformanceImprovementPlanComponent', () => {
  let component: EditPerformanceImprovementPlanComponent;
  let fixture: ComponentFixture<EditPerformanceImprovementPlanComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditPerformanceImprovementPlanComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditPerformanceImprovementPlanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
