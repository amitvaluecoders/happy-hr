import { Component, OnInit, AfterViewInit} from '@angular/core';
import { Cookie } from 'ng2-cookies/ng2-cookies';
import { PerformanceImprovementPlanService } from '../performance-improvement-plan.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { CoreService } from '../../../shared/services/core.service';
import {APPCONFIG } from '../../../config';
import { IDatePickerConfig } from 'ng2-date-picker';
import * as moment from 'moment';


@Component({
  selector: 'app-edit-performance-improvement-plan',
  templateUrl: './edit-performance-improvement-plan.component.html',
  styleUrls: ['./edit-performance-improvement-plan.component.scss']
})
export class EditPerformanceImprovementPlanComponent implements OnInit {

public isFocused=false;  
public loggedInUser:any;
    public tempSelectedItem={};
  public progress;
  chk=true;
  dropdownListEmployees = [];
  selectedItems = [];
  inviteSelectedItems = [];  
  dropdownSettings = {};
  selectedParty = [];
  isProcessing =false;
  invitedropdownListEmployees = [];
  invitedropdownSettings = {};
  isButtonClicked=false;
  public isCreateProcessing=false;
  public minTrainingTime;
  public assessmentTimeFlag=0;
  public trainingTimeFlag=0;
  public reassessmentFormProcessing=false;
  
  assessmentTimeConfig: IDatePickerConfig = {};
  config: IDatePickerConfig = {};


  userInfo={
      name:''
  };
   pipPlan= {
      pipID:'',
      reaccess:'No',
      employeeID: '',
      title: '',
      reason: '',
      actionPlan: '',
      objective: '',
      trainingTime: '',   
      assessmentTime: '',
      managerID: '',
      thirdParty: []
  }

   public plan:any;
  
  
    // variables for third party invite . 
  public isThirdPartyAlreadyExist = false;
  public isEnterPressed = false;
  public thirdPartyName = ''; 
  public thirdPartyList = [];
  public invitelistCollapse = false;
  public tempTrainingTime:any;
  public tempAssessmentTime:any;
    public heading='';

    constructor(public  performanceImprovementPlanService: PerformanceImprovementPlanService,
               public router: Router, public activatedRoute: ActivatedRoute,
               public coreService: CoreService) {

                this.assessmentTimeConfig.locale='en';
                this.assessmentTimeConfig.format='DD MMM YYYY hh:mm:ss';
                this.assessmentTimeConfig.showMultipleYearsNavigation=true;
                this.assessmentTimeConfig.weekDayFormat='dd';
                this.assessmentTimeConfig.disableKeypress=true,
                this.assessmentTimeConfig.monthFormat='MMM YYYY';
                this.assessmentTimeConfig.min=moment().add(+1, 'days');
                this.config.locale = "en";
                this.config.format = "DD MMM YYYY hh:mm:ss ";
                this.config.showMultipleYearsNavigation = true;
                this.config.weekDayFormat = 'dd';
                this.config.disableKeypress = true;
                this.config.monthFormat="MMM YYYY";
                this.config.min=moment();
               }

  ngOnInit() {
       this.loggedInUser = JSON.parse(Cookie.get('happyhr-userInfo')) || JSON.parse(localStorage.getItem('happyhr-userInfo'));
      if(this.activatedRoute.snapshot.params['id']) this.reassessmentFormProcessing=true;
      this.minTrainingTime=moment(new Date()).format("DD MMM YYYY hh:mm:ss");
      this.getEmployeeeList();
     APPCONFIG.heading="Update performance improvement plan";
     this.dropdownSettings = {
        singleSelection: true, 
        text:"Select manager",
        enableSearchFilter: true,
        classes:"myclass assmng"
     }; 
    this.invitedropdownSettings = {
        singleSelection: false, 
        text:"Select third party",
        enableSearchFilter: true,
        classes:"myclass"
    };
    this.heading=APPCONFIG.heading;
    
  }

  setReassessmentForm(){
      this.reassessmentFormProcessing=true;
      this.getpipDetails();
  }


  getpipDetails() {
     this.performanceImprovementPlanService.getPipPlanDetails(this.activatedRoute.snapshot.params['id']).subscribe(
          d => {
            if (d.code == "200") {
              this.pipPlan=d.data; 
              this.pipPlan['reaccess']='No';
               this.userInfo=d.data.employee;
               this.plan=d.data.oldPip;
              this.tempTrainingTime=moment(new Date(d.data.trainingTime)).format("DD MMM YYYY hh:mm:ss");
                this.tempAssessmentTime=moment(new Date(d.data.assessmentTime)).format("DD MMM YYYY hh:mm:ss");
                   
              this.thirdPartyList =this.thirdPartyList.filter(item =>{
                       if( item.userId == d.data.employee.userID )return null;
                        if (item.userId == d.data.manager.userID) return false;
                      return item;
                  })     
                  this.dropdownListEmployees = this.dropdownListEmployees.filter(item =>{
                      if (item.userId == d.data.manager.userID) {
                          item['isSelected'] = true;
                          this.selectedItems.push(item);
                      } 
                       if( item.userId == d.data.employee.userID ){
                            this.pipPlan.employeeID=item.userId;
                           this.userInfo=item;
                           return null;
                       }
                      return item;
                  })     
            
              this.reassessmentFormProcessing=false;
                
            }
            
            else this.coreService.notify("Unsuccessful", d.message, 0);
          },
          error => this.coreService.notify("Unsuccessful", "Error while getting development details", 0),
          () => { }
      )
  }


  // method to get all employee listing for manger and third pary listing 
  getEmployeeeList() {
     this.isProcessing = true;
     this.performanceImprovementPlanService.getAllEmployee().subscribe(
          d => {
              if (d.code == "200") {
                  this.dropdownListEmployees=d.data.filter(item =>{          
                      item['id'] = item.userId;
                      item['itemName'] = item.name;
                      item['isSelected'] = false;
                        if((!this.activatedRoute.snapshot.params['info']) && item.userId==this.activatedRoute.snapshot.params['id']) {
                          this.pipPlan.employeeID=item.userId;
                          this.userInfo=item;
                          return false;
                        } 
                        //  if(this.loggedInUser.userID==item.userId)return null;
                        if(item.role=='companyManager')this.dropdownListEmployees.push(JSON.parse(JSON.stringify(item))); 
                      return item;
                  })
                   if(this.activatedRoute.snapshot.params['id']) this.setReassessmentForm();

                  this.thirdPartyList=JSON.parse(JSON.stringify(this.dropdownListEmployees));
                this.isProcessing = false; 
               
              }
              else this.coreService.notify("Unsuccessful", "Error while getting contract types", 0);
          },
          error => this.coreService.notify("Unsuccessful", "Error while getting contract types", 0),
          () => { }
      )
  }


 
  onItemSelect(item: any) {  
          if(!(Object.keys(this.tempSelectedItem).length === 0 && this.tempSelectedItem.constructor === Object)) this.thirdPartyList.unshift(this.tempSelectedItem);
        this.isFocused=false;
       for(let k of this.dropdownListEmployees){
           k['isSelected']=(k.userId==item.userId)?true:false;
       }
       if(item.isSelected){
            this.thirdPartyList = this.thirdPartyList.filter(i =>{ 
                if (item.userId == i.userId) return null;
                else return i;
            }) 
        }
        this.tempSelectedItem=item;
    }

    OnItemDeSelect(item: any) {
        this.isFocused=false;        
        let temp= JSON.parse(JSON.stringify(item));
        temp['isSelected']=false;
        this.thirdPartyList.unshift(temp);
        this.tempSelectedItem={};
    }

      onThirdPartySelect(e){
        e.isSelected=!e.isSelected;      
        if(e.isSelected){
            this.dropdownListEmployees = this.dropdownListEmployees.filter(item =>{
                if (item.userId == e.userId) return null;
                else return item;
            }) 
        }
        else {
            if(e.role){
            let temp= JSON.parse(JSON.stringify(e));
            temp['isSelected']=false;
            this.dropdownListEmployees.push(temp)
            }  
        }
    }

  




  // method to create development plan  
  

// assign/reassisgn training time on change.
    ontrainingTimeChange(){
        if(this.trainingTimeFlag >0) {
            this.pipPlan.trainingTime = moment(this.tempTrainingTime).format("YYYY-MM-DD hh:mm:ss");   
            let copy: IDatePickerConfig = JSON.parse(JSON.stringify(this.assessmentTimeConfig));
            copy.min = moment( this.pipPlan.trainingTime);
            this.assessmentTimeConfig = copy;
            this.tempAssessmentTime=null;
            this.pipPlan.assessmentTime='';
        }
        this.trainingTimeFlag++;
    }

// assign/reassisgn assessment time on change.
    onAssessmentTimeChange(){      
        if(this.assessmentTimeFlag >0) this.pipPlan.assessmentTime = moment(this.tempAssessmentTime).format("YYYY-MM-DD hh:mm:ss");
        this.assessmentTimeFlag++;
        
    }



// on adding external third party.
    onEnterExternalThirdParty(isvalid){
         if(this.thirdPartyName){
      this.isThirdPartyAlreadyExist=false;
      this.isEnterPressed=true;
      if(isvalid && this.thirdPartyName.trim() != ''){
            for(let c of this.thirdPartyList ){
                if(c.email==this.thirdPartyName)this.isThirdPartyAlreadyExist=true;
            }
            if(!this.isThirdPartyAlreadyExist){
                this.thirdPartyList.splice(0,0,{
                    isSelected:true,
                    name:this.thirdPartyName,
                    email:this.thirdPartyName,
                    external:true,
                    imageUrl:"http://happyhrapi.n1.iworklab.com/public/user-profile-image/user.png"
                })
                this.thirdPartyName='';
                this.isEnterPressed=false;
            } 
      }
         }

  }

// method to set third party selected list for request body.
  onCreatePip(isvalid){
    console.log("aa gya");
       this.isButtonClicked=true;
      if(isvalid){
          if(this.pipPlan.reaccess=='No') {
             this.pipPlan.managerID = this.selectedItems[0].id;  
          }
              console.log("aaya",this.pipPlan)

          if(this.pipPlan.reaccess=='Yes'){
              this.pipPlan['managerID'] = this.pipPlan['manager'].userID;
          } 
           for(let i of this.thirdPartyList){
               if(i.isSelected){
                 this.pipPlan.thirdParty.push({
                     type:(i.external)?"External":"Internal",
                     userID:(!i.external)?i.userId:'',
                     email:(i.external)?i.email:''
                });  
               }
           }
          this.createPlan(); 
      }   
  }

// method to create third party.
  createPlan(){
        this.isCreateProcessing=true;
        this.pipPlan['employeeID']= this.pipPlan['employee'].userID;  
        this.performanceImprovementPlanService.createPLan(this.pipPlan).subscribe(
          d => {
              if (d.code == "200") {
                  this.coreService.notify("Successful",d.message, 1);
                 this.router.navigate([`/app/company-admin/listing-page`]);
              }
              else this.coreService.notify("Unsuccessful",d.message, 0);
               this.isCreateProcessing = false; 
          },
          error => {
              this.coreService.notify("Unsuccessful", error.message, 0);
              this.isCreateProcessing = false; 
          }     
      )
  }
  
 onViewUserDetails(id){
          var newWindow = window.open(`/#/app/company-admin/employee-management/profile/${id}`);
    }


}
