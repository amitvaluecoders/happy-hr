import { Injectable } from '@angular/core';
import { CanActivate, Router, RouterStateSnapshot, ActivatedRouteSnapshot } from "@angular/router";
import { CoreService } from '../../shared/services/core.service';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';

@Injectable()
export class PerformanceImprovementPlanGuardService {

    public module = "pip";

    constructor(private _core: CoreService, private router: Router) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        let url: string;
        if (route['_routeConfig']['path'])
            url = route['_routeConfig']['path']
        else url = state.url;
        if (this.checkHavePermission(url)) {
            return true;
        } else {
            this.router.navigate(['/extra/unauthorized']);
            return false;
        }
    }

    private checkHavePermission(path) {
        return true;
        // switch (path) {
        //     case "/app/company-admin/listing-page":
        //         return this._core.getModulePermission(this.module, 'view');
        //     case "/app/company-admin/performance-improvement-plan":
        //         return this._core.getModulePermission(this.module, 'view');
        //     case 'create/:id':
        //         return this._core.getModulePermission(this.module, 'add');
        //     case 'create/:info/:id':
        //         return this._core.getModulePermission(this.module, 'add');
        //     case 'training/:id':
        //         return this._core.getModulePermission(this.module, 'view');
        //     case 'details/:id':
        //         return this._core.getModulePermission(this.module, 'view');
        //     case 'update/:id':
        //         return this._core.getModulePermission(this.module, 'edit');
        //     default:
        //         return false;
        // }
    }
}

