import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MyDatePickerModule } from 'mydatepicker';
import { AngularMultiSelectModule } from 'angular2-multiselect-checkbox-dropdown/angular2-multiselect-dropdown';
import { PerformanceImprovementPlanService } from './performance-improvement-plan.service';
import { PerformanceImprovementRoutingModule } from './performance-improvement-plan.routing';
import { CreatePerformanceImprovementPlanComponent } from './create-performance-improvement-plan/create-performance-improvement-plan.component';
import { TrainingComponent } from './training/training.component';
import { AssessmentComponent } from './assessment/assessment.component';
import { ResultComponent } from './result/result.component';
import { CreateReAssessPIPComponent } from './re-assessment/create-re-assess-pip/create-re-assess-pip.component';
import { ReAssessTrainingComponent } from './re-assessment/re-assess-training/re-assess-training.component';
import { ReAssessmentComponent } from './re-assessment/re-assessment/re-assessment.component';
import { CreateDisciplinaryComponent } from './re-assessment/create-disciplinary/create-disciplinary.component';
import { CreateWarningComponent, ActionDialog } from './re-assessment/create-warning/create-warning.component';
import { CreateTerminationLetterComponent, terminationDialog} from './re-assessment/create-termination-letter/create-termination-letter.component';
import { ReassessmentResultComponent } from './re-assessment/reassessment-result/reassessment-result.component';
import { SharedModule } from '../../shared/shared.module';
import { MaterialModule } from '@angular/material';
import { FormsModule } from '@angular/forms';
import { EmployeePipDetailsComponent } from './employee-pip-details/employee-pip-details.component';
import { EditPipNoteEmployeeComponent } from './employee-pip-details/edit-pip-note-employee/edit-pip-note-employee.component';
import { EditPerformanceImprovementPlanComponent } from './edit-performance-improvement-plan/edit-performance-improvement-plan.component';
import { PerformanceImprovementPlanGuardService } from './performance-improvement-plan-guard.service';
@NgModule({
  imports: [
    MaterialModule,
    CommonModule,
    SharedModule,
    FormsModule,
    MyDatePickerModule,
    AngularMultiSelectModule,
    PerformanceImprovementRoutingModule
  ],
  declarations: [CreatePerformanceImprovementPlanComponent,TrainingComponent, AssessmentComponent, ResultComponent, CreateReAssessPIPComponent, ReAssessTrainingComponent, ReAssessmentComponent, CreateDisciplinaryComponent, CreateWarningComponent, CreateTerminationLetterComponent, ReassessmentResultComponent, ActionDialog, terminationDialog, EmployeePipDetailsComponent, EditPipNoteEmployeeComponent, EditPerformanceImprovementPlanComponent],
  providers:[PerformanceImprovementPlanService,PerformanceImprovementPlanGuardService],
  entryComponents:[ActionDialog, terminationDialog,EditPipNoteEmployeeComponent]
})
export class PerformanceImprovementPlanModule { }
