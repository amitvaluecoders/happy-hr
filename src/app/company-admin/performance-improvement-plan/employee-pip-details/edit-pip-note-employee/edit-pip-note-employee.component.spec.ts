import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditPipNoteEmployeeComponent } from './edit-pip-note-employee.component';

describe('EditPipNoteEmployeeComponent', () => {
  let component: EditPipNoteEmployeeComponent;
  let fixture: ComponentFixture<EditPipNoteEmployeeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditPipNoteEmployeeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditPipNoteEmployeeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
