import { Component, OnInit } from '@angular/core';
import { PerformanceImprovementPlanService } from '../../performance-improvement-plan.service';
import { CoreService } from '../../../../shared/services/core.service';
import { MdDialogRef } from '@angular/material';


@Component({
  selector: 'app-edit-pip-note-employee',
  templateUrl: './edit-pip-note-employee.component.html',
  styleUrls: ['./edit-pip-note-employee.component.scss']
})
export class EditPipNoteEmployeeComponent implements OnInit {

 public noteDetails:any;
  public isProcessing =false;
  public isButtonClicked=false;
  public mode:any;

  constructor( public pipService: PerformanceImprovementPlanService,
               public coreService: CoreService,public dialog: MdDialogRef<EditPipNoteEmployeeComponent>) { }

  ngOnInit() {
    console.log("dddddddd",this.noteDetails)
  }

  onUpdate(isValid){
    this.isButtonClicked=true;
    if(isValid){
      this.isProcessing=true;
     this.pipService.updateDevelopmentPlanNotes(this.noteDetails).subscribe(
          d => {
            this.isProcessing = false;  
              if (d.code == "200"){
                this.coreService.notify("Successful", d.message, 1);
                this.dialog.close({data:d.data,mode:this.mode});
              } 
              else this.coreService.notify("Unsuccessful", d.message, 0);
          },
          error => {
            this.coreService.notify("Unsuccessful", "Error while Updating.", 0), 
            this.isProcessing = false;  
          }
      )
  }
  }

}
