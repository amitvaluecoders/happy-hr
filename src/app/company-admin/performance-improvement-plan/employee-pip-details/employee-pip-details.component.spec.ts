import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployeePipDetailsComponent } from './employee-pip-details.component';

describe('EmployeePipDetailsComponent', () => {
  let component: EmployeePipDetailsComponent;
  let fixture: ComponentFixture<EmployeePipDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmployeePipDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployeePipDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
