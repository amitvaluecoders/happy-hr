import { Component, OnInit } from '@angular/core';
import { APPCONFIG } from '../../../config';
import { Router} from '@angular/router';

@Component({
  selector: 'app-result',
  templateUrl: './result.component.html',
  styleUrls: ['./result.component.scss']
})
export class ResultComponent implements OnInit {

  public progress;

    public dropdropdownListActions=[];
    public selectedItemsActions = [];
    public dropdownSettingsActions={};

    public invitelistCollapse = false;

   constructor(public router:Router ) { }

  ngOnInit() {

     APPCONFIG.heading="Result";
     this.progressBarSteps();

     this.dropdropdownListActions=[
      { "id": 1, "itemName": "Re-Assess Performance Improvement Plan" },
      { "id": 2, "itemName": "Warning" },
      { "id": 3, "itemName": "Final Warning" },
      { "id": 4, "itemName": "Terminate" },
      { "id": 5, "itemName": "Disciplinary Action" },
      { "id": 6, "itemName": "Volunteer" },
    ]
    this.dropdownSettingsActions={
     singleSelection: true,
     text: "Action"
   }
 }
// on select user discount type
    onActionsUserSelect(obj){
      console.log("OBKJJBJBJBJBJBJJBJB",obj);
      if(obj.itemName=='Final Warning')
      this.router.navigate(['app/company-admin/performance-improvement-plan/reassessment/warning/create'])
     // this.subscriptionPlan.userDiscoutType=obj.itemName;
    }
 
 // on de-select user discount type
    onActionsUserDeSelect(obj){
     // this.subscriptionPlan.userDiscoutType='';
    }
progressBarSteps() {
      this.progress={
          progress_percent_value:100, //progress percent
          total_steps:4,
          current_step:4,
          circle_container_width:25, //circle container width in percent
          steps:[
              // {num:1,data:"Select Role"},
               {num:1,data:"Create PIP"},
               {num:2,data:"Training"},
               {num:3,data:"Assessment"},
               {num:4,data:"Result",active:true}
          ]  
       }         
  }

}
