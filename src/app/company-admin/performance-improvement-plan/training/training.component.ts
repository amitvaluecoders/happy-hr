import { Component, OnInit } from '@angular/core';
import { APPCONFIG } from '../../../config';
import { PerformanceImprovementPlanService } from '../performance-improvement-plan.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { CoreService } from '../../../shared/services/core.service';
import { MdSnackBar, MdDialog, MdDialogRef } from '@angular/material';
import { EditPipNoteEmployeeComponent } from '../employee-pip-details/edit-pip-note-employee/edit-pip-note-employee.component';
import { ActionsOnEmployeePopupComponent } from '../../../shared/components/actions-on-employee-popup/actions-on-employee-popup.component';
import * as moment from 'moment';
@Component({
  selector: 'app-training',
  templateUrl: './training.component.html',
  styleUrls: ['./training.component.scss']
})
export class TrainingComponent implements OnInit {

  public progress;
  public tempDP: any;
  public isTraCreateProcessing = false;
  public isTraButtonClicked = false;
  public heading = '';
  public serviceDetails: any;
  dropdownListEmployees = [];
  selectedParty = [];
  public originalPIPToggle=[];
  public invitelistCollapse: Boolean;
  invitedropdownSettings = {};
  public plan: any;
  public planNew = {
    insertTime: '',
    status: '',
    developmentFor: { imageUrl: '', employee: '', employeeProfile: '' },
    assinedBy: { imageUrl: '', employee: '', employeeProfile: '' },
    thirdParty: [],
    title: '',
    reason: '',
    objective: '',
    actionPlan: '',
    trainingDate: new Date(),
    trainingTime: '',
    assessmentDate: new Date(),
    assessmentTime: '',
    trainingNotes: [],
    assessmentNotes: [],
    employeeNotes: []
  };
  public isButtonClicked = false;
  public isCreateProcessing = false;
  public isResultProcessing = false;
  public isProcessing = false;
  public counter = 0;
  public pipManagerNote = {
    pipID: "",
    assessmentNote: "",
    managerNote: "",
    objectiveMet: "Yes",
    managerTraNote: ''
  };
  permission;
  public reqBody = {
    search:"",
    filter:{
      result:{},
      employeeIDs:[],
      managerIDs:[],
      due:[],
      status:{}
    }
  }

  constructor(public pipService: PerformanceImprovementPlanService,
    public router: Router, public activatedRoute: ActivatedRoute,
    public coreService: CoreService, public dialog: MdDialog) { }


  ngOnInit() {
    //'Awaiting Employee Acceptance','Accepted','Under Assessment','Under Training','Complete','Re Assessment','Reschedule Requested','Awaiting Result'
    this.permission = this.coreService.getNonRoutePermission();
    this.getPlans();
    // this.serviceDetails = this.coreService.getPIP();
    // if (!this.serviceDetails) {
    //   this.router.navigate([`/app/company-admin/listing-page`]);
    // }
    // else {
    //   this.progressBarSteps(this.serviceDetails);
    //   this.getpipDetails();
    // }


  }

  formateDate(v){
    return moment(v).isValid()? moment(v).format('DD MMM ,YYYY') : 'Not specified';
}

formateShortTime(v){
  return moment(v).isValid()? moment(v).format('HH:mm A') : 'Not specified';
}

  onEditPIP(pipID) {
    this.router.navigate([`/app/company-admin/performance-improvement-plan/update/${pipID}`]);
  }

  getPlans() {
    this.isProcessing = true;
    this.pipService.getAllPlans(JSON.stringify(this.reqBody)).subscribe(
         d => {
             if (d.code == "200") {
                  d.data.filter(item=>{
                    if(item.pipID==this.activatedRoute.snapshot.params['id'])
                    {
                      this.serviceDetails= item;

                    }
                  })
                  if (!this.serviceDetails) {
                    this.router.navigate([`/app/company-admin/listing-page`]);
                  }
                  else {
                    this.progressBarSteps(this.serviceDetails);
                    this.getpipDetails();
                  }

                
               }
               else this.coreService.notify("Unsuccessful", d.message, 0);
               //this.isProcessing = false;    
             },
         error => this.coreService.notify("Unsuccessful", error.message, 0),
         () => { }
     )
 }



  onEditNotes(note, mode) {
    let dialogRef = this.dialog.open(EditPipNoteEmployeeComponent);
    dialogRef.componentInstance.mode = mode;
    dialogRef.componentInstance.noteDetails = JSON.parse(JSON.stringify(note));
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        console.log("in", result)
        if (result.mode == 'training') {
          for (let n of this.planNew.trainingNotes) {
            if (n.noteID == result.data.noteID) {
              n.note = result.data.note;
              n.time = result.data.time;
              n.noteByUser.name = result.data.noteByUser.name;
            }
          }
        }
        else {
          for (let n of this.planNew.assessmentNotes) {
            if (n.noteID == result.data.noteID) {
              n.note = result.data.note;
              n.time = result.data.time;
              n.noteByUser.name = result.data.noteByUser.name;
              n.objectiveMet = result.data.objectiveMet;
            }
          }
        }
      }
    });

  }

  progressBarSteps(data) {
    this.tempDP = data;
    APPCONFIG.heading = "Create";
    this.progress = {
      progress_percent_value: 25, //progress percent
      total_steps: 4,
      current_step: 1,
      circle_container_width: 25, //circle container width in percent
      steps: [
        { num: 1, data: (data.reaccess == 'No') ? "Create PIP" : "Re-Assess: Create PIP", active: false },
        { num: 2, data: "Training", active: false },
        { num: 3, data: "Assessment", active: false },
        { num: 4, data: "Result", active: false }
      ]
    }

    if ((data.status == 'Under Training') || (data.status == 'Awaiting Employee Acceptance') || (data.status == 'Accepted')) {
      APPCONFIG.heading = "Training";
      this.progress.progress_percent_value = 50;
      this.progress.current_step = 2;
      this.progress.steps[1].active = true;
    }
    if (data.status == 'Under Assessment') {
      APPCONFIG.heading = "Assessment";
      this.progress.progress_percent_value = 75;
      this.progress.current_step = 3;
      this.progress.steps[2].active = true;
    }
    if (data.status == 'Complete') {
      APPCONFIG.heading = "Result";
      this.progress.progress_percent_value = 100;
      this.progress.current_step = 4;
      this.progress.steps[3].active = true;
    }
    this.heading = APPCONFIG.heading;
  }
  
  onTime(time){return new Date(time);}

  getpipDetails() {
    this.isProcessing = true;
    this.pipService.getPipPlanDetails(this.activatedRoute.snapshot.params['id']).subscribe(
      d => {
        if (d.code == "200") {
          this.planNew = d.data;
          this.pipManagerNote.pipID = this.planNew['pipID'];
          this.plan = this.planNew['oldPip'].reverse();
          for (let i in this.plan) { this.originalPIPToggle[i] = false }
          this.isProcessing = false;
          for (let k of this.planNew['thirdParty']) {
            k['id'] = k['employeeID'];
            k['itemName'] = k['employee'];
            this.selectedParty.push(k);
          }
          this.progressBarSteps(this.planNew)
        }

        else this.coreService.notify("Unsuccessful", d.message, 0);
      },
      error => this.coreService.notify("Unsuccessful", "Error while getting development details", 0),
      () => { }
    )
  }




  onAddResult(id, result) {
     if ((result == 'Terminate') || (result == 'Warning') || (result == 'Final Warning')) {
      let info = { pipID: this.planNew['pipID'], type: result, userID: this.planNew['employee'].userID, subject: "", body: '' }
      let dialogRef = this.dialog.open(ActionsOnEmployeePopupComponent);
      dialogRef.componentInstance.actionInfo = info;
      dialogRef.afterClosed().subscribe(r => {
        if (r) {
          this.submitResult(id, result);
        }
      })
    }
    else if (result == 'Disciplinary') {
      this.router.navigate([`/app/company-admin/disciplinary-meeting/create/${this.planNew['employee'].userID}/pip/${id}`]);
    }
    else this.submitResult(id, result);
  }

  
  onAddResultNotPasses(id, result) {
  //   if ((result == 'Terminate') || (result == 'Warning') || (result == 'Final Warning')) {
  //    let info = { pipID: this.planNew['pipID'], type: result, userID: this.planNew['employee'].userID, subject: "", body: '' }
  //    let dialogRef = this.dialog.open(ActionsOnEmployeePopupComponent);
  //    dialogRef.componentInstance.actionInfo = info;
  //    dialogRef.afterClosed().subscribe(r => {
  //      if (r) {
  //        this.submitResult(id, result);
  //      }
  //    })
  //  }
  //  else if (result == 'Disciplinary') {
  //    this.router.navigate([`/app/company-admin/disciplinary-meeting/create/${this.planNew['employee'].userID}/pip/${id}`]);
  //  }
  //  else this.submitResult(id, result);
 }

  



  submitResult(id, result) {
    this.isResultProcessing = true;
    let reqBody = { pipID: id, result: result }
    this.pipService.onAddResult(reqBody).subscribe(
      d => {
        if (d.code == "200") {
          this.coreService.notify("Successful", d.message, 1);
          // if(result=='Disciplinary') this.router.navigate([`/app/company-admin/disciplinary-meeting/create/${this.planNew['employee'].userID}`]);
          if (result == 'Re-Access') {
            this.planNew['result'] = result;
            this.planNew.status = 'Complete';
          }
          else this.router.navigate([`/app/company-admin/listing-page/`]);
          this.isResultProcessing = false;
        }
        else {
          this.coreService.notify("Unsuccessful", d.message, 0);
          this.isResultProcessing = false;
        }
      },
      error => {
        this.coreService.notify("Unsuccessful", error.message, 0);
        this.isResultProcessing = false;
      }
    )
  }

  onAddManagerTrainingNote(trainingNoteForm) {

    this.isTraButtonClicked = true;

    if (trainingNoteForm.form.valid) {
      let reqBody = {
        pipID: this.pipManagerNote.pipID,
        trainingNote: this.pipManagerNote.managerTraNote
      };
      this.isTraCreateProcessing = true;
      this.pipService.onAddManagerNote(reqBody).subscribe(
        d => {
          if (d.code == "200") {
            this.isTraCreateProcessing = false;
            this.coreService.notify("Successful", d.message, 1);
            this.router.navigate([`/app/company-admin/listing-page`]);      
            // this.planNew.trainingNotes.push(d.data);
            // this.pipManagerNote.managerNote = '';
            // this.isTraButtonClicked = false;
            // this.tempDP.status = 'Under Training';
            // trainingNoteForm.reset();
          }
          else {
            this.isTraCreateProcessing = false;
            this.coreService.notify("Unsuccessful", d.message, 0);
          }
        },
        error => {
          this.coreService.notify("Unsuccessful", error.message, 0);
          this.isTraCreateProcessing = false;
        }

      )
    }
  }

  onAddManagerAssessmentNote(form) {
    this.isButtonClicked = true;
    if (form.form.valid) {
      this.isCreateProcessing = true;
      this.pipManagerNote.assessmentNote = this.pipManagerNote.managerNote;
      this.pipService.onAssessment(this.pipManagerNote).subscribe(
        d => {
          this.isCreateProcessing = false;
          if (d.code == "200") {
            this.coreService.notify("Successful", d.message, 1);
            this.planNew.assessmentNotes.push(d.data);
            this.pipManagerNote.managerNote = '';
            this.isCreateProcessing = false;
            this.isButtonClicked = false;
            this.tempDP.status = 'Under Assessment'
            form.reset();
          }
          else this.coreService.notify("Unsuccessful", d.message, 0);
        },
        error => {
          this.coreService.notify("Unsuccessful", error.message, 0);
          this.isCreateProcessing = false;
        }

      )
    }
  }


  onReassessemnt(id,userID) {
    this.router.navigate([`/app/company-admin/performance-improvement-plan/create/re/${userID}/${id}`]);
  }




}

