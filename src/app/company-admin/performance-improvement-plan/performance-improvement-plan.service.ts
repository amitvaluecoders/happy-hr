import { Injectable } from '@angular/core';
import { RestfulLaravelService} from '../../shared/services/restful-laravel.service';

@Injectable()
export class PerformanceImprovementPlanService {

 constructor(public restfulWebService:RestfulLaravelService) { }

   getAllManager(id):any{
   return this.restfulWebService.get(`show-manager-and-ceo-and-fm/${id}`)
  }
  getAllEmployee():any{
    return this.restfulWebService.get('show-all-employee')
   }

  createPLan(reqBody):any{
   return this.restfulWebService.post('pip-create-update',reqBody); 
  }

  getDataForFilters():any{
   return this.restfulWebService.get(`pip-filter-data`)    
  }

  getAllPlans(reqBody):any{
   return this.restfulWebService.get(`pip-list/${encodeURIComponent(reqBody)}`)    
  }

  getPipPlanDetails(reqBody):any{
   return this.restfulWebService.get(`pip-detail/${reqBody}`)    
  }

  onAddResult(reqBody):any{
   return this.restfulWebService.post('pip-result',reqBody);     
  }

  onAddManagerNote(reqBody):any{
   return this.restfulWebService.post('pip-training',reqBody);         
  }

   onAssessment(reqBody):any{
   return this.restfulWebService.post('pip-assessment',reqBody);         
  }

  updateDevelopmentPlanNotes(reqBody):any{
       return this.restfulWebService.put('pip-note-edit',reqBody)
  }

   deletePip(reqBody):any{
    return this.restfulWebService.delete(`pip-delete/${reqBody}`)
  }

}
