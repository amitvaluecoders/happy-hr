import { ModuleWithProviders } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { PerformanceImprovementPlanService } from './performance-improvement-plan.service';
import { CreatePerformanceImprovementPlanComponent } from './create-performance-improvement-plan/create-performance-improvement-plan.component';
import { TrainingComponent } from './training/training.component';
import { AssessmentComponent } from './assessment/assessment.component';
import { ResultComponent } from './result/result.component';
import { CreateReAssessPIPComponent } from './re-assessment/create-re-assess-pip/create-re-assess-pip.component';
import { ReAssessTrainingComponent } from './re-assessment/re-assess-training/re-assess-training.component';
import { ReAssessmentComponent } from './re-assessment/re-assessment/re-assessment.component';
import { CreateDisciplinaryComponent } from './re-assessment/create-disciplinary/create-disciplinary.component';
import { CreateWarningComponent } from './re-assessment/create-warning/create-warning.component';
import { CreateTerminationLetterComponent } from './re-assessment/create-termination-letter/create-termination-letter.component';
import { ReassessmentResultComponent } from './re-assessment/reassessment-result/reassessment-result.component';
import { EmployeePipDetailsComponent } from './employee-pip-details/employee-pip-details.component';
import { EditPerformanceImprovementPlanComponent } from './edit-performance-improvement-plan/edit-performance-improvement-plan.component';
import { PerformanceImprovementPlanGuardService } from "./performance-improvement-plan-guard.service";

export const PerformanceImprovementPagesRoutes: Routes = [
    { path: 'create/:id', component: CreatePerformanceImprovementPlanComponent, canActivate: [PerformanceImprovementPlanGuardService] },
    { path: 'create/:info/:id', component: CreatePerformanceImprovementPlanComponent, canActivate: [PerformanceImprovementPlanGuardService] },
    { path: 'create/:info/:userID/:id', component: CreatePerformanceImprovementPlanComponent, canActivate: [PerformanceImprovementPlanGuardService] },    
    { path: 'training/:id', component: TrainingComponent, canActivate: [PerformanceImprovementPlanGuardService] },
    { path: 'details/:id', component: EmployeePipDetailsComponent, canActivate: [PerformanceImprovementPlanGuardService] },
    { path: 'update/:id', component: EditPerformanceImprovementPlanComponent, canActivate: [PerformanceImprovementPlanGuardService] },

    //    { path: 'result', component: ResultComponent},
    //    { path: 'reassessment-PIP/create', component: CreateReAssessPIPComponent},
    //    { path: 'reassessment/training', component: ReAssessTrainingComponent},
    //    { path: 're-assessment', component: ReAssessmentComponent},
    //    { path: 'reassessment/disciplinary/create', component: CreateDisciplinaryComponent},
    //    { path: 'reassessment/warning/create', component: CreateWarningComponent},
    //    { path: 'reassessment/termination-letter/create', component: CreateTerminationLetterComponent},
    //    { path: 'reassessment/result', component: ReassessmentResultComponent},

];

export const PerformanceImprovementRoutingModule = RouterModule.forChild(PerformanceImprovementPagesRoutes);
