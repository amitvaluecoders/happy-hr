import { Injectable } from '@angular/core';
import { RestfulLaravelService} from '../../shared/services/restful-laravel.service';
@Injectable()
export class GeneralNoteServiceService {

   constructor(public restfulLaravelService:RestfulLaravelService) { }

  getNoteDetails(companyGeneralNoteID){
    return this.restfulLaravelService.get(`get-general-note-detail/${companyGeneralNoteID}`);
  }

  deleteNote(companyGeneralNoteID){
    return this.restfulLaravelService.delete(`delete-general-note/${companyGeneralNoteID}`);
  }

  getNoteList(){
    return this.restfulLaravelService.get(`list-general-note`);
  }

  createNote(reqBody){
    return this.restfulLaravelService.post(`add-general-note`,reqBody);
  }

  updateNote(reqBody){
    return this.restfulLaravelService.put(`edit-general-note`,reqBody);
  }

  uploadFile(reqBody){
    return this.restfulLaravelService.post(`file-upload`,reqBody);
  }
  
  
}
