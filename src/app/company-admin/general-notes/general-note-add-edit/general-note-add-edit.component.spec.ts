import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GeneralNoteAddEditComponent } from './general-note-add-edit.component';

describe('GeneralNoteAddEditComponent', () => {
  let component: GeneralNoteAddEditComponent;
  let fixture: ComponentFixture<GeneralNoteAddEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GeneralNoteAddEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GeneralNoteAddEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
