import { Component, OnInit } from '@angular/core';
import { GeneralNoteServiceService } from '../general-note-service.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { CoreService } from '../../../shared/services/core.service';
import { APPCONFIG } from '../../../config';

@Component({
  selector: 'app-general-note-add-edit',
  templateUrl: './general-note-add-edit.component.html',
  styleUrls: ['./general-note-add-edit.component.scss']
})
export class GeneralNoteAddEditComponent implements OnInit {
 
  public isProcessing:Boolean=false;
  public isDetailsProcessing:Boolean=false;
  public searchKey="";
  public id:any;
  public selectedUser="";
  public isCreateProcessing:Boolean=false;
  public isButtonClicked=false;
  public counter = 0;
  public docPath='';
  public docError=false;
  public fileName:any;
  dropdownListEmployees = [];
  selectedItems = [];
  dropdownSettings = {};

  public note={
	title:"",
	note:"",
	generalNoteFile:""
  }

  constructor(public service:GeneralNoteServiceService,
               public router:Router,public activatedRoute:ActivatedRoute,
               public coreService:CoreService) { }

  ngOnInit() {
    APPCONFIG.heading="Create general note";
    this.id  = this.activatedRoute.snapshot.params['id'];
    if(this.id){
      this.getNoteDetails();
      APPCONFIG.heading="Update general note";
    } 
    
    
  }

  getNoteDetails(){
     
     this.isDetailsProcessing=true;
      this.service.getNoteDetails(this.id).subscribe(
          d => {  
              if(d.code == "200" && d.data){
               this.fileName=d.data.generalNoteFile;
                 this.docPath=d.data.generalNoteFileUrl;
                 this.note = d.data;
            }
              else this.coreService.notify("Unsuccessful",d.message, 0); 
              this.isDetailsProcessing=false;  
          },
          error =>{
            this.coreService.notify("Unsuccessful", "Error while adding note", 0);
            this.isDetailsProcessing = false;
          } 
      )
  }


  fileChange(event) {
    this.isProcessing=true;
    this.docError=false;
    this.fileName='';
    this.docPath='';
    let fileList: FileList = event.target.files;
    if(fileList.length > 0) {
        let file: File = fileList[0];
        let formData:FormData = new FormData();
        formData.append('generalNoteFile',file);
        this.service.uploadFile(formData).subscribe(
          d=>{ 
            if(d.code="200"){
              this.isProcessing=false;
                this.docPath=d.data.generalNoteFile;
                this.fileName=file.name;
            }
            else {this.docError=true;this.isProcessing=false;}
          },
          error=>{this.docError=true;this.isProcessing=false;}
          )
    }
}
 

  onCreateNote(isValid){
    this.isButtonClicked=true;
    console.log("invalid"+this.note+"---"+this.docPath);
    if(isValid){
      this.note['generalNoteFile']=this.docPath;
     this.isCreateProcessing=true;
      this.service.createNote(this.note).subscribe(
          d => {
              this.isCreateProcessing=true;    
              if(d.code == "200") {
                this.router.navigate([`/app/company-admin/general-notes`]);
                this.coreService.notify("Successful",d.message, 1); 
              } 
              else this.coreService.notify("Unsuccessful",d.message, 0); 
          },
          error =>{
            this.coreService.notify("Unsuccessful", "Error while adding note", 0);
            this.isCreateProcessing = false;
          } 
      )
    }

  }




}
