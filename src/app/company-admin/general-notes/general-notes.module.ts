import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GeneralNoteListComponent } from './general-note-list/general-note-list.component';
import { GeneralNoteDetailsComponent } from './general-note-details/general-note-details.component';
import { GeneralNoteAddEditComponent } from './general-note-add-edit/general-note-add-edit.component';
import { GeneralNoteServiceService } from './general-note-service.service';
import { GeneralNotesRoutingModule } from './general-note-routing';
import { DataTableModule} from 'angular2-datatable';
import { MaterialModule} from '@angular/material';
import { FormsModule} from '@angular/forms';
import { SharedModule } from '../../shared/shared.module';
import { AngularMultiSelectModule } from 'angular2-multiselect-checkbox-dropdown/angular2-multiselect-dropdown';
@NgModule({
  imports: [
    DataTableModule,
    MaterialModule,
    FormsModule,
    SharedModule,
    CommonModule,
    GeneralNotesRoutingModule
  ],
  declarations: [GeneralNoteListComponent, GeneralNoteDetailsComponent, GeneralNoteAddEditComponent],
  providers:[GeneralNoteServiceService]
})
export class GeneralNotesModule { }
