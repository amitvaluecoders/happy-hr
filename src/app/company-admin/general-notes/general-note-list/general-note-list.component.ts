import { Component, OnInit } from '@angular/core';
import { MdSnackBar, MdDialog, MdDialogRef } from '@angular/material';
import { GeneralNoteDetailsComponent } from "../general-note-details/general-note-details.component";
import { GeneralNoteServiceService } from '../general-note-service.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { CoreService } from '../../../shared/services/core.service';
import { APPCONFIG } from '../../../config';

@Component({
  selector: 'app-general-note-list',
  templateUrl: './general-note-list.component.html',
  styleUrls: ['./general-note-list.component.scss']
})
export class GeneralNoteListComponent implements OnInit {

  public employment="";
  public position="";
  public notesList=[];
  public isProcessing:Boolean;
  public unsubscriber:any;
  public serchFor='';
  public serchOwner='';
  public isApplyBtnClicked=false;
  permission;

  constructor(public service:GeneralNoteServiceService,public dialog: MdDialog,
               public router:Router,public activatedRoute:ActivatedRoute,
               public coreService:CoreService) { }

  ngOnInit() {
    this.getNotesList();
    APPCONFIG.heading="File notes";
    this.permission=this.coreService.getNonRoutePermission();
  }

    getNotesList(){
     this.isProcessing=true;    
     this.unsubscriber = this.service.getNoteList().subscribe(
          d => {
              this.isProcessing=false;
              if (d.code == "200"){
                this.notesList=d.data.filter(i=>{
                  i['isDeleteProcessing']=false;
                  return i; 
                }); 

              }
              else this.coreService.notify("Unsuccessful",d.message, 0); 
          },
          error =>{
            this.coreService.notify("Unsuccessful", error.message, 0);
            this.isProcessing=false
          } 
      )
  }

   onDelete(note){
     note.isDeleteProcessing=true;    
     this.unsubscriber = this.service.deleteNote(note.companyGeneralNoteID).subscribe(
          d => {
              note.isDeleteProcessing=false;
              if(d.code=='200'){
                this.notesList = this.notesList.filter(i=>i.companyGeneralNoteID != note.companyGeneralNoteID) ;
                this.coreService.notify("Successful",d.message, 1); 
              }
              else this.coreService.notify("Unsuccessful",d.message, 0); 
          },
          error =>{
            this.coreService.notify("Unsuccessful", error.message, 0);
            note.isDeleteProcessing=false
          } 
      )
  }


  onViewDetails(d){
         let dialogRef = this.dialog.open(GeneralNoteDetailsComponent,{width:"1000px"});  
          dialogRef.componentInstance.notesDetails=d;
          dialogRef.afterClosed().subscribe(result => {});   
  }

  onDownload(d){
    window.open(d.generalNoteFileUrl);
  }

  onAddNote(){
       this.router.navigate([`/app/company-admin/general-notes/create`]);
  }
  onEdit(d){
       this.router.navigate([`/app/company-admin/general-notes/update/${d.companyGeneralNoteID}`]);
  }
}
