import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GeneralNoteListComponent } from './general-note-list.component';

describe('GeneralNoteListComponent', () => {
  let component: GeneralNoteListComponent;
  let fixture: ComponentFixture<GeneralNoteListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GeneralNoteListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GeneralNoteListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
