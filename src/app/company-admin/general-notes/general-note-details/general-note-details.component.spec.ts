import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GeneralNoteDetailsComponent } from './general-note-details.component';

describe('GeneralNoteDetailsComponent', () => {
  let component: GeneralNoteDetailsComponent;
  let fixture: ComponentFixture<GeneralNoteDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GeneralNoteDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GeneralNoteDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
