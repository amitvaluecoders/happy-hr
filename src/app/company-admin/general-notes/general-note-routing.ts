import {  ModuleWithProviders  } from '@angular/core';
import {  RouterModule,Routes  } from '@angular/router';

import { GeneralNoteAddEditComponent } from './general-note-add-edit/general-note-add-edit.component';
import { GeneralNoteDetailsComponent } from './general-note-details/general-note-details.component';
import { GeneralNoteListComponent } from './general-note-list/general-note-list.component';

export const GeneralNotesPagesRoutes: Routes = [
    {
        path: '',
        canActivate:[],
        children: [      
            { path: '', component: GeneralNoteListComponent},
            { path: 'create', component: GeneralNoteAddEditComponent},                 
            { path: 'update/:id', component: GeneralNoteAddEditComponent},                 
            { path: 'details', component: GeneralNoteDetailsComponent},                 
        ]
    }
];

export const GeneralNotesRoutingModule = RouterModule.forChild(GeneralNotesPagesRoutes);
