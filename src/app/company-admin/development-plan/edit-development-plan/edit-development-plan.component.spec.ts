import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditDevelopmentPlanComponent } from './edit-development-plan.component';

describe('EditDevelopmentPlanComponent', () => {
  let component: EditDevelopmentPlanComponent;
  let fixture: ComponentFixture<EditDevelopmentPlanComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditDevelopmentPlanComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditDevelopmentPlanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
