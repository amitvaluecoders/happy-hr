import { Component, OnInit, AfterViewInit } from '@angular/core';
import { DevelopmentPlanService } from '../development-plan.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { CoreService } from '../../../shared/services/core.service';
import {APPCONFIG } from '../../../config';
import { IDatePickerConfig } from 'ng2-date-picker';
import * as moment from 'moment';


@Component({
  selector: 'app-edit-development-plan',
  templateUrl: './edit-development-plan.component.html',
  styleUrls: ['./edit-development-plan.component.scss']
})
export class EditDevelopmentPlanComponent implements OnInit {

  assessmentTimeConfig: IDatePickerConfig = {};
  config: IDatePickerConfig = {};  
      public tempTrainingTime:any;
  public tempAssessmentTime:any;
  selectedItems=[];
   trainingTimeFlag =0;
  assessmentTimeFlag=0;
  public progress;
  thirdPartyList=[];
  public originalPIPToggle:Boolean;
  public invitelistCollapse:Boolean;
  public thirdpartyListProcessing=false;
  public isThirdPartyAlreadyExist:Boolean;
  public isButtonClicked=false;
  public isEnterPressed=false;
  public thirdPartyName='';
  dropdownListEmployees = [];
  selectedParty = [];
  invitedropdownSettings = {};
  public developmentPlan={
      developmentFor:{imageUrl:'',employee:'',employeeProfile:''},
      assinedBy:{imageUrl:'',employee:'',employeeProfile:''},
      thiredParty:[],
       title:'',
      reason:'',
      objective:'',
      actionPlan:'',
      trainingDate:'',
      trainingTime:'',
      assessmentDate:'',
      assessmentTime:'',
       trainingNote:[],
      assessmentNote:[]
  };
  public developmentPlanReAssessment={
      title:'',
      reason:'',
      objective:'',
      actionPlan:'',
      trainingDate:new Date(),
      trainingTime:'',
      assessmentDate:new Date(),
      assessmentTime:'',
  };
  public isCreateProcessing=false;
  public isProcessing=false;
  public counter=0;

  constructor( public developmentPlanService: DevelopmentPlanService,
               public router: Router, public activatedRoute: ActivatedRoute,
               public coreService: CoreService) { 
                this.assessmentTimeConfig.locale='en';
                this.assessmentTimeConfig.format='YYYY-MM-DD hh:mm:ss';
                this.assessmentTimeConfig.showMultipleYearsNavigation=true;
                this.assessmentTimeConfig.weekDayFormat='dd';
                this.assessmentTimeConfig.disableKeypress=true,
                this.assessmentTimeConfig.monthFormat='MMM YYYY';
                this.assessmentTimeConfig.min=moment().add(+1, 'days');
                this.config.locale = "en";
                this.config.format = "YYYY-MM-DD hh:mm:ss ";
                this.config.showMultipleYearsNavigation = true;
                this.config.weekDayFormat = 'dd';
                this.config.disableKeypress = true;
                this.config.monthFormat="MMM YYYY";
                this.config.min=moment();
               }

  ngOnInit() {
    this.getDevelopmentPlanDetails();
    APPCONFIG.heading = "Development plan update";
    this.progressBarSteps();
    this.dropdownListEmployees = [];
    this.invitedropdownSettings = {
          singleSelection: false, 
          text:"Select Third Party",
          selectAllText:'Select All',
          unSelectAllText:'UnSelect All',
          enableSearchFilter: true,
          classes:"myclass thirdParty",
          badgeShowLimit:0
        }; 
  }

    getEmployeeeList(thirdparty) {
     this.thirdpartyListProcessing = true;
     this.developmentPlanService.getAllEmployee().subscribe(
          d => {
              if (d.code == "200") {
                  this.dropdownListEmployees=d.data.filter(item =>{
                    item['isSelected']=false;
                     item['itemName']=item.name;
                    item['id']=item.userId;
                    if(this.developmentPlanReAssessment['assinedBy'].employeeID == item.userId ){
                         item['isSelected']=true;
                         this.selectedItems.push(item);
                     }
                    if(  item.userId==this.developmentPlan.developmentFor['employeeID'] )return null;       
                      return item;
                  });     
                   this.thirdPartyList=JSON.parse(JSON.stringify(this.dropdownListEmployees))
                this.thirdPartyList=this.thirdPartyList.filter(i=>{
                    if(this.developmentPlanReAssessment['assinedBy'].employeeID == i.userId )return null;
                 return i;
                })
              this.isProcessing = false;         
              }
              else this.coreService.notify("Unsuccessful", d.message, 0);
          },
          error => this.coreService.notify("Unsuccessful", error.message, 0),
          () => {  this.thirdpartyListProcessing = false;}
      )
  }

  ngAfterViewInit(){
    this.dropdownListEmployees.filter(function(item, i){
        $('.thirdParty ul li').eq(i).prepend(`<img src="assets/images/${item.img}" alt="" />`);
        $('.thirdParty ul li').eq(i).children('label').append('<span>' + item.role + '</span>');
    });

  }

  progressBarSteps() {
      this.progress={
          progress_percent_value:25, //progress percent
          total_steps:4,
          current_step:1,
          circle_container_width:25, //circle container width in percent
          steps:[
              // {num:1,data:"Select Role"},
               {num:1,data:"Re-Assess: Create Development Plan", active:true},
               {num:2,data:"Training"},
               {num:3,data:"Assessment"},
               {num:4,data:"Result"}
          ]  
       }         
  }

  attactImageAndRolesDropDownList(){

      if(this.counter == 0){
    this.dropdownListEmployees.filter(function(item, i){
        $('.assmng ul li').eq(i).prepend(`<img src="${item.imageUrl}" alt="" />`);
        $('.assmng ul li').eq(i).children('label').append('<span>' + item.designation+ '</span>');
    });
    this.dropdownListEmployees.filter(function(item, i){
        $('.thirdParty ul li').eq(i).prepend(`<img src="${item.imageUrl}" alt="" />`);
        $('.thirdParty ul li').eq(i).children('label').append('<span>' + item.designation || '-'  + '</span>');
    });
      }
      this.counter++;
  }

     getDevelopmentPlanDetails() {
     this.isProcessing = true;
     this.developmentPlanService.getDevelopmentPlanDetails(this.activatedRoute.snapshot.params['id']).subscribe(
          d => {
            if (d.code == "200") {        
              this.developmentPlan=d.data; 
              this.developmentPlanReAssessment=JSON.parse(JSON.stringify(this.developmentPlan));
              this.getEmployeeeList(this.developmentPlanReAssessment['thirdParty']);          
              this.tempTrainingTime= moment(this.developmentPlanReAssessment['trainingTime']).format("YYYY-MM-DD hh:mm:ss");   
              this.tempAssessmentTime= moment(this.developmentPlanReAssessment['assessmentTime']).format("YYYY-MM-DD hh:mm:ss");   
            //   this.isProcessing = false;   
         
            }
            else this.coreService.notify("Unsuccessful", d.message, 0);
          },
          error => this.coreService.notify("Unsuccessful", "Error while getting development details", 0),
          () => { }
      )
  }

onUpdate(isvalid){
      this.isButtonClicked=true;
      if(isvalid){
          this.developmentPlanReAssessment['thirdParty']=[];
             for(let i of this.thirdPartyList){
               if(i.isSelected){
                 this.developmentPlanReAssessment['thirdParty'].push({
                     type:(i.external)?"External":"Internal",
                     userID:(!i.external)?i.userId:'',
                     email:(i.external)?i.name:''
                });  
               }
           }
        //    this.developmentPlanReAssessment['assessmentTime']=this.developmentPlanReAssessment['assessmentTime'].toString();
        //    this.developmentPlanReAssessment['trainingTime']=this.developmentPlanReAssessment['trainingTime'].toString();
           this.developmentPlanReAssessment['assessmentNote']='';
           this.developmentPlanReAssessment['managerNote']='';
           this.developmentPlanReAssessment['result']='';
           this.developmentPlanReAssessment['status']='';
        
          this.createDevelopmentPlan();
        
      }   
  }

    // assign/reassisgn training time on change.
    ontrainingTimeChange(){
        if(this.trainingTimeFlag >0) {
            this.developmentPlanReAssessment.trainingTime = moment(this.tempTrainingTime).format("YYYY-MM-DD hh:mm:ss");   
            let copy: IDatePickerConfig = JSON.parse(JSON.stringify(this.assessmentTimeConfig));
            copy.min = moment( this.developmentPlanReAssessment.trainingTime);
            this.assessmentTimeConfig = copy;
            this.developmentPlanReAssessment.assessmentTime='';
            this.tempAssessmentTime= null;
        }
        this.trainingTimeFlag++;
    }

// assign/reassisgn assessment time on change.
    onAssessmentTimeChange(){      
        if(this.assessmentTimeFlag >0) this.developmentPlanReAssessment.assessmentTime = moment(this.tempAssessmentTime).format("YYYY-MM-DD hh:mm:ss");
        this.assessmentTimeFlag++;
        
    }


  createDevelopmentPlan(){
    
    this.isCreateProcessing=true;
  this.developmentPlanReAssessment['employeeID'] = this.developmentPlanReAssessment['developmentFor'].employeeID;
  this.developmentPlanReAssessment['managerID'] = this.developmentPlanReAssessment['assinedBy'].employeeID;
     let reqBody=JSON.parse(JSON.stringify(this.developmentPlanReAssessment));
        // reqBody['assessmentDate']=reqBody['assessmentDate'].toString();
        // reqBody['trainingDate']=reqBody['trainingDate'].toString();
        this.developmentPlanService.updateDevelopmentPLan(reqBody).subscribe(
          d => {
              if (d.code == "200") {
                 this.router.navigate([`/app/company-admin/listing-page/development-plan-list`]);
                this.isCreateProcessing = false; 
                this.coreService.notify("Successful",d.message, 1);  
              }
              else{
                this.isCreateProcessing = false; 
                this.coreService.notify("Unsuccessful",d.message, 0);  
              } 
          },
          error =>{
               this.isCreateProcessing = false;
               this.coreService.notify("Unsuccessful", error.message, 0) 
        } 
      )
  }

  onCancel(){
        this.router.navigate([`/app/company-admin/listing-page/development-plan-list`]);
  }

  // methods to add external third party 
  onEnterExternalThirdParty(isvalid){
      if(this.thirdPartyList){
      this.isThirdPartyAlreadyExist=false;
      this.isEnterPressed=true;
      if(isvalid){
            for(let c of this.thirdPartyList ){
                if(c.email==this.thirdPartyName)this.isThirdPartyAlreadyExist=true;
            }
            if(!this.isThirdPartyAlreadyExist){
                this.thirdPartyList.splice(0,0,{
                    isSelected:true,
                    name:this.thirdPartyName,
                    email:this.thirdPartyName,
                    external:true,
                    imageUrl:"http://happyhrapi.n1.iworklab.com/public/user-profile-image/user.png"
                })
                this.thirdPartyName='';
                this.isEnterPressed=false;
            } 
      }
      }

  }
  onEditDevelopmentPlan(id){}

onViewUserDetails(id){
                var newWindow = window.open(`/#/app/company-admin/employee-management/profile/${id}`);   
    }
}
