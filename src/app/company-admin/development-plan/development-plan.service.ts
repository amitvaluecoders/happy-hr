import { Injectable } from '@angular/core';
import { RestfulLaravelService} from '../../shared/services/restful-laravel.service';


@Injectable()
export class DevelopmentPlanService {
  public developmentPlan:any;

  constructor(public restfulWebService:RestfulLaravelService) { }

  getAllEmployee():any{
   return this.restfulWebService.get('show-all-employee')
  }
  getAllManager(id):any{
    return this.restfulWebService.get(`show-manager-and-ceo-and-fm/${id}`)
   }

  getAllDevelopmentPlans(reqBody):any{
   return this.restfulWebService.get(`development-plan-list/${encodeURIComponent(reqBody)}`)
  }

  getDataForFilters():any{
   return this.restfulWebService.get('development-plan-filter-data')
  }

  getDevelopmentPlanDetails(reqBody):any{
   return this.restfulWebService.get(`development-plan-detail/${reqBody}`)
  }

  createDevelopmentPLan(reqBody):any{
   return this.restfulWebService.post('development-plan-create',reqBody)
  }

   onAddManagerNote(reqBody):any{
   return this.restfulWebService.post('development-plan-training',reqBody)
  }

    onAssessment(reqBody):any{
   return this.restfulWebService.post('development-plan-assessment',reqBody)
  }
  
  getReScheduleData(id):any{
   return this.restfulWebService.get(`development-plan-reschedule-list/${id}`)
  }

  responseReSchedule(reqBody):any{
   return this.restfulWebService.post('development-plan-accept-reject-reschedule',reqBody)
  }

  reScheduleDevelopemnetPlan(reqBody):any{
   return this.restfulWebService.post('development-plan-update-time',reqBody)
  }

  onAddResult(reqBody):any{
   return this.restfulWebService.post('development-plan-result',reqBody)
  }

updateDevelopmentPLan(reqBody):any{
   return this.restfulWebService.put('development-plan-update',reqBody)
  }

  updateDevelopmentPlanNotes(reqBody):any{
   return this.restfulWebService.put('development-plan-edit-note',reqBody)
  }

 deleteDevelopmentPlan(reqBody):any{
    return this.restfulWebService.delete(`development-plan-delete/${reqBody}`)
  }


}
