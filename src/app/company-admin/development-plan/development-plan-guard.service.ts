import { Injectable } from '@angular/core';
import { CanActivate, Router, RouterStateSnapshot, ActivatedRouteSnapshot } from "@angular/router";
import { CoreService } from '../../shared/services/core.service';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';

@Injectable()
export class DevelopmentPlanGuardService {

    public module = "developmentPlan";

    constructor(private _core: CoreService, private router: Router) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        let url: string;
        if (route['_routeConfig']['path'])
            url = route['_routeConfig']['path']
        else url = state.url;
        if (this.checkHavePermission(url)) {
            return true;
        } else {
            this.router.navigate(['/extra/unauthorized']);
            return false;
        }
    }

    private checkHavePermission(path) {
        switch (path) {
            case "development-plan-list":
                return this._core.getModulePermission(this.module, 'view');
            case "/app/company-admin/development-plan":
                return this._core.getModulePermission(this.module, 'view');
            case "view/:id":
                return this._core.getModulePermission(this.module, 'view');
            case "Details/:status/:id":
                return this._core.getModulePermission(this.module, 'view');
            case "create/:id":
                return this._core.getModulePermission(this.module, 'add');
            case "training/:id":
                return this._core.getModulePermission(this.module, 'view');
            case "assessment/:id":
                return this._core.getModulePermission(this.module, 'view');
            case "result/:id":
                return this._core.getModulePermission(this.module, 'view');
            case "reassessment-create/:userID/:id":
                return this._core.getModulePermission(this.module, 'add');
            case "reassessment-training/:id":
                return this._core.getModulePermission(this.module, 'view');
            case "reassessment-assessment/:id":
                return this._core.getModulePermission(this.module, 'view');
            case "reassessment-result/:id":
                return this._core.getModulePermission(this.module, 'view');
            case "edit/:id":
                return this._core.getModulePermission(this.module, 'edit');
            default:
                return false;
        }
    }
}
