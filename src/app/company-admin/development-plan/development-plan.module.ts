import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from '@angular/material';
import { FormsModule } from '@angular/forms';
import { AngularMultiSelectModule } from 'angular2-multiselect-checkbox-dropdown/angular2-multiselect-dropdown';
import { SharedModule } from '../../shared/shared.module';
import { MyDatePickerModule } from 'mydatepicker';
import { NguiDatetimePickerModule } from '@ngui/datetime-picker';

import { DevelopmentPlanRoutingModule } from './development-plan.routing';
import { DevelopmentPlanService } from './development-plan.service';
import { DevelopmentPlanDetailComponent } from './development-plan-detail/development-plan-detail.component';
import { CreateDevelopmentPlanComponent } from './create-development-plan/create-development-plan.component';
import { TrainingComponent } from './training/training.component';
import { AssessmentComponent,DevelopmentResultPopUpComponent } from './assessment/assessment.component';
import { ResultComponent } from './result/result.component';
import { ReassessmentDevelopmentPlanComponent } from './reassessment-development-plan/reassessment-development-plan.component';
import { ReassessmentTrainingComponent } from './reassessment-training/reassessment-training.component';
import { ReassessmentNotesComponent } from './reassessment-notes/reassessment-notes.component';
import { ReassessmentResultComponent } from './reassessment-result/reassessment-result.component';
import { EditDevelopmentPlanComponent } from './edit-development-plan/edit-development-plan.component';
import { EditDevelopmentPlanNoteComponent } from './development-plan-detail/edit-development-plan-note/edit-development-plan-note.component';
import { DevelopmentPlanGuardService} from './development-plan-guard.service';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    MaterialModule,
    SharedModule,
    DevelopmentPlanRoutingModule,
    AngularMultiSelectModule,
    MyDatePickerModule,
    NguiDatetimePickerModule
  ],
  entryComponents: [DevelopmentResultPopUpComponent,EditDevelopmentPlanNoteComponent],
  declarations: [DevelopmentPlanDetailComponent,DevelopmentResultPopUpComponent, CreateDevelopmentPlanComponent, TrainingComponent, AssessmentComponent, ResultComponent, ReassessmentDevelopmentPlanComponent, ReassessmentTrainingComponent, ReassessmentNotesComponent, ReassessmentResultComponent, EditDevelopmentPlanComponent, EditDevelopmentPlanNoteComponent],
  providers: [DevelopmentPlanService,DevelopmentPlanGuardService]
})
export class DevelopmentPlanModule { }
