import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReassessmentResultComponent } from './reassessment-result.component';

describe('ReassessmentResultComponent', () => {
  let component: ReassessmentResultComponent;
  let fixture: ComponentFixture<ReassessmentResultComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReassessmentResultComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReassessmentResultComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
