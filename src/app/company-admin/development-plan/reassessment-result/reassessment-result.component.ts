import { Component, OnInit } from '@angular/core';
import { DevelopmentPlanService } from '../development-plan.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { CoreService } from '../../../shared/services/core.service';
import { APPCONFIG } from '../../../config';

@Component({
  selector: 'app-reassessment-result',
  templateUrl: './reassessment-result.component.html',
  styleUrls: ['./reassessment-result.component.scss']
})
export class ReassessmentResultComponent implements OnInit {


  public progress;
  dropdownListEmployees = [];
  selectedParty = [];
  public originalPIPToggle: Boolean;
  public invitelistCollapse: Boolean;
  invitedropdownSettings = {};
  public developmentPlan = {
    dpInsetTime: '',
    developmentFor: { imageUrl: '', employee: '', employeeProfile: '' },
    assinedBy: { imageUrl: '', employee: '', employeeProfile: '' },
    thiredParty: [],
    title: '',
    reason: '',
    objective: '',
    actionPlan: '',
    trainingDate: '',
    trainingTime: '',
    assessmentDate: '',
    assessmentTime: '',
  };
  public developmentPlanReAssessment = {
    dpInsetTime: '',
    result: '',
    developmentFor: { imageUrl: '', employee: '', employeeProfile: '' },
    assinedBy: { imageUrl: '', employee: '', employeeProfile: '' },
    thiredParty: [],
    title: '',
    reason: '',
    objective: '',
    actionPlan: '',
    trainingDate: new Date(),
    trainingTime: '',
    assessmentDate: new Date(),
    assessmentTime: '',
  };
  public isButtonClicked = false;
  public isCreateProcessing = false;
  public isProcessing = false;
  public counter = 0;
  public developmentPlanManagerNote = {
    developmentPlanID: "",
    assessmentNote: "",
  };
  permission;

  constructor(public developmentPlanService: DevelopmentPlanService,
    public router: Router, public activatedRoute: ActivatedRoute,
    public coreService: CoreService) { }


  ngOnInit() {
    this.getDevelopmentPlanDetails();
    APPCONFIG.heading = "Re-assessment";
    this.permission = this.coreService.getNonRoutePermission();
    this.progressBarSteps();
  }


  onTime(time){ return new Date(time);}

  progressBarSteps() {
    this.progress = {
      progress_percent_value: 100, //progress percent
      total_steps: 4,
      current_step: 4,
      circle_container_width: 25, //circle container width in percent
      steps: [
        // {num:1,data:"Select Role"},
        { num: 1, data: "Re-Assess: Create development plan" },
        { num: 2, data: "Training" },
        { num: 3, data: "Assessment" },
        { num: 4, data: "Result", active: true }
      ]
    }
  }

  getDevelopmentPlanDetails() {
    this.isProcessing = true;
    this.developmentPlanService.getDevelopmentPlanDetails(this.activatedRoute.snapshot.params['id']).subscribe(
      d => {
        if (d.code == "200") {
          this.developmentPlanReAssessment = d.data;
          this.developmentPlan = this.developmentPlanReAssessment['oldDevelopment'];
          let assTime = new Date(this.developmentPlanReAssessment['assessmentTime'])
          let traTime = new Date(this.developmentPlanReAssessment['trainingTime'])
          this.developmentPlanReAssessment['trainingTime'] = traTime.getHours() + ":" + traTime.getMinutes() + ":" + traTime.getSeconds();
          this.developmentPlanReAssessment['trainingDate'] = traTime;
          this.developmentPlanReAssessment['assessmentTime'] = assTime.getHours() + ":" + assTime.getMinutes() + ":" + assTime.getSeconds();
          this.developmentPlanReAssessment['assessmentDate'] = assTime;
          this.isProcessing = false;

        }

        else this.coreService.notify("Unsuccessful", d.message, 0);
      },
      error => this.coreService.notify("Unsuccessful", "Error while getting development details", 0),
      () => { }
    )
  }

  onReAssessment(id) {
    this.router.navigate([`/app/company-admin/development-plan/reassessment-create/${id}`]);
  }


}
