import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReassessmentDevelopmentPlanComponent } from './reassessment-development-plan.component';

describe('ReassessmentDevelopmentPlanComponent', () => {
  let component: ReassessmentDevelopmentPlanComponent;
  let fixture: ComponentFixture<ReassessmentDevelopmentPlanComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReassessmentDevelopmentPlanComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReassessmentDevelopmentPlanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
