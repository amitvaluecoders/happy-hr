import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReassessmentNotesComponent } from './reassessment-notes.component';

describe('ReassessmentNotesComponent', () => {
  let component: ReassessmentNotesComponent;
  let fixture: ComponentFixture<ReassessmentNotesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReassessmentNotesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReassessmentNotesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
