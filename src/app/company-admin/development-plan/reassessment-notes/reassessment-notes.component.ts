import { Component, OnInit } from '@angular/core';
import { DevelopmentPlanService } from '../development-plan.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { CoreService } from '../../../shared/services/core.service';
import { APPCONFIG } from '../../../config';
import { MdSnackBar, MdDialog, MdDialogRef } from '@angular/material';
import { DevelopmentResultPopUpComponent } from '../assessment/assessment.component';
import { EditDevelopmentPlanNoteComponent } from '../development-plan-detail/edit-development-plan-note/edit-development-plan-note.component';
import { Cookie } from 'ng2-cookies/ng2-cookies';


@Component({
  selector: 'app-reassessment-notes',
  templateUrl: './reassessment-notes.component.html',
  styleUrls: ['./reassessment-notes.component.scss']
})
export class ReassessmentNotesComponent implements OnInit {

  public originalPIPToggle=[];
  public trainingNoteAddPermission = true;
  public assessmentNoteAddPermission = true;
  public progress;
  public heading = '';
  public temp = '';
  public loggedInUser: any;
  public isTraCreateProcessing = false;
  public serviceDetails: any;
  dropdownListEmployees = [];
  developmentPlans=[];
  selectedParty = [];
  public invitelistCollapse: Boolean;
  invitedropdownSettings = {};
  public developmentPlan=[];
  public tempDP: any;
  public developmentPlanNew = {
    insertTime: '',
    status: '',
    developmentFor: { imageUrl: '', employee: '', employeeProfile: '' },
    assinedBy: { imageUrl: '', employee: '', employeeProfile: '' },
    thirdParty: [],
    title: '',
    reason: '',
    objective: '',
    actionPlan: '',
    trainingDate: new Date(),
    trainingTime: '',
    assessmentDate: new Date(),
    assessmentTime: '',
    trainingNote: [],
    assessmentNote: [],
    employeeNote: []
  };
  public isButtonClicked = false;
  public isTraButtonClicked = false;
  public isCreateProcessing = false;
  public isResultProcessing = false;
  public isProcessing = false;
  public counter = 0;
  public developmentPlanManagerNote = {
    developmentPlanID: "",
    assessmentNote: "",
    managerNote: "",
    managerTraNote: ''
  };

  public reqBody = {
    search:"",
    filter:{
      result:{},
      employeeIDs:[],
      managerIDs:[],
      due:[],
      status:{}
    }
  }
  permission;

  constructor(public developmentPlanService: DevelopmentPlanService,
    public router: Router, public activatedRoute: ActivatedRoute,
    public coreService: CoreService, public dialog: MdDialog) { }


  ngOnInit() {
    this.loggedInUser = JSON.parse(Cookie.get('happyhr-userInfo')) || JSON.parse(localStorage.getItem('happyhr-userInfo'));
    this.permission = this.coreService.getNonRoutePermission();
    this.getDevelopmentPlans();
    //this.serviceDetails = this.coreService.getDevelopmentPlan();
    console.log(this.serviceDetails);
    // if (!this.serviceDetails) {
    //   this.router.navigate([`/app/company-admin/listing-page/development-plan-list`]);
    //   console.log("indsssssssssss if");
    // }
    // else {
    //   this.progressBarSteps(this.serviceDetails);
    //   this.getDevelopmentPlanDetails();
    //   console.log("indsssssssssss  else");
    // }


  }
  getDevelopmentPlans() {
    this.isProcessing = true;
     this.developmentPlanService.getAllDevelopmentPlans(JSON.stringify(this.reqBody)).subscribe(
         d => {
             if (d.code == "200") {
               this.developmentPlans=d.data; 
                this.developmentPlans.filter(item=>{
                    if(item.developmentPlanID==this.activatedRoute.snapshot.params['id']){
                      this.serviceDetails= item;  
                    }
                    
                })
                if(!this.serviceDetails){
                  this.router.navigate([`/app/company-admin/listing-page/development-plan-list`]);
                  console.log("indsssssssssss if");
                }
                else{
                  console.log("data...",this.serviceDetails);
                  this.progressBarSteps(this.serviceDetails);
                  this.getDevelopmentPlanDetails();
                  console.log("indsssssssssss  else");
                }
               this.isProcessing = false;    
             }
             else this.coreService.notify("Unsuccessful", d.message, 0);
         },
         error => this.coreService.notify("Unsuccessful", "Error while getting development details", 0),
         () => { }
     )
 }

  progressBarSteps(data) {
    this.tempDP = data;
    APPCONFIG.heading = "Create development plan";
    this.progress = {
      progress_percent_value: 25, //progress percent
      total_steps: 4,
      current_step: 1,
      circle_container_width: 25, //circle container width in percent
      steps: [
        { num: 1, data: (data.mode == 'isNew') ? "Create development plan" : "Re-Assess: Create development plan", active: false },
        { num: 2, data: "Training", active: false },
        { num: 3, data: "Assessment", active: false },
        { num: 4, data: "Result", active: false }
      ]
    }

    if ((data.status == 'Under training') || (data.status == 'Awaiting Employee Acceptance') || (data.status == 'Accepted')) {
      APPCONFIG.heading = "Training";
      this.progress.progress_percent_value = 50;
      this.progress.current_step = 2;
      this.progress.steps[1].active = true;
    }
    if (data.status == 'Under assessment') {
      APPCONFIG.heading = "Assessment";
      this.progress.progress_percent_value = 75;
      this.progress.current_step = 3;
      this.progress.steps[2].active = true;
    }
    if (data.status == 'Complete') {
      APPCONFIG.heading = "Result";
      this.progress.progress_percent_value = 100;
      this.progress.current_step = 4;
      this.progress.steps[3].active = true;
    }
    this.heading = APPCONFIG.heading;
  }
  onTime(time){
    return new Date(time);
  }

  getDevelopmentPlanDetails() {
    this.isProcessing = true;
    this.developmentPlanService.getDevelopmentPlanDetails(this.activatedRoute.snapshot.params['id']).subscribe(
      d => {
        if (d.code == "200") {
          this.developmentPlanNew = d.data;
          this.developmentPlanManagerNote.developmentPlanID = this.developmentPlanNew['developmentPlanID'];
          this.developmentPlan = JSON.parse(JSON.stringify(this.developmentPlanNew['oldDevelopment']));
          for (let i in this.developmentPlan) { this.originalPIPToggle[i] = false }
          let assTime = new Date(this.developmentPlanNew['assessmentTime'])
          let traTime = new Date(this.developmentPlanNew['trainingTime'])
          // this.developmentPlanNew['trainingTime'] = traTime.getHours() + ":" + traTime.getMinutes() + ":" + traTime.getSeconds();
          // this.developmentPlanNew['trainingDate'] = traTime;
          // this.developmentPlanNew['assessmentTime'] = assTime.getHours() + ":" + assTime.getMinutes() + ":" + assTime.getSeconds();
          // this.developmentPlanNew['assessmentDate'] = assTime;
          this.isProcessing = false;
          for (let k of this.developmentPlanNew['thirdParty']) {
            k['id'] = k['employeeID'];
            k['itemName'] = k['employee'];
            this.selectedParty.push(k);
          }
           for (let c of this.developmentPlanNew.trainingNote) {
            if (c.noteByUser.employeeID == this.loggedInUser.userID) this.trainingNoteAddPermission = false;
          }
           for (let c of this.developmentPlanNew.assessmentNote) {
            if (c.noteByUser.employeeID == this.loggedInUser.userID) this.assessmentNoteAddPermission = false;
          }
          this.progressBarSteps(this.developmentPlanNew);
        }
        else this.coreService.notify("Unsuccessful", d.message, 0);
      },
      error => this.coreService.notify("Unsuccessful", "Error while getting development details", 0),
      () => { }
    )
  }

  onEditDevelopmentPlan(id) {
    this.router.navigate([`/app/company-admin/development-plan/edit/${id}`]);
  }


  onAddResult(id,result) {

        this.isResultProcessing = true;
        let reqBody = { developmentPlanID: id, result: result }
        this.developmentPlanService.onAddResult(reqBody).subscribe(
          d => {
            if (d.code == "200") {
              this.coreService.notify("Successful", d.message, 1);
              this.developmentPlanNew['result']=result;
              this.developmentPlanNew['status']='Complete';              
              if(result=='Pass' || result=='Fail')this.router.navigate([`/app/company-admin/listing-page/development-plan-list`]);
              this.isResultProcessing = false;
            }
            else {
              this.coreService.notify("Unsuccessful", d.message, 0);
              this.isResultProcessing = false;
            }
          },
          error => {
            this.coreService.notify("Unsuccessful", error.message, 0);
            this.isResultProcessing = false;
          }
        )
    
  }
  onAddResultNotPassed(id,result){
    // this.isResultProcessing = true;
    // let reqBody = { developmentPlanID: id, result: result }
    // this.developmentPlanService.onAddResult(reqBody).subscribe(
    //   d => {
    //     if (d.code == "200") {
    //       this.coreService.notify("Successful", d.message, 1);
    //       this.developmentPlanNew['result']=result;
    //       this.developmentPlanNew['status']='Complete';              
    //       if(result=='Pass' || result=='Fail')this.router.navigate([`/app/company-admin/listing-page/development-plan-list`]);
    //       this.isResultProcessing = false;
    //     }
    //     else {
    //       this.coreService.notify("Unsuccessful", d.message, 0);
    //       this.isResultProcessing = false;
    //     }
    //   },
    //   error => {
    //     this.coreService.notify("Unsuccessful", error.message, 0);
    //     this.isResultProcessing = false;
    //   }
    // )
  }

  onEditNotes(note) {
    this.temp = note;
    let dialogRef = this.dialog.open(EditDevelopmentPlanNoteComponent);
    dialogRef.componentInstance.noteDetails = JSON.parse(JSON.stringify(note));
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.temp = result;
        note.note = result.note;
        note.noteID = result.noteID;
        note.time = result.time;
        console.log("con", note);
      }
    });

  }

  onAddManagerTrainingNote(form) {
    this.isTraButtonClicked = true;
    if (form.form.valid) {
      let reqBody = {
        developmentPlanID: this.developmentPlanManagerNote.developmentPlanID,
        managerNote: this.developmentPlanManagerNote.managerTraNote
      };

      this.isTraCreateProcessing = true;
      this.developmentPlanService.onAddManagerNote(reqBody).subscribe(
        d => {
          if (d.code == "200") {
            this.coreService.notify("Successful", d.message, 1);
            this.router.navigate([`/app/company-admin/listing-page/development-plan-list`]);
            
            // this.developmentPlanNew.trainingNote.push(d.data)
            // form.reset();
            // this.isTraButtonClicked = false;
            // this.isTraCreateProcessing = false;
            // this.tempDP.status = 'Under Training';
            // this.progressBarSteps(this.tempDP);

          }
          else this.coreService.notify("Unsuccessful", d.message, 0);
        },
        error => {
          this.coreService.notify("Unsuccessful", error.message, 0);
          this.isTraCreateProcessing = false;
        }

      )
    }
  }

  onAddManagerAssessmentNote(form) {
    this.isButtonClicked = true;
    if (form.form.valid) {
      this.isCreateProcessing = true;
      this.developmentPlanManagerNote.assessmentNote = this.developmentPlanManagerNote.managerNote;
      this.developmentPlanService.onAssessment(this.developmentPlanManagerNote).subscribe(
        d => {
          this.isCreateProcessing = false;
          if (d.code == "200") {
            this.coreService.notify("Successful", d.message, 1);
            this.developmentPlanNew.assessmentNote.push(d.data);
            this.assessmentNoteAddPermission = false;
            form.reset();
            this.isButtonClicked = false;
            this.tempDP.status = 'Under Assessment'
            this.progressBarSteps(this.tempDP);
            this.isCreateProcessing = false;
          }
          else this.coreService.notify("Unsuccessful", d.message, 0);
        },
        error => {
          this.coreService.notify("Unsuccessful", error.message, 0);
          this.isCreateProcessing = false;
        }

      )
    }
  }


  onReAssessment(id,userID) {
    console.log("asdhjklad",userID);
    this.router.navigate([`/app/company-admin/development-plan/reassessment-create/${userID}/${id}`]);
  }




}
