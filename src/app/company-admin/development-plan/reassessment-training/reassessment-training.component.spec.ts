import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReassessmentTrainingComponent } from './reassessment-training.component';

describe('ReassessmentTrainingComponent', () => {
  let component: ReassessmentTrainingComponent;
  let fixture: ComponentFixture<ReassessmentTrainingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReassessmentTrainingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReassessmentTrainingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
