import { Component, OnInit } from '@angular/core';
import { DevelopmentPlanService } from '../development-plan.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { CoreService } from '../../../shared/services/core.service';
import { APPCONFIG } from '../../../config';


@Component({
  selector: 'app-reassessment-training',
  templateUrl: './reassessment-training.component.html',
  styleUrls: ['./reassessment-training.component.scss']
})
export class ReassessmentTrainingComponent implements OnInit {
  public progress;
  dropdownListEmployees = [];
  selectedParty = [];
  public originalPIPToggle: Boolean;
  public invitelistCollapse: Boolean;
  invitedropdownSettings = {};
  public developmentPlan = {
    dpInsetTime: '',
    developmentFor: { imageUrl: '', employee: '', employeeProfile: '' },
    assinedBy: { imageUrl: '', employee: '', employeeProfile: '' },
    thiredParty: [],
    title: '',
    reason: '',
    objective: '',
    actionPlan: '',
    trainingDate: '',
    trainingTime: '',
    assessmentDate: '',
    assessmentTime: '',
  };
  public developmentPlanReAssessment = {
    dpInsetTime: '',
    developmentFor: { imageUrl: '', employee: '', employeeProfile: '' },
    assinedBy: { imageUrl: '', employee: '', employeeProfile: '' },
    thiredParty: [],
    title: '',
    reason: '',
    objective: '',
    actionPlan: '',
    trainingDate: new Date(),
    trainingTime: '',
    assessmentDate: new Date(),
    assessmentTime: '',
  };
  public isButtonClicked = false;
  public isCreateProcessing = false;
  public isProcessing = false;
  public counter = 0;
  public developmentPlanManagerNote = {
    developmentPlanID: "",
    managerNote: "",

  };
  permission;

  constructor(public developmentPlanService: DevelopmentPlanService,
    public router: Router, public activatedRoute: ActivatedRoute,
    public coreService: CoreService) { }


  ngOnInit() {
    this.getDevelopmentPlanDetails();
    APPCONFIG.heading = "Re-assessment";
    this.permission = this.coreService.getNonRoutePermission();
    this.progressBarSteps();
  }

  onTime(time){return new Date(time);}

  progressBarSteps() {
    this.progress = {
      progress_percent_value: 50, //progress percent
      total_steps: 4,
      current_step: 2,
      circle_container_width: 25, //circle container width in percent
      steps: [
        // {num:1,data:"Select Role"},
        { num: 1, data: "Re-Assess: Create development plan" },
        { num: 2, data: "Training", active: true },
        { num: 3, data: "Assessment" },
        { num: 4, data: "Result" }
      ]
    }
  }

  getDevelopmentPlanDetails() {
    this.isProcessing = true;
    this.developmentPlanService.getDevelopmentPlanDetails(this.activatedRoute.snapshot.params['id']).subscribe(
      d => {
        if (d.code == "200") {
          this.developmentPlanReAssessment = d.data;
          this.developmentPlanManagerNote.developmentPlanID = this.developmentPlanReAssessment['developmentPlanID'];

          this.developmentPlan = this.developmentPlanReAssessment['oldDevelopment'];
          let assTime = new Date(this.developmentPlanReAssessment['assessmentTime'])
          let traTime = new Date(this.developmentPlanReAssessment['trainingTime'])
          this.developmentPlanReAssessment['trainingTime'] = traTime.getHours() + ":" + traTime.getMinutes() + ":" + traTime.getSeconds();
          this.developmentPlanReAssessment['trainingDate'] = traTime;
          this.developmentPlanReAssessment['assessmentTime'] = assTime.getHours() + ":" + assTime.getMinutes() + ":" + assTime.getSeconds();
          this.developmentPlanReAssessment['assessmentDate'] = assTime;
          this.isProcessing = false;
          for (let k of this.developmentPlanReAssessment['thiredParty']) {
            k['id'] = k['employeeID'];
            k['itemName'] = k['employee'];
            this.selectedParty.push(k);
          }
          console.log("plaaaaaan", this.developmentPlanReAssessment);
        }

        else this.coreService.notify("Unsuccessful", d.message, 0);
      },
      error => this.coreService.notify("Unsuccessful", "Error while getting development details", 0),
      () => { }
    )
  }


  onAddManagerNote(isvalid) {
    this.isButtonClicked = true;
    if (isvalid) {
      this.isCreateProcessing = true;
      this.developmentPlanService.onAddManagerNote(this.developmentPlanManagerNote).subscribe(
        d => {
          if (d.code == "200") {
            this.coreService.notify("Successful", d.message, 1);
            this.router.navigate([`/app/company-admin/listing-page/development-plan-list`]);
            this.isCreateProcessing = false;
          }
          else this.coreService.notify("Unsuccessful", d.message, 0);
        },
        error => {
          this.coreService.notify("Unsuccessful", error.message, 0);
          this.isCreateProcessing = false;
        }

      )
    }
  }

}
