import { ModuleWithProviders } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { DevelopmentPlanDetailComponent } from './development-plan-detail/development-plan-detail.component';
import { CreateDevelopmentPlanComponent } from './create-development-plan/create-development-plan.component';
import { TrainingComponent } from './training/training.component';
import { AssessmentComponent } from './assessment/assessment.component';
import { ResultComponent } from './result/result.component';
import { ReassessmentDevelopmentPlanComponent } from './reassessment-development-plan/reassessment-development-plan.component';
import { ReassessmentTrainingComponent } from './reassessment-training/reassessment-training.component';
import { ReassessmentNotesComponent } from './reassessment-notes/reassessment-notes.component';
import { ReassessmentResultComponent } from './reassessment-result/reassessment-result.component';
import { EditDevelopmentPlanComponent } from './edit-development-plan/edit-development-plan.component';
import { DevelopmentPlanGuardService } from "./development-plan-guard.service";

export const DevelopmentPlanPagesRoutes: Routes = [
    { path: 'view/:id', component: DevelopmentPlanDetailComponent, canActivate: [DevelopmentPlanGuardService] },
    { path: 'Details/:status/:id', component: DevelopmentPlanDetailComponent, canActivate: [DevelopmentPlanGuardService] },    
    { path: 'create/:id', component: CreateDevelopmentPlanComponent, canActivate: [DevelopmentPlanGuardService] },
    { path: 'training/:id', component: TrainingComponent, canActivate: [DevelopmentPlanGuardService] },
    { path: 'assessment/:id', component: AssessmentComponent, canActivate: [DevelopmentPlanGuardService] },
    { path: 'result/:id', component: ResultComponent, canActivate: [DevelopmentPlanGuardService] },
    { path: 'reassessment-create/:userID/:id', component: ReassessmentDevelopmentPlanComponent, canActivate: [DevelopmentPlanGuardService] },
    { path: 'reassessment-training/:id', component: ReassessmentTrainingComponent, canActivate: [DevelopmentPlanGuardService] },
    { path: 'reassessment-assessment/:id', component: ReassessmentNotesComponent, canActivate: [DevelopmentPlanGuardService] },
    { path: 'reassessment-result/:id', component: ReassessmentResultComponent, canActivate: [DevelopmentPlanGuardService] },
    { path: 'edit/:id', component: EditDevelopmentPlanComponent, canActivate: [DevelopmentPlanGuardService] },
];

export const DevelopmentPlanRoutingModule = RouterModule.forChild(DevelopmentPlanPagesRoutes);
