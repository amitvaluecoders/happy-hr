import { Component, OnInit } from '@angular/core';
import { DevelopmentPlanService } from '../development-plan.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { CoreService } from '../../../shared/services/core.service';
import { APPCONFIG } from '../../../config';

@Component({
    selector: 'app-result',
    templateUrl: './result.component.html',
    styleUrls: ['./result.component.scss']
})
export class ResultComponent implements OnInit {
    public progress;

    public isButtonClicked = false;
    public isCreateProcessing = false;
    public isProcessing = false;
    public developmentPlan = {
        result: '',
        managerNote: '',
        assessmentNote: ''
    };
    permission;

    constructor(public developmentPlanService: DevelopmentPlanService,
        public router: Router, public activatedRoute: ActivatedRoute,
        public coreService: CoreService) { }

    ngOnInit() {
        this.getDevelopmentPlanDetails();
        APPCONFIG.heading = "Result";
        this.permission = this.coreService.getNonRoutePermission();
        this.progressBarSteps();
    }

    progressBarSteps() {
        this.progress = {
            progress_percent_value: 100, //progress percent
            total_steps: 4,
            current_step: 4,
            circle_container_width: 25, //circle container width in percent
            steps: [
                // {num:1,data:"Select Role"},
                { num: 1, data: "Create development plan" },
                { num: 2, data: "Training" },
                { num: 3, data: "Assessment" },
                { num: 4, data: "Result", active: true }
            ]
        }
    }

    getDevelopmentPlanDetails() {
        this.isProcessing = true;
        this.developmentPlanService.getDevelopmentPlanDetails(this.activatedRoute.snapshot.params['id']).subscribe(
            d => {
                if (d.code == "200") {
                    this.developmentPlan = d.data;
                    this.isProcessing = false;
                }
                else this.coreService.notify("Unsuccessful", d.message, 0);
            },
            error => this.coreService.notify("Unsuccessful", "Error while getting development details", 0),
            () => { }
        )
    }

    onReAssessment(id) {
        this.router.navigate([`/app/company-admin/development-plan/reassessment-create/${id}`]);
    }

}


