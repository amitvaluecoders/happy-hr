import { Component, OnInit } from '@angular/core';
import { DevelopmentPlanService } from '../development-plan.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { CoreService } from '../../../shared/services/core.service';
import { APPCONFIG } from '../../../config';
import { MdSnackBar, MdDialog, MdDialogRef } from '@angular/material';
import { EditDevelopmentPlanNoteComponent } from './edit-development-plan-note/edit-development-plan-note.component';
import * as moment from 'moment';

@Component({
  selector: 'app-development-plan-detail',
  templateUrl: './development-plan-detail.component.html',
  styleUrls: ['./development-plan-detail.component.scss']
})
export class DevelopmentPlanDetailComponent implements OnInit {

  public progress;
  public temp;
  public isButtonClicked = false;
  public isCreateProcessing = false;
  public isProcessing = false;
  status:any;
  public developmentPlanOld=[];
  public originalPIPToggle=[];
  public developmentPlan = {
    result: '',
    trainingNote: [],
    assessmentNote: [],
    employeeNote: []
  };
  permission;

  constructor(public developmentPlanService: DevelopmentPlanService,
    public router: Router, public activatedRoute: ActivatedRoute,
    public coreService: CoreService, public dialog: MdDialog) { }

  ngOnInit() {
    this.status = this.activatedRoute.snapshot.params['status'];
    this.getDevelopmentPlanDetails();
    APPCONFIG.heading = "Development plan details";
    this.permission = this.coreService.getNonRoutePermission();
  }



  getDevelopmentPlanDetails() {
    this.isProcessing = true;
    this.developmentPlanService.getDevelopmentPlanDetails(this.activatedRoute.snapshot.params['id']).subscribe(
      d => {
        if (d.code == "200") {
          this.developmentPlan = d.data;
          this.developmentPlanOld=JSON.parse(JSON.stringify(d.data.oldDevelopment.reverse()));
          for(let c in this.developmentPlanOld){this.originalPIPToggle[c]=false;}

          this.isProcessing = false;
        }
        else this.coreService.notify("Unsuccessful", d.message, 0);
      },
      error => this.coreService.notify("Unsuccessful", "Error while getting development details", 0),
      () => { }
    )
  }
  formateDate(v){
    return moment(v).isValid()? moment(v).format('DD MMM ,YYYY') : 'Not specified';
  }
  
  formateShortTime(v){
  return moment(v).isValid()? moment(v).format('HH:mm A') : 'Not specified';
  }

  onReAssessment(id) {
    this.router.navigate([`/app/company-admin/development-plan/reassessment-create/${id}`]);
  }

  onEditDevelopmentPlan(id) {
    this.router.navigate([`/app/company-admin/development-plan/edit/${id}`]);
  }

  onEditNotes(note) {
    this.temp = note;
    let dialogRef = this.dialog.open(EditDevelopmentPlanNoteComponent, { width: '600px' });
    dialogRef.componentInstance.noteDetails = JSON.parse(JSON.stringify(note));
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        console.log("hahah")
        this.temp = result;
      }
    });

  }

}