import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DevelopmentPlanDetailComponent } from './development-plan-detail.component';

describe('DevelopmentPlanDetailComponent', () => {
  let component: DevelopmentPlanDetailComponent;
  let fixture: ComponentFixture<DevelopmentPlanDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DevelopmentPlanDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DevelopmentPlanDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
