import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditDevelopmentPlanNoteComponent } from './edit-development-plan-note.component';

describe('EditDevelopmentPlanNoteComponent', () => {
  let component: EditDevelopmentPlanNoteComponent;
  let fixture: ComponentFixture<EditDevelopmentPlanNoteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditDevelopmentPlanNoteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditDevelopmentPlanNoteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
