import { Component, OnInit } from '@angular/core';
import { DevelopmentPlanService } from '../../development-plan.service';
import { CoreService } from '../../../../shared/services/core.service';
import { MdDialogRef } from '@angular/material';

@Component({
  selector: 'app-edit-development-plan-note',
  templateUrl: './edit-development-plan-note.component.html',
  styleUrls: ['./edit-development-plan-note.component.scss']
})
export class EditDevelopmentPlanNoteComponent implements OnInit {
  public noteDetails:any;
  public isProcessing =false;
  public isButtonClicked=false;

  constructor( public developmentPlanService: DevelopmentPlanService,
               public coreService: CoreService,public dialog: MdDialogRef<EditDevelopmentPlanNoteComponent>) { }

  ngOnInit() {
  }

  onUpdate(isValid){
    this.isButtonClicked=true;
    if(isValid){
      this.isProcessing=true;
     this.developmentPlanService.updateDevelopmentPlanNotes(this.noteDetails).subscribe(
          d => {
            this.isProcessing = false;  
              if (d.code == "200"){
                this.coreService.notify("Successful", d.message, 1);
                this.dialog.close(this.noteDetails);
              } 
              else this.coreService.notify("Unsuccessful", d.message, 0);
          },
          error => {
            this.coreService.notify("Unsuccessful", "Error while Updating.", 0), 
            this.isProcessing = false;  
          }
      )
  }
  }

}
