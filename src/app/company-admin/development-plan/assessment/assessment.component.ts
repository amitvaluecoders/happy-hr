import { Component, OnInit } from '@angular/core';
import { DevelopmentPlanService } from '../development-plan.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { CoreService } from '../../../shared/services/core.service';
import { APPCONFIG } from '../../../config';
import { MdSnackBar, MdDialog, MdDialogRef } from '@angular/material';


@Component({
  selector: 'app-assessment',
  templateUrl: './assessment.component.html',
  styleUrls: ['./assessment.component.scss']
})
export class AssessmentComponent implements OnInit {
  public progress;

  public isButtonClicked = false;
  public isCreateProcessing = false;
  public isProcessing = false;
  public developmentPlan = {};
  public developmentPlanManagerNote = {
    developmentPlanID: "",
    assessmentNote: "",
    result: ""

  };
  permission;

  constructor(public developmentPlanService: DevelopmentPlanService,
    public router: Router, public activatedRoute: ActivatedRoute,
    public coreService: CoreService, public dialog: MdDialog) { }

  ngOnInit() {

    APPCONFIG.heading = "Assessment";
    this.permission = this.coreService.getNonRoutePermission();
    this.progressBarSteps();
    this.getDevelopmentPlanDetails();
  }
onTime(time){return new Date(time);}
  progressBarSteps() {
    this.progress = {
      progress_percent_value: 75, //progress percent
      total_steps: 4,
      current_step: 3,
      circle_container_width: 25, //circle container width in percent
      steps: [
        // {num:1,data:"Select Role"},
        { num: 1, data: "Create development plan" },
        { num: 2, data: "Training" },
        { num: 3, data: "Assessment", active: true },
        { num: 4, data: "Result" }
      ]
    }
  }

  getDevelopmentPlanDetails() {
    this.isProcessing = true;
    this.developmentPlanService.getDevelopmentPlanDetails(this.activatedRoute.snapshot.params['id']).subscribe(
      d => {
        if (d.code == "200") {
          this.developmentPlan = d.data;
          this.isProcessing = false;
        }
        else this.coreService.notify("Unsuccessful", d.message, 0);
      },
      error => this.coreService.notify("Unsuccessful", "Error while getting development details", 0),
      () => { }
    )
  }

  onAddTrainingNote(isvalid) {
    this.isButtonClicked = true;
    if (isvalid) {
      let dialogRef = this.dialog.open(DevelopmentResultPopUpComponent);
      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          this.developmentPlanManagerNote['result'] = result;
          this.submitNotesAndResult();
        }
      });

    }
  }

  submitNotesAndResult() {
    this.isCreateProcessing = true;
    this.developmentPlanManagerNote.developmentPlanID = this.developmentPlan['developmentPlanID'];
    this.developmentPlanService.onAssessment(this.developmentPlanManagerNote).subscribe(
      d => {
        this.isCreateProcessing = false;
        if (d.code == "200") {
          this.router.navigate([`/app/company-admin/development-plan/result/${this.developmentPlanManagerNote.developmentPlanID}`]);
          this.isCreateProcessing = false;
        }
        else this.coreService.notify("Unsuccessful", d.message, 0);
      },
      error => {
        this.coreService.notify("Unsuccessful", error.message, 0);
        this.isCreateProcessing = false;
      }

    )
  }


}

@Component({
  selector: 'app-development-plan-result',
  template: `
    <button (click)="onResult('Pass')" class="btn btn-success ">Pass</button>
    <button (click)="onResult('Fail')" class="btn btn-danger ">Fail</button>
    <button (click)="onResult('Re-Assess')" class="btn btn-special">Re-Assessment</button>

  `,
  styles: [`button{
  margin-left:15px;
}`],
})
export class DevelopmentResultPopUpComponent implements OnInit {

  ngOnInit() {

  }
  constructor(public dialogRef: MdDialogRef<DevelopmentResultPopUpComponent>) { }

  onResult(result) {
    if (result) this.dialogRef.close(result);
  }

}
