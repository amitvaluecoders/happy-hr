import { Component, OnInit, AfterViewInit} from '@angular/core';
import { Cookie } from 'ng2-cookies/ng2-cookies';
import { DevelopmentPlanService } from '../development-plan.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { CoreService } from '../../../shared/services/core.service';
import {APPCONFIG } from '../../../config';
import { IDatePickerConfig } from 'ng2-date-picker';
import * as moment from 'moment';


@Component({
  selector: 'app-create-development-plan',
  templateUrl: './create-development-plan.component.html',
  styleUrls: ['./create-development-plan.component.scss']
})
export class CreateDevelopmentPlanComponent implements OnInit, AfterViewInit {

  assessmentTimeConfig: IDatePickerConfig = {};
    public tempSelectedItem={};
      public isFocused=false;
  config: IDatePickerConfig = {};  
  iterableDiffer:any;
  public progress;
  public isCreateProcessing:Boolean;
  public counter = 0;
  trainingTimeFlag =0;
  assessmentTimeFlag=0;
  public isButtonClicked=false;
  public isProcessing;
  dropdownListEmployees = [];
  selectedItems = [];
  dropdownSettings = {};
  thirdPartyList = [];
  public thirdPartyName="";
  public isThirdPartyAlreadyExist=false;
  public isEnterPressed=false;
  selectedParty = [];
  invitedropdownSettings = {};
   public userInfo: any;
   public loggedInUser:any;
   developmentPlan= {
      developmentPlanID:'',
      employeeID: '',
      title: '',
      reason: '',
      actionPlan: '',
      objective: '',
      trainingTime: '',
      assessmentTime: '',
      trainingDate: '',
      assessmentDate: '',
      managerID: '',
      thirdParty: []
  }

    public tempTrainingTime:any;
  public tempAssessmentTime:any;


  constructor(public developmentPlanService: DevelopmentPlanService,
               public router: Router, public activatedRoute: ActivatedRoute,
               public coreService: CoreService) { 
                this.assessmentTimeConfig.locale='en';
                this.assessmentTimeConfig.format='DD MMM YYYY HH:mm:ss';
                this.assessmentTimeConfig.showMultipleYearsNavigation=true;
                this.assessmentTimeConfig.weekDayFormat='dd';
                this.assessmentTimeConfig.disableKeypress=true,
                this.assessmentTimeConfig.monthFormat='MMM YYYY';
                this.assessmentTimeConfig.min=moment().add(+1, 'days');
                this.config.locale = "en";
                this.config.format = "DD MMM YYYY HH:mm:ss ";
                this.config.showMultipleYearsNavigation = true;
                this.config.weekDayFormat = 'dd';
                this.config.disableKeypress = true;
                this.config.monthFormat="MMM YYYY";
                this.config.min=moment();
                   
               }

  ngOnInit() {
      APPCONFIG.heading = 'Create development plan';
      this.getEmployeeList();
      this.getManagerList();
      this.loggedInUser = JSON.parse(Cookie.get('happyhr-userInfo')) || JSON.parse(localStorage.getItem('happyhr-userInfo'));
      console.log("loggedin user",this.loggedInUser)
      this.progressBarSteps();

    this.dropdownSettings = {
          singleSelection: true, 
          text:"Select Name",
          enableSearchFilter: true,
          classes:"myclass assmng"
        }; 
    this.invitedropdownSettings = {
          singleSelection: false, 
          text:"Select Third Party",
          selectAllText:'Select All',
          unSelectAllText:'UnSelect All',
          enableSearchFilter: true,
          classes:"myclass thirdParty"
        }; 

  }
  ngAfterViewInit(){
  
    this.dropdownListEmployees.filter(function(item, i){
        $('.assmng ul li').eq(i).prepend(`<img src="assets/images/${item.img}" alt="" />`);
        $('.assmng ul li').eq(i).children('label').append('<span>' + item.role + '</span>');
    });
    this.dropdownListEmployees.filter(function(item, i){
        $('.thirdParty ul li').eq(i).prepend(`<img src="assets/images/${item.img}" alt="" />`);
        $('.thirdParty ul li').eq(i).children('label').append('<span>' + item.role + '</span>');
    });

  }

  attactImageAndRolesDropDownList(){}

  progressBarSteps() {
      this.progress = {
          progress_percent_value: 25,
          total_steps: 4,
          current_step: 1,
          circle_container_width: 25,
          steps: [
              // {num:1,data:"Select Role"},
               {num: 1, data: 'Create development plan', active: true},
               {num: 2, data: 'Training'},
               {num: 3, data: 'Assessment'},
               {num: 4, data: 'Result'}
          ]  
       }         
  }

 onItemSelect(item: any) {  
          if(!(Object.keys(this.tempSelectedItem).length === 0 && this.tempSelectedItem.constructor === Object)) this.thirdPartyList.unshift(this.tempSelectedItem);
        this.isFocused=false;
       for(let k of this.dropdownListEmployees){
           k['isSelected']=(k.userId==item.userId)?true:false;
       }
       if(item.isSelected){
            this.thirdPartyList = this.thirdPartyList.filter(i =>{ 
                if (item.userId == i.userId) return null;
                else return i;
            }) 
        }
        this.tempSelectedItem=item;
    }

    OnItemDeSelect(item: any) {
        this.isFocused=false;        
        let temp= JSON.parse(JSON.stringify(item));
        temp['isSelected']=false;
        this.thirdPartyList.unshift(temp);
        this.tempSelectedItem={};
    }

      onThirdPartySelect(e){
        e.isSelected=!e.isSelected;      
        if(e.isSelected){
            this.dropdownListEmployees = this.dropdownListEmployees.filter(item =>{
                if (item.userId == e.userId) return null;
                else return item;
            }) 
        }
        else {
            if(e.role){
            let temp= JSON.parse(JSON.stringify(e));
            temp['isSelected']=false;
            this.dropdownListEmployees.push(temp)
            }  
        }
    }
  // assign/reassisgn training time on change.
    ontrainingTimeChange(){
        if(this.trainingTimeFlag >0) {
            this.developmentPlan.trainingTime = moment(this.tempTrainingTime).format("YYYY-MM-DD HH:mm:ss");   
            let copy: IDatePickerConfig = JSON.parse(JSON.stringify(this.assessmentTimeConfig));
            copy.min = moment( this.developmentPlan.trainingTime);
            this.assessmentTimeConfig = copy;
            this.developmentPlan.assessmentTime='';
            this.tempAssessmentTime= null;
        }
        this.trainingTimeFlag++;
    }

// assign/reassisgn assessment time on change.
    onAssessmentTimeChange(){      
        if(this.assessmentTimeFlag >0) this.developmentPlan.assessmentTime = moment(this.tempAssessmentTime).format("YYYY-MM-DD HH:mm:ss");
        this.assessmentTimeFlag++;
        
    }


  getEmployeeList() {
     this.isProcessing = true;
     this.developmentPlanService.getAllEmployee().subscribe(
          d => {
              if (d.code == "200") {
                  let empList=d.data.filter(item =>{
                      if(item.userId==this.activatedRoute.snapshot.params['id']) {
                          this.developmentPlan.employeeID=item.userId;
                          this.userInfo=item;
                          return null;
                        } 
                      
                  })

                  //this.thirdPartyList=JSON.parse(JSON.stringify(this.dropdownListEmployees));
                this.isProcessing = false; 
               
              }
              else this.coreService.notify("Unsuccessful", "Error while getting contract types", 0);
          },
          error => this.coreService.notify("Unsuccessful", "Error while getting contract types", 0),
          () => { }
      )
  }

  getManagerList() {
    this.isProcessing = true;
    this.developmentPlanService.getAllManager(this.activatedRoute.snapshot.params['id']).subscribe(
         d => {
             if (d.code == "200") {
                 this.dropdownListEmployees=d.data.filter(item =>{
                       // if(this.loggedInUser.userID==item.userId)return null;
                     item['id'] = item.userId;
                     item['itemName'] = item.name;
                     item['isSelected'] = false;
                     return item;
                 })

                 this.thirdPartyList=JSON.parse(JSON.stringify(this.dropdownListEmployees));
               this.isProcessing = false; 
              
             }
             else this.coreService.notify("Unsuccessful", "Error while getting contract types", 0);
         },
         error => this.coreService.notify("Unsuccessful", "Error while getting contract types", 0),
         () => { }
     )
 }


  onCreate(isvalid){
      this.isButtonClicked=true;
      if(isvalid){
           this.developmentPlan.managerID = this.selectedItems[0].id;
           for(let i of this.thirdPartyList){
               if(i.isSelected){
                 this.developmentPlan.thirdParty.push({
                     type:(i.external)?"External":"Internal",
                     userID:(!i.external)?i.userId:'',
                     email:(i.external)?i.email:''
                });  
               }
           }
            // let tempTraTime =new Date(this.developmentPlan.trainingTime)
            // let tempAssessmentTime =new Date(this.developmentPlan.trainingTime)
            // this.developmentPlan.trainingDate=tempTraTime.getFullYear()+'-'+(Number(tempTraTime.getMonth())+1)+'-'+tempTraTime.getDate();
            // this.developmentPlan.trainingTime=tempTraTime.toLocaleTimeString();
            // this.developmentPlan.assessmentDate=tempAssessmentTime.getFullYear()+'-'+(Number(tempAssessmentTime.getMonth())+1)+'-'+tempAssessmentTime.getDate();;
            // this.developmentPlan.assessmentTime=tempAssessmentTime.toLocaleTimeString();
          this.createDevelopmentPlan();
        // console.log("dev",this.developmentPlan)
        
      }   
  }

  createDevelopmentPlan(){
       this.isCreateProcessing=true;
        this.developmentPlanService.createDevelopmentPLan(this.developmentPlan).subscribe(
          d => {
              if (d.code == "200") {
                  this.coreService.notify("Successful",d.message, 1);
                 this.router.navigate([`/app/company-admin/listing-page/development-plan-list`]);
              }
              else this.coreService.notify("Unsuccessful",d.message, 0);
               this.isCreateProcessing = false; 
          },
          error => {
              this.coreService.notify("Unsuccessful", error.message, 0);
              this.isCreateProcessing = false; 
          }
          
      )
  }
  
// methods to add external third party 
  onEnterExternalThirdParty(isvalid){
      if(this.thirdPartyName){
      this.isThirdPartyAlreadyExist=false;
      this.isEnterPressed=true;
      if(isvalid){
            for(let c of this.thirdPartyList ){
                if(c.email==this.thirdPartyName)this.isThirdPartyAlreadyExist=true;
            }
            if(!this.isThirdPartyAlreadyExist){
                this.thirdPartyList.splice(0,0,{
                    isSelected:true,
                    name:this.thirdPartyName,
                    email:this.thirdPartyName,
                    external:true,
                    imageUrl:"http://happyhrapi.n1.iworklab.com/public/user-profile-image/user.png"
                })
                this.thirdPartyName='';
                this.isEnterPressed=false;
            } 
      }
      }

  }

   onViewUserDetails(id){
                var newWindow = window.open(`/#/app/company-admin/employee-management/profile/${id}`);   
    }
}
