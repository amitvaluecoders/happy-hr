import { Component, OnInit } from '@angular/core';
import { DevelopmentPlanService } from '../development-plan.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { CoreService } from '../../../shared/services/core.service';
import { APPCONFIG } from '../../../config';


@Component({
  selector: 'app-training',
  templateUrl: './training.component.html',
  styleUrls: ['./training.component.scss']
})
export class TrainingComponent implements OnInit {

  public isButtonClicked = false;
  public progress;
  public isCreateProcessing = false;
  public isProcessing = true;
  public DevelopmentPlan = {};
  public developmentPlanManagerNote = {
    developmentPlanID: "",
    managerNote: "",

  };
  permission;

  constructor(public developmentPlanService: DevelopmentPlanService,
    public router: Router, public activatedRoute: ActivatedRoute,
    public coreService: CoreService) { }

  ngOnInit() {
    APPCONFIG.heading = "Training";
    this.permission = this.coreService.getNonRoutePermission();
    this.progressBarSteps();
    this.getDevelopmentPlanDetails();
  }
  progressBarSteps() {
    this.progress = {
      progress_percent_value: 50, //progress percent
      total_steps: 4,
      current_step: 2,
      circle_container_width: 25, //circle container width in percent
      steps: [
        // {num:1,data:"Select Role"},
        { num: 1, data: "Create development plan" },
        { num: 2, data: "Training", active: true },
        { num: 3, data: "Assessment" },
        { num: 4, data: "Result" }
      ]
    }
  }

  onAddManagerNote(isvalid) {
    this.isButtonClicked = true;
    if (isvalid) {
      console.log("valid hai")
      this.isCreateProcessing = true;
      this.developmentPlanManagerNote.developmentPlanID = this.DevelopmentPlan['developmentPlanID'];
      this.developmentPlanService.onAddManagerNote(this.developmentPlanManagerNote).subscribe(
        d => {
          if (d.code == "200") {
            this.isCreateProcessing = false;
            this.router.navigate([`/app/company-admin/listing-page/development-plan-list`]);
          }
          else this.coreService.notify("Unsuccessful", d.message, 0);
        },
        error => {
          this.coreService.notify("Unsuccessful", error.message, 0);
          this.isCreateProcessing = false;
        }

      )
    }
  }

  getDevelopmentPlanDetails() {
    this.isProcessing = true;
    this.developmentPlanService.getDevelopmentPlanDetails(this.activatedRoute.snapshot.params['id']).subscribe(
      d => {
        if (d.code == "200") {
          this.DevelopmentPlan = d.data;
          this.isProcessing = false;
        }
        else this.coreService.notify("Unsuccessful", d.message, 0);
      },
      error => this.coreService.notify("Unsuccessful", "Error while getting development details", 0),
      () => { }
    )
  }



}
