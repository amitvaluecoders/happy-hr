import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AngularMultiSelectModule } from 'angular2-multiselect-checkbox-dropdown/angular2-multiselect-dropdown';
import { PerformanceImprovementPlanListingComponent } from './performance-improvement-plan-listing/performance-improvement-plan-listing.component';
import { ContractsRoutingModule } from './company-module-listing.routing';
import { DevelopmentPlanListingComponent, developmentPlanReschedulePopup } from './development-plan-listing/development-plan-listing.component';
import { DevelopmentPlanService } from '../development-plan/development-plan.service';
import { PerformanceImprovementPlanService } from '../performance-improvement-plan/performance-improvement-plan.service';
import { BehavioralPerformanceImprovementPlanService } from '../behavioral-performance-improvement-plan/behavioral-performance-improvement-plan.service';
import { DisciplinaryMeetingService } from '../disciplinary-meeting/disciplinary-meeting.service';
import { MaterialModule } from '@angular/material';
import { SharedModule } from '../../shared/shared.module';
import { FormsModule } from '@angular/forms';
import { BehaviorPerformanceImprovementPlanListingComponent } from './behavior-performance-improvement-plan-listing/behavior-performance-improvement-plan-listing.component';
// import { EmployeeListingPopupComponent } from './employee-listing-popup/employee-listing-popup.component';
import { WarningListComponent } from './warning-list/warning-list.component';
import { GrievanceListingComponent } from './grievance-listing/grievance-listing.component';
import { DisciplinaryActionListingComponent } from './disciplinary-action-listing/disciplinary-action-listing.component';
import { PerformanceAppraisalListingComponent } from './performance-appraisal-listing/performance-appraisal-listing.component';
import { InductionOnboardingListingComponent } from './induction-onboarding-listing/induction-onboarding-listing.component';
import { OffBoardingListingComponent } from './off-boarding-listing/off-boarding-listing.component';
import { PerformanceIndicatorListingComponent,EmployeeKPIwithNote } from './performance-indicator-listing/performance-indicator-listing.component';
import { NewEmployeeApprovalComponent } from './new-employee-approval/new-employee-approval.component';
import { AssetsApprovalComponent } from './assets-approval/assets-approval.component';
import { AssetsListingComponent } from './assets-listing/assets-listing.component';
import { ExitSurveyComponent } from './exit-survey/exit-survey.component';
import { ProbationaryMeetingComponent } from './probationary-meeting/probationary-meeting.component';
import { ReScheduleDevelopmentPlanComponent } from './development-plan-listing/re-schedule-development-plan/re-schedule-development-plan.component';
import { NguiDatetimePickerModule } from '@ngui/datetime-picker';
import { DataTableModule } from 'angular2-datatable';
import { GrievanceService } from "../grievance/grievance.service";
import { ReScheduleDisciplinaryMeetingPopupComponent } from './disciplinary-action-listing/re-schedule-disciplinary-meeting-popup/re-schedule-disciplinary-meeting-popup.component';
import { RescheduleResponseComponent } from './disciplinary-action-listing/reschedule-response/reschedule-response.component';
import { OhsListingComponent } from "./ohs-listing/ohs-listing.component";
import { OhService } from "../oh/oh.service";
import { CmpExitSurveyService } from "../exit-survey/cmp-exit-survey.service";
import { PerformanceAppraisalService } from "../performance-appraisal/performance-appraisal.service";

import { CompanyModuleGuardService } from './company-module-guard.service';
import { GrievanceGuardService } from "../grievance/grievance-guard.service";
import { PerformanceImprovementPlanGuardService } from "../performance-improvement-plan/performance-improvement-plan-guard.service";
import { DevelopmentPlanGuardService } from "../development-plan/development-plan-guard.service";
import { BehaviouralPerformanceImprovementPlanGuardService } from "../behavioral-performance-improvement-plan/behavioural-performance-improvement-plan-guard.service";
import { DisciplinaryMeetingGuardService } from "../disciplinary-meeting/disciplinary-meeting-guard.service";
import { CompanyModuleListingService } from "./company-module-listing.service";
import { AssetsAddEditComponent } from './assets-add-edit/assets-add-edit.component';
import { AssetsApprovalAddEditComponent } from './assets-approval-add-edit/assets-approval-add-edit.component';
import { ProbationActionComponent } from './probation-action/probation-action.component';
import { WarningReasonPopupComponent } from './warning-list/warning-reason-popup/warning-reason-popup.component';
import { TrophyCertificatesComponent } from './trophy-certificates/trophy-certificates.component';
import { CertificatePopUpComponent } from './trophy-certificates/certificate-pop-up/certificate-pop-up.component';
import { CreateOhComponent } from './create-oh/create-oh.component';
import { EmployeeOhsService } from "../../company-employee/employee-ohs/employee-ohs.service";
import { CompanyAdminDashboardService} from '../company-admin-dashboard/company-admin-dashboard.service';
import { ContractUpdateListingComponent } from './contract-update-listing/contract-update-listing.component';
import { ContractDetailsComponent } from './contract-update-listing/contract-details/contract-details.component';
//import { ContractEmployeeService } from '../../company-employee/employee-contracts/employee-contracts.service';
import { CoreService } from '../../shared/services/core.service';
@NgModule({
  imports: [
    FormsModule, SharedModule, AngularMultiSelectModule, MaterialModule, ContractsRoutingModule, CommonModule,
    NguiDatetimePickerModule, DataTableModule
  ],
  declarations: [PerformanceImprovementPlanListingComponent, DevelopmentPlanListingComponent, BehaviorPerformanceImprovementPlanListingComponent,
    WarningListComponent, GrievanceListingComponent,EmployeeKPIwithNote,
    DisciplinaryActionListingComponent, PerformanceAppraisalListingComponent, InductionOnboardingListingComponent,
    OffBoardingListingComponent, PerformanceIndicatorListingComponent,
    NewEmployeeApprovalComponent, AssetsApprovalComponent, AssetsListingComponent,
    ExitSurveyComponent, ProbationaryMeetingComponent, developmentPlanReschedulePopup, ReScheduleDevelopmentPlanComponent,
    ReScheduleDisciplinaryMeetingPopupComponent, RescheduleResponseComponent, OhsListingComponent, AssetsAddEditComponent, AssetsApprovalAddEditComponent, ProbationActionComponent, WarningReasonPopupComponent, TrophyCertificatesComponent, CertificatePopUpComponent, CreateOhComponent, ContractUpdateListingComponent, ContractDetailsComponent],

  entryComponents: [CertificatePopUpComponent,RescheduleResponseComponent, developmentPlanReschedulePopup, ReScheduleDevelopmentPlanComponent,
    ReScheduleDisciplinaryMeetingPopupComponent,WarningReasonPopupComponent,EmployeeKPIwithNote],

  providers: [CompanyModuleListingService, DevelopmentPlanService, PerformanceImprovementPlanService, BehavioralPerformanceImprovementPlanService,
    DisciplinaryMeetingService, GrievanceService, OhService, CmpExitSurveyService, PerformanceAppraisalService, CompanyModuleGuardService,

    GrievanceGuardService,CoreService, PerformanceImprovementPlanGuardService, DevelopmentPlanGuardService, BehaviouralPerformanceImprovementPlanGuardService,
    DisciplinaryMeetingGuardService,CompanyAdminDashboardService],
})
export class CompanyModuleListingModule { }
