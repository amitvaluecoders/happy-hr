import { Component, OnInit } from '@angular/core';
import { MdDialogRef, MdDialog, MdSnackBar } from '@angular/material';
import { APPCONFIG } from '../../../config';
import { EmployeeListingPopupComponent } from '../employee-listing-popup/employee-listing-popup.component';
import {  CompanyModuleListingService } from '../company-module-listing.service';
import { CoreService } from '../../../shared/services/core.service';
import { ResponseNoteComponent } from '../../../shared/components/response-note/response-note.component';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-assets-approval',
  templateUrl: './assets-approval.component.html',
  styleUrls: ['./assets-approval.component.scss']
})
export class AssetsApprovalComponent implements OnInit {
  public assestApprovalList = [];
  public isProcessing;
  public filter = { search : "" };
  public toggleFilter: boolean;
  public employmentList=[];

  constructor(public dialog: MdDialog, public companyModuleListingService : CompanyModuleListingService
  ,public coreService: CoreService , public router:Router) { }

  ngOnInit() {
      APPCONFIG.heading = "Assets approval";
       this.toggleFilter = true;
      this.getAssestList();
  } 

   searchList(v){
    this.filter.search = encodeURIComponent(v);
    this.getAssestList();
  }

  changeStatus(assest , type){
  
    this.isProcessing = true;
      let tempResponse={
          url:'change-asset-status',
          note:{
            responseNote:"",
            assetApprovalID:assest.assetApprovalID,
            "status":type,
          },
          method : 'PUT'
    }
      let dialogRef=this.dialog.open(ResponseNoteComponent,{width:'600px'});
      dialogRef.componentInstance.noteDetails=tempResponse;
      dialogRef.afterClosed().subscribe(result => {
        this.isProcessing = false;
        if(result){
        assest.responseNote =   tempResponse.note.responseNote;
        assest.status =   tempResponse.note.status;
          if(type=='Deleted'){
            this.assestApprovalList.splice(this.assestApprovalList.indexOf(assest),1)
          }
        }
      
      });   
  }

  routerchange(asset?){
    if(!asset) this.router.navigate([`/app/company-admin/listing-page/assets-approve/create`]);  
    else {
          this.companyModuleListingService.tempAssetForOperation = asset;
          this.router.navigate([`/app/company-admin/listing-page/assets-approve/edit/${ asset.assetApprovalID }`]);  
    }  

  }

  getAssestList(){
   this.isProcessing = true;
   this.companyModuleListingService.getAssestApprovalList(JSON.stringify(this.filter)).subscribe(
      res => {
        this.isProcessing = false;
        if (res.code == 200) {
          this.assestApprovalList =res.data? res.data.filter(i=>i.isNative):[];
        }
        else return this.coreService.notify("Unsuccessful", res.message, 0);
      },
      err => {
        this.isProcessing = false;
        if (err.code == 400) {
          return this.coreService.notify("Unsuccessful", err.message, 0);
        }
        this.coreService.notify("Unsuccessful", "Error while getting data.", 0)
      }
    );
  }
  onPopUp() {
    let dialogRef = this.dialog.open(EmployeeListingPopupComponent);
        dialogRef.componentInstance.moduleName='trainingPlan';
    dialogRef.afterClosed().subscribe(result => {
      if (result) {

      }
    });
  }

}
