import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AssetsApprovalAddEditComponent } from './assets-approval-add-edit.component';

describe('AssetsApprovalAddEditComponent', () => {
  let component: AssetsApprovalAddEditComponent;
  let fixture: ComponentFixture<AssetsApprovalAddEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AssetsApprovalAddEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssetsApprovalAddEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
