import { Component, OnInit } from '@angular/core';
import { CompanyModuleListingService } from "../company-module-listing.service";
import { CoreService } from '../../../shared/services/core.service';
import { Router, ActivatedRoute } from '@angular/router';
import { APPCONFIG } from '../../../config';

@Component({
  selector: 'app-assets-approval-add-edit',
  templateUrl: './assets-approval-add-edit.component.html',
  styleUrls: ['./assets-approval-add-edit.component.scss']
})
export class AssetsApprovalAddEditComponent implements OnInit {

  public asset = {
      name : '',
      description : '',
      assetApprovalID : '',
      userID : ''
  };
  public isButtonClicked=false;
  public  isProcessing : Boolean;
  public selectedItems = [];
  public whiteSpaceError = false;
  public dropdownSettings = {};
  public dropdownListEmployees = [];

  constructor(public companyService: CompanyModuleListingService,public coreService: CoreService,
  public activatedRoute : ActivatedRoute, public router : Router) { }

  ngOnInit() {
    APPCONFIG.heading = "Create/Edit assets approval";
        this.asset = {
              name : '',
              description : '',
              assetApprovalID : '',
              userID : ''
        };
        this.getEmployeeeList();
      if(this.activatedRoute.snapshot.params['id']){
         if(this.companyService.tempAssetForOperation && this.companyService.tempAssetForOperation.name){
               this.asset.name =  this.companyService.tempAssetForOperation.name;
                this.asset.description =  this.companyService.tempAssetForOperation.description;
                this.asset.assetApprovalID = this.companyService.tempAssetForOperation.assetApprovalID;
                this.asset.userID = this.companyService.tempAssetForOperation.assignedUser.userID;
         } else {
            this.isProcessing = true;
            this.companyService.getAssestApprovalDetail(this.activatedRoute.snapshot.params['id']).subscribe(
                    d => {
                      this.isProcessing = false;
                      if (d.code == "200") {
                       
                        this.asset.name =  d.data.name;
                        this.asset.description = d.data.description;
                        this.asset.assetApprovalID = d.data.assetID;
                        this.asset.userID = d.data.assignedUser;
                        if(this.dropdownListEmployees.length){
                            this.selectedItems = this.dropdownListEmployees.filter(item => item.userId == this.asset.userID)
                        } 
                      }
                      else this.coreService.notify("Unsuccessful", d.message, 0);
                    },
                    error => this.coreService.notify("Unsuccessful", "Error while getting data", 0),
                    () => { }
            );
         }
      }
    
      this.dropdownSettings = {
            singleSelection: true,
            text: "Select employee",
            enableSearchFilter: true,
            classes: "myclass assmng"
      };
  }

    saveUpdateAsset(form){
        if(form.form.valid){
              this.asset.userID = this.selectedItems[0] && this.selectedItems[0].userId ? this.selectedItems[0].userId : this.asset.userID;
              this.isProcessing = true;
              this.companyService.saveUpdateAssetAprrove(this.asset).subscribe(
                      d => {
                         this.isProcessing = false;
                        if (d.code == "200") {
                           this.router.navigate([`/app/company-admin/listing-page/assets-approval`]);  
                          // this.dropdownListEmployees = d.data;
                          this.coreService.notify("Successful", d.message, 1);
                        }
                        else this.coreService.notify("Unsuccessful", d.message, 0);
                      },
                      error => this.coreService.notify("Unsuccessful", "Error while getting data", 0),
                      () => { }
              );
        }     
    }

  
  getEmployeeeList() {
        this.isProcessing = true;
    
    this.companyService.getAllEmployee().subscribe(
      d => {
        this.isProcessing = false;
        if (d.code == "200") {
          // this.dropdownListEmployees = d.data;
          this.dropdownListEmployees = d.data.filter(item => {
            item['id'] = item.userId;
            item['itemName'] = item.name;
            return item;
          })
          if(this.asset.userID){
                 this.selectedItems = this.dropdownListEmployees.filter(item => item.userId == this.asset.userID)
          }
         
        }
        else this.coreService.notify("Unsuccessful", d.message, 0);
      },
      error => this.coreService.notify("Unsuccessful", "Error while getting data", 0),
      () => { }
    )
  }

}
