import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { APPCONFIG } from '../../../config';
import { MdDialogRef, MdDialog, MdSnackBar } from '@angular/material';
import { EmployeeListingPopupComponent } from '../employee-listing-popup/employee-listing-popup.component';
import { OhService } from "../../oh/oh.service";
import { CoreService } from '../../../shared/services/core.service';
import * as moment from 'moment';

@Component({
  selector: 'app-ohs-listing',
  templateUrl: './ohs-listing.component.html',
  styleUrls: ['./ohs-listing.component.scss']
})
export class OhsListingComponent implements OnInit {
  public toggleFilter = true;
  public empSearch = {};
  public ohsList = [];
  public employeeList = [];
  public isProcessing: boolean;
  public isChecked = false;
  public filter = {
    "userID": [], "filter": { "status": { "Pending": false, "Under Investigation": false, 'Resolved': false, 'Unresolved': false } }
  };

  constructor(public dialog: MdDialog, public companyService: OhService, public coreService: CoreService,
    public router: Router) { }

  ngOnInit() {
    APPCONFIG.heading = "OH&S listing";
    this.getOhsList();
    this.getEmployee();
  }

  onPopUp() {
    let dialogRef = this.dialog.open(EmployeeListingPopupComponent);
   dialogRef.componentInstance.moduleName = 'ohs';
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
          
      }
    });
  }

  getEmployee() {
    this.companyService.getEmployee().subscribe(
      res => {
        if (res.code == 200) {
          if (res.data instanceof Array) {
            this.employeeList = res.data;
          }
        }
        else return this.coreService.notify("Unsuccessful", res.message, 0);
      },
      err => {
        if (err.code == 400) {
          return this.coreService.notify("Unsuccessful", err.message, 0);
        }
        this.coreService.notify("Unsuccessful", "Error while getting data.", 0)
      }
    );
  }

  getOhsList() {
    this.isProcessing = true;
    console.log(JSON.stringify(this.filter))
    this.companyService.getOhsList(JSON.stringify(this.filter)).subscribe(
      res => {
        this.isProcessing = false;
        if (res.code == 200) {
          this.ohsList = res.data;
          this.toggleFilter = true;
        }
        else return this.coreService.notify("Unsuccessful", res.message, 0);
      },
      err => {
        this.isProcessing = false;
        if (err.code == 400) {
          return this.coreService.notify("Unsuccessful", err.message, 0);
        }
        this.coreService.notify("Unsuccessful", "Error while getting data.", 0)
      }
    );
  }

  formatDateTime(date) {
    return moment(date).isValid() ? moment(date).format('YYYY-MM-DD hh:mm A') : 'Not specified';
  }

  onCheckEmp(event, id) {
    this.isChecked = true;
    let ind = this.filter.userID.indexOf(id);

    if (event.target.checked) {
      (ind >= 0) ? null : this.filter.userID.push(id);
    } else {
      (ind >= 0) ? this.filter.userID.splice(ind, 1) : null;
    }
  }

  investigate(ohs) {
    if (ohs.status == 'Under Investigation') {
      if (!ohs.injuryDetail) {
        return this.router.navigate(['/app/company-admin/ohs/company-admin-notified', ohs.ohsID]);
      } else if (!ohs.correctiveAction.length) {
        return this.router.navigate(['/app/company-admin/ohs/corrective-actions', ohs.ohsID]);
      } else {
        return this.router.navigate(['/app/company-admin/ohs/repair-assessment', ohs.ohsID]);
      }
    } else {
      return this.router.navigate(['/app/company-admin/ohs/result', ohs.ohsID]);
    }
  }

  downloadPDF(ohs) {
    this.companyService.downloadPdf(ohs.ohsID);
  }

}
