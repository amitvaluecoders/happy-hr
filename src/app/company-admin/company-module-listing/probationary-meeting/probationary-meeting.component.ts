import { Component, OnInit,ViewContainerRef } from '@angular/core';
import { Router } from "@angular/router";
import { APPCONFIG } from '../../../config';
import { MdDialogRef, MdDialog, MdSnackBar } from '@angular/material';
import { EmployeeListingPopupComponent } from '../employee-listing-popup/employee-listing-popup.component';
import { OhService } from "../../oh/oh.service";
import { CoreService } from '../../../shared/services/core.service';
import * as moment from 'moment';
import { ActionsOnEmployeePopupComponent } from '../../../shared/components/actions-on-employee-popup/actions-on-employee-popup.component';
import { ConfirmService } from '../../../shared/services/confirm.service';

@Component({
  selector: 'app-probationary-meeting',
  templateUrl: './probationary-meeting.component.html',
  styleUrls: ['./probationary-meeting.component.scss']
})
export class ProbationaryMeetingComponent implements OnInit {

  public probationList=[];
  public isProcessing: boolean;

  constructor(public confirmService:ConfirmService,public viewContainerRef:ViewContainerRef,public dialog: MdDialog, public companyService: OhService, public coreService: CoreService,
    public router: Router) { }

  ngOnInit() {
    APPCONFIG.heading = "Probationary meeting listing";
    this.getProbationaryListing();
  }

  //GET PROBATIONARY LISTING
  getProbationaryListing() {
    this.isProcessing = true;
    this.companyService.getProbationaryList().subscribe(
      res => {
        this.isProcessing = false;
        if (res.code == 200) {
          if (res.data instanceof Array) {
            this.probationList = res.data.filter(item=>{
              item['isActionProcessing']= false;
              return item;
            });
            
          }
        }
        else return this.coreService.notify("Unsuccessful", res.message, 0);
      },
      err => {
        this.isProcessing = false;
        if (err.code == 400) {
          return this.coreService.notify("Unsuccessful", err.message, 0);
        }
        this.coreService.notify("Unsuccessful", "Error while getting data.", 0)
      }
    );
  }

  //SAVE PROBATION ACTION
  saveProbationAction(probId,action){
    console.log(probId);
    console.log(action)
    this.isProcessing = true;
    if(action == "Pass" || action == "reject" || action == "delete") {
      var data={
        probationaryMeetingID:probId,
        result:"Pass"
      };
    }
    else{
      var data={
        probationaryMeetingID:probId,
        result:"Termination"
      };
    }
    
    this.companyService.probationAction(data)
    .subscribe(
      res => {
        this.isProcessing = false;
        if (res.code == 200) {
          this.coreService.notify("Success", res.message, 1)
          this.getProbationaryListing();
        }
        else return this.coreService.notify("Unsuccessful", res.message, 0);
      },
      err => {
        this.isProcessing = false;
        if (err.code == 400) {
          return this.coreService.notify("Unsuccessful", err.message, 0);
        }
        this.coreService.notify("Unsuccessful", "Problem while performing action.", 0)
      }
    );
  }

  deleteProbationaryMeating(p){
    this.confirmService.confirm(this.confirmService.tilte, this.confirmService.message, this.viewContainerRef)
    .subscribe(res => {
      let result = res;
      if (result) {
        p['isActionProcessing'] = true;
        this.companyService.deleteProbationMeeting(p.probationaryMeetingID)
        .subscribe(
          res => {
            if (res.code == 200) {
              this.probationList = this.probationList.filter(item=> p.probationaryMeetingID != item.probationaryMeetingID );
              this.coreService.notify("Success", res.message, 1);
              p['isActionProcessing'] = false;
            }
            else return this.coreService.notify("Unsuccessful", res.message, 0);
            
          },
          err => {
            p['isActionProcessing'] = false;
            if (err.code == 400) {
              return this.coreService.notify("Unsuccessful", err.message, 0);
            }
            this.coreService.notify("Unsuccessful", "Problem while performing action.", 0)
          }
        );
      }
    })
   
  }

  cancelProbationaryMeating(p){
    p['isActionProcessing'] = true;
    this.companyService.cancelProbationMeeting({probationaryMeetingID:p.probationaryMeetingID})
    .subscribe(
      res => {
        if (res.code == 200) {
          this.coreService.notify("Success", res.message, 1);
          this.probationList = this.probationList.filter(item=> {
            if(p.probationaryMeetingID == item.probationaryMeetingID) item['status']= "Cancelled";
            return item;
          } );
          p['isActionProcessing'] = false;
        }
        else return this.coreService.notify("Unsuccessful", res.message, 0);
      },
      err => {
        p['isActionProcessing'] = false;
        if (err.code == 400) {
          return this.coreService.notify("Unsuccessful", err.message, 0);
        }
        this.coreService.notify("Unsuccessful", "Problem while performing action.", 0)
      }
    );
    
  }


  //POPUP
  onPopUp() {
    let dialogRef = this.dialog.open(EmployeeListingPopupComponent);
        dialogRef.componentInstance.moduleName='createProbation';
    dialogRef.afterClosed().subscribe(result => {
      if (result) {

      }
    });
  }

  //TERMINATION
  onTerminate(probId,action,userId){
    let info = { probationaryMeetingID: probId, type: 'Terminate', userID: userId, subject: "", body: '' }
      let dialogRef = this.dialog.open(ActionsOnEmployeePopupComponent);
      dialogRef.componentInstance.actionInfo = info;
      dialogRef.afterClosed().subscribe(r => {
        if (r) {
          this.saveProbationAction(probId,action);
        }
      })
  }

  //ACTION
  OnAction(p){
    console.log(p)
    this.router.navigate(['/app/company-admin/listing-page/probationary-meeting/action',p.status,p.probationaryMeetingID]);
  }


}
