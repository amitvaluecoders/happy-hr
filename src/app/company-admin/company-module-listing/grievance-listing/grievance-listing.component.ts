import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { APPCONFIG } from '../../../config';
import { MdDialogRef, MdDialog, MdSnackBar } from '@angular/material';
import { EmployeeListingPopupComponent } from '../employee-listing-popup/employee-listing-popup.component';
import { GrievanceService } from "../../grievance/grievance.service";
import { CoreService } from '../../../shared/services/core.service';
import * as moment from 'moment';

@Component({
  selector: 'app-grievance-listing',
  templateUrl: './grievance-listing.component.html',
  styleUrls: ['./grievance-listing.component.scss']
})
export class GrievanceListingComponent implements OnInit {
  public toggleFilter = false;
  public empSearch = {};
  public grievanceList = [];

  public employeeList = [];
  public managerList = [];
  public isProcessing: boolean;
  public isChecked = false;
  public filter = {
    "raisedBy": [], "managers": [], "raisedAgainst": [],
    "filter": { "status": { "Under Investigation": false, 'Pending': false, 'Resolved': false, 'Unresolved': false } }
  };

  constructor(public dialog: MdDialog, public companyService: GrievanceService, public coreService: CoreService,
    public router: Router) { }

  ngOnInit() {
    APPCONFIG.heading = "Grievance listing";
    this.getGrievanceList();
    this.getEmployee();
    this.getManagers();
  }

  onPopUp() {
    // let dialogRef = this.dialog.open(EmployeeListingPopupComponent);
    // dialogRef.componentInstance.moduleName = 'trainingPlan';
    // dialogRef.afterClosed().subscribe(result => {
    //   if (result) {

    //   }
    // });
  }

  getEmployee() {
    this.companyService.getEmployee().subscribe(
      res => {
        if (res.code == 200) {
          if (res.data instanceof Array) {
            this.employeeList = res.data;
          }
        }
        else return this.coreService.notify("Unsuccessful", res.message, 0);
      },
      err => {
        if (err.code == 400) {
          return this.coreService.notify("Unsuccessful", err.message, 0);
        }
        this.coreService.notify("Unsuccessful", "Error while getting data.", 0)
      }
    );
  }

  getManagers() {
    this.companyService.getManagers().subscribe(
      res => {
        if (res.code == 200) {
          if (res.data instanceof Array) {
            this.managerList = res.data;
          }
        }
        else return this.coreService.notify("Unsuccessful", res.message, 0);
      },
      err => {
        if (err.code == 400) {
          return this.coreService.notify("Unsuccessful", err.message, 0);
        }
        this.coreService.notify("Unsuccessful", "Error while getting data.", 0)
      }
    );
  }

  getGrievanceList() {
    this.isProcessing = true;
    console.log(JSON.stringify(this.filter))
    this.companyService.getGrievanceList(JSON.stringify(this.filter)).subscribe(
      res => {
        this.isProcessing = false;
        if (res.code == 200) {
          this.grievanceList = res.data;
        }
        else return this.coreService.notify("Unsuccessful", res.message, 0);
      },
      err => {
        this.isProcessing = false;
        if (err.code == 400) {
          return this.coreService.notify("Unsuccessful", err.message, 0);
        }
        this.coreService.notify("Unsuccessful", "Error while getting data.", 0)
      }
    );
  }

  formatDateTime(date) {
    return moment(date).isValid() ? moment(date).format('DD MMM, YYYY hh:mm A') : 'Not specified';
  }

  onCheckEmp(event, type, id) {
    this.isChecked = true;
    let ind = this.filter[type].indexOf(id);

    if (event.target.checked) {
      (ind >= 0) ? null : this.filter[type].push(id);
    } else {
      (ind >= 0) ? this.filter[type].splice(ind, 1) : null;
    }
  }

  investigate(grievance) {
    switch (grievance.stage) {
      case '': this.router.navigate(['/app/company-admin/grievance/meeting-complainer', grievance.grievanceID]);
        break;
      case '1': this.router.navigate(['/app/company-admin/grievance/meeting-grievance-against', grievance.grievanceID]);
        break;
      case '2': this.router.navigate(['/app/company-admin/grievance/meeting-both-parties', grievance.grievanceID]);
        break;
      case '3': this.router.navigate(['/app/company-admin/grievance/result', grievance.grievanceID]);
        break;
    }
  }

}
