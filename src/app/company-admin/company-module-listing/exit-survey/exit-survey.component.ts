import { Component, OnInit,ViewContainerRef} from '@angular/core';
import { APPCONFIG } from '../../../config';
import { EmployeeListingPopupComponent } from '../employee-listing-popup/employee-listing-popup.component';
import { CmpExitSurveyService } from "../../exit-survey/cmp-exit-survey.service";
import { CoreService } from '../../../shared/services/core.service';
import { ConfirmService } from '../../../shared/services/confirm.service';

import * as moment from 'moment';

@Component({
  selector: 'app-exit-survey',
  templateUrl: './exit-survey.component.html',
  styleUrls: ['./exit-survey.component.scss']
})
export class ExitSurveyComponent implements OnInit {
  public toggleFilter = true;
0.
  public empSearch = {};
  public exitSurveyList = [];
  public employeeList = [];
  public isProcessing: boolean;
  public isChecked = false;
  public filter = { "search": '', "filter": { "userID": [], "status": { "Pending": false, "Done": false } } };

  constructor(public viewContainerRef: ViewContainerRef, public confirmService: ConfirmService,public companyService: CmpExitSurveyService, public coreService: CoreService) { }

  ngOnInit() {
    APPCONFIG.heading = "Exit survey listing";
    this.getEmployee();
    this.getExitSurveyList();
  }

  getEmployee() {
    this.companyService.getEmployee().subscribe(
      res => {
        if (res.code == 200) {
          if (res.data instanceof Array) {
            this.employeeList = res.data;
          }
        }
        else return this.coreService.notify("Unsuccessful", res.message, 0);
      },
      err => {
        if (err.code == 400) {
          return this.coreService.notify("Unsuccessful", err.message, 0);
        }
        this.coreService.notify("Unsuccessful", "Error while getting data.", 0)
      }
    );
  }

  getExitSurveyList() {
    console.log(JSON.stringify(this.filter))
    let params = { "search": encodeURIComponent(this.filter.search), "filter": this.filter.filter }
    this.isProcessing = true;
    this.companyService.getExitSurveyList(encodeURIComponent(JSON.stringify(params))).subscribe(
      res => {
        this.isProcessing = false;
        if (res.code == 200) {
          this.exitSurveyList = res.data;
        }
        else return this.coreService.notify("Unsuccessful", res.message, 0);
      },
      err => {
        this.isProcessing = false;
        if (err.code == 400) {
          return this.coreService.notify("Unsuccessful", err.message, 0);
        }
        this.coreService.notify("Unsuccessful", "Error while getting data.", 0)
      }
    );
  }

  formatDateTime(date) {
    return moment(date).isValid() ? moment(date).format('DD MMM YYYY hh:mm A') : 'Not specified';
  }

  onDelete(survey){
       this.confirmService.confirm(this.confirmService.tilte, this.confirmService.message, this.viewContainerRef)
          .subscribe(res => {
            let result = res;
            if (result) {
              survey['isProcessing']= true;
                     this.companyService.deleteExitSurveyList(survey.employeeExitSurveyID).subscribe(
                      res => {
                        if (res.code == 200) {
                          this.exitSurveyList = this.exitSurveyList.filter(i=>i.employeeExitSurveyID!=survey.employeeExitSurveyID);
                           survey['isProcessing']= false;
                           this.coreService.notify("Successful", res.message, 1);
                        }
                        else return this.coreService.notify("Unsuccessful", res.message, 0);
                        survey['isProcessing']= false;
                        
                      },
                      err => {
                        this.isProcessing = false;
                        if (err.code == 400) {
                          return this.coreService.notify("Unsuccessful", err.message, 0);
                        }
                        this.coreService.notify("Unsuccessful", "Error while getting data.", 0)
                      }
                    );
            }
      })
  }

  onCheckEmp(event, id) {
    this.isChecked = true;
    let ind = this.filter.filter.userID.indexOf(id);

    if (event.target.checked) {
      (ind >= 0) ? null : this.filter.filter.userID.push(id);
    } else {
      (ind >= 0) ? this.filter.filter.userID.splice(ind, 1) : null;
    }
  }
}
