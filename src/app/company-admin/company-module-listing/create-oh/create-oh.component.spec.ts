import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateOhComponent } from './create-oh.component';

describe('CreateOhComponent', () => {
  let component: CreateOhComponent;
  let fixture: ComponentFixture<CreateOhComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateOhComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateOhComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
