import { Component, OnInit } from '@angular/core';
import { Router,ActivatedRoute } from '@angular/router';
import { IDatePickerConfig } from 'ng2-date-picker';
import { APPCONFIG } from '../../../config';
import { CoreService } from "../../../shared/services/core.service";
import {CompanyModuleListingService} from "../../company-module-listing/company-module-listing.service";
import { Ohs } from "../../../company-employee/employee-ohs/employee-ohs";
import * as moment from 'moment';

@Component({
  selector: 'app-create-oh',
  templateUrl: './create-oh.component.html',
  styleUrls: ['./create-oh.component.scss']
})
export class CreateOhComponent implements OnInit {
  role:string;
    config: IDatePickerConfig = {};
    employeesList = [];
    selectedWitness = [];
    selectedInvolved = [];
    dropdownSettings = {};
    isUploading: boolean;
    isProcessing: boolean;
    _validFileExtensions = [".jpg", ".jpeg", ".png", ".pdf", ".doc", ".docx"];
    incidentTime;
    ohs = new Ohs;
    userId:any;

  constructor(public CompanyService: CompanyModuleListingService, public coreService: CoreService, public router: Router, private route: ActivatedRoute) { 
    this.config.locale = "en";
    this.config.format = "DD MMM, YYYY hh:mm A";
    this.config.showMultipleYearsNavigation = true;
    this.config.weekDayFormat = 'dd';
    this.config.disableKeypress = true;
    this.config.max = moment();
  }

  ngOnInit() {
    
    APPCONFIG.heading = "Report incident";
    this.role = this.coreService.getRole(); 
    this.userId = this.route.snapshot.params['id'];
    this.getEmployeeList();
    this.dropdownSettings = {
        singleSelection: false,
        text: "Select user",
        enableSearchFilter: true,
        classes: "myclass"
  }
  $('angular2-multiselect:eq(0) .list-area ul').addClass('inviteListItems');
  $('angular2-multiselect:eq(1) .list-area ul').addClass('inviteListItems2');

}
getEmployeeList() {
  this.CompanyService.getEmployees().subscribe(
      res => {
          if (res.code == 200) {
              this.employeesList = res.data;
              this.employeesList.filter((item, i) => { item['itemName'] = item.name; item['id'] = item.userId; });
              setTimeout(() => {
                  this.employeesList.filter(function (item, i) {
                      $('.inviteListItems li').eq(i).prepend(`<img src="${item.imageUrl}" alt= />`);
                      $('.inviteListItems2 li').eq(i).prepend(`<img src="${item.imageUrl}" alt= />`);
                      $('.inviteListItems li').eq(i).children('label').append('<span>' + item.designation + '</span>');
                      $('.inviteListItems2 li').eq(i).children('label').append('<span>' + item.designation + '</span>');
                  });
              }, 0);
          }
          else return this.coreService.notify("Unsuccessful", res.message, 0);
      },
      err => {
          if (err.code == 400) {
              return this.coreService.notify("Unsuccessful", err.message, 0);
          }
          this.coreService.notify("Unsuccessful", "Error while getting employee.", 0);
      }
  )
}

uploadDocument($event) {
  const files = $event.target.files || $event.srcElement.files;

  const formData = new FormData();
  for (let i = 0; i < files.length; i++) {
      formData.append('ohsdocument[]', files[i]);
      let ext = files[i].name.substring(files[i].name.lastIndexOf('.')).toLowerCase();
      if (this._validFileExtensions.indexOf(ext) == -1) {
          return this.coreService.notify("Unsuccessful", "Invalid file extension.", 0)
      }
  }

  this.isUploading = true;
  this.CompanyService.uplodDocument(formData).subscribe(
      res => {
          this.isUploading = false;
          if (res.code == 200) {
              this.ohs.document = res.data.ohsdocument;
          }
          else return this.coreService.notify("Unsuccessful", res.message, 0);
      },
      err => {
          this.isUploading = false;
          if (err.code == 400) {
              return this.coreService.notify("Unsuccessful", err.message, 0);
          }
          this.coreService.notify("Unsuccessful", "Error while uploading document.", 0);
      }
  );
}

reportIncident() {
  this.isProcessing = true;
  this.ohs.witness = this.selectedWitness.map(i => { return i.id });
  this.ohs.personInvolved = this.selectedInvolved.map(i => { return i.id });
  this.ohs['userId']= this.userId;
  this.ohs.incidentTime = this.incidentTime && moment(this.incidentTime).isValid() ? moment(this.incidentTime).format('YYYY-MM-DD HH:mm:ss') : '';

  this.CompanyService.reportIncident(this.ohs).subscribe(
      res => {
          this.isProcessing = false;
          if (res.code == 200) {
              this.coreService.notify("Successful", res.message, 1);
              this.router.navigate([`/app/company-admin/listing-page/ohs-listing`]);  
          }
          else return this.coreService.notify("Unsuccessful", res.message, 0);
      },
      err => {
          this.isProcessing = false;
          if (err.code == 400) {
              return this.coreService.notify("Unsuccessful", err.message, 0);
          }
          this.coreService.notify("Unsuccessful", "Error while submitting report.", 0);
      }
  )
}
 
}
