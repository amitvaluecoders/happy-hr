import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RescheduleResponseComponent } from './reschedule-response.component';

describe('RescheduleResponseComponent', () => {
  let component: RescheduleResponseComponent;
  let fixture: ComponentFixture<RescheduleResponseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RescheduleResponseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RescheduleResponseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
