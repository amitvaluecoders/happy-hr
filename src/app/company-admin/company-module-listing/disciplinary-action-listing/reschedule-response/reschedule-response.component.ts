import { Component, OnInit } from '@angular/core';
import { CoreService } from '../../../../shared/services/core.service';
import * as moment from 'moment';
import { IDatePickerConfig } from 'ng2-date-picker';
import {RestfulLaravelService} from '../../../../shared/services/restful-laravel.service';
import { MdDialogRef, MdDialog, MdSnackBar } from '@angular/material';

@Component({
  selector: 'app-reschedule-response',
  templateUrl: './reschedule-response.component.html',
  styleUrls: ['./reschedule-response.component.scss']
})
export class RescheduleResponseComponent implements OnInit {

public disciplinaryActionID:any;
 isProcessing=false;
 isCreateProcessing=false;
 response={
   action:''
 };
  constructor(public restfulService:RestfulLaravelService,public coreService:CoreService,
              public dialogRef:MdDialogRef<RescheduleResponseComponent>) { }

  ngOnInit() {
    this.getRescheduleList();
  }

  getRescheduleList(){
     console.log("sadasdasdasdasd",this.disciplinaryActionID)
    this.isProcessing=true;
    this.restfulService.get(`reschedule-disciplinary-action-list/${this.disciplinaryActionID}`).subscribe(
      d=>{
        if(d.code=='200'){
            this.response=d.data[0];
            // this.response['action']='';
             this.isProcessing=false;          
            // this.dialogRef.close(this.rescheduleDetails.proposedTime);
        }
      },
       error => this.coreService.notify("Unsuccessful", error.message, 0),
          () => {this.isProcessing=false; }
      )
  }

  onResponse(res){
    let reqBody={
      disciplinaryActionID:this.response['disciplinaryActionID'],
      rescheduleRequestID:this.response['rescheduleRequestID'],
      action:res
    }
    this.isCreateProcessing=true;
    this.restfulService.post(`reschedule-disciplinary-accept-reject`,reqBody).subscribe(
      d=>{
        if(d.code=='200'){
            this.isCreateProcessing=false;
            this.coreService.notify("Successful", d.message, 1);
            var r={
              response:res,
              time:this.response['proposedTime']
            }
            this.dialogRef.close(r);
        }
      },
       error => this.coreService.notify("Unsuccessful", error.message, 0),
          () => {this.isCreateProcessing=false; }
      )

  }

}
