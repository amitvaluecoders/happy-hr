import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReScheduleDisciplinaryMeetingPopupComponent } from './re-schedule-disciplinary-meeting-popup.component';

describe('ReScheduleDisciplinaryMeetingPopupComponent', () => {
  let component: ReScheduleDisciplinaryMeetingPopupComponent;
  let fixture: ComponentFixture<ReScheduleDisciplinaryMeetingPopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReScheduleDisciplinaryMeetingPopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReScheduleDisciplinaryMeetingPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
