import { Component, OnInit } from '@angular/core';
import { CoreService } from '../../../../shared/services/core.service';
import * as moment from 'moment';
import { IDatePickerConfig } from 'ng2-date-picker';
import {RestfulLaravelService} from '../../../../shared/services/restful-laravel.service';
import { MdDialogRef, MdDialog, MdSnackBar } from '@angular/material';

@Component({
  selector: 'app-re-schedule-disciplinary-meeting-popup',
  templateUrl: './re-schedule-disciplinary-meeting-popup.component.html',
  styleUrls: ['./re-schedule-disciplinary-meeting-popup.component.scss']
})
export class ReScheduleDisciplinaryMeetingPopupComponent implements OnInit {

public actionPlanDetails:any;
 
  public rescheduleDetails:any;
  public config: IDatePickerConfig = {};
  public tempDateTime:any; 
  public isProcessing=false;
  public isButtonClicked=false;

  constructor(public restfulService:RestfulLaravelService,public coreService:CoreService,
              public dialogRef:MdDialogRef<ReScheduleDisciplinaryMeetingPopupComponent>) { }

  ngOnInit() {
        this.config.locale = "en";
        this.config.format = "DD MMM YYYY hh:mm A";
        this.config.showMultipleYearsNavigation = true;
        this.config.weekDayFormat = 'dd';
        this.config.min=moment().add(+1, 'days');
        this.config.disableKeypress = true;
        this.config.monthFormat="MMM YYYY";
    this.tempDateTime=moment(this.rescheduleDetails.proposedTime).format("DD MMM YYYY hh:mm:ss"); 
  }

  onSave(){
    this.isProcessing=true;
    this.isButtonClicked=true;
    this.rescheduleDetails.proposedTime=moment(this.tempDateTime).format("YYYY-MM-DD hh:mm:ss"); 
    this.restfulService.post(this.rescheduleDetails.url,this.rescheduleDetails).subscribe(
      d=>{
        if(d.code=='200'){
    this.isProcessing=false;
         
            this.coreService.notify("Successful", d.message, 1);
            this.dialogRef.close(this.rescheduleDetails.proposedTime);
        }
      },
       error => this.coreService.notify("Unsuccessful", error.message, 0),
          () => {this.isProcessing=false; }
      )
  }
}
