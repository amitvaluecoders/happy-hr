import { Component, OnInit,ViewContainerRef } from '@angular/core';
import { MdDialogRef, MdDialog, MdSnackBar } from '@angular/material';
import { APPCONFIG } from '../../../config';
import { EmployeeListingPopupComponent } from '../employee-listing-popup/employee-listing-popup.component';
import { DisciplinaryMeetingService } from '../../disciplinary-meeting/disciplinary-meeting.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { CoreService } from '../../../shared/services/core.service';
import { ConfirmService } from '../../../shared/services/confirm.service';
import { ReScheduleDisciplinaryMeetingPopupComponent } from './re-schedule-disciplinary-meeting-popup/re-schedule-disciplinary-meeting-popup.component';
import { RescheduleResponseComponent } from './reschedule-response/reschedule-response.component';
import * as moment from 'moment';

@Component({
  selector: 'app-disciplinary-action-listing',
  templateUrl: './disciplinary-action-listing.component.html',
  styleUrls: ['./disciplinary-action-listing.component.scss']
})
export class DisciplinaryActionListingComponent implements OnInit {

public toggleFilter:boolean;
  public employmentList;
  public positionList;
  public isProcessing=false;
  public isFilterDataProcessing=false;
  public enableFilter=false;
  public plans=[]; 
  public unsubscriber:any;
  public searchKey=''; 
  public filter={
    status: [],
    manager: [],
    employee: [],
  }
  public searchKeyStatus='';
  public searchKeyManager='';
  public searchKeyEmployee='';



public reqBody = {
  search:"",
  filter:{
    employeeIDs:[],
    managerIDs:[],
    status:{}
  }
}


  constructor( public disciplinaryMeetingService: DisciplinaryMeetingService,public confirmService:ConfirmService,
               public router: Router, public activatedRoute: ActivatedRoute,public viewContainerRef:ViewContainerRef,
               public coreService: CoreService,public dialog: MdDialog) { }


  ngOnInit() {
    APPCONFIG.heading = "Disciplinary action listing";
    this.toggleFilter = true;
    this.getDataForFilters();
    this.getPlans();

  }


  formateDate(v){
    return moment(v).calendar()? moment(v).format('DD MMM YYYY hh:mm A') : 'Not specified';
}

  // method to get all filter data.
  getDataForFilters(){
     this.isFilterDataProcessing = true;
     this.disciplinaryMeetingService.getDataForFilters().subscribe(
          d => {
              if (d.code == "200") {
                this.filter=d.data; 
                this.isFilterDataProcessing = false;    
              }
              else this.coreService.notify("Unsuccessful", d.message, 0);
          },
          error => this.coreService.notify("Unsuccessful", "Error while getting development details", 0),
          () => {this.isFilterDataProcessing = false;}
      )
  }

// method to set filter data for getting filtered list. 
  onApplyFilters(){
        this.reqBody.filter.status={};
        this.reqBody.filter.managerIDs=[];
        this.reqBody.filter.employeeIDs=[];
        for(let i of this.filter.status){
          if(i.isChecked)this.reqBody.filter.status[i.title]=true;
        } 
        for(let i of this.filter.manager){
          if(i.isChecked)this.reqBody.filter.managerIDs.push(i.userID);
        } 
        for(let i of this.filter.employee){
          if(i.isChecked)this.reqBody.filter.employeeIDs.push(i.userID);
        } 
        this.getPlans();
  }

   // method to get list by serch key.
    onSearchKeyEnter(){
      this.unsubscriber.unsubscribe();
    this.reqBody.search=encodeURIComponent(this.searchKey);
        this.getPlans();    
  }


   onPopUp() {
    let dialogRef = this.dialog.open(EmployeeListingPopupComponent);
        dialogRef.componentInstance.moduleName='disciplinaryAction';
    dialogRef.afterClosed().subscribe(result => {
      if (result) {

      }
    });
  }


//method to get list of PIP plans
    getPlans() {
     this.isProcessing = true;
     this.unsubscriber=this.disciplinaryMeetingService.getAllDisciplinaryActions(JSON.stringify(this.reqBody)).subscribe(
          d => {
              if (d.code == "200") {
                this.plans=d.data; 
                for(let i of this.plans){
                  i['isDeleteProcessing']=false;
                }
                this.isProcessing = false;    
              }
              else this.coreService.notify("Unsuccessful", d.message, 0);
          },
          error => this.coreService.notify("Unsuccessful", error.message, 0),
          () => { }
      )
  }

  OnAction(d) {
      this.coreService.setPIP(d);
         this.router.navigate([`/app/company-admin/disciplinary-meeting/view/${d.disciplinaryActionID}`]);
  }

  onView(d){
    // this.coreService.setPIP(d);
      this.router.navigate([`/app/company-admin/disciplinary-meeting/view/${d.disciplinaryActionID}`]);
  }

  OnDelete(data){
      this.confirmService.confirm(this.confirmService.tilte, this.confirmService.message, this.viewContainerRef)
      .subscribe(res => {
        let result = res;
        if (result) {
                this.unsubscriber=this.disciplinaryMeetingService.deleteDisciplinaryAction(data.disciplinaryActionID).subscribe(
                d => {
                    if (d.code == "200") {
                      this.coreService.notify("Successful", d.message, 1);
                      this.plans=this.plans.filter(function(item){
                        if(item.disciplinaryActionID==data.disciplinaryActionID)return null; 
                        else return item;
                      })
                    }
                    else this.coreService.notify("Unsuccessful", d.message, 0);
                },
                error => this.coreService.notify("Unsuccessful", error.message, 0),
                () => { data.isDeleteProcessing=false;}
            )
        }
        else data.isDeleteProcessing=false;
  })
}

  OnReschedule(data){
       let dialogRef = this.dialog.open(ReScheduleDisciplinaryMeetingPopupComponent,{width:'700px',height:'400px'});
       let reqBody = {
     url:'reschedule-disciplinary-meeting-manager',
    disciplinaryActionID:data.disciplinaryActionID,
    proposedTime:data.meetingTime
   }
        dialogRef.componentInstance.rescheduleDetails=reqBody;
    dialogRef.afterClosed().subscribe(result => {
      
      if(result) {
       data.meetingTime=result;
      }  
       data.status='Awaiting Employee Acceptance' ; 
    });
  }

OnRescheduleAcceptReject(data){
     let dialogRef = this.dialog.open(RescheduleResponseComponent);
     dialogRef.componentInstance.disciplinaryActionID=data.disciplinaryActionID;
     dialogRef.afterClosed().subscribe(result => {
      if(result) {
        if(result.response=='Accept'){
          data.meetingTime=result;
          data.status='Awaiting Employee Acceptance' ;
        }
        
      }  
    });
  
}

}
