import { Component, OnInit } from '@angular/core';
import { MdDialogRef, MdDialog, MdSnackBar } from '@angular/material';
import { APPCONFIG } from '../../../config';
import { EmployeeListingPopupComponent } from '../employee-listing-popup/employee-listing-popup.component';
import {  CompanyModuleListingService } from '../company-module-listing.service';
import { CoreService } from '../../../shared/services/core.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-assets-listing',
  templateUrl: './assets-listing.component.html',
  styleUrls: ['./assets-listing.component.scss']
})
export class AssetsListingComponent implements OnInit {


  public assestList = [];
  public positionList;
  public isProcessing;
  public filter = { search : "" };

  constructor(public dialog: MdDialog, public companyModuleListingService : CompanyModuleListingService
  ,public coreService: CoreService , public router:Router) { }

  ngOnInit() {
    APPCONFIG.heading = "Assets";
    this.getAssestList();

  } 



  routerchange(asset?){
    if(!asset) this.router.navigate([`/app/company-admin/listing-page/assets/create`]);  
    else {
          this.companyModuleListingService.tempAssetForOperation = asset;
          this.router.navigate([`/app/company-admin/listing-page/assets/edit/${ asset.assetID }`]);  
    }  

  }

  searchList(v){
    this.filter.search = encodeURIComponent(v);
    this.getAssestList();
  }

  deleteAsset(id){
      this.isProcessing = true;
      this.companyModuleListingService.deleteAssestStatus(id).subscribe(
          res => {
                  this.isProcessing = false;
                  if (res.code == 200) {
                    try{ 
                      this.assestList.splice(this.assestList.indexOf(this.assestList.filter(item => item.assetID  == id)[0]) , 1 )
                    }
                     catch(e){
                       
                     }
                    
                    this.coreService.notify("Successful", res.message, 1);
                  }
                  else return this.coreService.notify("Unsuccessful", res.message, 0);
                },
                err => {
                  this.isProcessing = false;
                  if (err.code == 400) {
                    return this.coreService.notify("Unsuccessful", err.message, 0);
                  }
                  this.coreService.notify("Unsuccessful", "Error while getting data.", 0)
                }
      );
}

  getAssestList(){
   this.isProcessing = true;
   this.companyModuleListingService.getAssestList(JSON.stringify(this.filter)).subscribe(
      res => {
        this.isProcessing = false;
        if (res.code == 200)  this.assestList =res.data? res.data.filter(i=>i.isNative):[];
        else return this.coreService.notify("Unsuccessful", res.message, 0);
      },
      err => {
        this.isProcessing = false;
        if (err.code == 400) {
          return this.coreService.notify("Unsuccessful", err.message, 0);
        }
        this.coreService.notify("Unsuccessful", "Error while getting data.", 0)
      }
    );
  }

  onPopUp() {
    let dialogRef = this.dialog.open(EmployeeListingPopupComponent);
        dialogRef.componentInstance.moduleName='assestList';
    dialogRef.afterClosed().subscribe(result => {
      if (result) {

      }
    });
  }

}
