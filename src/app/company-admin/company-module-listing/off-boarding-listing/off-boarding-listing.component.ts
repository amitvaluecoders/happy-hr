import { Component, OnInit,ViewContainerRef } from '@angular/core';
import { MdDialogRef, MdDialog, MdSnackBar } from '@angular/material';
import { APPCONFIG } from '../../../config';
import { EmployeeListingPopupComponent } from '../employee-listing-popup/employee-listing-popup.component';
import { CompanyModuleListingService } from './../company-module-listing.service';
import { CoreService } from '../../../shared/services/core.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { ConfirmService } from '../../../shared/services/confirm.service';
import { RestfulLaravelService } from '../../../shared/services/restful-laravel.service';

@Component({
  selector: 'app-off-boarding-listing',
  templateUrl: './off-boarding-listing.component.html',
  styleUrls: ['./off-boarding-listing.component.scss']
})
export class OffBoardingListingComponent implements OnInit {
public toggleFilter: boolean;
public employmentList;
public positionList;
public isProcessing=false;
public offBoardList=[];

  constructor(
    public dialog: MdDialog,
    private router: Router,
    public confirmService: ConfirmService,
    public restfulService: RestfulLaravelService,
    public viewContainerRef: ViewContainerRef,
    private companyModuleListingService:CompanyModuleListingService,
    private coreService: CoreService
  ) { }

  ngOnInit() {
      APPCONFIG.heading = "Off-boarding listing";
    this.toggleFilter = true;

    this.employmentList = ['Casual', 'Contract', 'Full time', 'Own contract', 'Part time'];
    this.positionList = ['Creative director', 'Developer', 'Facility manager', 'Graphics designer', 'IT consultant'];
    this.getOffBoardListing()
  }
   onPopUp() {
    let dialogRef = this.dialog.open(EmployeeListingPopupComponent);
        dialogRef.componentInstance.moduleName='trainingPlan';
    dialogRef.afterClosed().subscribe(result => {
      if (result) {

      }
    });
  }

  //GET OFF BOARD LISTING
  getOffBoardListing(){
    this.isProcessing = true;
    this.companyModuleListingService.getOffBoardList()
      .subscribe(
        d => {
            console.log(d)
              if (d.code == "200") {
                this.offBoardList = d.data;
                 this.offBoardList = this.offBoardList.filter(item=>{
                   if(item.status!='Deleted'){
                     return item;
                   }
                 })  

                this.isProcessing = false;    
              }
              else this.coreService.notify("Unsuccessful", d.message, 0);
          },
          error => this.coreService.notify("Unsuccessful", error.message, 0)
      );
  }

  onDelete(data){
    this.confirmService.confirm(
         this.confirmService.tilte,
         this.confirmService.message,
         this.viewContainerRef
       )
       .subscribe(res => {
         let result = res;
         if (result) {
           this.restfulService.delete(`delete-checklist/${data.employeeChecklistID}`)
       .subscribe(
         res => {
           if (res.code == 200) {
             this.coreService.notify("Success", res.message, 1);
             this.getOffBoardListing();
            //this.employmentList = this.employmentList.filter(i=> i.employeeChecklistID != data.employeeChecklistID);
           }
           else return this.coreService.notify("Unsuccessful", res.message, 0);
         },
         err => {
           if (err.code == 400) {
             return this.coreService.notify("Unsuccessful", err.message, 0);
           }
           this.coreService.notify("Unsuccessful", "Problem while performing action.", 0)
         }
       );
         }
       })
       
   
     }


  //ON EDIT DETAILS
  onEdit(id) {
      this.router.navigate([`/app/company-admin/checklist/edit/off-boarding/${id}`]);
    }

  //ON VIEW DETAILS
  onViewDetails(id) {
    this.router.navigate([`/app/company-admin/checklist/view/off-boarding/${id}`]);
  }

}
