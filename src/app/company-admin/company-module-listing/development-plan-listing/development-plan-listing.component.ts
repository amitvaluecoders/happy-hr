import { Component, OnInit,ViewContainerRef } from '@angular/core';
import { DevelopmentPlanService } from '../../development-plan/development-plan.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { CoreService } from '../../../shared/services/core.service';
import {APPCONFIG } from '../../../config'; 
import { MdDialogRef, MdDialog, MdSnackBar } from '@angular/material';
import { ConfirmService } from '../../../shared/services/confirm.service';
import { EmployeeListingPopupComponent } from '../employee-listing-popup/employee-listing-popup.component';
import { ReScheduleDevelopmentPlanComponent } from './re-schedule-development-plan/re-schedule-development-plan.component';
import * as moment from 'moment';

@Component({
  selector: 'app-development-plan-listing',
  templateUrl: './development-plan-listing.component.html',
  styleUrls: ['./development-plan-listing.component.scss']
})
export class DevelopmentPlanListingComponent implements OnInit {
  public toggleFilter:boolean;
  public employmentList;
  public positionList;
  public isProcessing=false;
  public isFilterDataProcessing=false;
  public enableFilter=false;
  public developmentPlans=[]; 
  public searchKey=''; 
  public filter={
    status: [],
    result: [],
    manager: [],
    employee: [],
    due: []
  }
  public searchKeyStatus='';
  public searchKeyManager='';
  public searchKeyEmployee='';
  public unsubscriber:any;



public reqBody = {
  search:"",
  filter:{
    result:{},
    employeeIDs:[],
    managerIDs:[],
    due:[],
    status:{}
  }
}


  constructor( public developmentPlanService: DevelopmentPlanService,public confirmService:ConfirmService,
               public router: Router, public activatedRoute: ActivatedRoute,public viewContainerRef:ViewContainerRef,
               public coreService: CoreService,public dialog: MdDialog) { }

  ngOnInit() {
    this.getDataForFilters();
    this.getDevelopmentPlans();
    APPCONFIG.heading="Development plan listing";
    this.toggleFilter=true;
    
  }
  formateDate(v){
    return moment(v).calendar()? moment(v).format('DD MMM YYYY') : 'Not specified';
}

  onApplyFilters(){

        this.reqBody.filter.status={};
        this.reqBody.filter.managerIDs=[];
        this.reqBody.filter.employeeIDs=[];
        this.reqBody.filter.result={};
        this.reqBody.filter.due=[];
        for(let i of this.filter.status){
          if(i.isChecked)this.reqBody.filter.status[i.title]=true;
        } 
        for(let i of this.filter.manager){
          if(i.isChecked)this.reqBody.filter.managerIDs.push(i.userID);
        } 
        for(let i of this.filter.employee){
          if(i.isChecked)this.reqBody.filter.employeeIDs.push(i.userID);
        } 
        for(let i of this.filter.result){
          if(i.isChecked)this.reqBody.filter.result[i.title]=true;
        }
        for(let i of this.filter.due){
          if(i.isChecked)this.reqBody.filter.due.push(i.value);
        }
        this.getDevelopmentPlans();
  }

  onSearchKeyEnter(){
    this.unsubscriber.unsubscribe();
    this.reqBody.search=encodeURIComponent(this.searchKey);
        this.getDevelopmentPlans();    
  }

  getDataForFilters(){
     this.isFilterDataProcessing = true;
     this.developmentPlanService.getDataForFilters().subscribe(
          d => {
              if (d.code == "200") {
                this.filter=d.data; 
                this.isFilterDataProcessing = false;    
              }
              else this.coreService.notify("Unsuccessful", d.message, 0);
          },
          error => this.coreService.notify("Unsuccessful", "Error while getting development details", 0),
          () => {this.isFilterDataProcessing = false;}
      )
  }

  getDevelopmentPlans() {
     this.isProcessing = true;
      this.unsubscriber = this.developmentPlanService.getAllDevelopmentPlans(JSON.stringify(this.reqBody)).subscribe(
          d => {
              if (d.code == "200") {
                this.developmentPlans=d.data; 
                 for(let i of this.developmentPlans){
                  i['isDeleteProcessing']=false;
                }
                this.isProcessing = false;    
              }
              else this.coreService.notify("Unsuccessful", d.message, 0);
          },
          error => this.coreService.notify("Unsuccessful", "Error while getting development details", 0),
          () => { }
      )
  }


    OnDelete(data){
      this.confirmService.confirm(this.confirmService.tilte, this.confirmService.message, this.viewContainerRef)
      .subscribe(res => {
        let result = res;
        if (result) {
                this.unsubscriber=this.developmentPlanService.deleteDevelopmentPlan(data.developmentPlanID).subscribe(
                d => {
                    if (d.code == "200") {
                      this.coreService.notify("Successful", d.message, 1);
                      this.developmentPlans=this.developmentPlans.filter(function(item){
                        if(item.developmentPlanID==data.developmentPlanID)return null; 
                        else return item;
                      })
                       data.isDeleteProcessing=false;
                    }
                    else{
                      this.coreService.notify("Unsuccessful", d.message, 0);
                      data.isDeleteProcessing=false;
                    } 
                  },
                error => {
                  data.isDeleteProcessing=false;
                  this.coreService.notify("Unsuccessful", error.message, 0);
                }
                
            )
        }
        else data.isDeleteProcessing=false;
  })
}

  OnAction(d) {
      console.log("responce........",d);
      this.coreService.setDevelopmentPlan(d);
      this.router.navigate([`/app/company-admin/development-plan/reassessment-assessment/${d.developmentPlanID}`]);
  }

   onPopUp() {
    let dialogRef = this.dialog.open(EmployeeListingPopupComponent);
    dialogRef.componentInstance.moduleName='developmentPlan';
    dialogRef.afterClosed().subscribe(result => {
      if (result) {

      }
    });
  }

  OnViewDevelopmentPlan(d){
    this.router.navigate([`/app/company-admin/development-plan/Details/${d.status}/${d.developmentPlanID}`]);
  }

// method to accept or reject re-schedule request for development plan 
    OnReschedule(d){
        let dialogRef = this.dialog.open(ReScheduleDevelopmentPlanComponent);
        dialogRef.componentInstance.developmentPlan=JSON.parse(JSON.stringify(d));
        dialogRef.afterClosed().subscribe(result => {
          if (result) {
              this.getDevelopmentPlans();
      }
    });
    }


// method to accept or reject re-schedule request for development plan 
    OnRescheduleAcceptReject(d){
        let dialogRef = this.dialog.open(developmentPlanReschedulePopup);
        dialogRef.componentInstance.developmentPlanID=d.developmentPlanID;
        dialogRef.afterClosed().subscribe(result => {
          if (result) {
              d.status='Awaiting employee acceptance';
      }
    });
    }

}

@Component({
  selector:'Development-reschedule-popup',
  template:`<div class="reschedule-popup">
            <h5>Reschedule Accept / Reject</h5>
            <hr>
            <md-progress-spinner *ngIf="isProcessing" mode="indeterminate" color=""></md-progress-spinner>
            <div  *ngIf="!isProcessing" >
            <form #reschedule="ngForm">
            <div class="row form-group">
              <div class="col-sm-6"><label class="form-inline">Requested by</label></div>
              <div class="col-sm-6">
                  <div class="media">
                    <img class="d-flex align-self-center mr-3" src="{{developmentPlan.requestedByUser.imageUrl}}" alt="user">
                    <div class="media-body">
                      <h6 class="mt-0" style="margin:0;">{{developmentPlan.requestedByUser.employee}}</h6>
                      <p>{{developmentPlan.requestedByUser.employeeProfile}}</p>
                    </div>
                  </div>              
              </div>
              </div>
              <div class="row form-group">
              <div class=" form-group col-sm-6"><label class="form-inline">Original training date:
                   <small>{{developmentPlan.originalTime| dateFormat}}</small> at 
                   <small>{{developmentPlan.originalTime| date:'shortTime'}}</small>
                   </label>
              </div>
              <div class="form-group col-sm-6"><label class="form-inline">Requested training date: 
                <small>{{developmentPlan.proposedTime| dateFormat}}</small> at
                <small>{{developmentPlan.proposedTime| date:'shortTime'}}</small>
                </label>
              </div>
      
              <div class="form-group col-sm-6"><label class="form-inline">Original assessment date: 
                <small>{{developmentPlan.originalAssessmentTime| dateFormat}}</small> at
                <small>{{developmentPlan.originalAssessmentTime| date:'shortTime'}}</small>
                </label>
              </div>
              <div class="col-sm-6">
                <label class="form-inline">Re-schedule assessment date </label>  
                 <div class="form-group">                
                  <input [readonly]=true #assessmentDate="ngModel" required [(ngModel)]="tempDevelopmentPlan.assessmentDate" name="assessmentDate1"
                    placeholder="Select date" class="form-control" ngui-datetime-picker date-format="YYYY-MM-DD" date-only=true
                    [close-on-select]="true" />
                  <div *ngIf="assessmentDate.errors && ( (assessmentDate.dirty || assessmentDate.touched) || isButtonClicked )">
                    <div class="alert alert-danger" [hidden]="!(assessmentDate.errors.required || assessmentDate.errors.whitespace)">
                      Assessment date is required.
                    </div>
                 </div>
                  </div>
                  <label><i>at</i></label>

                 <div class="form-group">                            
                  <input [readonly]=true #assessmentTime="ngModel" required [(ngModel)]="tempDevelopmentPlan.assessmentTime" name="assessmentTimeq"
                    placeholder="Select time" class="form-control" ngui-datetime-picker date-format="hh:mm:ss" time-only=true
                    [close-on-select]="true" />
                  <div *ngIf="assessmentTime.errors && ( (assessmentTime.dirty || assessmentTime.touched) || isButtonClicked )">
                    <div class="alert alert-danger" [hidden]="!(assessmentTime.errors.required || assessmentTime.errors.whitespace)">
                      Assessment time is required.
                    </div>
                 </div>
               </div>
               </div>
     
              <div class="col-sm-12 text-center">
              <br>
              <br>
              <br>
              
              <md-progress-spinner class="buttonWrap" *ngIf="isActionProcessing" mode="indeterminate" color=""></md-progress-spinner>
                <div *ngIf="!isActionProcessing" class="buttonWrap">
                  <button (click)="OnRescheduleResponse('Accept',reschedule.form.valid)" class="btn btn-success">Accept</button>
                  <button (click)="OnRescheduleResponse('Reject',true)" class="btn btn-danger">Reject</button>
                </div>
              </div>        
            </div>
             </form>
            </div>
           
            </div>
            `,
  styles:[`.reschedule-popup{max-width:800px;}
            img.d-flex.align-self-center.mr-3{    max-width: 50px; 
    margin-top: -17px;
    max-height: 40px;}`]

})
export class developmentPlanReschedulePopup implements OnInit{

  public developmentPlanID:any;
  public developmentPlan:any;
  public isButtonClicked=false;
   public tempDevelopmentPlan={
    assessmentDate:'',
    assessmentTime:''
  };
  public isProcessing=false;
  public isActionProcessing=false;
  constructor(public dialogRef:MdDialogRef<developmentPlanReschedulePopup>, 
              public coreService: CoreService,
              public developmentPlanService: DevelopmentPlanService){}

  ngOnInit(){
    this.getReScheduleData(this.developmentPlanID);
  }

  getReScheduleData(id){
    this.isProcessing=true;
    this.developmentPlanService.getReScheduleData(id).subscribe(
          d => {
              if (d.code == "200") {
                this.developmentPlan=d.data[0]; 
              }
              else this.coreService.notify("Unsuccessful", d.message, 0);
          },
          error => this.coreService.notify("Unsuccessful", error.message, 0),
          () => {this.isProcessing=false; }
      )
  }

  OnRescheduleResponse(res,isvalid){
  this.isButtonClicked=true;
  if(isvalid){
  let reqBody = { 
    "dpRescheduleID": this.developmentPlan.dpRescheduleID,
    "action":res,
    "trainingTime":this.developmentPlan.proposedTime,
    "assessmentTime":this.tempDevelopmentPlan.assessmentDate.toString()+" "+this.tempDevelopmentPlan.assessmentTime.toString()
  }
  this.isActionProcessing=true;
      this.developmentPlanService.responseReSchedule(reqBody).subscribe(
          d => {
              if (d.code == "200") {
                this.coreService.notify("Successful", d.message, 1);
              }
              else this.coreService.notify("Unsuccessful", d.message, 0);
          },
          error => this.coreService.notify("Unsuccessful", error.message, 0),
          () => {this.isActionProcessing=false;
              this.dialogRef.close(res); 
            }
      )

  }
  }

}


