import { Component, OnInit } from '@angular/core';
import { DevelopmentPlanService } from '../../../development-plan/development-plan.service';
import { MdDialogRef} from '@angular/material';
import { CoreService } from '../../../../shared/services/core.service';


@Component({
  selector: 'app-re-schedule-development-plan',
  templateUrl: './re-schedule-development-plan.component.html',
  styleUrls: ['./re-schedule-development-plan.component.scss']
})
export class ReScheduleDevelopmentPlanComponent implements OnInit {

  public developmentPlan:any;
  public isButtonClicked=false;
  public isActionProcessing= false;
  constructor(public developmentPlanService: DevelopmentPlanService,public coreService: CoreService,
              public dialogRef:MdDialogRef<ReScheduleDevelopmentPlanComponent>) { }

  ngOnInit() {
    
    this.developmentPlan.trainingDate=new Date(this.developmentPlan.trainingTime)
    this.developmentPlan.assessmentDate=new Date(this.developmentPlan.assessmentTime)

  }

  OnReschedule(isvalid){
    this.isButtonClicked=true;
    if(isvalid){
      let reqBody={
          developmentPlanID: this.developmentPlan.developmentPlanID,
          trainingTime:this.developmentPlan.trainingDate.toString()+" "+this.developmentPlan.trainingTime.toString(),
          assessmentTime:this.developmentPlan.assessmentDate.toString()+" "+this.developmentPlan.assessmentTime.toString()
      }
      this.isActionProcessing=true;
      this.developmentPlanService.reScheduleDevelopemnetPlan(reqBody).subscribe(
          d => {
              this.isActionProcessing=false;              
              if (d.code == "200") {
                this.developmentPlan.trainingTime=reqBody.trainingTime;
                this.developmentPlan.assessmentTime=reqBody.assessmentTime;
                this.dialogRef.close(this.developmentPlan);
                this.coreService.notify("Successful", d.message, 1);
              }
              else this.coreService.notify("Unsuccessful", d.message, 0);
          },
          error => {
              this.isActionProcessing=false,
              this.coreService.notify("Unsuccessful", error.message, 0)
          }
         
      )
    }
  }

}
