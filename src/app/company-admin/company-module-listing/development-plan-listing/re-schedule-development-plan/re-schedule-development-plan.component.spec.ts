import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReScheduleDevelopmentPlanComponent } from './re-schedule-development-plan.component';

describe('ReScheduleDevelopmentPlanComponent', () => {
  let component: ReScheduleDevelopmentPlanComponent;
  let fixture: ComponentFixture<ReScheduleDevelopmentPlanComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReScheduleDevelopmentPlanComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReScheduleDevelopmentPlanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
