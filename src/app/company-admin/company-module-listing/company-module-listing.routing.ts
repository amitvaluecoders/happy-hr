import { ModuleWithProviders } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PerformanceImprovementPlanListingComponent } from './performance-improvement-plan-listing/performance-improvement-plan-listing.component';
import { DevelopmentPlanListingComponent } from './development-plan-listing/development-plan-listing.component';
import { BehaviorPerformanceImprovementPlanListingComponent } from './behavior-performance-improvement-plan-listing/behavior-performance-improvement-plan-listing.component';
import { WarningListComponent } from './warning-list/warning-list.component';
import { DisciplinaryActionListingComponent } from './disciplinary-action-listing/disciplinary-action-listing.component';
import { PerformanceAppraisalListingComponent } from './performance-appraisal-listing/performance-appraisal-listing.component';
import { InductionOnboardingListingComponent } from './induction-onboarding-listing/induction-onboarding-listing.component';
import { OffBoardingListingComponent } from './off-boarding-listing/off-boarding-listing.component';
import { PerformanceIndicatorListingComponent } from './performance-indicator-listing/performance-indicator-listing.component';
import { NewEmployeeApprovalComponent } from './new-employee-approval/new-employee-approval.component'
import { AssetsApprovalComponent } from './assets-approval/assets-approval.component';
import { AssetsListingComponent } from './assets-listing/assets-listing.component';
import { ExitSurveyComponent } from './exit-survey/exit-survey.component';
import { GrievanceListingComponent } from "./grievance-listing/grievance-listing.component";
import { OhsListingComponent } from "./ohs-listing/ohs-listing.component";
import { ProbationaryMeetingComponent } from './probationary-meeting/probationary-meeting.component';
// import {LeaveListComponent} from '../leaves/leave-list/leave-list.component';
import { GrievanceGuardService } from "../grievance/grievance-guard.service";
import { PerformanceImprovementPlanGuardService } from "../performance-improvement-plan/performance-improvement-plan-guard.service";
import { DevelopmentPlanGuardService } from "../development-plan/development-plan-guard.service";
import { BehaviouralPerformanceImprovementPlanGuardService } from "../behavioral-performance-improvement-plan/behavioural-performance-improvement-plan-guard.service";
import { DisciplinaryMeetingGuardService } from "../disciplinary-meeting/disciplinary-meeting-guard.service";
import { AssetsAddEditComponent } from './assets-add-edit/assets-add-edit.component';
import { AssetsApprovalAddEditComponent } from './assets-approval-add-edit/assets-approval-add-edit.component'
import { ProbationActionComponent } from './probation-action/probation-action.component';
import { TrophyCertificatesComponent } from './trophy-certificates/trophy-certificates.component';
import {CreateOhComponent} from './create-oh/create-oh.component';
import {ContractUpdateListingComponent} from './contract-update-listing/contract-update-listing.component';
//import {ContractDetailComponent} from '../../company-employee/employee-contracts/contract-detail/contract-detail.component';
import { ContractDetailsComponent } from './contract-update-listing/contract-details/contract-details.component';
export const CompanyModuleListingRoutes: Routes = [

    {
        path: '',
        canActivate: [],
        children: [
            { path: '', component: PerformanceImprovementPlanListingComponent, canActivate: [PerformanceImprovementPlanGuardService] },
            { path: 'development-plan-list', component: DevelopmentPlanListingComponent, canActivate: [DevelopmentPlanGuardService] },
            { path: 'behavior-performance-improvement-listing', component: BehaviorPerformanceImprovementPlanListingComponent, canActivate: [BehaviouralPerformanceImprovementPlanGuardService] },
            { path: 'warning-list', component: WarningListComponent },
            { path: 'grievance-listing', component: GrievanceListingComponent, canActivate: [GrievanceGuardService] },
            { path: 'disciplinary-action-listing', component: DisciplinaryActionListingComponent, canActivate: [DisciplinaryMeetingGuardService] },
            { path: 'performance-appraisal-listing', component: PerformanceAppraisalListingComponent },
            { path: 'induction-onboarding-listing', component: InductionOnboardingListingComponent },
            { path: 'off-boarding-listing', component: OffBoardingListingComponent },
            { path: 'performance-indicator-listing', component: PerformanceIndicatorListingComponent },
            { path: 'new-emloyee-approval', component: NewEmployeeApprovalComponent },
            { path: 'assets-approval', component: AssetsApprovalComponent },
            { path: 'assets-listing', component: AssetsListingComponent },
            { path: 'exit-survey', component: ExitSurveyComponent },
            { path: 'ohs-listing', component: OhsListingComponent },
            {path:  'create-oh&s/:id', component:CreateOhComponent},
            { path: 'probationary-meeting', component: ProbationaryMeetingComponent },
            { path: 'assets/create', component: AssetsAddEditComponent },
            { path: 'assets/edit/:id', component: AssetsAddEditComponent },
            { path: 'assets-approve/edit/:id', component: AssetsApprovalAddEditComponent },
            { path: 'assets-approve/create', component: AssetsApprovalAddEditComponent },
            { path: 'probationary-meeting/action/:status/:id', component: ProbationActionComponent },
            { path: 'trophy-certificates', component: TrophyCertificatesComponent },
            { path: 'contract-update', component: ContractUpdateListingComponent },
            { path: 'contract-detail/:id/:employeeCompanyContractID', component: ContractDetailsComponent},
            // { path:'leaves', component:LeaveListComponent}
        ]
    }
];

export const ContractsRoutingModule = RouterModule.forChild(CompanyModuleListingRoutes);
