import { Component, OnInit } from '@angular/core';
import { MdDialogRef, MdDialog, MdSnackBar } from '@angular/material';
import { APPCONFIG } from '../../../config';
import { EmployeeListingPopupComponent } from '../employee-listing-popup/employee-listing-popup.component';
import {  CompanyModuleListingService } from '../company-module-listing.service';
import { CoreService } from '../../../shared/services/core.service';
import { Router, ActivatedRoute } from '@angular/router';
import { CertificatePopUpComponent } from './certificate-pop-up/certificate-pop-up.component';
import * as moment from 'moment';

@Component({
  selector: 'app-trophy-certificates',
  templateUrl: './trophy-certificates.component.html',
  styleUrls: ['./trophy-certificates.component.scss']
})
export class TrophyCertificatesComponent implements OnInit {

    public isProcessing = false;
    public list =[];
  constructor(public dialog: MdDialog, public companyModuleListingService : CompanyModuleListingService
    ,public coreService: CoreService , public router:Router) { }

    ngOnInit() {
      APPCONFIG.heading = "Trophy certificates";
      this.getAssestList();
    }

    formatDate(v){
      return moment(v).isValid()? moment(v).format('DD MMM YYYY') : 'Not specified';
  }

    getAssestList(){
      this.isProcessing = true;
      this.companyModuleListingService.getTrophyCertificatesList().subscribe(
         res => {
           this.isProcessing = false;
           if (res.code == 200) {
             this.list = res.data?res.data:[];
           }
           else return this.coreService.notify("Unsuccessful", res.message, 0);
         },
         err => {
           this.isProcessing = false;
           if (err.code == 400) {
             return this.coreService.notify("Unsuccessful", err.message, 0);
           }
           this.coreService.notify("Unsuccessful", "Error while getting data.", 0)
         }
       );
     }

     onView(certificate){
      let dialogRef = this.dialog.open(CertificatePopUpComponent);
      dialogRef.componentInstance.data = certificate;
      dialogRef.afterClosed().subscribe(res => {
       
      });
     }

}
