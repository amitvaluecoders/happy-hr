import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TrophyCertificatesComponent } from './trophy-certificates.component';

describe('TrophyCertificatesComponent', () => {
  let component: TrophyCertificatesComponent;
  let fixture: ComponentFixture<TrophyCertificatesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TrophyCertificatesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TrophyCertificatesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
