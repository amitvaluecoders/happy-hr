import { Component, OnInit } from '@angular/core';
import { MdDialogRef, MdDialog, MdSnackBar } from '@angular/material';
import { APPCONFIG } from '../../../config';
import { EmployeeListingPopupComponent } from '../employee-listing-popup/employee-listing-popup.component';
import {  CompanyModuleListingService } from '../company-module-listing.service';
import { CoreService } from '../../../shared/services/core.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-new-employee-approval',
  templateUrl: './new-employee-approval.component.html',
  styleUrls: ['./new-employee-approval.component.scss']
})
export class NewEmployeeApprovalComponent implements OnInit {

  public newEmployeeList = [];
  public isProcessing:boolean;
   public filter = { search : "" };

  constructor(public dialog: MdDialog, public companyModuleListingService : CompanyModuleListingService
  ,public coreService: CoreService , public router:Router) { }

  ngOnInit() {
      APPCONFIG.heading = "New employee approval listing";
    this.getNewEmployeeList();
  } 
  onPopUp() {
    let dialogRef = this.dialog.open(EmployeeListingPopupComponent);
        dialogRef.componentInstance.moduleName='trainingPlan';
    dialogRef.afterClosed().subscribe(result => {
      if (result) {

      }
    });
  }

  search(v){
      this.filter.search = encodeURIComponent(v);
    //  this.getNewEmployeeList();
  }


  statusChange(employee , status){
      this.isProcessing = true;
      this.companyModuleListingService.changeNewEmployeeStatus({
        "employeeInvitedID": employee.employeeInvitedID,
        "status":status
      }).subscribe(
         res => {
        this.isProcessing = false;
        if (res.code == 200) {
          this.coreService.notify("Successful", res.message, 1);
          employee.status = status;
        }
        else return this.coreService.notify("Unsuccessful", res.message, 0);
      },
      err => {
        this.isProcessing = false;
        if (err.code == 400) {
          return this.coreService.notify("Unsuccessful", err.message, 0);
        }
        this.coreService.notify("Unsuccessful", "Error while getting data.", 0)
      }
      )
  }


  getNewEmployeeList(){
      this.isProcessing = true;
   this.companyModuleListingService.getNewEmployeeList().subscribe(
      res => {
        this.isProcessing = false;
        if (res.code == 200) {
          this.newEmployeeList = res.data;
        }
        else return this.coreService.notify("Unsuccessful", res.message, 0);
      },
      err => {
        this.isProcessing = false;
        if (err.code == 400) {
          return this.coreService.notify("Unsuccessful", err.message, 0);
        }
        this.coreService.notify("Unsuccessful", "Error while getting data.", 0)
      }
    );
  }
}
