import { Injectable } from '@angular/core';
import {CanActivate,Router,RouterStateSnapshot,ActivatedRouteSnapshot} from "@angular/router";
import {CoreService} from '../../shared/services/core.service';
import {Observable} from 'rxjs/Rx'; 
import 'rxjs/add/operator/map';

@Injectable()
export class CompanyModuleGuardService {

  public module = "listing";
  
        constructor(private _core: CoreService, private router: Router){ }
  
        canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {  
            let url: string;
            if(route['_routeConfig']['path'])
            url= route['_routeConfig']['path']
            else url = state.url;
            if(this.checkHavePermission(url)){
              return true;
            }else{
              this.router.navigate(['/extra/unauthorized']);
              return false;
            }
        }
  
        private checkHavePermission(path) {
            switch (path) {
                case "development-plan-list":
                    return this._core.getModulePermission(this.module, 'view');
                case "behavior-performance-improvement-listing":
                    return this._core.getModulePermission(this.module, 'view');
                case "training-plan-listing":
                    return this._core.getModulePermission(this.module, 'view');
                case "warning-list":
                    return this._core.getModulePermission(this.module, 'view');
                case "grievance-listing":
                    return this._core.getModulePermission(this.module, 'view');
                case "disciplinary-action-listing":
                    return this._core.getModulePermission(this.module, 'view');
                case "performance-appraisal-listing":
                    return this._core.getModulePermission(this.module, 'view');
                case "induction-onboarding-listing":
                    return this._core.getModulePermission(this.module, 'view');
                case "off-boarding-listing":
                    return this._core.getModulePermission(this.module, 'view');
                case "performance-indicator-listing":
                    return this._core.getModulePermission(this.module, 'view');
                case "Trophy-certificates":
                    return this._core.getModulePermission(this.module, 'view');
                case "new-contract-list":
                    return this._core.getModulePermission(this.module, 'view');
                case "new-policy-list":
                    return this._core.getModulePermission(this.module, 'view');
                case "new-emloyee-approval":
                    return this._core.getModulePermission(this.module, 'view');
                case "approve-to-recruit-listing":
                    return this._core.getModulePermission(this.module, 'view');
                case "assets-approval":
                    return this._core.getModulePermission(this.module, 'view');
                case "assets-listing":
                    return this._core.getModulePermission(this.module, 'view');
                case "exit-survey":
                    return this._core.getModulePermission(this.module, 'view');
                case "ohs-listing":
                    return this._core.getModulePermission(this.module, 'view');
                case "probationary-meeting":
                    return this._core.getModulePermission(this.module, 'view');
                default:
                    return false;
            }
        }
}


