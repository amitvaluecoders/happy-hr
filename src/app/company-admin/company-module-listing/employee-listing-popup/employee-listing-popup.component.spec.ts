import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployeeListingPopupComponent } from './employee-listing-popup.component';

describe('EmployeeListingPopupComponent', () => {
  let component: EmployeeListingPopupComponent;
  let fixture: ComponentFixture<EmployeeListingPopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmployeeListingPopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployeeListingPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
