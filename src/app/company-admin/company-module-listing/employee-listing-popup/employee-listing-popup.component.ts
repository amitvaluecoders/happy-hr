import { Component, OnInit } from '@angular/core';
import { MdDialogRef, MdDialog, MdSnackBar } from '@angular/material';
// import { DevelopmentPlanService } from '../../development-plan/development-plan.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { CoreService } from '../../../shared/services/core.service';
import { CompanyModuleListingService } from "../company-module-listing.service";
import {LeaveApplyPopupComponent} from "../../leaves/leave-apply-popup/leave-apply-popup.component";
import { ActionsOnEmployeePopupComponent } from '../../../shared/components/actions-on-employee-popup/actions-on-employee-popup.component';

@Component({
  selector: 'app-employee-listing-popup',
  templateUrl: './employee-listing-popup.component.html',
  styleUrls: ['./employee-listing-popup.component.scss']
})
export class EmployeeListingPopupComponent implements OnInit {

  public moduleName: any;
  public user = { message: "" };
  public selectedItems = [];
  public whiteSpaceError = false;
  public dropdownSettings = {};
  public isProcessing;
  public counter = 0;
  result:any;
  public dropdownListEmployees = [];


  constructor(public dialogRef: MdDialogRef<EmployeeListingPopupComponent>,
    public companyService: CompanyModuleListingService,
    public router: Router, public activatedRoute: ActivatedRoute,
    public dialog: MdDialog,
    public coreService: CoreService) {

  }

  ngOnInit() {
    this.getEmployeeeList();
    this.dropdownListEmployees = [];
    this.dropdownSettings = {
      singleSelection: true,
      text: "Select employee",
      enableSearchFilter: true,
      classes: "myclass assmng"
    };
  }

  getEmployeeeList() {
    this.isProcessing = true;
    this.companyService.getAllEmployee().subscribe(
      d => {
        this.isProcessing = false;
        if (d.code == "200") {
          // this.dropdownListEmployees = d.data;
          this.dropdownListEmployees = d.data.filter(item => {
            item['id'] = item.userId;
            item['itemName'] = item.name;
            return item;
          })
        }
        else this.coreService.notify("Unsuccessful", d.message, 0);
      },
      error => this.coreService.notify("Unsuccessful", "Error while getting data", 0),
      () => { }
    )
  }


  attactImageAndRolesDropDownList() {

    if (this.counter == 0) {
      this.dropdownListEmployees.filter(function (item, i) {
        $('.assmng ul li').eq(i).prepend(`<img src="${item.imageUrl}" alt="" />`);
        $('.assmng ul li').eq(i).children('label').append('<span>' + item.role + '</span>');
      });

    }
    this.counter++;
  }

  onItemSelect(item: any) {
    if(this.moduleName =='leaveCadmin'){
      let dialogRef1 = this.dialog.open(LeaveApplyPopupComponent);
      dialogRef1.componentInstance.userId = item.userId;   
      dialogRef1.afterClosed().subscribe(message => {
        if (message) {
            //console.log("emp popup",r)
            this.dialogRef.close(message);  
        }
      });
    }
    else{

    let url = '/app/company-admin';
    switch (this.moduleName) {

      case 'developmentPlan':
        url = `/app/company-admin/development-plan/create/${item.userId}`;
        break;
        
      case 'ohs':
        url = `/app/company-admin/listing-page/create-oh&s/${item.userId}`;
        break;  
    
      case 'disciplinaryAction':
        url = `/app/company-admin/disciplinary-meeting/create/${item.userId}`;
        break;

      case 'pip':
        url = `/app/company-admin/performance-improvement-plan/create/${item.userId}`;
        break;

      case 'bpip':
        url = `/app/company-admin/behavioral-performance-improvement-plan/create/${item.userId}`;
        break;

      case 'performanceAppraisal':
        url = `/app/company-admin/performance-appraisal/create/${item.userId}`;
        break;

      case 'assestList':
      url = `/app/company-admin/performance-appraisal/create/${item.userId}`;
      break;

      case 'createProbation':
      url = `/app/company-admin/listing-page/probation-meeting/create/${item.userId}`;
      break;

      case 'directAssessmentPA':
      url = `/app/company-admin/performance-indicator/direct-assessment/${item.userId}`;
      break;

      // case 'leaveCadmin' :
      //   let dialogRef1 = this.dialog.open(LeaveApplyPopupComponent);
      //   dialogRef1.componentInstance.userId = item.userId;   
      //   dialogRef1.afterClosed().subscribe(message => {
      //     if (message) {
      //       return this.router.navigate(['/app/company-admin/leaves']);  
      //     }
      //   });
                
      //   break;
            
      case 'warning':
        let reqBody={userID:item.userId,
                    type:'Warning',
                    subject:"", body:""}   
        let dialogRef = this.dialog.open(ActionsOnEmployeePopupComponent);
        dialogRef.componentInstance.actionInfo = reqBody;
        dialogRef.afterClosed().subscribe(r => {
          if (r) {
            return this.router.navigate(['/app/company-admin/listing-page/warning-list']);
          }
        })
      
        //url = `app/company-admin/behavioral-performance-improvement-plan/training/${item.userId}`;
        this.dialogRef.close(item)
        break;
    }
    if(this.moduleName != 'warning'){
      this.dialogRef.close(0);
      this.router.navigate([url]);
    } 
  }
}
}
