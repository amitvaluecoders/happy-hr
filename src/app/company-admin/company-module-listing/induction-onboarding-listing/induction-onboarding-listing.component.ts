import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { MdDialogRef, MdDialog, MdSnackBar } from '@angular/material';
import { APPCONFIG } from '../../../config';
import { EmployeeListingPopupComponent } from '../employee-listing-popup/employee-listing-popup.component';
import { RestfulLaravelService } from '../../../shared/services/restful-laravel.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { CoreService } from '../../../shared/services/core.service';
import { ConfirmService } from '../../../shared/services/confirm.service';

@Component({
  selector: 'app-induction-onboarding-listing',
  templateUrl: './induction-onboarding-listing.component.html',
  styleUrls: ['./induction-onboarding-listing.component.scss']
})
export class InductionOnboardingListingComponent implements OnInit {
  public toggleFilter: boolean;
  public employmentList = [];
  public isProcessing = false;
  public isFilterDataProcessing = false;
  public enableFilter = false;
  public unsubscriber: any;
  public filter = {
    status: [],
    user: []
  }
  permission;

  public reqBody = {
    search: "",
    filter: {
      user: {},
      status: {}
    }
  }
  searchEmp = '';

  constructor(public restfulService: RestfulLaravelService, public confirmService: ConfirmService,
    public router: Router, public activatedRoute: ActivatedRoute, public viewContainerRef: ViewContainerRef,
    public coreService: CoreService, public dialog: MdDialog) { }


  ngOnInit() {
    this.toggleFilter = true;
    APPCONFIG.heading = "Induction/OnBoarding listing";
    this.getFilterData();
    this.getOnBoardList();
  }

  // onPopUp() {
  //   let dialogRef = this.dialog.open(EmployeeListingPopupComponent);
  //   dialogRef.componentInstance.moduleName = 'trainingPlan';
  //   dialogRef.afterClosed().subscribe(result => {
  //     if (result) {

  //     }
  //   });
  // }

  getFilterData() {
    this.isFilterDataProcessing = true;
    this.restfulService.get(`get-induction-on-boarding-listing-filter-data`).subscribe(
      d => {
        if (d.code == "200") {
          this.filter = d.data;
          this.isFilterDataProcessing = false;
        }
        else this.coreService.notify("Unsuccessful", d.message, 0);
      },
      error => this.coreService.notify("Unsuccessful", "Error while getting development details", 0),
      () => { this.isFilterDataProcessing = false; }
    )
  }

  // method to set filter data for getting filtered list. 
  onApplyFilters() {
    this.reqBody.filter.status = {};
    this.reqBody.filter.user = {};
    for (let i of this.filter.status) {
      this.reqBody.filter.status[i.title] = i.isSelected;
    }
    for (let i of this.filter.user) {
      if (i.isSelected) this.reqBody.filter.user[i.userID] = true;
    }
    this.getOnBoardList();
  }

  getOnBoardList() {
    this.isProcessing = true;
    this.unsubscriber = this.restfulService.get(`get-induction-on-boarding-listing/${JSON.stringify(this.reqBody)}`).subscribe(
      d => {
        this.isProcessing = false;
        if (d.code == "200") {
          this.employmentList = d.data ? d.data : [];
        }
        else this.coreService.notify("Unsuccessful", d.message, 0);
      },
      error => {
        this.isProcessing = false;
        this.coreService.notify("Unsuccessful", error.message, 0);
      }
    )
  }

  onCancel(data){
    
    this.restfulService.put(`checklist-cancel`,{employeeChecklistID:data.employeeChecklistID})
    .subscribe(
      res => {
        if (res.code == 200) {
          this.coreService.notify("Success", res.message, 1);
         data['status']='Cancelled'; 
        }
        else return this.coreService.notify("Unsuccessful", res.message, 0);
      },
      err => {
        if (err.code == 400) {
          return this.coreService.notify("Unsuccessful", err.message, 0);
        }
        this.coreService.notify("Unsuccessful", "Problem while performing action.", 0)
      }
    );
  
  }

  onDelete(data){
 this.confirmService.confirm(
      this.confirmService.tilte,
      this.confirmService.message,
      this.viewContainerRef
    )
    .subscribe(res => {
      let result = res;
      if (result) {
        this.restfulService.delete(`delete-checklist/${data.employeeChecklistID}`)
    .subscribe(
      res => {
        if (res.code == 200) {
          this.coreService.notify("Success", res.message, 1);
         this.employmentList = this.employmentList.filter(i=> i.employeeChecklistID != data.employeeChecklistID);
        }
        else return this.coreService.notify("Unsuccessful", res.message, 0);
      },
      err => {
        if (err.code == 400) {
          return this.coreService.notify("Unsuccessful", err.message, 0);
        }
        this.coreService.notify("Unsuccessful", "Problem while performing action.", 0)
      }
    );
      }
    })
    

  }

  onEdit(id) {
    this.router.navigate([`/app/company-admin/checklist/edit/on-boarding/${id}`]);
  }

  onViewDetails(id) {
    this.router.navigate([`/app/company-admin/checklist/view/on-boarding/${id}`]);
  }

}




