import { Component, OnInit,ViewContainerRef } from '@angular/core';
import { PerformanceImprovementPlanService } from '../../performance-improvement-plan/performance-improvement-plan.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { CoreService } from '../../../shared/services/core.service';
import {APPCONFIG } from '../../../config'; 
import { ConfirmService } from '../../../shared/services/confirm.service';
import { MdDialogRef, MdDialog, MdSnackBar } from '@angular/material';
import * as moment from 'moment';
import { EmployeeListingPopupComponent } from '../employee-listing-popup/employee-listing-popup.component';


@Component({
  selector: 'app-performance-improvement-plan-listing',
  templateUrl: './performance-improvement-plan-listing.component.html',
  styleUrls: ['./performance-improvement-plan-listing.component.scss']
})
export class PerformanceImprovementPlanListingComponent implements OnInit {
  public toggleFilter:boolean;
  public employmentList;
  public positionList;
  public isProcessing=false;
  public isFilterDataProcessing=false;
  public enableFilter=false;
  public plans=[]; 
  public searchKey=''; 
  public unsubscriber:any;
  public filter={
    status: [],
    result: [],
    manager: [],
    employee: [],
    due:[]
  }
  public searchKeyStatus='';
  public searchKeyManager='';
  public searchKeyEmployee='';



  public reqBody = {
    search:"",
    filter:{
      result:{},
      employeeIDs:[],
      managerIDs:[],
      due:[],
      status:{}
    }
  }


  constructor( public performanceImprovementPlanService: PerformanceImprovementPlanService,public confirmService:ConfirmService,
               public router: Router, public activatedRoute: ActivatedRoute,public viewContainerRef:ViewContainerRef,
               public coreService: CoreService,public dialog: MdDialog) { }

  ngOnInit() {
    // this.getDataForFilters();
    APPCONFIG.heading = "Performance improvement plan";
    this.toggleFilter = true;
    this.getDataForFilters();
    this.getPlans();
  }

// method to get all filter data.
  getDataForFilters(){
     this.isFilterDataProcessing = true;
     this.performanceImprovementPlanService.getDataForFilters().subscribe(
          d => {
              if (d.code == "200") {
                this.filter=d.data; 
                this.isFilterDataProcessing = false;    
              }
              else this.coreService.notify("Unsuccessful", d.message, 0);
          },
          error => this.coreService.notify("Unsuccessful", "Error while getting development details", 0),
          () => {this.isFilterDataProcessing = false;}
      )
  }
  formateDate(v){
    return moment(v).isValid()? moment(v).format('DD MMM YYYY hh:mm A') : 'Not specified';
}


// method to set filter data for getting filtered list. 
  onApplyFilters(){
        this.reqBody.filter.status={};
        this.reqBody.filter.managerIDs=[];
        this.reqBody.filter.employeeIDs=[];
        this.reqBody.filter.result={};
        this.reqBody.filter.due=[];
        for(let i of this.filter.status){
          if(i.isChecked)this.reqBody.filter.status[i.title]=true;
        } 
        for(let i of this.filter.manager){
          if(i.isChecked)this.reqBody.filter.managerIDs.push(i.userID);
        } 
        for(let i of this.filter.employee){
          if(i.isChecked)this.reqBody.filter.employeeIDs.push(i.userID);
        } 
        for(let i of this.filter.result){
          if(i.isChecked)this.reqBody.filter.result[i.title]=true;
        }
        for(let i of this.filter.due){
          if(i.isChecked)this.reqBody.filter.due.push(i.value);
        }
       this.getPlans();
  }

   // method to get list by serch key.
    onSearchKeyEnter(){
      this.unsubscriber.unsubscribe();
    this.reqBody.search=encodeURIComponent(this.searchKey);
        this.getPlans();    
  }

// method to get list of all employees on pop up to create new PIP plan.
  onPopUp() {
    let dialogRef = this.dialog.open(EmployeeListingPopupComponent);
    dialogRef.componentInstance.moduleName='pip';
    dialogRef.afterClosed().subscribe(result => {
      if (result) {

      }
    });
  }

//method to get list of PIP plans
    getPlans() {
     this.isProcessing = true;
      this.unsubscriber = this.performanceImprovementPlanService.getAllPlans(JSON.stringify(this.reqBody)).subscribe(
          d => {
              if (d.code == "200") {
                this.plans=d.data; 
                 for(let i of this.plans){
                  i['isDeleteProcessing']=false;
                }
                this.isProcessing = false;    
              }
              else this.coreService.notify("Unsuccessful", d.message, 0);
          },
          error => this.coreService.notify("Unsuccessful", error.message, 0),
          () => { }
      )
  }

    OnDelete(data){
      this.confirmService.confirm(this.confirmService.tilte, this.confirmService.message, this.viewContainerRef)
      .subscribe(res => {
        let result = res;
        if (result) {
                this.unsubscriber=this.performanceImprovementPlanService.deletePip(data.pipID).subscribe(
                d => {
                    if (d.code == "200") {
                      this.coreService.notify("Successful", d.message, 1);
                      this.plans=this.plans.filter(function(item){
                        if(item.pipID==data.pipID)return null; 
                        else return item;
                      })
                       data.isDeleteProcessing=false;
                    }
                    else{
                      this.coreService.notify("Unsuccessful", d.message, 0);
                      data.isDeleteProcessing=false;
                    } 
                  },
                error => {
                  data.isDeleteProcessing=false;
                  this.coreService.notify("Unsuccessful", error.message, 0);
                }
                
            )
        }
        else data.isDeleteProcessing=false;
  })
}

  OnAction(d) {
      this.coreService.setPIP(d);
      this.router.navigate([`/app/company-admin/performance-improvement-plan/training/${d.pipID}`]);
  }

  onView(d){
    this.coreService.setPIP(d);
      this.router.navigate([`/app/company-admin/performance-improvement-plan/details/${d.pipID}`]);
  }

}



