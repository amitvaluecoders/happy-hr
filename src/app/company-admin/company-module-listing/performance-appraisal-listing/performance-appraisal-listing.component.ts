import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { APPCONFIG } from '../../../config';
import { MdDialogRef, MdDialog, MdSnackBar } from '@angular/material';
import { EmployeeListingPopupComponent } from '../employee-listing-popup/employee-listing-popup.component';
import { PerformanceAppraisalService } from "../../performance-appraisal/performance-appraisal.service";
import { CoreService } from '../../../shared/services/core.service';
import * as moment from 'moment';

@Component({
  selector: 'app-performance-appraisal-listing',
  templateUrl: './performance-appraisal-listing.component.html',
  styleUrls: ['./performance-appraisal-listing.component.scss']
})
export class PerformanceAppraisalListingComponent implements OnInit {
  public toggleFilter = true;
  public empSearch = {};
  public paList = [];
  public employeeList = [];
  public managerList = [];
  id=null;
  public isProcessing: boolean;
  public isChecked = false;
  public filter = {
    "search": "", "filter": {
      "status": { "Awaiting Employee Acceptance": false, "Complete": false, "Awaiting Manager Response": false, "Under Assessment": false },
      "userID": [], "managerID": []
    }
  };
  constructor(public dialog: MdDialog, public companyService: PerformanceAppraisalService, public coreService: CoreService,
    public router: Router) { }

  ngOnInit() {
    APPCONFIG.heading = "Performance appraisal & direct assessment listing";

    this.getPaList();
    this.getEmployee();
    this.getManagerList();
  }

  onPopUp(moduleName) {
    let dialogRef = this.dialog.open(EmployeeListingPopupComponent);
    dialogRef.componentInstance.moduleName = moduleName;
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
      }
    });
  }

  isValidData(data) {
    return !(data instanceof Array);
  }

  getManagerList() {
    this.companyService.getManagerList(this.id).subscribe(
      res => {
        if (res.code == 200) {
          if (res.data instanceof Array) {
            this.managerList = res.data;
          }
        }
        else return this.coreService.notify("Unsuccessful", res.message, 0);
      }
    );
  }

  getEmployee() {
    this.companyService.getEmployeeList().subscribe(
      res => {
        if (res.code == 200) {
          if (res.data instanceof Array) {
            this.employeeList = res.data;
          }
        }
        else return this.coreService.notify("Unsuccessful", res.message, 0);
      }
    );
  }

  getPaList() {
    this.isProcessing = true;
    console.log(JSON.stringify(this.filter))
    this.companyService.getPerformanceAppraisalList(JSON.stringify(this.filter)).subscribe(
      res => {
        this.isProcessing = false;
        if (res.code == 200) {
          this.paList = res.data;
          this.toggleFilter = true;
        }
        else return this.coreService.notify("Unsuccessful", res.message, 0);
      },
      err => {
        this.isProcessing = false;
        if (err.code == 400) {
          return this.coreService.notify("Unsuccessful", err.message, 0);
        }
        this.coreService.notify("Unsuccessful", "Error while getting data.", 0)
      }
    );
  }

  formatDateTime(date) {
    return moment(date).isValid() ? moment(date).format('DD MMM YYYY') : 'Not specified';
  }

  onCheckEmp(event, id, type) {
    this.isChecked = true;
    let ind = this.filter.filter[type].indexOf(id);

    if (event.target.checked) {
      (ind >= 0) ? null : this.filter.filter[type].push(id);
    } else {
      (ind >= 0) ? this.filter.filter[type].splice(ind, 1) : null;
    }
  }

  viewDetail(pa) {
    if (pa.directAssessment == 'Yes') {
      this.router.navigate(['/app/company-admin/performance-appraisal/direct-assessment-result', pa.performaceAppraisalID]);
    } else {
      this.router.navigate(['/app/company-admin/performance-appraisal/result', pa.performaceAppraisalID]);
    }
  }
}
