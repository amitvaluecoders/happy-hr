import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from "@angular/router";
import { APPCONFIG } from '../../../config';
import { MdDialogRef, MdDialog, MdSnackBar } from '@angular/material';
import { EmployeeListingPopupComponent } from '../employee-listing-popup/employee-listing-popup.component';
import { OhService } from "../../oh/oh.service";
import { CoreService } from '../../../shared/services/core.service';
import { ActionsOnEmployeePopupComponent } from '../../../shared/components/actions-on-employee-popup/actions-on-employee-popup.component';
import * as moment from 'moment';

@Component({
  selector: 'app-probation-action',
  templateUrl: './probation-action.component.html',
  styleUrls: ['./probation-action.component.scss']
})
export class ProbationActionComponent implements OnInit {

  public probId;
  public status;
  public probationDetails;
  public pipManagerNote = {
    managerTraNote: ''
  };
  public isTraCreateProcessing=false;
  public onEdit=false;

  constructor(public dialog: MdDialog, public companyService: OhService, public coreService: CoreService,
    public router: Router, public activatedRoute:ActivatedRoute) { }

  ngOnInit() {
    APPCONFIG.heading = "Details";
    if(this.activatedRoute.snapshot.params['id']){
      this.status= this.activatedRoute.snapshot.params['status'];
      console.log(this.activatedRoute.snapshot.params['id']);  
      this.probId=this.activatedRoute.snapshot.params['id'];
      this.getProbationDetails(this.probId);
    }
    
  }
  formateDate(v){
    return moment(v).calendar()? moment(v).format('DD MMM YYYY') : 'Not specified';
}

  //GET PROBATION DETAILS
  getProbationDetails(probId){
    this.companyService.getProbationDetail(probId)
      .subscribe(
        data=>{
          console.log(data)
          if(data.code == 200){
            this.probationDetails=data.data;
          } 
        },
        error=>console.log(error)
    );
  }

  //ADD MANAGER NOTES
  addTrainingNotes(notesForm){
    console.log(notesForm)
    console.log(this.pipManagerNote)
    if(notesForm.valid){
      this.isTraCreateProcessing=true;
      var data={
        probationaryMeetingID:this.probId,
        note:this.pipManagerNote.managerTraNote
      };
      this.companyService.addProbationNotes(data)
        .subscribe(
          data=>{
            console.log(data)
            this.isTraCreateProcessing=false;
            this.getProbationDetails(this.probId);
            this.onEdit=false;
            this.coreService.notify("Successful", data.message, 1);
          },
          error=>{
            console.log(error)
            this.isTraCreateProcessing=false;
            this.coreService.notify("Unsuccessful", error.message, 0);
          }
        );
    }
  }

  //SAVE PROBATION ACTION
  saveProbationAction(probId,action){
    console.log(probId);
    console.log(action)
    this.isTraCreateProcessing=true;
    if(action == "Pass") {
      var data={
        probationaryMeetingID:probId,
        result:"Pass"
      };
    }
    else{
      var data={
        probationaryMeetingID:probId,
        result:"Terminate"
      };
    }
    
    this.companyService.probationAction(data)
    .subscribe(
      res => {
        this.isTraCreateProcessing=false;
        if (res.code == 200) {
          this.coreService.notify("Success", res.message, 1)
          this.getProbationDetails(this.probId);
          this.router.navigate(['/app/company-admin/listing-page/probationary-meeting']);
        }
        else return this.coreService.notify("Unsuccessful", res.message, 0);
      },
      err => {
        this.isTraCreateProcessing=false;
        if (err.code == 400) {
          return this.coreService.notify("Unsuccessful", err.message, 0);
        }
        this.coreService.notify("Unsuccessful", "Problem while performing action.", 0)
      }
    );
  }

  //TERMINATION
  onTerminate(probId,action,userId){
    console.log(probId)
    console.log(action)
    console.log(userId)
    let info = { probationaryMeetingID: probId, type: 'Terminate', userID: userId, subject: "", body: '' }
      let dialogRef = this.dialog.open(ActionsOnEmployeePopupComponent);
      dialogRef.componentInstance.actionInfo = info;
      dialogRef.afterClosed().subscribe(r => {
        if (r) {
          this.saveProbationAction(probId,action);
        }
      })
  }

  //ON EDIT
  onEditNotes(){
    this.onEdit=true;
    this.pipManagerNote.managerTraNote=this.probationDetails.note;
  }

  //ON CANCEL
  onCancel(){
    this.onEdit=false;
  }



}
