import { Component, OnInit } from '@angular/core';
import { MdDialogRef, MdDialog, MdSnackBar } from '@angular/material';
import { APPCONFIG } from '../../../config';
import { EmployeeListingPopupComponent } from '../employee-listing-popup/employee-listing-popup.component';
import {  CompanyModuleListingService } from '../company-module-listing.service';
import { CoreService } from '../../../shared/services/core.service';
import { Router, ActivatedRoute } from '@angular/router';
import {  WarningReasonPopupComponent } from './warning-reason-popup/warning-reason-popup.component';
import { ActionsOnEmployeePopupComponent } from '../../../shared/components/actions-on-employee-popup/actions-on-employee-popup.component';

@Component({
  selector: 'app-warning-list',
  templateUrl: './warning-list.component.html',
  styleUrls: ['./warning-list.component.scss']
})
export class WarningListComponent implements OnInit {
public toggleFilter: boolean;
public warningList = [];
public employmentList =[];
public isProcessing : boolean;
public filter = {
  "filter":{
    "toUserIDs":{},
    "byUserIDs":{}
  },
  "search" : ""
};

  constructor(public dialog: MdDialog , 
        public companyModuleListingService: CompanyModuleListingService
        ,public coreService: CoreService , public router:Router ) { }

  ngOnInit() {
      APPCONFIG.heading = "Warning listing";
    this.toggleFilter = true;
     this.getWarningList();
  }
   onPopUp(c) {
     let response : any;
     let dialogRef = this.dialog.open(EmployeeListingPopupComponent);
     dialogRef.componentInstance.moduleName = 'warning';
     dialogRef.afterClosed().subscribe(res=>{
      this.sendWarninglatter(res.userId);
     });  
  }
  onPopUp1(r){
    let dialogRef = this.dialog.open(WarningReasonPopupComponent);
    dialogRef.componentInstance.moduleName = 'warning';
    dialogRef.componentInstance.reason = r;
    dialogRef.afterClosed().subscribe(res=>{
      console.log(res)
    });
  }


  sendWarninglatter(response){
    let info = {type: 'Warning',userID:response,subject:"", body:""}
    let dialog_Ref = this.dialog.open(ActionsOnEmployeePopupComponent);
     dialog_Ref.componentInstance.actionInfo = info;
     dialog_Ref.afterClosed().subscribe(result=>{
       this.getWarningList();
     });
  }

  searchList(v){
     this.isProcessing = true;
     this.filter.search = encodeURIComponent(v);
     this.getWarningList();
  }

  getWarningList(){
    this.isProcessing = true;
   this.companyModuleListingService.getWarningList(JSON.stringify(this.filter)).subscribe(
      res => {
        this.isProcessing = false;
        if (res.code == 200) {
          this.warningList = res.data;
        }
        else return this.coreService.notify("Unsuccessful", res.message, 0);
      },
      err => {
        this.isProcessing = false;
        if (err.code == 400) {
          return this.coreService.notify("Unsuccessful", err.message, 0);
        }
        this.coreService.notify("Unsuccessful", "Error while getting data.", 0)
      }
    );
  }
}
