import { Component, OnInit } from '@angular/core';
import { MdDialogRef, MdDialog, MdSnackBar } from '@angular/material';
// import { DevelopmentPlanService } from '../../development-plan/development-plan.service';
import { Router, ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-warning-reason-popup',
  templateUrl: './warning-reason-popup.component.html'
})
export class WarningReasonPopupComponent implements OnInit {
   public moduleName: any;
   public reason : string

  constructor() { }

  ngOnInit() {
  }

}
