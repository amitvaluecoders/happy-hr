import { Injectable } from '@angular/core';
import { RestfulLaravelService } from "../../shared/services/restful-laravel.service";

@Injectable()
export class CompanyModuleListingService {

  constructor(private restfulService: RestfulLaravelService) { }

  public tempAssetForOperation : any;

  getAllEmployee() {
    return this.restfulService.get('show-all-employee');
  }
  getLeaveTypes() {
    return this.restfulService.get('get-leave-type');
}

  getAssestApprovalList(params){
    return this.restfulService.get(`get-assets-approval-listing/${encodeURIComponent(params)}`);
  }

  getAssestList(params){
    return this.restfulService.get(`get-assets-listing/${encodeURIComponent(params)}`);
  }

  deleteAssestStatus(id){
    return this.restfulService.delete(`delete-asset/${id}`)
  }

  getAssestDetail(params){
    return this.restfulService.get(`get-assets-detail/${encodeURIComponent(params)}`);
  }

  getAssestApprovalDetail(params){
    return this.restfulService.get(`get-assets-approval-detail/${encodeURIComponent(params)}`);
  }

  

  getAssestApproveDetail(params){
    return this.restfulService.get(`save-update-asset-approval/${encodeURIComponent(params)}`);
  }

  getWarningList(params){
    return this.restfulService.get(`warning-action-list/${encodeURIComponent(params)}`);
  }
  
  saveUpdateAsset(formData){
     return this.restfulService.post('save-update-asset', formData);
  }

  changeNewEmployeeStatus(formData){
     return this.restfulService.post('employee-invited-reject-by-company-admin', formData);
  }

  saveUpdateAssetAprrove(formData){
     return this.restfulService.post('save-update-asset-approval', formData);
  }

  getNewEmployeeList(){
      return this.restfulService.get(`get-new-employee-approval-listing`);
  }

  getTrophyCertificatesList(){
    return this.restfulService.get(`get-trophy-winner-list`);
  } 

  //GET OFF BOARD LISTING
  getOffBoardList(){
    return this.restfulService.get(`get-off-boarding-listing`);
  }
  
  getKpiListing(){
    return this.restfulService.get(`kpi-listing`);    
  }

  sendReminderforKpi(reqBody){
    return this.restfulService.put(`kpi-send-reminder`,reqBody);    
  }
  
  getEmployees() {
    return this.restfulService.get('show-all-employee');
  }

  uplodDocument(formData) {
    return this.restfulService.post('upload-multiple-files', formData);
  }

  reportIncident(reqBody) {
    return this.restfulService.post('employee/ohs-report', reqBody);
  }
  getEmployeeContractList(reqBody){
    return this.restfulService.get(`list-employeement-contract/${encodeURIComponent(reqBody)}`);
  }
  acceptOrReject(reqBody):any{
    return this.restfulService.post('employee/employee-accept-reject-contract',reqBody)
  }
  acceptContract(reqBody):any{
    return this.restfulService.post(`employee/employee-accept-contract`,reqBody)
  }
  getAllContracts(reqBody):any{
    return this.restfulService.get(`employee/employee-contract-list/${reqBody}`)
   }
   getFilters() {
    return this.restfulService.get('employee-listing-filter-data');
  }
 
   // getContractDetails(reqBody):any{
   //  return this.restfulWebService.get(`employee/employee-contract-detail/${reqBody}`)
   // }
    getContractDetails(reqBody):any{
     // if(reqBody.isHHRContract) return this.restfulWebService.get(`get-contract-detail/${reqBody.id}`)
     // else  
     return this.restfulService.get(`employee/employee-contract-detail/${reqBody.id}/${reqBody.employeeCompanyContractID}`)
   }
   // getContractDetailsCa(reqBody):any{
   //   // if(reqBody.isHHRContract) return this.restfulWebService.get(`get-contract-detail/${reqBody.id}`)
   //   // else  
   //   return this.restfulWebService.get(`employee/employee-contract-detail/${reqBody.id}`)
   // }
   getContractVersionList(contractType,reqId){
    let url='';
    if(contractType=='ce') url=`employee/employee-contract-version-detail/${reqId}`;
    if(contractType=='ca') url=`get-contract-version-detail/${reqId}`;
       return this.restfulService.get(url)
  }
  getContractComparisonDetails(id):any{
    return this.restfulService.get(`employee/compare-contract-version/${id}`)
  }
 
 
  
}
