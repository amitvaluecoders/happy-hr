import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { APPCONFIG } from '../../../config';
import { MdDialogRef, MdDialog, MdSnackBar } from '@angular/material';
import { EmployeeListingPopupComponent } from '../employee-listing-popup/employee-listing-popup.component';
import { CompanyModuleListingService } from "../company-module-listing.service";
import { PerformanceAppraisalService } from "../../performance-appraisal/performance-appraisal.service";
import { CoreService } from '../../../shared/services/core.service';
import { ResponseNoteComponent } from '../../../shared/components/response-note/response-note.component';
import * as moment from 'moment';

@Component({
  selector: 'app-performance-indicator-listing',
  templateUrl: './performance-indicator-listing.component.html',
  styleUrls: ['./performance-indicator-listing.component.scss']
})
export class PerformanceIndicatorListingComponent implements OnInit {
  public toggleFilter = true;
  public empSearch = {};
  public paList = [];
  public employeeList = [];
  public isProcessing = false;
  public isChecked = false;
  public isActionProcessing = false;
  public filter = {
    "search": "", "filter": {
      "status": { "Awaiting Employee Acceptance": false, "Complete": false },
      "userID": [], "managerID": []
    }
  };

  constructor(public dialog: MdDialog, public paService: PerformanceAppraisalService,
    public companyService: CompanyModuleListingService, public coreService: CoreService, public router: Router) { }

  ngOnInit() {
    APPCONFIG.heading = "Performance indicator listing";

    this.getEmployee();
    // this.getPaList();
  }

  onViewKPI(k) {
    let dialogRef = this.dialog.open(EmployeeKPIwithNote);
    dialogRef.componentInstance.kpi = k;
    dialogRef.afterClosed().subscribe(result => {
      if (result) {

      }
    });
  }

  sendReminder(user,k){

   k.isActionProcessing = true;
    let reqBody = {
      "companyPositionPerformanceIndicatorID":k.companyPositionPerformanceIndicatorID,
      "employeeUserID":user.employee.userID  
    }
    this.companyService.sendReminderforKpi(reqBody).subscribe(
      res => {
        if (res.code == 200) this.coreService.notify("Successful", res.message, 1);
        else return this.coreService.notify("Unsuccessful", res.message, 0);
        k.isActionProcessing = false;
      },
      err => {
        k.isActionProcessing = false;
      }
    );
  }

  ondirectAssessment(user,k){
      let dialogRef = this.dialog.open(ResponseNoteComponent);  
      let tempResponse={
        url:"pi-assessment",
        note:{
          companyPositionPerformanceIndicatorID:k.companyPositionPerformanceIndicatorID,
          employeeUserID:user.employee.userID,
          employeePerformanceIndicatorID:k.employeePerformanceIndicatorID,
          responseNote:""
        }
      }
        dialogRef.componentInstance.noteDetails=tempResponse;
        dialogRef.afterClosed().subscribe(result => {
         if(result) k.notes.push({note:APPCONFIG.tempNote})
        });   
  }

  getEmployee() {
    this.isProcessing = true;
    this.companyService.getKpiListing().subscribe(
      res => {
        this.isProcessing = false;
        if (res.code == 200) {
          if (res.data instanceof Array) {
            this.employeeList = res.data;

            res.data.forEach(item=>{  
                item.kpis['valid']= false;                                           
                    item.kpis.Current.find(item2=>{
                      if(item2.lapseDuration==true){
                        item.kpis['valid']= true;
                        } 
                    })
                  
                
              })


            this.employeeList = this.employeeList.filter(item=>{
              item['isCollapsed'] = true;
              return item;
            })
          }
        }
        else return this.coreService.notify("Unsuccessful", res.message, 0);
      },
      err => {
        this.isProcessing = false;
      }
    );
  } 
}


@Component({
  selector: 'app-employee-kpi-with-note',
  templateUrl: './kpi-with-note-popup.html',
  styles: [``]
})
export class EmployeeKPIwithNote implements OnInit {
  public kpi:any;
  constructor() { }
  ngOnInit() {}
}
