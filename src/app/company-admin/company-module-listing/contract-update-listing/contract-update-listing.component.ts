import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { APPCONFIG } from '../../../config';
import { CoreService } from '../../../shared/services/core.service';
import {CompanyModuleListingService} from "../../company-module-listing/company-module-listing.service";
import { ConfirmService } from '../../../shared/services/confirm.service';
import * as moment from 'moment';


@Component({
  selector: 'app-contract-update-listing',
  templateUrl: './contract-update-listing.component.html',
  styleUrls: ['./contract-update-listing.component.scss']
})
export class ContractUpdateListingComponent implements OnInit {
  isProcessing = false;
  unsubscriber:any;
  public toggleFilter = true;
  type='';
  contract=[];
  public isChecked = false;
  public employeeList = [];
  isDeleteProcessing=false;
  public empSearch = {};
  // requestBody ={
  //   search:''
  // }
  public filter = {
    "userID": []
  };

  constructor(public companyService: CompanyModuleListingService, public coreService: CoreService, 
    private router: Router,private route: ActivatedRoute, public confirmService: ConfirmService, public viewContainerRef:ViewContainerRef) { }

    ngOnInit() {
      APPCONFIG.heading = "Pending Employee Contracts";
      this.getEmployee();
      this.getContractList();
    }
    
    // onSearch(){
    //   this.unsubscriber.unsubscribe();
    //   //this.requestBody.search=encodeURIComponent(this.filter.search);
      
    //   this.getContractList();
    // }
    formateDate(v){
      return moment(v).isValid()? moment(v).format('DD MMM YYYY') : 'Not specified';
  }
    getEmployee() {
      this.companyService.getEmployees().subscribe(
        res => {
          if (res.code == 200) {
            if (res.data instanceof Array) {
              this.employeeList = res.data;
            }
          }
          else return this.coreService.notify("Unsuccessful", res.message, 0);
        },
        err => {
          if (err.code == 400) {
            return this.coreService.notify("Unsuccessful", err.message, 0);
          }
          this.coreService.notify("Unsuccessful", "Error while getting data.", 0)
        }
      );
    }
  
    getContractList() {
      this.isProcessing = true;
      this.unsubscriber = this.companyService.getEmployeeContractList(JSON.stringify(this.filter)).subscribe(
        d => {
          if (d.code == "200") {
            this.contract=d.data;
            this.isProcessing = false;
            this.toggleFilter = true;
          }
          else this.coreService.notify("Unsuccessful", d.message, 0);
        },
        error => this.coreService.notify("Unsuccessful", error.message, 0),
        () => { }
      )
    }
    onCheckEmp(event, id) {
      this.isChecked = true;
      let ind = this.filter.userID.indexOf(id);
  
      if (event.target.checked) {
        (ind >= 0) ? null : this.filter.userID.push(id);
      } else {
        (ind >= 0) ? this.filter.userID.splice(ind, 1) : null;
      }
    }

    goToDetails(c){
        this.router.navigate([`/app/company-manager/listing-page/contract-detail/${c.companyContractID}/${c.employeeCompanyContractID}`]);
    }
  
    // onViewDoc(link){
    //   console.log("url",link);
    //   window.open(link);
    // }
  
    // onArchieve(data, index){
    //   let item={module:''};
    //   item.module='mandatoryDocument';
    //         data["isDeleteProcessing"] = true;
    //         data["type"]='archive';
    //         this.companyService.archiveData(item, data).subscribe(
    //           res => {
    //             data["isDeleteProcessing"]= false;
    //             if (res.code == 200) {
    //               this.docs.splice(index, 1);
    //               this.coreService.notify("Successful", res.message, 1);
    //             }
    //             else return this.coreService.notify("Unsuccessful", res.message, 0);
    //           },
    //           error => {
    //             data["isDeleteProcessing"]= false;
    //             this.coreService.notify("Unsuccessful", error.message, 0);
    //           }
    //         )
          
    // }
  
    //   onMarkCurrent(data,index){
    //   let item={module:''};
    //   item.module='mandatoryDocument';
    //         data["isDeleteProcessing"] = true;
    //         data["type"]='archive';
    //         this.companyService.onMarkCurrent(item, data).subscribe(
    //           res => {
    //             data["isDeleteProcessing"]= false;
    //             if (res.code == 200) {
    //               this.docs.splice(index, 1);
    //               this.coreService.notify("Successful", res.message, 1);
    //             }
    //             else return this.coreService.notify("Unsuccessful", res.message, 0);
    //           },
    //           error => {
    //             data["isDeleteProcessing"]= false;
    //             this.coreService.notify("Unsuccessful", error.message, 0);
    //           }
    //         )
          
    // }
  
    // onDelete(data, index) {
    //   let item={title:data.title,module:''};
    //   item.module='mandatoryDocument';
    //   this.confirmService.confirm("Delete confirmation", "Do you want to delete this " + item.title + " ?", this.viewContainerRef).subscribe(
    //     confirm => {
    //       if (confirm) {
    //         data["isDeleteProcessing"] = true;
    //         this.companyService.deleteData(item, data).subscribe(
    //           res => {
    //             data["isDeleteProcessing"]= false;
    //             if (res.code == 200) {
    //               this.docs.splice(index, 1);
    //               this.coreService.notify("Successful", res.message, 1);
    //             }
    //             else return this.coreService.notify("Unsuccessful", res.message, 0);
    //           },
    //           error => {
    //             data["isDeleteProcessing"]= false;
    //             this.coreService.notify("Unsuccessful", error.message, 0);
    //           }
    //         )
    //       }
    //     }
    //   )
    // }
  
  }
  
