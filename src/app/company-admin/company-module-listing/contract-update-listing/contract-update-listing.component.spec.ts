import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContractUpdateListingComponent } from './contract-update-listing.component';

describe('ContractUpdateListingComponent', () => {
  let component: ContractUpdateListingComponent;
  let fixture: ComponentFixture<ContractUpdateListingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContractUpdateListingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContractUpdateListingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
