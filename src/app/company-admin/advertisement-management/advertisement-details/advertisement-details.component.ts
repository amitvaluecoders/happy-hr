import { Component, OnInit } from '@angular/core';
import { Router,ActivatedRoute} from '@angular/router';
import { MdDialog, MdDialogRef,  } from '@angular/material';
import { PreviewAdvertisementPopupComponent } from '../preview-advertisement-popup/preview-advertisement-popup.component';
import { APPCONFIG } from '../../../config';
import { AdvertisementManagementService } from '../advertisement-management.service';
import { CoreService } from '../../../shared/services/core.service';

@Component({
  selector: 'app-advertisement-details',
  templateUrl: './advertisement-details.component.html',
  styleUrls: ['./advertisement-details.component.scss']
})
export class AdvertisementDetailsComponent implements OnInit {
  public isProcessing=false;
  public advertisement = {
    basicInfo:{},
    interviewQuestions:{},
    refrenceCheckQuestions:[]
  }
  constructor( public dialog:MdDialog,private router:Router,public activatedRoute:ActivatedRoute,
                private advertisementManagementService:AdvertisementManagementService,
                public coreService:CoreService ) { }


  ngOnInit() {
    APPCONFIG.heading="Advertisement Details";    
    this.getBasicDeatils();
    this.getInterviewQuestionDeatils();
    this.getRefrenceCheckDeatils();
  }



  getBasicDeatils(){
    this.isProcessing=true;
    this.advertisementManagementService.getAdvertisementDeatils(this.activatedRoute.snapshot.params['id']).subscribe(
      d => {
              if (d.code == "200") {
                this.advertisement.basicInfo = d.data;
                this.isProcessing=false;                                                                                                                                                                                                                                                                                                                                         
              }
              else this.coreService.notify("Unsuccessful", d.message, 0);
          },
          error => {
            this.coreService.notify("Unsuccessful", error.message, 0); 
          }    
      )
  }

    getInterviewQuestionDeatils(){
    this.isProcessing=true;
    this.advertisementManagementService.getAdvertisementInterviewDeatils( this.activatedRoute.snapshot.params['id']).subscribe(
      d => {
              if (d.code == "200") {
                this.advertisement.interviewQuestions = d.data;
                this.isProcessing=false;                                                                                                                                                                                                                                                                                                                                               
              }
              else this.coreService.notify("Unsuccessful", d.message, 0);
          },
          error => {
            this.coreService.notify("Unsuccessful", error.message, 0); 
          }    
      )
  }

    getRefrenceCheckDeatils(){
    this.isProcessing=true;
    this.advertisementManagementService.getAdvertisementRefrenceDeatils(this.activatedRoute.snapshot.params['id']).subscribe(
      d => {
              if (d.code == "200") {
                this.advertisement.refrenceCheckQuestions = d.data;
                this.isProcessing=false;                                                                                                                                                                                                                                                                                                                                               
              }
              else this.coreService.notify("Unsuccessful", d.message, 0);
          },
          error => {
            this.coreService.notify("Unsuccessful", error.message, 0); 
          }    
      )
  }

  onBasicInfoEdit(){
    this.router.navigate([`/app/company-admin/advertisement/edit/${this.activatedRoute.snapshot.params['id']}`]);  
  }

  onInterviewEdit(){
    this.router.navigate([`/app/company-admin/advertisement/edit/interview-questions/${this.activatedRoute.snapshot.params['id']}`]);      
  }
  
  onRefrenceCheckEdit(){
    this.router.navigate([`/app/company-admin/advertisement/edit/refrence-chk-questions/${this.activatedRoute.snapshot.params['id']}`]);  
    
  }

}
