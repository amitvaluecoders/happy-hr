import { Component, OnInit,ViewContainerRef } from '@angular/core';
import { APPCONFIG } from '../../../config';
import { Router,ActivatedRoute} from '@angular/router';
import { MdDialog, MdDialogRef,  } from '@angular/material';
import { AdvertisementManagementService } from '../advertisement-management.service';
import { CoreService } from '../../../shared/services/core.service';
import { ConfirmService } from '../../../shared/services/confirm.service';

@Component({
  selector: 'app-reference-check-form',
  templateUrl: './reference-check-form.component.html',
  styleUrls: ['./reference-check-form.component.scss']
})
export class ReferenceCheckFormComponent implements OnInit {
public progress;
public editToggle=[];
public isCreateProcessing=false;
public temp={};
isButtonClicked=false;
public isProcessing=false;
public referenceCheckQuestions:any;
  constructor( public dialog:MdDialog,private router:Router,public activatedRoute:ActivatedRoute,public viewContainerRef:ViewContainerRef,
                private advertisementManagementService:AdvertisementManagementService,public confirmService:ConfirmService,
                public coreService:CoreService ) { }

  ngOnInit() {
    if(!this.advertisementManagementService.adsFormData)this.router.navigate(["/app/company-admin/advertisement/create"]);
   else if(!this.advertisementManagementService.interviewQuestions)this.router.navigate(["/app/company-admin/advertisement/interview-question"]);
    else this.getReferenceCheckQuestions();
    this.progressBarSteps();
    APPCONFIG.heading="Reference check questions";
  }

  progressBarSteps() {
      this.progress={
          progress_percent_value:100, //progress percent
          total_steps:4,
          current_step:4,
          circle_container_width:25, //circle container width in percent
          steps:[
              // {num:1,data:"Select Role"},
               {num:1,data:"Create advertisement"},
               {num:2,data:"Preview"},
               {num:3,data:"Interview question"},
               {num:4,data:"Reference check question", active:true}
          ]  
       }         
  }

  getReferenceCheckQuestions(){
        this.isProcessing=true;
    this.advertisementManagementService.getReferenceCheckQuestions().subscribe(
      d => {
              if (d.code == "200") {
                 this.isProcessing = false;
                 this.referenceCheckQuestions=d.data;
                 d.data.filter(i=>{
                   console.log("i",i)
                   this.temp[i.heading]="";
                 })
              }
              else this.coreService.notify("Unsuccessful", d.message, 0);
          },
          error => {
            this.coreService.notify("Unsuccessful", error.message, 0); 
          }    
      )
  }

    onAdd(key,objkey){
      this.isButtonClicked=true;
      if(this.temp[objkey]){
      this.referenceCheckQuestions[key].data.push({title:this.temp[objkey]})
      this.temp[objkey]='';
       this.isButtonClicked=false;
    }
  }

  onsubmit(){
    this.isCreateProcessing=true;
    let reqbody={
      advertisement:this.advertisementManagementService.adsFormData,
      interviewQuestions:this.advertisementManagementService.interviewQuestions,
      referenceQuestion:{
        data:this.referenceCheckQuestions
      }
    }

     this.advertisementManagementService.addAdvertisement(reqbody).subscribe(
      d => {
              if (d.code == "200") {
                 this.isCreateProcessing = false; 
                 this.router.navigate(["/app/company-admin/advertisement"]);
              }
              else {
                 this.isCreateProcessing = false; 
               this.coreService.notify("Unsuccessful", d.message, 0); 
              }
          },
          error => {
                 this.isCreateProcessing = false;             
            this.coreService.notify("Unsuccessful", error.message, 0); 
          }    
      )

  }

}
