import { Component, OnInit } from '@angular/core';
import { APPCONFIG } from '../../../config';
import { AdvertisementManagementService } from '../advertisement-management.service';
import { Router} from '@angular/router';
import { CoreService } from '../../../shared/services/core.service';

@Component({
  selector: 'app-preview-advertisement-popup',
  templateUrl: './preview-advertisement-popup.component.html',
  styleUrls: ['./preview-advertisement-popup.component.scss']
})
export class PreviewAdvertisementPopupComponent implements OnInit {
 public progress;
 public isProcessing=false;
  adsForm:any;
  dropDownData:any;
  constructor(private router:Router,private advertisementManagementService:AdvertisementManagementService, public coreService:CoreService ) { }

  ngOnInit() {
    APPCONFIG.heading="Preview advertisement";
    this.progressBarSteps();
    if(this.advertisementManagementService.adsFormData){
     this.adsForm=this.advertisementManagementService.adsFormData;
      this.dropDownData=this.advertisementManagementService.dropDownData;
    }
    else this.router.navigate(["/app/company-admin/advertisement/create"]);   
    console.log("adddfro",this.adsForm);
  }
    progressBarSteps() {
      this.progress={
          progress_percent_value:50, //progress percent
          total_steps:4,
          current_step:2,
          circle_container_width:25, //circle container width in percent
          steps:[
               {num:1,data:"Create advertisement"},
               {num:2,data:"Preview", active:true},
               {num:3,data:"Interview question"},
               {num:4,data:"Reference check question"}
          ]  
       }         
  }

  onSubmit(){
    this.isProcessing=true;
    this.router.navigate(["/app/company-admin/advertisement/interview-question"]); 
    // this.advertisementManagementService.addAdvertisement(this.adsForm).subscribe(
    //   d => {
    //           if (d.code == "200") {
    //              this.isProcessing = false;
    //               this.router.navigate(["/app/company-admin/advertisement"]);                    
    //              this.coreService.notify("Successful", d.message, 1);
    //           }
    //           else{
    //             this.coreService.notify("Unsuccessful", d.message, 0);
    //             this.isProcessing = false;
    //           } 
    //       },
    //       error => {
    //         this.isProcessing = false;
    //         this.coreService.notify("Unsuccessful", error.message, 0); 
    //       }    
    //   )
  }

}
