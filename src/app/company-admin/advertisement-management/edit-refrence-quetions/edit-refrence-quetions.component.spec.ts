import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditRefrenceQuetionsComponent } from './edit-refrence-quetions.component';

describe('EditRefrenceQuetionsComponent', () => {
  let component: EditRefrenceQuetionsComponent;
  let fixture: ComponentFixture<EditRefrenceQuetionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditRefrenceQuetionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditRefrenceQuetionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
