import { Component, OnInit,ViewContainerRef } from '@angular/core';
import { APPCONFIG } from '../../../config';
import { Router,ActivatedRoute} from '@angular/router';
import { MdDialog, MdDialogRef,  } from '@angular/material';
import { AdvertisementManagementService } from '../advertisement-management.service';
import { CoreService } from '../../../shared/services/core.service';
import { ConfirmService } from '../../../shared/services/confirm.service';

@Component({
  selector: 'app-edit-refrence-quetions',
  templateUrl: './edit-refrence-quetions.component.html',
  styleUrls: ['./edit-refrence-quetions.component.scss']
})
export class EditRefrenceQuetionsComponent implements OnInit {

 public progress;
public editToggle=[];
public isCreateProcessing=false;
public temp={};
isButtonClicked=false;
public isProcessing=false;
public referenceCheckQuestions:any;
  constructor( public dialog:MdDialog,private router:Router,public activatedRoute:ActivatedRoute,public viewContainerRef:ViewContainerRef,
                private advertisementManagementService:AdvertisementManagementService,public confirmService:ConfirmService,
                public coreService:CoreService ) { }

  ngOnInit() {
     this.getReferenceCheckQuestions();
    this.progressBarSteps();
    APPCONFIG.heading="Update refrence check questions";
  }

  progressBarSteps() {
      this.progress={
          progress_percent_value:100, //progress percent
          total_steps:4,
          current_step:4,
          circle_container_width:25, //circle container width in percent
          steps:[
              // {num:1,data:"Select Role"},
               {num:1,data:"Create advertisement"},
               {num:2,data:"Preview"},
               {num:3,data:"Interview question"},
               {num:4,data:"Referece check question", active:true}
          ]  
       }         
  }

  getReferenceCheckQuestions(){
        this.isProcessing=true;
    this.advertisementManagementService.getAdvertisementRefrenceDeatils(this.activatedRoute.snapshot.params['id']).subscribe(
      d => {
              if (d.code == "200") {
                 this.isProcessing = false;
                 this.referenceCheckQuestions=d.data;
                 d.data.filter(i=>{
                   this.temp[i.heading]="";
                 })
              }
              else this.coreService.notify("Unsuccessful", d.message, 0);
          },
          error => {
            this.coreService.notify("Unsuccessful", error.message, 0); 
          }    
      )
  }

    onAdd(key,objkey){
      this.isButtonClicked=true;
      if(this.temp[objkey]){
      this.referenceCheckQuestions[key].data.push({title:this.temp[objkey]})
      this.temp[objkey]='';
       this.isButtonClicked=false;
    }
  }

  onsubmit(){
    this.isCreateProcessing=true;
    let reqbody={
     advertisementID:this.activatedRoute.snapshot.params['id'],
        data:this.referenceCheckQuestions 
    }
     this.advertisementManagementService.updateRefrencechkQuestions(reqbody).subscribe(
      d => {
              if (d.code == "200") {
                 this.isCreateProcessing = false; 
               this.coreService.notify("Successful", d.message, 1);                  
                this.router.navigate([`/app/company-admin/advertisement/view/${ this.activatedRoute.snapshot.params['id']}`]);
              }
              else {
                 this.isCreateProcessing = false; 
               this.coreService.notify("Unsuccessful", d.message, 0); 
              }
          },
          error => {
                 this.isCreateProcessing = false;             
            this.coreService.notify("Unsuccessful", error.message, 0); 
          }    
      )

  }

}
