import { Component, OnInit,ViewContainerRef } from '@angular/core';
import { APPCONFIG } from '../../../config';
import { Router,ActivatedRoute} from '@angular/router';
import { MdDialog, MdDialogRef,  } from '@angular/material';
import { AdvertisementManagementService } from '../advertisement-management.service';
import { CoreService } from '../../../shared/services/core.service';
import { ConfirmService } from '../../../shared/services/confirm.service';


@Component({
  selector: 'app-interview-question',
  templateUrl: './interview-question.component.html',
  styleUrls: ['./interview-question.component.scss']
})
export class InterviewQuestionComponent implements OnInit {
public progress;
public editToggle=[];
public isProcessing=false;
public interviewQuestions=[];
public temp={
  Attitude:'',
  Behavior:'',
  CoreCompetency:'',
  Intergrity:''
}
public isButtonClicked=false;

    constructor( public dialog:MdDialog,private router:Router,public activatedRoute:ActivatedRoute,public viewContainerRef:ViewContainerRef,
                private advertisementManagementService:AdvertisementManagementService,public confirmService:ConfirmService,
                public coreService:CoreService ) { }

  ngOnInit() {
    this.progressBarSteps();
    this.isProcessing=true;
    APPCONFIG.heading="Interview question";
     if(!this.advertisementManagementService.adsFormData)this.router.navigate(["/app/company-admin/advertisement/create"]);
    else this.getInterviewQuestions();
  }

  progressBarSteps() {
      this.progress={
          progress_percent_value:75, //progress percent
          total_steps:4,
          current_step:3,
          circle_container_width:25, //circle container width in percent
          steps:[
              // {num:1,data:"Select Role"},
               {num:1,data:"Create advertisement"},
               {num:2,data:"Preview"},
               {num:3,data:"Interview question", active:true},
               {num:4,data:"Reference check question"}
          ]  
       }         
  }

  getInterviewQuestions(){
    this.isProcessing=true;
    this.advertisementManagementService.getInterviewQuestions().subscribe(
      d => {
              if (d.code == "200") {
                 this.isProcessing = false;
                 this.interviewQuestions=d.data;
                 this.interviewQuestions['Attitude'].filter(d=> {d['isActionProcessing']=false})
                 this.interviewQuestions['Behavior'].filter(d=> {d['isActionProcessing']=false})
                 this.interviewQuestions['CoreCompetency'].filter(d=> {d['isActionProcessing']=false})
                 this.interviewQuestions['Intergrity'].filter(d=> {d['isActionProcessing']=false})
              }
              else this.coreService.notify("Unsuccessful", d.message, 0);
          },
          error => {
            this.coreService.notify("Unsuccessful", error.message, 0); 
          }    
      )
  }

  onDeleteQuestion(key, serialNumber) {
    this.confirmService.confirm(this.confirmService.tilte, this.confirmService.message, this.viewContainerRef)
      .subscribe(res => {
        let result = res;
        if (result) {
          let i =0;
          this.interviewQuestions[key] = this.interviewQuestions[key].filter(d => {
            if (d['serialNumber'] == serialNumber) return null
            else{
              i++;
              d['serialNumber']=i;
              return d;
            } 
          }
          )
        }
      })
  }

    onAdd(key){
      this.isButtonClicked=true;
      if(this.temp[key]){
      this.interviewQuestions[key].push({interviewQuestion:this.temp[key],serialNumber:this.interviewQuestions[key].length+1})
      this.temp[key]='';
       this.isButtonClicked=false;
    }
  }

  onNext(){
    this.advertisementManagementService.interviewQuestions=this.interviewQuestions;
    this.router.navigate(["/app/company-admin/advertisement/reference-check"]);
  }

}
