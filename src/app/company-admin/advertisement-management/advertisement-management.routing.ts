import {  ModuleWithProviders  } from '@angular/core';
import {  RouterModule,Routes  } from '@angular/router';
import { AdvertisementListingComponent } from './advertisement-listing/advertisement-listing.component';
import { CreateAdvertisementComponent } from './create-advertisement/create-advertisement.component';
import { PreviewAdvertisementPopupComponent } from './preview-advertisement-popup/preview-advertisement-popup.component';
import { InterviewQuestionComponent } from './interview-question/interview-question.component';
import { ReferenceCheckFormComponent } from './reference-check-form/reference-check-form.component';
import { EditAdvertisementComponent } from './edit-advertisement/edit-advertisement.component';
import { AdvertisementDetailsComponent } from './advertisement-details/advertisement-details.component';
import { EditInterviewQuetionsComponent } from './edit-interview-quetions/edit-interview-quetions.component';
import { EditRefrenceQuetionsComponent } from './edit-refrence-quetions/edit-refrence-quetions.component';

export const AdvertisementManagementPagesRoutes: Routes = [
    { path: '', component:AdvertisementListingComponent},
    { path:'create', component:CreateAdvertisementComponent},
    { path:'create/:re', component:CreateAdvertisementComponent},
    { path:'preview', component:PreviewAdvertisementPopupComponent},
    { path:'interview-question', component:InterviewQuestionComponent},
    { path:'reference-check', component:ReferenceCheckFormComponent},
    { path:'edit/:id', component:EditAdvertisementComponent},
    { path:'view/:id', component:AdvertisementDetailsComponent},
    { path:'edit/interview-questions/:id', component:EditInterviewQuetionsComponent},
    { path:'edit/refrence-chk-questions/:id', component:EditRefrenceQuetionsComponent},
];

export const AdvertisementManagementRoutingModule = RouterModule.forChild(AdvertisementManagementPagesRoutes);
