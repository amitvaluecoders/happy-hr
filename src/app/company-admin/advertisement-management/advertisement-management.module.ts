import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdvertisementManagementRoutingModule } from './advertisement-management.routing';
import { AdvertisementManagementService } from './advertisement-management.service';
import { AdvertisementListingComponent } from './advertisement-listing/advertisement-listing.component';
import { DataTableModule } from 'angular2-datatable';
import { SharedModule } from '../../shared/shared.module';
import { FormsModule } from '@angular/forms';
import { MaterialModule } from '@angular/material';
import { AngularMultiSelectModule } from 'angular2-multiselect-checkbox-dropdown/angular2-multiselect-dropdown';
import { CreateAdvertisementComponent } from './create-advertisement/create-advertisement.component';
import { PreviewAdvertisementPopupComponent } from './preview-advertisement-popup/preview-advertisement-popup.component';
import { InterviewQuestionComponent } from './interview-question/interview-question.component';
import { ReferenceCheckFormComponent } from './reference-check-form/reference-check-form.component';
import { EditAdvertisementComponent } from './edit-advertisement/edit-advertisement.component';
import { AdvertisementManagementGuardService } from './advertisement-management-guard.service'; 
import { QuillModule } from 'ngx-quill';
import { AdvertisementDetailsComponent } from './advertisement-details/advertisement-details.component';
import { EditInterviewQuetionsComponent } from './edit-interview-quetions/edit-interview-quetions.component';
import { EditRefrenceQuetionsComponent } from './edit-refrence-quetions/edit-refrence-quetions.component';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    MaterialModule,
    AngularMultiSelectModule,
    FormsModule,
    DataTableModule,
    QuillModule,
    AdvertisementManagementRoutingModule
  ],
  declarations: [AdvertisementListingComponent, CreateAdvertisementComponent, PreviewAdvertisementPopupComponent, InterviewQuestionComponent, ReferenceCheckFormComponent, EditAdvertisementComponent, AdvertisementDetailsComponent, EditInterviewQuetionsComponent, EditRefrenceQuetionsComponent],
  providers:[AdvertisementManagementService, AdvertisementManagementGuardService],
  entryComponents:[PreviewAdvertisementPopupComponent]
})
export class AdvertisementManagementModule { }
