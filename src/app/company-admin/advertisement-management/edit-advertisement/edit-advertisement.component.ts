import { Component, OnInit } from '@angular/core';
import { Router,ActivatedRoute} from '@angular/router';
import { MdDialog, MdDialogRef,  } from '@angular/material';
import { PreviewAdvertisementPopupComponent } from '../preview-advertisement-popup/preview-advertisement-popup.component';
import { APPCONFIG } from '../../../config';
import { AdvertisementManagementService } from '../advertisement-management.service';
import { CoreService } from '../../../shared/services/core.service';

@Component({
  selector: 'app-edit-advertisement',
  templateUrl: './edit-advertisement.component.html',
  styleUrls: ['./edit-advertisement.component.scss']
})
export class EditAdvertisementComponent implements OnInit {

  public isProcessing=false;
  public progress;
  public isDataProcessing=false;
  public isFormDataProcessing=false;
  public isUpdateProcessing=false; 
  public isButtonClicked=false;
  public isJobFunctionProcessing=false;
  public adsForm={
      positionID: '',
      industryID: '',
      jobFunctionID: '',
      locationID: '',
      employmentTypeID: '',
      advertiseName: '',
      advertisePositions: '',
      positionTitle: '', // from pd title
      advertiseSalary: '',
      sellingPoint: '',
      aboutCompany: '',
      jobBenifits: '',
      positionSummary: '',
      positionRoleAndResponsibilities: '',
      aboutYou: '',
      KeyRequirements: ''
  };
  public dropdownSettings={};

// LOCATION
  public dropDownListLocation=[];
  public selectedLocation=[];

//EMPLOYEEMENT TYPE
  public dropDownListEmployeementType=[];
  public selectedEmployeementType=[];

//INDUSTRY
  public dropDownListIndustry=[];
  public selectedIndustry=[];

//JOB FUNCTION
  public dropDownListJobFunction=[];
  public selectedJobFunction=[];

//PDs
  public dropDownListPDs=[];
  public selectedPDs=[];
  

  constructor( public dialog:MdDialog,private router:Router,public activatedRoute:ActivatedRoute,
                private advertisementManagementService:AdvertisementManagementService,
                public coreService:CoreService ) { }

  ngOnInit() {
    APPCONFIG.heading="Update advertisement";
    // if( this.activatedRoute.snapshot.params['re'] && this.advertisementManagementService.adsFormData)this.adsForm=this.advertisementManagementService.adsFormData;
    // this.progressBarSteps();
    this.getDeatils();
    this.dropdownSettings = {
        singleSelection: true, 
        text:"Select",
        enableSearchFilter: true,
        classes:"myclass"
    };
  }

//method to get job function on industry select
onIndustrySelect(e){
   this.isJobFunctionProcessing=true;
    this.advertisementManagementService.getJobFunction(e.id).subscribe(
      d => {
              if (d.code == "200") {
                 this.isJobFunctionProcessing = false;
                 this.dropDownListJobFunction=d.data.filter(i=>{i['id']=i.jobFunctionID ; i['itemName']=i.title; 
                   if(i.jobFunctionID == this.adsForm.jobFunctionID){
                      this.selectedJobFunction=[];
                      this.selectedJobFunction.push(i);
                   }                                                        
                 return i });         
              }
              else{
                 this.isJobFunctionProcessing = false;
                this.coreService.notify("Unsuccessful", d.message, 0); 
              }
          },
          error => {
                 this.isJobFunctionProcessing = false;            
            this.coreService.notify("Unsuccessful", error.message, 0); 
          }    
      )
}

//metod to get  data for all drop down fields
  getRequiredData(){
    this.isDataProcessing=true;
    this.advertisementManagementService.getRequiredData().subscribe(
      d => {
              if (d.code == "200") {
                 this.isDataProcessing = false;
                 this.dropDownListEmployeementType=d.data.employeementType.filter(i=>{
                   i['id']=i.employmentTypeID ; i['itemName']=i.title;
                   if(i.employmentTypeID == this.adsForm.employmentTypeID){
                     this.selectedEmployeementType=[];
                     this.selectedEmployeementType.push(i);
                   }                    
                    return i });
                 this.dropDownListIndustry=d.data.industries.filter(i=>{
                   i['id']=i.industryID ; i['itemName']=i.title;
                   if(i.industryID == this.adsForm.industryID){
                     this.selectedIndustry=[];                     
                     this.selectedIndustry.push(i);
                     this.onIndustrySelect(i);
                   }                                       
                    return i });
                 this.dropDownListLocation=d.data.locations.filter(i=>{
                   i['id']=i.locationID ; i['itemName']=i.title; 
                   if(i.locationID == this.adsForm.locationID){
                     this.selectedLocation=[];
                     this.selectedLocation.push(i);
                   }                                   
                   return i });
                 this.dropDownListPDs=d.data.positions.filter(i=>{
                   i['id']=i.positionID ; i['itemName']=i.positionTitle;
                   if(i.positionID == this.adsForm['companyPositionID']){
                     this.selectedPDs=[];
                     this.selectedPDs.push(i);    
                   }                                   
                    return i });  
                
              }
              else this.coreService.notify("Unsuccessful", d.message, 0);
          },
          error => {
            this.coreService.notify("Unsuccessful", error.message, 0); 
          }    
      )
  }

  getDeatils(){
    this.isProcessing=true;
    this.advertisementManagementService.getAdvertisementDeatils( this.activatedRoute.snapshot.params['id']).subscribe(
      d => {
              if (d.code == "200") {
                this.adsForm=d.data;
                 this.isProcessing=false;                                                                                                                                                                                                                                                                   this.getRequiredData();                                                                                    
              }
              else this.coreService.notify("Unsuccessful", d.message, 0);
          },
          error => {
            this.coreService.notify("Unsuccessful", error.message, 0); 
          }    
      )
  }

  //on selecting partuicular postions, getting details of seleted position
    onPDSelect(item){
      this.isFormDataProcessing=true;
       this.advertisementManagementService.getPositionDetails(item.id).subscribe(
      d => {
              if (d.code == "200") {
                  this.isFormDataProcessing=false;    
                  this.adsForm.positionID=item.id;
                  this.adsForm.positionTitle=item.itemName;
                  this.adsForm.advertiseName=d.data.positionTitle;
                  this.adsForm.positionSummary=d.data.positionSummary;
                  this.adsForm.positionRoleAndResponsibilities=d.data.positionRoleAndResponsibilities;
                  this.adsForm.KeyRequirements=d.data.positionPreviousExperience;
              }
              else {
                this.coreService.notify("Unsuccessful", d.message, 0);
                  this.isFormDataProcessing=false;                    
              }
          },
          error => {
            this.coreService.notify("Unsuccessful", error.message, 0); 
                  this.isFormDataProcessing=false;                
          }    
      )
    }



  progressBarSteps() {
      this.progress={
          progress_percent_value:25, //progress percent
          total_steps:4,
          current_step:1,
          circle_container_width:25, //circle container width in percent
          steps:[
              // {num:1,data:"Select Role"},
               {num:1,data:"Create advertisement",active:true},
               {num:2,data:"Preview"},
               {num:3,data:"Interview question"},
               {num:4,data:"Referece check question"}
          ]  
       }         
  }

      onUpdate(form){
        this.isButtonClicked=true;
        this.isUpdateProcessing=true;   
        if(form.valid){
          this.adsForm.locationID=this.selectedLocation[0].id;
          this.adsForm.employmentTypeID=this.selectedEmployeementType[0].id;
          this.adsForm.industryID=this.selectedIndustry[0].id;
          this.adsForm.jobFunctionID=this.selectedJobFunction[0].id;
          this.adsForm.positionID=this.selectedPDs[0].id;
              this.advertisementManagementService.updateAdvertisementDeatils(this.adsForm).subscribe(
            d => {
                    if (d.code == "200") {
                      this.isProcessing=false;        
                      this.coreService.notify("Successful", d.message, 1);                                                                                                  
                      this.isUpdateProcessing=false;                                                                                                                                                                                                                                                             this.router.navigate([`/app/company-admin/advertisement/view/${ this.activatedRoute.snapshot.params['id']}`]);                                                                  
                    }
                    else {
                      this.coreService.notify("Unsuccessful", d.message, 0);
                      this.isUpdateProcessing=false;
                    }
                },
                error => {
                  this.isUpdateProcessing=false;                  
                  this.coreService.notify("Unsuccessful", error.message, 0); 
                }    
            )            
      }
    }
}
