import { Component, OnInit,ViewContainerRef } from '@angular/core';
import { APPCONFIG } from '../../../config';
import { Router,ActivatedRoute} from '@angular/router';
import { MdDialog, MdDialogRef,  } from '@angular/material';
import { AdvertisementManagementService } from '../advertisement-management.service';
import { CoreService } from '../../../shared/services/core.service';
import { ConfirmService } from '../../../shared/services/confirm.service';

@Component({
  selector: 'app-advertisement-listing',
  templateUrl: './advertisement-listing.component.html',
  styleUrls: ['./advertisement-listing.component.scss']
})
export class AdvertisementListingComponent implements OnInit {
public toggleFilter=true;
public isProcessing=false;
public advertisements=[];
public enableFilter=false;
public unsubscriber:any;
public searchKey='';

  public filter={
    status: [
      // {title:"Request for approval",isChecked:false},
      // {title:"Approved",isChecked:false},
      // {title:"Completed",isChecked:false},
      {title:"Active",isChecked:false},
      {title:"Archived",isChecked:false}
    ]
  }
public reqBody = {
  search:"",
  filter:{
    status:{}
  }
}
public employeeListings=[];
   constructor( public dialog:MdDialog,private router:Router,public activatedRoute:ActivatedRoute,public viewContainerRef:ViewContainerRef,
                private advertisementManagementService:AdvertisementManagementService,public confirmService:ConfirmService,
                public coreService:CoreService ) { }

  ngOnInit() {
    APPCONFIG.heading="Advertisement list";
        this.getAdvertisements();    
  }

  onKeyChange(){
   this.unsubscriber.unsubscribe();
    this.reqBody.search=encodeURIComponent(this.searchKey);
        this.getAdvertisements();  
  }

  onApplyFilters(){
    this.reqBody.filter.status={};
      for(let i of this.filter.status){
          if(i.isChecked)this.reqBody.filter.status[i.title]=true;
        } 
        this.getAdvertisements();
  }

   getAdvertisements(){
    this.isProcessing=true;
    this.unsubscriber= this.advertisementManagementService.getAdvertisements(JSON.stringify(this.reqBody)).subscribe(
      d => {
              if (d.code == "200") {
                 this.isProcessing = false;
                if(d.data) {
                  this.advertisements=d.data.filter(item=>{
                   item['isRowProcessing']=false;
                   return item;
                })
                this.advertisements.reverse();
              } else this.advertisements=[];
                 
              }
              else{
                this.coreService.notify("Unsuccessful", d.message, 0);
                this.isProcessing=false;
              }   
          },
          error => {
            this.isProcessing=false;
            this.coreService.notify("Unsuccessful", error.message, 0); 
          }    
      )
  }

  // COPY
  onCopy(advs){
       this.advertisementManagementService.copyAdvertisement(advs.advertiseSystemID).subscribe(
      d => {
              if (d.code == "200") {
                this.coreService.notify("Successful", d.message, 1);                
                  this.advertisements.unshift(d.data);
                   advs.isRowProcessing=false;  
              }
              else{
                this.coreService.notify("Unsuccessful", d.message, 0);
                advs.isRowProcessing=false;
              }   
          },
          error => {
            advs.isRowProcessing=false;
            this.coreService.notify("Unsuccessful", error.message, 0); 
          }    
      )
  }

  // EDIT
  onEdit(advs){
    this.router.navigate([`/app/company-admin/advertisement/edit/${advs.advertiseSystemID}`]);
  }
  
  // Delete
  onDelete(advs){
      this.confirmService.confirm(this.confirmService.tilte, this.confirmService.message, this.viewContainerRef)
      .subscribe(res => {
        let result = res;
        if (result) {
    this.advertisementManagementService.deleteAdvertisement(advs.advertiseSystemID).subscribe(
      d => {
              if (d.code == "200") {
                this.coreService.notify("Successful", d.message, 1);                
                  this.advertisements=this.advertisements.filter(item=>{
                   if(item['advertiseSystemID']==advs['advertiseSystemID']) return null;
                   else return item;
                })
              }
              else{
                this.coreService.notify("Unsuccessful", d.message, 0);
                advs.isRowProcessing=false;
              }   
          },
          error => {
            advs.isRowProcessing=false;
            this.coreService.notify("Unsuccessful", error.message, 0); 
          }    
      )
       }
       else advs.isRowProcessing=false;
      })
  }
  
  // Archive
  onArchive(advs){
       this.advertisementManagementService.archiveAdvertisement(advs.advertiseSystemID).subscribe(
      d => {
              if (d.code == "200") {
                this.coreService.notify("Successful", d.message, 1);
                  this.advertisements=this.advertisements.filter(item=>{
                   if(item['advertiseSystemID']==advs['advertiseSystemID']) return null;
                   else return item;
                })
                 
              }
              else{
                this.coreService.notify("Unsuccessful", d.message, 0);
                advs.isRowProcessing=false;
              }   
          },
          error => {
            advs.isRowProcessing=false;
            this.coreService.notify("Unsuccessful", error.message, 0); 
          }    
      )
  }

  onViewCandidates(advs){
    this.router.navigate([`/app/company-admin/candidate-management/list/${advs.advertiseSystemID}`]);    
  }
  
  onViewDetails(advs){
    this.router.navigate([`/app/company-admin/advertisement/view/${advs.advertiseSystemID}`]);    
  }
}
