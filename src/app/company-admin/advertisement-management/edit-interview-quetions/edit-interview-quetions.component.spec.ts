import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditInterviewQuetionsComponent } from './edit-interview-quetions.component';

describe('EditInterviewQuetionsComponent', () => {
  let component: EditInterviewQuetionsComponent;
  let fixture: ComponentFixture<EditInterviewQuetionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditInterviewQuetionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditInterviewQuetionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
