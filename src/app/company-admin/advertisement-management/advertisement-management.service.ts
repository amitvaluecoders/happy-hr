import { Injectable } from '@angular/core';
import { RestfulLaravelService} from '../../shared/services/restful-laravel.service';

@Injectable()
export class AdvertisementManagementService {

  public adsFormData:any;
  public interviewQuestions:any;
  public dropDownData:any;
  constructor(public restfulWebService:RestfulLaravelService) { }

  getAllPositions():any{
    return this.restfulWebService.get('get-all-positions');
  }

  getPositionDetails(id):any{
  return this.restfulWebService.get(`get-position-description/${id}`);
  }

  getJobFunction(id):any{
  return this.restfulWebService.get(`get-job-functions-by-industry/${id}`);
  }

  addAdvertisement(reqBody):any{
    return this.restfulWebService.post('create-advertisement',reqBody);
  }

  getInterviewQuestions():any{
    return this.restfulWebService.get('get-default-interview-questions');
  }
  getReferenceCheckQuestions():any{
    return this.restfulWebService.get('get-default-reference-check-form');    
  }
  
  getRequiredData():any{
    return this.restfulWebService.get('advertisement-additional-information');    
  }

  getAdvertisements(reqBody):any{
    return this.restfulWebService.get(`get-advertisement-list/${encodeURIComponent(reqBody)}`);
  }

  deleteAdvertisement(id):any{
    return this.restfulWebService.delete(`delete-advertisement/${id}`);
  }

  archiveAdvertisement(id):any{
    return this.restfulWebService.delete(`archive-advertisement/${id}`);
  }

  copyAdvertisement(id):any{
    return this.restfulWebService.get(`copy-advertisement/${id}`);
  }

  getAdvertisementDeatils(id):any{
    return this.restfulWebService.get(`get-advertisement-details/${id}`);    
  }

  updateAdvertisementDeatils(reqBody):any{
    return this.restfulWebService.put(`edit-advertisement`,reqBody);    
  }

// method to get interview questions of particular advertisement.
  getAdvertisementInterviewDeatils(id):any{
     return this.restfulWebService.get(`get-advertisement-interview-question/${id}`);     
  }

  // method to get interview questions of particular advertisement.
  getAdvertisementRefrenceDeatils(id):any{
    return this.restfulWebService.get(`get-advertisement-reference-check-question/${id}`); 
  }

  updateInterviewQuestions(reqBody):any{
    return this.restfulWebService.put(`update-company-interview-question`,reqBody);        
  }
  
  updateRefrencechkQuestions(reqBody):any{
    return this.restfulWebService.put(`update-company-reference-question`,reqBody);        
  }
}
