import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { MaterialModule } from '@angular/material';
import { BroadcastMessagesService } from './broadcast-messages.service';
import { BroadcastComponent } from './broadcast/broadcast.component';
import { BroadcastMessagesRoutingModule } from './broadcast-messages.routing';
import { BroadcastDetailComponent,AddBroadcastDetail } from './broadcast-detail/broadcast-detail.component';
import { SharedModule } from '../../shared/shared.module';
import { BroadastMessagesGuardService } from './broadast-messages-guard.service';
@NgModule({
  imports: [
    SharedModule,
    CommonModule,
    FormsModule,
    MaterialModule,
    BroadcastMessagesRoutingModule
  ],
  declarations: [BroadcastComponent, BroadcastDetailComponent,AddBroadcastDetail],
  providers:[BroadcastMessagesService, BroadastMessagesGuardService],
  entryComponents:[AddBroadcastDetail]
})
export class BroadcastMessagesModule { }
