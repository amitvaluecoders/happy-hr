import { Injectable } from '@angular/core';
import { RestfulLaravelService} from '../../shared/services/restful-laravel.service';

@Injectable()
export class BroadcastMessagesService {

  constructor(public restfulWebService:RestfulLaravelService) { }

  public getBradcastGoups():any{
    return this.restfulWebService.get('broadcast-message-group');
  }
  
  public getALLMessages(reqBody):any{
    return this.restfulWebService.get(`broadcast-message-list/${JSON.stringify(reqBody)}`);
  }
  public addBroadcastMessage(reqBody):any{
    return this.restfulWebService.post(`broadcast-message-add`,reqBody);
  }
  
}
