import { Component, OnInit } from '@angular/core';
import { MdSnackBar, MdDialog, MdDialogRef } from '@angular/material';
import { APPCONFIG } from '../../../config';
import { BroadcastMessagesService } from '../broadcast-messages.service'
import { Router, ActivatedRoute, Params } from '@angular/router';
import { CoreService } from '../../../shared/services/core.service';


@Component({
  selector: 'app-broadcast-detail',
  templateUrl: './broadcast-detail.component.html',
  styleUrls: ['./broadcast-detail.component.scss']
})
export class BroadcastDetailComponent implements OnInit {

  public groupInfo='';
  public heading='';
  public broadcastMessages=[];
  public isProcessing=false;

  constructor(public dialog: MdDialog, public broadcastMessageService:BroadcastMessagesService,
               public router: Router, public activatedRoute: ActivatedRoute,
               public coreService: CoreService) { }

  ngOnInit() {
    this.groupInfo = this.activatedRoute.snapshot.params['groupInfo'];
    this.heading='';
    if(this.groupInfo == 'from-super-admin') this.heading = this.heading+'Messages received from Happy HR';
    if(this.groupInfo == 'all-user-in-company') this.heading = this.heading+'Broadcast message to all employees';
    if(this.groupInfo == 'company-sub-admin') this.heading = this.heading+'Broadcast message to sub company admin';
    if(this.groupInfo == 'company-main-admin') this.heading = this.heading+'Broadcast message from main company admin';
    if(this.groupInfo == 'company-manager') this.heading = this.heading+'Broadcast message to all managers';
    APPCONFIG.heading = this.heading;
    this.getAAMeassages();
  }
  onTime(time){
    return new Date(time);
  }
  onAddMessage() {

      let dialogRef = this.dialog.open(AddBroadcastDetail);
      dialogRef.componentInstance.user={message:'',group:this.groupInfo}
      console.log("group info",this.groupInfo)
      dialogRef.afterClosed().subscribe(result =>{
       if(result)this.broadcastMessages.unshift(result);
      })
  }

  // method to get alll messages of particular group.
  getAAMeassages(){
      this.isProcessing = true;
       this.broadcastMessageService.getALLMessages({group:this.groupInfo}).subscribe(
          d => {
            this.isProcessing = false;
              if (d.code == "200") {
                this.broadcastMessages=d.data;
                this.isProcessing = false; 
              }
              else this.coreService.notify("Unsuccessful", d.message, 0);
          },
          error =>{
            this.coreService.notify("Unsuccessful", error.message, 0);
            this.isProcessing = false;
          }   
      )
  }
}


@Component({
    selector: 'add-broadcast-detail',
    template: `<h1 md-dialog-title>{{messageHeading}}</h1>
        <div md-dialog-content style="padding-bottom: 20px;">
         <form #userForm="ngForm" name="material_signup_form" class="md-form-auth form-validation" >
          
           <div class="form-group">
               
                <div class="full-width">
                  <textarea style="height:100px; width:600px;resize:none;" class="form-control" name="planObjective" placeholder="Enter message" #message="ngModel" required [(ngModel)]="user.message"
                    myNoSpaces rows="4"></textarea>
                  <div *ngIf="message.errors && ( (message.dirty || message.touched) || isButtonClicked )">
                    <div class="alert alert-danger" [hidden]="!(message.errors.required || message.errors.whitespace)">
                      Message is required !
                    </div>
                  </div>
                </div>
              </div>
          </form>     
        
        </div>
        <md-progress-spinner *ngIf="isProcessing" mode="indeterminate" color=""></md-progress-spinner>
        <div *ngIf="!isProcessing">
            <button (click) = 'onSubmit(userForm.form.valid)' style="margin-right:5px;" class="btn btn-primary">Send</button>
            <button (click) = 'dialogRef.close(false);' class="btn btn-default">Cancel</button>
        </div>
        
        `,
})
export class AddBroadcastDetail implements OnInit {
  public user={
        message:'',
        group:''
      };
      public isButtonClicked=false;
      public isProcessing=false;
      public messageHeading='';
      constructor(public dialogRef: MdDialogRef<AddBroadcastDetail>, public broadcastMessageService: BroadcastMessagesService,
        public router: Router, public activatedRoute: ActivatedRoute,
        public coreService: CoreService) {
        // if (this.user.group == 'from-super-admin') this.messageHeading = this.messageHeading + 'Messages received from Happy HR';
        // if (this.user.group == 'all-user-in-company') this.messageHeading = this.messageHeading + 'Broadcast message to all employees';
        // if (this.user.group == 'company-sub-admin') this.messageHeading = this.messageHeading + 'Broadcast message to sub company admin';
        // if(this.groupInfo == 'company-employee') this.heading = this.heading+'company employee';
        // if (this.user.group == 'company-manager') this.messageHeading = this.messageHeading + 'Broadcast message to all managers';
      }
    
      ngOnInit(){

        if (this.user.group == 'from-super-admin') this.messageHeading = this.messageHeading + 'Messages received from Happy HR';
        if (this.user.group == 'all-user-in-company') this.messageHeading = this.messageHeading + 'Broadcast message to all employees';
        if (this.user.group == 'company-sub-admin') this.messageHeading = this.messageHeading + 'Broadcast message to sub company admin';
        // if(this.groupInfo == 'company-employee') this.heading = this.heading+'company employee';
        if (this.user.group == 'company-manager') this.messageHeading = this.messageHeading + 'Broadcast message to all managers';
      
      }

    onSubmit(isvalid){
      this.isButtonClicked=true;
      if(isvalid){
           this.isProcessing = true;
       this.broadcastMessageService.addBroadcastMessage(this.user).subscribe(
          d => {
            this.isProcessing = false;
              if (d.code == "200") {
                this.dialogRef.close(d.data);
                this.isProcessing = false; 
                this.coreService.notify("Successful", d.message, 1);
              }
              else this.coreService.notify("Unsuccessful", d.message, 0);
          },
          error =>{
            this.coreService.notify("Unsuccessful", error.message, 0);
            this.isProcessing = false;
          }   
      )
  }

      
    }
}