import {  ModuleWithProviders  } from '@angular/core';
import {  RouterModule,Routes  } from '@angular/router';
import { BroadcastMessagesService } from './broadcast-messages.service';
import { BroadcastComponent } from './broadcast/broadcast.component';
import { BroadcastDetailComponent } from './broadcast-detail/broadcast-detail.component';

export const BroadcastMessagesPagesRoutes: Routes = [
    {
        path: '',
        canActivate:[],
        children: [
            { path: '', component: BroadcastComponent},
            { path: 'detail/:groupInfo', component: BroadcastDetailComponent},
        ]
    }
];

export const BroadcastMessagesRoutingModule = RouterModule.forChild(BroadcastMessagesPagesRoutes);
