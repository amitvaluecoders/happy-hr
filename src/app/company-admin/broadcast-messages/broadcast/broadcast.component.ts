import { Component, OnInit } from '@angular/core';
import { APPCONFIG } from '../../../config';
import { BroadcastMessagesService } from '../broadcast-messages.service'
import { Router, ActivatedRoute, Params } from '@angular/router';
import { CoreService } from '../../../shared/services/core.service';
import { MdSnackBar, MdDialog, MdDialogRef } from '@angular/material';
import * as moment from 'moment';

@Component({
  selector: 'app-broadcast',
  templateUrl: './broadcast.component.html',
  styleUrls: ['./broadcast.component.scss']
})
export class BroadcastComponent implements OnInit {

  public isProcessing=false;
  public broadcastGroups=[];
  constructor( public broadcastMessageService:BroadcastMessagesService,
               public router: Router, public activatedRoute: ActivatedRoute,
               public coreService: CoreService,public dialog: MdDialog) { }

  ngOnInit() {
     APPCONFIG.heading="Broadcast";
     this.getBroadcastGroups();
  }
  formatDate(v){
    return moment(v).isValid()? moment(v).format('DD MMM YYYY') : 'Not specified';
}

// Method to get all broadcast groups.
  getBroadcastGroups(){
      this.isProcessing = true;
       this.broadcastMessageService.getBradcastGoups().subscribe(
          d => {
            this.isProcessing = false;
              if (d.code == "200") {
                this.broadcastGroups=d.data;
                this.isProcessing = false; 
              }
              else this.coreService.notify("Unsuccessful", d.message, 0);
          },
          error =>{
            this.coreService.notify("Unsuccessful", error.message, 0);
            this.isProcessing = false;
          }   
      )
  }

}
