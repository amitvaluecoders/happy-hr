import { Injectable } from '@angular/core';
import { RestfulLaravelService } from "../../shared/services/restful-laravel.service";

@Injectable()
export class PerformanceIndicatorService {

  constructor(private restfulService: RestfulLaravelService) { }

  getPerformanceIndicators(params) { //{"userID":23}
    return this.restfulService.get(`position-performance-indicator/${params}`);
  }

  getEmployeeList() {
    return this.restfulService.get('show-all-employee');
  }

  saveManagerResponse(reqBody) {
    return this.restfulService.post('create-direct-assessment', reqBody);
  }
}
