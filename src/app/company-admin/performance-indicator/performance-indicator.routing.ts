import { ModuleWithProviders } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PiDirectAssessmentComponent } from "./pi-direct-assessment/pi-direct-assessment.component";

export const PerformanceIndicatorPagesRoutes: Routes = [
    { path: 'direct-assessment/:uid', component: PiDirectAssessmentComponent },
];

export const PerformanceIndicatorRoutingModule = RouterModule.forChild(PerformanceIndicatorPagesRoutes);
