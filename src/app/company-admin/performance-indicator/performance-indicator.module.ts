import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { MaterialModule } from '@angular/material';
import { SharedModule } from '../../shared/shared.module';
import { PiDirectAssessmentComponent } from './pi-direct-assessment/pi-direct-assessment.component';
import { PerformanceIndicatorRoutingModule } from "./performance-indicator.routing";
import { PerformanceIndicatorService } from "./performance-indicator.service";
import { AngularMultiSelectModule } from 'angular2-multiselect-checkbox-dropdown/angular2-multiselect-dropdown';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    PerformanceIndicatorRoutingModule,
    AngularMultiSelectModule,
    MaterialModule,
    SharedModule
  ],
  declarations: [PiDirectAssessmentComponent],
  providers: [PerformanceIndicatorService]
})
export class PerformanceIndicatorModule { }
