import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { CoreService } from '../../../shared/services/core.service';
import { APPCONFIG } from '../../../config';
import { IDatePickerConfig } from 'ng2-date-picker';
import * as moment from "moment";
import { MdSnackBar, MdDialog, MdDialogRef } from '@angular/material';
import { ActionsOnEmployeePopupComponent } from "../../../shared/components/actions-on-employee-popup/actions-on-employee-popup.component";
import { PerformanceIndicatorService } from "../performance-indicator.service";

@Component({
  selector: 'app-pi-direct-assessment',
  templateUrl: './pi-direct-assessment.component.html',
  styleUrls: ['./pi-direct-assessment.component.scss']
})
export class PiDirectAssessmentComponent implements OnInit {

  public progress;
  public collapse = [];
  public isProcessing = false;
  public isSubmitted = false;
  public noResult = true;
  public userID = "";
  public userData = { "userID": "", "name": "", "imageUrl": "", "position": "" }
  // public pa = {
  //   "performaceAppraisalID": "", "title": "", "assessmentDate": "",
  //   "managerID": { "userID": "", "name": "", "imageUrl": "", "position": "" },
  //   "userID": { "userID": "", "name": "", "imageUrl": "", "position": "" },
  //   "performanceIndicator": [], "question": [], "thirdParty": []
  // }

  public managerResponse = {
    "employeeUserID": "", "nextAssessmentDate": "", "reminderDate": "",
    "generalManagerNote": "", "performanceIndicator": [], "thirdParty": []
  }

  config: IDatePickerConfig = {};
  reminderDate; nextAssessmentDate;
  itemList = [];
  modelName = '';

  constructor(public coreService: CoreService, private router: Router, private route: ActivatedRoute,
    public companyService: PerformanceIndicatorService, public dialog: MdDialog) {

    this.config.locale = "en";
    this.config.format = "DD MMM, YYYY hh:mm A";
    this.config.showMultipleYearsNavigation = true;
    this.config.weekDayFormat = 'dd';
    this.config.disableKeypress = true;
    this.config.monthFormat = "MMM YYYY";
    this.config.min = moment();
  }

  ngOnInit() {
    localStorage.removeItem('PaUserData');
    localStorage.removeItem('PaKpi');
    localStorage.removeItem('PaCallbackUrl');
    APPCONFIG.heading = "Performance indicator";

    if (this.route.snapshot.params['uid']) {
      this.userID = this.route.snapshot.params['uid'];
      this.getPerformanceIndicators();
    }
    this.getEmployee();
  }

  getEmployee() {
    this.companyService.getEmployeeList().subscribe(
      res => {
        if (res.code == 200) {
          this.itemList = res.data;
          this.itemList.filter((item, i) => {
            item['itemName'] = item.name; item['id'] = item.userId; item['position'] = item.designation;
          });
        }
        else return this.coreService.notify("Unsuccessful", res.message, 0);
      },
      err => {
        this.coreService.notify("Unsuccessful", err.message, 0);
      })
  }

  formatDate(date) {
    return (date && moment(date).isValid()) ? moment(date).format('DD MMM YYYY') : 'Not specified';
  }

  getPerformanceIndicators() {
    this.isProcessing = true;
    let params = { "userID": this.userID };
    this.companyService.getPerformanceIndicators(JSON.stringify(params)).subscribe(
      res => {
        this.isProcessing = false;
        if (res.code == 200) {
          this.noResult = false;
          this.userData = res.data.userID;
          this.managerResponse.performanceIndicator = res.data.performanceIndicator;
          this.managerResponse.performanceIndicator.filter(item => {
            item['result'] = !(item['result']) ? null : item.result;
            item['managerNote'] = !(item['managerNote']) ? '' : item.managerNote;
          });
        }
        else return this.coreService.notify("Unsuccessful", res.message, 0);
      },
      err => {
        this.isProcessing = false;
        this.coreService.notify("Unsuccessful", err.message, 0);
      }
    )
  }

  onAction(actionType, category, index) {
    let route;
    switch (actionType) {
      case 'PIP':
        route = ['/app/company-admin/performance-improvement-plan/create', this.userID];
        if (category == 'kpi') {
          this.managerResponse.performanceIndicator[index]['isProcessingResult'] = true;
          this.managerResponse.performanceIndicator[index].result = actionType;
          localStorage.setItem('PaKpi', JSON.stringify(this.managerResponse.performanceIndicator[index]));
        }
        this.saveManagerResponse(route);
        break;

      case 'BPIP':
        route = ['/app/company-admin/behavioral-performance-improvement-plan/create', this.userID];
        if (category == 'kpi') {
          this.managerResponse.performanceIndicator[index]['isProcessingResult'] = true;
          this.managerResponse.performanceIndicator[index].result = actionType;
        }
        this.saveManagerResponse(route);
        break;

      case 'Disciplinary Action':
        route = ['/app/company-admin/disciplinary-meeting/create', this.userID];
        if (category == 'kpi') {
          this.managerResponse.performanceIndicator[index]['isProcessingResult'] = true;
          this.managerResponse.performanceIndicator[index].result = actionType;
        }
        this.saveManagerResponse(route);
        break;

      case 'Development plan':
        route = ['/app/company-admin/development-plan/create', this.userID];
        if (category == 'kpi') {
          this.managerResponse.performanceIndicator[index]['isProcessingResult'] = true;
          this.managerResponse.performanceIndicator[index].result = actionType;
        }
        this.saveManagerResponse(route);
        break;

      default:
        let info = { type: actionType, userID: this.userID, subject: "", body: '' }
        if (category == 'kpi') {
          info['performanceIndicatorID'] = this.managerResponse.performanceIndicator[index].performanceID;
        }

        let dialogRef = this.dialog.open(ActionsOnEmployeePopupComponent);
        dialogRef.componentInstance.actionInfo = info;
        dialogRef.afterClosed().subscribe(r => {
          if (r) {
            if (category == 'kpi') {
              this.managerResponse.performanceIndicator[index].result = actionType;
            }
            this.saveManagerResponse(route);
          }
        });
        break;
    }
  }

  isValid() {
    let pi = true;

    for (let i = 0; i < this.managerResponse.performanceIndicator.length; i++) {
      if (!this.managerResponse.performanceIndicator[i].result) {
        pi = false; break;
      }
    }
    return pi;
  }

  saveManagerResponse(route?: any[]) {
    this.managerResponse.employeeUserID = this.userID;
    this.managerResponse.thirdParty = JSON.parse(JSON.stringify(this.itemList.filter(i => i['isSelected'])));
    this.managerResponse.thirdParty.filter(i => {
      if (i['external']) {
        i['type'] = 'external'; i['email'] = i.itemName;
      }
      else {
        i['type'] = 'internal';
        i['userID'] = i.userId;
      }
    });

    this.managerResponse['status'] = this.isValid() ? 'Complete' : 'Under Assessment';
    this.managerResponse.nextAssessmentDate = this.nextAssessmentDate ? moment(this.nextAssessmentDate).format('YYYY-MM-DD HH:mm:ss') : '';
    this.managerResponse.reminderDate = this.reminderDate ? moment(this.reminderDate).format('YYYY-MM-DD HH:mm:ss') : '';

    console.log(this.managerResponse);
    this.isSubmitted = true;
    this.companyService.saveManagerResponse(this.managerResponse).subscribe(
      res => {
        this.isSubmitted = false;
        if (res.code == 200) {
          this.coreService.notify("Successful", res.message, 1);
          if (route && route.length) {
            localStorage.setItem('PaUserData', JSON.stringify(this.userData));
            localStorage.setItem('PaCallbackUrl', `/app/company-admin/performance-appraisal/direct-assessment/${res.data.performaceAppraisalID}`);
            this.router.navigate(route);
          } else {
            this.router.navigate(['/app/company-admin/performance-appraisal/direct-assessment-result/', res.data.performaceAppraisalID])
          }
        }
        else return this.coreService.notify("Unsuccessful", res.message, 0);
      },
      err => {
        this.isSubmitted = false;
        this.coreService.notify("Unsuccessful", err.message, 0);
      }
    )
  }
}
