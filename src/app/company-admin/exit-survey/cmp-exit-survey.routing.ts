import { RouterModule, Routes } from '@angular/router';
import { CreateExitSurveyComponent } from './create-exit-survey/create-exit-survey.component';
import { ExitSurveyResponseComponent } from "./exit-survey-response/exit-survey-response.component";

export const route: Routes = [
    { path: '', component: CreateExitSurveyComponent },
    { path: 'view-response/:id', component: ExitSurveyResponseComponent }
];

export const exitSurveyModuleRouter = RouterModule.forChild(route);
