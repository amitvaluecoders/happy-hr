import { Injectable } from '@angular/core';
import { RestfulLaravelService } from "../../shared/services/restful-laravel.service";

@Injectable()
export class CmpExitSurveyService {

  constructor(private restfulService: RestfulLaravelService) { }

  getExitSurveyList(params) {
    return this.restfulService.get(`exit-survey-list/${params}`);
  }

    deleteExitSurveyList(employeeExitSurveyID) {
    return this.restfulService.delete(`delete-employee-exit-survey/${employeeExitSurveyID}`);
  }

  createExitSurvey(reqBody) {
    if (!reqBody.companyExitSurveyID) {
      return this.restfulService.post('create-exit-survey', reqBody);
    } else {
      return this.restfulService.put('update-exit-survey', reqBody);
    }

  }

  assignToEmployee(reqBody) {
    return this.restfulService.post('assign-exit-survey-to-employee', reqBody);
  }

  addQuestion(reqBody) {
    return this.restfulService.post('add-exit-survey-question', reqBody);
  }

  deleteQuestion(id) {
    return this.restfulService.delete(`delete-exit-survey-question/${id}`);
  }

  getEmployee() {
    return this.restfulService.get(`show-all-employee`);
  }

  getSurveyQuestions() {
    // return this.restfulService.get('admin/list-exist-survey');
    return this.restfulService.get('list-admin-exist-survey');
  }

  getExitSurveyDetail() {
    return this.restfulService.get('exit-survey-detail');
  }

  updateExitSurvey(reqBody) {
    return this.restfulService.put('update-exit-survey', reqBody);
  }

  getEmployeeResponse(id) {
    return this.restfulService.get(`exit-survey-employee-response/${id}`);
  }
}
