import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { APPCONFIG } from '../../../config';
import { CoreService } from '../../../shared/services/core.service';
import { CmpExitSurveyService } from "../cmp-exit-survey.service";
import * as moment from "moment";
import { ConfirmService } from '../../../shared/services/confirm.service';

@Component({
  selector: 'app-create-exit-survey',
  templateUrl: './create-exit-survey.component.html',
  styleUrls: ['./create-exit-survey.component.scss']
})
export class CreateExitSurveyComponent implements OnInit {
  editable = {};
  isProcessing = false;
  isProcessingForm = false;
  isProcessingQsn = false;
  adminQuestions = [];
  selectedQuestion = [];
  dropdownSettings = {};

  questionBox = { editMode: false, questionID: '', questionTitle: '' };
  exitSurvey = {
    "companyExitSurveyID": "", "title": "", "question": []
  }

  constructor(public companyService: CmpExitSurveyService, public coreService: CoreService, private router: Router,
    private route: ActivatedRoute, public confirmService: ConfirmService, public viewContainerRef: ViewContainerRef) { }

  ngOnInit() {
    APPCONFIG.heading = "View exit survey";
    this.dropdownSettings = {
      singleSelection: true,
      text: "Choose a question",
      enableSearchFilter: true,
      classes: "myclass"
    };

    this.getSurveyQuestions();
    this.getExitSurveyDetail();
    // if (this.route.snapshot.params['id']) {
    //   this.exitSurvey.forUserID = this.route.snapshot.params['id'];
    // }
  }

  getSurveyQuestions() {
    // this.isProcessing = true;
    this.companyService.getSurveyQuestions().subscribe(
      res => {
        // this.isProcessing = false;
        if (res.code == 200) {
          this.adminQuestions = res.data;
          this.adminQuestions.filter((item, i) => { item['itemName'] = item.title; item['id'] = item.exitSurveyID; });
        }
        else return this.coreService.notify("Unsuccessful", res.message, 0);
      },
      err => {
        // this.isProcessing = false;
        if (err.code == 400) {
          return this.coreService.notify("Unsuccessful", err.message, 0);
        }
        this.coreService.notify("Unsuccessful", "Error while getting data.", 0)
      }
    )
  }

  getExitSurveyDetail() {
    this.isProcessing = true;
    this.companyService.getExitSurveyDetail().subscribe(
      res => {
        this.isProcessing = false;
        if (res.code == 200) {
          this.exitSurvey = this.coreService.copyObject(this.exitSurvey, res.data);
        }
        else return this.coreService.notify("Unsuccessful", res.message, 0);
      },
      err => {
        this.isProcessing = false;
        if (err.code == 400) {
          return this.coreService.notify("Unsuccessful", err.message, 0);
        }
        this.coreService.notify("Unsuccessful", "Error while getting data.", 0)
      }
    )
  }

  deleteQuestion(qsn, index) {
    this.confirmService.confirm("Delete confirmation", "Do you want to delete this question?", this.viewContainerRef).subscribe(
      confirm => {
        if (confirm) {
          qsn['isProcessing'] = true;
          this.companyService.deleteQuestion(qsn.questionID).subscribe(
            res => {
              qsn['isProcessing'] = false;
              if (res.code == 200) {
                this.exitSurvey.question.splice(index, 1);
              }
              else return this.coreService.notify("Unsuccessful", res.message, 0);
            },
            err => {
              qsn['isProcessing'] = false;
              if (err.code == 400) {
                return this.coreService.notify("Unsuccessful", err.message, 0);
              }
              this.coreService.notify("Unsuccessful", "Error while saving.", 0)
            }
          )
        }
      }
    )
  }

  saveQuestion() {
    if (!this.exitSurvey.companyExitSurveyID) {
      let reqBody = { "companyExitSurveyID": "", "title": this.exitSurvey.title, "question": [{ "questionID": "", "questionTitle": this.questionBox.questionTitle }] };
      return this.companyService.createExitSurvey(reqBody);
    }
    let reqBody = { "companyExitSurveyID": this.exitSurvey.companyExitSurveyID, "question": [{ "questionID": "", "questionTitle": this.questionBox.questionTitle }] };
    return this.companyService.addQuestion(reqBody);
  }

  onSaveQuestion() {
    if (!this.exitSurvey.title) return this.coreService.notify('Unsuccessful', 'Please enter the survey title', 0);

    this.isProcessingQsn = true;
    this.saveQuestion().subscribe(
      res => {
        this.isProcessingQsn = false;
        if (res.code == 200) {
          this.getExitSurveyDetail();
          // this.exitSurvey.question.push({ "questionID": this.questionBox.questionID, "questionTitle": this.questionBox.questionTitle });
          this.resetQuestionBox(false);
        }
        else return this.coreService.notify("Unsuccessful", res.message, 0);
      },
      err => {
        this.isProcessingQsn = false;
        if (err.code == 400) {
          return this.coreService.notify("Unsuccessful", err.message, 0);
        }
        this.coreService.notify("Unsuccessful", "Error while saving.", 0)
      }
    )

  }

  resetQuestionBox(editMode) {
    this.questionBox = {
      editMode: editMode,
      questionID: "",
      questionTitle: ""
    };
  }

  onSelectQuestion(item) {
    // let qsn = this.superAdminQuestions.filter(i => i.surveyQuetionnerID == surveyQuetionnerID).shift();
    this.questionBox.questionID = item.exitSurveyID;
    this.questionBox.questionTitle = item.title;
  }

  onDeSelectQuestion(item) {
    this.resetQuestionBox(true);
  }

  saveExitSurvey(isValid, isSaving) {
    if (!isValid) return;
    if (!this.exitSurvey.question.length) return this.coreService.notify('Unsuccessful', 'Please add a question.', 0);

    this.isProcessingForm = true;
    this.companyService.createExitSurvey(this.exitSurvey).subscribe(
      res => {
        this.isProcessingForm = false;
        if (res.code == 200) {
          this.coreService.notify("Successful", res.message, 1);
          if (isSaving) {
            this.router.navigate(['/app/company-admin/listing-page/exit-survey']);
          }
        }
        else return this.coreService.notify("Unsuccessful", res.message, 0);
      },
      err => {
        this.isProcessingForm = false;
        if (err.code == 400) {
          return this.coreService.notify("Unsuccessful", err.message, 0);
        }
        this.coreService.notify("Unsuccessful", "Error while getting data.", 0)
      }
    )
  }

}
