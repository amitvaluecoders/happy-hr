import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { MaterialModule } from "@angular/material";
import { CreateExitSurveyComponent } from './create-exit-survey/create-exit-survey.component';
import { exitSurveyModuleRouter } from './cmp-exit-survey.routing';
import { DataTableModule } from 'angular2-datatable';
import { CmpExitSurveyService } from "./cmp-exit-survey.service";
import { AngularMultiSelectModule } from 'angular2-multiselect-checkbox-dropdown/angular2-multiselect-dropdown';
import { ExitSurveyGuardService } from './exit-survey-guard.service';
import { ExitSurveyResponseComponent } from './exit-survey-response/exit-survey-response.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    MaterialModule,
    exitSurveyModuleRouter,
    DataTableModule,
    AngularMultiSelectModule
  ],
  declarations: [CreateExitSurveyComponent, ExitSurveyResponseComponent],
  providers: [CmpExitSurveyService,ExitSurveyGuardService]
})
export class CmpExitSurveyModule { }
