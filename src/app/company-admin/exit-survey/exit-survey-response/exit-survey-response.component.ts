import { Component, OnInit } from '@angular/core';
import { APPCONFIG } from '../../../config';
import { Router, ActivatedRoute } from '@angular/router';
import { CmpExitSurveyService } from '../cmp-exit-survey.service';
import { CoreService } from '../../../shared/services/core.service';
import * as moment from "moment";

@Component({
  selector: 'app-exit-survey-response',
  templateUrl: './exit-survey-response.component.html',
  styleUrls: ['./exit-survey-response.component.scss']
})
export class ExitSurveyResponseComponent implements OnInit {
  isProcessing = false;
  noResult = true;
  employeeExitSurveyID = '';
  survey = {
    "companyExitSurveyID": "", "question": [],
    "assignedBy": { "userID": "", "name": "", "imageUrl": "", "position": "" },
    "assignedTo": { "userID": "", "name": "", "imageUrl": "", "position": "" }
  };

  constructor(public companyService: CmpExitSurveyService, public coreService: CoreService, public router: Router, public route: ActivatedRoute) { }

  ngOnInit() {
    APPCONFIG.heading = "Exit survey";
    if (this.route.snapshot.params['id']) {
      this.employeeExitSurveyID = this.route.snapshot.params['id'];
      this.getExitSurveyDetail();
    }
  }

  getExitSurveyDetail() {
    this.isProcessing = true;
    this.companyService.getEmployeeResponse(this.employeeExitSurveyID).subscribe(
      res => {
        this.isProcessing = false;
        if (res.code == 200) {
          this.survey = res.data;
          this.noResult = false;
        }
        else return this.coreService.notify('Unsuccessful', res.message, 0);
      },
      error => {
        this.isProcessing = false;
        if (error.code == 400) {
          return this.coreService.notify('Unsuccessful', error.message, 0);
        }
        this.coreService.notify('Unsuccessful', "Error while getting survey detail", 0);
      }
    )
  }

}
