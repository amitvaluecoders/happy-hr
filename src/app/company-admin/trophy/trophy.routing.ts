import { ModuleWithProviders } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CreateTrophyComponent } from './create-trophy/create-trophy.component';
import { TrophyListComponent } from './trophy-list/trophy-list.component';
import { CreateTrophyNominationsComponent } from './create-trophy-nominations/create-trophy-nominations.component';
import { TrophyProgressComponent } from './trophy-progress/trophy-progress.component';
import { CreateShotoutComponent } from './create-shotout/create-shotout.component';
import { TrophyGuardService } from "./trophy-guard.service";

export const TrophyPagesRoutes: Routes = [
    {
        path: '',
        canActivate: [],
        children: [

            { path: '', component: TrophyListComponent, canActivate: [TrophyGuardService] },
            { path: 'trophy-list', component: TrophyListComponent, canActivate: [TrophyGuardService] },
            { path: 'create-trophy', component: CreateTrophyComponent, canActivate: [TrophyGuardService] },
            { path: 'edit-trophy/:id', component: CreateTrophyComponent, canActivate: [TrophyGuardService] },
            // { path: 'trophy-list', component: TrophyListComponent, canActivate: [TrophyGuardService] },
            { path: 'create-trophy-nominations/:id', component: CreateTrophyNominationsComponent, canActivate: [TrophyGuardService] },
            { path: 'trophy-progress/:id', component: TrophyProgressComponent, canActivate: [TrophyGuardService] },
            { path: 'create-shout-out/:id', component: CreateShotoutComponent, canActivate: [TrophyGuardService] }]
    }
];

export const TrophyRoutingModule = RouterModule.forChild(TrophyPagesRoutes);
