import { Component, OnInit } from '@angular/core';
import { MdSnackBar, MdDialog, MdDialogRef } from '@angular/material';
import { TrophyService } from '../trophy.service';
import { CoreService } from '../../../shared/services/core.service';
import { TrophyDetail } from '../trophy';

@Component({
  selector: 'app-brodcast-message-popup',
  templateUrl: './brodcast-message-popup.component.html',
  styleUrls: ['./brodcast-message-popup.component.scss']
})
export class BrodcastMessagePopupComponent implements OnInit {

  trophy = new TrophyDetail;
  winners = [];
  message: string = "Hello to all users in my company.";

  constructor(public trophyService: TrophyService, public coreService: CoreService, public dialogRef: MdDialogRef<BrodcastMessagePopupComponent>) { }

  ngOnInit() { }

  broadcast() {
    let broadCast = {
      "trophyID": this.trophy.trophyID,
      "nomineeIDs": this.winners.map(i => { return i.nomineeID }),
      "group": "all-user-in-company",
      "message": this.message
    }
    this.dialogRef.close(broadCast);
  }

  cancel() {
    this.dialogRef.close(null);
  }

}
