import { Injectable } from '@angular/core';
import { RestfulLaravelService } from '../../shared/services/restful-laravel.service';

@Injectable()
export class TrophyService {

  constructor(public restfulLaravelService: RestfulLaravelService) { }

  createOrUpdateTrophy(reqBody) {
    return this.restfulLaravelService.post('create-update-trophy', reqBody);
  }

  createTrophyNominations(reqBody) {
    return this.restfulLaravelService.post('create-nomination-trophy', reqBody);
  }

  getTrophyList(params) {
    return this.restfulLaravelService.get(`get-trophy-list/${params}`);
  }

  getTrophyDetail(id) {
    return this.restfulLaravelService.get(`get-trophy-detail/${id}`);
  }

  deleteTrophy(id) {
    return this.restfulLaravelService.delete(`delete-trophy/${id}`);
  }

  broadcast(reqBody) {
    return this.restfulLaravelService.post('add-trophy-winner', reqBody);
  }

  createShoutOut(reqBody) {
    return this.restfulLaravelService.post('shoutout-add', reqBody);
  }

  uploadIcon(formData) {
    return this.restfulLaravelService.post('file-upload', formData);
  }

  /**
   * get list of all employee
   */
  getAllEmployee() {
    return this.restfulLaravelService.get('show-all-employee');
  }

  /**
 * get list of employee only
 */
  getEmployee() {
    return this.restfulLaravelService.get('show-only-employee');
  }

  /**
 * get list of manager only
 */
  getManagers() {
    return this.restfulLaravelService.get('show-manager-and-ceo');
  }

  /**
   * get trophy created by superAdmin
   * @param params {string} filter params example: {"offset":"0","limit":"10","search":"Trophy 1"}
   */
  getTrophy(params) {
    return this.restfulLaravelService.get(`admin/list-base-trophy/${params}`);
  }

  /**
   * get awards created by superAdmin
   * @param params {string} filter params example: {"filter": {"awards": {"A": true, "B": true, "C": false}}}
   */
  getAwards(params) {
    return this.restfulLaravelService.get(`admin/award-list/${params}`);
  }

}
