export class TrophyDetail {
    awardFor = "";
    icon = ""
    nominatedUserIDs = []
    reason = "";
    title = "";
    trophyID: any = "";
    votingCloseTime = "";
}