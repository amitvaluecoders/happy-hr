import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { APPCONFIG } from '../../../config';
import { TrophyService } from '../trophy.service';
import { CoreService } from '../../../shared/services/core.service';
import { MdDialogRef, MdDialog, MdSnackBar } from '@angular/material';
import { BrodcastMessagePopupComponent } from '../brodcast-message-popup/brodcast-message-popup.component';
import * as moment from 'moment';
import { TrophyDetail } from '../trophy';

@Component({
  selector: 'app-trophy-progress',
  templateUrl: './trophy-progress.component.html',
  styleUrls: ['./trophy-progress.component.scss']
})
export class TrophyProgressComponent implements OnInit {

  isProcessing: boolean;
  isProcessingWinner: boolean;
  trophy = new TrophyDetail;

  constructor(public dialog: MdDialog, public trophyService: TrophyService, public coreService: CoreService, public router: Router, public route: ActivatedRoute) { }

  ngOnInit() {
    APPCONFIG.heading = "Progress";

    if (this.route.snapshot.params['id']) {
      this.trophy['trophyID'] = this.route.snapshot.params['id'];
      this.getTrophyDetail();
    }
  }

  onPopup() {
    let dialogRef = this.dialog.open(BrodcastMessagePopupComponent);
    let maxcount = Math.max.apply(Math, this.trophy.nominatedUserIDs.map(function (o) { return o.totalVotes; }));
    let winners = this.trophy.nominatedUserIDs.filter(i => i.totalVotes == maxcount);

    dialogRef.componentInstance.trophy = this.trophy;
    dialogRef.componentInstance.winners = winners;
    dialogRef.afterClosed().subscribe(broadCast => {
      if (broadCast) {
        this.isProcessingWinner = true;
        this.trophyService.broadcast(broadCast).subscribe(
          res => {
            this.isProcessingWinner = false;
            if (res.code == 200) {
              this.coreService.notify("Successful", res.message, 1);
            }
            else return this.coreService.notify("Unsuccessful", res.message, 0);
          },
          error => {
            this.isProcessingWinner = false;
            if (error.code == 400) {
              return this.coreService.notify("Unsuccessful", error.message, 0);
            }
            this.coreService.notify("Unsuccessful", "Error while broadcasting.", 0);
          }
        )
      }
    });
  }

  getTrophyDetail() {
    this.isProcessing = true;
    this.trophyService.getTrophyDetail(this.trophy['trophyID']).subscribe(
      res => {
        if (res.code == 200) {
          this.trophy = res.data;
          // if (this.trophy.nominatedUserIDs.length == 0) {
          //   this.router.navigate(['/app/company-admin/trophy/create-trophy-nominations', this.trophy.trophyID])
          // }
          this.isProcessing = false;
        }
      },
      error => {
        this.isProcessing = false;
        if (error.code == 400) {
          return this.coreService.notify("Unsuccessful", error.message, 0);
        }
        this.coreService.notify("Unsuccessful", "Error while getting trophy details.", 0)
      })
  }

  validateBroadcast() {
    let disable = false;

    if (this.trophy.votingCloseTime == null || !this.trophy.nominatedUserIDs.length) {
      disable = true;
    } else if (moment().isBefore(moment(this.trophy.votingCloseTime))) {
      disable = true;
    }
    return disable;
  }

  getTime(dt) {
    return moment(dt).format('hh:mm A');
  }

  getDate(dt) {
    return moment(dt).format('DD MMM YYYY');
  }

  timeLeft(dt) {
    // Start date
    let countDownTill = new Date(dt);
    let now = new Date();

    // Get the months
    let months = moment(countDownTill).diff(moment(), 'months');
    // console.log(months);

    // Add months to the date
    now.setMonth(now.getMonth() + months);

    // Get the weeks
    let weeks = moment(countDownTill).diff(now, 'weeks');
    // Seems like moment is doing something wrong here... it should be 0 weeks...
    // 15 Nov to 19 Nov is 0 weeks to me at least...
    // console.log(weeks);

    // Add the weeks to the date
    now.setDate(now.getDate() + (7 * weeks));

    let days = moment(countDownTill).diff(now, 'days');

    let m = (Math.abs(months) > 0) ? Math.abs(months) + ' months ' : '';
    let w = (Math.abs(weeks) > 0) ? Math.abs(weeks) + ' weeks ' : '';
    let d = (Math.abs(days) > 0) ? Math.abs(days) + ' days ' : '';

    let negativeValue = (months < 0) ? true : (weeks < 0) ? true : (days < 0) ? true : (months == 0 && weeks == 0 && days == 0) ? true : false;
    return (negativeValue) ? '0 days' : m + w + d;
  }

}
