import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { APPCONFIG } from '../../../config';
import { TrophyService } from '../trophy.service';
import { CoreService } from '../../../shared/services/core.service';
import { TrophyDetail } from '../trophy';

@Component({
  selector: 'app-create-shoutout',
  templateUrl: './create-shotout.component.html',
  styleUrls: ['./create-shotout.component.scss']
})
export class CreateShotoutComponent implements OnInit {
  employeeList = [];
  selectedEmployee = [];
  dropdownSettings = {};

  isProcessing = false;
  isProcessingForm = false;
  trophy = new TrophyDetail;
  trophyID = '';

  shoutOut = {
    "title": "", "reason": "", "employeeUserIDs": [], "trophyID": ""
  }

  constructor(public trophyService: TrophyService, public coreService: CoreService, public router: Router, public route: ActivatedRoute) { }

  ngOnInit() {
    APPCONFIG.heading = "Create shoutout";

    this.dropdownSettings = {
      singleSelection: false,
      text: "Select user",
      selectAllText: 'Select all',
      unSelectAllText: 'Unselect all',
      enableSearchFilter: true,
      classes: "myclass"
    };

    $('.list-area ul').addClass('listItems');

    if (this.route.snapshot.params['id']) {
      this.trophyID = this.route.snapshot.params['id'];
      this.getTrophyDetail();
    }

  }

  getTrophyDetail() {
    this.isProcessing = true;
    this.trophyService.getTrophyDetail(this.trophyID).subscribe(
      res => {
        this.isProcessing = false;
        if (res.code == 200) {
          this.trophy = res.data;

          this.employeeList = this.trophy.nominatedUserIDs;
          this.employeeList.filter(item => { item['itemName'] = item.NominieeMember.name; item['id'] = item.NominieeMember.userID; });
          setTimeout(() => {
            this.employeeList.filter(function (item, i) {
              $('.listItems li').eq(i).prepend(`<img src="${item.NominieeMember.imageUrl}" alt="" />`);
              $('.listItems li').eq(i).children('label').append('<span>' + item.NominieeMember.position + '</span>');
            });
          }, 0);
          console.log(this.employeeList)
        }
      },
      error => {
        this.isProcessing = false;
        if (error.code == 400) {
          return this.coreService.notify("Unsuccessful", error.message, 0);
        }
        this.coreService.notify("Unsuccessful", "Error while getting trophy details.", 0)
      }
    )
  }

  createTrophy(isValid) {
    if (!isValid) return;

    this.shoutOut.trophyID = this.trophyID;
    this.shoutOut.employeeUserIDs = this.selectedEmployee.map(item => { return item.id });
    console.log(this.shoutOut);

    this.isProcessingForm = true;
    this.trophyService.createShoutOut(this.shoutOut).subscribe(
      res => {
        this.isProcessingForm = false;
        if (res.code == 200) {
          this.coreService.notify("Successful", res.message, 1);
          this.router.navigate(['/app/company-admin/trophy/trophy-list']);
        }
        else return this.coreService.notify("Unsuccessful", res.message, 0);
      },
      error => {
        this.isProcessingForm = false;
        if (error.code == 400) {
          return this.coreService.notify("Unsuccessful", error.message, 0);
        }
        this.coreService.notify("Unsuccessful", "Error while getting trophy details.", 0)
      }
    )
  }

}
