import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { MdDialogRef, MdDialog, MdSnackBar } from '@angular/material';
import { APPCONFIG } from '../../../config';
import { TrophyService } from '../trophy.service';
import { CoreService } from '../../../shared/services/core.service';
import { ConfirmService } from '../../../shared/services/confirm.service';

@Component({
  selector: 'app-trophy-list',
  templateUrl: './trophy-list.component.html',
  styleUrls: ['./trophy-list.component.scss']
})
export class TrophyListComponent implements OnInit {
  trophyList = [];
  isProcessing: boolean;
  searchKey: string = "";
  permission;

  constructor(public dialog: MdDialog, public trophyService: TrophyService, public coreService: CoreService, public confirmService: ConfirmService,
    public viewContainerRef: ViewContainerRef) { }

  ngOnInit() {
    APPCONFIG.heading = "Trophy listing";
    this.permission = this.coreService.getNonRoutePermission();
    console.log('perm ===> ',this.permission);
    this.getTrophyList();
  }

  getTrophyList() {
    this.isProcessing = true;
    let params = { "search": encodeURIComponent(this.searchKey) };
    this.trophyService.getTrophyList(encodeURIComponent(JSON.stringify(params))).subscribe(
      res => {
        if (res.code == 200) {
          this.trophyList = res.data;
          this.isProcessing = false;
          console.log(res);
        }
        else return this.coreService.notify("Unsuccessful", res.message, 0);
      },
      error => {
        this.isProcessing = false;
        if (error.code == 400) {
          return this.coreService.notify("Unsuccessful", error.message, 0);
        }
        this.coreService.notify("Unsuccessful", "Error while getting trophy list", 0);
      }
    )
  }

  deleteTrophy(trophy) {
    this.confirmService.confirm("Delete confirmation", "Do you want to delete this trophy?", this.viewContainerRef).subscribe(
      confirm => {
        if (confirm) {
          this.isProcessing = true;
          this.trophyService.deleteTrophy(trophy.trophyID).subscribe(
            res => {
              this.isProcessing = false;
              if (res.code == 200) {
                console.log(res);
                let index = this.trophyList.findIndex(i => i.trophyID == trophy.trophyID);
                this.trophyList.splice(index, 1);
                this.coreService.notify("Successful", res.message, 1);
              }
              else return this.coreService.notify("Unsuccessful", res.message, 0);
            },
            error => {
              this.isProcessing = false;
              if (error.code == 400) {
                return this.coreService.notify("Unsuccessful", error.message, 0);
              }
              this.coreService.notify("Unsuccessful", "Error while deleting trophy", 0);
            }
          )
        }
      }
    )
  }

}
