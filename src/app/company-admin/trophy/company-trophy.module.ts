import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { TrophyRoutingModule } from './trophy.routing';
import { TrophyService } from './trophy.service';
import { CreateTrophyComponent } from './create-trophy/create-trophy.component';
import { TrophyListComponent } from './trophy-list/trophy-list.component';
import { CreateTrophyNominationsComponent } from './create-trophy-nominations/create-trophy-nominations.component';
import { CreateShotoutComponent } from './create-shotout/create-shotout.component';
import { TrophyProgressComponent } from './trophy-progress/trophy-progress.component';
import { BrodcastMessagePopupComponent } from './brodcast-message-popup/brodcast-message-popup.component';
import { AngularMultiSelectModule } from 'angular2-multiselect-checkbox-dropdown/angular2-multiselect-dropdown';
import { MaterialModule } from '@angular/material';
import { SharedModule } from '../../shared/shared.module';
import { DataTableModule } from 'angular2-datatable';
import { TrophyGuardService } from './trophy-guard.service';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    TrophyRoutingModule,
    AngularMultiSelectModule,
    MaterialModule,
    SharedModule,
    DataTableModule
  ],
  entryComponents: [BrodcastMessagePopupComponent],
  declarations: [CreateTrophyComponent, TrophyListComponent, CreateTrophyNominationsComponent,
    CreateShotoutComponent, TrophyProgressComponent, BrodcastMessagePopupComponent],
  providers: [TrophyService,TrophyGuardService]
})
export class CompanyTrophyModule { }
