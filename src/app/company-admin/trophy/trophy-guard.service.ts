import { Injectable } from '@angular/core';
import {CanActivate,Router,RouterStateSnapshot,ActivatedRouteSnapshot} from "@angular/router";
import {CoreService} from '../../shared/services/core.service';
import {Observable} from 'rxjs/Rx'; 
import 'rxjs/add/operator/map';

@Injectable()
export class TrophyGuardService {

  public module = "trophy";
  
        constructor(private _core: CoreService, private router: Router){ }
  
        canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {  
            let url: string;
            if(route['_routeConfig']['path'])
            url= route['_routeConfig']['path']
            else url = state.url;
            if(this.checkHavePermission(url)){
              return true;
            }else{
              this.router.navigate(['/extra/unauthorized']);
              return false;
            }
        }
  
        private checkHavePermission(path) {
            switch (path) {
                case "/app/company-admin/trophy":
                    return this._core.getModulePermission(this.module, 'view');
                case 'create-trophy':
                    return this._core.getModulePermission(this.module, 'add');
                case 'edit-trophy/:id':
                    return this._core.getModulePermission(this.module, 'edit');
                case 'trophy-list':
                    return this._core.getModulePermission(this.module, 'view');
                case 'create-trophy-nominations/:id':
                    return this._core.getModulePermission(this.module, 'add');   
                case 'trophy-progress/:id':
                    return this._core.getModulePermission(this.module, 'view');    
                case 'create-shout-out':
                    return this._core.getModulePermission(this.module, 'add');              
                default:
                    return false;
            }
        }
}