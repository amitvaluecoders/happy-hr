import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { APPCONFIG } from '../../../config';
import { TrophyService } from '../trophy.service';
import { CoreService } from '../../../shared/services/core.service';
import { TrophyDetail } from '../trophy';

@Component({
  selector: 'app-create-trophy',
  templateUrl: './create-trophy.component.html',
  styleUrls: ['./create-trophy.component.scss']
})
export class CreateTrophyComponent implements OnInit {
  trophyList = [];
  selectedTrophy = [];
  awardForList = [];
  selectedAwardFor = [];
  dropdownSettings = {};
  isProcessing = false;
  isProcessingData = false;
  isProcessinglist = false;
  heading: string;
  colors = [];
  tropyId = '';

  trophy = new TrophyDetail;

  constructor(public trophyService: TrophyService, public coreService: CoreService, public router: Router, public route: ActivatedRoute) { }

  ngOnInit() {
    this.heading = "Create trophy";

    this.dropdownSettings = {
      singleSelection: true,
      text: "Select one",
      enableSearchFilter: true,
    };

    if (this.route.snapshot.params['id']) {
      this.heading = "Edit trophy";
      this.tropyId = this.route.snapshot.params['id'];
      this.getTrophyDetail();
    }

    APPCONFIG.heading = this.heading;
    this.colors = this.coreService.getColors();
    this.getAwardFor();
    this.getTrophy();
  }

  getTrophyDetail() {
    this.isProcessingData = true;
    this.trophyService.getTrophyDetail(this.tropyId).subscribe(
      res => {
        if (res.code == 200) {
          this.isProcessingData = false;
          this.trophy = res.data
          this.selectedAwardFor = this.awardForList.filter(i => i.itemName == this.trophy.awardFor);
        }
        else return this.coreService.notify("Unsuccessful", res.message, 0);
      },
      error => {
        this.isProcessingData = false;
        if (error.code == 400) {
          return this.coreService.notify("Unsuccessful", error.message, 0);
        }
        this.coreService.notify("Unsuccessful", "Error while getting trophy details.", 0);
      })
  }

  onItemDeSelect(item: any) {
    this.trophy = new TrophyDetail;
    this.selectedAwardFor = [];
  }

  onItemSelect(item: any) {
    let trophy = this.trophyList.filter(i => i.baseTrophyID == item.id).shift();
    if (trophy.awardFor.length > 1) this.selectedAwardFor = this.awardForList.filter(i => i.itemName == 'Anyone');
    else this.selectedAwardFor = this.awardForList.filter(i => i.itemName == trophy.awardFor);
    let icon = this.colors.filter(i => i.split('/').pop() == trophy.icon.split('/').pop()).shift();

    this.trophy = {
      awardFor: this.selectedAwardFor[0].itemName,
      icon: icon,
      reason: trophy.reason,
      title: trophy.title,
      trophyID: '',
      nominatedUserIDs: [],
      votingCloseTime: '',
    }

    console.log('ssss =>> ', this.trophy);
  }

  createTrophy() {
    if (this.trophy.icon == '') return;
    this.isProcessing = true;
    this.trophy.trophyID = this.tropyId;
    this.trophy.awardFor = this.selectedAwardFor[0].itemName;
    console.log(this.trophy)
    this.trophyService.createOrUpdateTrophy(this.trophy).subscribe(
      res => {
        if (res.code == 200) {
          console.log(res);
          this.isProcessing = false;
          this.coreService.notify("Successful", res.message, 1);
          this.router.navigate(['/app/company-admin/trophy/trophy-list']);
        }
        else return this.coreService.notify("Unsuccessful", res.message, 0);
      },
      error => {
        this.isProcessing = false;
        if (error.code == 400) {
          return this.coreService.notify("Unsuccessful", error.message, 0);
        }
        this.coreService.notify("Unsuccessful", "Error while creating trophy", 0);
      }
    )
  }

  setColor(color) {
    console.log(color)
    this.trophy.icon = color;
  }

  // isChecked(color: string) {
  //   if (!this.trophy.icon || !color) return;
  //   return color.split('/').pop() == this.trophy.icon.split('/').pop();
  // }

  getAwardFor() {
    this.awardForList = this.coreService.getAwardForList();
  }

  getTrophy() {
    this.isProcessinglist = true;
    let params = {};
    this.trophyService.getTrophy(JSON.stringify(params)).subscribe(
      res => {
        this.isProcessinglist = false;
        if (res.code == 200) {
          this.trophyList = res.data;
          this.trophyList.filter((item, i) => { item['itemName'] = item.title; item['id'] = item.baseTrophyID; })
        }
        else return this.coreService.notify("Unsuccessful", res.message, 0);
      },
      error => {
        this.isProcessinglist = false;
        if (error.code == 400) {
          return this.coreService.notify("Unsuccessful", error.message, 0);
        }
        this.coreService.notify("Unsuccessful", "Error while creating trophy", 0);
      })
  }

}
