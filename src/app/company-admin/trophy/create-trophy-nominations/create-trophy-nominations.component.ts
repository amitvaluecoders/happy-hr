import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { APPCONFIG } from '../../../config';
import { TrophyService } from '../trophy.service';
import { CoreService } from '../../../shared/services/core.service';
import { IDatePickerConfig } from 'ng2-date-picker';
import * as moment from 'moment';
import { TrophyDetail } from '../trophy';

@Component({
  selector: 'app-create-trophy-nominations',
  templateUrl: './create-trophy-nominations.component.html',
  styleUrls: ['./create-trophy-nominations.component.scss']
})
export class CreateTrophyNominationsComponent implements OnInit {

  employeeList = [];
  selectedEmployee = [];
  dropdownSettings = {};
  isProcessing = false;
  isProcessingNomination = false;
  isProcessinglist = false;
  askToNominate = false;
  trophyNomination = {
    trophyID: 1, nominatedUserIDs: [], votingDateTime: "", askEmployeeToNominate: "No"
  }
  trophy = new TrophyDetail;

  public config: IDatePickerConfig = {};
  public votingTime: any;
  isDisable = false;

  constructor(public trophyService: TrophyService, public coreService: CoreService, public router: Router, public route: ActivatedRoute) {
    this.config.locale = 'en';
    this.config.format = "YYYY-MM-DD HH:mm:ss";
    this.config.showMultipleYearsNavigation = true;
    this.config.weekDayFormat = 'dd';
    this.config.disableKeypress = true;
    this.config.drops = "down";
    this.config.min = moment();
  }

  ngOnInit() {
    APPCONFIG.heading = "Create trophy nominations";
    this.dropdownSettings = {
      singleSelection: false,
      text: "Select name",
      selectAllText: 'Select all',
      unSelectAllText: 'Unselect all',
      enableSearchFilter: true,
      classes: "myclass"
    }
    $('.list-area ul').addClass('listItems');

    // this.getEmployee();
    if (this.route.snapshot.params['id']) {
      this.trophyNomination.trophyID = this.route.snapshot.params['id'];
      this.getTrophyDetail();
    }
  }

  getTrophyDetail() {
    this.isProcessing = true;
    this.trophyService.getTrophyDetail(this.trophyNomination.trophyID).subscribe(
      res => {
        this.isProcessing = false;
        if (res.code == 200) {
          this.trophy = res.data;
          this.trophyNomination = {
            askEmployeeToNominate: "No", nominatedUserIDs: [],
            trophyID: this.trophy.trophyID,
            votingDateTime: this.trophy.votingCloseTime,
          }

          this.votingTime = !this.trophy.votingCloseTime ? '' : moment(this.trophy.votingCloseTime).format('YYYY-MM-DD HH:mm:ss');
          this.isDisable = this.trophy.nominatedUserIDs.length > 0;
          this.getEmployee();
        }
      },
      error => {
        this.isProcessing = false;
        if (error.code == 400) {
          return this.coreService.notify("Unsuccessful", error.message, 0);
        }
        this.coreService.notify("Unsuccessful", "Error while getting trophy details.", 0)
      })
  }

  getEmp() {
    if (this.trophy.awardFor == 'Employee') {
      return this.trophyService.getEmployee();
    } else if (this.trophy.awardFor == 'Manager') {
      return this.trophyService.getManagers();
    } else {
      return this.trophyService.getAllEmployee();
    }
  }

  getEmployee() {
    this.isProcessinglist = true;
    this.getEmp().subscribe(
      res => {
        this.isProcessinglist = false;
        if (res.code == 200) {
          this.employeeList = res.data;
          this.employeeList.filter((item, i) => { item['itemName'] = item.name; item['id'] = item.userId; });
          setTimeout(() => {
            this.employeeList.filter(function (item, i) {
              $('.listItems li').eq(i).prepend(`<img src="${item.imageUrl}" alt="" />`);
              $('.listItems li').eq(i).children('label').append('<span>' + item.designation + '</span>');
            });
          }, 0);

          setTimeout(() => {
            if (this.trophy.nominatedUserIDs.length > 0) {
              this.employeeList.forEach(emp => {
                this.trophy.nominatedUserIDs.forEach(nom => {
                  if (nom.NominieeMember.userID == emp.userId) {
                    this.selectedEmployee.push(emp);
                  }
                })
              })
            }
          }, 10);
        }
      },
      error => {
        this.isProcessinglist = false;
        if (error.code == 400) {
          return this.coreService.notify("Unsuccessful", error.message, 0);
        }
        this.coreService.notify("Unsuccessful", "Error while getting employee.", 0);
      })
  }

  createNomination() {
    if (!this.votingTime) return
    if (!this.askToNominate && this.selectedEmployee.length == 0) return;
    this.trophyNomination.nominatedUserIDs = (this.askToNominate) ? [] : this.selectedEmployee.map(i => { return i.id; })
    this.trophyNomination.votingDateTime = moment(this.votingTime).format('YYYY-MM-DD HH:mm:ss');
    this.trophyNomination.askEmployeeToNominate = (this.askToNominate) ? "Yes" : "No";

    this.isProcessingNomination = true;
    this.trophyService.createTrophyNominations(this.trophyNomination).subscribe(
      res => {
        if (res.code == 200) {
          this.isProcessingNomination = false;
          this.coreService.notify("Successful", res.message, 1);
          this.router.navigate(['/app/company-admin/trophy/trophy-list']);
        }
        else return this.coreService.notify("Unsuccessful", res.message, 0);
      },
      error => {
        this.isProcessingNomination = false;
        if (error.code == 400) {
          return this.coreService.notify("Unsuccessful", error.message, 0);
        }
        this.coreService.notify("Unsuccessful", "Error while creating trophy nominations.", 0)
      })
  }
}
