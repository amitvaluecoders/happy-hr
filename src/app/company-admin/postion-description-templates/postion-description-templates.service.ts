import { Injectable } from '@angular/core';
import { RestfulLaravelService} from '../../shared/services/restful-laravel.service';

@Injectable()
export class PostionDescriptionTemplatesService {

  constructor(public restfulWebService:RestfulLaravelService) { }

  getAllIndustryTypes():any{
   return this.restfulWebService.get('get-industry')
  }

  addEditPositionDescription(reqBody):any{
   return this.restfulWebService.post('create-company-position',reqBody)
   
  }

  getPostionDescriptionDetails(reqBody){
     return this.restfulWebService.get(`company-position-detail/${reqBody}`)
  }

  getPositionDescriptionList(reqBody){
  return this.restfulWebService.get(`company-position/${encodeURIComponent(reqBody)}`)
  }

  deletePosition(reqBody){
     return this.restfulWebService.delete(`delete-company-position/${reqBody}`)
  }

  getPositionDescriptionListByIndustry(reqBody){
     return this.restfulWebService.get(`get-industry-position/${reqBody}`)    
  }

  getPostionDescriptionDetailsByPositionID(reqBody){
    return this.restfulWebService.get(`get-industry-position-detail/${reqBody}`)    
  }

  addEditperformanceIndicator(data){
    if(data.isAdd)return this.restfulWebService.post(`create-company-performace-indicator`,data.reqBody)   
    else return this.restfulWebService.put(`update-company-performance-indicator`,data.reqBody)   
  }

  getcontractTypesforLevels(){
    return this.restfulWebService.get(`get-contract-type-with-salary-structure`)    
  }

  deleteperformanceIndicator(reqBody){
     return this.restfulWebService.delete(`delete-company-performance-indicator/${reqBody}`)
  }
}
