import { Component, OnInit,ViewContainerRef } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { CoreService } from '../../../shared/services/core.service';
import { PostionDescriptionTemplatesService } from '../postion-description-templates.service';
import { ConfirmService } from '../../../shared/services/confirm.service';
import { APPCONFIG } from '../../../config';

@Component({
  selector: 'app-position-description-list',
  templateUrl: './position-description-list.component.html',
  styleUrls: ['./position-description-list.component.scss']
})
export class PositionDescriptionListComponent implements OnInit {
  public sortOrder="asc";
  public searchKey='';
  public rowOnPAge=10;
  public userInfo:any;
  public unsubscriber:any;
  public positionDescriptionArray=[];
  public isProcessing=false;
  permission;
  constructor(public router:Router,public activatedRoute:ActivatedRoute,
              public coreService:CoreService,public confirmService:ConfirmService,public viewContainerRef:ViewContainerRef,
              public postionDescriptionTemplatesService:PostionDescriptionTemplatesService) { }

  ngOnInit() {
     this.userInfo=JSON.parse(localStorage.getItem('happyhr-userInfo'));        
    APPCONFIG.heading="Position description";
          this.getPositionDescriptionList();
          this.permission=this.coreService.getNonRoutePermission();
  }

    getPositionDescriptionList(){
        this.isProcessing=true;
       this.unsubscriber = this.postionDescriptionTemplatesService.getPositionDescriptionList(JSON.stringify({search:encodeURIComponent(this.searchKey)})).subscribe(
            d => {
                if (d.code == "200") {
                    this.positionDescriptionArray=d.data;
                    this.isProcessing=false;
                }
                else if(d.code == "400"){
                     this.isProcessing=false;
                     this.positionDescriptionArray=[];
                }
                else this.coreService.notify("Unsuccessful", "Error while getting position description list", 0);
            },
            error =>{
                if(error.code=="400"){
                    this.isProcessing=false;
                     this.positionDescriptionArray=[];
                }
                else this.coreService.notify("Unsuccessful", "Error while getting position description list", 0);
            }        
        )
    }
    getPositionDescriptionListOnSearch(){
       this.unsubscriber.unsubscribe();
       this.getPositionDescriptionList();
    }

    onAddNewButton(){
        this.router.navigate([`/app/company-admin/position-description/add`]);
    }

    OnEdit(positionId){
        this.router.navigate([`/app/company-admin/position-description/edit/${positionId}`]);
    }

    onShowDetails(positionId){
        this.router.navigate([`/app/company-admin/position-description/detail/${positionId}`]);
    }

    OnDelete(positionId) {
        this.confirmService.confirm(this.confirmService.tilte, this.confirmService.message, this.viewContainerRef)
            .subscribe(res => {
            let result = res;
            if (result) {
                 this.isProcessing=true;
                this.postionDescriptionTemplatesService.deletePosition(JSON.stringify({"pkCompanyPositionID":positionId})).subscribe(
                    d => {
                        if (d.code == "200") {
                            this.coreService.notify("Successful",d.message, 1);
                            this.getPositionDescriptionList();
                        }
                        else{
                          this.coreService.notify("Unsuccessful", d.message, 0);
                           this.isProcessing = false; 
                        console.log("error 1")                           
                        } 
                    },
                    error => {
                        
                        if(error.code == '400'){
                            this.isProcessing = false; 
                            this.coreService.notify("Unsuccessful", error.message, 0);
                        }

                    }
                    
                )
            }
        })
    }
  
}
