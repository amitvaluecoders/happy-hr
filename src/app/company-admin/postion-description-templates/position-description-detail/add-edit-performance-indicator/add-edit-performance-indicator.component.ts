import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { CoreService } from '../../../../shared/services/core.service';
import { PostionDescriptionTemplatesService } from '../../postion-description-templates.service';
import { MdSnackBar, MdDialog, MdDialogRef } from '@angular/material';

@Component({
  selector: 'app-add-edit-performance-indicator',
  templateUrl: './add-edit-performance-indicator.component.html',
  styleUrls: ['./add-edit-performance-indicator.component.scss']
})
export class AddEditPerformanceIndicatorComponent implements OnInit {
  public performanceIndicator:any;
  public performanceIndicatorObject:any;
  public isProcessing=false;
  public positionId:any;
  public isNew:Boolean=true;
  public buttionText:String; 
  constructor(public router:Router,public activatedRoute:ActivatedRoute,
              public coreService:CoreService,public dialog: MdDialog,
              public postionDescriptionTemplatesService:PostionDescriptionTemplatesService,public dialogRef: MdDialogRef<AddEditPerformanceIndicatorComponent>) { }

  ngOnInit() {
    if(this.performanceIndicator){
      this.isNew=false;
      this.performanceIndicatorObject=JSON.parse(JSON.stringify(this.performanceIndicator))
    }
    else this.setFormInitialValue();
    this.buttionText=(!this.isNew)?"Update":"Create";
    
  }
  setFormInitialValue(){
    this.performanceIndicatorObject={
      measure:"",title:"",duration:""
    }
  }

  addEditButton(isvalid){
    if(isvalid){
      this.isProcessing=true;
      let reqBody={isAdd:this.isNew,reqBody:{positionId:this.positionId,performanceIndicator: this.performanceIndicatorObject}}
      this.postionDescriptionTemplatesService.addEditperformanceIndicator(reqBody).subscribe(
          d => {
            if (d.code == "200"){
              this.isProcessing=false;
              d['title']=this.performanceIndicatorObject.title;
              d['duration']=this.performanceIndicatorObject.duration;
              d['measure']=this.performanceIndicatorObject.measure;
              this.dialogRef.close(d);
               this.coreService.notify("Successful", d.message, 1);
            } 
            else{
              this.isProcessing=false;
              this.coreService.notify("Unsuccessful", "Error while getting position details", 0);
            } 
          },
          error => {
            this.coreService.notify("Unsuccessful", "Error while getting position details", 0);
              this.isProcessing=false;            
          }
          
        ) 
    }
  }

  onCancel(){
     this.dialogRef.close(false);
  }

}
