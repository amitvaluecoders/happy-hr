import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddEditPerformanceIndicatorComponent } from './add-edit-performance-indicator.component';

describe('AddEditPerformanceIndicatorComponent', () => {
  let component: AddEditPerformanceIndicatorComponent;
  let fixture: ComponentFixture<AddEditPerformanceIndicatorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddEditPerformanceIndicatorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddEditPerformanceIndicatorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
