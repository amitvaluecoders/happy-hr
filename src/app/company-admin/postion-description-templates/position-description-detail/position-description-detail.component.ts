import { Component, OnInit, ViewContainerRef, ElementRef, ViewChild, Renderer2 } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { CoreService } from '../../../shared/services/core.service';
import { PostionDescriptionTemplatesService } from '../postion-description-templates.service';
import { AddEditPerformanceIndicatorComponent } from './add-edit-performance-indicator/add-edit-performance-indicator.component';
import { MdSnackBar, MdDialog, MdDialogRef } from '@angular/material';
import { ConfirmService } from '../../../shared/services/confirm.service';
import { APPCONFIG } from '../../../config';

declare let jsPDF;
@Component({
  selector: 'app-position-description-detail',
  templateUrl: './position-description-detail.component.html',
  styleUrls: ['./position-description-detail.component.scss']
})
export class PositionDescriptionDetailComponent implements OnInit {

  public hightlightStatus: Array<boolean> = [];

  highlightItem(event) {
    event.target.setAttribute("data-selected", "true");
  }

  isHighlighted(event) {
    return event.target.getAttribute("data-selected") == "true";
  }

  public reservedPerformanceindicators=[];
  public positionDescription: any;
  public isProcessing = false; 
   public userInfo:any;
  public hideForPDF = false;
  public isperformanceIndicatorMore=false;
  permission;

  @ViewChild('snapshot') snapshot: ElementRef;
  constructor(public router: Router, public activatedRoute: ActivatedRoute, public viewContainerRef: ViewContainerRef,
    public coreService: CoreService, public dialog: MdDialog, public confirmService: ConfirmService,
    public postionDescriptionTemplatesService: PostionDescriptionTemplatesService, public renderer: Renderer2) { }
  ngOnInit() {
    this.userInfo=JSON.parse(localStorage.getItem('happyhr-userInfo'));      
    this.getPositionDescriptionDetails();
    APPCONFIG.heading = "Position description details";
    this.permission=this.coreService.getNonRoutePermission();
  }

  // public generatePDF() {
  //   this.hideForPDF = true;
  //   let pdf = new jsPDF('p','pt', 'letter');
  //   let options = {
  //     pagesplit: true
  //   };

  //   pdf.internal.scaleFactor = 2.25;
  //   pdf.addHTML(this.el.nativeElement, 25, 0, options, () => {
  //     pdf.save(this.positionDescription.positionTitle + ".pdf");
  //     this.hideForPDF = false;
  //   });
  // }

  specialElementHandlers = {
    '#pdfheader': function (element, renderer) {
      return false;
    },
    '#pdfbtn': function (element, renderer) {
      return true;
    },
    '#pdftable': function (element, renderer) {
      return false;
    }
  };

  public generatePDF(position) {

    window.open(position.pdfUrl);
    // let pdf = new jsPDF('p', 'pt', 'letter');
    // let margins = {
    //   top: 50,
    //   bottom: 10,
    //   left: 40,
    //   width: pdf.internal.pageSize.width - 80
    // };

    // let source = this.snapshot.nativeElement.cloneNode(true);
    // source.querySelector('#notpdftable').remove();
    // this.renderer.setStyle(source.querySelector('#pdftable'), "display", "")

    // let options = {
    //   'width': margins.width,
    //   'elementHandlers': this.specialElementHandlers
    // };

    // pdf.fromHTML(source, margins.left, margins.top, options, () => {
    //   pdf.save(this.positionDescription.positionTitle + ".pdf");
    // }, margins);
  }


  getPositionDescriptionDetails() {
    this.isProcessing = true;
    this.postionDescriptionTemplatesService.getPostionDescriptionDetails(this.activatedRoute.snapshot.params['id']).subscribe(
      d => {
        if (d.code == "200") {
          this.positionDescription = d.data;
           this.positionDescription.performanceIndicator = this.positionDescription.performanceIndicator.filter(function (i) {
             i['isDeleteProcessing']=false;
            return true;
          })
        this.reservedPerformanceindicators=JSON.parse(JSON.stringify(d.data.performanceIndicator));
          if(d.data.performanceIndicator.length>10){
            this.isperformanceIndicatorMore=true;
            this.positionDescription.performanceIndicator=this.reservedPerformanceindicators.slice(0,10);
          }
          this.isProcessing = false;
          console.log("pi len",this.reservedPerformanceindicators)
        }
        else this.coreService.notify("Successful", d.message, 0);
      },
      error => this.coreService.notify("Successful", error.message, 0),
      () => { }
    )
  }

  onAddEditPerformanceIndicator(performanceIndicator) {
    let dialogRef = this.dialog.open(AddEditPerformanceIndicatorComponent);
    dialogRef.componentInstance.positionId = this.activatedRoute.snapshot.params['id'];
    if (performanceIndicator) dialogRef.componentInstance.performanceIndicator = performanceIndicator;
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        if(result.data)this.reservedPerformanceindicators.push(result.data);
        this.positionDescription.performanceIndicator=this.reservedPerformanceindicators;
        this.reservedPerformanceindicators=JSON.parse(JSON.stringify(this.positionDescription.performanceIndicator));
         if(this.isperformanceIndicatorMore && this.positionDescription.performanceIndicator.length>10){
            this.isperformanceIndicatorMore=true;
            this.positionDescription.performanceIndicator=this.positionDescription.performanceIndicator.slice(0,10);
          }
            if(performanceIndicator){
                for(let k of this.positionDescription.performanceIndicator){
               if(k.performanceId==performanceIndicator.performanceId){
                 k['title']=result['title'];
                 k['measure']=result['measure'];
                 k['duration']=result['duration'];
               }
               console.log("ddd",result)
             }
           } 
        this.reservedPerformanceindicators=JSON.parse(JSON.stringify(this.positionDescription.performanceIndicator));
           
         console.log("pd",this.positionDescription.performanceIndicator) ;
         
      }
    });
  }

  onDeletePerformanceIndicator(p, i) {
   
    this.confirmService.confirm(this.confirmService.tilte, this.confirmService.message, this.viewContainerRef)
      .subscribe(res => {
        let result = res;
        if (result) {
          p['isDeleteProcessing']=true;
          this.deletePerformanceIndicator(p, i);
        }
      })
  }

  onViewMore(){
     this.positionDescription.performanceIndicator=this.reservedPerformanceindicators;
     this.isperformanceIndicatorMore=false;
  }

  deletePerformanceIndicator(p, i) {
    this.hightlightStatus[i] = !this.hightlightStatus[i];
    this.postionDescriptionTemplatesService.deleteperformanceIndicator(p.performanceId).subscribe(
      d => {
        if (d.code == "200") {
          this.hightlightStatus[i] = !this.hightlightStatus[i];
          this.positionDescription.performanceIndicator = this.positionDescription.performanceIndicator.filter(function (item) {
            if (item.performanceId == p.performanceId){
              return false;
            } 
            else return true;
          })
         this.reservedPerformanceindicators=this.positionDescription.performanceIndicator;
          this.coreService.notify("Successful", d.message, 1);
           p['isDeleteProcessing']=false;
        }
        else {
          this.isProcessing = false;
          this.coreService.notify("Unsuccessful", "Error while getting position details", 0);
        }
      },
      error => {
        this.coreService.notify("Unsuccessful", "Error while getting position details", 0);
        this.isProcessing = false;
      }
    )
  }



}
