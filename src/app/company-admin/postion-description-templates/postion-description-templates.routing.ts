import {  ModuleWithProviders  } from '@angular/core';
import {  RouterModule,Routes  } from '@angular/router';
import { PositionDescriptionListComponent } from "./position-description-list/position-description-list.component";
import {PositionDescriptionDetailComponent} from "./position-description-detail/position-description-detail.component";
import {PositionDescriptionVersionComponent} from "./position-description-version/position-description-version.component";
import {PositionDescriptionAddEditComponent} from "./position-description-add-edit/position-description-add-edit.component";
import { PositionDescriptionGuardService } from './position-description-guard.service';
export const PostionDescriptionTemplatesPagesRoutes: Routes = [
    {
        path: '',
        canActivate:[],
        children: [
            // { path: '', redirectTo:'list'},
             { path:'', component: PositionDescriptionListComponent,canActivate:[PositionDescriptionGuardService]},
             { path:'detail/:id', component: PositionDescriptionDetailComponent,canActivate:[PositionDescriptionGuardService]},
             { path:'version',component:PositionDescriptionVersionComponent,canActivate:[PositionDescriptionGuardService]},
             { path:'add',component:PositionDescriptionAddEditComponent,canActivate:[PositionDescriptionGuardService]},
             { path:'edit/:id',component:PositionDescriptionAddEditComponent,canActivate:[PositionDescriptionGuardService]}
        ]
    }
];

export const PostionDescriptionTemplatesRoutingModule = RouterModule.forChild(PostionDescriptionTemplatesPagesRoutes);
