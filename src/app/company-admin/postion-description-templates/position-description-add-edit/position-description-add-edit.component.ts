import { Component, OnInit, ViewContainerRef } from "@angular/core";
import {
  trigger,
  state,
  style,
  transition,
  animate
} from "@angular/animations";
import { Router, ActivatedRoute, Params } from "@angular/router";
import { CoreService } from "../../../shared/services/core.service";
import { PostionDescriptionTemplatesService } from "../postion-description-templates.service";
import { APPCONFIG } from "../../../config";
import { ConfirmService } from "../../../shared/services/confirm.service";

@Component({
  selector: "app-position-description-add-edit",
  templateUrl: "./position-description-add-edit.component.html",
  styleUrls: ["./position-description-add-edit.component.scss"]
})
export class PositionDescriptionAddEditComponent implements OnInit {
  public originalPIPToggle = false;
  public isProcessing = false;
  public isAddForm = true;
  public industryTypeListSection = false;
  public selectedIndustry: any;
  public selectedPd: any;
  public loader: any;
  public industryToggle = true;
  public formToggle = true;
  public pdToggle = true;
  public industryTypesLoader = false;
  public pdTypesLoader = false;
  public selectIndustryInvalid = false;
  public buttonText = "";
  public isbuttonClicked = false;
  public contractTypeProcessing = false;
  public industryTypesList = [];
  public pdTypesList = [];
  public tempIndustryId = "";
  public contractTypes = [];
  public tempcompanyPositionLevel = {
    // "companyPositionLevelID": "3",
    levelTitle: "",
    contractType: [
      // {
      //   "contractTypeID": "1",
      //   "salary": "100"
      // },
      // {
      //   "contractTypeID": "2",
      //   "salary": "20"
      // },
      // {
      //   "contractTypeID": "3",
      //   "salary": "300"
      // }
    ]
  };

  public positionDescription = {
    companyPositionLevel: [],
    positionTitle: "",
    positionSummary: "",
    rolesAndResponsibilities: "",
    pastExperience: "",
    education: "",
    performanceIndicator: [
      {
        title: "",
        duration: "",
        measure: ""
      }
    ],
    industryID: "",
    subCompanyShow: "Yes"
  };
  constructor(
    public router: Router,
    public activatedRoute: ActivatedRoute,
    public confirmService: ConfirmService,
    public viewContainerRef: ViewContainerRef,
    public coreService: CoreService,
    public postionDescriptionTemplatesService: PostionDescriptionTemplatesService
  ) {}

  ngOnInit() {
    APPCONFIG.heading = "Update PD template";
    this.getAllIndustryTypes();
    if (this.activatedRoute.snapshot.params["id"] == undefined) {
      this.industryTypeListSection = true;
      APPCONFIG.heading = "Create new PD template";
    }

    this.getAllContractTypes();
  }

  onEditLevel(c) {
    this.isAddForm = false;
    this.tempcompanyPositionLevel = JSON.parse(JSON.stringify(c));
  }

  getAllContractTypes() {
    this.contractTypeProcessing = true;
    this.postionDescriptionTemplatesService
      .getcontractTypesforLevels()
      .subscribe(
        d => {
          if (d.code == "200") {
            this.contractTypes = d.data;
            for (let c of this.contractTypes) {
              this.tempcompanyPositionLevel.contractType.push({
                contractTypeID: c.contractTypeID,
                salary: ""
              });
            }
          } else
            this.coreService.notify(
              "Unsuccessful",
              "Error while getting industry list",
              0
            );
        },
        error =>
          this.coreService.notify(
            "Unsuccessful",
            "Error while getting industry list",
            0
          ),
        () => {}
      );
  }

  getPostionDescriptionForUpdate() {
    this.loader = this.postionDescriptionTemplatesService
      .getPostionDescriptionDetails(this.activatedRoute.snapshot.params["id"])
      .subscribe(
        d => {
          if (d.code == "200") {
            this.positionDescription = d.data;
            this.setSelectedIndustries(d.data.industryID);
            this.industryTypesLoader = false;
            for (let j of this.industryTypesList) {
              if (d.data.industryID == j.industryID) {
                this.positionDescription.industryID = this.tempIndustryId =
                  d.data.industryID;
                this.selectedIndustry = j.industryName;
              }
            }
            for (let c in this.positionDescription.companyPositionLevel) {
              this.positionDescription.companyPositionLevel[c]["tempID"] = c;
            }
          } else {
            this.coreService.notify("Unsuccessful", d.message, 0);
          }
        },
        error => {
          this.coreService.notify("Unsuccessful", error.message, 0);
        }
      );
  }

  setSelectedIndustries(ids) {
    let IDArray = ids.split(",");
    this.industryTypesList = this.industryTypesList.filter(function(item) {
      if (IDArray.includes(String(item.industryID))) item["isSelected"] = true;
      return item;
    });
  }

  getAllIndustryTypes() {
    this.industryTypesLoader = true;
    this.postionDescriptionTemplatesService.getAllIndustryTypes().subscribe(
      d => {
        if (d.code == "200") {
          this.industryTypesList = d.data.filter(function(item) {
            item["isSelected"] = false;
            return item;
          });

          if (this.activatedRoute.snapshot.params["id"] != undefined)
            this.getPostionDescriptionForUpdate();
          else this.industryTypesLoader = false;
        } else
          this.coreService.notify(
            "Unsuccessful",
            "Error while getting industry list",
            0
          );
      },
      error =>
        this.coreService.notify(
          "Unsuccessful",
          "Error while getting industry list",
          0
        ),
      () => {}
    );
  }

  trackByIndex(index: number, value: number) {
    return index;
  }

  // for adding Performance field.
  addPerformanceField() {
    let item = { title: "", duration: "", measure: "" };
    this.positionDescription.performanceIndicator.push(item);
    console.log("adddddddddd>>", this.positionDescription.performanceIndicator);
  }

  // for deleting Performance field.
  deletePerformanceField(index) {
    this.confirmService
      .confirm(this.confirmService.tilte, this.confirmService.message, this.viewContainerRef)
      .subscribe(res => {
        if (res) {
          this.positionDescription.performanceIndicator.splice(index, 1);
          // this.positionDescription.performanceIndicator = this.positionDescription.performanceIndicator.filter(val => val);
        }
      });
  }

  onIndusrtySelect(i) {
    this.selectIndustryInvalid = false;
    this.selectedIndustry = i.industryName;
    this.positionDescription.industryID = this.tempIndustryId = String(
      i.industryID
    );
    this.pdTypesLoader = true;
    this.industryToggle = !this.industryToggle;
    this.pdTypesList = [];
    this.postionDescriptionTemplatesService
      .getPositionDescriptionListByIndustry(i.industryID)
      .subscribe(
        d => {
          if (d.code == "200") {
            this.pdTypesList = d.data;
            this.pdTypesLoader = false;
          } else {
            this.pdTypesLoader = false;
            this.coreService.notify("Unsuccessful", d.message, 0);
          }
        },
        error => {
          this.pdTypesLoader = false;
          this.coreService.notify("Unsuccessful", error.message, 0);
        }
      );
  }

  onPdTypeSelect(i) {
    this.formToggle = false;
    this.pdToggle = !this.pdToggle;
    this.selectedPd = i.positionTitle;
    this.postionDescriptionTemplatesService
      .getPostionDescriptionDetailsByPositionID(i.positionId)
      .subscribe(
        d => {
          if (d.code == "200") {
            this.positionDescription = d.data;
            this.positionDescription["companyPositionLevel"] = [];
            // this.setSelectedIndustries(d.data.industryID);
            this.formToggle = true;
          } else
            this.coreService.notify(
              "Unsuccessful",
              "Error while getting industry list",
              0
            );
        },
        error =>
          this.coreService.notify(
            "Unsuccessful",
            "Error while getting industry list",
            0
          ),
        () => {}
      );

    // console.log(positionID)get-industry-position-detail/positionId
  }

  onCancel() {
    this.router.navigate(["/app/company-admin/position-description/"]);
  }

  addEditPositionDescription(isvalid) {
    
    this.positionDescription.performanceIndicator.filter(item=>{
      if(!item.duration){
        this.coreService.notify("Unsuccessful", "Please assign a item duration to a KPI.", 0);
        return ;
      }
    }) 
    
     if (isvalid) {
      this.isbuttonClicked = true;
      this.isbuttonClicked = true;
      this.selectIndustryInvalid = false;
      this.isProcessing = true;
      this.postionDescriptionTemplatesService.addEditPositionDescription(this.positionDescription).subscribe(
        d => {
          if (d.code == "200") {
            this.coreService.notify("Successful", d.message, 1);
            this.router.navigate([`/app/company-admin/position-description`]);
          }
          else {
            this.isProcessing = false;
            this.isbuttonClicked = false;
            this.coreService.notify("Unsuccessful", d.message, 0);
          }
        },
        error => {
          this.isbuttonClicked = false;
          this.isProcessing = false;
          this.coreService.notify("Unsuccessful", error.message, 0);
        }
      )
    }
  }

  onSelectIndustryChange() {
    let flag = true;
    for (let item of this.industryTypesList) {
      if (item.isSelected) flag = false;
    }
    if (flag) this.selectIndustryInvalid = true;
    else this.selectIndustryInvalid = false;
  }

  onAddLevel(type) {
    if (type == "edit") {
      this.positionDescription.companyPositionLevel = this.positionDescription.companyPositionLevel.filter(
        item => {
          if (item.tempID == this.tempcompanyPositionLevel["tempID"]) {
            item.companyPositionLevelID = this.tempcompanyPositionLevel[
              "companyPositionLevelID"
            ]
              ? this.tempcompanyPositionLevel["companyPositionLevelID"]
              : null;
            item["levelTitle"] = this.tempcompanyPositionLevel["levelTitle"];
            item.companyPositionLevelID = this.tempcompanyPositionLevel[
              "companyPositionLevelID"
            ]
              ? this.tempcompanyPositionLevel["companyPositionLevelID"]
              : null;
            for (let c in item["contractType"]) {
              item["contractType"][c].salary = this.tempcompanyPositionLevel[
                "contractType"
              ][c].salary;
            }
          }
          return item;
        }
      );
    } else {
      this.tempcompanyPositionLevel[
        "tempID"
      ] = this.positionDescription.companyPositionLevel.length;
      this.positionDescription.companyPositionLevel.push(
        JSON.parse(JSON.stringify(this.tempcompanyPositionLevel))
      );
    }
    this.tempcompanyPositionLevel.levelTitle = "";
    this.tempcompanyPositionLevel.contractType = this.tempcompanyPositionLevel.contractType.filter(
      d => {
        d["salary"] = "";
        d["companyPositionLevelID"] = "";
        return d;
      }
    );
    this.isAddForm = true;
  }
}
