import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PositionDescriptionAddEditComponent } from './position-description-add-edit.component';

describe('PositionDescriptionAddEditComponent', () => {
  let component: PositionDescriptionAddEditComponent;
  let fixture: ComponentFixture<PositionDescriptionAddEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PositionDescriptionAddEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PositionDescriptionAddEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
