import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DataTableModule } from "angular2-datatable";
import { PostionDescriptionTemplatesRoutingModule } from './postion-description-templates.routing';
import { PostionDescriptionTemplatesService } from './postion-description-templates.service';
import { PositionDescriptionListComponent } from './position-description-list/position-description-list.component';
import { PositionDescriptionDetailComponent } from './position-description-detail/position-description-detail.component';
import { PositionDescriptionVersionComponent } from './position-description-version/position-description-version.component';
import { PositionDescriptionAddEditComponent } from './position-description-add-edit/position-description-add-edit.component';
import { SharedModule } from '../../shared/shared.module';
import { FormsModule } from '@angular/forms';
import { MaterialModule } from '@angular/material';
import { AddEditPerformanceIndicatorComponent } from './position-description-detail/add-edit-performance-indicator/add-edit-performance-indicator.component';
import { PositionDescriptionGuardService } from './position-description-guard.service';
import { QuillModule } from 'ngx-quill';

@NgModule({
  imports: [
    QuillModule,
    MaterialModule,
    DataTableModule,
    FormsModule,
    SharedModule,
    CommonModule,
    PostionDescriptionTemplatesRoutingModule
  ],
  declarations: [PositionDescriptionListComponent,AddEditPerformanceIndicatorComponent, PositionDescriptionDetailComponent, PositionDescriptionVersionComponent, PositionDescriptionAddEditComponent],
  providers:[PostionDescriptionTemplatesService,PositionDescriptionGuardService],
  entryComponents:[AddEditPerformanceIndicatorComponent]
})
export class PostionDescriptionTemplatesModule { }
