import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-position-description-version',
  templateUrl: './position-description-version.component.html',
  styleUrls: ['./position-description-version.component.scss']
})
export class PositionDescriptionVersionComponent implements OnInit {
public versionList;
public tabing=false;
public currentTab=1.0;
constructor() { }

  ngOnInit() {
    this.versionList=[
      {version:1.3,date:"22 March 2017"},
      {version:1.2,date:"22 March 2016"},
      {version:1.1,date:"22 March 2015"},
      {version:1.0,date:"21 May 2017"},
    ];
  }
  onTabing(tab){
      this.currentTab=tab;
      this.tabing=!this.tabing;
  }

}
