import { Injectable } from '@angular/core';
import { RestfulLaravelService } from "../../shared/services/restful-laravel.service";

@Injectable()
export class SettingsService {

  constructor(private restfulService: RestfulLaravelService) { }

  getSubscriptionDetail() {
    return this.restfulService.get('get-next-payment-details');
  }

  changePassword(reqBody) {
    return this.restfulService.post('change-password', reqBody);
  }

  getExportedFile(){
    return this.restfulService.get(`csvCreate`);
  }
}