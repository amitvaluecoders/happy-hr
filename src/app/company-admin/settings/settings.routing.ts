import {  ModuleWithProviders  } from '@angular/core';
import {  RouterModule,Routes  } from '@angular/router';
import {  CpmAccountSettingsComponent } from './cpm-account-settings/cpm-account-settings.component';
import {  CmpModuleManagementComponent } from './cmp-module-management/cmp-module-management.component';

export const SettingsPagesRoutes: Routes = [
    {
        path: '',
        canActivate:[],
        children: [
           
            { path: '', loadChildren:'./setting-company-profile/setting-company-profile.module#SettingCompanyProfileModule'},
            {path:'accounts-settings',component:CpmAccountSettingsComponent},
            {path:'module-management', component:CmpModuleManagementComponent}

            
            
        ]
    }
];

export const SettingsRoutingModule = RouterModule.forChild(SettingsPagesRoutes);
