export class Profile {
    "firstName": string = "";
    "lastName": string = "";
    "emailID": string = "";
    "companyName": string = "";
    "companyLogo": string = "";
    "companyLogoUrl": string = "";
    "companyWebsite": string = "";
    "companyABNNumber": string = "";
    "companyNumber": string = "";
    "companyAdminEmail": string = "";
    "companyAbout": string = "";
    "companyStatus": string = "";
    "companyType": string = "";
    "streetAddress": string = "";
    "landmark": string = "";
    "city": string = "";
    "postalCode": string = "";
    "stateName": string = "";
    "countryName": string = "";
    "countryID": string = "";
    "awardID": string = "";
    "awardName": string = "";
    "questionair": any[];
    "paymentInfo" = {};
}

