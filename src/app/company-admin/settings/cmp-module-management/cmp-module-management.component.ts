import { Component, OnInit } from '@angular/core';
import { APPCONFIG } from '../../../config';

@Component({
  selector: 'app-cmp-module-management',
  templateUrl: './cmp-module-management.component.html',
  styleUrls: ['./cmp-module-management.component.scss']
})
export class CmpModuleManagementComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    APPCONFIG.heading="Module management";
  }

}
