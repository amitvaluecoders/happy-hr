import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { APPCONFIG } from '../../../config';
import { CoreService } from '../../../shared/services/core.service';
import { SettingsService } from "../settings.service";
import * as moment from "moment";

@Component({
  selector: 'app-cpm-account-settings',
  templateUrl: './cpm-account-settings.component.html',
  styleUrls: ['./cpm-account-settings.component.scss']
})
export class CpmAccountSettingsComponent implements OnInit {
  public user = { oldPassword: "", newPassword: "", confirmPassword: "" }

  subscription = { plan: "", cost: null, renewal: "" };
  isProcessing = false;
  isExportProcessing=false;
  isProcessingForm = false;

  constructor(private companyService: SettingsService, private router: Router, public coreService: CoreService) { }

  ngOnInit() {
    APPCONFIG.heading = "Account settings";
    this.getSubscriptionDetail();
  }

  onExport(){
    let userInfo=JSON.parse(localStorage.getItem('happyhr-userInfo'));
    if(userInfo){
       window.open(userInfo.exportUrl);
    }
   
    // this.isExportProcessing = true;
    // this.companyService.getExportedFile().subscribe(
    //   res => {
    //     this.isExportProcessing = false;
    //     if (res.code == 200) {}
    //     else return this.coreService.notify("Unsuccessful", res.message, 0);
    //   },
    //   err => {
    //     this.isExportProcessing = false;
    //     this.coreService.notify("Unsuccessful", err.message, 0);
    //   }
    // )
  }

  getSubscriptionDetail() {
    this.isProcessing = true;
    this.companyService.getSubscriptionDetail().subscribe(
      res => {
        this.isProcessing = false;
        if (res.code == 200) {
          this.subscription = {
            cost: Number(res.data.subscription_preview.current_billing_manifest.total_in_cents) / 100 || '',
            plan: res.data.subscription_preview.current_billing_manifest.line_items.filter(i => i.kind == "baseline")[0].memo || '',
            renewal: res.data.subscription_preview.current_billing_manifest.end_date || ''
          }
        }
        else return this.coreService.notify("Unsuccessful", res.message, 0);
      },
      err => {
        this.isProcessing = false;
        this.coreService.notify("Unsuccessful", err.message, 0);
      }
    )
  }

  formatDate(d) {
    return d ? moment(d).format('DD MMM YYYY hh:mm A') : '--';
  }

  changePassword() {
    this.isProcessingForm = true;
    this.companyService.changePassword(this.user).subscribe(
      res => {
        this.isProcessingForm = false;
        if (res.code == 200) {
          this.coreService.notify("Successful", res.message, 1);
          this.router.navigate(['/app/company-admin']);
        }
        else return this.coreService.notify("Unsuccessful", res.message, 0);
      },
      err => {
        this.isProcessingForm = false;
        this.coreService.notify("Unsuccessful", err.message, 0);
      }
    )
  }

}
