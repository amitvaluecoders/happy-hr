import {  ModuleWithProviders  } from '@angular/core';
import {  RouterModule,Routes  } from '@angular/router';
import {  SettingCompanyProfileComponentComponent } from './setting-company-profile-component/setting-company-profile-component.component';
import {  SettingsNotificationComponent } from './settings-notification/settings-notification.component';
import {  SupportServicesComponent } from './support-services/support-services.component';
import {  SettingsTransactionHistoryComponent } from './settings-transaction-history/settings-transaction-history.component';
import {  UpcomingPaymentsComponent } from './upcoming-payments/upcoming-payments.component'; 
import {  SettingsApiIntegrationComponent } from './settings-api-integration/settings-api-integration.component';

export const companyProfileRoutes: Routes = [   
            { path: '', component:SettingCompanyProfileComponentComponent},
            { path:'notifications', component:SettingsNotificationComponent},
            { path:'support', component:SupportServicesComponent },
            {path:'transaction', component:SettingsTransactionHistoryComponent},
            {path:'upcomming-payments',component:UpcomingPaymentsComponent},
            {path:'api-integration', component:SettingsApiIntegrationComponent}
];

export const companyProfileRoutingModule = RouterModule.forChild(companyProfileRoutes);
