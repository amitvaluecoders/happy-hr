import { Component, OnInit } from '@angular/core';
import { APPCONFIG } from '../../../../config';
import { CoreService } from '../../../../shared/services/core.service';
import { Router, ActivatedRoute } from '@angular/router';
import { ProfileSettingsService } from "../profile-settings.service";
import * as moment from 'moment';

@Component({
  selector: 'app-settings-notification',
  templateUrl: './settings-notification.component.html',
  styleUrls: ['./settings-notification.component.scss']
})
export class SettingsNotificationComponent implements OnInit {
  public notiList = [];
  public isProcessing : boolean;
  constructor(public coreService: CoreService , public router:Router
  ,public profileSettingsService : ProfileSettingsService) { }
  public params = { "offset":0 , "limit":20};
  ngOnInit() {
    APPCONFIG.heading="Notifications";  
     this.getNotiList();
    
  }



  formateDate(v){
      return moment(v).calendar()? moment(v).format('DD MMM YYYY') : 'Not specified';
  }

  getNotiList(){
      this.isProcessing = true;
   this.profileSettingsService.getAllNotification(JSON.stringify(this.params)).subscribe(
      res => {
        this.isProcessing = false;
        if (res.code == 200) {
          this.params.offset = this.params.offset + this.params.limit;
          Array.prototype.push.apply(this.notiList, res.data)
        }
        else return this.coreService.notify("Unsuccessful", res.message, 0);
      },
      err => {
        this.isProcessing = false;
        if (err.code == 400) {
          return this.coreService.notify("Unsuccessful", err.message, 0);
        }
        this.coreService.notify("Unsuccessful", "Error while getting data.", 0)
      }
    );
  }

}
