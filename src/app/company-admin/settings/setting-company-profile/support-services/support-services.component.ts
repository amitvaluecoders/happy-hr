import { Component, OnInit } from '@angular/core';
import { APPCONFIG } from '../../../../config';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { CoreService } from '../../../../shared/services/core.service';
import { IDatePickerConfig } from 'ng2-date-picker';
import * as moment from "moment";
import { ProfileSettingsService } from "../profile-settings.service";
import { Profile } from "../../settings";
import { MdSnackBar, MdDialog, MdDialogRef } from '@angular/material';
 
@Component({
  selector: 'app-support-services',
  templateUrl: './support-services.component.html',
  styleUrls: ['./support-services.component.scss']
})
export class SupportServicesComponent implements OnInit {
  public isCouponCodeValid=false;
  public isAgree=false;
  public isChkProcessing=false;
  public isProcessing =false;
  public isButtonClicked =false;
  public totalHours=[];
  public reqBody={
    supportServiceID:'',
    hour:null,
    taxRate:10,
    coupon:'',
    creditCardInfo:{
        full_number:'',
        expiration_month:'',
        expiration_year:'',
        billing_address:'',
        billing_city:'',
        billing_state:'',
        billing_country:'',
        billing_zip:''
    },
  };
 constructor(public companyService: ProfileSettingsService, public coreService: CoreService, private router: Router,
    private route: ActivatedRoute, public dialog: MdDialog) { }

  ngOnInit() {
    
    APPCONFIG.heading="Support services";
    while(this.totalHours.length<25) this.totalHours.push(this.totalHours.length);
  }


  onApplyCouponCode() {
    if(this.reqBody.coupon){
          this.isChkProcessing = true;
          this.companyService.checkCouponCode(this.reqBody.coupon).subscribe(
            d => {
              if (d.code == "200") {
                this.coreService.notify("Successful", d.message, 1);
                this.isChkProcessing = false;   
                this.isCouponCodeValid =true;
              }
              else {
                this.coreService.notify("Unsuccessful", d.message, 0);
                this.isChkProcessing = false;
              }
            },
            error => {
              this.isChkProcessing = false;
              this.coreService.notify("Unsuccessful", error.message, 0);              
            })
        }
  }

  onSubmit(isvalid){
    if(isvalid){
      this.isProcessing=true;
      this.companyService.submitSupportService(this.reqBody).subscribe(
        d => {
          if (d.code == "200") {
            this.coreService.notify("Successful", d.message, 1);
            this.router.navigate([`/`]); 
            this.isProcessing = false;    
          }
          else {
            this.coreService.notify("Unsuccessful", d.message, 0);
            this.isProcessing = false;
          }
        },
        error => {
          this.isProcessing = false;
          this.coreService.notify("Unsuccessful", error.message, 0);              
        })
    }
  }


}
