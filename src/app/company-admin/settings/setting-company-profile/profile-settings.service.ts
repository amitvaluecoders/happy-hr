import { Injectable } from '@angular/core';
import { RestfulLaravelService } from "../../../shared/services/restful-laravel.service";

@Injectable()
export class ProfileSettingsService {

  constructor(private restfulService: RestfulLaravelService) { }

  getProfileDetail() {
    return this.restfulService.get('get-company-profile-setting');
  }

  saveProfileDetail(reqBody) {
    return this.restfulService.put('update-company-information', reqBody);
  }

  getAwards(params) {
    return this.restfulService.get(`admin/award-list/${params}`);
  }

  uploadLogo(formData) {
    return this.restfulService.post('file-upload', formData);
  }

  getSecurityQuestions() {
    return this.restfulService.get(`get-security-question`);
  }

  getCountries() {
    return this.restfulService.get('country-list');
  }

  getAllNotification(params) {
    return this.restfulService.get(`notification-list/${encodeURIComponent(params)}`);
  }

  checkCouponCode(code){
    return this.restfulService.get(`validate-coupon/${code}`);    
  }

  submitSupportService(reqBody){
    return this.restfulService.post(`apply-subscription-support`,reqBody);    
  
  }
    getTranscationList(){
      return this.restfulService.get('transaction-history');
  }

  getUpcomingPayList(){
      return this.restfulService.get('get-next-payment-details');
  }

  getAllApiList(){
    return this.restfulService.get('api-list');
  }

  ondisconent(id){
    return this.restfulService.get(`disconnect-api/${id}`);
  }


}
