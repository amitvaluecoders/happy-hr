import { Component, OnInit } from '@angular/core';
import { APPCONFIG } from '../../../../config';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { CoreService } from '../../../../shared/services/core.service';
import { IDatePickerConfig } from 'ng2-date-picker';
import * as moment from "moment";
import { ProfileSettingsService } from "../profile-settings.service";
import { Profile } from "../../settings";
import { MdSnackBar, MdDialog, MdDialogRef } from '@angular/material';
import { CountryStateService } from "../../../../shared/services/country-state.service";

@Component({
  selector: 'app-setting-company-profile-component',
  templateUrl: './setting-company-profile-component.component.html',
  styleUrls: ['./setting-company-profile-component.component.scss']
})
export class SettingCompanyProfileComponentComponent implements OnInit {
  isProcessing = false;
  isProcessingForm = false;
  isProcessingImg = false;
  noResult = true;
  profile: Profile;
  options = {};
  errors = {};
  dateConfig: IDatePickerConfig = {};
  timeConfig: IDatePickerConfig = {};
  years = []; months = [];plans=[];
  dateTime = {};
  awardList = {};
  companyLogo = null;
  securityQuestions = [];
  countryList = [];
  stateList = [];
  disableState = true;

  constructor(public companyService: ProfileSettingsService, public coreService: CoreService, private router: Router,
    private route: ActivatedRoute, public dialog: MdDialog, public countryService: CountryStateService) {
    this.timeConfig.locale = this.dateConfig.locale = "en";
    this.dateConfig.format = "DD MMM YYYY hh:mm A";
    this.timeConfig.format = "hh:mm A";
    this.dateConfig.showMultipleYearsNavigation = true;
    this.dateConfig.weekDayFormat = 'dd';
    this.timeConfig.disableKeypress = this.dateConfig.disableKeypress = true;
    this.dateConfig.monthFormat = "MMM YYYY";
    // this.dateConfig.max = moment();
    this.profile = new Profile;
  }

  ngOnInit() {
    APPCONFIG.heading = "Company profile settings";
    this.getProfileDetail();
    this.getAwards();
    this.getExpiryDates();
    this.getSecurityQuestions();
    this.getCountriesLists();
  }

  getExpiryDates() {
    for (let i = 0; i <= 5; i++) {
      this.years.push(moment().year() + i);
    }
    this.months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
    this.plans = ['1','2','3','4'];
  }

  getSecurityQuestions() {
    this.companyService.getSecurityQuestions().subscribe(
      res => {
        if (res.code == 200) {
          this.securityQuestions = res.data;
        }
      }
    );
  }

  getCountriesLists() {
    // this.companyService.getCountries().subscribe(
    //   data => {
    //     if (data.code == 200) {
    //       this.countryList = data.data;
    //     }
    //   });
    this.countryList = this.countryService.getCountries();
  }

  onSelectCountry() {
    this.disableState = false;
    this.profile.stateName = '';
    this.stateList = this.countryService.getState(this.profile.countryID);
  }

  getAwards() {
    let params = { "filter": { "awards": {} } };
    this.companyService.getAwards(JSON.stringify(params)).subscribe(
      res => {
        if (res.code == 200) {
          this.awardList = res.data;
        }
      })
  }

  getProfileDetail() {
    this.isProcessing = true;
    this.companyService.getProfileDetail().subscribe(
      res => {
        this.isProcessing = false;
        if (res.code == 200) {
          this.noResult = false;
          this.profile = res.data;
          this.companyLogo = this.profile.companyLogoUrl;
          if (this.profile.countryID) {
            this.disableState = false;
            this.stateList = this.countryService.getState(this.profile.countryID);
          }
          this.validateOptions();
        }
        else return this.coreService.notify("Unsuccessful", res.message, 0);
      },
      err => {
        this.isProcessing = false;
        this.coreService.notify("Unsuccessful", err.message, 0);
      }
    )
  }

  validateOptions() {
    for (let i = 0; i < this.profile.questionair.length; i++) {
      this.profile.questionair[i]['other'] = this.profile.questionair[i]['other'] ? this.profile.questionair[i]['other'] : null;
      if (this.profile.questionair[i].field === "Option") {
        this.options[i] = this.profile.questionair[i].answer;
        if (this.profile.questionair[i].companyQuestionairID === 4) {
          for (let j = 0; j < this.profile.questionair[i].answer.length; j++) {
            if (this.profile.questionair[i].options.indexOf(this.profile.questionair[i].answer[j]) == -1) {
              this.profile.questionair[i].options.push(this.profile.questionair[i].answer[j]);
            }
          }
        }
      }
      if (this.profile.questionair[i].field == "Time") {
        // ===== oooooooooo ===========
        // this.profile.questionair[i].answer = this.profile.questionair[i].answer ? this.profile.questionair[i].answer :  [''];
        this.dateTime[i] = !this.profile.questionair[i].answer[0] ? moment() : moment(this.profile.questionair[i].answer[0], 'hh:mm A');
      }
      // if (this.profile.questionair[i].field == "Date") this.dateTime[i] = moment(this.profile.questionair[i].answer[0], 'DD MMM YYYY hh:mm A') || '';

    }
  }

  handleFileSelect($event) {
    const files = $event.target.files || $event.srcElement.files;
    const file: File = files[0];


    if(file.size>1961432){
      return this.coreService.notify("Unsuccessful", "File size is too big.Please reduce or choose another photo", 0)
    }
    let ext = file.name.substring(file.name.lastIndexOf('.')).toLowerCase();
    let _validFileExtensions = [".jpg", ".jpeg", ".png"];
    if (_validFileExtensions.indexOf(ext) >= 0) {
      const formData = new FormData();
      formData.append('companyLogo', file);

      this.isProcessingImg = true;
      this.companyService.uploadLogo(formData).subscribe(res => {
        this.isProcessingImg = false;
        this.profile.companyLogo = res.data.companyLogo;

        let reader = new FileReader();
        reader.onload = (e) => {
          this.companyLogo = reader.result;
        };
        reader.readAsDataURL(file);
      });
    } else return this.coreService.notify("Unsuccessful", "Invalid file extension.", 0)
  }

  save() {
    for (let key in this.options) {
      this.profile.questionair[key].answer = this.options[key];
    }
    if (!this.isValid()) return;
    console.log(this.profile);
    this.isProcessingForm = true;
    this.companyService.saveProfileDetail(this.profile).subscribe(
      res => {
        if (res.code == 200) {
          this.isProcessingForm = false;
          this.coreService.notify("Successful", res.message, 1);
        }
      },
      error => {
        this.isProcessingForm = false;
        this.coreService.notify('Unsuccessful', error.message, 0);
      })
  }

  onCheckOption(event, Qindex) {
    console.log('ooooooo =>  ', Qindex, this.options, event);
    this.errors['answer' + Qindex] = '';
    if (!this.options.hasOwnProperty(Qindex)) this.options[Qindex] = [];
    if (event.target.checked) {
      if (this.options[Qindex].length == 10) {
        this.coreService.notify('', 'You can only choose maximum ten options', 0);
        event.target.checked = false;
        return false;
      }
      if ((this.options[Qindex].length == 1) && (this.options[Qindex][0] == '')) this.options[Qindex] = [];
      this.options[Qindex].push(event.target.value);
    } else {
      let ind = this.options[Qindex].indexOf(event.target.value);
      if (ind >= 0) {
        this.options[Qindex].splice(ind, 1);
        // ===== oooooooooo ===========        
        // this.errors['answer' + Qindex] = (this.options[Qindex].length==0 || this.options[Qindex][0] == '') ? 'Required' : '';
        this.errors['answer' + Qindex] = this.options[Qindex].length == 0 ? 'Required' : '';
      }
    }
  }

  isValidQuestion(i) {
    let index;
    let qsn = this.profile.questionair[i];
    switch (qsn.companyQuestionairID) {
      case 36:
        index = this.profile.questionair.findIndex(i => i.companyQuestionairID == 35);
        return this.profile.questionair[index].answer.length && this.profile.questionair[index].answer[0] == 'Yes';
      case 37:
        index = this.profile.questionair.findIndex(i => i.companyQuestionairID == 35);
        return this.profile.questionair[index].answer.length && this.profile.questionair[index].answer[0] == 'Yes';
      case 38:
        index = this.profile.questionair.findIndex(i => i.companyQuestionairID == 35);
        return this.profile.questionair[index].answer.length && this.profile.questionair[index].answer[0] == 'Yes';
      case 40:
        index = this.profile.questionair.findIndex(i => i.companyQuestionairID == 39);
        return this.profile.questionair[index].answer.length && this.profile.questionair[index].answer[0] == 'Yes';
      case 42:
        index = this.profile.questionair.findIndex(i => i.companyQuestionairID == 41);
        return this.profile.questionair[index].answer.length && this.profile.questionair[index].answer[0] == 'Yes';
      case 44:
        index = this.profile.questionair.findIndex(i => i.companyQuestionairID == 43);
        return this.profile.questionair[index].answer.length && this.profile.questionair[index].answer[0] == 'Yes';
      case 46:
        index = this.profile.questionair.findIndex(i => i.companyQuestionairID == 45);
        return this.profile.questionair[index].answer.length && this.profile.questionair[index].answer[0] == 'Yes';
      case 48:
        index = this.profile.questionair.findIndex(i => i.companyQuestionairID == 47);
        return this.profile.questionair[index].answer.length && this.profile.questionair[index].answer[0] == 'Yes';
      case 50:
        index = this.profile.questionair.findIndex(i => i.companyQuestionairID == 49);
        return this.profile.questionair[index].answer.length && this.profile.questionair[index].answer[0] == 'Yes';
      case 52:
        index = this.profile.questionair.findIndex(i => i.companyQuestionairID == 51);
        return this.profile.questionair[index].answer.length && this.profile.questionair[index].answer[0] == 'Yes';

      default: return true;
    }
  }

  validate(value, field) {
    console.log('value', value, field)
    this.errors[field] = '';
    this.errors[field] = (value == '' || value == undefined || value == null) ? 'Required' : '';
  }

  isValid() {
    let count = 0;
    for (let i = 0; i < this.profile.questionair.length; i++) {
      // ===== oooooooooo ===========
      // if (this.isValidQuestion(i)) {
      //   this.profile.questionair[i].answer == "undefined" &&  count++;
      //   if(this.profile.questionair[i].answer && this.profile.questionair[i].answer.length && this.profile.questionair[i].answer[0] == ''){
      //     count++;
      //     this.validate(this.profile.questionair[i].answer, 'answer' + i);
      //   }
      // }
      if (this.isValidQuestion(i) && (this.profile.questionair[i].answer.length == 0 || this.profile.questionair[i].answer[0] == '')) {
        count++;
        this.validate(this.profile.questionair[i].answer[0], 'answer' + i);
      }
    }
    return count == 0;
  }

  isChecked(option, index) {
    return this.profile.questionair[index].answer.indexOf(option.toString()) >= 0;
  }

  onPlanDetails(){
    window.open("https://happyhr.com/pricing.html");
  }

  setTime(Qindex) {
    let answer = this.dateTime[Qindex];
    if (this.profile.questionair[Qindex].field == 'Date') {
      this.profile.questionair[Qindex].answer[0] = moment(answer).format('YYYY-MM-DD HH:mm:ss');
    } else if (this.profile.questionair[Qindex].field == 'Time') {
      this.profile.questionair[Qindex].answer[0] = moment(answer).format('HH:mm:ss');
    }
  }

  addValue() {
    let dialogRef = this.dialog.open(CompanyAwardDialog, { width: '400px' });
    dialogRef.componentInstance.displayType = 'addValue';
    dialogRef.afterClosed().subscribe(value => {
      if (value) {
        this.profile.questionair.filter(i => i.companyQuestionairID === 4).shift().options.push(value);
      }
    })
  }

  onClick() {
    let dialogRef = this.dialog.open(CompanyAwardDialog, { width: '900px', height: '500px' });
    dialogRef.componentInstance.awardList = this.awardList;
    dialogRef.componentInstance.displayType = 'award';
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        console.log('award ==> ', result)
        this.profile.awardID = result.awardID;
        this.profile.awardName = result.title;
      }
    });
  }

}


@Component({
  selector: 'company-award',
  template: `
  <md-progress-spinner *ngIf="isProcessing" mode="indeterminate" color=""></md-progress-spinner>
  <div *ngIf="!isProcessing && displayType=='award'" class="filter-wrapp box box-default contentBox">
    <div class="row">
        <div class="col-md-12">
            <ul class="awardsFilter">
                <li style="display:inline-block; margin:0px 7px 7px 0px;" *ngFor="let alpha of awards" (click)="toggleCollapse(alpha)">
                  <span class="badge badge-primary cursor" style="min-width:26px;">{{alpha}}</span>
                </li>
            </ul>
        </div>
    </div>
    <div class="AwardAlphabet-list">
      <div *ngFor="let alpha of awards" class="row">
        <div class="col-md-12" *ngIf="collapse[alpha]">
            <div class="box box-default full-width">
                <div class="box-header box-dark">{{alpha}}</div>
                <div class="box-body">
                    <ul>
                        <li *ngFor="let award of awardList[alpha]; let i=index;">
                            <div class="customchkbox">
                                <input [id]="'award'+award.awardID" type="radio" name="award" [value]="award.awardID" (change)="onCheckOption(i,alpha)"
                                    class="required">&nbsp;
                                <label [for]="'award'+award.awardID"></label>
                            </div>{{award.title}}
                        </li>
                    </ul>
                </div>
            </div>
        </div>
      </div>
    </div>
  </div>
  <div *ngIf="!isProcessing && displayType=='addValue'" class="filter-wrapp box box-default contentBox">
    <h3 md-dialog-title>Add your own value</h3>
    <div md-dialog-content style="padding-bottom: 20px;">
      <form #qsnForm="ngForm" name="material_signup_form" class="md-form-auth form-validation" (ngSubmit)="qsnForm.valid && onSubmit(value)">  
        <div class="full-width">
          <input mdInput  required rows="4" name="name" [(ngModel)]="value" #name="ngModel" placeholder="Enter Name">
        </div> 
        <div class="alert alert-danger" *ngIf="name.invalid && qsnForm.submitted">
            Name is required !
        </div>
      </form>       
    </div>
    <div>
        <button class="btn btn-primary btn-lg" type="submit" [disabled]="qsnForm.invalid" (click)="qsnForm.valid && onSubmit(value)">Submit</button>
        <button class="btn btn-default btn-lg" type="button" (click)="dialogRef.close()">Close</button>
    </div>
  </div>
  `,
  styles: [`
    .customchkbox{
      display:inline-block;
    }
    .customchkbox input + label {
        display: block;
    }
    
  `]
})
export class CompanyAwardDialog implements OnInit {
  public awards = [];
  public awardList = {};
  public isProcessing = false;
  public collapse = {};
  displayType = '';
  public value = "";

  constructor(public dialogRef: MdDialogRef<CompanyAwardDialog>, public awardService: ProfileSettingsService) { }

  ngOnInit() {
    this.getAwards();
    this.collapse['A'] = true;
  }

  toggleCollapse(alpha) {
    this.collapse = {};
    this.collapse[alpha] = true;
  }

  getAwards() {
    this.isProcessing = true;
    let arr = Object.keys(this.awardList);
    this.awards = arr.filter(i => { if (this.awardList[i].length > 0) return i; })
    this.isProcessing = false;
  }

  onCheckOption(index, alpha) {
    this.dialogRef.close(this.awardList[alpha][index]);
  }

  onSubmit(value) {
    this.dialogRef.close(value);
  }
}
