import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SettingCompanyProfileComponentComponent, CompanyAwardDialog } from './setting-company-profile-component/setting-company-profile-component.component';
import { companyProfileRoutingModule } from './setting-company-profile.routing';
import { SettingsNotificationComponent } from './settings-notification/settings-notification.component';
import { SupportServicesComponent } from './support-services/support-services.component';
import { SettingsTransactionHistoryComponent } from './settings-transaction-history/settings-transaction-history.component';
import { UpcomingPaymentsComponent } from './upcoming-payments/upcoming-payments.component';
import { SettingsApiIntegrationComponent } from './settings-api-integration/settings-api-integration.component';
import { FormsModule } from '@angular/forms';
import { ProfileSettingsService } from "./profile-settings.service";
import { MaterialModule } from "@angular/material";
import { SharedModule } from "../../../shared/shared.module";
import { CountryStateService } from "../../../shared/services/country-state.service";
import { QuillModule } from 'ngx-quill';
import { DataTableModule } from 'angular2-datatable';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    MaterialModule,
    SharedModule,
    QuillModule,
    companyProfileRoutingModule,
    DataTableModule
  ],
  declarations: [SettingCompanyProfileComponentComponent, SettingsNotificationComponent, SupportServicesComponent,
    SettingsTransactionHistoryComponent, UpcomingPaymentsComponent, SettingsApiIntegrationComponent, CompanyAwardDialog],
  providers: [ProfileSettingsService, CountryStateService],
  entryComponents: [CompanyAwardDialog]
})
export class SettingCompanyProfileModule { }
