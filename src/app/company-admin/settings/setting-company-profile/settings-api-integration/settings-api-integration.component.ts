import { Component, OnInit,ViewContainerRef } from '@angular/core';
import { APPCONFIG } from '../../../../config';
import { CoreService } from '../../../../shared/services/core.service';
import { Router, ActivatedRoute } from '@angular/router';
import { ProfileSettingsService } from "../profile-settings.service";
import { ConfirmService } from '../../../../shared/services/confirm.service';
import * as moment from 'moment';

@Component({
  selector: 'app-settings-api-integration',
  templateUrl: './settings-api-integration.component.html',
  styleUrls: ['./settings-api-integration.component.scss']
})
export class SettingsApiIntegrationComponent implements OnInit {

  public isProcessing=false;
  public list=[];
  public isActionProcessing=false;
  constructor(public coreService: CoreService , public router:Router,
    public profileSettingsService : ProfileSettingsService,
    public viewContainerRef: ViewContainerRef,
    public confirmService: ConfirmService) { }

  ngOnInit() {
    APPCONFIG.heading="API integration";
    this.getList();
  }

  getList(){
    this.isProcessing = true;
    this.profileSettingsService.getAllApiList().subscribe(
      res => {
        this.isProcessing = false;
        if (res.code == 200) {
          this.list=res.data?res.data:[]
        }
        else return this.coreService.notify("Unsuccessful", res.message, 0);
      },
      err => {
        this.isProcessing = false;
        if (err.code == 400) {
          return this.coreService.notify("Unsuccessful", err.message, 0);
        }
        this.coreService.notify("Unsuccessful", "Error while getting data.", 0)
      }
  );
}

  onAction(api){
    // companyID:JSON.parse(localStorage.getItem('happyhr-userInfo')).userID,
    // if(!api.status) var newWindow = window.open(api.link);
    //console.log("api.....",api);
    let warning= 
    "1.You must log out of your application " +  "("+api.name+")" + " to ensure successful integration.\n"+
    "2. Please ensure that the email address in the application " +  "("+api.name+")" + " is the same email address in Happy HR.\n"+ 
        "  The reason for this is the employee email is the “unique identifier” and if the email for the employee is different,\n"+
        "  it will push the information into your application creating a duplicate profile.\n"+"  Before you integrate please ensure the email addresses are the same in your application and Happy HR so you don’t cause an employee duplicate.\n"+
    "3. If you have done points one and two please click ok.\n"+
    "4. It can take 1 hour for the information to sync.\n";
    
    if(!api.status){
      this.confirmService.confirm(warning ,"",this.viewContainerRef).subscribe(
        confirm=>{
          
           if(confirm) window.location.href=api.link;
        },
        err => {
            if (err.code == 400) {
              return this.coreService.notify("Unsuccessful", err.message, 0);
            }
              this.coreService.notify("Unsuccessful", "Error while connecting the API.", 0);
             }
           )
         }

        else {
          api['isActionProcessing']= true;
          //this.isActionProcessing = true;
          this.profileSettingsService.ondisconent(api.id).subscribe(
            res => {
              if (res.code == 200) {
                api['isActionProcessing']= false;
                this.getList();
              }
              else return this.coreService.notify("Unsuccessful", res.message, 0);
            },
            err => {
              if (err.code == 400) {
                api['isActionProcessing']= false;
                return this.coreService.notify("Unsuccessful", err.message, 0);
              }
              this.coreService.notify("Unsuccessful", "Error while getting data.", 0)
            }
        );
        }
      }
       
  //   if(!api.status) window.location.href=api.link; 
  //   else {
  //     this.isActionProcessing = true;
  //     this.profileSettingsService.ondisconent(api.id).subscribe(
  //       res => {
  //         this.isActionProcessing = false;
  //         if (res.code == 200) {
  //           this.getList();
  //         }
  //         else return this.coreService.notify("Unsuccessful", res.message, 0);
  //       },
  //       err => {
  //         this.isActionProcessing = false;
  //         if (err.code == 400) {
  //           return this.coreService.notify("Unsuccessful", err.message, 0);
  //         }
  //         this.coreService.notify("Unsuccessful", "Error while getting data.", 0)
  //       }
  //   );
  //   }
  // }

}
