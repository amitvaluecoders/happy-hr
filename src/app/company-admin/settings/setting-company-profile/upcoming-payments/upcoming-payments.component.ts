import { Component, OnInit } from '@angular/core';
import { APPCONFIG } from '../../../../config';
import { CoreService } from '../../../../shared/services/core.service';
import { Router, ActivatedRoute } from '@angular/router';
import { ProfileSettingsService } from "../profile-settings.service";

@Component({
  selector: 'app-upcoming-payments',
  templateUrl: './upcoming-payments.component.html',
  styleUrls: ['./upcoming-payments.component.scss']
})
export class UpcomingPaymentsComponent implements OnInit {

   public transactionLists = [];
  public isProcessing;

  constructor(public coreService: CoreService , public router:Router,public profileSettingsService : ProfileSettingsService) { }

  ngOnInit() {
    APPCONFIG.heading="Upcoming payments";
    this.getTranscationList();
  }

  floatingNumber(n){
    return (n / 100).toFixed(2);
  }

  getTranscationList(){
      this.isProcessing = true;
   this.profileSettingsService.getUpcomingPayList().subscribe(
      res => {
        this.isProcessing = false;
        if (res.code == 200) {
          this.transactionLists = res.data.subscription_preview.current_billing_manifest.line_items;
          Array.prototype.push.apply(this.transactionLists , res.data.subscription_preview.next_billing_manifest.line_items);
        }
        else return this.coreService.notify("Unsuccessful", res.message, 0);
      },
      err => {
        this.isProcessing = false;
        if (err.code == 400) {
          return this.coreService.notify("Unsuccessful", err.message, 0);
        }
        this.coreService.notify("Unsuccessful", "Error while getting data.", 0)
      }
    );
  }


}
