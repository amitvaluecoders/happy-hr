import { Component, OnInit  } from '@angular/core';
import { APPCONFIG } from '../../../../config';
import { CoreService } from '../../../../shared/services/core.service';
import { Router, ActivatedRoute } from '@angular/router';
import { ProfileSettingsService } from "../profile-settings.service";

@Component({
  selector: 'app-settings-transaction-history',
  templateUrl: './settings-transaction-history.component.html',
  styleUrls: ['./settings-transaction-history.component.scss']
})
export class SettingsTransactionHistoryComponent implements OnInit {
  public transactionList = [];
  public isProcessing;

  constructor(public coreService: CoreService , public router:Router,public profileSettingsService : ProfileSettingsService) { }

  ngOnInit() {
    APPCONFIG.heading="Transaction history";
    this.getTranscationList();
  }

  floatingNumber(n){
    return (n / 100).toFixed(2);
  }

  getTranscationList(){
      this.isProcessing = true;
   this.profileSettingsService.getTranscationList().subscribe(
      res => {
        this.isProcessing = false;
        if (res.code == 200) {
          this.transactionList = res.data;
        }
        else return this.coreService.notify("Unsuccessful", res.message, 0);
      },
      err => {
        this.isProcessing = false;
        if (err.code == 400) {
          return this.coreService.notify("Unsuccessful", err.message, 0);
        }
        this.coreService.notify("Unsuccessful", "Error while getting data.", 0)
      }
    );
  }

}
