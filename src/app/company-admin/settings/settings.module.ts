import { NgModule } from '@angular/core';
import { MaterialModule } from "@angular/material";
import { CommonModule } from '@angular/common';
import { SettingsRoutingModule } from './settings.routing';
import { SettingsService } from './settings.service';
import { FormsModule } from '@angular/forms';
import { SettingCompanyProfileModule } from './setting-company-profile/setting-company-profile.module';
import { CpmAccountSettingsComponent } from './cpm-account-settings/cpm-account-settings.component';
import { CmpModuleManagementComponent } from './cmp-module-management/cmp-module-management.component';
import { SettingGuardService } from './setting-guard.service';
import { DataTableModule } from 'angular2-datatable';
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    MaterialModule,
    SettingsRoutingModule,
    SettingCompanyProfileModule,
    DataTableModule
  ],
  declarations: [CpmAccountSettingsComponent, CmpModuleManagementComponent],
  providers: [SettingsService, SettingGuardService]
})
export class SettingsModule { }
