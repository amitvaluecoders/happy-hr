import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditProfileSubcontractorComponent } from './edit-profile-subcontractor.component';

describe('EditProfileSubcontractorComponent', () => {
  let component: EditProfileSubcontractorComponent;
  let fixture: ComponentFixture<EditProfileSubcontractorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditProfileSubcontractorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditProfileSubcontractorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
