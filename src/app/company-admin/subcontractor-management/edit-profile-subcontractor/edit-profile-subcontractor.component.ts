import { Component, OnInit } from "@angular/core";
import { APPCONFIG } from "../../../config";
import { Router, ActivatedRoute } from "@angular/router";
import { CoreService } from "../../../shared/services/core.service";
import { SubcontractorManagementService } from "../subcontractor-management.service";
import { RestfulLaravelService } from "../../../shared/services/restful-laravel.service";
import { CountryStateService } from "../../../shared/services/country-state.service";

@Component({
  selector: 'app-edit-profile-subcontractor',
  templateUrl: './edit-profile-subcontractor.component.html',
  styleUrls: ['./edit-profile-subcontractor.component.scss']
})
export class EditProfileSubcontractorComponent implements OnInit {
disableState:boolean;
 public progress;
  constructor(
        public countryService: CountryStateService,
    public restfulWebService: RestfulLaravelService,
    public coreService: CoreService,
    public router: Router,
    public activatedRoute: ActivatedRoute,
    public service: SubcontractorManagementService
  ) {}
    public stateList =[];
  public isProcessing: Boolean;
  public isButtonClicked = false;
  public payment = "company";
  ownContract:any={};
  public ownContractList = [];
  contractTypeID:any;
  isActionProcessing = false;
  signatureText= '';
  public invite = {
    isInvited:0,
    companyName: "",
    ABN: "",
    address: "",
    suburb: "",
    stateName: "",
    email: "",
    phoneNumber: "",
    firstName: "",
    lastName: "",
    rateType: "",
    rateValue: "",
    scope: "",
    invoicePaid: "",
    postalCode: "",
    abnContractType: "",
    invoicePaidOther: "",
    contractFileName: "",
    nodeID: "",
    status:"",
    sign:"",
    signature:{
      type:'',
      text:''
    }
  };
  adoptSign = false;  

  public docPath = "";
  public docError = false;
  public fileName: any;

  dropdownSettings = {};

  listRateType = [];
  selectedRateType = [];

  listInvoiceType = [];
  selectedInvoiceType = [];
  selectedOwnContractType=[];

  listContractType = [];
  selectedContractType = [];
  LoggedInUser:any;

  ngOnInit() {
    this.stateList = this.countryService.getState(13);
    this.LoggedInUser = JSON.parse(localStorage.getItem("happyhr-userInfo"));
    APPCONFIG.heading = "General information";
    this.progressBarSteps();
    this.setDropdownItems();
    this.dropdownSettings = {
      singleSelection: true,
      text: "Select",
      enableSearchFilter: true,
      classes: "myclass"
    };
    this.getOwnContractList();
    this.getProfileInfo();
  }

  getProfileInfo(){
     let type = this.activatedRoute.snapshot.params['type'];
      let reqbody={
      id:this.activatedRoute.snapshot.params['id'],
      isInvited: type=='current'?0:1
    }
    this.service.getEmployeeProfile(reqbody).subscribe(
      d => {
        if (d.code == "200") {
          this.invite = d.data;
          this.selectedRateType = this.listRateType.filter(k=> k.value == this.invite.rateType);
          this.selectedInvoiceType = this.listInvoiceType.filter(k=> k.value == this.invite.invoicePaid);
          this.selectedContractType = this.listContractType.filter(k=> k.value == this.invite.abnContractType);
          if(this.selectedContractType[0].value=='Own_1' || this.selectedContractType[0].value=='Own_2'||this.selectedContractType[0].value=='Own_3'||this.selectedContractType[0].value=='Own_4'){
            this.contractTypeID='x';
            this.selectedOwnContractType[0].value=this.selectedContractType[0].value;
            this.selectedContractType[0].value='x';
          }
          this.isProcessing = false;
        }
      },
      err => {
        this.isProcessing = false;
        this.coreService.notify("Unsuccessful", err.message, 0);
      }
    );
  }

  selectSignature(sign) {
    this.invite.signature.type = sign;
    this.invite.signature.text = this.signatureText;
  }


  progressBarSteps() {
    this.progress = {
      progress_percent_value: 25, //progress percent
      total_steps: 3,
      current_step: 1,
      circle_container_width: 25, //circle container width in percent
      steps: [
        // {num:1,data:"Select Role"},
        { num: 1, data: "General informtion", active: true },
        { num: 2, data: "Contract approval" },
        { num: 3, data: "Policy approval" }
      ]
    };
  }

 setDropdownItems(){
    this.listRateType = [
      { id : 0, itemName : "Rate per hour " , value : "Per hour" },
      { id : 1, itemName : "Rate per hour +GST" , value : "Per hour GST" },
      { id : 2, itemName : "Rate per day " , value : "Per day" },
      { id : 3, itemName : "Rate per day +GST" , value : "Per day GST" },
      { id : 4, itemName : "Project fee " , value : "Project"},
      { id : 5, itemName : "Project fee + GST" , value : "Project GST"},
      { id : 6, itemName : "Percentage of sales", value : "Percentage"},
      { id : 7, itemName : "Percentage of consultation", value : "Percentage consulation"},
      { id : 8, itemName : "Percentage of GP", value : "Percentage GP"},


    ]

    this.listInvoiceType = [
      { id : 0, itemName : "Day of invoice" , value : "0" },
      { id : 1, itemName : "7 Days from invoice date" , value : "7" },
      { id : 2, itemName : "14 Days from invoice date" , value : "14" },
      { id : 3, itemName : "30 Days from invoice date" , value : "30" },
      { id : 4, itemName : "45 Days from invoice date" , value : "45" },
      { id : 5, itemName : "60 Days from invoice date" , value : "60" },
      { id : 6, itemName : "Other days from invoice date" , value : "-1" },
    ]

    this.listContractType = [
      { id : 0, itemName : "Use Happy HR ABN contract" , value : "HHR" },
      { id : 1, itemName : "Use own ABN contract" , value : "Own" },
      { id : 2, itemName : "Upload of current contract" , value : "Upload" },
      { id : 3, itemName : "Own Contract" , value: 'x'},
    ]
  }

  fileChange(event) {
    this.isProcessing = true;
    this.docError = false;
    this.fileName = "";
    this.docPath = "";
    let fileList: FileList = event.target.files;
    if (fileList.length > 0) {
      let file: File = fileList[0];
      let formData: FormData = new FormData();
      formData.append("eligibilityDoc", file);
      this.restfulWebService.post("file-upload", formData).subscribe(
        d => {
          if ((d.code = "200")) {
            this.isProcessing = false;
            this.docPath = d.data.eligibilityDoc;
            this.fileName = file.name;
          } else {
            this.docError = true;
            this.isProcessing = false;
          }
        },
        error => {
          this.docError = true;
          this.isProcessing = false;
        }
      );
    }
  }
  getOwnContractList(){
    this.isProcessing = true;
    this.service.getOwnContractForSubContractor().subscribe(
      res => {
        this.isProcessing = false;
        if (res.code == 200) {
          this.ownContract = res.data;
          this.ownContract.types.forEach((item,i)=>{
              if(item){
                    if(item=="Own"){
                      let data = {"id" : i,"itemName":item+"\t"+"contract","value":"Own_1"}
                      this.ownContractList.push(data);
                      console.log("list",this.ownContractList);  
                    }
                    else{
                      let name= item.split("_");
                      let data = {"id" : i,"itemName":name[0]+"\t"+"contract"+"\t"+name[1],"value":"Own"+"_" + name[1]}
                      this.ownContractList.push(data);
                    }
              }
              
          })
        }
      },
      err => {
        this.isProcessing = false;
      }
    )
  
  }
  onSelectContractType(item, selected) {
    this.contractTypeID = (selected) ? item.value : null;
  }
  

  register(form) {
    if (form.form.valid) {
      let type = this.activatedRoute.snapshot.params['type'];
        this.invite.isInvited = type=='current'?0:1;
      this.invite.rateType = this.selectedRateType[0].value;
      this.invite.invoicePaid = this.selectedInvoiceType[0].value;
      this.invite.abnContractType=(this.contractTypeID=='x') ? this.selectedOwnContractType[0].value : this.selectedContractType[0].value;      
      //this.invite.abnContractType = this.selectedContractType[0].value;
      this.invite.contractFileName = this.fileName;
      this.invite['userId'] = this.activatedRoute.snapshot.params['id'];
      this.isActionProcessing = true;
      this.service.updateEmployeeProfile(this.invite).subscribe(
        d => {
          if (d.code == "200") {
            this.router.navigate(["/app/company-admin/subcontractor-management"]);
            this.coreService.notify("Successful", d.message, 1);
            this.isActionProcessing = false;
          }
        },
        err => {
          this.isActionProcessing = false;
          this.coreService.notify("Unsuccessful", err.message, 0);
        }
      );
    }
  }
}
