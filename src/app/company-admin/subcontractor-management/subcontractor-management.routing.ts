import { ModuleWithProviders } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ListSubcontractorComponent } from './list-subcontractor/list-subcontractor.component';
import { EditProfileSubcontractorComponent } from './edit-profile-subcontractor/edit-profile-subcontractor.component';
import { UpdateProfileCurrentContractorComponent } from './update-profile-current-contractor/update-profile-current-contractor.component';

export const SubcontractorManagementPagesRoutes: Routes = [
    {
        path: '',
        canActivate: [],
        children: [

            { path: '', component: ListSubcontractorComponent },
            { path: 'profile/:id/:type', component: EditProfileSubcontractorComponent },
            { path: 'update/profile/:id', component: UpdateProfileCurrentContractorComponent },
            
        ]
    }
];

export const SubcontractorManagementRoutingModule = RouterModule.forChild(SubcontractorManagementPagesRoutes);
