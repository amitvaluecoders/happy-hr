import { Injectable } from '@angular/core';
import { RestfulLaravelService } from '../../shared/services/restful-laravel.service';

@Injectable()
export class SubcontractorManagementService {

  constructor(private restfulLaravelService: RestfulLaravelService) { }

  getContractorList(reqBody) {
    return this.restfulLaravelService.get(`contractor-list/${reqBody}`);
  }

  getFilters() {
    return this.restfulLaravelService.get('employee-listing-filter-data');
  }

  getEmployeeProfile(reqBody) {
    return this.restfulLaravelService.get(`contractor-profile/${JSON.stringify(reqBody)}`);
  }
  getAwards(params) {
    return this.restfulLaravelService.get(`admin/award-list/${params}`);
  }
  getOwnContract(){
    return this.restfulLaravelService.get('get-own-types-present');    
  }
  getOwnContractForSubContractor(){
    return this.restfulLaravelService.get('get-own-types-present/contractor');    
  }
  uploadDoc(formData) {
    return this.restfulLaravelService.post('upload-file', formData);
  }
  
   updateEmployeeProfile(reqBody) {
    return this.restfulLaravelService.post(`contractor-profile-edit`,reqBody);
  }
  
  getProfileInfo(id){
     return this.restfulLaravelService.get(`get-contractor-profile/${id}`);
  }

  acceptRejectRequest(reqBody){
     return this.restfulLaravelService.post(`contractor-approve-request`,reqBody);
  }

   registerSubcontractor(details) {
    return this.restfulLaravelService.post(`contractor/update-profile`, details);
  }

  }


