import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateProfileCurrentContractorComponent } from './update-profile-current-contractor.component';

describe('UpdateProfileCurrentContractorComponent', () => {
  let component: UpdateProfileCurrentContractorComponent;
  let fixture: ComponentFixture<UpdateProfileCurrentContractorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateProfileCurrentContractorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateProfileCurrentContractorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
