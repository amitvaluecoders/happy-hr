import { Component, OnInit, ViewContainerRef } from "@angular/core";
import { APPCONFIG } from "../../../config";
import { Router, ActivatedRoute } from "@angular/router";
import { CoreService } from "../../../shared/services/core.service";
import { SubcontractorManagementService } from '../subcontractor-management.service';
import { RestfulLaravelService } from "../../../shared/services/restful-laravel.service";
import { CountryStateService } from "../../../shared/services/country-state.service";
import { ConfirmService } from '../../../shared/services/confirm.service';
import { RequestChangePopupComponent } from '../../../user/subcontractor-induction/request-change-popup/request-change-popup.component';
import * as moment from 'moment';
import { IDatePickerConfig } from 'ng2-date-picker';
import { UploadMendatoryDocsPopupComponent } from '../../../user/subcontractor-induction/upload-mendatory-docs-popup/upload-mendatory-docs-popup.component';

import { MdDialog, MdDialogRef } from '@angular/material';
import { Profile, MyDocument, CompanyDocument } from '../../../company-employee/induction/induction';

@Component({
  selector: 'app-update-profile-current-contractor',
  templateUrl: './update-profile-current-contractor.component.html',
  styleUrls: ['./update-profile-current-contractor.component.scss']
})
export class UpdateProfileCurrentContractorComponent implements OnInit {

 public progress;
  collapse:any;
  ownContract:any={};
  public ownContractList = [];
  contractTypeID:any;
  errors=[];
  constructor(public viewContainerRef:ViewContainerRef,
    public confirmService: ConfirmService,  
    public dialog:MdDialog,
    public countryService: CountryStateService,
    public restfulWebService: RestfulLaravelService,
    public coreService: CoreService,
    public router: Router,
    public activatedRoute: ActivatedRoute,
    public service: SubcontractorManagementService
  ) {} 
    public stateList =[];
  public isProcessing: Boolean;
  public isButtonClicked = false;
  profileImage='';
  public payment = "company";
  public docRequired = false;
  isActionProcessing = false;
  isGetDetailsProcessing = false;
  signatureText= '';
  public invite = {
    contractorUserID:'',
    companyName: "",
    ABN: "",
    address: "",
    suburb: "",
    stateName: "",
    email: "",
    phoneNumber: "",
    firstName: "",
    lastName: "",
    rateType: "",
    rateValue: "",
    scope: "",
    invoicePaid: "",
    postalCode: "",
    abnContractType: "",
    invoicePaidOther: "",
    contractFileName: "",
    nodeID: "",
    sign:"", 
    signature:{
      type:'',
      text:''
    }, 
     mandatoryDocuments:[],
     employeeDocumentSubmit:[],
       bankDetail: {
          "bankName": "",
          "bsbNumber": "",
          "accountName": "",
          "accountNumber": ""
      }
  };
  adoptSign = false;  

  public docPath = "";
  public docError = false;
  public fileName: any;

  dropdownSettings = {};

  listRateType = [];
  selectedRateType = [];

  listInvoiceType = [];
  selectedInvoiceType = [];

  listContractType = [];
  listAllContractType =[];
  selectedContractType = [];
  employeeDocumentSigned=[];
  isProcessingImg=false;
  profile ={ 
    leaveType:[]
  };
  ngOnInit() {  
    this.collapse={      
      c1:true,c2:true,c3:true,c4:true,c5:true,c6:false
    }
    this.stateList = this.countryService.getState(13);
    APPCONFIG.heading = "General information";
    this.progressBarSteps();
    this.setDropdownItems();
    this.dropdownSettings = {
      singleSelection: true,
      text: "Select",
      enableSearchFilter: true,
      classes: "myclass"
    };
    this.getProfileInfo();
    this.getOwnContractList();
  }


  formatDateTime(date) {
    return moment(date).isValid() ? moment(date).format('DD MMM YYYY hh:mm A') : 'Not specified';
  }

  
   removeDoc(index) {
    this.confirmService.confirm(this.confirmService.tilte,this.confirmService.message,this.viewContainerRef)
    .subscribe(confirm=>{
            if(confirm){
              this.invite.employeeDocumentSubmit.splice(index, 1);             
            }
    })
  }

    getProfileInfo(){
     this.isGetDetailsProcessing = true;
    this.service.getProfileInfo(this.activatedRoute.snapshot.params['id']).subscribe(
      d => {
        if (d.code == "200") {
          this.invite = d.data;
          this.profile = d.data['0'];
          this.profile['leaveType'] = []; 
          this.selectedRateType = this.listRateType.filter(k=> k.value == this.invite.rateType);
          this.selectedInvoiceType = this.listInvoiceType.filter(k=> k.value == this.invite.invoicePaid);
          this.selectedContractType = this.listAllContractType.filter(k=> k.value == this.invite.abnContractType);  
          this.signatureText = this.invite.signature.text ? this.invite.signature.text : this.invite.firstName+" "+ this.invite.lastName;
          if(this.invite.mandatoryDocuments.length){
            this.flashRedWhenUpload();
          }
          this.invite.mandatoryDocuments =  this.invite.mandatoryDocuments.filter(k=>{
            k['isProcessing']= false;
            return k;
          });
          this.invite.contractorUserID = this.activatedRoute.snapshot.params['id'];
          this.profileImage=this.profile['personalInformation'].profileImageUrl;
          this.isGetDetailsProcessing = false;
        }
      },
      err => {
        this.isGetDetailsProcessing = false;
        this.coreService.notify("Unsuccessful", err.message, 0);
      }
    );
  }

  onAcceptReject(req,action){
      req['isProcessing'] = true;
      this.service.acceptRejectRequest({
      contractorChangeRequestID:req.contractorChangeRequestID,
      action:action}).subscribe(
      d => {
        if (d.code == "200") {
          this.coreService.notify("Successful", d.message, 1);  
          this.profile['contractorInformation'].Historical= d.data.Historical?d.data.Historical:[];
          this.profile['contractorInformation'].Pending= d.data.Pending?d.data.Pending:[];
          this.profile['contractorInformation'].Current= d.data.Current?d.data.Current:[];
          req['isProcessing'] = true;
          return false;
        }
      },
      err => {
          req['isProcessing'] = true;
        this.coreService.notify("Unsuccessful", err.message, 0);
      }
    );
    return;
  }
 

  selectSignature(sign) {
    this.invite.signature.type = sign;
    this.invite.signature.text = this.signatureText;
  }

    RequestForChange(req){
      console.log("ownContract--->",this.ownContractList);
      let dialogRef = this.dialog.open(RequestChangePopupComponent);
      dialogRef.componentInstance.listContractType = this.listContractType;
      dialogRef.componentInstance.listRateType = this.listRateType;
      dialogRef.componentInstance.ownContractList = this.ownContractList;      
      dialogRef.componentInstance.listInvoiceType = this.listInvoiceType;
      dialogRef.componentInstance.reqBody = JSON.parse(JSON.stringify(req));
      dialogRef.afterClosed().subscribe(r => {
        if (r) {
          this.profile['contractorInformation'] = r;
        }
      })

  } 


  progressBarSteps() {
    this.progress = {
      progress_percent_value: 25, //progress percent
      total_steps: 3,
      current_step: 1,
      circle_container_width: 25, //circle container width in percent
      steps: [
        // {num:1,data:"Select Role"},
        { num: 1, data: "General informtion", active: true },
        { num: 2, data: "Contract approval" },
        { num: 3, data: "Policy approval" }
      ]
    };
  }
 
      setDropdownItems(){
        this.listRateType = [
          { id : 0, itemName : "Rate per hour " , value : "Per hour" },
          { id : 1, itemName : "Rate per hour +GST" , value : "Per hour GST" },
          { id : 2, itemName : "Rate per day " , value : "Per day" },
          { id : 3, itemName : "Rate per day +GST" , value : "Per day GST" },
          { id : 4, itemName : "Project fee " , value : "Project"},
          { id : 5, itemName : "Project fee + GST" , value : "Project GST"},
          { id : 6, itemName : "Percentage of sales", value : "Percentage"},
          { id : 7, itemName : "Percentage of consultation", value : "Percentage consulation"},
          { id : 8, itemName : "Percentage of GP", value : "Percentage GP"},

        ]

        this.listInvoiceType = [
          { id : 0, itemName : "Day of invoice" , value : "0" },
          { id : 1, itemName : "7 Days from invoice date" , value : "7" },
          { id : 2, itemName : "14 Days from invoice date" , value : "14" },
          { id : 3, itemName : "30 Days from invoice date" , value : "30" },
          { id : 4, itemName : "45 Days from invoice date" , value : "45" },
          { id : 5, itemName : "60 Days from invoice date" , value : "60" },
          { id : 6, itemName : "Other days from invoice date" , value : "-1" },
        ]

        this.listContractType = [
          { id : 0, itemName : "Use Happy HR ABN contract" , value : "HHR" },
          { id : 1, itemName : "Use own ABN contract" , value : "Own" },
          { id : 2, itemName : "Upload of current contract" , value : "Upload" },
          { id : 3, itemName: "Own contract", value: "x"}
        ]
        this.listAllContractType = [
          { id : 0, itemName : "Use Happy HR ABN contract" , value : "HHR" },
          { id : 1, itemName : "Use own ABN contract" , value : "Own" },
          { id : 2, itemName : "Upload of current contract" , value : "Upload" },
          { id : 3, itemName: "Own contract", value: "Own_1"},
          { id : 4, itemName: "Own contract 2", value: "Own_2"},
          { id : 5, itemName: "Own contract 3", value: "Own_3"},
          { id : 6, itemName: "Own contract 4", value: "Own_4"},
                    
        ]
    }
    getOwnContractList(){
      this.isProcessing = true;
      this.service.getOwnContractForSubContractor().subscribe(
        res => {
          this.isProcessing = false;
          if (res.code == 200) {
            this.ownContract = res.data;
            this.ownContract.types.forEach((item,i)=>{
                if(item){
                      if(item=="Own"){
                        let data = {"id" : i,"itemName":item+"\t"+"contract","value":"Own_1"}
                        this.ownContractList.push(data);
                        console.log("list",this.ownContractList);  
                      }
                      else{
                        let name= item.split("_");
                        let data = {"id" : i,"itemName":name[0]+"\t"+"contract"+"\t"+name[1],"value":"Own"+"_" + name[1]}
                        this.ownContractList.push(data);
                      }
                }
                
            })
          }
        },
        err => {
          this.isProcessing = false;
        }
      )
    
    }

disp(){
  console.log("sdfkjhdskjf");
}
 handleFileSelect($event) {
    const files = $event.target.files || $event.srcElement.files;
    const file: File = files[0];
    let validFileExtensions = [".jpg", ".jpeg", ".png"];
    
    if(file.size>1961432){
      return this.coreService.notify("Unsuccessful", "The photo file is to large! Please use a file smaller that 2MB. Please try again.", 0)
    }

    let ext = file.name.substring(file.name.lastIndexOf('.')).toLowerCase();
    if (validFileExtensions.indexOf(ext) >= 0) {
      const formData = new FormData();
      formData.append('profileImage', file);
    
    this.isProcessingImg = true;
    this.restfulWebService.post("upload-file", formData).subscribe(res => {
      this.isProcessingImg = false;
      this.profileImage = res.data.profileImage;
      this.profile['personalInformation'].profileImage = res.data.fileName;
    });
    } else return this.coreService.notify("Unsuccessful", "Invalid file extension.", 0)
  }


  checkValidations(elArray){
    for(let e of elArray){
      if(e.invalid){
        //this.isAmountExist=true;
        return true;
      }   
    }
    return false;
  }

  register(form) {
    this.errors=[];
    this.errors = this.coreService.getFormValidationErrors(form);
    console.log("errrorrrrr",this.errors);
    if(this.errors.length>0){
     
    }
    if (this.errors && this.errors.length==0) {
      this.invite.rateType = this.selectedRateType[0].value;
      this.invite.invoicePaid = this.selectedInvoiceType[0].value;
      this.invite.abnContractType = this.selectedContractType[0].value; 
      this.invite.contractFileName = this.fileName;
      this.invite.nodeID = this.activatedRoute.snapshot.params["nodeID"];
      this.isActionProcessing = true;
      this.invite['profile']= this.profile;
      this.service.registerSubcontractor(this.invite).subscribe(
        d => {
          if (d.code == "200") {    
            this.router.navigate(['/app/company-admin/subcontractor-management']);
            this.coreService.notify("Successful", d.message, 1);
            this.isActionProcessing = false;
          }
        },
        err => {
        this.isActionProcessing = false;
          this.coreService.notify("Unsuccessful", err.message, 0);
        }
      );
    }
  
  }

  onViewContract(req){
    window.open(req.uploadFileUrl);
  }


onDelete(data){
  this.confirmService.confirm(
    this.confirmService.tilte,
    this.confirmService.message,
    this.viewContainerRef
  )
    .subscribe(res => {
      let result = res;
      if (result) {
        this.restfulWebService.delete(`contractor/change-request-delete/${data.contractorChangeRequestID}`)
          .subscribe(
          res => {
            if (res.code == 200) {
              this.coreService.notify("Success", res.message, 1);
              this.profile['contractorInformation'] = res.data;
            }
            else return this.coreService.notify("Unsuccessful", res.message, 0);
          },
          err => {
            if (err.code == 400) {
              return this.coreService.notify("Unsuccessful", err.message, 0);
            }
            this.coreService.notify("Unsuccessful", "Problem while performing action.", 0)
          }
          );
      }
    })


}
onpopup(type) {
  let dialogRef = this.dialog.open(UploadEmpDocumentDialog, { width: '500px' });
  dialogRef.componentInstance.type = type;
  dialogRef.afterClosed().subscribe((result) => {
    if (result) {
      console.log('result ==>> ', result);
      // if (type == 1) this.employeeDocumentSigned.push(result);
      if (type == 2) this.invite.employeeDocumentSubmit.push(result);
      else if (type == 'award') {
        // this.additionalInfo.awardIDs = result.awardID;
        // this.additionalInfo.awardName = result.title;
      }
    }
  });
}


removeManDoc(doc, index) {
  this.confirmService.confirm(this.confirmService.tilte,this.confirmService.message,this.viewContainerRef)
  .subscribe(res=>{
    if(res){
        doc['file']=''
        doc['expiryDate']='';
        this.flashRedWhenDelete();           
            
    }
  })       
}

//upload Mandatory documents
uploadDocs(doc,index) {  

  let dialogRef = this.dialog.open(UploadMendatoryDocsPopupComponent, { width: '500px' });
  dialogRef.componentInstance.doc=JSON.parse(JSON.stringify(doc));
    dialogRef.afterClosed().subscribe((result) => {
    if(result) {                                      
          doc['description'] = result['desc']; 
          doc['file'] = result['edsFile']; 
          doc['expiryDate'] = result['expiryDate'];
          this.flashRedWhenUpload();
    }}) 
  }

//flash red the mandatory field when file uploaded 
flashRedWhenUpload(){
  for(let item of this.invite.mandatoryDocuments){
    if(!item.file){
      this.docRequired = true;
      break;
    }
    else{
      this.docRequired = false;
    }
  } 
}

//flash red the mandatory field when file deleted 
flashRedWhenDelete(){
  this.invite.mandatoryDocuments.forEach(item=>{
    if(!item.file){
      this.docRequired=true;
      return;
    }
  })
}

}
@Component({
  selector: 'upload-document',
  template: `
      <div *ngIf="type !== 'award'">
        <h3>Upload document</h3>
        <form name="docForm" #docForm="ngForm" (ngSubmit)="docForm.valid && upload()">
          <div class="form-group">
            <label>Title</label>
            <input type="text" name="title" #title="ngModel" [(ngModel)]="doc.title" placeholder="Title ie. Driver's Licence" required/>
            <div *ngIf="title.dirty || docForm.submitted">
                <div class="color-danger" [hidden]="title.valid"> Please enter the title. </div>
            </div>
          </div>
          <div>
            <div class="form-group">
              <label>Description</label>
              
              <textarea type="text" placeholder="Description" rows="4" name="description" #description="ngModel" [(ngModel)]="doc.desc" required></textarea>
              <div *ngIf="description.dirty || description.touched || docForm.submitted">
                  <div class="color-danger" [hidden]="description.valid"> Please enter the description. </div>
              </div>
            </div>
          </div>
          <div>
            <div class="form-group">
              <label>Expiry</label>
              <dp-date-picker class="form-control" name="expiryDate" #expiryDate="ngModel" [theme]="'dp-material'" [mode]="'day'" [(ngModel)]="licExpiry" [config]="config" placeholder="Choose a date"></dp-date-picker>
            </div>
          </div>
          <div>
            <div class="uploadBtn">
              <label>{{fileName}}</label>&nbsp;&nbsp;
              <button type="button" class="btn-bs-file btn btn-lg btn-primary">
                Upload document
                <input type="file" accept=".doc,.docx,.pdf,image/png,image/jpg,image/jpeg" name="doc" (change)="handleFileSelect($event)" />
              </button>
            </div>
            <strong>{{uploadedFileName}}</strong>
            <p>Select file (JPEG, PNG, PDF or DOC files) </p>
            <div *ngIf="!doc.edsFile && docForm.submitted">
                  <div class="color-danger"> Please select a document. </div>
            </div>
          </div>
          <md-progress-spinner *ngIf="isProcessingImg" mode="indeterminate" color=""></md-progress-spinner>
          <button *ngIf="!isProcessingImg" type="submit" class="btn btn-primary">Submit</button>
        </form>
      </div>

      <div *ngIf="type == 'award'">
        <md-progress-spinner *ngIf="isProcessing" mode="indeterminate" color=""></md-progress-spinner>
        <div *ngIf="!isProcessing" class="filter-wrapp box box-default contentBox">
          <div class="row">
              <div class="col-md-12">
                  <ul class="awardsFilter">
                      <li style="display:inline-block; margin:0px 7px 7px 0px;" *ngFor="let alpha of awards" (click)="toggleCollapse(alpha)">
                        <span class="badge badge-primary cursor" style="min-width:26px;">{{alpha}}</span>
                      </li>
                  </ul>
              </div>
          </div>
          <div class="AwardAlphabet-list">
            <div *ngFor="let alpha of awards" class="row">
              <div class="col-md-12" *ngIf="collapse[alpha]">
                  <div class="box box-default full-width">
                      <div class="box-header box-dark">{{alpha}}</div>
                      <div class="box-body">
                          <ul>
                              <li *ngFor="let award of awardList[alpha]; let i=index;">
                                  <div class="customChoice" style="display:inline-block;">
                                      <input [id]="'award'+award.awardID" type="radio" name="award" [value]="award.awardID" (change)="onCheckOption(i,alpha)"
                                          class="required">
                                      <label style="display:block !important;" [for]="'award'+award.awardID">{{award.title}}</label>
                                  </div>
                              </li>
                          </ul>
                      </div>
                  </div>
              </div>
            </div>
          </div>
        </div>
      </div>
          `,
  styles: [`
      h3{
        font-size:22px;
      }
      .form-group label{
        font-size: 15px;
        color: #4b4c4d;
        max-width: 600px;
        width: 100%;
        padding-right: 18px;
        font-family: inherit;
        display: block;
        padding-right: 12px;
      }
      .form-group input{
        display: block;
        width: 100%;
        padding: 0.5rem 0.75rem;
        font-size: 1rem;
        line-height: 1.25;
        color: #464a4c;
        background-color: #fff;
        background-image: none;
        background-clip: padding-box;
        border: 1px solid rgba(0, 0, 0, 0.15);
        border-radius: 0.2rem;
        transition: border-color ease-in-out 0.15s, box-shadow ease-in-out 0.15s;
      }
      .form-group textarea{
         display: block;
         width: 100%;
         border: 1px solid rgba(0, 0, 0, 0.15);
        border-radius: 0.2rem;
        transition: border-color ease-in-out 0
      }
      .uploadBtn{
        float:left;
        width:100%;
      }
      .uploadBtn .btn-primary{
          padding:6px;
          float:left;
          width:100%;
          position:relative;
      }
      .uploadBtn input[type="file"]{
          width: 100%;
          position: absolute;
          float: left;
          height: 100%;
          left: 0;
          top: 0;
          opacity: 0;
        }
      .uploadBtn label {
          display: none;
      }
      .customChoice {
        position: relative;
        input {
            +label {
                padding-left: 28px;
                cursor: pointer;
                font-weight: normal !important;
                position: relative;
                display:block !important;
                &:before {
                    content: "";
                    height: 19px;
                    width: 19px;
                    display: inline-block;
                    border: 1px solid #acacac;
                    position: absolute;
                    left: 0px;
                    border-radius: 50%;
                    -webkit-border-radius: 50%;
                    -moz-border-radius: 50%;
                    -ms-border-radius: 50%;
                }
            }
            display:none;
            &:checked+label {
                position: relative;
                &:after {
                    content: "";
                    position: absolute;
                    height: 7px;
                    width: 7px;
                    display: inline-block;
                    border-radius: 50%;
                    -webkit-border-radius: 50%;
                    -moz-border-radius: 50%;
                    -ms-border-radius: 50%;
                    left: 6px;
                    top: 6px;
                    background: #444444;
                }
            }
        }
    }
  `]
})
export class UploadEmpDocumentDialog implements OnInit {
  doc: CompanyDocument;
  type = null;
  uploadedFileName:any;
  fileName = "";
  isProcessingImg = false;
  _validFileExtensions = [".jpg", ".jpeg", ".png", ".pdf", ".doc", ".docx"];

  licExpiry: any;
  config: IDatePickerConfig = {};

  public awards = [];
  public awardList = {};
  public isProcessing = false;
  public collapse = {};

  constructor(public dialog: MdDialogRef<UploadEmpDocumentDialog>, public coreService: CoreService, public service: SubcontractorManagementService) {
    this.doc = new CompanyDocument;
    this.config.locale = "en";
    this.config.format = "DD MMM YYYY";
    this.config.showMultipleYearsNavigation = true;
    this.config.weekDayFormat = 'dd';
    this.config.disableKeypress = true;
    this.config.monthFormat = "MMM YYYY";
    this.config.min = moment();
  }

  ngOnInit() {
   // this.getAwards();
    this.collapse['A'] = true;
  }

  handleFileSelect($event) {
    const files = $event.target.files || $event.srcElement.files;
    const file: File = files[0];
    this.fileName = file.name;
    if(file.size>1961432){
      this.dialog.close();
      return this.coreService.notify("Unsuccessful", " Uploaded document file is to large is must be a file smaller that 2MB. Please try again!", 0)
    }
    else{

    
    let ext = this.fileName.substring(this.fileName.lastIndexOf('.')).toLowerCase();
    if (this._validFileExtensions.indexOf(ext) >= 0) {
      const formData = new FormData();
      formData.append('userDocument', file);

      this.isProcessingImg = true;
      this.service.uploadDoc(formData).subscribe(res => {
        this.isProcessingImg = false;
        this.doc.edsFile = res.data.userDocument;
        this.uploadedFileName= res.data.fileName;
      });
    } else return this.coreService.notify("Unsuccessful", "Invalid file extension.", 0)
  }
  }

  upload() {
    //  || (this.type == 1 && !this.licExpiry)
    if (!this.doc.edsFile) return;
    this.doc.expiryDate = !(this.licExpiry) ? '' : moment(this.licExpiry).format('YYYY-MM-DD');
    this.dialog.close(this.doc);
  }


 
  // *************************************

  toggleCollapse(alpha) {
    this.collapse = {};
    this.collapse[alpha] = true;
  }

  getAwards() {
    this.isProcessing = true;
    let params = { "filter": { "awards": {} } };
    this.service.getAwards(JSON.stringify(params)).subscribe(
      res => {
        this.isProcessing = false;
        if (res.code == 200) {
          this.awardList = res.data;

          let arr = Object.keys(this.awardList);
          this.awards = arr.filter(i => { if (this.awardList[i].length > 0) return i; })
        }
      },
      error => {
        this.isProcessing = false;
        console.log(error);
      })
  }

  onCheckOption(index, alpha) {
    this.dialog.close(this.awardList[alpha][index]);
  }
}


