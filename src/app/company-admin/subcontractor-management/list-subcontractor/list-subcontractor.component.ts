import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SubcontractorManagementService } from '../subcontractor-management.service';
import { Pipe, PipeTransform } from '@angular/core';
import { APPCONFIG } from '../../../config';
import { CoreService } from "../../../shared/services/core.service";

@Component({
  selector: 'app-list-subcontractor',
  templateUrl: './list-subcontractor.component.html',
  styleUrls: ['./list-subcontractor.component.scss']
})
export class ListSubcontractorComponent implements OnInit {

 public searchUser = {};
  public toggleFilter = true;
  public employeeListings = [];
  public searchText = "";
  public isProcessing = false;
  isChecked = false;
  statusList = [];
  roleList = [];
  filter = { status: null };

  filters = {
    "status": { "Current contractor": false, "Incomplete induction": false, "Invited": false, "Terminated": false }
  }

  constructor(private service: SubcontractorManagementService, private router: Router, private coreService: CoreService) { }

  ngOnInit() {
    APPCONFIG.heading = "Contractor management";
    this.statusList = Object.keys(this.filters.status);
    // this.getFilters();
    this.getEmployeeListing();
  }

  getFilters() {
    // this.isProcessing = true;
    this.service.getFilters().subscribe(
      res => {
        // this.isProcessing = false;
        if (res.code == 200) {
          // this.filters = res.data;
          this.statusList = Object.keys(this.filters.status);
          // this.roleList = Object.keys(this.filters.roles);
        }
        else return this.coreService.notify('Unsuccessful', res.message, 0);
      },
      err => {
        // this.isProcessing = false;
        this.coreService.notify('Unsuccessful', err.message, 0);
      })
  }

  getEmployeeListing() {
    this.filter.status = this.filters.status;
    let params = { "search": this.searchText, "filter": this.filter }

    this.isProcessing = true;
    this.service.getContractorList(JSON.stringify(params)).subscribe(
      res => {
        this.isProcessing = false;
        if (res.code == 200) {
          this.employeeListings = res.data;
        }
        else return this.coreService.notify('Unsuccessful', res.message, 0);
      },
      err => {
        this.isProcessing = false;
        this.coreService.notify('Unsuccessful', err.message, 0);
      })
  }

  viewDetails(contractor) {
    if (contractor.contractorUserId) {
      this.router.navigate([`/app/company-admin/subcontractor-management/update/profile/${contractor.contractorUserId}`]);
    } else {
      this.router.navigate([`/app/company-admin/subcontractor-management/profile/${contractor.contractorInvitedId}`, 'invited']);
    }
  }

  editProfile(contractor) {
    this.viewDetails(contractor);
  }

  onEditEmployment(employee){  
        let id = employee.employeeInvitedID?employee.employeeInvitedID:employee.employeeUserID;
        this.router.navigate([`/app/company-admin/employee-management/general-information/${id}`]);            
    }

  onCheckEmp(event, id, type) {
    this.isChecked = true;
    let ind = this.filter[type].indexOf(id);

    if (event.target.checked) {
      (ind >= 0) ? null : this.filter[type].push(id);
    } else {
      (ind >= 0) ? this.filter[type].splice(ind, 1) : null;
    }
  }

}
