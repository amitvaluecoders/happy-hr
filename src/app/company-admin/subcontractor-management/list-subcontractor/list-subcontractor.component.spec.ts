import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListSubcontractorComponent } from './list-subcontractor.component';

describe('ListSubcontractorComponent', () => {
  let component: ListSubcontractorComponent;
  let fixture: ComponentFixture<ListSubcontractorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListSubcontractorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListSubcontractorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
