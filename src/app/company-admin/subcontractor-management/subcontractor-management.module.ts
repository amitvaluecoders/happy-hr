import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListSubcontractorComponent } from './list-subcontractor/list-subcontractor.component';
import { EditProfileSubcontractorComponent } from './edit-profile-subcontractor/edit-profile-subcontractor.component';
import { SubcontractorManagementRoutingModule } from './subcontractor-management.routing';
import { SubcontractorManagementService } from './subcontractor-management.service';
import { SharedModule } from '../../shared/shared.module';
import { CountryStateService } from '../../shared/services/country-state.service';
import { AngularMultiSelectModule } from 'angular2-multiselect-checkbox-dropdown/angular2-multiselect-dropdown';
import { FormsModule } from '@angular/forms';
import { MaterialModule } from '@angular/material';
import { DataTableModule } from "angular2-datatable";
//import { EditEmployeeProfileComponent, UploadEmpDocumentDialog } from './edit-employee-profile/edit-employee-profile.component';
//import { GeneralInformationComponent, AwardDialog } from './invite employee/general-information/general-information.component';

import { UpdateProfileCurrentContractorComponent,UploadEmpDocumentDialog } from './update-profile-current-contractor/update-profile-current-contractor.component';

@NgModule({
  imports: [
    SharedModule,
    FormsModule,
    MaterialModule,
    DataTableModule,
    AngularMultiSelectModule,
    SubcontractorManagementRoutingModule,
    CommonModule
  ],
  declarations: [ListSubcontractorComponent, UploadEmpDocumentDialog,EditProfileSubcontractorComponent, UpdateProfileCurrentContractorComponent],
  providers:[SubcontractorManagementService,CountryStateService],
  entryComponents: [UploadEmpDocumentDialog]
})
export class SubcontractorManagementModule { }
