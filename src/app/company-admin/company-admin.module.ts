import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CompanyAdminRoutingModule } from './company-admin.routing';
// import { CalendarModule } from "ap-angular2-fullcalendar";
import { CompanyAdminGuardService } from './company_admin_guard.service';
import { CompanyAdminService } from './company-admin.service';
import {  RestfulwebService  } from '../shared/services';
import { RestfulLaravelService } from '../shared/services';
// import { CreateDisciplinaryMeetingComponent } from './disciplinary-meeting/create-disciplinary-meeting/create-disciplinary-meeting.component';
import {FormsModule} from '@angular/forms';
import { MaterialModule } from '@angular/material';
import { SharedModule } from '../shared/shared.module';
import { AngularMultiSelectModule } from 'angular2-multiselect-checkbox-dropdown/angular2-multiselect-dropdown';
import { CompanyModuleListingService } from "./company-module-listing/company-module-listing.service";
@NgModule({
  imports: [
    MaterialModule,
    FormsModule,SharedModule,AngularMultiSelectModule,
    CommonModule,
    CompanyAdminRoutingModule,
  //  CalendarModule
  ],
  providers:[CompanyAdminGuardService,CompanyAdminService,RestfulLaravelService,RestfulwebService,CompanyModuleListingService
  ]
})
export class CompanyAdminModule { }
