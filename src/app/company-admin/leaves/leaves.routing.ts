import { ModuleWithProviders } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LeaveListComponent } from './leave-list/leave-list.component';
import { LeaveApplyPopupComponent } from './leave-apply-popup/leave-apply-popup.component';
import { LeavesGuardService } from "./leaves-guard.service";

export const LeavesPagesRoutes: Routes = [
    {
        path: '',
        canActivate: [],
        children: [
            { path: '', component: LeaveListComponent, canActivate: [LeavesGuardService] },
          
            
        ]
    }
];

export const LeavesRoutingModule = RouterModule.forChild(LeavesPagesRoutes);
