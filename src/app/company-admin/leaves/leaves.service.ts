import { Injectable } from '@angular/core';
import { RestfulLaravelService } from "../../shared/services/restful-laravel.service";

@Injectable()
export class LeavesService {

  constructor(private restfulLaravelService: RestfulLaravelService) { }

  getLeaveList(params) {
    return this.restfulLaravelService.get(`leave-list/${params}`);
  }

  updateLeaveStatus(reqBody) {
    return this.restfulLaravelService.post('accept-reject-leave', reqBody);
  }

  getEmployees() {
    return this.restfulLaravelService.get('show-all-employee');
  }

   deleteLeave(id) {
    return this.restfulLaravelService.delete(`deleteLeaves/${id}`);
  } 

}
