import { Component, OnInit } from '@angular/core';
import { MdSnackBar, MdDialog, MdDialogRef } from '@angular/material';
import { IDatePickerConfig } from 'ng2-date-picker';
import {Router} from '@angular/router';
import { EmployeeLeaves } from "../../../company-employee/employee-leaves/employee-leaves.service";
import { CoreService } from "../../../shared/services/core.service";
// import { CompanyAdminDashboardService } from "../../company-admin-dashboard/company-admin-dashboard.service";
import { LeavesService } from "../leaves.service";
import * as moment from "moment";

@Component({
  selector: 'app-leave-apply-popup',
  templateUrl: './leave-apply-popup.component.html',
  styleUrls: ['./leave-apply-popup.component.scss']
})
export class LeaveApplyPopupComponent implements OnInit {
  leaveTypes = [];
  noData:boolean=false;
  message:string;
  userId:any;
  role:string;
  selectedLeaveType = [];
  isProcessingEmp= false;
  dropdownListEmployees = [];
  respData={};
  dropdownSettings = {};
  dropdownSettingsEmp= {};
  config: IDatePickerConfig = {};
  public employeeList = [];
  selectedItems=[];
  isProcessing: boolean = false;
  isProcessingLeaveType: boolean = false;
  endDateErr = null;
  leave = {
    "title": "", "leaveTypeID": "", "comment": "",
    "startDate": moment().format('YYYY-MM-DD'), "endDate": moment().format('YYYY-MM-DD')
  }

  constructor(public companyService: LeavesService,public employeeservice: EmployeeLeaves, public router: Router,public coreService: CoreService, public dialogRef: MdDialogRef<LeaveApplyPopupComponent>) {
    this.config.locale = "en";
    this.config.format = "YYYY-MM-DD";
    this.config.showMultipleYearsNavigation = true;
    this.config.weekDayFormat = 'dd';
    this.config.disableKeypress = true;
    this.config.monthFormat = "MMM YYYY";
    // this.config.min = moment();
  }

  ngOnInit() {
    this.role = this.coreService.getRole();
    this.getEmployees();
    this.dropdownSettings = {
      singleSelection: true,
      text: "Select One",
      enableSearchFilter: true,
    };

    this.dropdownSettingsEmp = {
      singleSelection: true,
      text: "Select employee",
      enableSearchFilter: true,
      classes: "myclass assmng"
    };


    this.getLeaveTypes();
  }

  getLeaveTypes() {
    this.isProcessingLeaveType = true;
    this.leaveTypes = [];
    this.employeeservice.getLeaveTypesEmployee(this.userId).subscribe(
      res => {
        this.isProcessingLeaveType = false;
        if (res.code == 200) {
              //this.noData = false;
              if (res.data instanceof Array) {
                this.leaveTypes = res.data;
                this.leaveTypes.filter((item, i) => { item['itemName'] = item.title; item['id'] = item.leaveTypeID; });
              }
            }
            
        else{
          this.noData=true;
          this.message = res.message;
          return this.coreService.notify("Unsuccessful", res.message, 0);
        }
      },
      error => {
        this.isProcessingLeaveType = false;
        if (error.code == 400) {
          this.noData=true;
          this.message = error.message;
          
          //return this.coreService.notify("Unsuccessful", error.message, 0);
        }
        this.coreService.notify("Unsuccessful", "Error while getting leave list.", 0)
      }
    )
  }
  getEmployees() {
    this.isProcessingEmp = false;
    this.companyService.getEmployees().subscribe(
      res => {
        if (res.code == 200) {
          if (res.data instanceof Array) {
            this.employeeList = res.data;
            let tempEmployee=JSON.parse(JSON.stringify(this.employeeList));
            this.dropdownListEmployees=tempEmployee.filter(item =>{
              item['id'] = item.userId;
              item['itemName'] = item.name;
              item['isSelected'] = false;            
              return item;
          })
        this.isProcessingEmp = false;        
          }
        }
        else return this.coreService.notify("Unsuccessful", res.message, 0);
      },
      error => {
        this.isProcessingEmp = false;
        if (error.code == 400) {
          return this.coreService.notify("Unsuccessful", error.message, 0);
        }
        this.coreService.notify("Unsuccessful", "Error while getting leave list.", 0)
      }
    )
  }

  onItemSelect(e, selected) {
      console.log("emppppp",e);
      this.leave['employeeUserID'] = e.userId;
  }

  applyLeave() {
    this.leave['employeeUserID'] = this.userId;
    let startDate = moment(this.leave.startDate);
    let endDate = moment(this.leave.endDate);
    if (endDate < startDate) return this.endDateErr = 'End must be greater than start date';

    if(this.role != 'companyContractor') this.leave.leaveTypeID = this.selectedLeaveType[0].id;
    this.leave.startDate = startDate.isValid() ? startDate.format('YYYY-MM-DD') : '';
    this.leave.endDate = endDate.isValid() ? endDate.format('YYYY-MM-DD') : '';

    this.isProcessing = true;
    this.employeeservice.applyLeave(this.leave).subscribe(
      res => {
        this.isProcessing = false;
        if (res.code == 200) {
          console.log("responce......",res.data);
          this.dialogRef.close(res.message);
        }
        else return this.coreService.notify("Unsuccessful", res.message, 0);
      },
      error => {
        this.isProcessing = false;
        if (error.code == 400) {
          return this.coreService.notify("Unsuccessful", error.message, 0);
        }
        this.coreService.notify("Unsuccessful", "Error while applying for leave.", 0)
      }
    )
  }

  cancel() {
    this.dialogRef.close(false);
  }


 

}
