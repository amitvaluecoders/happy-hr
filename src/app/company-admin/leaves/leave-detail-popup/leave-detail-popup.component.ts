import { Component, OnInit } from '@angular/core';
import { MdSnackBar, MdDialog, MdDialogRef } from '@angular/material';
import { LeavesService } from "../leaves.service";
import { CoreService } from "../../../shared/services/core.service";
import * as moment from "moment";

@Component({
  selector: 'app-leave-detail-popup',
  templateUrl: './leave-detail-popup.component.html',
  styleUrls: ['./leave-detail-popup.component.scss']
})
export class LeaveDetailPopupComponent implements OnInit {
  display: string;
  leave = {};
  isProcessing: boolean;
  rejectNote = "";

  constructor(public companyService: LeavesService, public coreService: CoreService, public dialogRef: MdDialogRef<LeaveDetailPopupComponent>) { }

  ngOnInit() {
  }

  formatDate(date) {
    if (moment(date).isValid()) {
      return moment(date).format('DD MMM YYYY');
    }
  }

  cancel() {
    this.dialogRef.close(false);
  }

  reject() {
    this.display = 'reject';
  }

  onReject() {
    this.leave['status'] = 'Rejected';
    this.updateLeaveStatus();
  }

  onApprove() {
    this.leave['status'] = 'Approved';
    this.updateLeaveStatus();
  }

  updateLeaveStatus() {
    this.isProcessing = true;
    let reqBody = { leavesID: this.leave['leavesID'], note: this.rejectNote, status: this.leave['status'] };
    this.companyService.updateLeaveStatus(reqBody).subscribe(
      res => {
        if (res.code == 200) {
          this.isProcessing = false;
          this.dialogRef.close(res.message);
        }
        else {
          this.coreService.notify("Unsuccessful", res.message, 0);
          this.dialogRef.close(false);
        }
      },
      error => {
        this.isProcessing = false;
        if (error.code == 400) {
          this.coreService.notify("Unsuccessful", error.message, 0);
          return this.dialogRef.close(false);
        }
        this.coreService.notify("Unsuccessful", "Error while getting leave list.", 0)
        this.dialogRef.close(false);
      }
    )
  }

}
