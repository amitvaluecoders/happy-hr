import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { MaterialModule } from "@angular/material";
import { LeavesRoutingModule } from './leaves.routing';
import { LeavesService } from './leaves.service';
import { LeaveListComponent } from './leave-list/leave-list.component';
import { LeaveDetailPopupComponent } from './leave-detail-popup/leave-detail-popup.component';
import { DataTableModule } from "angular2-datatable";
import { SharedModule } from "../../shared/shared.module";
import { LeavesGuardService } from './leaves-guard.service';
//import { LeaveApplyPopupComponent } from '../../company-employee/employee-leaves/leave-apply-popup/leave-apply-popup.component';
// import {CalendarComponent} from "angular2-fullcalendar/src/calendar/calendar";
import { AngularMultiSelectModule } from 'angular2-multiselect-checkbox-dropdown/angular2-multiselect-dropdown';
import { EmployeeLeaves } from '../../company-employee/employee-leaves/employee-leaves.service';
import { LeaveApplyPopupComponent } from './leave-apply-popup/leave-apply-popup.component';





@NgModule({
  imports: [
    AngularMultiSelectModule,
    CommonModule,
    LeavesRoutingModule,
    MaterialModule,
    FormsModule,
    DataTableModule,
    SharedModule
  ],

  declarations: [LeaveListComponent, LeaveDetailPopupComponent, LeaveApplyPopupComponent],
  entryComponents: [LeaveDetailPopupComponent,LeaveApplyPopupComponent],
  providers: [LeavesService,LeavesGuardService,EmployeeLeaves]
})
export class LeavesModule { }
