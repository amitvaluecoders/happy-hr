import { Component, OnInit ,ViewContainerRef} from '@angular/core';
import { MdSnackBar, MdDialog, MdDialogRef } from '@angular/material';
import {Router} from '@angular/router';
import { APPCONFIG } from '../../../config';
import { LeaveDetailPopupComponent } from '../leave-detail-popup/leave-detail-popup.component';
import { LeavesService } from "../leaves.service";
import { CoreService } from "../../../shared/services/core.service";
import { ConfirmService } from '../../../shared/services/confirm.service';
import { EmployeeListingPopupComponent } from '../../company-module-listing/employee-listing-popup/employee-listing-popup.component';
import { LeaveApplyPopupComponent } from '../leave-apply-popup/leave-apply-popup.component';
import * as moment from "moment";
import { Message } from '@angular/compiler/src/i18n/i18n_ast';

@Component({
  selector: 'app-leave-list',
  templateUrl: './leave-list.component.html',
  styleUrls: ['./leave-list.component.scss']
})
export class LeaveListComponent implements OnInit {
  
  dropdownListEmployees = [];
  selectedItems = [];
  selectedEmployee = [];
  tempLeaveList:any;
  isCalanderProcessing=false;
  public isEmployeeListProcessing=false;
  dropdownSettings = {};

  LeaveListCalander=[];
  public calendarOptions={
        header: {
        left: 'prev,next today',
        center: 'title',
        right: 'month,agendaWeek,agendaDay'
        },
        height: '5%',
        width:'70%',
        eventColor: '#64c3fb',
        fixedWeekCount : false,
        defaultDate: new Date(),
        eventLimit: true, 
        editable: true,
        events: []
  };
  public toggleFilter = false;
  public isProcessing: boolean;
  public isProcessingEmp: boolean;
  public isChecked = false;
  public leaveList = [];
  public employeeList = [];
  public empSearch = "";
  public filter = { 'status': { 'Approved': false, 'Rejected': false, 'Pending': false, 'Cancelled': false }, 'userSearch': [] };
  public permission;

  constructor(public viewContainerRef:ViewContainerRef,public confirmService:ConfirmService,public dialog: MdDialog, public companyService: LeavesService, public coreService: CoreService,public router : Router ) { }

  ngOnInit() {
    this.dropdownSettings = {
      singleSelection: true, 
      text:"Select employee",
      enableSearchFilter: true,
      classes:"myclass assmng"
   };
   
    APPCONFIG.heading = "Leaves";
    this.permission = this.coreService.getNonRoutePermission();
    this.getLeaveList();
    this.getEmployees();
  }

  onPopUpEmp(moduleName) {
    let dialogRef = this.dialog.open(EmployeeListingPopupComponent);
    dialogRef.componentInstance.moduleName = moduleName;
    dialogRef.afterClosed().subscribe(message => {
      if (message) {
        this.getLeaveList();
        return this.coreService.notify("Successful", message, 1);
      }
    });
  }
 

  gotoApplyPopUp(){
   let dialogRef = this.dialog.open(LeaveApplyPopupComponent);   
   dialogRef.afterClosed().subscribe(message => {
     if (message) {
     
       
     }
   });
  }


  onPopUp(type, leave) {
    let index = this.leaveList.findIndex(i => i.leavesID == leave.leavesID);
    let dialogRef = this.dialog.open(LeaveDetailPopupComponent);
    dialogRef.componentInstance.display = type;
    dialogRef.componentInstance.leave = this.leaveList[index];
    dialogRef.afterClosed().subscribe(message => {
      if (message) {
        this.toggleFilter = false;
        this.coreService.notify('Successful', message, 1);
        this.getLeaveList();
      }
    });
  }

  onDelete(leave){
    let message =`Do you want to delete this item?<br>
     You need to delete the same in integrated APIs(XERO,Myob,keypay,deputy,tanda,quickbooks) if connected.`;
     this.confirmService.confirm(this.confirmService.tilte, message, this.viewContainerRef)
      .subscribe(res => {
        let result = res;
        if (result) {
          leave['isProcessing'] = true;
            this.companyService.deleteLeave(leave.leavesID).subscribe(
              res => {
                
                if (res.code == 200) {
                  leave['isProcessing'] = true;
                  this.coreService.notify("Successful", res.message, 1);
                   this.getLeaveList();
                }
                else  this.coreService.notify("Unsuccessful", res.message, 0);
               leave['isProcessing'] = false;
                
              },
              error => {
               leave['isProcessing'] = false;
                
                this.coreService.notify("Unsuccessful", "Error while deleting leave.", 0)
              }
            )
        }
      })
  }

  getEmployees() {
    this.isProcessingEmp = false;
    this.companyService.getEmployees().subscribe(
      res => {
        if (res.code == 200) {
          if (res.data instanceof Array) {
            this.employeeList = res.data;
            let tempEmployee=JSON.parse(JSON.stringify(this.employeeList));
            this.dropdownListEmployees=tempEmployee.filter(item =>{
              item['id'] = item.userId;
              item['itemName'] = item.name;
              item['isSelected'] = false;            
              return item;
          })
        this.isProcessingEmp = false;        
          }
        }
        else return this.coreService.notify("Unsuccessful", res.message, 0);
      },
      error => {
        this.isProcessingEmp = false;
        if (error.code == 400) {
          return this.coreService.notify("Unsuccessful", error.message, 0);
        }
        this.coreService.notify("Unsuccessful", "Error while getting leave list.", 0)
      }
    )
  }

  getLeaveList() {
    this.isProcessing = true;
    let params = { "filter": { 'status': this.filter.status }, 'userSearch': this.filter.userSearch };
    this.leaveList = [];
    this.companyService.getLeaveList(JSON.stringify(params)).subscribe(
      res => {
        
        if (res.code == 200) {
          if (res.data instanceof Array) {
            this.leaveList = res.data;
            this.leaveList.filter(i => { i['count'] = this.countDays(i.startDate, i.endDate); });
            this.tempLeaveList = JSON.parse(JSON.stringify(this.leaveList));
            this.LeaveListCalander = JSON.parse(JSON.stringify(this.leaveList));
          }
          this.setCalanderData();
          this.isProcessing = false;
        }
        else return this.coreService.notify("Unsuccessful", res.message, 0);
      },
      error => {
        this.isProcessing = false;
        if (error.code == 400) {
          return this.coreService.notify("Unsuccessful", error.message, 0);
        }
        this.coreService.notify("Unsuccessful", "Error while getting leave list.", 0)
      }
    )
  }

  onItemSelect(e){
    this.isCalanderProcessing=true;
    let leaves = JSON.parse(JSON.stringify(this.tempLeaveList));
    this.LeaveListCalander=[];
    this.LeaveListCalander=leaves.filter(item=>{
      if(e.id == item.leaveAppliedBy.userID) return item;
    });
    this.setCalanderData();
  }


 

  isString(data) {
    return typeof data == 'string';
  }

  formatDate(date) {
    if (moment(date).isValid()) {
      return moment(date).format('DD MMM YYYY');
    }
  }

  countDays(start, end) {
    return moment(end).diff(moment(start), 'days') + 1;
  }

  onCheckEmp(event, id) {
    this.isChecked = true;
    let ind = this.filter.userSearch.indexOf(id);

    if (event.target.checked) {
      (ind >= 0) ? null : this.filter.userSearch.push(id);
    } else {
      (ind >= 0) ? this.filter.userSearch.splice(ind, 1) : null;
    }
  }

  setCalanderData(){
    this.isCalanderProcessing=true;
    setTimeout(() => {
      this.calendarOptions.events=[];
      this.LeaveListCalander.filter(d=>{
        let color='#fbe9b1';
        if(d.status=='Cancelled') color='#ffe4c3';
        if(d.status=='Approved') color='#b3f3dd';
        if(d.status=='Rejected') color='#f9d2d2';
        this.calendarOptions.events.push(
          {
            title:d.leaveAppliedBy.name+" (" +d.title+")",
            start:d.startDate,
            end:d.endDate2?d.endDate2:d.endDate,
            backgroundColor:color,
          }
        )
      })
    this.isCalanderProcessing=false;
    },500)
   
  }
  

}
