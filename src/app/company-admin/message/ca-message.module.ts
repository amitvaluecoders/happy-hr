import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { CaMessageRoutingModule } from './ca-message.routing';
import { CompanyAdminMessageService } from './ca-message.service';
import { CompanyAdminMessagesComponent } from './messages/messages.component';
import { NewMessageComponent } from './new-message/new-message.component';
import { GroupMessageComponent } from './group-message/group-message.component';
import { AngularMultiSelectModule } from 'angular2-multiselect-checkbox-dropdown/angular2-multiselect-dropdown';
import { SharedModule } from '../../shared/shared.module';
import { MaterialModule } from '@angular/material';
import { MessageGuardService } from './message-guard.service';
@NgModule({
  imports: [
    CommonModule,
    CaMessageRoutingModule,
    FormsModule,
    AngularMultiSelectModule,
    SharedModule,
    MaterialModule
  ],
  declarations: [CompanyAdminMessagesComponent, NewMessageComponent, GroupMessageComponent],
  providers:[CompanyAdminMessageService,MessageGuardService]
})
export class companyAdminMessageModule { }
