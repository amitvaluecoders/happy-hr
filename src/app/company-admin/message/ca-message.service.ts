import { Injectable } from '@angular/core';
import { RestfulLaravelService} from '../../shared/services/restful-laravel.service';


@Injectable()
export class CompanyAdminMessageService {

  constructor(private restfulWebService:RestfulLaravelService) { }
  
  getUsersWithMessages():any{
      return this.restfulWebService.get('admin/get-message-user-list')
     }

     getUserMessages(user):any{
      return this.restfulWebService.get(`admin/get-message-conversation/${user}`)
     }

     sendUserMessages(reqBody):any{
       if(reqBody.userIDs) return this.restfulWebService.post('admin/create-message',reqBody);
      else return this.restfulWebService.post('admin/send-message',reqBody);
     }

      getAllUsers():any{
      return this.restfulWebService.get('admin/get-all-user-list')
     }

     uploadFile(reqBody){
      return this.restfulWebService.post('admin/file-upload',reqBody);
     }
 

}
