import {  ModuleWithProviders  } from '@angular/core';
import {  RouterModule,Routes  } from '@angular/router';
import {  CompanyAdminMessagesComponent} from './messages/messages.component';
// import {  NewMessageComponent} from './new-message/new-message.component';
// import {  GroupMessageComponent} from './group-message/group-message.component';
export const CaMessagePagesRoutes: Routes = [
    {
        path: '',
        canActivate:[],
        children: [
           
            { path: '', component:CompanyAdminMessagesComponent },
            // { path: 'new-message', component:NewMessageComponent },
            // { path:'group-message', component:GroupMessageComponent}
            
            
        ]
    }
];

export const CaMessageRoutingModule = RouterModule.forChild(CaMessagePagesRoutes);
