import { Component, OnInit } from '@angular/core';
import { IDatePickerConfig } from 'ng2-date-picker';

@Component({
  selector: 'app-offer-expired',
  templateUrl: './offer-expired.component.html',
  styleUrls: ['./offer-expired.component.scss']
})
export class OfferExpiredComponent implements OnInit {
  
  birthday: any;
config: IDatePickerConfig = {};

  constructor() {
    this.config.locale = "en";
    this.config.format = "YYYY-MM-DD";
    this.config.showMultipleYearsNavigation = true;
    this.config.weekDayFormat = 'dd';
    this.config.disableKeypress = true;
   }

  ngOnInit() {
  }

}
