import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { EmployeeManagementService } from '../employee-management.service';
import { Pipe, PipeTransform } from '@angular/core';
import { APPCONFIG } from '../../../config';
import { CoreService } from "../../../shared/services/core.service";

@Component({
  selector: 'app-employee-listing',
  templateUrl: './employee-listing.component.html',
  styleUrls: ['./employee-listing.component.scss']
})
export class EmployeeListingComponent implements OnInit {
  public searchUser = {};
  public toggleFilter = true;
  public employeeListings = [];
  public searchText = "";
  public isProcessing = false;
  isChecked = false;
  statusList = [];
  roleList = [];
  filter = { employmentTypeIDs: [], positionIDs: [], status: null, roles: null };

  filters = {
    "employmentType": [], "companyPositions": [],
    "status": { "Current staff": false, "Incomplete induction": false, "Invited": false, "Offer expired": false, "Terminated": false },
    "roles": { "companyManager": false, "companyEmployee": false }
  }

  constructor(private employeeManagementService: EmployeeManagementService, private router: Router, private coreService: CoreService) { }

  ngOnInit() {
    APPCONFIG.heading = "Employee management";
    this.getFilters();
    this.getEmployeeListing();
  }

  getFilters() {
    // this.isProcessing = true;
    this.employeeManagementService.getFilters().subscribe(
      res => {
        // this.isProcessing = false;
        if (res.code == 200) {
          this.filters = res.data;
          this.statusList = Object.keys(this.filters.status);
          this.roleList = Object.keys(this.filters.roles);
        }
        else return this.coreService.notify('Unsuccessful', res.message, 0);
      },
      err => {
        // this.isProcessing = false;
        this.coreService.notify('Unsuccessful', err.message, 0);
      })
  }

  getEmployeeListing() {
    this.filter.status = this.filters.status;
    this.filter.roles = this.filters.roles;
    let params = { "search": this.searchText, "filter": this.filter }

    this.isProcessing = true;
    this.employeeManagementService.getEmployeeList(JSON.stringify(params)).subscribe(
      res => {
        this.isProcessing = false;
        if (res.code == 200) {
          this.employeeListings = res.data;
        }
        else return this.coreService.notify('Unsuccessful', res.message, 0);
      },
      err => {
        this.isProcessing = false;
        this.coreService.notify('Unsuccessful', err.message, 0);
      })
  }

  viewDetails(employee) {
    if (employee.hasOwnProperty('employeeUserID')) {
      let id = employee.employeeUserID;
      this.router.navigate([`/app/company-admin/employee-management/profile/${id}`]);
    } else {
      let id = employee.employeeInvitedID;
      this.router.navigate([`/app/company-admin/employee-management/profile/${id}`, 'invited']);
    }
  }

  editProfile(employee) {
    if (employee.hasOwnProperty('employeeUserID')) {
      this.router.navigate(['/app/company-admin/employee-management/profile/edit/', employee.employeeUserID]);
    } else {
      this.router.navigate([`/app/company-admin/employee-management/general-information/${employee.employeeInvitedID}`]);
    }
  }

  onEditEmployment(employee){  
        let id = employee.employeeInvitedID?employee.employeeInvitedID:employee.employeeUserID;
        this.router.navigate([`/app/company-admin/employee-management/general-information/${id}`]);            
    }

  onCheckEmp(event, id, type) {
    this.isChecked = true;
    let ind = this.filter[type].indexOf(id);

    if (event.target.checked) {
      (ind >= 0) ? null : this.filter[type].push(id);
    } else {
      (ind >= 0) ? this.filter[type].splice(ind, 1) : null;
    }
  }
}

