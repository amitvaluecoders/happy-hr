import { ModuleWithProviders } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EmployeeListingComponent } from './employee-listing/employee-listing.component';
import { UnassignedPositionComponent } from './invite employee/unassigned-position/unassigned-position.component';
import { EmployeeDashboardComponent } from './employee-dashboard/employee-dashboard.component';
import { EmployeeProfileComponent } from './employee-profile/employee-profile.component';
import { EmployeeManagementService } from './employee-management.service';
import { GeneralInformationComponent } from './invite employee/general-information/general-information.component';
import { ExtraInformationComponent } from './invite employee/extra-information/extra-information.component';
import { ProposedComponent } from './invite employee/proposed/proposed.component';
import { OfferExpiredComponent } from './offer-expired/offer-expired.component';
import { EditEmployeeProfileComponent } from './edit-employee-profile/edit-employee-profile.component';

export const EmployeeManagementPagesRoutes: Routes = [
    {
        path: '',
        canActivate: [],
        children: [

            { path: '', component: EmployeeListingComponent },
            { path: 'unassigned-position/:id', component: UnassignedPositionComponent },
            {
                path: 'general-information',
                children: [
                    { path: '', component: GeneralInformationComponent },
                    { path: ':uid', component: GeneralInformationComponent },
                    { path: 'cid/:id', component: GeneralInformationComponent }
                ]
            },
            { path: 'extra-information', component: ExtraInformationComponent },
            { path: 'proposed', component: ProposedComponent },
            { path: 'offer-expired', component: OfferExpiredComponent },
            {
                path: 'profile',
                children: [
                    { path: 'edit/:id', component: EditEmployeeProfileComponent },
                    {
                        path: ':id', component: EmployeeProfileComponent,
                        redirectTo: '/app/company-admin/employee-management/profile/edit/:id'
                    },
                    {
                        path: ':id/:type', component: EmployeeProfileComponent,
                        redirectTo: '/app/company-admin/employee-management/general-information/:uid'
                    }
                ]
            }
        ]
    }
];

export const EmployeeManagementRoutingModule = RouterModule.forChild(EmployeeManagementPagesRoutes);
