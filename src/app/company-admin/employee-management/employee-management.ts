export interface GeneralInfo {
    "contractType": string;
    "contractTypeID": string;    
    "orgNodeID": string;
    "positionID": string;
    "firstName": string;
    "lastName": string;
    "email": string;
    "employmentTypeID": any;
    "remunarationRate": string;
    "remunarationPeriod": string;
    "superannuation": string;
    "offerExpiryDate": any;
    "isCurrentEmployee": string;
    "employeeStartDate": any;
    "awardIDs": any;
    "awardName": any;
    "contractStartDate": any;
    "contractEndDate": any;
    "companyPositionLevelID": string;
    "mandatoryDocuments" :any[];
}

export interface ExtraInfo {
    "commissionOption": string;
    "commissionNote": string;
    "companyBonusOption": string;
    "companyBonusNote": string;
    "mobilePhoneOption": string;
    "mobilePhoneNote": string;
    "laptopOption": string;
    "laptopNote": string;
    "tabletOption": string;
    "tabletNote": string;
    "vehicleAllowanceOption": string;
    "vehicleAllowanceNote": string;
    "otherBonusOption": string;
    "otherBonusNote": string;
    "otherApplicableInformationOption": string;
    "otherApplicableInformationNote": string;
    "mobilePhoneContribution": string;
}

export interface Proposed {
    "sexsualHarrassmentOfficer": string;
    "fireWarden": string;
    "ohsOfficer": string;
    "firstAidOfficer": string;
}

export interface InviteEmployeeData extends GeneralInfo, ExtraInfo, Proposed { }