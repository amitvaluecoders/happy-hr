import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { MdSnackBar, MdDialog, MdDialogRef } from '@angular/material';
import { APPCONFIG } from '../../../../config';
// import { IMyDpOptions } from 'mydatepicker';
import { EmployeeManagementService } from '../../employee-management.service';
import { GeneralInfo } from '../../employee-management';
import { IDatePickerConfig } from 'ng2-date-picker';
import * as moment from 'moment';
import { CoreService } from '../../../../shared/services/core.service';
import { MandatoryDocumentsPopupComponent } from '../../../../shared/components/mandatory-documents-popup/mandatory-documents-popup.component';

@Component({
  selector: 'app-general-information',
  templateUrl: './general-information.component.html',
  styleUrls: ['./general-information.component.scss']
})
export class GeneralInformationComponent implements OnInit {
  public progress;

  birthday: any;
  config: IDatePickerConfig = {};
  configEmp: IDatePickerConfig = {};

  public employementList = [];
  public contractList = [];
  public owncontractListTypes = [];  
  public ownContractList = [];
  public selectedEmployeeTypes = [];
  public selectedContractTypes = [];
  public selectedOwnContract = [];  
  public selectedOwnContractTypes=null;
  
  public dropdownSettings = {};

  public info = <GeneralInfo>{};
  // public award = { awardID: "", title: "" };
  public isProcessing: boolean;
  public contractTypeID = null;
  //drop:boolean=false;
  employmentTypeID = null;
  ownContractID=null;
  candidateID = null;
  ownContract:any={};
  employementLevelList = [];
  selectedEmployementLevel = [];
  dropdownSettings2 = {};
  dropdownSettings3={};
  validSalary = true;
  minSalary;
  userID = null;

  constructor(public coreService:CoreService,private employeeService: EmployeeManagementService, private router: Router, private route: ActivatedRoute, public dialog: MdDialog) {
    this.configEmp.locale = this.config.locale = "en";
    this.configEmp.format = this.config.format = "DD MMM YYYY";
    this.configEmp.showMultipleYearsNavigation = this.config.showMultipleYearsNavigation = true;
    this.configEmp.weekDayFormat = this.config.weekDayFormat = 'dd';
    this.configEmp.disableKeypress = this.config.disableKeypress = true;
    this.config.min = moment();
  }

  ngOnInit() {
    APPCONFIG.heading = "Invite employee";
       this.info = this.employeeService.getInviteEmployeeData();
       this.info.mandatoryDocuments=[];
    this.progressBarSteps();

    this.dropdownSettings = {
      singleSelection: true,
      text: "Select any one employment type"
    }

    this.dropdownSettings2 = {
      singleSelection: true,
      text: "Select any one employment level"
    }
    this.dropdownSettings3 = {
      singleSelection: true,
      text: "Select any one Own contract type"
    }

    if (this.route.snapshot.params['id']) {
      this.candidateID = this.route.snapshot.params['id'];
      this.getCandidateDetail();
    }

    if (this.route.snapshot.params['uid']) {
      this.userID = this.route.snapshot.params['uid'];
      this.getInviteEmployeeDetail();
    }
    if (this.info.isCurrentEmployee && this.info.isCurrentEmployee.toLowerCase() == 'yes') {
       this.configEmp.min = moment();
    } else {
      //this.additionalInfo.employeeStartDate).format('DD MMM YYYY');
      if(!this.info.employeeStartDate){ this.configEmp.max = moment();}
      
      
      //!(this.additionalInfo.employeeStartDate) ? '' : moment(this.additionalInfo.employeeStartDate).format('DD MMM YYYY');
    }
    if (!this.route.snapshot.params['uid']) {
      this.getDropdownList();
      this.getOwnContractList();
      this.getEmploymentLevel();
    }
  }

  progressBarSteps() {
    this.progress = {
      progress_percent_value: 50, //progress percent
      total_steps: 4,
      current_step: 2,
      circle_container_width: 25, //circle container width in percent
      steps: [
        { num: 1, data: "Select role" },
        { num: 2, data: "General information", active: true },
        { num: 3, data: "Additional information" },
        { num: 4, data: "Confirmation" }
      ]
    }
  }

  getInviteEmployeeDetail() {
    this.isProcessing = true;
    this.employeeService.getInvitedEmployeeDetail(this.userID).subscribe(
      res => {
        this.isProcessing = false;
        if (res.code == 200) {
          this.info = res.data;
          this.info.offerExpiryDate=!(this.info.offerExpiryDate)?'' : moment(this.info.offerExpiryDate).format('DD MMM YYYY');
          this.info.employeeStartDate=!(this.info.employeeStartDate)?'' : moment(this.info.employeeStartDate).format('DD MMM YYYY');
          this.employeeService.setInfo(res.data);
          this.getEmploymentLevel();
          this.getDropdownList();
          this.getOwnContractList();
          this.setdatePickerconfigEmp();
        }
      })
  }

  getEmploymentLevel() {
    if (!this.info.positionID) return;
    this.employeeService.getEmploymentLevel(this.info.positionID).subscribe(
      res => {
        if (res.code == 200) {
          this.employementLevelList = res.data;
          this.employementLevelList.filter((item, i) => { item['itemName'] = item.levelTitle; item['id'] = item.companyPositionLevelID; });
          this.selectedEmployementLevel = this.employementLevelList.filter(i => i.id == this.info.companyPositionLevelID);
        }
      })
  }

  onSelectSelectedEmployementLevel(item, selected) {
    this.info.companyPositionLevelID = item.id;
    this.setEmployeeSalary();
  }

  setEmployeeSalary() {
    if (!this.selectedEmployementLevel.length || !this.employementLevelList.length) return;
    let index = this.selectedEmployementLevel[0].contractType.findIndex(i => this.employmentTypeID == i.contractTypeID);
    if (index >= 0) {
      this.info.remunarationRate = this.selectedEmployementLevel[0].contractType[index].salary;

    }
  }

  resetEmployeeSalary() {
    this.info.remunarationRate = '';
  }

  isValidSalary() {
    if (!this.selectedEmployementLevel.length || !this.employementLevelList.length) return;
    let index = this.selectedEmployementLevel[0].contractType.findIndex(i => this.employmentTypeID == i.contractTypeID);
    if (index >= 0) {
      this.validSalary = Number(this.info.remunarationRate) >= Number(this.selectedEmployementLevel[0].contractType[index].salary)
      // this.info.remunarationRate = this.selectedEmployementLevel[0].contractType[index].salary;
      this.minSalary = !this.validSalary ? this.selectedEmployementLevel[0].contractType[index].salary : null;

    }
  }

  getCandidateDetail() {
    this.isProcessing = true;
    this.employeeService.getCandidateDetail(this.candidateID).subscribe(
      res => {
        this.isProcessing = false;
        if (res.code == 200) {
          this.info.firstName = res.data.firstName;
          this.info.lastName = res.data.lastName;
          this.info.email = res.data.email;
        }
      })
  }

  getDropdownList() {
    this.isProcessing = true;
    this.employeeService.getDropdown().subscribe(
      res => {
        this.isProcessing = false;
        if (res.code == 200) {
          this.contractList = res.data;
          this.contractList.filter((item, i) => { item['itemName'] = item.title; item['id'] = item.contractTypeID; });
          this.employementList = JSON.parse(JSON.stringify(this.contractList));
          this.employementList.push({ "id": "x", "itemName": "Own contract" });

          if (this.info.contractType == 'HHR') {
            this.selectedEmployeeTypes = this.contractList.filter(i => i.id == this.info.employmentTypeID);
            this.employmentTypeID = this.selectedEmployeeTypes[0].id;
          } else if (this.info.contractType == 'Own') {
            this.selectedEmployeeTypes = this.employementList.filter(i => i.id == 'x');
            this.selectedContractTypes = this.contractList.filter(i => i.id == this.info.employmentTypeID);
            this.employmentTypeID = this.selectedEmployeeTypes[0].id;
            this.contractTypeID = this.selectedContractTypes[0].id;
          }
        }
      },
      err => {
        this.isProcessing = false;
      }
    )
  }
  getOwnContractList(){
    this.isProcessing = true;
    this.employeeService.getOwnContract().subscribe(
      res => {
        this.isProcessing = false;
        if (res.code == 200) {
          this.ownContract = res.data;
          this.ownContract.types.forEach((item,i)=>{
              if(item){
                    if(item=="Own"){
                      let data = {"id" : i,"itemName":item+"\t"+"contract"}
                      this.ownContractList.push(data);
                      console.log("list",this.ownContractList);  
                    }
                    else{
                      let name= item.split("_");
                      let data = {"id" : i,"itemName":name[0]+"\t"+"contract"+"\t"+name[1]}
                      this.ownContractList.push(data);
                    }
              }
              
          })
        }
      },
      err => {
        this.isProcessing = false;
      }
    )

  }
  filterContractList(id){
    this.owncontractListTypes=[];
    if(id==0){
      this.contractList.forEach((item,i)=>{ 
        for(let c of this.ownContract.contractTypeIDs.Own){
          if(c.contractTypeID==item.contractTypeID){
            this.owncontractListTypes.push(item);
          }
        }    
      })
      console.log("data",this.owncontractListTypes);
    }
    if(id==1){
      this.contractList.forEach((item,i)=>{ 
        for(let c of this.ownContract.contractTypeIDs.Own_2){
          if(c.contractTypeID==item.contractTypeID){
            this.owncontractListTypes.push(item);
          }
        }
      })
      console.log("data",this.owncontractListTypes);
      
    }
    if(id==2){
      this.contractList.forEach((item,i)=>{ 
        for(let c of this.ownContract.contractTypeIDs.Own_3){
          if(c.contractTypeID==item.contractTypeID){
            this.owncontractListTypes.push(item);
          }
        }
      })
      console.log("data",this.owncontractListTypes);
      
    }
    if(id==3){
      this.contractList.forEach((item,i)=>{ 
        for(let c of this.ownContract.contractTypeIDs.Own_4){
          if(c.contractTypeID==item.contractTypeID){
            this.owncontractListTypes.push(item);
          }
        }
      })
      console.log("data",this.owncontractListTypes);
      
    }
  }

  onChangeCurrentEmp(event) {
    let copy: IDatePickerConfig = JSON.parse(JSON.stringify(this.configEmp));
    if (event.target.value.toLowerCase() == "no") {
      copy.min = moment();
      copy.max = undefined;
    } else {
      copy.max = moment();
      copy.min = undefined;
    }
    this.configEmp = copy;
  }

  setdatePickerconfigEmp() {
    let copy: IDatePickerConfig = JSON.parse(JSON.stringify(this.configEmp));
    if (this.info.isCurrentEmployee.toLowerCase() == "no") {
      copy.min = moment();
      copy.max = undefined;
    } else {
      copy.max = moment();
      copy.min = undefined;
    }
    this.configEmp = copy;
  }


  initRemunarationPeriod() {
    if (this.employmentTypeID == 1 || this.contractTypeID == 1) {
      this.info.remunarationPeriod = 'Hourly';
    } else if (this.employmentTypeID == 2 || this.contractTypeID == 2) {
      this.info.remunarationPeriod = 'Hourly Rate';
    } else if (this.employmentTypeID == 3 || this.contractTypeID == 3) {
      this.info.remunarationPeriod = 'Hourly';
    } else if (this.employmentTypeID == 4 || this.contractTypeID == 4) {
      this.info.remunarationPeriod = 'Hourly';
    }
  }

  onSelectEmployementType(item, selected) {
    this.employmentTypeID = (selected) ? item.id : null;
    this.ownContractID=null;
    this.contractTypeID = null;
    this.selectedContractTypes = [];
    this.setEmployeeSalary();
    this.initRemunarationPeriod();
  }

  onSelectOwnContractTypes(item,selected){
    this.selectedOwnContractTypes= item.id;
    this.ownContractID = (selected) ? item.id : null;
    this.contractTypeID = null;
    this.selectedContractTypes = [];
    //console.log("id",item.id);
    this.filterContractList(item.id);
    this.setEmployeeSalary();
    this.initRemunarationPeriod();
  }


  onSelectContractType(item, selected) {
    this.contractTypeID = (selected) ? item.id : null;
    this.initRemunarationPeriod();
  }
  

  onClick() {
    let dialogRef = this.dialog.open(AwardDialog, { width: '900px', height: '600px' });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.info.awardIDs = [result.awardID];
        this.info.awardName = result.title;
      }
    });
  }


  openMandatoryDocPopup(type,item,key){
  let dialogRef = this.dialog.open(MandatoryDocumentsPopupComponent);
    dialogRef.componentInstance.document = JSON.parse(JSON.stringify(item));
  var mandatoryDocs=[];
  dialogRef.afterClosed().subscribe(response => {
    if(response){
    if(type=='add')this.info['mandatoryDocuments'].push(response);
    else {
      this.info['mandatoryDocuments'] = this.info['mandatoryDocuments'].map((item,index)=>{
        if(index == key) item = response;
        return item;
      })
    }
    }
  });
  
}

  deleteDoc(i){
        this.employeeService.deleteMendatoryDoc(i).subscribe(
          res=>{
                if(res.code == 200){
                  this.coreService.notify("Successful", res.message, 1); 
                }
          },
          err=>{
            this.coreService.notify("Unsuccessful", "Error while deleting", 0);
          }
        )
  }


onDeleteDoc(i){
    this.info['mandatoryDocuments'].splice(i,1);
}


  saveAndNext() {
    if (!this.info.awardIDs || !this.info.awardIDs.length) return;

    // this.info.positionID = this.employeeService.getInviteEmployeeData().positionID;
    this.info.employmentTypeID = !(this.contractTypeID) || (this.employmentTypeID !== 'x') ? this.employmentTypeID : this.contractTypeID;
   // this.info.contractType = !(this.contractTypeID) || (this.employmentTypeID !== 'x') ? "HHR" : "Own";
    if(this.selectedOwnContractTypes==0){
        this.info.contractType="Own";
    }
    else if(this.selectedOwnContractTypes==1){
      this.info.contractType="Own_2";
    }
    else if(this.selectedOwnContractTypes==2){
      this.info.contractType="Own_3";
    }
    else if(this.selectedOwnContractTypes==3){
      this.info.contractType="Own_4";
    }
    else{
      this.info.contractType="HHR";    
    }
    this.info.contractStartDate = (this.info.contractStartDate) ? moment(this.info.contractStartDate).format('YYYY-MM-DD') : null;
    this.info.contractEndDate = (this.info.contractEndDate) ? moment(this.info.contractEndDate).format('YYYY-MM-DD') : null;
    this.info.employeeStartDate = (this.info.employeeStartDate) ? moment(this.info.employeeStartDate).format('YYYY-MM-DD') : null;
    this.info.offerExpiryDate = (this.info.offerExpiryDate && this.info.isCurrentEmployee == 'No') ? moment(this.info.offerExpiryDate).format('YYYY-MM-DD') : null;
    this.employeeService.setInfo(this.info);
    console.log('emp', this.employeeService.getInviteEmployeeData());
    return this.router.navigate(['/app/company-admin/employee-management/extra-information']);
  }

}



@Component({
  selector: 'award-add',
  template: `
  <md-progress-spinner *ngIf="isProcessing" mode="indeterminate" color=""></md-progress-spinner>
  <div *ngIf="!isProcessing" class="filter-wrapp box box-default contentBox">
    <div class="row">
        <div class="col-md-12">
            <ul class="awardsFilter">
                <li style="display:inline-block; margin:0px 7px 7px 0px;" *ngFor="let alpha of awards" (click)="toggleCollapse(alpha)">
                  <span class="badge badge-primary cursor" style="min-width:26px;">{{alpha}}</span>
                </li>
            </ul>
        </div>
    </div>
    <div class="AwardAlphabet-list">
      <div *ngFor="let alpha of awards" class="row">
        <div class="col-md-12" *ngIf="collapse[alpha]">
            <div class="box box-default full-width">
                <div class="box-header box-dark">{{alpha}}</div>
                <div class="box-body">
                    <ul>
                        <li *ngFor="let award of awardList[alpha]; let i=index;">
                            <div class="customchkbox">
                                <input [id]="'award'+award.awardID" type="radio" name="award" [value]="award.awardID" (change)="onCheckOption(i,alpha)"
                                    class="required">&nbsp;
                                <label [for]="'award'+award.awardID"></label>
                            </div>{{award.title}}
                        </li>
                    </ul>
                </div>
            </div>
        </div>
      </div>
    </div>
  </div>
`,
  styles: [`
    .customchkbox{
      display:inline-block;
    }
    .customchkbox input + label {
        display: block;
    }
    
  `]
})
export class AwardDialog implements OnInit {
  public awards = [];
  public awardList = {};
  public isProcessing = false;
  public collapse = {};

  constructor(public dialogRef: MdDialogRef<AwardDialog>, public awardService: EmployeeManagementService) { }

  ngOnInit() {
    this.getAwards();
    this.collapse['A'] = true;
  }

  toggleCollapse(alpha) {
    this.collapse = {};
    this.collapse[alpha] = true;
  }

  getAwards() {
    this.isProcessing = true;
    let params = { "filter": { "awards": {} } };
    this.awardService.getAwards(JSON.stringify(params)).subscribe(
      res => {
        this.isProcessing = false;
        if (res.code == 200) {
          this.awardList = res.data;

          let arr = Object.keys(this.awardList);
          this.awards = arr.filter(i => { if (this.awardList[i].length > 0) return i; })
        }
      },
      error => {
        this.isProcessing = false;
        console.log(error);
      })
  }

  onCheckOption(index, alpha) {
    this.dialogRef.close(this.awardList[alpha][index]);
  }
}