import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { APPCONFIG } from '../../../../config';
import { EmployeeManagementService } from '../../employee-management.service';
import { ExtraInfo } from '../../employee-management';

@Component({
  selector: 'app-extra-information',
  templateUrl: './extra-information.component.html',
  styleUrls: ['./extra-information.component.scss']
})
export class ExtraInformationComponent implements OnInit {

  public progress;
  public info = <ExtraInfo>{};

  constructor(private employeeService: EmployeeManagementService, private router: Router) { }

  ngOnInit() {

    this.info = this.employeeService.getInviteEmployeeData();
    APPCONFIG.heading = "Invite employee";
    this.progressBarSteps();
  }

  progressBarSteps() {
    this.progress = {
      progress_percent_value: 75, //progress percent
      total_steps: 4,
      current_step: 3,
      circle_container_width: 25, //circle container width in percent
      steps: [
        { num: 1, data: "Select role" },
        { num: 2, data: "General information" },
        { num: 3, data: "Additional information", active: true },
        { num: 4, data: "Confirmation" }
      ]
    }
  }

  saveAndNext() {
    this.info.mobilePhoneContribution = (this.info.mobilePhoneOption != 'Partially Subsidized')?"":this.info.mobilePhoneContribution;
    console.log(this.info);
    this.employeeService.setInfo(this.info);
    console.log('emp2', this.employeeService.getInviteEmployeeData());
    this.router.navigate(['/app/company-admin/employee-management/proposed']);
  }

}
