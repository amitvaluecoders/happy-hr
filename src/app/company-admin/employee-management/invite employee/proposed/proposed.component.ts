import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { APPCONFIG } from '../../../../config';
import { EmployeeManagementService } from '../../employee-management.service';
import { Proposed } from '../../employee-management';
import { CoreService } from '../../../../shared/services/core.service';

@Component({
  selector: 'app-proposed',
  templateUrl: './proposed.component.html',
  styleUrls: ['./proposed.component.scss']
})
export class ProposedComponent implements OnInit {

  public progress;
  public info = <Proposed>{};
  public isProcessing = false;
  public roles = { "sho": false, "ohs": false, "fw": false, "fao": false };

  constructor(private employeeService: EmployeeManagementService, public router: Router, public coreService: CoreService) { }

  ngOnInit() {
    this.info = this.employeeService.getInviteEmployeeData();
    APPCONFIG.heading = "Invite employee";
    this.progressBarSteps();

    this.getAssignedSpecialRole();
  }

  progressBarSteps() {
    this.progress = {
      progress_percent_value: 100, //progress percent
      total_steps: 4,
      current_step: 4,
      circle_container_width: 25, //circle container width in percent
      steps: [
        // {num:1,data:"Select Role"},
        { num: 1, data: "Select role" },
        { num: 2, data: "General information" },
        { num: 3, data: "Additional information" },
        { num: 4, data: "Confirmation", active: true }
      ]
    }
  }

  getAssignedSpecialRole() {
    this.isProcessing = true;
    this.employeeService.getAssignedSpecialRoles().subscribe(
      res => {
        this.isProcessing = false;
        if (res.code == 200) {
          this.roles = res.data;
        }
      },
      err => {
        this.isProcessing = false;
      }
    )
  }

  saveAndNext() {
    this.employeeService.setInfo(this.info);
    console.log('emp3', this.employeeService.getInviteEmployeeData());
    let inviteEmployeeData = this.employeeService.getInviteEmployeeData();
    this.employeeService.inviteEmployee(inviteEmployeeData).subscribe(
      res => {
        if (res.code == 200) {
          this.coreService.notify("Successful", "Invitation sent successfully", 1);
          if (inviteEmployeeData['invitedEmployeeID'] && inviteEmployeeData['invitedEmployeeID'] !== '') {
            return this.router.navigate(['/app/company-admin/employee-management']);
          } else if (inviteEmployeeData.orgNodeID !== '') {
            return this.router.navigate(['/app/company-admin/organisation-chart']);
          }
          this.router.navigate(['/app/company-admin/employee-management']);
        }
      },
      error => {
        if (error.code == 400) {
          return this.coreService.notify("Error", error.message, 0);
        }
        this.coreService.notify("Error", "Error while sending invitation", 0);
        console.log(error);
      }
    )
  }

}