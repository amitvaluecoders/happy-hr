import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { APPCONFIG } from '../../../../config';
import { EmployeeManagementService } from '../../employee-management.service';

@Component({
  selector: 'app-unassigned-position',
  templateUrl: './unassigned-position.component.html',
  styleUrls: ['./unassigned-position.component.scss']
})
export class UnassignedPositionComponent implements OnInit {
  public isProcessing: boolean;
  public positions = [];
  orgNodeID = '';

  constructor(private employeeService: EmployeeManagementService, public router: Router, public route: ActivatedRoute) { }

  ngOnInit() {
    APPCONFIG.heading = "Invite employee";
    this.getUnassignedPositions();

    if (this.route.snapshot.params['id']) {
      this.orgNodeID = this.route.snapshot.params['id'];
    }
  }

  getUnassignedPositions() {
    this.isProcessing = true;
    this.employeeService.getUnassignedPositions().subscribe(
      res => {
        if (res.code == 200) {
          this.positions = res.data
          this.isProcessing = false;
        }
      },
      error => {
        this.isProcessing = false;
        console.log(error)
      });
  }

  deletePosition(index) {
    console.log(this.positions[index]);
  }

  invite(position) {
    this.employeeService.setPosition(position.positionID, this.orgNodeID);
    this.router.navigate(['/app/company-admin/employee-management/general-information']);
  }

}
