import { Component, OnInit,ViewChild,ElementRef,ViewContainerRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { APPCONFIG } from '../../../config';
import { CoreService } from '../../../shared/services/core.service';
import { CountryStateService } from "../../../shared/services/country-state.service";
import { IDatePickerConfig } from 'ng2-date-picker';
import { Profile, MyDocument, CompanyDocument } from '../../../company-employee/induction/induction';
import { UploadMendatoryDocsPopupComponent } from '../../../user/subcontractor-induction/upload-mendatory-docs-popup/upload-mendatory-docs-popup.component';
import { InviteEmployeeData } from "../employee-management";
import * as moment from 'moment';
import { MdDialog, MdDialogRef } from '@angular/material';
import { ConfirmService } from '../../../shared/services/confirm.service';
import { EmployeeManagementService } from "../employee-management.service";
import { EmployeeIntroduction } from "../../../company-employee/induction/induction.service";

@Component({
  selector: 'app-edit-employee-profile',
  templateUrl: './edit-employee-profile.component.html',
  styleUrls: ['./edit-employee-profile.component.scss']
})
export class EditEmployeeProfileComponent implements OnInit {
  @ViewChild('profileForm') taskNoteRef:ElementRef;
  testtDate:any;
  progress;
  collapse = {};
  isRequired:any;
  awardName:any;
  isManager = false;
  birthday: any;
  isSuppernnuationListProcessing=false;
  public unsubscriber:any;
  licExpiry: any;
  employeeStartDate:any;
  config: IDatePickerConfig = {};
  profile: Profile;
  employeeDocumentSigned = [];
  employeeDocumentSubmit = [];
  employeeId: string;
  isProcessing: boolean;
  isSaving: boolean;
  fundNameArr=[];
  selectedOwnContractTypes=null;
  ownContractID=null;
  isProcessingSign: boolean;
  signatureText = "";
  public docRequired=false;
  adoptSign = false;
  countryList = [];
  employementLevelList = []; selectedEmployementLevel = []; selectedEmployeeTypes = []; selectedContractTypes = [];
  dropdownSettings = {}; dropdownSettings2 = {};dropdownSettings3 = {};
  contractList = []; employementList = [];
  employmentTypeID = ""; contractTypeID = "";
  contractStartDate; contractEndDate;
  validSalary = true;
  minSalary = "";
  isProcessingImg = false;
  errors=[];
  mandatoryDocName:any;
  public owncontractListTypes=[];
  stateList=[];
  public ownContractList=[];
  ownContract:any;
  isAmountExist=false;

public selectedOwnContract = [];
  additionalInfo = <InviteEmployeeData>{};

  constructor( public countryService: CountryStateService,public companyService: EmployeeManagementService, public coreService: CoreService,
    public router: Router, public route: ActivatedRoute,public viewContainerRef:ViewContainerRef, public dialog: MdDialog,public confirmService: ConfirmService,  ) {
    this.profile = new Profile;
    this.config.locale = "en";
    this.config.format = "DD MMM YYYY";
    this.config.showMultipleYearsNavigation = true;
    this.config.showGoToCurrent = true,
    this.config.weekDayFormat = 'dd';
    this.config.disableKeypress = true;
    this.config.monthFormat = "MMM YYYY";
    
  }

  chjValidations(elArray){
    for(let e of elArray){
      if(e.invalid){
        //this.isAmountExist=true;
        return true;
      }   
    }
    return false;
  }

  amountExist(elArray){
    for(let e of elArray){
      if(e.invalid){
        this.isAmountExist=true;
        return true;
      }   
    }
    return false;
  }
  //amountExist()

  ngOnInit() {
    this.isManager = this.coreService.isManager();
    this.stateList = this.countryService.getState(13);
    APPCONFIG.heading = "Employee profile";
    this.dropdownSettings = {
      singleSelection: true,
      text: "Select any one employement type"
    }

    this.dropdownSettings2 = {
      singleSelection: true,
      text: "Select any one employement level"
    }
    this.dropdownSettings3 = {
      singleSelection: true,
      text: "Select any one own contract type"
    }

    this.getCountryList();
    if (this.route.snapshot.params['id']) {
      this.employeeId = this.route.snapshot.params['id'];
      this.getEmployeeProfile();
    }
  }

  SearchSuppernnuationList(searchKey){
    if(this.unsubscriber)this.unsubscriber.unsubscribe();
    this.getSuppernnuationList(searchKey);
  }

  getSuppernnuationList(searchKey){
    searchKey = encodeURIComponent(searchKey);
    this.isSuppernnuationListProcessing = true;
    this.unsubscriber = this.companyService.getSuppernnuationList({search:searchKey}).subscribe(
      res => {
        if (res.code == 200) {
          this.fundNameArr = res.data;
          this.profile.superannuation['isDropDown']=true;
          this.isSuppernnuationListProcessing = false;

        }
      },
      error => {
        this.isSuppernnuationListProcessing = false;
        this.coreService.notify("Error", error.message, 0)
      }
    )
  }



  downloadPDF(doc) {
    if (!doc.docID) return;
    this.companyService.downloadPdf(doc);
  }
  
  getCountryList() {
    this.companyService.getCountries().subscribe(
      data => {
        if (data.code == 200) {
          this.countryList = data.data;
        }
      }
    );
  }

  formatDateTime(date) {
    return date ? moment(date).format('DD MMM YYYY hh:mm A') : 'Not specified';
  }

  getEmployeeProfile() {
    this.isProcessing = true;
    this.companyService.getEmployeeProfile(this.employeeId).subscribe(
      res => {
        if (res.code == 200) {

          this.isProcessing = false;
          this.profile = res.data[0];
          this.birthday = moment(this.profile.personalInformation.dob).isValid() ? moment(this.profile.personalInformation.dob) : '';
          this.licExpiry = !(this.profile.employeeLicence.expiryDate) ? '' : moment(this.profile.employeeLicence.expiryDate).format('YYYY-MM-DD');
          this.employeeDocumentSubmit = res.data[2].employeeDocumentSubmit;
          this.employeeDocumentSigned = res.data[1].employeeDocumentSigned;
          this.additionalInfo = res.data[4].additionalInformation;
          if(this.additionalInfo.mandatoryDocuments.length){
                  this.flashRedWhenUpload();      
          }
          if(this.additionalInfo.commissionOption=='None'){
            this.additionalInfo.commissionNote="";
          }
          if(this.additionalInfo.mobilePhoneOption=='None'){
            this.additionalInfo.mobilePhoneContribution="";
            this.additionalInfo.mobilePhoneNote="";
          }
          this.employeeStartDate = !(this.additionalInfo.employeeStartDate) ? '' : moment(this.additionalInfo.employeeStartDate).format('DD MMM YYYY');
          this.testtDate = this.employeeStartDate;
          this.signatureText = this.profile.signature.text ? this.profile.signature.text : this.profile.name;
          this.contractStartDate = (this.additionalInfo.contractStartDate) ? moment(this.additionalInfo.contractStartDate) : '';
          this.contractEndDate = (this.additionalInfo.contractEndDate) ? moment(this.additionalInfo.contractEndDate) : '';
          this.getEmploymentLevel();
          this.getOwnContractList();
          this.getDropdownList();
          this.setEmployeeSalary();
          
          for (var _i = 1; _i < 13; _i++) {    
              this.collapse[`pfl${_i}`] = true ;
              this.collapse[`pfl11`] = false;
          }
        }
        else return this.coreService.notify("Unsuccessful", res.message, 0);
      },
      error => {
        this.isProcessing = false;
        if (error.code == 400) {
          return this.coreService.notify("Unsuccessful", error.message, 0);
        }
        this.coreService.notify("Error", "Error while getting profile details.", 0)
      }
    )
  }

  getEmploymentLevel() {
    console.log('prof ==>> ', this.profile)
    if (!this.profile.positionID) return;
    this.companyService.getEmploymentLevel(this.profile.positionID).subscribe(
      res => {
        if (res.code == 200) {
          this.employementLevelList = res.data;
          this.employementLevelList.filter((item, i) => { item['itemName'] = item.levelTitle; item['id'] = item.companyPositionLevelID; });
          this.selectedEmployementLevel = this.employementLevelList.filter(i => i.id == this.additionalInfo.companyPositionLevelID);
        }
      })
  }

  onSelectSelectedEmployementLevel(item, selected) {
    console.log(item);
    this.additionalInfo.companyPositionLevelID = item.id;   
     
    
    // this.selectedEmployementLevel.filter(item=>{
    //   if(item.contractTypeID==2){
    //     this.additionalInfo.remunarationPeriod="Hourly Rate"
    //   }
    // })
    
    //console.log("optionnnnn..",this.additionalInfo.remunarationPeriod);
    this.setEmployeeSalary();
  }

  setEmployeeSalary() {
    if (!this.selectedEmployementLevel.length || !this.employementLevelList.length) return;
    let index = this.selectedEmployementLevel[0].contractType.findIndex(i => this.employmentTypeID == i.contractTypeID);
    if (index >= 0) {
      this.additionalInfo.remunarationRate = this.selectedEmployementLevel[0].contractType[index].salary;
  

    }
    //console.log("optionnnnn..",this.additionalInfo.remunarationPeriod);
  }

  resetEmployeeSalary() {
    this.additionalInfo.remunarationRate = '';
  }

  isValidSalary() {
    if (!this.selectedEmployementLevel.length || !this.employementLevelList.length) return;
    let index = this.selectedEmployementLevel[0].contractType.findIndex(i => this.employmentTypeID == i.contractTypeID);
    if (index >= 0) {
      this.validSalary = Number(this.additionalInfo.remunarationRate) >= Number(this.selectedEmployementLevel[0].contractType[index].salary)
      // this.profile.remunarationRate = this.selectedEmployementLevel[0].contractType[index].salary;
      this.minSalary = !this.validSalary ? this.selectedEmployementLevel[0].contractType[index].salary : null;

    }
  }

  getDropdownList() {
  
    this.companyService.getDropdown().subscribe(
      res => {
        if (res.code == 200) {
          this.contractList = res.data;
          this.contractList.filter((item, i) => { item['itemName'] = item.title; item['id'] = item.contractTypeID; });
          this.employementList = JSON.parse(JSON.stringify(this.contractList));
         
          this.employementList.push({ "id": "x", "itemName": "Own contract" });
          //console.log("addditional",this.additionalInfo);
          if (this.additionalInfo.contractType == 'HHR') {
            this.selectedEmployeeTypes = this.contractList.filter(i => i.id == this.additionalInfo.employmentTypeID);
            this.employmentTypeID = this.selectedEmployeeTypes[0].id;
          } else if (this.additionalInfo.contractType == 'Own' ||
                     this.additionalInfo.contractType == 'Own_2' ||
                     this.additionalInfo.contractType == 'Own_3' ||
                     this.additionalInfo.contractType == 'Own_4') {
                       let hhrContract=this.contractList;
                       let temp= JSON.parse(JSON.stringify(this.ownContractList));
            this.selectedEmployeeTypes = this.employementList.filter(i => i.id == 'x');
            this.selectedContractTypes = this.contractList.filter(i => i.id == this.additionalInfo.employmentTypeID);
            this.selectedOwnContract = temp.filter(i=> i.id == this.additionalInfo.contractType);
           // console.log("contract.......",this.selectedOwnContract);
            this.ownContractID = this.selectedOwnContract[0].id;
            this.employmentTypeID = this.selectedEmployeeTypes[0].id;
            this.contractTypeID = this.selectedContractTypes[0].id;
          }
        }
      })
  }

  // To get Own contract types(eg. Own contract 2, Own contract 3 etc)
  getOwnContractList(){
    this.isProcessing = true;
    this.companyService.getOwnContract().subscribe(
      res => {
        this.isProcessing = false;
        if (res.code == 200) {
          this.ownContract = res.data;
          this.ownContract.types.forEach((item,i)=>{
              if(item){
                    if(item=="Own"){
                      let data = {"id" : item,"itemName":item+"\t"+"contract"}
                      this.ownContractList.push(data);
                      console.log("list",this.ownContractList);  
                    }
                    else{
                      let name= item.split("_");
                      let data = {"id" : item,"itemName":name[0]+"\t"+"contract"+"\t"+name[1]}
                      this.ownContractList.push(data);
                    }
                    //console.log("data->>>>>",this.ownContractList)
              }
          })
        }
      },
      err => {
        this.isProcessing = false;
      }
    )
  }

  //To filer contract type list based on Own contract types

  filterContractList(id){
    this.owncontractListTypes=[];
    if(id=='Own'){
      this.contractList.forEach((item,i)=>{ 
        for(let c of this.ownContract.contractTypeIDs.Own){
          if(c.contractTypeID==item.contractTypeID){
            this.owncontractListTypes.push(item);
          }
        }    
      })
     // console.log("data",this.owncontractListTypes);
    }
    if(id=='Own_2'){
      this.contractList.forEach((item,i)=>{ 
        for(let c of this.ownContract.contractTypeIDs.Own_2){
          if(c.contractTypeID==item.contractTypeID){
            this.owncontractListTypes.push(item);
          }
        }
      })
     // console.log("data",this.owncontractListTypes);
      
    }
    if(id=='Own_3'){
      this.contractList.forEach((item,i)=>{ 
        for(let c of this.ownContract.contractTypeIDs.Own_3){
          if(c.contractTypeID==item.contractTypeID){
            this.owncontractListTypes.push(item);
          }
        }
      })
     // console.log("data",this.owncontractListTypes);
      
    }
    if(id=='Own_4'){
      this.contractList.forEach((item,i)=>{ 
        for(let c of this.ownContract.contractTypeIDs.Own_4){
          if(c.contractTypeID==item.contractTypeID){
            this.owncontractListTypes.push(item);
          }
        }
      })
      //console.log("data",this.owncontractListTypes);
      
    }
  }

  onSelectEmployementType(item, selected) {
    //console.log("hgsdfsdf",item);
    if(item.itemName=='Part Time'){
      this.additionalInfo.remunarationPeriod="Hourly Rate";
    }
    else{
      this.additionalInfo.remunarationPeriod="Hourly";
    }
    this.employmentTypeID = (selected) ? item.id : null;
    this.ownContractID=null;
    this.contractTypeID = null;
    this.selectedContractTypes = [];
    this.selectedOwnContract =[];
    this.setEmployeeSalary();
  }

  onSelectOwnContractTypes(item,selected){
    this.selectedOwnContractTypes= item.id;
    this.ownContractID = (selected) ? item.id : null;
    this.contractTypeID = null;
    this.selectedContractTypes = [];
    this.filterContractList(item.id);
    this.setEmployeeSalary();
  }


  onSelectContractType(item, selected) {
    this.contractTypeID = (selected) ? item.id : null;
    //console.log("dkjfhskj",this.contractTypeID);    
  }

  saveAs(type,profileForm) {
    // if (this.profile.signature.text == '' || this.profile.signature.type == '') return;
    this.errors=[];
    this.errors = this.coreService.getFormValidationErrors(profileForm);
    if(this.errors.length>0){
     
    }
  if( this.errors && this.errors.length==0){
    this.additionalInfo.employmentTypeID = !(this.contractTypeID) || (this.employmentTypeID !== 'x') ? this.employmentTypeID : this.contractTypeID; 
    //this.additionalInfo.contractType = !(this.contractTypeID) || (this.employmentTypeID !== 'x') ? "HHR" : "Own";
    this.additionalInfo.contractTypeID = (this.ownContractID)? this.ownContractID : "HHR";
            if(this.selectedOwnContractTypes=='Own'){
                this.additionalInfo.contractType="Own";
                //this.contractTypeID = "Own";
            }
            else if(this.selectedOwnContractTypes=='Own_2'){
              this.additionalInfo.contractType="Own_2";
              //this.contractTypeID = "Own_2";     
            }
            else if(this.selectedOwnContractTypes=='Own_3'){
              this.additionalInfo.contractType="Own_3";
              //this.contractTypeID = "Own_3";
            }
            else if(this.selectedOwnContractTypes=='Own_4'){
              this.additionalInfo.contractType="Own_4";
              this.contractTypeID = "Own_4";              
            }
            else{
              this.additionalInfo.contractType="HHR"; 
              //this.contractTypeID = "HHR";                               
            }
    this.additionalInfo.contractStartDate = (this.contractStartDate) ? moment(this.contractStartDate).format('YYYY-MM-DD') : null;
    this.additionalInfo.contractEndDate = (this.contractEndDate) ? moment(this.contractEndDate).format('YYYY-MM-DD') : null;
    this.profile.personalInformation.dob = !(this.birthday) ? '' : moment(this.birthday).format('YYYY-MM-DD');
    this.profile.employeeLicence.expiryDate = !(this.licExpiry) ? '' : moment(this.licExpiry).format('YYYY-MM-DD');
    this.additionalInfo.employeeStartDate = !(this.employeeStartDate) ? '' : moment(this.employeeStartDate).format('YYYY-MM-DD');
  console.log("doc save",this.additionalInfo.mandatoryDocuments);
      this.profile.mandatoryDocuments=this.additionalInfo.mandatoryDocuments;
    let reqBody = this.profile;
    // console.log("dfsadkjfsd",this.profile);
    reqBody['saveType'] = type;
    reqBody['employeeUserID'] = this.employeeId;
    reqBody['employeeDocumentSubmit'] = this.employeeDocumentSubmit;
    reqBody['employeeDocumentSigned'] = this.employeeDocumentSigned;
    reqBody['additionalInformation'] = this.additionalInfo;
    reqBody['awardID'] = this.additionalInfo.awardIDs;
    reqBody['awardName'] = this.additionalInfo.awardName;
    this.isSaving = true;
    this.companyService.updateEmployeeProfile(reqBody).subscribe(
      res => {
        this.isSaving = false;
        if (res.code == 200) {
          this.coreService.notify("Successful", "Profile details are updated successfully.", 1);
          if (type == 'save') {
            this.router.navigate(['/app/company-admin/employee-management']);
          }
        }
        else return this.coreService.notify("Unsuccessful", res.message, 0);
      },
      error => {
        this.isSaving = false;
        if (error.code == 400) {
          return this.coreService.notify("Unsuccessful", error.message, 0);
        }
        this.coreService.notify("Error", "Error while updating profile details.", 0)
      }
    )
  }
  }

// upload profile photo

  handleFileSelect($event) {
    const files = $event.target.files || $event.srcElement.files;
    const file: File = files[0];
    console.log("image-->",file.size);
    let validFileExtensions = [".jpg", ".jpeg", ".png"];
    
    if(file.size>1961432){
      return this.coreService.notify("Unsuccessful", "The photo file is to large! Please use a file smaller that 2MB. Please try again.", 0)
    }

    let ext = file.name.substring(file.name.lastIndexOf('.')).toLowerCase();
    if (validFileExtensions.indexOf(ext) >= 0) {
      const formData = new FormData();
      formData.append('profileImage', file);

      this.isProcessingImg = true;
      this.companyService.uploadDoc(formData).subscribe(res => {
        this.isProcessingImg = false;
        this.profile.personalInformation.profileImageUrl = res.data.profileImage;
      });
    } else return this.coreService.notify("Unsuccessful", "Invalid file extension.", 0)
  }

  //upload employee document

  onpopup(type) {
    let dialogRef = this.dialog.open(UploadEmpDocumentDialog, { width: '500px' });
    dialogRef.componentInstance.type = type;
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        console.log('result ==>> ', result);
        // if (type == 1) this.employeeDocumentSigned.push(result);
        if (type == 2) this.employeeDocumentSubmit.push(result);
        else if (type == 'award') {
          this.additionalInfo.awardIDs = result.awardID;
          this.additionalInfo.awardName = result.title;
        }
      }
    });
  }


  // remove mandatory and employee uploaded documents 
  removeDoc(type, index) {
    this.confirmService.confirm(this.confirmService.tilte,this.confirmService.message,this.viewContainerRef)
    .subscribe(res=>{
      if(res){
        if (type == 'employeeDocumentSubmit') {
          this.employeeDocumentSubmit.splice(index, 1);
        }
        else if(type=='employeeMandatoryDoc'){
          //this.docRequired= true;
          this.mandatoryDocName = this.additionalInfo.mandatoryDocuments[index].title;
          this.additionalInfo.mandatoryDocuments[index].title;
          this.additionalInfo.mandatoryDocuments[index].file='';
          this.additionalInfo.mandatoryDocuments[index].expiryDate = '';
          this.flashRedWhenDelete();             
        }
      }
    })       
  }

 //flash red the mandatory field when file uploaded 
flashRedWhenUpload(){
  for(let item of this.additionalInfo.mandatoryDocuments){
    if(!item.file){
      this.docRequired = true;
      break;
    }
    else{
      this.docRequired = false;
    }
  } 
}
//flash red the mandatory field when file deleted 
flashRedWhenDelete(){
  this.additionalInfo.mandatoryDocuments.forEach(item=>{
    if(!item.file){
      this.docRequired=true;
      return;
    }
  })
}

  //upload Mandatory documents
  uploadDocs(doc,index) {  
    let dialogRef = this.dialog.open(UploadMendatoryDocsPopupComponent, { width: '500px' });
    dialogRef.componentInstance.doc=JSON.parse(JSON.stringify(doc));
      dialogRef.afterClosed().subscribe((result) => {
      if(result) {  
        console.log("res",result);
        this.additionalInfo.mandatoryDocuments[index]['description'] = result['desc'];   
        this.additionalInfo.mandatoryDocuments[index]['file'] = result['edsFile'];               
        this.additionalInfo.mandatoryDocuments[index]['expiryDate'] = result['expiryDate'];                                           
        this.flashRedWhenUpload();
      }}) 
    } 
}


@Component({
  selector: 'upload-document',
  template: `
      <div *ngIf="type !== 'award'">
        <h3>Upload document</h3>
        <form name="docForm" #docForm="ngForm" (ngSubmit)="docForm.valid && upload()">
          <div class="form-group">
            <label>Title</label>
            <input type="text" name="title" #title="ngModel" [(ngModel)]="doc.title" placeholder="Title ie. Driver's Licence" required/>
            <div *ngIf="title.dirty || docForm.submitted">
                <div class="color-danger" [hidden]="title.valid"> Please enter the title. </div>
            </div>
          </div>
          <div>
            <div class="form-group">
              <label>Description</label>
              
              <textarea type="text" placeholder="Description" rows="4" name="description" #description="ngModel" [(ngModel)]="doc.desc" required></textarea>
              <div *ngIf="description.dirty || description.touched || docForm.submitted">
                  <div class="color-danger" [hidden]="description.valid"> Please enter the description. </div>
              </div>
            </div>
          </div>
          <div>
            <div class="form-group">
              <label>Expiry</label>
              <dp-date-picker class="form-control" name="expiryDate" #expiryDate="ngModel" [theme]="'dp-material'" [mode]="'day'" [(ngModel)]="licExpiry" [config]="config" placeholder="Choose a date"></dp-date-picker>
            </div>
          </div>
          <div>
            <div class="uploadBtn">
              <label>{{fileName}}</label>&nbsp;&nbsp;
              <button type="button" class="btn-bs-file btn btn-lg btn-primary">
                Upload document
                <input type="file" accept=".doc,.docx,.pdf,image/png,image/jpg,image/jpeg" name="doc" (change)="handleFileSelect($event)" />
              </button>
            </div>
            <strong>{{UploadedFileName}}</strong>
            <p>Select file (JPEG, PNG, PDF or DOC files) </p>
            <div *ngIf="!doc.edsFile && docForm.submitted">
                  <div class="color-danger"> Please select a document. </div>
            </div>
          </div>
          <md-progress-spinner *ngIf="isProcessingImg" mode="indeterminate" color=""></md-progress-spinner>
          <button *ngIf="!isProcessingImg" type="submit" class="btn btn-primary">Submit</button>
        </form>
      </div>

      <div *ngIf="type == 'award'">
        <md-progress-spinner *ngIf="isProcessing" mode="indeterminate" color=""></md-progress-spinner>
        <div *ngIf="!isProcessing" class="filter-wrapp box box-default contentBox">
          <div class="row">
              <div class="col-md-12">
                  <ul class="awardsFilter">
                      <li style="display:inline-block; margin:0px 7px 7px 0px;" *ngFor="let alpha of awards" (click)="toggleCollapse(alpha)">
                        <span class="badge badge-primary cursor" style="min-width:26px;">{{alpha}}</span>
                      </li>
                  </ul>
              </div>
          </div>
          <div class="AwardAlphabet-list">
            <div *ngFor="let alpha of awards" class="row">
              <div class="col-md-12" *ngIf="collapse[alpha]">
                  <div class="box box-default full-width">
                      <div class="box-header box-dark">{{alpha}}</div>
                      <div class="box-body">
                          <ul>
                              <li *ngFor="let award of awardList[alpha]; let i=index;">
                                  <div class="customChoice" style="display:inline-block;">
                                      <input [id]="'award'+award.awardID" type="radio" name="award" [value]="award.awardID" (change)="onCheckOption(i,alpha)"
                                          class="required">
                                      <label style="display:block !important;" [for]="'award'+award.awardID">{{award.title}}</label>
                                  </div>
                              </li>
                          </ul>
                      </div>
                  </div>
              </div>
            </div>
          </div>
        </div>
      </div>
          `,
  styles: [`
      h3{
        font-size:22px;
      }
      .form-group label{
        font-size: 15px;
        color: #4b4c4d;
        max-width: 600px;
        width: 100%;
        padding-right: 18px;
        font-family: inherit;
        display: block;
        padding-right: 12px;
      }
      .form-group input{
        display: block;
        width: 100%;
        padding: 0.5rem 0.75rem;
        font-size: 1rem;
        line-height: 1.25;
        color: #464a4c;
        background-color: #fff;
        background-image: none;
        background-clip: padding-box;
        border: 1px solid rgba(0, 0, 0, 0.15);
        border-radius: 0.2rem;
        transition: border-color ease-in-out 0.15s, box-shadow ease-in-out 0.15s;
      }
      .form-group textarea{
         display: block;
         width: 100%;
         border: 1px solid rgba(0, 0, 0, 0.15);
        border-radius: 0.2rem;
        transition: border-color ease-in-out 0
      }
      .uploadBtn{
        float:left;
        width:100%;
      }
      .uploadBtn .btn-primary{
          padding:6px;
          float:left;
          width:100%;
          position:relative;
      }
      .uploadBtn input[type="file"]{
          width: 100%;
          position: absolute;
          float: left;
          height: 100%;
          left: 0;
          top: 0;
          opacity: 0;
        }
      .uploadBtn label {
          display: none;
      }
      .customChoice {
        position: relative;
        input {
            +label {
                padding-left: 28px;
                cursor: pointer;
                font-weight: normal !important;
                position: relative;
                display:block !important;
                &:before {
                    content: "";
                    height: 19px;
                    width: 19px;
                    display: inline-block;
                    border: 1px solid #acacac;
                    position: absolute;
                    left: 0px;
                    border-radius: 50%;
                    -webkit-border-radius: 50%;
                    -moz-border-radius: 50%;
                    -ms-border-radius: 50%;
                }
            }
            display:none;
            &:checked+label {
                position: relative;
                &:after {
                    content: "";
                    position: absolute;
                    height: 7px;
                    width: 7px;
                    display: inline-block;
                    border-radius: 50%;
                    -webkit-border-radius: 50%;
                    -moz-border-radius: 50%;
                    -ms-border-radius: 50%;
                    left: 6px;
                    top: 6px;
                    background: #444444;
                }
            }
        }
    }
  `]
})
export class UploadEmpDocumentDialog implements OnInit {
  doc: CompanyDocument;
  type = null;
  UploadedFileName:any;
  fileName = "";
  isProcessingImg = false;
  _validFileExtensions = [".jpg", ".jpeg", ".png", ".pdf", ".doc", ".docx"];

  licExpiry: any;
  config: IDatePickerConfig = {};

  public awards = [];
  public awardList = {};
  public isProcessing = false;
  public collapse = {};

  constructor(public dialog: MdDialogRef<UploadEmpDocumentDialog>, public coreService: CoreService, public companyService: EmployeeManagementService) {
    this.doc = new CompanyDocument;
    this.config.locale = "en";
    this.config.format = "DD MMM YYYY";
    this.config.showMultipleYearsNavigation = true;
    this.config.weekDayFormat = 'dd';
    this.config.disableKeypress = true;
    this.config.monthFormat = "MMM YYYY";
    this.config.min = moment();
  }

  ngOnInit() {
    this.getAwards();
    this.collapse['A'] = true;
  }

  handleFileSelect($event) {
    const files = $event.target.files || $event.srcElement.files;
    const file: File = files[0];
    this.fileName = file.name;
    if(file.size>1961432){
      this.dialog.close();
      return this.coreService.notify("Unsuccessful", " Uploaded document file is to large is must be a file smaller that 2MB. Please try again!", 0)
    }
    
    let ext = this.fileName.substring(this.fileName.lastIndexOf('.')).toLowerCase();
    if (this._validFileExtensions.indexOf(ext) >= 0) {
      const formData = new FormData();
      formData.append('userDocument', file);

      this.isProcessingImg = true;
      this.companyService.uploadDoc(formData).subscribe(res => {
        this.isProcessingImg = false;
        this.doc.edsFile = res.data.userDocument;
        this.UploadedFileName= res.data.fileName;
      });
    } else return this.coreService.notify("Unsuccessful", "Invalid file extension.", 0)

  }

  upload() {
    //  || (this.type == 1 && !this.licExpiry)
    if (!this.doc.edsFile) return;
    this.doc.expiryDate = !(this.licExpiry) ? '' : moment(this.licExpiry).format('YYYY-MM-DD');
    this.dialog.close(this.doc);
  }

  // *************************************

  toggleCollapse(alpha) {
    this.collapse = {};
    this.collapse[alpha] = true;
  }

  getAwards() {
    this.isProcessing = true;
    let params = { "filter": { "awards": {} } };
    this.companyService.getAwards(JSON.stringify(params)).subscribe(
      res => {
        this.isProcessing = false;
        if (res.code == 200) {
          this.awardList = res.data;

          let arr = Object.keys(this.awardList);
          this.awards = arr.filter(i => { if (this.awardList[i].length > 0) return i; })
        }
      },
      error => {
        this.isProcessing = false;
        console.log(error);
      })
  }

  onCheckOption(index, alpha) {
    this.dialog.close(this.awardList[alpha][index]);
  }
}
