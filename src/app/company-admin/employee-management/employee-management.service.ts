import { Injectable } from '@angular/core';
import { RestfulLaravelService } from '../../shared/services/restful-laravel.service';
import { InviteEmployeeData, GeneralInfo, ExtraInfo, Proposed } from './employee-management';

@Injectable()
export class EmployeeManagementService {

  constructor(private restfulLaravelService: RestfulLaravelService) { }

  getEmployeeList(params) {
    return this.restfulLaravelService.get(`employee-listing/${params}`);
  }

  getFilters() {
    return this.restfulLaravelService.get('employee-listing-filter-data');
  }

  getEmployeeProfile(id) {
    return this.restfulLaravelService.get(`employee-profile/${id}`);
  }

  updateEmployeeProfile(reqBody) {
    return this.restfulLaravelService.post('complete-profile', reqBody)
  }

  uploadDoc(formData) {
    return this.restfulLaravelService.post('upload-file', formData);
  }

  getCountries() {
    return this.restfulLaravelService.get('country-list');
  }

  getInvitedEmployeeProfile(id) {
    return this.restfulLaravelService.get(`invite-employee-profile/${id}`);
  }

  getUnassignedPositions() {
    return this.restfulLaravelService.get(`get-unassigned-company-position`);
  }

  getAwards(params) {
    return this.restfulLaravelService.get(`admin/award-list/${params}`);
  }

  getDropdown() {
    return this.restfulLaravelService.get('list-contract-type');
  }

  getOwnContract(){
    return this.restfulLaravelService.get('get-own-types-present');    
  }

  inviteEmployee(reqBody) {
    sessionStorage.removeItem('inviteEmployeeData');
    return this.restfulLaravelService.post('invite-employee', reqBody);
  }

  getCandidateDetail(id) {
    return this.restfulLaravelService.get(`get-candidate-details/${id}`);
  }

  getInvitedEmployeeDetail(id) {
    return this.restfulLaravelService.get(`get-invited-employee-details/${id}`);
  }

  getEmploymentLevel(id) {
    return this.restfulLaravelService.get(`get-company-position-level/${id}`);
  }

  getAssignedSpecialRoles() {
    return this.restfulLaravelService.get('get-assignmentof-special-roles');
  }

  downloadPdf(doc) {
    switch (doc.type) {
      case 'contract':
        window.open(`${this.restfulLaravelService.baseUrl}generate-company-contract-pdf/${doc.docID}`); break;
      case 'policy':
        window.open(`${this.restfulLaravelService.baseUrl}generate-company-policy-pdf/${doc.docID}`); break;
      case 'position':
        window.open(`${this.restfulLaravelService.baseUrl}generate-position-pdf/${doc.docID}`); break;
    }
  }


  // ===================== Invite Employee =======================
  inviteEmployeeData = <InviteEmployeeData>{};

  setPosition(id, orgNodeID) {
    this.inviteEmployeeData.orgNodeID = orgNodeID;
    this.inviteEmployeeData.positionID = id;
    this.saveInviteEmployeeData();
  }

  setInfo(data: GeneralInfo | ExtraInfo | Proposed) {
    this.inviteEmployeeData = this._merge(this.inviteEmployeeData, data);
    this.saveInviteEmployeeData();
  }

  getInviteEmployeeData(): InviteEmployeeData {
    let data = sessionStorage.getItem('inviteEmployeeData');
    if (data) {
      this.inviteEmployeeData = JSON.parse(data);
    }
    return this.inviteEmployeeData;
  }

  saveInviteEmployeeData() {
    sessionStorage.setItem('inviteEmployeeData', JSON.stringify(this.inviteEmployeeData));
  }

  deleteMendatoryDoc(id){
    return this.restfulLaravelService.get(`delete-mendatory-document/${id}`);
  }
  getSuppernnuationList(reqBody) {
    return this.restfulLaravelService.get(`suppernnuation-list/${encodeURIComponent(JSON.stringify(reqBody))}`);
}




  /**
   * Recursively merge properties of two objects 
   */
  _merge(obj1: InviteEmployeeData, obj2: GeneralInfo | ExtraInfo | Proposed) {

    for (var p in obj2) {
      try {
        // Property in destination object set; update its value.
        if (obj2[p].constructor == Object) {
          obj1[p] = this._merge(obj1[p], obj2[p]);

        } else {
          obj1[p] = obj2[p];

        }

      } catch (e) {
        // Property in destination object not set; create it and set its value.
        obj1[p] = obj2[p];

      }
    }

    return obj1;
  }
}
