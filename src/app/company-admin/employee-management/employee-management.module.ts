import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { MaterialModule } from '@angular/material';
import { DataTableModule } from "angular2-datatable";
import { EmployeeListingComponent } from './employee-listing/employee-listing.component';
import { EmployeeDashboardComponent } from './employee-dashboard/employee-dashboard.component';
import { EmployeeProfileComponent } from './employee-profile/employee-profile.component';
import { EmployeeManagementRoutingModule } from './employee-management.routing';
import { EmployeeManagementService } from './employee-management.service';
import { UnassignedPositionComponent } from './invite employee/unassigned-position/unassigned-position.component';
import { GeneralInformationComponent, AwardDialog } from './invite employee/general-information/general-information.component';
import { ExtraInformationComponent } from './invite employee/extra-information/extra-information.component';
import { ProposedComponent } from './invite employee/proposed/proposed.component';
import { SharedModule } from '../../shared/shared.module';
import { AngularMultiSelectModule } from 'angular2-multiselect-checkbox-dropdown/angular2-multiselect-dropdown';
import { OfferExpiredComponent } from './offer-expired/offer-expired.component';
import { EmployeeManagementGuardService } from './employee-management-guard.service';
import { CountryStateService } from "../../shared/services/country-state.service";
import { EditEmployeeProfileComponent, UploadEmpDocumentDialog } from './edit-employee-profile/edit-employee-profile.component';

@NgModule({
  imports: [
    CommonModule,
    MaterialModule,
    DataTableModule,
    EmployeeManagementRoutingModule,
    AngularMultiSelectModule,
    SharedModule,
    FormsModule
  ],
  declarations: [EmployeeListingComponent, EmployeeDashboardComponent, EmployeeProfileComponent, UnassignedPositionComponent, GeneralInformationComponent, ExtraInformationComponent,
    ProposedComponent, AwardDialog, OfferExpiredComponent, EditEmployeeProfileComponent, UploadEmpDocumentDialog],
  providers: [EmployeeManagementService, EmployeeManagementGuardService,CountryStateService],
  entryComponents: [AwardDialog, UploadEmpDocumentDialog]
})
export class EmployeeManagementModule { }
