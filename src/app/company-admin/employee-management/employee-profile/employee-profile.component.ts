import { Component, OnInit, AfterViewInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { APPCONFIG } from '../../../config';
import { EmployeeManagementService } from '../employee-management.service';
import { CoreService } from '../../../shared/services/core.service';
import * as moment from 'moment';

@Component({
  selector: 'app-employee-profile',
  templateUrl: './employee-profile.component.html',
  styleUrls: ['./employee-profile.component.scss']
})
export class EmployeeProfileComponent implements OnInit {

  dropdownList = [];
  selectedItems = [];
  dropdownSettings = {};

  actionList = [];
  selectedAction = [];
  actionSettings = {};

  collapse = {};
  employeeId: string;
  employeeProfile = {};
  employeeDocs = [];
  employeeAwards = [];
  employeeUploadDocs = [];
  isProcessing: boolean;

  constructor(private employeeService: EmployeeManagementService, private router: Router, private route: ActivatedRoute,
    private coreService: CoreService) { }

  ngOnInit() {
    APPCONFIG.heading = "Employee profile";

    this.dropdownList = [
      { "id": 1, "itemName": "Award 1" },
      { "id": 2, "itemName": "Award 2" },
      { "id": 3, "itemName": "Award 3" },
      { "id": 4, "itemName": "Award 4" },
    ];

    this.actionList = [
      { "id": 1, "itemName": "PIP", url: `/app/company-admin/performance-improvement-plan/create/${this.employeeId}` },
      { "id": 2, "itemName": "Behaviour PIP", url: `/app/company-admin/behavioral-performance-improvement-plan/create/${this.employeeId}` },
      { "id": 3, "itemName": "Terminate" },
      { "id": 4, "itemName": "Redundant" },
      { "id": 5, "itemName": "Leave request" },
      { "id": 6, "itemName": "Development plan", url: `/app/company-admin/development-plan/create/${this.employeeId}` },
      { "id": 7, "itemName": "Upload documents" },
      { "id": 8, "itemName": "OH&S" },
      { "id": 9, "itemName": "Disciplinary action", url: `/app/company-admin/disciplinary-meeting/create/${this.employeeId}` },
      { "id": 10, "itemName": "Assist PI" },
      { "id": 11, "itemName": "On boarding checklist" },
      { "id": 12, "itemName": "Off boarding checklist" },
      { "id": 13, "itemName": "Exit survey" },
      { "id": 14, "itemName": "Off-boarding" },
      { "id": 15, "itemName": "Induction" },
      { "id": 16, "itemName": "Create performance appraisal", url: `/app/company-admin/performance-appraisal/create/${this.employeeId}` },
      { "id": 17, "itemName": "Training plan" },
    ];

    this.dropdownSettings = {
      singleSelection: false,
      text: "Select name",
      selectAllText: 'Select all',
      unSelectAllText: 'UnSelect all',
      enableSearchFilter: true,
    };

    this.actionSettings = {
      singleSelection: true,
      text: "Action",
      enableSearchFilter: true,
    };

    if (this.route.snapshot.params['type']) {
      console.log('inv')
      this.employeeId = this.route.snapshot.params['id'];
      this.getInvitedEmployeeProfile();
    } else if (this.route.snapshot.params['id']) {
      console.log('no invite')
      this.employeeId = this.route.snapshot.params['id'];
      this.getEmployeeProfile();
    }
  }

  onActivites(){
    this.router.navigate([`/app/company-admin/activities/${this.route.snapshot.params['id']}`]);
  }

  onItemSelect(item) {
    if (item.url) return this.router.navigate([item.url]);

  }
  OnItemDeSelect(item: any) {
    console.log(item);
    console.log(this.selectedItems);
  }
  onSelectAll(items: any) {
    console.log(items);
  }
  onDeSelectAll(items: any) {
    console.log(items);
  }

  formatDateTime(date) {
    return date ? moment(date).format('DD MMM YYYY hh:mm A') : 'Not specified';
  }

  getEmployeeProfile() {
    this.isProcessing = true;
    this.employeeService.getEmployeeProfile(this.employeeId).subscribe(
      res => {
        if (res.code == 200) {
          console.log(res);
          this.employeeProfile = res.data[0];
          this.employeeDocs = res.data[1].employeeDocumentSigned;
          this.employeeUploadDocs = res.data[2].employeeDocumentSubmit;
          this.employeeAwards = res.data[3].employeeAward;
          this.isProcessing = false;
        }
      },
      error => {
        this.isProcessing = false;
        this.coreService.notify("Error", "Error while getting profile details", 0)
      }
    )
  }

  getInvitedEmployeeProfile() {
    this.isProcessing = true;
    this.employeeService.getInvitedEmployeeProfile(this.employeeId).subscribe(
      res => {
        if (res.code == 200) {
          console.log(res);
          this.employeeProfile = res.data[0];
          this.employeeDocs = res.data[1].employeeDocumentSigned;
          this.employeeUploadDocs = res.data[2].employeeDocumentSubmit;
          this.employeeAwards = res.data[3].employeeAward;
          this.isProcessing = false;
        }
      },
      error => {
        this.isProcessing = false;
        this.coreService.notify("Error", "Error while getting profile details", 0)
      }
    )
  }



}


