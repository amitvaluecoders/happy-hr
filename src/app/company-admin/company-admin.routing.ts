import { ModuleWithProviders } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CompanyAdminGuardService } from './company_admin_guard.service';
export const CompanyAdminPagesRoutes: Routes = [
    {
        path: '',
        canActivate: [CompanyAdminGuardService],
        children: [
            { path: '', loadChildren: './company-admin-dashboard/company-admin-dashboard.module#CompanyAdminDashboardModule' },
            { path: 'grievance', loadChildren: './grievance/grievance.module#GrievanceModule' },
            { path: 'employee-management', loadChildren: './employee-management/employee-management.module#EmployeeManagementModule' },
            { path: 'subcontractor-management', loadChildren: './subcontractor-management/subcontractor-management.module#SubcontractorManagementModule' },
            { path: ':ct/contract', loadChildren: './contracts/contracts.module#CompanyContractsModule'},
            { path: 'development-plan', loadChildren: './development-plan/development-plan.module#DevelopmentPlanModule' },
            { path: 'candidate-management', loadChildren: './candidate-management-shortlisted/candidate-management-shortlisted.module#CandidateManagementShortlistedModule' },
            { path: 'checklist', loadChildren: './checklist/checklist.module#ChecklistModule' },
            { path: 'performance-improvement-plan', loadChildren: './performance-improvement-plan/performance-improvement-plan.module#PerformanceImprovementPlanModule' },
            { path: 'behavioral-performance-improvement-plan', loadChildren: './behavioral-performance-improvement-plan/behavioral-performance-improvement-plan.module#BehavioralPerformanceImprovementPlanModule' },
            { path: 'grievance', loadChildren: './grievance/grievance.module#GrievanceModule' },
            { path: 'ohs', loadChildren: './oh/oh.module#OhModule' },
            { path: 'training-day', loadChildren: './training-day/training-day.module#TrainingDayModule' },
            { path: 'disciplinary-meeting', loadChildren: './disciplinary-meeting/disciplinary-meeting.module#DisciplinaryMeetingModule' },
            { path: 'user-access', loadChildren: './user-access-control/user-access-control.module#UserAccessControlModule' },
            { path: 'leaves', loadChildren: './leaves/leaves.module#LeavesModule' },
            { path: 'policy-documents', loadChildren: './policy-documents/policy-documents.module#PolicyDocumentsModule' },
            //{ path: 'contracts-listing', loadChildren: './contracts/contracts.module#ContractsModule'}
            // { path: 'feedback-management', loadChildren: './feedback-management/feedback-management.module#FeedbackManagementModule'},
            // { path: 'ohs', loadChildren: './ohs/ohs.module#OHSModule'},
            // { path: 'site-setting', loadChildren: './site-setting/site-setting.module#SiteSettingModule'},
            // { path: 'policy', loadChildren: './policy-document-management/policy-document-management.module#PolicyDocumentManagementModule'},
            { path: 'contract', loadChildren: './contracts/contracts.module#CompanyContractsModule' },
            { path: 'file-notes', loadChildren: './file-notes/file-notes.module#FileNotesModule' },
            // { path: 'letters', loadChildren: './letters/letters.module#LettersModule'},
            // { path: 'exit-survey', loadChildren: './exit-survey/exit-survey.module#ExitSurveyModule'},
            // { path: 'discount-coupon-management', loadChildren: './discount-coupon-management/discount-coupon-management.module#DiscountCouponManagementModule'},
            { path: 'message', loadChildren: './message/ca-message.module#companyAdminMessageModule' },
            { path: 'broadcast', loadChildren: './broadcast-messages/broadcast-messages.module#BroadcastMessagesModule' },
            { path: 'survey', loadChildren: './survey360/survey.module#SurveyModule' },
            // { path: 'subscription-plans', loadChildren: './subscription-plans/subscription-plans.module#SubscriptionPlansModule'}
            { path: 'organisation-chart', loadChildren: './organisation-chart/organisation-chart.module#OrganisationChartModule' },
            { path: 'listing-page', loadChildren: './company-module-listing/company-module-listing.module#CompanyModuleListingModule' },
            { path: 'trophy', loadChildren: './trophy/company-trophy.module#CompanyTrophyModule' },
            { path: 'position-description', loadChildren: './postion-description-templates/postion-description-templates.module#PostionDescriptionTemplatesModule' },
            { path: 'jobs', loadChildren: './jobs/jobs.module#JobsModule' },
            { path: 'setting', loadChildren: './settings/settings.module#SettingsModule' },
            { path: 'performance-appraisal', loadChildren: './performance-appraisal/performance-appraisal.module#PerformanceAppraisalModule' },
            { path: 'advertisement', loadChildren: './advertisement-management/advertisement-management.module#AdvertisementManagementModule' },
            { path: 'sub-company', loadChildren: './sub-companies/sub-companies.module#SubCompaniesModule' },
            { path: 'exit-survey', loadChildren: './exit-survey/cmp-exit-survey.module#CmpExitSurveyModule' },
            { path: 'performance-indicator', loadChildren: './performance-indicator/performance-indicator.module#PerformanceIndicatorModule' },
            { path: 'sub-contractor', loadChildren: './contractor/contractor.module#ContractorModule' },
            { path: 'general-notes', loadChildren: './general-notes/general-notes.module#GeneralNotesModule' }
        ]
    }
];

export const CompanyAdminRoutingModule = RouterModule.forChild(CompanyAdminPagesRoutes);
