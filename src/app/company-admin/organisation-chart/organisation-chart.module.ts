import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { MaterialModule } from '@angular/material';
import { OrganisationChartRoutingModule } from './organisation-chart.routing';
import { OrganisationChartService } from './organisation-chart.service';
import { OrganisationChartComponent } from './organisation-chart/organisation-chart.component';
import { OrganizationChartPopupComponent } from './organization-chart-popup/organization-chart-popup.component';
// import { TreeModule } from 'ng2-tree';
import { AngularMultiSelectModule } from 'angular2-multiselect-checkbox-dropdown/angular2-multiselect-dropdown';
import { OrganizationChartGuardService } from './organization-chart-guide.service';
import { SharedModule } from '../../shared/shared.module';
import { QuillModule } from 'ngx-quill';


@NgModule({
  imports: [
    QuillModule,
    SharedModule,
    CommonModule,
    MaterialModule,
    FormsModule,
    OrganisationChartRoutingModule,
    AngularMultiSelectModule
  ],
  declarations: [OrganisationChartComponent, OrganizationChartPopupComponent],
  providers: [OrganisationChartService, OrganizationChartGuardService],
  entryComponents: [OrganizationChartPopupComponent]
})
export class OrganisationChartModule { }
