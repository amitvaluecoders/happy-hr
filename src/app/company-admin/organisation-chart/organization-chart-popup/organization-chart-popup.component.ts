import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { MdSnackBar, MdDialog, MdDialogRef } from '@angular/material';
import { CoreService } from "../../../shared/services/core.service";
import { OrganisationChartService } from "../organisation-chart.service";

@Component({
  selector: 'app-organization-chart-popup',
  templateUrl: './organization-chart-popup.component.html',
  styleUrls: ['./organization-chart-popup.component.scss']
})
export class OrganizationChartPopupComponent implements OnInit {

  displayType = "action";
  removeType = '';
  letterType = '';
  isProcessing = false;
  isSending = false;
  userData = {
    "contractorInvitedID":'',"contractorUserID":'',
    isInductionComplete:"Yes",
    "employeeUserID": "", "employeeInvitedID": "", "nodeID": "", "parentNodeID": "", "companyPositionID": "",
    "positionType": "", "type": "", "subNodes": [], "title": "", "count": "", "role": { "roleID": "", "title": "" },
    "subRole": { "isSho": false, "isOhso": false, "isFw": false, "isFao": false }
  }
  letter = { "subject": "", "body": "" };
  chart = { "companyID": "", "title": "", "type": "", "subNodes": [] };
  departmentTitle = '';
  positionList = [];
  positions = [];
  roles = [];
  subRoleList = [];
  selectedPosition = [];
  dropdownSettings;
  candidateID = '';
  isManager = false;
  isInviteClicked = false;
  reqBody:any;

  constructor(public dialogRef: MdDialogRef<OrganizationChartPopupComponent>, public coreService: CoreService,
    public companyService: OrganisationChartService, public router: Router) { }

  ngOnInit() {
    this.dropdownSettings = {
      singleSelection: true,
      text: "Select a position",
      enableSearchFilter: true,
      classes: "myclass"
    };

    this.isManager = this.coreService.isManager();
  }

  private _getLetter(type) {
    let isInvited = ! ( this.userData.employeeUserID || this.userData.contractorUserID ) ;
    let userId = isInvited ? ( this.userData.employeeInvitedID || this.userData.contractorInvitedID  )  : (this.userData.employeeUserID || this.userData.contractorUserID);
    console.log('isInvited ==>> ', isInvited, userId);
    if (type == 'terminate') {
      this.letterType = ( isInvited || this.userData.isInductionComplete =='No') ? 'revoke-invite-offer' : 'termination';
      return this.companyService.terminate(userId, isInvited,this.userData.nodeID,this.userData.isInductionComplete);
    } else if (type == 'redundant') {
      this.letterType = isInvited ? 'invited-redundancy-letter' : 'redundancy';
      return this.companyService.makeRedundant(userId, isInvited);
    } else if (type == 'left') {
      this.letterType = isInvited ? 'invited-left-on-accord-letter' : 'left-on-their-own-accord';
      return this.companyService.userLeft(userId, isInvited);
    }
  }

  getLetter(type) {
    this.displayType = 'letter';
    if (!this.userData.employeeUserID && !this.userData.employeeInvitedID && !this.userData.contractorInvitedID && !this.userData.contractorUserID) {
      this.dialogRef.close(false);
      return this.coreService.notify("Unsuccessful", "Please select a valid user", 0);
    }

    this.isProcessing = true;
    this._getLetter(type).subscribe(
      res => {
        this.isProcessing = false;
        if (res.code == 200) {
          this.letter = res.data;
        }
        else return this.coreService.notify("Unsuccessful", res.message, 0);
      },
      err => {
        this.isProcessing = false;
        this.dialogRef.close(false);
        if (err.code == 400) {
          return this.coreService.notify("Unsuccessful", err.message, 0);
        }
        this.coreService.notify("Unsuccessful", "Error while getting data.", 0)
      }
    )
  }

  reminder(){
    
    let body= {
      "userID" : this.userData.employeeUserID
    };
    this.isProcessing = true;
    this.companyService.reminder(body).subscribe(
      res=>{
        if(res.code==200){
          this.isProcessing = false;
          this.dialogRef.close(false);
          this.coreService.notify("Successful", res.message, 1);
        }
        else return this.coreService.notify("Unsuccessful", res.message, 0);
    },
    err => {
      this.isProcessing = false;
        this.dialogRef.close(false);
      if (err.code == 400) {
        return this.coreService.notify("Unsuccessful", err.message, 0);
      }
      this.coreService.notify("Unsuccessful", "Error while getting data.", 0)
    }
  )
  }



  sendLetter() {
    // if (!this.userData.employeeUserID) {
    //   this.dialogRef.close(false);
    //   return this.coreService.notify("Unsuccessful", "User is not a current employee", 0);
    // }

    let reqBody = {
      "nodeID": this.userData.nodeID,
      "subject": this.letter.subject, "body": this.letter.body, "type": this.letterType
    };
    if (!this.userData.employeeUserID) {
      reqBody["employeeInvitedID"] = this.userData.employeeInvitedID;
    } else {
      reqBody["userID"] = this.userData.employeeUserID
    }
    this.isSending = true;
    this.companyService.sendLatter(reqBody).subscribe(
      res => {
        this.isSending = false;
        this.dialogRef.close('reload');
        if (res.code == 200) {
          this.coreService.notify("Successful", res.message, 1);
        }
        else return this.coreService.notify("Unsuccessful", res.message, 0);
      },
      err => {
        this.isSending = false;
        this.dialogRef.close(false);
        if (err.code == 400) {
          return this.coreService.notify("Unsuccessful", err.message, 0);
        }
        this.coreService.notify("Unsuccessful", "Error while submitting data.", 0)
      }
    )
  }

  cancel() {
    this.dialogRef.close(false);
  }

  resendOffer(){
    this.isProcessing = true;
    this.companyService.resendOffer(this.userData.nodeID).subscribe(
      res => {
        this.isProcessing = false;
        this.dialogRef.close(false);
        if (res.code == 200) {
          this.coreService.notify("Successful", res.message, 1);
        }
        else return this.coreService.notify("Unsuccessful", res.message, 0);
      },
      err => {
        this.isProcessing = false;
        this.dialogRef.close(false);
        if (err.code == 400) {
          return this.coreService.notify("Unsuccessful", err.message, 0);
        }
        this.coreService.notify("Unsuccessful", "Error while submitting data.", 0)
      }
    )
  }

  getPositionList(type) {
    this.displayType = type;
    this.isProcessing = true;
    this.companyService.getPositions().subscribe(
      res => {
        this.isProcessing = false;
        if (res.code == 200) {
          this.positionList = res.data;
        }
        else return this.coreService.notify("Unsuccessful", res.message, 0);
      },
      err => {
        this.isProcessing = false;
        if (err.code == 400) {
          return this.coreService.notify("Unsuccessful", err.message, 0);
        }
        this.coreService.notify("Unsuccessful", "Error while getting data.", 0)
      }
    )
  }

  addPosition(position) {
    let reqBody = {
      "parentNodeID": this.userData.nodeID,
      "companyPositionID": position.companyPositionID
    };
    this.isProcessing = true;
    this.companyService.addPosition(reqBody).subscribe(
      res => {
        this.isProcessing = false;
        this.dialogRef.close('reload');
        if (res.code == 200) {
          this.coreService.notify("Successful", res.message, 1);
        }
        else return this.coreService.notify("Unsuccessful", res.message, 0);
      },
      err => {
        this.isProcessing = false;
        this.dialogRef.close(false);
        if (err.code == 400) {
          return this.coreService.notify("Unsuccessful", err.message, 0);
        }
        this.coreService.notify("Unsuccessful", "Error while submitting data.", 0)
      }
    )
  }

  reAssignPosition(position) {
    if (!this.userData.employeeUserID) {
      this.dialogRef.close(false);
      return this.coreService.notify("Unsuccessful", "User is not a current employee", 0);
    }

    let reqBody = {
      "nodeID": this.userData.nodeID,
      "employeeUserID": this.userData.employeeUserID,
      "companyPositionID": position.companyPositionID,
      "isInductionComplete":"No"
     
    };
    this.isProcessing = true;
    this.companyService.reAssignPosition(reqBody).subscribe(
      res => {
        this.isProcessing = false;
        this.dialogRef.close('reload');
        if (res.code == 200) {
          this.coreService.notify("Successful", res.message, 1);
        }
        else return this.coreService.notify("Unsuccessful", res.message, 0);
      },
      err => {
        this.isProcessing = false;
        this.dialogRef.close(false);
        if (err.code == 400) {
          return this.coreService.notify("Unsuccessful", err.message, 0);
        }
        this.coreService.notify("Unsuccessful", "Error while submitting data.", 0)
      }
    )
  }

  private getElem(node) {
    if (node.nodeID !== this.userData.nodeID && node.nodeID !== this.userData.parentNodeID) {
      node['id'] = node.nodeID;
      node['itemName'] = node.title + ' / ' + node.count;
      this.positions.push(node);
    }
    if (node.subNodes && node.subNodes.length) {
      for (let i = 0; i < node.subNodes.length; i++) {
        this.getElem(node.subNodes[i])
      }
    }
  }

  getPositions() {
    this.displayType = 'movePosition';
    this.isProcessing = true;
    // this.positions.push({ "id": this.chart.companyID, "title": this.chart.title });

    if (this.chart.subNodes && this.chart.subNodes.length) {
      for (let i = 0; i < this.chart.subNodes.length; i++) {
        this.getElem(this.chart.subNodes[i])
      }
    }
    this.isProcessing = false;
    console.log('positions == >> ', this.positions);
  }

  movePosition() {
    if (!this.selectedPosition.length) return;
    let parentNodeID = this.selectedPosition[0].nodeID;
    let reqBody = { "nodeID": this.userData.nodeID, "underParentNodeID": parentNodeID }
    this.isProcessing = true;
    this.companyService.moveToNewPosition(reqBody).subscribe(
      res => {
        this.isProcessing = false;
        this.dialogRef.close('reload');
        if (res.code == 200) {
          this.coreService.notify("Successful", res.message, 1);
        }
        else return this.coreService.notify("Unsuccessful", res.message, 0);
      },
      err => {
        this.isProcessing = false;
        this.dialogRef.close(false);
        if (err.code == 400) {
          return this.coreService.notify("Unsuccessful", err.message, 0);
        }
        this.coreService.notify("Unsuccessful", "Error while submitting data.", 0)
      }
    )
  }

  toggleAssignManager(assign) {
    if (!this.userData.employeeUserID && !this.userData.contractorUserID ) {   
          this.dialogRef.close(false);
          return this.coreService.notify("Unsuccessful", "User is not a current employee", 0);  
  
    } 
    // else if(!this.userData.contractorUserID && this.userData.employeeUserID){
    //   this.dialogRef.close(false);
    //   return this.coreService.notify("Unsuccessful", "User is not a current contractor", 0);
    // }
    if(this.userData.employeeUserID){
      this.reqBody = {
        "employeeUserID": this.userData.employeeUserID,
        "assign": assign
      }
    }
    else if(this.userData.contractorUserID){
      this.reqBody = {
        "employeeUserID": this.userData.contractorUserID,
        "assign": assign
      }
    }
    
    this.isProcessing = true;
    this.companyService.toggleAssignManager(this.reqBody).subscribe(
      res => {
        this.isProcessing = false;
        this.dialogRef.close('reload');
        if (res.code == 200) {
          this.coreService.notify("Successful", res.message, 1);
        }
        else return this.coreService.notify("Unsuccessful", res.message, 0);
      },
      err => {
        this.isProcessing = false;
        this.dialogRef.close(false);
        if (err.code == 400) {
          return this.coreService.notify("Unsuccessful", err.message, 0);
        }
        this.coreService.notify("Unsuccessful", "Error while submitting data.", 0)
      }
    )
  }
  

  addSubCompany(displayType) {
    this.displayType = displayType;
    this.onAddSubCompany();
  }

  addSubContractor(displayType) {
    this.displayType = displayType;
    this.onAddSubContractor();
  }

  addDepartment(displayType) {
    this.displayType = displayType;
    this.departmentTitle = (displayType == 'addDepartment') ? '' : this.userData.title;
  }

  onAddSubCompany() {
    let reqBody;
    reqBody = { "parentNodeID": this.userData.nodeID }
    this.isProcessing = true;
    this.companyService.addSubCompany(reqBody).subscribe(
      res => {
        this.isProcessing = false;
        this.dialogRef.close('reload');
        if (res.code == 200) {
          if (res.data) this.userData.nodeID = res.data.nodeID;
          this.coreService.notify("Successful", res.message, 1);
        }
        else return this.coreService.notify("Unsuccessful", res.message, 0);
      },
      err => {
        this.isProcessing = false;
        //  this.dialogRef.close(false);
        if (err.code == 400) {
          return this.coreService.notify("Unsuccessful", err.message, 0);
        }
        this.coreService.notify("Unsuccessful", "Error while submitting data.", 0)
      }
    )
  }

   onAddSubContractor() {
    let reqBody;
    reqBody = { "parentNodeID": this.userData.nodeID }
    this.isProcessing = true;
    this.companyService.addSubContractor(reqBody).subscribe(
      res => {
        this.isProcessing = false;
        this.dialogRef.close('reload');
        if (res.code == 200) {
          if (res.data) this.userData.nodeID = res.data.nodeID;
          this.coreService.notify("Successful", res.message, 1);
        }
        else return this.coreService.notify("Unsuccessful", res.message, 0);
      },
      err => {
        this.isProcessing = false;
        //  this.dialogRef.close(false);
        if (err.code == 400) {
          return this.coreService.notify("Unsuccessful", err.message, 0);
        }
        this.coreService.notify("Unsuccessful", "Error while submitting data.", 0)
      }
    )
  }







  onAddDepartment() {
    let reqBody;
    if (this.displayType == 'addDepartment') {
      reqBody = { "title": this.departmentTitle, "parentNodeID": this.userData.nodeID }
    } else {
      reqBody = { "title": this.departmentTitle, "parentNodeID": this.userData.parentNodeID, "nodeID": this.userData.nodeID }
    }

    this.isProcessing = true;
    this.companyService.addDepartment(reqBody).subscribe(
      res => {
        this.isProcessing = false;
        this.dialogRef.close('reload');
        if (res.code == 200) {
          this.coreService.notify("Successful", res.message, 1);
        }
        else return this.coreService.notify("Unsuccessful", res.message, 0);
      },
      err => {
        this.isProcessing = false;
        this.dialogRef.close(false);
        if (err.code == 400) {
          return this.coreService.notify("Unsuccessful", err.message, 0);
        }
        this.coreService.notify("Unsuccessful", "Error while submitting data.", 0)
      }
    )
  }

  inviteEmployee() {
    this.isInviteClicked = true;
    console.log('ccccccc ==> ', this.candidateID)
    let cid = !this.candidateID ? '' : 'cid/' + this.candidateID;
    // this.router.navigate(['/app/company-admin/employee-management/unassigned-position', this.userData.nodeID]);
    sessionStorage.setItem('inviteEmployeeData', JSON.stringify({ "orgNodeID": this.userData.nodeID, "positionID": this.userData.companyPositionID }));
    this.router.navigate([`/app/company-admin/employee-management/general-information/${cid}`]);
    this.dialogRef.close(true);    
  }

  getRoles() {
    this.displayType = 'role';
    this.isProcessing = true;
    this.companyService.getRoles().subscribe(
      res => {
        this.isProcessing = false;
        if (res.code == 200) {
          this.roles = res.data;
        }
        else return this.coreService.notify("Unsuccessful", res.message, 0);
      },
      err => {
        this.isProcessing = false;
        this.dialogRef.close(false);
        this.coreService.notify("Unsuccessful", "Error while getting data.", 0)
      }
    )
  }

  assignRole(role) {
    let reqBody;
    if (!this.userData.employeeUserID && !this.userData.contractorUserID) {
      this.dialogRef.close(false);
      return this.coreService.notify("Unsuccessful", "User is not a current employee", 0);
    }
    if(this.userData.contractorUserID){
      reqBody = {
        "employeeUserID": this.userData.contractorUserID,
        "nodeID": this.userData.nodeID
      }
    }
    if(this.userData.employeeUserID){
      reqBody = {
        "employeeUserID": this.userData.employeeUserID,
        "nodeID": this.userData.nodeID
      }
    }
    
    this.userData.role.roleID != role.companyRoleID ? reqBody["companyRoleID"] = role.companyRoleID : null;

    this.isProcessing = true;
    this.companyService.updateRole(reqBody).subscribe(
      res => {
        this.isProcessing = false;
        this.dialogRef.close('reload');
        if (res.code == 200) {
          this.coreService.notify("Successful", res.message, 1);
        }
        else return this.coreService.notify("Unsuccessful", res.message, 0);
      },
      err => {
        this.isProcessing = false;
        this.dialogRef.close(false);
        if (err.code == 400) {
          return this.coreService.notify("Unsuccessful", err.message, 0);
        }
        this.coreService.notify("Unsuccessful", "Error while submitting data.", 0)
      }
    )
  }

  removeRole() {
    return this.assignRole({ "companyRoleID": undefined });
  }

  getSubRole() {
    this.displayType = 'subRole';
    this.subRoleList = [{ title: "SHO", key: "isSho" }, { title: "OH&S", key: "isOhso" },
    { title: "FW", key: "isFw" }, { title: "FA", key: "isFao" }];
    // { title: "SMO", key: "isSmo" }
  }

  assignSubRole() {
    if (!this.userData.employeeUserID) {
      this.dialogRef.close(false);
      return this.coreService.notify("Unsuccessful", "User is not a current employee", 0);
    }

    this.userData.subRole["employeeUserID"] = this.userData.employeeUserID;
    this.userData.subRole["nodeID"] = this.userData.nodeID;

    this.isProcessing = true;
    this.companyService.updateSubRole(this.userData.subRole).subscribe(
      res => {
        this.isProcessing = false;
        this.dialogRef.close('reload');
        if (res.code == 200) {
          this.coreService.notify("Successful", res.message, 1);
        }
        else return this.coreService.notify("Unsuccessful", res.message, 0);
      },
      err => {
        this.isProcessing = false;
        this.dialogRef.close(false);
        if (err.code == 400) {
          return this.coreService.notify("Unsuccessful", err.message, 0);
        }
        this.coreService.notify("Unsuccessful", "Error while submitting data.", 0)
      }
    )
  }

  inviteSubCompany() {
    this.dialogRef.close(0);
    this.router.navigate([`/app/company-admin/sub-company/invite/${this.userData.nodeID}`]);
  }

  inviteSubContractor(){
    this.dialogRef.close(0);
    this.router.navigate([`/app/company-admin/sub-contractor/invite/${this.userData.nodeID}`]);
  }

  removeSubCompany() {
    this.isProcessing = true;
    this.companyService.removeSubCompany(this.userData.nodeID).subscribe(
      res => {
        this.isProcessing = false;
        this.dialogRef.close('reload');
        if (res.code == 200) {
          this.coreService.notify("Successful", res.message, 1);
        }
        else return this.coreService.notify("Unsuccessful", res.message, 0);
      },
      err => {
        this.isProcessing = false;
        this.dialogRef.close(false);
        if (err.code == 400) {
          return this.coreService.notify("Unsuccessful", err.message, 0);
        }
        this.coreService.notify("Unsuccessful", "Error while removing.", 0)
      }
    )

  }

  removeNode() {
    this.isProcessing = true;
    this.companyService.removeNode(this.userData.nodeID).subscribe(
      res => {
        this.isProcessing = false;
        this.dialogRef.close('reload');
        if (res.code == 200) {
          this.coreService.notify("Successful", res.message, 1);
        }
        else return this.coreService.notify("Unsuccessful", res.message, 0);
      },
      err => {
        this.isProcessing = false;
        this.dialogRef.close(false);
        if (err.code == 400) {
          return this.coreService.notify("Unsuccessful", err.message, 0);
        }
        this.coreService.notify("Unsuccessful", "Error while removing.", 0)
      }
    )
  }



  remove(type) {
    this.displayType = 'remove1';
    this.removeType = type;
  }

  confirmRemove() {
    switch (this.removeType) {
      case 'role':
        this.removeRole();
        break;

      case 'department':
        this.removeNode();
        break;

      case 'position':
        this.removeNode();
        break;

       case 'subCompany':
        this.removeSubCompany();
        break;

        case 'contractor':
        this.removeNode();
        break;

      default:
        break;
    }
  }

  is(type) {
    let condition = false;
    switch (type) {
      case 'isEmplyeeUnassigned':
        condition = this.userData.type == 'Position' && this.userData.positionType == 'unassigned';
        break;

        case 'isContractorUnassigned':
        condition = this.userData.type == 'Contractor';
        break;

      case 'assigned':
        condition = this.userData.type == 'Position' && this.userData.positionType !== 'unassigned';
        break;

      case 'invitedEmployee':
        condition = this.userData.type == 'Position' && this.userData.positionType == 'invitedEmployee';
        break;
      case 'isContractor':
        condition = this.userData.type =='Contractor'; 
        // && this.userData.positionType!='unassigned';
        break;
    
        case 'isAtDashboard':
        condition = ( ( this.userData.type == 'Position' || this.userData.type == 'Contractor' ) && this.userData.isInductionComplete == "Yes" ) ;
        break;
        
        case 'isAtInduction':
        condition = ( ( ( this.userData.type == 'Position' && this.userData.employeeUserID  ) ||  (this.userData.type == 'Contractor' && this.userData.contractorUserID ) )  && this.userData.isInductionComplete == "No" ) ;
        break;

        case 'isAtInvitation':
        condition = ( ( ( this.userData.type == 'Position' && this.userData.employeeInvitedID && !this.userData.employeeUserID ) ||  (this.userData.type == 'Contractor' && this.userData.contractorInvitedID && !this.userData.contractorUserID ) )  && this.userData.isInductionComplete == "No" ) ;
        break;

        case 'isBlankNode':
        condition = ( ( ( this.userData.type == 'Position' && !this.userData.employeeInvitedID && !this.userData.employeeUserID ) ||  (this.userData.type == 'Contractor' && !this.userData.contractorInvitedID && !this.userData.contractorUserID ) )  && this.userData.isInductionComplete == "No" ) ;
        break;


      case 'employee':
        condition = this.userData.type == 'Position' && this.userData.positionType == 'employee' && this.userData.isInductionComplete == "Yes";
        break;

      case 'Position':
        condition = this.userData.type == 'Position' ;
        break;

      case 'Department':
        condition = this.userData.type == 'Department';
        break;

      case 'Company':
        condition = this.userData.type == 'Company';
        break;

      case 'SubCompany':
        condition = this.userData.type == 'SubCompany';
        break;

      case 'InvitedSubCompany':
        condition = this.userData.type == 'SubCompany' && this.userData.title !== 'Unassigned';
        break;

      case 'SubContractor':
        condition = this.userData.type == 'Contractor' && this.userData.title !== 'Unassigned' && this.userData.positionType!=='Unassigned' && this.userData.isInductionComplete == "Yes";
        break;

      default:
        condition = false;
        break;
    }
    return condition;
  }
}
