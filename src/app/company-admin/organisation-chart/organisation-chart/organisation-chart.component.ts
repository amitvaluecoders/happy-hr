import { Component, OnInit, ElementRef, ViewChild, Renderer2 } from '@angular/core';
import { Router, ActivatedRoute } from "@angular/router";
import { MdSnackBar, MdDialog, MdDialogRef } from '@angular/material';
import { APPCONFIG } from '../../../config';
import { CoreService } from "../../../shared/services/core.service";
import { OrganisationChartService } from "../organisation-chart.service";
import { OrganizationChartPopupComponent } from "../organization-chart-popup/organization-chart-popup.component";
// import { TreeModel, NodeEvent, Ng2TreeSettings, RenamableNode } from 'ng2-tree';

@Component({
  selector: 'app-organisation-chart',
  templateUrl: './organisation-chart.component.html',
  styleUrls: ['./organisation-chart.component.scss']
})
export class OrganisationChartComponent implements OnInit {

  public actionEvntId = {}
  public isProcessing = false;
  public contentWith;
  count;
  public permission:any;
  isProfileEditPermission = true;
  noResult = true;
  candidateID = '';

  public chart = { "companyID": "", "nodeID": "", "title": "", "type": "", "subNodes": [], "logo": "" };

  @ViewChild('root') root: ElementRef;
  @ViewChild('node') node: ElementRef;
  constructor(public dialog: MdDialog, public renderer: Renderer2, public coreService: CoreService,
    public companyService: OrganisationChartService, public router: Router, public route: ActivatedRoute) { }

  ngOnInit() {
    this.setHorizontalScroll();
    APPCONFIG.heading = "Organisation chart";
    if (this.route.snapshot.params['id']) {
      this.candidateID = this.route.snapshot.params['id'];
    }
    this.getOrganizationChart();
    this.isProfileProfileEditPermission();  
  }

  setHorizontalScroll(){
    function scrollHorizontally(e) {
        e = window.event || e;
        var delta = Math.max(-1, Math.min(1, (e.wheelDelta || -e.detail)));
        document.getElementById('tree-container-box').scrollLeft -= (delta*90); // Multiplied by 40
        e.preventDefault();
    }
    if (document.getElementById('tree-container-box').addEventListener) {
        // IE9, Chrome, Safari, Opera
        document.getElementById('tree-container-box').addEventListener("mousewheel", scrollHorizontally, false);
        // Firefox
        document.getElementById('tree-container-box').addEventListener("DOMMouseScroll", scrollHorizontally, false);
    }
    //  else {
    //     // IE 6/7/8
    //     document.getElementById('yourDiv').attachEvent("onmousewheel", scrollHorizontally);
    // }

  }

  isProfileProfileEditPermission(){
      this.permission = this.coreService.permission;
      if(this.permission){
        this.permission.filter(item=>{
          if(item.module == "employee") this.isProfileEditPermission = item.permission.edit;
        })
      }
  }

  getOrganizationChart() {
    this.root.nativeElement.querySelector('.tree').innerHTML = '';
    this.count = 1;
    this.isProcessing = true;
    this.companyService.getOrganizationChart().subscribe(
      res => {
        this.isProcessing = false;
        if (res.code == 200) {
          this.noResult = false;
          this.chart = res.data;
          this.chart['count'] = this.count;

          if (this.chart.subNodes && this.chart.subNodes.length) {
            for (let i = 0; i < this.chart.subNodes.length; i++) {
              this.chart.subNodes[i]['parentNodeID'] = this.chart.nodeID;
              this.addNode(this.chart.subNodes[i], this.root.nativeElement)
            }
            console.log('chart ===>> ', this.chart);
          }
        }
        else return this.coreService.notify("Unsuccessful", res.message, 0);
      },
      err => {
        this.isProcessing = false;
        if (err.code == 400) {
          return this.coreService.notify("Unsuccessful", err.message, 0);
        }
        this.coreService.notify("Unsuccessful", "Error while getting data.", 0)
      }
    )
  }

  addNode(node, parent) {
    let child = this.node.nativeElement.cloneNode(true);
    this.renderer.setStyle(child, "display", "");
    // this.renderer.setAttribute(child.querySelector('a i'), 'id', node.id);
    this.renderer.listen(child.querySelector('.action'), 'click', this.onClick.bind(this, node));
    this.renderer.listen(child.querySelector('.action-btn'), 'click', this.onPopUp.bind(this, node));
    node['count'] = ++this.count;
    child.querySelector('.positionTabg').innerHTML = node['count'];
    if(node.role && node.role.title){
     let tempRole='';
      switch(node.role.title){
        case 'Generic Manager':
         tempRole = 'M';
          break;
        case 'Finance': 
          tempRole = 'Fin';
          break;
        case 'Operations':
          tempRole = 'OPs';
          break;
        case 'CEO': 
          tempRole = 'CEO';
          break;
        case 'HR Manager': 
        tempRole = 'HR';
        break;
        default:
        tempRole = '';
      }
      child.querySelector('.Node_position').innerHTML = ( node.role && node.role.title)? tempRole:'';
      console.log("role",tempRole);
    }
     

    if (node.type == "SubCompany" || node.type == "Company" || node.type == "Department") {
      this.renderer.addClass(child.querySelector('figure'), 'unsignned');
      child.querySelector('span').innerHTML = node.title;
      child.querySelector('figcaption small').innerHTML = node.type;
      this.renderer.setStyle(child.querySelector('img'), "display", "none");
      this.renderer.setStyle(child.querySelector('.designation'), "display", "none");
      this.renderer.setStyle(child.querySelector('.profile-btn'), "display", "none");

      if (node.type == "SubCompany" && node.title !== 'Unassigned') {
        this.renderer.setStyle(child.querySelector('.action'), "display", "");
        if (node.status == 'Complete') this.renderer.setStyle(child.querySelector('.starBtn'), "display", "none");
        else this.renderer.setStyle(child.querySelector('.starBtn'), "display", "");
        this.renderer.setAttribute(child.querySelector('.starBtn'), 'title', 'Sub company invited');
      }
    }
    else if (node.type == "Contractor") {
      this.renderer.setStyle(child.querySelector('span'), "display", "none");
      if (!node.contractorUserID && node.contractorInvitedID){
        this.renderer.setStyle(child.querySelector('.starBtn'), "display", "");
        this.renderer.setStyle(child.querySelector('.starBtn'), "display", (node.showSmiley=="Yes")?"":"none");
        this.renderer.setAttribute(child.querySelector('.starBtn'), 'title', 'Subcontractor invited');
      } 
      else if (node.contractorUserID && node.isInductionComplete == 'No'){
        this.renderer.setStyle(child.querySelector('.starBtnInduction'), "display", "");
        this.renderer.setStyle(child.querySelector('.starBtnInduction'), "display", (node.showSmiley=="Yes")?"":"none");
        this.renderer.setAttribute(child.querySelector('.starBtnInduction'), 'title', 'Incomplete induction');
      } 
      this.renderer.setStyle(child.querySelector('.profile-btn'), "display", "none"); 
      this.renderer.setStyle(child.querySelector('.designation'), "display", "none");
      this.renderer.setStyle(child.querySelector('.nodeTypeElement'), "background-color", "red");
      child.querySelector('figcaption small').innerHTML =node.contractor? node.contractor.name:'Sub contractor';
      child.querySelector('img').src = (node.contractor && node.contractor.imageUrl) ? node.contractor.imageUrl : 'assets/images/color-grey.png';  
      
      
    }

    else if (node.type == "Position") {
      this.renderer.setStyle(child.querySelector('span'), "display", "none");
      this.renderer.listen(child.querySelector('.profile-btn'), 'click', this.redirectUser.bind(this, node));
      // Employee
      if (node.subRole) {
            this.renderer.setStyle(child.querySelector('.labelsBtn'), "display", "");
          (node.subRole['isFao']) ? child.querySelector('.labelsBtn').appendChild(this.getLabel('FA')) : false;
          (node.subRole['isFw']) ? child.querySelector('.labelsBtn').appendChild(this.getLabel('FW')) : false;
          (node.subRole['isOhso']) ? child.querySelector('.labelsBtn').appendChild(this.getLabel('OH&S')) : false;
          (node.subRole['isSho']) ? child.querySelector('.labelsBtn').appendChild(this.getLabel('SHO')) : false;
        }
      if (node.employeeUserID && node.employee instanceof Object) {
        node['positionType'] = 'employee';
        if (node.isInductionComplete == 'No') this.renderer.setStyle(child.querySelector('.starBtnInduction'), "display", "");
        child.querySelector('img').src = (node.employee && node.employee.imageUrl) ? node.employee.imageUrl : 'assets/images/color-yellow.png';
        child.querySelector('figcaption small').innerHTML = (node.employee && node.employee.name) ? node.employee.name : 'User';
        child.querySelector('.designation small').innerHTML = (node.employee && node.employee.position) ? node.employee.position : 'Not specified';
        this.renderer.setAttribute(child.querySelector('.starBtnInduction'), 'title', 'Incomplete induction');
        // child.querySelector('.designation small').innerHTML = node.title;
      }
      // Invited Employee
      else if (node.employeeInvitedID && node.employeeInvited instanceof Object) {
        node['positionType'] = 'invitedEmployee';
        child.querySelector('img').src = (node.employeeInvited && node.employeeInvited.imageUrl) ? node.employeeInvited.imageUrl : 'assets/images/color-yellow.png';
        child.querySelector('figcaption small').innerHTML = (node.employeeInvited && node.employeeInvited.name) ? node.employeeInvited.name : 'User';
        child.querySelector('.designation small').innerHTML = (node.employeeInvited && node.employeeInvited.position) ? node.employeeInvited.position : 'Not specified';
        // child.querySelector('.designation small').innerHTML = node.title;
        this.renderer.setStyle(child.querySelector('.starBtn'), "display", "");
        this.renderer.setAttribute(child.querySelector('.starBtn'), 'title', 'Employee invited');
      }
      // Unassigned position
      else {
        node['positionType'] = 'unassigned';
        this.renderer.addClass(child.querySelector('figure'), 'unsignned');
        this.renderer.setStyle(child.querySelector('span'), "display", "");
        child.querySelector('span').innerHTML = 'Unassigned position';
        child.querySelector('.designation small').innerHTML = node.title;
        this.renderer.setStyle(child.querySelector('img'), "display", "none");
        this.renderer.setStyle(child.querySelector('figcaption'), "display", "none");
        this.renderer.setStyle(child.querySelector('.profile-btn'), "display", "none");
      }
      if(!this.isProfileEditPermission) this.renderer.setStyle(child.querySelector('.profile-btn'), "display", "none");
    }
    parent.querySelector('.tree').appendChild(child);

    if (node.subNodes && node.subNodes.length) {
      this.renderer.addClass(child, 'hasChild');
      for (let i = 0; i < node.subNodes.length; i++) {
        node.subNodes[i]['parentNodeID'] = node.nodeID;
        this.addNode(node.subNodes[i], child);
      }
    }
  }

  getLabel(name) {
    let label = this.renderer.createElement('label');
    this.renderer.addClass(label, 'lblInfo');
    label.innerHTML = name;
    return label;
  }

  onClick(node, event) {
    console.log(event, '====action====', node);
    let actionDiv = event.target.parentNode.nextElementSibling;
    if (actionDiv.style.display == "none") {
      $('.actionDiv').hide();
      this.renderer.setStyle(actionDiv, "display", "");
    } else {
      this.renderer.setStyle(actionDiv, "display", "none");
    }
  }

  onPopUp(node, event) {
    console.log(event, '====popup====', node)
    let dialogRef = this.dialog.open(OrganizationChartPopupComponent);
    dialogRef.componentInstance.userData = this.coreService.copyObject(dialogRef.componentInstance.userData, node);
    dialogRef.componentInstance.chart = this.chart;
    dialogRef.componentInstance.candidateID = this.candidateID;
    dialogRef.afterClosed().subscribe(result => {
      $('.actionDiv').hide();
      if (result) {
        if (result == 'reload') return this.getOrganizationChart();
      }
    });
  }

  redirectUser(node, event) {
    if (node.type == 'Position' && node.positionType != 'unassigned') {
      let id = node.employeeUserID ? node.employeeUserID : node.employeeInvitedID;
      if (node.hasOwnProperty('employeeUserID')) {
        window.open(`/#/app/company-admin/employee-management/profile/edit/${id}`);
      } else {
        window.open(`/#/app/company-admin/employee-management/general-information/${id}`);
      }
    }
  }
}
