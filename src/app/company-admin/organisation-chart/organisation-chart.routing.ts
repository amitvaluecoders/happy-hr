import {  ModuleWithProviders  } from '@angular/core';
import {  RouterModule,Routes  } from '@angular/router';
import { OrganisationChartComponent } from './organisation-chart/organisation-chart.component'

export const OrganisationChartPagesRoutes: Routes = [
    {
        path: '',
        canActivate:[],
        children: [
            //{ path: '', component: CompanyDashboardComponent},
            {path:'',component:OrganisationChartComponent},
            {path:'cid/:id',component:OrganisationChartComponent}
            
        ]
    }
];

export const OrganisationChartRoutingModule = RouterModule.forChild(OrganisationChartPagesRoutes);
