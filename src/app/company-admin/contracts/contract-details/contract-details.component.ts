import { Component, OnInit,ViewContainerRef } from '@angular/core';
import { ContractsService } from '../contracts.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { CoreService } from '../../../shared/services/core.service';
import { CompanyContract } from '../contract';
import { MdDialog,MdDialogRef} from '@angular/material';
import { PreviewDocumentComponent} from '../preview-document/preview-document.component';
import {APPCONFIG } from '../../../config';
import { ConfirmService } from '../../../shared/services/confirm.service';

@Component({
  selector: 'app-contract-details',
  templateUrl: './contract-details.component.html',
  styleUrls: ['./contract-details.component.scss']
})
export class ContractDetailsComponent implements OnInit {
  public count=0;
  public isSubCompany:any;
  public id="";
  public contract={
    contractSubjects:[]
  };
  public tgl={};
  public isHHRContract=true;
  public addMoreSubjectsBox=false;
  public isProcessing=false;
  public isSaveProcessing=false;
  constructor(public contractsService:ContractsService, public dialog:MdDialog,public confirmService:ConfirmService,
               public router:Router,public activatedRoute:ActivatedRoute,public viewContainerRef:ViewContainerRef,
               public coreService:CoreService) { }

  public newContractSubject={
    companyContractID:"",
    status:"",
    contractSubjects:[]
  }
   subUrl='';

  ngOnInit() {
    this.subUrl= this.contractsService.getSubUrl(this.activatedRoute.snapshot.params['ct']);
    this.isSubCompany=JSON.parse(localStorage.getItem('happyhr-userInfo')).isSubCompany;        
    APPCONFIG.heading="Contract details"
    let id=this.activatedRoute.snapshot.params['OWNid'];
    if(id){
    this.isHHRContract=false;
    this.id=this.activatedRoute.snapshot.params['OWNid'];
    this.newContractSubject.companyContractID=this.id;
    }
    else this.id=this.activatedRoute.snapshot.params['HHRid'];
    this.getContractDetails()
  }

  OnEdit(){
    console.log("contract id",this.activatedRoute.snapshot.params['OWNid'])
    this.router.navigate([`/app/company-admin/${this.subUrl}contract/update/${this.activatedRoute.snapshot.params['OWNid']}`]);
  }

  onVersions(){
    this.router.navigate([`/app/company-admin/${this.subUrl}contract/version/${this.activatedRoute.snapshot.params['OWNid']}`]);    
  }

  onPreview(){
      let dialogRef=this.dialog.open(PreviewDocumentComponent,{height:'100%',width:'60%'});
      dialogRef.componentInstance.contract=this.contract;
      dialogRef.afterClosed().subscribe(result => {
        if(result){        
        }
      });      
  }

  addNewSubjects(){
    this.addMoreSubjectsBox=true;
     this.newContractSubject.contractSubjects.push( {
        title:"",
        description:"",
        contractSubjectID:"",
         status:"Accepted"
      })
  }

  getContractDetails(){
    this.isProcessing=true;
    let reqBody={isHHRContract:this.isHHRContract,id:this.id};
       this.contractsService.getContractDetails(reqBody).subscribe(
          d => {
              if (d.code == "200") {
                this.contract=d.data;
                console.log(d.data)
               for(let c in d.data.subjects){
                 let i=0;
                  this.tgl[i]=false;
                  i++;
               }
                this.isProcessing=false;   
              }
              else{
              
                this.coreService.notify("Unsuccessful", "Error while getting contracts", 0); 
              } 
          },
          error => {
            console.log(error)
              this.coreService.notify("Unsuccessful", "Error while getting contracts", 0);
             
          }
         
      )
  }

 onAddMore(){
   this.count++;
   this.newContractSubject.contractSubjects.push( {
        title:"",
        description:"",
        contractSubjectID:"",
         status:"Accepted"
      })
 }

 onDeleteSubjects(i){
   this.confirmService.confirm(this.confirmService.tilte, this.confirmService.message, this.viewContainerRef)
      .subscribe(res => {
        let result = res;
        if (result) {
   this.newContractSubject.contractSubjects.splice(i,1);
      }})
    
 }

 onCancel(){
   this.newContractSubject.contractSubjects=[];
    this.addMoreSubjectsBox=false;
 }

  onSave(isvalid){
    if(isvalid){
      this.isSaveProcessing=true;
       
    this.contractsService.createcontractSubject(this.newContractSubject).subscribe(

          d => {
      console.log(d)
            
              if (d.code == "200") {
                this.isSaveProcessing=false;
               this.addMoreSubjectsBox=false;
              //  for(let k of this.newContractSubject.contractSubjects){
              //    this.contract.contractSubjects.push(k);
              //  }
              //  this.newContractSubject.contractSubjects=[];
               this.router.navigate([`/app/company-admin/${this.subUrl}contract`]);
                this.coreService.notify("Successful", d.message, 1); 
              }
              else{
              
                this.coreService.notify("Unsuccessful", "Error while getting contracts", 0); 
              } 
          },
          error => {
              this.coreService.notify("Unsuccessful", "Error while getting contracts", 0);
             
          }
         
      )
    }
 }
}
