import { Component, OnInit,ViewContainerRef } from '@angular/core';
import { MdDialog,MdDialogRef} from '@angular/material';
import { ContractsService } from '../contracts.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { CoreService } from '../../../shared/services/core.service';
import {CompanyContract} from '../contract';
import { PreviewDocumentComponent} from '../preview-document/preview-document.component';
import {APPCONFIG } from '../../../config';
import { ConfirmService } from '../../../shared/services/confirm.service';

@Component({
  selector: 'app-create-contract',
  templateUrl: './create-contract.component.html',
  styleUrls: ['./create-contract.component.scss']
})
export class CreateContractComponent implements OnInit {
  public isAddEdit="Create";
  public isContractDetailProcessing=false;
  public isAddEditProcessing=false;
  public buttonClicked=false;
  public contractTitle="";
  public isProcessing=false;

// drop down options for contract type
public dropdropdownListContractTypes=[
]

// drop down Setting for contract type
public dropdownSettingsContractTypes={
  singleSelection: true,
  text: "Select contract type"
}
public selectedItemsContractTypes=[];
public id="";
public contract={
  companyContractID:"",
  contractID:"",
  contractTypeID:"",
  contractTitle:"",
  companyID:"",
  description:"",
  version:"",
  title:"",
  source:"",
  status:"Active",
  contractSubjects:[
    {
        title:"",
        description:"",
        contractSubjectID:"",
         status:"Accepted"
    }
  ]
}
public ddl=true;
 subUrl='';
  constructor( public dialog:MdDialog,public contractsService:ContractsService,public confirmService:ConfirmService,
               public router:Router,public activatedRoute:ActivatedRoute,public viewContainerRef:ViewContainerRef,
               public coreService:CoreService) { }

  ngOnInit() {
    this.subUrl= this.contractsService.getSubUrl(this.activatedRoute.snapshot.params['ct']);
    APPCONFIG.heading="Create contract";
    this.id=this.activatedRoute.snapshot.params['id'];
    this.contract.source= this.activatedRoute.snapshot.params['catagory'];
    if(this.id){
      this.isContractDetailProcessing=true;
      this.isAddEdit="Update";
      APPCONFIG.heading="Update contract";
    }
    this.getAllContractTypes();
  }

  onChangeStatus(){
    this.contract.status=(this.contract.status=="Active")?"Inactive":"Active"
  }

  getAllContractTypes(){
     this.isProcessing=true;
     this.contractsService.getContractType(this.activatedRoute.snapshot.params['catagory']).subscribe(
          d => {
              if (d.code == "200") {
                if(this.id) this.getContractDetails();
                this.isProcessing=false;
                this.dropdropdownListContractTypes = d.data.filter(function(item){
                  item['itemName']=item.title;
                  item['id']=item.contractTypeID;
                  return item
                });
              }
              else this.coreService.notify("Unsuccessful", d.message, 0);
          },
          error => this.coreService.notify("Unsuccessful", error.message, 0),
          () => { }
      )
  }

  getContractDetails(){
      this.isContractDetailProcessing=true;
      let reqBody={isHHRContract:false,id:this.activatedRoute.snapshot.params['id']};
       this.contractsService.getContractDetailsForEdit(reqBody).subscribe(
          d => {
              if (d.code == "200") {
                this.contract=d.data;
                console.log(this.contract)
                this.isContractDetailProcessing=false;   
                this.selectedItemsContractTypes=this.dropdropdownListContractTypes.filter(function(item){
                  if(item.id==d.data.contractTypeID) return item;
                })
              }
              else{  
                this.isContractDetailProcessing=false;   
                this.coreService.notify("Unsuccessful", d.message, 0); 
              } 
          },
          error => {
               this.isContractDetailProcessing=false;
              this.coreService.notify("Unsuccessful",error.message, 0);  
          }
      )
  }

  onAddMoreSubjects(){
    this.contract.contractSubjects.push({contractSubjectID:"",title:"",status:"Accepted",description:""})
  }

OnDeleteSubject(i){
  this.confirmService.confirm(this.confirmService.tilte, this.confirmService.message, this.viewContainerRef)
      .subscribe(res => {
        let result = res;
        if (result) {
   this.contract.contractSubjects.splice(i,1);
      }})
}

 onPreview(){
      let dialogRef=this.dialog.open(PreviewDocumentComponent,{height:'90%',width:'60%'});
      dialogRef.componentInstance.contract=this.contract;
      dialogRef.afterClosed().subscribe(result => {
        if(result){        
        }
      });      
  }


  createContract(isvalid){
  if(isvalid){
    this.isAddEditProcessing=true;    
    if(!this.subUrl)this.contract.contractTypeID=this.contract['contractTypeID'] || this.selectedItemsContractTypes[0].id;
    else this.contract.contractTypeID ='6';
     this.contractsService.addEditCompanyContracts(this.contract).subscribe(
          d => {
              if (d.code == "200") {
                this.coreService.notify("Successful", d.message, 1);
                this.router.navigate([`/app/company-admin/${this.subUrl}contract`]);
                this.isAddEditProcessing=false;
              }
              else{
                this.isAddEditProcessing=false;
                this.coreService.notify("Unsuccessful",d.message, 0); 
              } 
          },
          error => {
              this.coreService.notify("Unsuccessful", error.message, 0);
              this.isAddEditProcessing=false;
          }
         
      )
   
  }
  }

}



// @Component({
//     selector: 'create-contract-dialog',
//     template: `<h1 md-dialog-title>Create Contract</h1>
//         <div md-dialog-content style="padding-bottom: 20px;"> 
                   
//         </div>
//         <div md-dialog-actions>
//             <button md-raised-button color="primary" class="btn btn-primary" (click)="onSubmit();false">Save Document</button>
//         </div>`,
// })
// export class CreateContractDialog implements OnInit {

//     constructor(public dialogRef: MdDialogRef<CreateContractDialog>) {}
    
//     ngOnInit(){
//     }

//     onSubmit(){ 

//         }
//     }
