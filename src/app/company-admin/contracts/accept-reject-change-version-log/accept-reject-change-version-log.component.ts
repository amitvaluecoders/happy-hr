import { Component, OnInit } from '@angular/core';
import { ContractsService } from '../contracts.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { CoreService } from '../../../shared/services/core.service';
import {CompanyContract} from '../contract';
import { MdSnackBar, MdDialog, MdDialogRef } from '@angular/material';


@Component({
  selector: 'app-accept-reject-change-version-log',
  templateUrl: './accept-reject-change-version-log.component.html',
  styleUrls: ['./accept-reject-change-version-log.component.scss']
})
export class AcceptRejectChangeVersionLogComponent implements OnInit {
  public contractDetails:any;
  public isProcessing;
  public contract = {
    new: {
      description: ""
    }
  };

  constructor(public contractsService:ContractsService,
  public dialogRef: MdDialogRef<AcceptRejectChangeVersionLogComponent>,
               public router:Router,public activatedRoute:ActivatedRoute,
               public coreService:CoreService) { }

  ngOnInit() { 
    this.compareSubjectsDetails(); 
    this.contract.new.description=this.contractDetails.subject.description;
  }
  

  compareSubjectsDetails(){
    
      this.isProcessing=true;
         this.contractsService.compareSubjects(this.contractDetails.subject.contractSubjectID).subscribe(
          d => {
              if (d.code == "200") {
                if(d.data)this.contract=d.data;
                this.isProcessing=false;
              }
              else{
                //  this.dialogRef.close(0);
                this.isProcessing=false;
                this.coreService.notify("Unsuccessful", d.message, 0); 
              } 
          },
          error => {
            this.isProcessing=false;
            this.coreService.notify("Unsuccessful", error.message, 0)
            //  this.dialogRef.close(this.contract.new.description);
          }
         
      )
  }

  onAccept(isValid){
    if(isValid){
      this.dialogRef.close(this.contract.new.description);
    }
  }

}
