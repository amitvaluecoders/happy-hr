import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule} from '@angular/material';
 import { ContractsRoutingModule } from './contracts.routing';
import { ContractsService } from './contracts.service';
import {FormsModule} from '@angular/forms'
import { ContractListingComponent } from './contract-listing/contract-listing.component';
import { ContractDetailsComponent } from './contract-details/contract-details.component';
import { ContractVersioningComponent } from './contract-versioning/contract-versioning.component';
import { CreateContractComponent } from './create-contract/create-contract.component';
import { PreviewDocumentComponent } from './preview-document/preview-document.component';
import { AcceptRejectChangeComponent } from './accept-reject-change/accept-reject-change.component';
import { AcceptRejectChangeVersionLogComponent } from './accept-reject-change-version-log/accept-reject-change-version-log.component';
import { QuillModule } from 'ngx-quill';
import { AngularMultiSelectModule } from 'angular2-multiselect-checkbox-dropdown/angular2-multiselect-dropdown';
import { SharedModule } from '../../shared/shared.module';
import {DataTableModule} from 'angular2-datatable';
import { ContractsGuardService } from './contracts-guard.service';


@NgModule({
  imports: [
    SharedModule,
    DataTableModule,
    AngularMultiSelectModule,
    CommonModule,
    FormsModule,
    QuillModule,
    ContractsRoutingModule,
    MaterialModule
  ],
  declarations: [ContractListingComponent, ContractDetailsComponent, ContractVersioningComponent, CreateContractComponent, PreviewDocumentComponent, AcceptRejectChangeComponent, AcceptRejectChangeVersionLogComponent],
  providers:[ContractsService,ContractsGuardService],
  entryComponents:[AcceptRejectChangeVersionLogComponent,PreviewDocumentComponent]
})
export class CompanyContractsModule { }
