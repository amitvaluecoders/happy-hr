import {  ModuleWithProviders  } from '@angular/core';
import {  RouterModule,Routes  } from '@angular/router';
import{ContractListingComponent } from './contract-listing/contract-listing.component';
import{ContractDetailsComponent} from './contract-details/contract-details.component';
import {ContractVersioningComponent} from './contract-versioning/contract-versioning.component';
import {CreateContractComponent} from './create-contract/create-contract.component';
import { PreviewDocumentComponent } from "./preview-document/preview-document.component"
import {AcceptRejectChangeComponent} from './accept-reject-change/accept-reject-change.component';
export const ContractsPagesRoutes: Routes = [
    {
        path: '',
        canActivate:[],
        children: [
           
            { path: '', component: ContractListingComponent},
            { path: 'contract-list', component: ContractListingComponent},
            { path: 'contract-details/hhr/:HHRid', component: ContractDetailsComponent},
            { path: 'contract-details/:catagory/:OWNid', component: ContractDetailsComponent},
            { path: 'version/:id', component: ContractVersioningComponent},
            { path: 'sa-version/:ContractId', component: ContractVersioningComponent},
            { path:'create-new/:catagory',component:CreateContractComponent},
            { path:'update/:id',component:CreateContractComponent},
            { path:'change-cotract/:id',component:AcceptRejectChangeComponent},
            { path:'preview/:id',component:PreviewDocumentComponent},
            
        ]
    }
];

export const ContractsRoutingModule = RouterModule.forChild(ContractsPagesRoutes);
