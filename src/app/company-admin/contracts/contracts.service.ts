import { Injectable } from '@angular/core';
import { RestfulLaravelService} from '../../shared/services/restful-laravel.service';
import { ActivatedRoute} from '@angular/router';

@Injectable()
export class ContractsService {

  constructor(public activatedRouter:ActivatedRoute,public restfulWebService:RestfulLaravelService) {
     
   }

   getSubUrl(ct){
      let subUrl = (ct && ct =='sc')?'sc/':'';
     return subUrl;
   }

  getContractType(catagory):any{
   return this.restfulWebService.get(`get-remaining-contract-type/${catagory}`)
  }

  addEditCompanyContracts(reqBody):any{
    //  return this.restfulWebService.get(`admin/get-trophy-detail/3`);
   return this.restfulWebService.post('add-update-contracts',reqBody)
   
  }

  getContractList(reqBody):any{
     return this.restfulWebService.get(`list-contract/${encodeURIComponent(reqBody)}`)
  }

  getContractListFilerData(ct):any{
    let temp = this.getSubUrl(ct);
    let url = !temp?"list-contract-filter-data":"list-contract-filter-data/contractor"; 
     return this.restfulWebService.get(url);
  }


  createcontractSubject(reqBody):any{
  return this.restfulWebService.post('create-contract-subject',reqBody)
  }

  getContractDetails(reqBody):any{
    if(reqBody.isHHRContract) return this.restfulWebService.get(`admin/get-contract-detail/${reqBody.id}`)
    else  return this.restfulWebService.get(`get-contract-detail/${reqBody.id}`)
  }

  getContractDetailsForEdit(reqBody):any{
    if(reqBody.isHHRContract) return this.restfulWebService.get(`admin/get-contract-detail-with-var/${reqBody.id}`)
    else  return this.restfulWebService.get(`get-contract-detail-with-var/${reqBody.id}`)
  }
  
getContractVersionList(contractType,reqId){
  let url='';
  if(contractType=='ca') url=`get-contract-version-detail/${reqId}`;
  if(contractType=='sa') url=`get-sa-contract-version-detail/${reqId}`;
     return this.restfulWebService.get(url)
}

restoreContractVersion(reqBody){
     return this.restfulWebService.get(`restore-contract-version/${reqBody}`)
}

compareSubjects(reqBody){
     return this.restfulWebService.get(`compare-contract-subject/${reqBody}`)
}

deleteOwnContract(reqId){
  return this.restfulWebService.delete(`delete-contract/${reqId}`);
}

}
