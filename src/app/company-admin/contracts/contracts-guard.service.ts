import { Injectable } from '@angular/core';
import {CanActivate,Router,RouterStateSnapshot,ActivatedRouteSnapshot} from "@angular/router";
import {CoreService} from '../../shared/services/core.service';
import {Observable} from 'rxjs/Rx'; 
import 'rxjs/add/operator/map';

@Injectable()
export class ContractsGuardService {

  public module = "contracts";
  
        constructor(private _core: CoreService, private router: Router){ }
  
        canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {  
            let url: string;
            if(route['_routeConfig']['path'])
            url= route['_routeConfig']['path']
            else url = state.url;
            if(this.checkHavePermission(url)){
              return true;
            }else{
              this.router.navigate(['/extra/unauthorized']);
              return false;
            }
        }
  
        private checkHavePermission(path) {
            switch (path) {
                case "/app/company-admin/contracts":
                    return this._core.getModulePermission(this.module, 'view');
                case "contract-list":
                    return this._core.getModulePermission(this.module, 'view');
                case "contract-details/hhr/:HHRid":
                    return this._core.getModulePermission(this.module, 'view');
                case "contract-details/own/:OWNid":
                    return this._core.getModulePermission(this.module, 'view');
                case "version/:id":
                    return this._core.getModulePermission(this.module, 'view');
                case "sa-version/:ContractId":
                    return this._core.getModulePermission(this.module, 'view');
                case "create-new":
                    return this._core.getModulePermission(this.module, 'add');
                case "update/:id":
                    return this._core.getModulePermission(this.module, 'edit');
                case "change-cotract/:id":
                    return this._core.getModulePermission(this.module, 'edit');
                case "preview/:id":
                    return this._core.getModulePermission(this.module, 'view');
                default:
                    return false;
            }
        }
}


