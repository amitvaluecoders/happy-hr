export interface CompanyContract{

  companyContractId?:String,
  contractTypeId?:String,
  contractTitle?:String,
  contractDesc?:String,
  companyID?:String,
  description?:String,
  policy?:String,
  version?:String,
  title?:String,
  status?:String,
   subjects:[
    {contractSubjectID?:String,title?:String,status?:String,description?:String}
  ]


}