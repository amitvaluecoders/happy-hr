import { Component, OnInit } from '@angular/core';
import { ContractsService } from '../contracts.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { CoreService } from '../../../shared/services/core.service';
import { CompanyContract } from '../contract';
import { MdDialog,MdDialogRef} from '@angular/material';
import {AcceptRejectChangeVersionLogComponent } from '../accept-reject-change-version-log/accept-reject-change-version-log.component';
import {APPCONFIG } from '../../../config';
import { ResponseNoteComponent } from '../../../shared/components/response-note/response-note.component'

@Component({
  selector: 'app-accept-reject-change',
  templateUrl: './accept-reject-change.component.html',
  styleUrls: ['./accept-reject-change.component.scss']
})
export class AcceptRejectChangeComponent implements OnInit {
public contract={};
public isProcessing;
public isSubCompany:any;
public isCreateProcessing;
public isSubjectNew=true;
public tgl={};
subUrl='';
  constructor( public dialog:MdDialog,public contractsService:ContractsService,
               public router:Router,public activatedRoute:ActivatedRoute,
               public coreService:CoreService) { }

  ngOnInit() {
    this.subUrl= this.contractsService.getSubUrl(this.activatedRoute.snapshot.params['ct']);
    APPCONFIG.heading="Accept / Reject change"
    this.getContractDetails();
    this.isSubCompany=JSON.parse(localStorage.getItem('happyhr-userInfo')).isSubCompany;    
  }

  getContractDetails(){
  this.isProcessing=true;
  let reqBody={isHHRContract:true,id:this.activatedRoute.snapshot.params['id']};
      this.contractsService.getContractDetailsForEdit(reqBody).subscribe(
        d => {
           this.isProcessing=false;  
            if (d.code == "200") {
              d.data.contractSubjects.forEach(element => {
                element['status']='Accepted';
                element['isUpdated']=false;
                return true;
              });

              this.contract=d.data;
              this.contract['contractTypeID']=d.data.contracttype.contractTypeID;
              this.contract['companyContractID']='';
            } 
            else this.coreService.notify("Unsuccessful", "Error while getting contracts", 0) 
        },
        error => this.coreService.notify("Unsuccessful", "Error while getting contracts", 0)
    )
  }

  onEditBeforeAccept(subject){
    
      let dialogRef=this.dialog.open(AcceptRejectChangeVersionLogComponent,{height:'70%',width:'80%'});
      dialogRef.componentInstance.contractDetails={contract:this.contract,subject:subject};
      dialogRef.afterClosed().subscribe(result => {
        if(result){    
          this.isSubjectNew=false;    
            this.contract['contractSubjects'].filter(function(item){
              if(item.contractSubjectID==subject.contractSubjectID) {
                item.isUpdated=true;
                item.description=result;
              }
              return item;
            })
        }
      });   

  }

  onAceeptChangeCompletely(){
    this.isCreateProcessing=true; 
    this.contractsService.addEditCompanyContracts(this.contract).subscribe(
        d => {
            if (d.code == "200")  {
              this.coreService.notify("Successful", d.message, 1) 
              this.router.navigate([`/app/company-admin/${this.subUrl}contract`]);
            }
            else this.coreService.notify("Unsuccessful", d.message, 0) 
        },
        error => this.coreService.notify("Unsuccessful", error.message, 0)
    )
  }

  onRejectWithNote(){
       let tempResponse={
            url:"reject-contract",
            note:{
              contractID:this.contract['contractID'],
              responseNote:""
       }
      }
      console.log("ss",)
      let dialogRef=this.dialog.open(ResponseNoteComponent,{width:'600px'});
      dialogRef.componentInstance.noteDetails=tempResponse;
      dialogRef.afterClosed().subscribe(result => {
        if(result){    
         this.router.navigate([`/app/company-admin/${this.subUrl}contract`]);
        }
      });   
  }

onVersionList(){
  this.router.navigate([`/app/company-admin/${this.subUrl}contract/sa-version/${this.contract['contractID']}`]);
   
}
  

}
