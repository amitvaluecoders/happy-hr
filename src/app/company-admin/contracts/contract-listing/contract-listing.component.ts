import { Component, OnInit,ViewContainerRef } from '@angular/core';
import { ContractsService } from '../contracts.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { CoreService } from '../../../shared/services/core.service';
import {CompanyContract} from '../contract';
import {APPCONFIG } from '../../../config';
import { ConfirmService } from '../../../shared/services/confirm.service';



@Component({
  selector: 'app-contract-listing',
  templateUrl: './contract-listing.component.html',
  styleUrls: ['./contract-listing.component.scss']
})
export class ContractListingComponent implements OnInit {
 public collapse={};
 public isSubCompany:any;
  public toggleFilter = true;
  public isOwnContractProcessing=false;
  public hrToggle = false;
  public ownerToggle = false;
  public ownContracts=[];
  public ownContracts_2=[];
  public ownContracts_3=[];
  public unsubscriber:any;
  public ownContracts_4=[];  
  public counter =0;
  public contractLength = 5;
  public happyHRContracts=[];
  public isFilterDataProcessing=false;
  public versionSearch:'';
  public typeSearch:'';
  public serachKey:'';
  public tempReqBody={
    "search":"",
    "filter":{
      "status":{
        "Active":false,
        "Inactive":false
      },
      "versions":[],
      "lastUpdated":{
        "int7":false,
        "int30":false,
        "int90":false,
        "int180":false,
        "int365":false,
      },
      "types":[],
      "category":{
        Own:false,
        HHR:false
      }
    }
  }
  public reqBody={
    "search":"",
    "filter":{
      "status":{
        "Active":false,
        "Inactive":false
      },
      "versions":{
      },
      "lastUpdated":{
        "int7":false,
        "int30":false,
        "int90":false,
        "int180":false,
        "int365":false,
      },
      "types":{
      },
      "category":{
        Own:true,
        HHR:true,
        Own_2:true,
        Own_3:true,
        Own_4:true
      }
    }
  }

  contract={
    Own:'Own',
    Own_2:'Own_2',
    Own_3:'Own_3',
    Own_4:'Own_4'
  }
  
 subUrl='';
  constructor(public contractsService:ContractsService,
              public confirmService:ConfirmService,
              public viewContainerRef:ViewContainerRef,
               public router:Router,public activatedRoute:ActivatedRoute,
               public coreService:CoreService) { }

  ngOnInit() {
    this.subUrl= this.contractsService.getSubUrl(this.activatedRoute.snapshot.params['ct']);
    APPCONFIG.heading="Contracts";
  this.isSubCompany=JSON.parse(localStorage.getItem('happyhr-userInfo')).isSubCompany;  
    this.getCOntractsLsitFilterData();
  }

// method to apply filters 
  onApplyfilters(){
      this.reqBody.filter.category=JSON.parse(JSON.stringify(this.tempReqBody.filter.category));
      if(!this.tempReqBody.filter.category.HHR && !this.tempReqBody.filter.category.Own){
        this.reqBody.filter.category.HHR=true;
        this.reqBody.filter.category.Own=true;
        this.reqBody.filter.category.Own_2= true;
        this.reqBody.filter.category.Own_3= true;
        this.reqBody.filter.category.Own_4= true;                        
      }
      this.reqBody.filter.lastUpdated = this.tempReqBody.filter.lastUpdated;
      this.reqBody.filter.status =  this.tempReqBody.filter.status;
      this.reqBody.filter.types={};
      this.reqBody.filter.versions={};
      this.tempReqBody.filter.types.forEach(item =>{
        if(item.isChecked) this.reqBody.filter.types[item.type]=true;
      })
      this.tempReqBody.filter.versions.forEach(item =>{
        if(item.isChecked) this.reqBody.filter.versions[item.version]=true;
      })  
     this.getCOntractList();
  }


  onSearch(){
    this.reqBody.search=encodeURIComponent(this.tempReqBody.search);
    this.getCOntractList();
  }

// method to filter data details :- CONTRACT TYPES and VERSIONS
  getCOntractsLsitFilterData(){
    this.isFilterDataProcessing = true;
      this.isOwnContractProcessing=true;    
      this.contractsService.getContractListFilerData(this.activatedRoute.snapshot.params['ct']).subscribe(
          d => {
             this.isFilterDataProcessing = false;
              if (d.code == "200") {
                this.tempReqBody.filter.types=d.data.type;
                this.tempReqBody.filter.versions=d.data.versions;
                this.getCOntractList();
              }
              else this.coreService.notify("Unsuccessful", d.message, 0);
          },
          error => {
            this.isFilterDataProcessing = false;
            this.coreService.notify("Unsuccessful", error.message, 0); 
          }      
      )
  }


// method to get listing OWN and HAPPY HR contracts.
  getCOntractList(){
      this.isOwnContractProcessing=true;
      if(this.subUrl)this.reqBody.filter.types['Contractor']=true;    
         this.contractsService.getContractList(JSON.stringify(this.reqBody)).subscribe(
          d => {
              if (d.code == "200") {
                this.ownContracts=d.data.ownContracts;
                this.ownContracts_2=d.data.own_2_Contracts;
                this.ownContracts_3=d.data.own_3_Contracts;
                this.ownContracts_4=d.data.own_4_Contracts;
                

                if(this.counter==0)this.contractLength = JSON.parse(JSON.stringify(d.data.ownContracts)).length; 
                
                this.happyHRContracts=d.data.hhrContracts;     
                this.isOwnContractProcessing=false;
                this.counter++;
              }
              else{
                this.isOwnContractProcessing=false;
                this.coreService.notify("Unsuccessful", "Error while getting contracts", 0); 
              } 
          },
          error => {
              this.coreService.notify("Unsuccessful", "Error while getting contracts", 0);
              this.isOwnContractProcessing=false;
          }      
      )
  }

  onContractDetails(contract){
    console.log("contract........",contract);
    let url="/app/company-admin/"+this.subUrl+"contract/contract-details/hhr/"+contract.contractID;
    if(contract.companyContractID) url="/app/company-admin/"+this.subUrl+"contract/contract-details/"+contract.category+"/"+contract.companyContractID;
    this.router.navigate([url]);
  }

  onAcceptReject(contractID){
    this.router.navigate([`/app/company-admin/${this.subUrl}contract/change-cotract/${contractID}`]);
  }

  onEditContract(contract){
     this.router.navigate([`/app/company-admin/${this.subUrl}contract/update/${contract.companyContractID}`]);
  }

//Delete own contracts
  onDeleteContract(contract){
  
    this.confirmService.confirm(this.confirmService.tilte, this.confirmService.message, this.viewContainerRef)
      .subscribe(res => {
        let result = res;
        if (result) {
                    console.log("deleted"); 
                this.unsubscriber=this.contractsService.deleteOwnContract(contract.companyContractID).subscribe(
                d => {
                    if (d.code == "200") {
                      this.coreService.notify("Successful", d.message, 1);
                      contract.isDeleteProcessing=false;
                      this.getCOntractList();
                      
                    }
                    else{
                      this.coreService.notify("Unsuccessful", d.message, 0);
                      contract.isDeleteProcessing=false;
                    } 
                  },
                error => {
                  contract.isDeleteProcessing=false;                  
                  this.coreService.notify("Unsuccessful", error.message, 0);
                }
                
            )
        }
        else contract.isDeleteProcessing=false;
  })
  }

}
