import { Component, OnInit } from '@angular/core';
import { ContractsService } from '../contracts.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { CoreService } from '../../../shared/services/core.service';
import { CompanyContract } from '../contract';
import { APPCONFIG } from '../../../config'


@Component({
  selector: 'app-contract-versioning',
  templateUrl: './contract-versioning.component.html',
  styleUrls: ['./contract-versioning.component.scss']
})
export class ContractVersioningComponent implements OnInit {
    
AppConfig: any;
public versionList=[];
public tabing=false;
public isSubCompany:any;
public isContractDetailProcessing=false;
public contract={};
public currentTab=1.0;
public isProcessing=false;
 subUrl='';
  constructor(public contractsService:ContractsService,
               public router:Router,public activatedRoute:ActivatedRoute,
               public coreService:CoreService) { }

  ngOnInit() {
    this.subUrl= this.contractsService.getSubUrl(this.activatedRoute.snapshot.params['ct']);
    this.isSubCompany=JSON.parse(localStorage.getItem('happyhr-userInfo')).isSubCompany;    
    this.activatedRoute.snapshot.params['id'];
    this.activatedRoute.snapshot.params['ContractId'] 
    APPCONFIG.heading="Contract versions"
    this.getVersionList();
  }
 
 getVersionList(){
   let contractType="";
   let reqId="";
    if( this.activatedRoute.snapshot.params['id']) {
      contractType="ca";
      reqId=this.activatedRoute.snapshot.params['id'];
    }
    if( this.activatedRoute.snapshot.params['ContractId']){
      contractType="sa";
      reqId=this.activatedRoute.snapshot.params['ContractId'];      
    } 
    
     this.isProcessing=true;       
     this.contractsService.getContractVersionList(contractType,reqId).subscribe(
          d => {
              this.isProcessing=false;
              if(d.code == "200"){
              for(let i of d.data){
                if(i.status=='Active'){
                  i['localActive']=true;
                  this.getVersionDetails(i.companyContractID); 
                } 
                else i['localActive']=false;
              }
                this.versionList=d.data;
                console.log(this.versionList);
              }   
              else this.coreService.notify("Unsuccessful",d.message, 0); 
          },
          error =>{
            this.coreService.notify("Unsuccessful", "Error while getting notes", 0);
            this.isProcessing=false
          } 
      )
 }

 getVersionDetails(companyContractID){
    this.isContractDetailProcessing=true;
      let reqBody={isHHRContract:false,id:companyContractID};
       this.contractsService.getContractDetails(reqBody).subscribe(
          d => {
              if (d.code == "200") {
                this.contract=d.data;
                this.isContractDetailProcessing=false;        
              }
              else this.coreService.notify("Unsuccessful", "Error while getting contracts", 0);    
          },
          error =>  this.coreService.notify("Unsuccessful", "Error while getting contracts", 0)
      )
 }

onVersionSelect(companyContractID){
  console.log(this.versionList)
    this.versionList.forEach(element => {
      if(element.companyContractID==companyContractID){
        element['localActive']=true;
        console.log("cccc",element.companyContractID)
        this.getVersionDetails(element.companyContractID);
      }
      else  element['localActive']=false;
    });
}

 onRestoreVersion(id){
    this.isProcessing=true;       
     this.contractsService.restoreContractVersion(id).subscribe(
          d => {
              this.isProcessing=false;
              if(d.code == "200") {
                this.coreService.notify("Successful",d.message, 1);  
                this.getVersionList();
              }
              else this.coreService.notify("Unsuccessful",d.message, 0); 
          },
          error =>{
            this.coreService.notify("Unsuccessful", "Error while getting notes", 0);
            this.isProcessing=false
          } 
      )
 }

}
