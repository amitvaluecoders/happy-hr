import {  ModuleWithProviders  } from '@angular/core';
import {  RouterModule,Routes  } from '@angular/router';

import { UserAccessControlService } from './user-access-control.service';
import { UserAccessControlComponent } from './user-access-control/user-access-control.component';
import { PermissionComponent } from './permission/permission.component';

export const UserAccessControlPagesRoutes: Routes = [
    {
        path: '',
        canActivate:[],
        children: [
           
            { path: '', component: UserAccessControlComponent},
            { path: 'permission/:id', component: PermissionComponent},
        ]
    }
];

export const UserAccessControlRoutingModule = RouterModule.forChild(UserAccessControlPagesRoutes);
