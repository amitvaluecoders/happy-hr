import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { APPCONFIG } from '../../../config';
import { MdSnackBar, MdDialog, MdDialogRef } from '@angular/material';
import { UserAccessControlService } from '../user-access-control.service';
import { CoreService } from '../../../shared/services/core.service';
import { ConfirmService } from '../../../shared/services/confirm.service';
import { Router, ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-permission',
  templateUrl: './permission.component.html',
  styleUrls: ['./permission.component.scss']
})
export class PermissionComponent implements OnInit {

  // roles = [];
  modules = [];
  id = '';
  isProcessing = false;
  // isUpdateProcessing = false;
  constructor(private userAccessService: UserAccessControlService, private coreService: CoreService, private router: Router, private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    APPCONFIG.heading = "Permission";
    this.id = this.activatedRoute.snapshot.params['id'];
    if (this.id) this.getAllModulesPermission();
    // this.getRoles();
  }

  // getRoles() {
  //   //this.isProcessing=true;
  //   this.userAccessService.getRoles().subscribe(
  //     data => {
  //       if (data.code == "200") {
  //         //  this.isProcessing=false;
  //         this.roles = data.data;
  //       }
  //     },
  //     error => {
  //       //this.isProcessing=false;this.coreService.notify("Unsuccessful",error.message,0)},
  //     }
  //   );
  // }

  getAllModulesPermission() {
    this.isProcessing = true;
    this.userAccessService.getAllModules(this.id).subscribe(
      data => {
        if (data.code == "200") {
          //  this.isProcessing=false;
          this.modules = data.data;
          this.isProcessing = false;
          console.log("copnay Module data", this.modules)
        } else {
          //this.isProcessing=false;
          //this.coreService.notify("Warning",data.message,2)
        }
      },
      error => {
        this.isProcessing = false;
        //this.isProcessing=false;this.coreService.notify("Unsuccessful",error.message,0)},
      },
      () => { }
    );
  }

  // updatePermission() {
  //   // this.isUpdateProcessing = true;
  //   let reqBody = {
  //     roleID: this.id,
  //     modules: this.modules
  //   };

  //   this.userAccessService.updatePermission(reqBody).subscribe(
  //     data => {
  //       // this.isUpdateProcessing = false;
  //       this.router.navigate(['/app/company-admin/user-access']);
  //       this.coreService.notify('Successful', data.message, 1)
  //     }, error => {
  //       // this.isUpdateProcessing = false;
  //       this.coreService.notify('Unsuccessful', error.message, 0)
  //     })
  // }

  onCancel() {
    this.router.navigate(['/app/company-admin/user-access']);
  }

}
