import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { APPCONFIG } from '../../../config';
import { MdSnackBar, MdDialog, MdDialogRef } from '@angular/material';
import { UserAccessControlService } from '../user-access-control.service';
import { CoreService } from '../../../shared/services/core.service';
import { ConfirmService } from '../../../shared/services/confirm.service';
import { Router, ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-user-access-control',
  templateUrl: './user-access-control.component.html',
  styleUrls: ['./user-access-control.component.scss']
})
export class UserAccessControlComponent implements OnInit {

  roles = [];
  isProcessing = false;
  permission;
  role:any;
  constructor(public dialog: MdDialog, private userAccessService: UserAccessControlService, private coreService: CoreService, public confirmService: ConfirmService, public viewContainerRef: ViewContainerRef) { }

  ngOnInit() {
    APPCONFIG.heading = "User access control";
    this.role = this.coreService.getRole();
    this.getRoles();
    this.permission = this.coreService.getNonRoutePermission();
  }

  getRoles() {
    this.isProcessing = true;
    this.userAccessService.getRoles().subscribe(
      data => {
        this.isProcessing = false;
        if (data.code == "200") {
          this.roles = data.data;
          console.log("roles", data.data)
        }
      },
      error => { this.isProcessing = false; this.coreService.notify("Unsuccessful", error.message, 0) }
    )
  }

  onPopup(role) {
    if (role.count == 0) return;
    let dialogRef = this.dialog.open(addNewRoleDialog, { width: '400px' });
    dialogRef.componentInstance.role = null;
    dialogRef.componentInstance.role = role;
    dialogRef.afterClosed().subscribe(r => {
      if (r) { }
    })

  }

  // role = {
  //   roleID: '',
  //   title: ''
  // };

  // onEdit(role) {
  //   this.role = role
  //   this.openDialogAddRole();
  // }

  // openDialogAddRole() {
  //   let dialogRef = this.dialog.open(addNewRoleDialog, {
  //     width: '400px'
  //   });
  //   if (this.role) dialogRef.componentInstance.role = this.role;
  //   dialogRef.afterClosed().subscribe(result => {
  //     if (result) {
  //       this.isProcessing = true;
  //       this.userAccessService.addEditRole(result).subscribe(
  //         data => {
  //           if (data.code == "200") {
  //             this.getRoles();
  //             this.coreService.notify("Successful", data.message, 1)
  //           } else {
  //             this.getRoles();
  //             //this.getCategoryTitles();
  //             this.coreService.notify("Warning", data.message, 2)
  //           }
  //         },
  //         error => this.coreService.notify("Unsuccessful", error.message, 0),
  //         () => { }
  //       )
  //     }
  //   });
  // }

  // onDelete(id) {
  //   this.confirmService.confirm(this.confirmService.tilte, this.confirmService.message, this.viewContainerRef)
  //     .subscribe(res => {
  //       let result = res;
  //       if (result) {
  //         this.isProcessing = true;
  //         this.userAccessService.deleteRole(id).subscribe(
  //           data => {
  //             if (data.code == "200") {
  //               this.getRoles();
  //               this.coreService.notify("Successful", data.message, 1)
  //             } else {
  //               //this.getCategoryTitles();
  //               this.isProcessing = false;
  //               this.coreService.notify("Warning", data.message, 2)
  //             }
  //           },
  //           error => {
  //           this.isProcessing = false;
  //             this.coreService.notify("Unsuccessful", error.message, 0)
  //           },
  //           () => { }
  //         )
  //       }
  //     })
  // }

}

// openDialogWithAResult
@Component({
  selector: 'add-new-role-dialog',
  template: `<h1 md-dialog-title> {{role.title}}</h1>
        <div md-dialog-content style="padding-bottom: 20px;">
          <div *ngFor="let user of role.userList" style="margin-bottom:7px;">
          <img [src]="user.imageUrl" style="width:50px;border-radius:50%;height:50px;">
            <span style="margin-left: 5px;">{{user.name}}</span>
          </div>
        </div>
        <div class="text-center">
            <button class="btn btn-primary" (click)="onCancel()">Close</button>
        </div>
        `,
})
export class addNewRoleDialog implements OnInit {
  // role = {
  //   count: '', title: '', userList: []
  // }
  public role:any;

  constructor(public dialogRef: MdDialogRef<addNewRoleDialog>, private userAccessService: UserAccessControlService) { }

  ngOnInit() { 
}

  onCancel() {
    this.dialogRef.close(false);
  }
}