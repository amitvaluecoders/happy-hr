import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserAccessControlRoutingModule } from './user-access-control.routing';
import { UserAccessControlService } from './user-access-control.service';
import { UserAccessControlComponent, addNewRoleDialog} from './user-access-control/user-access-control.component';
import { PermissionComponent } from './permission/permission.component';
import { MaterialModule } from '@angular/material';
import { FormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    UserAccessControlRoutingModule,
    MaterialModule,
    FormsModule
  ],
  entryComponents: [
      addNewRoleDialog
  ],
  declarations: [UserAccessControlComponent, PermissionComponent, addNewRoleDialog],
  providers:[UserAccessControlService]
})
export class UserAccessControlModule { }
