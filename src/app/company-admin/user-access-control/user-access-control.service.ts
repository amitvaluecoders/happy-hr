import { Injectable } from '@angular/core';
import { RestfulLaravelService } from '../../shared/services/restful-laravel.service';

@Injectable()
export class UserAccessControlService {

  constructor(private restfulLaravelService: RestfulLaravelService) { }

  addEditRole(reqBody): any {
    return this.restfulLaravelService.post('create-update-role', reqBody);
  }

  getRoles(): any {
    return this.restfulLaravelService.get('role-list');
  }

  deleteRole(reqBody): any {
    return this.restfulLaravelService.delete(`role-delete/${reqBody}`);
  }

  getAllModules(reqBody) {
    return this.restfulLaravelService.get(`get-role-permissoin/${reqBody}`);
  }

  updatePermission(reqBody) {
    return this.restfulLaravelService.put('set-role-permission', reqBody);
  }

}
