import { Injectable } from '@angular/core';
import {CanActivate,Router,RouterStateSnapshot,ActivatedRouteSnapshot} from "@angular/router";
import {CoreService} from '../../shared/services/core.service';
import {Observable} from 'rxjs/Rx'; 
import 'rxjs/add/operator/map';

@Injectable()
export class TrainingDayGuardService {

  public module = "trainingPlan";
  
        constructor(private _core: CoreService, private router: Router){ }
  
        canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {  
            let url: string;
            if(route['_routeConfig']['path'])
            url= route['_routeConfig']['path']
            else url = state.url;
            if(this.checkHavePermission(url)){
              return true;
            }else{
              this.router.navigate(['/extra/unauthorized']);
              return false;
            }
        }
  
        private checkHavePermission(path) {
            return true;
            // switch (path) {
            //     case "/app/company-admin/training-day":
            //         return this._core.getModulePermission(this.module, 'view');
            //     case 'master-training-plan/add':
            //         return this._core.getModulePermission(this.module, 'add');
            //     case 'master-training-plan/edit/:id':
            //         return this._core.getModulePermission(this.module, 'edit');
            //     case 'master-training-plan/detail/:id':
            //         return this._core.getModulePermission(this.module, 'view');
            //     case 'training-plans/previous/:id':
            //         return this._core.getModulePermission(this.module, 'view');   
            //     case 'training-plan/add':
            //         return this._core.getModulePermission(this.module, 'add');    
            //     case 'training-plan/add/:id':
            //         return this._core.getModulePermission(this.module, 'add'); 
            //     case 'training-plan/edit/:id':
            //         return this._core.getModulePermission(this.module, 'edit');              
            //     case 'training-plan/list':
            //         return this._core.getModulePermission(this.module, 'view'); 
            //     case 'training-day/:id':
            //         return this._core.getModulePermission(this.module, 'view'); 
            //     case 'employee-participation/certificate/:id/:uid':
            //         return this._core.getModulePermission(this.module, 'view'); 
            //     case 'training-plan/detail/:id':
            //         return this._core.getModulePermission(this.module, 'view'); 
            //     default:
            //         return false;
            // }
        }

}
