import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { APPCONFIG } from '../../../config';
import { CoreService } from '../../../shared/services/core.service';
import { TrainingDayService } from "../training-day.service";
import * as moment from 'moment';

@Component({
  selector: 'app-training-plan-detail',
  templateUrl: './training-plan-detail.component.html',
  styleUrls: ['./training-plan-detail.component.scss']
})
export class TrainingPlanDetailComponent implements OnInit {

  isProcessing;
  noResult = true;
  loggenInUser:any;

  trainingPlan = {
    "title": "", "reason": "", "actionPlan": "", "objective": "", "trainningTime": "", "trainningID": "", "completedOn": "",
    "trainee": [], "assignedBy": { "name": "", "imageUrl": "", "position": "" }
  }

  constructor(public companyService: TrainingDayService, public coreService: CoreService, public route: ActivatedRoute) { }

  ngOnInit() {
    let user=JSON.parse(localStorage.getItem('happyhr-userInfo'));
    this.loggenInUser = user.userRole;
    APPCONFIG.heading = "Training plan detail";

    if (this.route.snapshot.params['id']) {
      this.trainingPlan.trainningID = this.route.snapshot.params['id'];
      this.getTrainingDayDetail();
    }
  }

  getTrainingDayDetail() {
    this.isProcessing = true;
    this.companyService.getTrainingDayDetail(this.trainingPlan.trainningID).subscribe(
      res => {
        this.isProcessing = false;
        if (res.code == 200) {
          this.trainingPlan = res.data;
          this.noResult = false;
        }
        else return this.coreService.notify("Unsuccessful", res.message, 0);
      },
      err => {
        this.isProcessing = false;
        // if (err.code == 400) {
        //   return this.coreService.notify("Unsuccessful", err.message, 0);
        // }
        // this.coreService.notify("Unsuccessful", "Error while getting data.", 0);
      }
    )
  }

  formatDateTime(date) {
    return moment(date).isValid() ? moment(date).format('DD MMM YYYY hh:mm A') : 'Not specified';
  }

}
