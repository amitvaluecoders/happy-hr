import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { APPCONFIG } from '../../../config';
import { CoreService } from '../../../shared/services/core.service';
import { TrainingDayService } from "../training-day.service";
import * as moment from 'moment';

@Component({
  selector: 'app-employee-certificate-of-participation',
  templateUrl: './employee-certificate-of-participation.component.html',
  styleUrls: ['./employee-certificate-of-participation.component.scss']
})
export class EmployeeCertificateOfParticipationComponent implements OnInit {

  public progress;
  isProcessing = false;
  noResult = true;
  trainingPlan = {
    "userId": "", "title": "", "reason": "", "actionPlan": "", "objective": "", "trainningTime": "", "trainningID": "",
    "completedOn": "", "signature": "", "managerNote": "", "trainee": [], "assignedBy": { "name": "", "imageUrl": "", "position": "" }
  }

  constructor(public companyService: TrainingDayService, public coreService: CoreService, public route: ActivatedRoute) { }

  ngOnInit() {
    APPCONFIG.heading = "Employee certificate of participation";
    // this.progressBarSteps();

    if (this.route.snapshot.params['id']) {
      this.trainingPlan.trainningID = this.route.snapshot.params['id'];
      this.trainingPlan.userId = this.route.snapshot.params['uid'];
      this.getCertificateDetail();
    }
  }

  // progressBarSteps() {
  //   this.progress = {
  //     progress_percent_value: 100, //progress percent
  //     total_steps: 4,
  //     current_step: 4,
  //     circle_container_width: 20, //circle container width in percent
  //     steps: [
  //       // {num:1,data:"Select Role"},
  //       { num: 1, data: "Create training plan" },
  //       { num: 2, data: "Employee acceptance" },
  //       { num: 3, data: "Training day" },
  //       { num: 4, data: "Employee certificate", active: true },
  //     ]
  //   }
  // }

  getCertificateDetail() {
    this.isProcessing = true;
    let params = { "trainningID": this.trainingPlan.trainningID, "traineeID": this.trainingPlan.userId };
    this.companyService.getTraineeDetail(JSON.stringify(params)).subscribe(
      res => {
        this.isProcessing = false;
        if (res.code == 200) {
          this.noResult = false;
          this.trainingPlan = res.data;
        }
        else return this.coreService.notify("Unsuccessful", res.message, 0);
      },
      err => {
        this.isProcessing = false;
        // if (err.code == 400) {
        //   return this.coreService.notify("Unsuccessful", err.message, 0);
        // }
        // this.coreService.notify("Unsuccessful", "Error while getting data.", 0);
      }
    )
  }

  formatDateTime(date) {
    return moment(date).isValid() ? moment(date).format('DD MMM YYYY hh:mm A') : 'Not specified';
  }
}
