import { Component, OnInit } from '@angular/core';
import { APPCONFIG } from '../../../config';
import { Router, ActivatedRoute } from '@angular/router';
import { CoreService } from '../../../shared/services/core.service';
import { TrainingDayService } from "../training-day.service";

@Component({
  selector: 'app-create-master-training-plan',
  templateUrl: './create-master-training-plan.component.html',
  styleUrls: ['./create-master-training-plan.component.scss']
})
export class CreateMasterTrainingPlanComponent implements OnInit {

  isProcessing = false;
  isProcessingForm = false;
  noResult = true;
  masterTrainningPlanID = null;
  heading = "Create master training plan";
  masterPlan = {
    masterTrainningID: '', title: '', reason: '', actionPlan: '', objective: ''
  }

  constructor(public companyService: TrainingDayService, public router: Router, public route: ActivatedRoute, public coreService: CoreService) { }

  ngOnInit() {

    if (this.route.snapshot.params['id']) {
      this.heading = "Edit master training plan";
      this.masterTrainningPlanID = this.route.snapshot.params['id'];
      this.getMasterPlanDetail();
    } else this.noResult = false;

    APPCONFIG.heading = this.heading;
  }

  getMasterPlanDetail() {
    this.isProcessing = true;
    this.companyService.getMasterTrainingPlanDetail(this.masterTrainningPlanID).subscribe(
      res => {
        this.isProcessing = false;
        if (res.code == 200) {
          this.masterPlan = {
            actionPlan: res.data.actionPlan,
            masterTrainningID: this.masterTrainningPlanID,
            objective: res.data.objective,
            reason: res.data.reason,
            title: res.data.title
          };
          this.noResult = false;
        }
        else return this.coreService.notify("Unsuccessful", res.message, 0);
      },
      err => {
        this.isProcessing = false;
        // if (err.code == 400) {
        //   return this.coreService.notify("Unsuccessful", err.message, 0);
        // }
        // this.coreService.notify("Unsuccessful", "Error while getting data.", 0);
      }
    )
  }

  createMasterPlan(isValid) {
    if (!isValid) return;
    this.isProcessingForm = true;
    this.companyService.createMasterTrainingPlan(this.masterPlan).subscribe(
      res => {
        this.isProcessingForm = false;
        if (res.code == 200) {
          this.coreService.notify("Successful", res.message, 1);
          this.router.navigate(['/app/company-admin/training-day/master-training-plan/detail', res.data.masterTrainningID]);
        }
        else return this.coreService.notify("Unsuccessful", res.message, 0);
      },
      err => {
        this.isProcessingForm = false;
        if (err.code == 400) {
          return this.coreService.notify("Unsuccessful", err.message, 0);
        }
        this.coreService.notify("Unsuccessful", "Error while creating master plan.", 0);
      }
    )
  }

}