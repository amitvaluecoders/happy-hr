import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { APPCONFIG } from '../../../config';
import { CoreService } from '../../../shared/services/core.service';
import { TrainingDayService } from "../training-day.service";
import * as moment from 'moment'

@Component({
  selector: 'app-previous-training-plans',
  templateUrl: './previous-training-plans.component.html',
  styleUrls: ['./previous-training-plans.component.scss']
})
export class PreviousTrainingPlansComponent implements OnInit {
  isProcessing = false;
  noResult = true;
  previousPlan = {
    masterTrainningID: '', data: {}
  }
  planList = [];

  constructor(public companyService: TrainingDayService, public coreService: CoreService, public route: ActivatedRoute) { }

  ngOnInit() {
    APPCONFIG.heading = "Previous training plans";

    if (this.route.snapshot.params['id']) {
      this.previousPlan.masterTrainningID = this.route.snapshot.params['id'];
      this.getPreviousTrainingPlanDetail();
    }
  }

  getPreviousTrainingPlanDetail() {
    this.isProcessing = true;
    this.companyService.getPreviousTrainningPlan(this.previousPlan.masterTrainningID).subscribe(
      res => {
        this.isProcessing = false;
        if (res.code == 200) {
          this.previousPlan.data = res.data;
          Object.keys(this.previousPlan.data).filter(i => {
            if (this.previousPlan.data[i] instanceof Array) {
              this.planList.push(i);
            }
          });
          this.noResult = false;
        }
        else return this.coreService.notify("Unsuccessful", res.message, 0);
      },
      err => {
        this.isProcessing = false;
        // this.coreService.notify("Unsuccessful", "Error while getting data.", 0);
      }
    )
  }

}
