import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { APPCONFIG } from '../../../config';
import { CoreService } from '../../../shared/services/core.service';
import { TrainingDayService } from "../training-day.service";
import * as moment from 'moment';

@Component({
  selector: 'app-training-day',
  templateUrl: './training-day.component.html',
  styleUrls: ['./training-day.component.scss']
})
export class TrainingDayComponent implements OnInit {

  public progress;
  isProcessing = false;
  isProcessingRoll = false;
  isProcessingTranee = false;
  isSubmitting = false;
  noResult = true;
  // rollCalled = false;

  trainingPlan = {
    "title": "", "reason": "", "actionPlan": "", "objective": "", "trainningTime": "", "trainningID": "",
    "masterTrainningID": "", "assignedTo": [], "assignedBy": { "name": "", "imageUrl": "", "position": "" }
  }
  // roll = { "trainningID": "", "assignedTo": [] }
  trainingResult = {
    "trainningID": "", "noteToAll": "", "trainee": []
  }
  permission;
  loggenInUser:any;

  constructor(public companyService: TrainingDayService, public coreService: CoreService, public route: ActivatedRoute,
    public router: Router) { }

  ngOnInit() {
    let user=JSON.parse(localStorage.getItem('happyhr-userInfo'));
    this.loggenInUser = user.userRole;
    APPCONFIG.heading = "Training day";
    this.permission = this.coreService.getNonRoutePermission();
    // this.progressBarSteps();

    if (this.route.snapshot.params['id']) {
      this.trainingPlan.trainningID = this.route.snapshot.params['id'];
      this.getTrainingPlanDetail();
      // this.getTrainingDayDetail();
    }
  }

  // progressBarSteps() {
  //   this.progress = {
  //     progress_percent_value: 75, //progress percent
  //     total_steps: 4,
  //     current_step: 3,
  //     circle_container_width: 20, //circle container width in percent
  //     steps: [
  //       // {num:1,data:"Select Role"},
  //       { num: 1, data: "Create training plan" },
  //       { num: 2, data: "Employee acceptance" },
  //       { num: 3, data: "Training day", active: true },
  //       { num: 4, data: "Employee certificate" },
  //     ]
  //   }
  // }
  /**
   * to set the previously saved detail.
   */
  // getTrainingDayDetail() {
  //   this.isProcessingTranee = true;
  //   this.companyService.getTrainingDayDetail(this.trainingPlan.trainningID).subscribe(
  //     res => {
  //       this.isProcessingTranee = false;
  //       if (res.code == 200) {
  //         console.log(res.data);
  //         this.trainingResult = this.coreService.copyObject(this.trainingResult, res.data);
  //         if (this.trainingResult.trainee.length) {
  //           this.roll.assignedTo = this.trainingResult.trainee;
  //           this.rollCalled = true;
  //         }
  //       }
  //       else return this.coreService.notify("Unsuccessful", res.message, 0);
  //     },
  //     err => {
  //       this.isProcessingTranee = false;
  //       // if (err.code == 400) {
  //       //   return this.coreService.notify("Unsuccessful", err.message, 0);
  //       // }
  //       // this.coreService.notify("Unsuccessful", "Error while getting data.", 0);
  //     }
  //   )
  // }

  /**
   * get training detail
   */
  getTrainingPlanDetail() {
    this.isProcessing = true;
    this.companyService.getTrainingPlanDetail(this.trainingPlan.trainningID).subscribe(
      res => {
        this.isProcessing = false;
        if (res.code == 200) {
          this.trainingPlan = res.data;
          this.noResult = false;
        }
        else return this.coreService.notify("Unsuccessful", res.message, 0);
      },
      err => {
        this.isProcessing = false;
        // if (err.code == 400) {
        //   return this.coreService.notify("Unsuccessful", err.message, 0);
        // }
        // this.coreService.notify("Unsuccessful", "Error while getting data.", 0);
      }
    )
  }

  formatDateTime(date) {
    return moment(date).isValid() ? moment(date).format('YYYY-MM-DD hh:mm A') : 'Not specified';
  }

  // isChecked(traineeID) {
  //   return this.trainingResult.trainee.findIndex(i => i.traineeID == traineeID) >= 0;
  // }

  onSelectEmp(event, user) {
    console.log('user ==>> ', user);
    let ind = this.trainingResult.trainee.findIndex(i => i.traineeID == user.traineeID);

    if (event.target.checked) {
      (ind >= 0) ? null : this.trainingResult.trainee.push({ "trainingMappingID": user.trainingMappingID, "traineeID": user.traineeID, "traineeDetail": user.traineeDetail, "note": "", "result": "" });
    } else {
      (ind >= 0) ? this.trainingResult.trainee.splice(ind, 1) : null;
    }
  }

  // rollCall() {
  //   this.roll.trainningID = this.trainingPlan.trainningID;
  //   this.isProcessingRoll = true;
  //   this.companyService.rollCall(this.roll).subscribe(
  //     res => {
  //       this.isProcessingRoll = false;
  //       if (res.code == 200) {
  //         // this.rollCalled = true;
  //         // this.trainingResult.trainee = this.roll.assignedTo;
  //         // this.trainingResult.trainee.filter(item => { item['note'] = ''; item['result'] = '' });
  //         this.getTrainingDayDetail();
  //         this.coreService.notify("Successful", res.message, 1);
  //       }
  //       else return this.coreService.notify("Unsuccessful", res.message, 0);
  //     },
  //     err => {
  //       this.isProcessingRoll = false;
  //       if (err.code == 400) {
  //         return this.coreService.notify("Unsuccessful", err.message, 0);
  //       }
  //       this.coreService.notify("Unsuccessful", "Error while submitting.", 0);
  //     }
  //   )
  // }

  isValidResult() {
    for (let i = 0; i < this.trainingResult.trainee.length; i++) {
      if (!this.trainingResult.trainee[i].result)
        return false;
    }
    return true;
  }

  submitNotes(isValid) {
    if (!isValid) return;
    if (!this.isValidResult()) return;
    this.trainingResult.trainningID = this.trainingPlan.trainningID;

    this.isSubmitting = true;
    this.companyService.saveTrainingNotes(this.trainingResult).subscribe(
      res => {
        this.isSubmitting = false;
        if (res.code == 200) {
          this.coreService.notify("Successful", res.message, 1);
          this.router.navigate(['/app/company-admin/training-day/training-plan/detail', this.trainingPlan.trainningID]);
        }
        else return this.coreService.notify("Unsuccessful", res.message, 0);
      },
      err => {
        this.isSubmitting = false;
        if (err.code == 400) {
          return this.coreService.notify("Unsuccessful", err.message, 0);
        }
        this.coreService.notify("Unsuccessful", "Error while submitting data.", 0);
      }
    )
  }
}
