import { Component, OnInit } from '@angular/core';
import { APPCONFIG } from '../../../config';
import { IDatePickerConfig } from 'ng2-date-picker';
import { Router, ActivatedRoute } from '@angular/router';
import { CoreService } from '../../../shared/services/core.service';
import { TrainingDayService } from "../training-day.service";
import * as moment from 'moment';

@Component({
    selector: 'app-create-training-plan',
    templateUrl: './create-training-plan.component.html',
    styleUrls: ['./create-training-plan.component.scss']
})
export class CreateTrainingPlanComponent implements OnInit {

    public progress;
    heading = 'Create training plan';
    isProcessingMaster = false;
    // isProcessingList = false;
    isProcessingForm = false;
    config: IDatePickerConfig = {};

    employeeList = [];
    selectedEmployee = [];
    dropdownSettings = {};

    trainningTime;
    trainingPlan = {
        "title": "", "reason": "", "actionPlan": "", "objective": "", "trainningTime": "", "trainningID": "", "masterTrainningID": "", "trainnee": []
    }

    // selectedMasterPlan = '';
    // masterPlanList = [];
    masterPlan = {
        "masterTrainningPlanID": "", "title": "", "reason": "", "actionPlan": "", "objective": ""
    }

    constructor(public companyService: TrainingDayService, public router: Router, public route: ActivatedRoute, public coreService: CoreService) {
        this.config.locale = "en";
        this.config.format = "YYYY-MM-DD hh:mm A";
        this.config.showMultipleYearsNavigation = true;
        this.config.weekDayFormat = 'dd';
        this.config.disableKeypress = true;
        this.config.monthFormat = "MMM YYYY";
        this.config.min = moment();
    }

    ngOnInit() {

        let type = this.route.snapshot.url[1].path;
        if (this.route.snapshot.params['id'] && type == 'add') {
            this.masterPlan.masterTrainningPlanID = this.route.snapshot.params['id'];
            this.getMasterPlanDetail();
        }

        else if (this.route.snapshot.params['id'] && type == 'edit') {
            this.heading = "Edit training plan";
            this.trainingPlan.trainningID = this.route.snapshot.params['id'];
            this.getTrainingPlanDetail();
        }

        APPCONFIG.heading = this.heading;
        // this.progressBarSteps();
        this.getEmployee();
        // this.getMasterPlanList();

        this.dropdownSettings = {
            singleSelection: false,
            text: "Select user",
            enableSearchFilter: true,
            classes: "myclass"
        };

        $('angular2-multiselect:eq(0) .list-area ul').addClass('listItems');
    }

    // progressBarSteps() {
    //     this.progress = {
    //         progress_percent_value: 25, //progress percent
    //         total_steps: 4,
    //         current_step: 1,
    //         circle_container_width: 20, //circle container width in percent
    //         steps: [
    //             { num: 1, data: "Create training plan", active: true },
    //             { num: 2, data: "Employee acceptance" },
    //             { num: 3, data: "Training day" },
    //             { num: 4, data: "Employee certificate" },
    //         ]
    //     }
    // }

    getEmployee() {
        this.companyService.getEmployeeWithSubContractor().subscribe(
            res => {
                if (res.code == 200) {
                    this.employeeList = res.data;
                    this.employeeList.filter((item, i) => { item['itemName'] = item.name; item['id'] = item.userId; });
                    setTimeout(() => {
                        this.employeeList.filter(function (item, i) {
                            $('.listItems li').eq(i).prepend(`<img src="${item.imageUrl}" alt="" />`);
                            $('.listItems li').eq(i).children('label').append('<span>' + item.designation + '</span>');
                        });
                    }, 0);
                }
                else return this.coreService.notify("Unsuccessful", res.message, 0);
            },
            err => {
                // if (err.code == 400) {
                //     return this.coreService.notify("Unsuccessful", err.message, 0);
                // }
                // this.coreService.notify("Unsuccessful", "Error while getting managers list.", 0);
            })
    }

    // getMasterPlanList() {
    //     this.isProcessingList = true;
    //     this.companyService.getMasterTrainingPlanList().subscribe(
    //         res => {
    //             this.isProcessingList = false;
    //             if (res.code == 200) {
    //                 this.masterPlanList = res.data;
    //                 // this.masterPlanList.filter((item, i) => { item['itemName'] = item.name; item['id'] = item.masterTrainningPlanID; });
    //             }
    //             else return this.coreService.notify("Unsuccessful", res.message, 0);
    //         },
    //         err => {
    //             this.isProcessingList = false;
    //             if (err.code == 400) {
    //                 return this.coreService.notify("Unsuccessful", err.message, 0);
    //             }
    //             this.coreService.notify("Unsuccessful", "Error while getting master plans.", 0);
    //         }
    //     )
    // }

    getMasterPlanDetail() {
        this.isProcessingMaster = true;
        this.companyService.getMasterTrainingPlanDetail(this.masterPlan.masterTrainningPlanID).subscribe(
            res => {
                this.isProcessingMaster = false;
                if (res.code == 200) {
                    this.masterPlan = res.data;
                    this.trainingPlan = {
                        "actionPlan": this.masterPlan.actionPlan, "masterTrainningID": this.masterPlan.masterTrainningPlanID,
                        "objective": this.masterPlan.objective, "reason": this.masterPlan.reason, "title": this.masterPlan.title,
                        "trainnee": [], "trainningID": '', "trainningTime": ''
                    }
                }
                else return this.coreService.notify("Unsuccessful", res.message, 0);
            },
            err => {
                this.isProcessingMaster = false;
                // if (err.code == 400) {
                //     return this.coreService.notify("Unsuccessful", err.message, 0);
                // }
                // this.coreService.notify("Unsuccessful", "Error while getting data.", 0);
            }
        )
    }

    getTrainingPlanDetail() {
        this.isProcessingMaster = true;
        this.companyService.getTrainingPlanDetail(this.trainingPlan.trainningID).subscribe(
            res => {
                this.isProcessingMaster = false;
                if (res.code == 200) {
                    this.trainingPlan = this.coreService.copyObject(this.trainingPlan, res.data);
                    this.trainningTime = moment(this.trainingPlan.trainningTime);
                    this.selectedEmployee = res.data.assignedTo ? res.data.assignedTo.map(i => {
                        return { id: i.traineeDetail.userID, itemName: i.traineeDetail.name }
                    }) : [];
                }
                else return this.coreService.notify("Unsuccessful", res.message, 0);
            },
            err => {
                this.isProcessingMaster = false;
                // if (err.code == 400) {
                //     return this.coreService.notify("Unsuccessful", err.message, 0);
                // }
                // this.coreService.notify("Unsuccessful", "Error while getting data.", 0);
            }
        )
    }

    // onSelectMasterPlan(e) {
    //     if (e == '') {
    //         this.trainingPlan = {
    //             "title": "", "reason": "", "actionPlan": "", "objective": "", "trainningTime": "", "trainningID": "",
    //             "masterTrainningID": "", "trainnee": []
    //         };
    //     }
    //     let master = this.masterPlanList.filter(i => i.masterTrainningPlanID == e).shift();
    //     if (master) {
    //         this.trainingPlan = {
    //             "actionPlan": master.actionPlan, "masterTrainningID": master.masterTrainningPlanID,
    //             "objective": master.objective, "reason": master.reason, "title": master.title,
    //             "trainnee": [], "trainningID": '', trainningTime: ''
    //         };
    //     }
    // }

    createTrainingPlan(isValid) {
        if (!isValid) return;
        this.trainingPlan.trainnee = this.selectedEmployee.length ? this.selectedEmployee.map(i => { return i.id }) : [];
        this.trainingPlan.trainningTime = !this.trainningTime ? '' : moment(this.trainningTime).format('YYYY-MM-DD HH:mm:ss');

        this.isProcessingForm = true;
        this.companyService.createTrainingPlan(this.trainingPlan).subscribe(
            res => {
                this.isProcessingForm = false;
                if (res.code == 200) {
                    this.coreService.notify("Successful", res.message, 1);
                    this.router.navigate(['/app/company-admin/training-day/training-plan/list']);
                }
                else return this.coreService.notify("Unsuccessful", res.message, 0);
            },
            err => {
                this.isProcessingForm = false;
                if (err.code == 400) {
                    return this.coreService.notify("Unsuccessful", err.message, 0);
                }
                this.coreService.notify("Unsuccessful", "Error while submitting data.", 0);
            }
        )
    }

}