import { Component, OnInit } from '@angular/core';
import { PolicyDocumentsService } from '../policy-documents.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { CoreService } from '../../../shared/services/core.service';
import {APPCONFIG } from '../../../config';

@Component({
  selector: 'app-unused-documents',
  templateUrl: './unused-documents.component.html',
  styleUrls: ['./unused-documents.component.scss']
})
export class UnusedDocumentsComponent implements OnInit {
  
  public collapse={};
  public hrToggle=false;
  public toggleFilter: boolean;
  public isProcessing=false;
  public documents=[];
  
  constructor(public policyDocumentsService:PolicyDocumentsService,
    public router:Router,public activatedRoute:ActivatedRoute,
    public coreService:CoreService) { }

  ngOnInit() {
     APPCONFIG.heading="List of unused documents";
     this.getList();
  }

  getList(){
    this.isProcessing=true;
        this.policyDocumentsService.getUnusedDocumentList().subscribe(
          d => {
            if (d.code == "200") {
              this.documents=d.data;
              this.isProcessing=false;
            }
            else{
              this.coreService.notify("Unsuccessful", d.message, 0);
              this.isProcessing=false;
            } 
          },
          error => this.coreService.notify("Unsuccessful", error.message, 0), 
      )
  }

  onShowDetails(id){
    this.router.navigate([`/app/company-admin/policy-documents/unused-document-details/${id}`]);
    
  }


}
