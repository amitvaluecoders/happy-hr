import { Component, OnInit } from '@angular/core';
import { PolicyDocumentsService } from '../policy-documents.service';
import { Router, ActivatedRoute, Params } from '@angular/router';

import { CoreService } from '../../../shared/services/core.service';

@Component({
  selector: 'app-preview-document',
  templateUrl: './preview-document.component.html',
  styleUrls: ['./preview-document.component.scss']
})
export class PreviewDocumentComponent implements OnInit {

 public isProcessing=false;
 public isNew=true;
public policy;
    constructor(
               public router:Router,public activatedRoute:ActivatedRoute,
               public coreService:CoreService,public policyDocumentsService:PolicyDocumentsService) { }

  ngOnInit() {
    // if(this.policy.companyContractID) this.isNew=false;
  }

  onPrint(){
    // window.print();window.close()
     let printContents, popupWin;
    printContents = document.getElementById('printBox').outerHTML;
    popupWin = window.open();
    popupWin.document.open();
    popupWin.document.write(`
      <html>
        <head>
          <title>Print tab</title>
          <style>
          //........Customized style.......
          </style>
        </head>
    <body onload="window.print();window.close()">${printContents}</body>
      </html>`
    );
    popupWin.document.close();

  }

}
