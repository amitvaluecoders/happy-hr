import { Component, OnInit } from '@angular/core';
import { PolicyDocumentsService } from '../policy-documents.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { CoreService } from '../../../shared/services/core.service';
import {APPCONFIG } from '../../../config';

@Component({
  selector: 'app-policy-documents-list',
  templateUrl: './policy-documents-list.component.html',
  styleUrls: ['./policy-documents-list.component.scss']
})
export class PolicyDocumentsListComponent implements OnInit {
  public unsubscriber:any;
  public isApplyBtnClicked=false;
  public hrPolicy=[];
  public generalHrPolicy=[];
  public performanceManagement=[];
 public collapse={};
 public isSubCompany:any;
  public toggleFilter = true;
  public isOwnContractProcessing=false;
  public hrToggle = false;
  public ownerToggle = false;
  public ownContracts=[];
  public happyHRContracts=[];
  public isFilterDataProcessing=false;
  public versionSearch:'';
  public typeSearch:'';
  public serachKey:'';
  public tempReqBody={
    "search":"",
    "filter":{
      "status":{
        "Active":false,
        "Inactive":false
      },
      "versions":[],
      "lastUpdated":{
        "int7":false,
        "int30":false,
        "int90":false,
        "int180":false,
        "int365":false,
      },
      "types":[],
      "category":{
        hrPolicy:false,
        generalHrPolicy:false,
        performanceManagement:false
      }
    }
  }
  public reqBody={
    "search":"",
    "filter":{
      "status":{
        "Active":false,
        "Inactive":false
      },
      "versions":{
      },
      "lastUpdated":{
        "int7":false,
        "int30":false,
        "int90":false,
        "int180":false,
        "int365":false,
      },
      "types":{
      },
      "category":{
        hrPolicy:true,
        generalHrPolicy:true,
        performanceManagement:true
      }
    }
  }
 
  constructor(public policyDocumentsService:PolicyDocumentsService,
               public router:Router,public activatedRoute:ActivatedRoute,
               public coreService:CoreService) { }

  ngOnInit() {
    this.isSubCompany=JSON.parse(localStorage.getItem('happyhr-userInfo')).isSubCompany;    
    APPCONFIG.heading="Policy documents"
    this.getCOntractsLsitFilterData();
    // this.getList();
    
  }

// method to apply filters 
  onApplyfilters(){
      this.reqBody.filter.category=JSON.parse(JSON.stringify(this.tempReqBody.filter.category));
      if(!this.tempReqBody.filter.category.hrPolicy && !this.tempReqBody.filter.category.generalHrPolicy && !this.tempReqBody.filter.category.performanceManagement){
        this.reqBody.filter.category.hrPolicy=true;
        this.reqBody.filter.category.generalHrPolicy=true;
        this.reqBody.filter.category.performanceManagement=true;
      }
      this.reqBody.filter.lastUpdated = this.tempReqBody.filter.lastUpdated;
      this.reqBody.filter.status =  this.tempReqBody.filter.status;
      // this.reqBody.filter.types={};
      this.reqBody.filter.versions={};
      // this.tempReqBody.filter.types.forEach(item =>{
      //   if(item.isChecked) this.reqBody.filter.types[item.type]=true;
      // })
      this.tempReqBody.filter.versions.forEach(item =>{
        if(item.isChecked) this.reqBody.filter.versions[item.version]=true;
      })
     this.getList();
  }


  onSearch(){
    this.unsubscriber.unsubscribe();
    this.reqBody.search=encodeURIComponent(this.tempReqBody.search);
    this.getList();    
  }

// method to filter data details :- CONTRACT TYPES and VERSIONS
  getCOntractsLsitFilterData(){
    this.isFilterDataProcessing = true;
      this.isOwnContractProcessing=true;    
      this.policyDocumentsService.getListFilerData().subscribe(
          d => {
             this.isFilterDataProcessing = false;
              if (d.code == "200") {
                this.tempReqBody.filter.types=d.data.type;
                this.tempReqBody.filter.versions=d.data.versions;
                this.getList();
              }
              else this.coreService.notify("Unsuccessful", d.message, 0);
          },
          error => {
            this.isFilterDataProcessing = false;
            this.coreService.notify("Unsuccessful", error.message, 0); 
          }      
      )
  }


// method to get listing OWN and HAPPY HR contracts.
  getList(){
      this.isOwnContractProcessing=true;
      console.log("ee",this.reqBody)
       this.unsubscriber =  this.policyDocumentsService.getPolicyList(JSON.stringify(this.reqBody)).subscribe(
          d => {
              if (d.code == "200") {  
                if(d.data){   
                this.hrPolicy=d.data['HR policy']?d.data['HR policy']:[];
                this.generalHrPolicy=d.data['General HR policy']?d.data['General HR policy']:[];   
                this.performanceManagement=d.data['Performance management']?d.data['Performance management']:[];
                }     
                this.isOwnContractProcessing=false;
              }
              else{
                this.isOwnContractProcessing=false;
                this.coreService.notify("Unsuccessful", "Error while getting contracts", 0); 
              } 
          },
          error => {
              this.coreService.notify("Unsuccessful", "Error while getting contracts", 0);
              this.isOwnContractProcessing=false;
          }      
      )
  }

  onPolicyDetails(policy){

    let url="/app/company-admin/policy-documents/detail/"+policy.policyDocumentID;
    if(policy.companyPolicyDocumentID) url="/app/company-admin/policy-documents/detail/own/"+policy.companyPolicyDocumentID;
    this.router.navigate([url]);
  }

  onAcceptReject(policyDocumentID){
    this.router.navigate([`/app/company-admin/policy-documents/accept/reject/change/${policyDocumentID}`]);
  }

  onEditContract(contract){
     this.router.navigate([`/app/company-admin/contract/update/${contract.companyContractID}`]);
  }

}
