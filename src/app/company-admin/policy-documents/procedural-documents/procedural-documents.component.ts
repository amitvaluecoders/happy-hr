import { Component, OnInit ,ViewContainerRef} from '@angular/core';
import { MdDialog,MdDialogRef} from '@angular/material';
import { PolicyDocumentsService } from '../policy-documents.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { CoreService } from '../../../shared/services/core.service';
import { Policy } from '../policy';
import { PreviewDocumentComponent} from '../preview-document/preview-document.component';
import {APPCONFIG } from '../../../config';

@Component({
  selector: 'app-procedural-documents',
  templateUrl: './procedural-documents.component.html',
  styleUrls: ['./procedural-documents.component.scss']
})
export class ProceduralDocumentsComponent implements OnInit {
  public unsubscriber:any;
  public collapse={};
  public isSubCompany:any;
  public versionSearch='';
  public isApplyBtnClicked=false;
  public documents=[];
  public isProcessing=false;
  public hrToggle=false;
  public toggleFilter: boolean;
  public tempReqBody={
    "search":"",
    "filter":{
      "status":{
        "Active":false,
        "Inactive":false
      },
      "version":[],
      "lastUpdated":{
        "int7":false,
        "int30":false,
        "int90":false,
        "int180":false,
        "int365":false,
      }
    }
  }
  public reqBody={
    "search":"",
    "filter":{
      "status":{
        "Active":false,
        "Inactive":false
      },
      "version":{
      },
      "lastUpdated":{
        "int7":false,
        "int30":false,
        "int90":false,
        "int180":false,
        "int365":false,
      }
    }
  }
  
  constructor( public dialog:MdDialog,public policyService:PolicyDocumentsService,
    public router:Router,public activatedRoute:ActivatedRoute,
    public coreService:CoreService) { }


  ngOnInit() {
    this.toggleFilter = true;
    this.isSubCompany=JSON.parse(localStorage.getItem('happyhr-userInfo')).isSubCompany;        
    APPCONFIG.heading="List of procedural documents";
    this.getList();
  }

  onApplyfilters(){
    
    this.reqBody.filter.lastUpdated = this.tempReqBody.filter.lastUpdated;
    this.reqBody.filter.status =  this.tempReqBody.filter.status;
    this.reqBody.filter.version={};
    this.tempReqBody.filter.version.forEach(item =>{
      if(item.isChecked) this.reqBody.filter.version[item.version]=true;
    })
   this.getList();
}

onSearch(){
  this.unsubscriber.unsubscribe();
  this.reqBody.search=encodeURIComponent(this.tempReqBody.search);
  this.getList();    
}


// method to get listing of procedural documents.
    getList(){
      this.isProcessing=true;  
      this.unsubscriber =  this.policyService.getProceduralDocumentList(JSON.stringify(this.reqBody)).subscribe(
          d => {
              if (d.code == "200") {  
                this.documents=d.data;  
                if(!this.tempReqBody.filter.version.length) {
                  d.data.filter(item=>{
                     this.tempReqBody.filter.version.push({isChecked:false,version:item.version});
                   })
                } 
                this.isProcessing=false;
              }
              else{
                this.isProcessing=false;
                this.coreService.notify("Unsuccessful", "Error while getting contracts", 0); 
              } 
          },
          error => {
              this.coreService.notify("Unsuccessful", "Error while getting contracts", 0);
              this.isProcessing=false;
          }      
      )
    }

    onDetails(companyProceduralDocumentID){
      this.router.navigate([`/app/company-admin/policy-documents/procedural-document/detail/${companyProceduralDocumentID}`]);      
    }

    onEdit(companyProceduralDocumentID){
      this.router.navigate([`/app/company-admin/policy-documents/update/pd/${companyProceduralDocumentID}`]);      
    }
}
