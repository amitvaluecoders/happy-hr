import { Component, OnInit } from '@angular/core';
import { MdSnackBar, MdDialog, MdDialogRef } from '@angular/material';
import { PolicyDocumentsService } from '../policy-documents.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { CoreService } from '../../../shared/services/core.service';
import {APPCONFIG } from '../../../config';


@Component({
  selector: 'app-accept-reject-change-version-log',
  templateUrl: './accept-reject-change-version-log.component.html',
  styleUrls: ['./accept-reject-change-version-log.component.scss']
})
export class AcceptRejectChangeVersionLogComponent implements OnInit {

    public contractDetails:any;
  public isProcessing;
  public policy = {
    new: {
      description: ""
    }
  };
   constructor(public policyDocumentsService:PolicyDocumentsService,public dialogRef: MdDialogRef<AcceptRejectChangeVersionLogComponent>,
               public router:Router,public activatedRoute:ActivatedRoute,
               public coreService:CoreService) { }
  
  public policyDetails:any;

   ngOnInit() { 
    this.compareSubjectsDetails(); 
    this.policy.new.description=this.policyDetails.subject.description;
  }
  

  compareSubjectsDetails(){
    
      this.isProcessing=true;
         this.policyDocumentsService.compareSubjects(this.policyDetails.subject.policyDocumentSubjectID).subscribe(
          d => {
              if (d.code == "200") {
                if(d.data){
                  this.policy['accepted']=d.data.newSubject;
                  this.policy['edited']=d.data.oldSubject;                  
                }
                this.isProcessing=false;
              }
              else{
                //  this.dialogRef.close(0);
                this.isProcessing=false;
                this.coreService.notify("Unsuccessful", d.message, 0); 
              } 
          },
          error => {
            this.isProcessing=false;
            this.coreService.notify("Unsuccessful", error.message, 0)
            //  this.dialogRef.close(this.contract.new.description);
          }
         
      )
  }

  onAccept(isValid){
    if(isValid){
      this.dialogRef.close(this.policy.new.description);
    }
  }

}
