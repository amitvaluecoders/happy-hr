import {  ModuleWithProviders  } from '@angular/core';
import {  RouterModule,Routes  } from '@angular/router';

import { PolicyDocumentsListComponent } from './policy-documents-list/policy-documents-list.component';
import { PolicyDetailComponent } from './policy-detail/policy-detail.component';
import { VersionHistoryComponent } from './version-history/version-history.component';
import { CreateNewPolicyComponent } from './create-new-policy/create-new-policy.component';
import { PreviewDocumentComponent } from './preview-document/preview-document.component';
import { ProceduralDocumentsComponent } from './procedural-documents/procedural-documents.component';
import { ProceduralDocumentDetailComponent } from './procedural-document-detail/procedural-document-detail.component';
import { UnusedDocumentsComponent } from './unused-documents/unused-documents.component';
import { AcceptRejectChangeComponent } from './accept-reject-change/accept-reject-change.component';
import { AcceptRejectChangeVersionLogComponent } from './accept-reject-change-version-log/accept-reject-change-version-log.component';
import { CreateProceduralDocumentComponent } from './create-procedural-document/create-procedural-document.component';
import { UnusedDocumentDetailsComponent } from './unused-document-details/unused-document-details.component';

export const PolicyDocumentsPagesRoutes: Routes = [
    {
        path: '',
        canActivate:[],
        children: [
           
            { path: '', component: PolicyDocumentsListComponent},
            { path: 'detail/:policyDocumentID', component: PolicyDetailComponent},
            { path: 'detail/own/:companyPolicyDocumentID', component: PolicyDetailComponent},
            { path: 'version-history/:companyPolicyDocumentID', component: VersionHistoryComponent},
            { path: 'version-history/sa/:policyDocumentID', component: VersionHistoryComponent},
            { path: 'create', component: CreateNewPolicyComponent},
            { path: 'create/pd', component: CreateProceduralDocumentComponent},
            { path: 'update/:id', component: CreateNewPolicyComponent},
            { path: 'update/pd/:id', component: CreateProceduralDocumentComponent},
            { path: 'document/preview', component: PreviewDocumentComponent},
            { path: 'procedural-document', component: ProceduralDocumentsComponent},
            { path: 'procedural-document/detail/:id', component: ProceduralDocumentDetailComponent},
            { path: 'unused-documents', component: UnusedDocumentsComponent},
            { path: 'unused-document-details/:id', component: UnusedDocumentDetailsComponent},
            { path: 'accept/reject/change/:id', component: AcceptRejectChangeComponent},
            { path: 'accept/reject/versions', component: AcceptRejectChangeVersionLogComponent},
                
        ]
    }
];

export const PolicyDocumentsRoutingModule = RouterModule.forChild(PolicyDocumentsPagesRoutes);
