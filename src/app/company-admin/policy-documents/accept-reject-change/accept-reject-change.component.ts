import { Component, OnInit } from '@angular/core';
// import { ContractsService } from '../contracts.service';
import { PolicyDocumentsService } from '../policy-documents.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { CoreService } from '../../../shared/services/core.service';
// import { CompanyContract } from '../contract';
import { MdDialog,MdDialogRef} from '@angular/material';
import {AcceptRejectChangeVersionLogComponent } from '../accept-reject-change-version-log/accept-reject-change-version-log.component';
import {APPCONFIG } from '../../../config';
import { ResponseNoteComponent } from '../../../shared/components/response-note/response-note.component'

@Component({
  selector: 'app-accept-reject-change',
  templateUrl: './accept-reject-change.component.html',
  styleUrls: ['./accept-reject-change.component.scss']
})
export class AcceptRejectChangeComponent implements OnInit {
public contract={};
public policy={};
public isProcessing;
public isCreateProcessing;
public isSubjectNew=true;
public tgl={};
  constructor( public dialog:MdDialog,public policyDocumentsService:PolicyDocumentsService,
               public router:Router,public activatedRoute:ActivatedRoute,
               public coreService:CoreService) { }

  ngOnInit() {
    APPCONFIG.heading="Accept / Reject change"
    this.getContractDetails();
  }

  getContractDetails(){
  this.isProcessing=true;
  let reqBody={isHHRContract:true,id:this.activatedRoute.snapshot.params['id']};
      this.policyDocumentsService.getPolicyDetailsforEdit(reqBody).subscribe(
        d => {
           this.isProcessing=false;  
            if (d.code == "200") {
              d.data.policyDocumentSubjects.forEach(element => {
                element['status']='Accepted';
                element['isUpdated']=false;
                return true;
              });

              this.policy=d.data;
              this.policy['policyDocumentID']=d.data.policyDocumentID;
              this.policy['companyPolicyDocumentID']='';
            } 
            else this.coreService.notify("Unsuccessful", "Error while getting contracts", 0) 
        },
        error => this.coreService.notify("Unsuccessful", "Error while getting contracts", 0)
    )
  }

  onEditBeforeAccept(subject){
      let dialogRef=this.dialog.open(AcceptRejectChangeVersionLogComponent,{height:'70%',width:'80%'});
      dialogRef.componentInstance.policyDetails={policy:this.policy,subject:subject};
      dialogRef.afterClosed().subscribe(result => {
        if(result){    
          this.isSubjectNew=false;    
            this.policy['policyDocumentSubjects'].filter(function(item){
              if(item.policyDocumentSubjectID==subject.policyDocumentSubjectID) {
                item.isUpdated=true;
                item.description=result;
              }
              return item;
            })
        }
      });   

  }

  onAceeptChangeCompletely(){
    this.isCreateProcessing=true; 
    this.policy['policySubjects']=this.policy['policyDocumentSubjects'];
    this.policyDocumentsService.addEditCompanyPolicy(this.policy).subscribe(
        d => {
            if (d.code == "200")  {
              this.coreService.notify("Successful", d.message, 1) 
              this.router.navigate([`/app/company-admin/policy-documents`]);
            }
            else this.coreService.notify("Unsuccessful", d.message, 0) 
        },
        error => this.coreService.notify("Unsuccessful", error.message, 0)
    )
  }

  onRejectWithNote(){
       let tempResponse={
            url:"policy-document-reject",
            note:{
              policyDocumentID:this.policy['policyDocumentID'],
              responseNote:""
       }
      }
      let dialogRef=this.dialog.open(ResponseNoteComponent,{width:'600px'});
      dialogRef.componentInstance.noteDetails=tempResponse;
      dialogRef.afterClosed().subscribe(result => {
        if(result){    
         this.router.navigate([`/app/company-admin/policy-documents`]);
        }
      });   
  }

  onVersionList(){
    this.router.navigate([`/app/company-admin/policy-documents/version-history/sa/${this.policy['policyDocumentID']}`]);  
  }


}
