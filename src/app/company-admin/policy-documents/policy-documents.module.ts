import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PolicyDocumentsRoutingModule } from './policy-documents.routing';
import { PolicyDocumentsService } from './policy-documents.service';
import { PolicyDocumentsListComponent } from './policy-documents-list/policy-documents-list.component';
import { PolicyDetailComponent } from './policy-detail/policy-detail.component';
import { VersionHistoryComponent } from './version-history/version-history.component';
import { CreateNewPolicyComponent } from './create-new-policy/create-new-policy.component';
import { PreviewDocumentComponent } from './preview-document/preview-document.component';
import { ProceduralDocumentsComponent } from './procedural-documents/procedural-documents.component';
import { ProceduralDocumentDetailComponent } from './procedural-document-detail/procedural-document-detail.component';
import { UnusedDocumentsComponent } from './unused-documents/unused-documents.component';
import { AcceptRejectChangeVersionLogComponent } from './accept-reject-change-version-log/accept-reject-change-version-log.component';
import { MaterialModule } from '@angular/material';
import { QuillModule } from 'ngx-quill';
import { FormsModule } from '@angular/forms';
import { SharedModule } from '../../shared/shared.module';
import { AcceptRejectChangeComponent } from './accept-reject-change/accept-reject-change.component';
import { AngularMultiSelectModule } from 'angular2-multiselect-checkbox-dropdown/angular2-multiselect-dropdown';
import { PolicyDocumentGuardService } from './policy-document-guard.service';
import {DataTableModule} from 'angular2-datatable';
import { CreateProceduralDocumentComponent } from './create-procedural-document/create-procedural-document.component';
import { UnusedDocumentDetailsComponent } from './unused-document-details/unused-document-details.component';

@NgModule({
  imports: [
    CommonModule,
    DataTableModule,
    PolicyDocumentsRoutingModule,
    MaterialModule,
    QuillModule,
    FormsModule,
    SharedModule,
    AngularMultiSelectModule
  ],
  declarations: [PolicyDocumentsListComponent, PolicyDetailComponent, VersionHistoryComponent, CreateNewPolicyComponent, PreviewDocumentComponent, ProceduralDocumentsComponent, ProceduralDocumentDetailComponent, UnusedDocumentsComponent, AcceptRejectChangeVersionLogComponent, AcceptRejectChangeComponent, CreateProceduralDocumentComponent, UnusedDocumentDetailsComponent],
  providers:[PolicyDocumentsService,PolicyDocumentGuardService]
})
export class PolicyDocumentsModule { }
