import { Component, OnInit ,ViewContainerRef} from '@angular/core';
import { MdDialog,MdDialogRef} from '@angular/material';
import { PolicyDocumentsService } from '../policy-documents.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { CoreService } from '../../../shared/services/core.service';
import { Policy } from '../policy';
import { PreviewDocumentComponent} from '../preview-document/preview-document.component';
import {APPCONFIG } from '../../../config';


@Component({
  selector: 'app-create-procedural-document',
  templateUrl: './create-procedural-document.component.html',
  styleUrls: ['./create-procedural-document.component.scss']
})
export class CreateProceduralDocumentComponent implements OnInit {


  public isAddEdit="Create";
  public isDetailProcessing=false;
  public isAddEditProcessing=false;
  public buttonClicked=false;
  public title="";
  public isProcessing=false;
  isSub=false;
// drop down options for Policy type
public dropdropdownListPDs=[
]
public dropdropdownListPDForSub=[];
// drop down Setting for Policy type
public dropdownSettings={
  
}
public selectedItemsPDs=[];
public id="";
public document={
  companyProceduralDocumentID:"",
  proceduralDocumentID:"",
  companyPositionID:[],
  title:"",
  description:"",
  source:"",
  status:'Active',
  documentSubjects:[
    {
        title:"",
        description:"",
    }
  ]
}

public array=[
  {
    name:"ajay",
    isSelected:false
  },
  {
    name:"vi",
    isSelected:false
  },
  {
    name:"asd",
    isSelected:false
  }
];


  constructor( public dialog:MdDialog,public policyService:PolicyDocumentsService,
               public router:Router,public activatedRoute:ActivatedRoute,
               public coreService:CoreService) { }

  ngOnInit() {
    APPCONFIG.heading="Create procedural document";
    this.id=this.activatedRoute.snapshot.params['id'];
    if(this.id){
      this.isDetailProcessing=true;
      this.isAddEdit="Update";
      APPCONFIG.heading="Update procedural document";
    }
    this.dropdownSettings={
      singleSelection: false,
      enableSearchFilter: false,
       disabled: false,
      text: "Select position description"
    }
    this.getAllPDTypes();

  }

  // onChangeStatus(){
  //   this.policy.status=(this.policy.status=="Active")?"Inactive":"Active"
  // }

  getAllPDTypes(){
     this.isProcessing=true;
     this.policyService.getPDType().subscribe(
          d => {
              if (d.code == "200") {
                if(this.id) this.getDetails();
                this.dropdropdownListPDs = d.data.filter(function(item){
                  item['itemName']=item['positionTitle'];
                  item['id']=item.positionId;
                  return item
                });
            this.dropdropdownListPDs.push({ "id": "x", "itemName": "Sub contractor" });
                  
                console.log("drop",this.dropdropdownListPDs);        
                this.isProcessing=false;
              }
              else this.coreService.notify("Unsuccessful", d.message, 0);
          },
          error => this.coreService.notify("Unsuccessful", "Error while getting policy types", 0),
          () => { }
      )
  }

  getDetails(){
      this.isDetailProcessing=true;
       this.policyService.getProceduralDocumentDetails(this.id).subscribe(
          d => {
              if (d.code == "200") {
                this.document=d.data;
                // this.selectedItemsPDs=this.dropdropdownListPDs.filter(function(item){
                //   if(item.id==d.data.policyTypeID) return item;
                // })
                this.selectedItemsPDs=d.data.position;
                this.isDetailProcessing=false;   
              }
              else{  
                this.isDetailProcessing=false;   
                this.coreService.notify("Unsuccessful", d.message, 0); 
              } 
          },
          error => {
               this.isDetailProcessing=false;
              this.coreService.notify("Unsuccessful", error.message, 0);  
          }
      )
  }

  onAddMoreSubjects(){
    this.document.documentSubjects.push({title:"",description:""})
  }

OnDeleteSubject(i){
   this.document.documentSubjects.splice(i,1);
  
}

//  onPreview(){
//       let dialogRef=this.dialog.open(PreviewDocumentComponent,{height:'90%',width:'60%'});
//       dialogRef.componentInstance.policy=this.policy;
//       dialogRef.afterClosed().subscribe(result => {
//         if(result){        
//         }
//       });      
//   }
onSelectSelectedPd(item,selected){
  this.dropdropdownListPDForSub=[];
  if(item.id=="x"){
    this.dropdropdownListPDForSub.push(item);
    this.isSub=true;
  }
  // else{
  //   this.dropdropdownListPDs.splice(this.dropdropdownListPDs.length,1);
  // } 
}
OnItemDeSelect(item){
  this.isSub=false;
}


createDocument(isvalid){
  if(isvalid){
    this.isAddEditProcessing=true;    
    this.document.companyPositionID=[];
    for(let c of this.selectedItemsPDs){
      if(c.id=="x"){
            this.document.companyPositionID=[];
            this.document["pdFor"]="companyContractor";
      }
      else{
          this.document.companyPositionID.push(c.id);
          this.document["pdFor"]="companyEmployee";
      }
    }
    console.log("pd",this.document);
    
     this.policyService.addEditCompanyProceduralDocument(this.document).subscribe(
          d => {
              if (d.code == "200") {
                this.coreService.notify("Successful", d.message, 1);
                this.router.navigate([`/app/company-admin/policy-documents/procedural-document`]);
                this.isAddEditProcessing=false;
              }
              else{
                this.isAddEditProcessing=false;
                this.coreService.notify("Unsuccessful", "Error while getting Policy types", 0); 
              } 
          },
          error => {
              this.coreService.notify("Unsuccessful", "Error while getting Policy types", 0);
              this.isAddEditProcessing=false;
          }
         
      )
   
  }
  }

}
