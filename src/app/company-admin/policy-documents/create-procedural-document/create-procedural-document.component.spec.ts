import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateProceduralDocumentComponent } from './create-procedural-document.component';

describe('CreateProceduralDocumentComponent', () => {
  let component: CreateProceduralDocumentComponent;
  let fixture: ComponentFixture<CreateProceduralDocumentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateProceduralDocumentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateProceduralDocumentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
