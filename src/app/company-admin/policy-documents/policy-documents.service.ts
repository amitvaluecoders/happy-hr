import { Injectable } from '@angular/core';
import { RestfulLaravelService} from '../../shared/services/restful-laravel.service';


@Injectable()
export class PolicyDocumentsService {

 constructor(public restfulWebService:RestfulLaravelService) { }

  getPolicyType():any{
   return this.restfulWebService.get('admin/list-policy-document-type')
  }

  addEditCompanyPolicy(reqBody):any{
   return this.restfulWebService.post('create-update-policy-document',reqBody)   
  }

  addEditCompanyProceduralDocument(reqBody):any{
    return this.restfulWebService.post('create-update-procedural-document',reqBody)   
   }

   getProceduralDocumentList(reqBody):any{
    return this.restfulWebService.get(`procedural-document-list/${encodeURIComponent(reqBody)}`)
   }

   getUnusedDocumentList():any{
    return this.restfulWebService.get(`policy-unused-list`)
   }

  addEditCompanyContracts(reqBody){
   return this.restfulWebService.post('create-update-policy-document',reqBody);
  }

  getPolicyList(reqBody):any{
     return this.restfulWebService.get(`list-company-policy-document/${encodeURIComponent(reqBody)}`)
  }

  getUnusedDocumentDetails(reqBody):any{
      return this.restfulWebService.get(`policy-unused-detail/${reqBody}`)
   
  }

// method to get all versions and types from filter data in contract listing.
  getListFilerData():any{
     return this.restfulWebService.get(`list-company-policy-document-filter-data`)
  }


  createcontractSubject(reqBody):any{
  return this.restfulWebService.post('create-policy-subject',reqBody)
  }

  getPolicyDetails(reqBody):any{
    if(reqBody.isHHRContract) return this.restfulWebService.get(`admin/get-policy-document-detail/${reqBody.id}`)
    else return this.restfulWebService.get(`ca-get-policy-detail/${reqBody.id}`)
  }

  getPolicyDetailsforEdit(reqBody):any{
    if(reqBody.isHHRContract) return this.restfulWebService.get(`admin/get-policy-document-detail-with-var/${reqBody.id}`)
    else return this.restfulWebService.get(`get-policy-detail-with-var/${reqBody.id}`)
  }

  getProceduralDocumentDetails(id):any{
     return this.restfulWebService.get(`procedural-document-detail/${id}`)
  }
  

getPolicyVersionList(type,reqId){
    //  return this.restfulWebService.get(`get-contract-version-detail/${reqBody}`)
       let url='';
      if(type=='ca') url=`get-policy-version-detail/${reqId}`;
      if(type=='sa') url=`admin/get-policy-document-version/${reqId}`;
     return this.restfulWebService.get(url)
}

restorePolicyVersion(reqBody){
     return this.restfulWebService.get(`restore-company-policy-document-version/${reqBody}`)
}

compareSubjects(reqBody){
     return this.restfulWebService.get(`compare-company-policy-subject/${reqBody}`)
}

getPDType():any{
  return this.restfulWebService.get(`company-position/${encodeURIComponent(JSON.stringify({"search":''}))}`)
 }

 acceptUnusedDocument(reqBody):any{
  return this.restfulWebService.post(`accept-unused-policy`,reqBody)  
 }

 discardPolicy(reqBody):any{
  return this.restfulWebService.post(`policy-document-discard`,reqBody)  
 }

 downloadPdf(id) {
   if(id.isNative){
        window.open(`${this.restfulWebService.baseUrl}generate-company-policy-document-pdf/${id.companyPolicyDocumentID}`);
   }
   else{
    window.open(`${this.restfulWebService.baseUrl}generate-admin-policy-pdf/${id.policyDocumentID}`);
     
   }
}

}
