import { Component, OnInit,ViewContainerRef } from '@angular/core';
import { PolicyDocumentsService } from '../policy-documents.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { CoreService } from '../../../shared/services/core.service';
// import { CompanyContract } from '../contract';
import { MdDialog,MdDialogRef} from '@angular/material';
import { PreviewDocumentComponent} from '../preview-document/preview-document.component';
import {APPCONFIG } from '../../../config';
import { ConfirmService } from '../../../shared/services/confirm.service';


@Component({
  selector: 'app-policy-detail',
  templateUrl: './policy-detail.component.html',
  styleUrls: ['./policy-detail.component.scss']
})
export class PolicyDetailComponent implements OnInit {
  public count=0;
  public isSubCompany:any;
  public id="";
  public contract={
    contractSubjects:[]
  };
  public policy={
    policySubjects:[]
  };
  public tgl={};
  public isHHRContract=true;
  public addMoreSubjectsBox=false;
  public isProcessing=false;
  public isSaveProcessing=false;
  constructor(public policyDocumentsService:PolicyDocumentsService, public dialog:MdDialog,public confirmService:ConfirmService,
               public router:Router,public activatedRoute:ActivatedRoute,public viewContainerRef:ViewContainerRef,
               public coreService:CoreService) { }

  public newPolicySubject={
    companyPolicyDocumentID:"",
    status:"",
    policySubjects:[]
  }
 

  ngOnInit() {
    this.isSubCompany=JSON.parse(localStorage.getItem('happyhr-userInfo')).isSubCompany;    
    APPCONFIG.heading="Policy details"
    let id=this.activatedRoute.snapshot.params['companyPolicyDocumentID'];
    if(id){
    this.isHHRContract=false;
    this.id=this.activatedRoute.snapshot.params['companyPolicyDocumentID'];
    this.newPolicySubject.companyPolicyDocumentID=this.id;
    }
    else this.id=this.activatedRoute.snapshot.params['policyDocumentID'];
    this.getContractDetails()
  }

  OnEdit(){
    console.log("contract id",this.activatedRoute.snapshot.params['companyPolicyDocumentID'])
    this.router.navigate([`/app/company-admin/policy-documents/update/${this.activatedRoute.snapshot.params['companyPolicyDocumentID']}`]);
  }

  onVersions(){
    this.router.navigate([`/app/company-admin/policy-documents/version-history/${this.activatedRoute.snapshot.params['companyPolicyDocumentID']}`]);    
  }

  onPreview(){
     
      this.policyDocumentsService.downloadPdf(this.policy);
      // let dialogRef=this.dialog.open(PreviewDocumentComponent,{height:'100%',width:'60%'});
      // dialogRef.componentInstance.policy=this.policy;
      // dialogRef.afterClosed().subscribe(result => {
      //   if(result){        
      //   }
      // });      
  }

  addNewSubjects(){
    this.addMoreSubjectsBox=true;
     this.newPolicySubject.policySubjects.push( {
        title:"",
        description:"",
        companyPolicyDocumentSubjectID:"",
        status:"Accepted"
      })
  }

  getContractDetails(){
    this.isProcessing=true;
    let reqBody={isHHRContract:this.isHHRContract,id:this.id};
    // let reqBody=this.activatedRoute.snapshot.params['id'];
       this.policyDocumentsService.getPolicyDetails(reqBody).subscribe(
          d => {
              if (d.code == "200") {
                this.policy=d.data;
                let subjects=d.data.policyDocumentID?d.data.policyDocumentSubjects:d.data.policySubjects;
               for(let c in subjects){
                 let i=0;
                  this.tgl[i]=false;
                  i++;
               }
               this.policy['policySubjects']=subjects;
                this.isProcessing=false;   
              }
              else{    
                this.coreService.notify("Unsuccessful", "Error while getting contracts", 0); 
              } 
          },
          error => {
              this.coreService.notify("Unsuccessful", "Error while getting contracts", 0);
          }
         
      )
  }

 onAddMore(){
   this.count++;
   this.newPolicySubject.policySubjects.push( {
        title:"",
        description:"",
        companyPolicyDocumentSubjectID:"",
         status:"Accepted"
      })
 }

 onDeleteSubjects(i){
   this.confirmService.confirm(this.confirmService.tilte, this.confirmService.message, this.viewContainerRef)
      .subscribe(res => {
        let result = res;
        if (result) {
   this.newPolicySubject.policySubjects.splice(i,1);
      }})
    
 }

 onCancel(){
   this.newPolicySubject.policySubjects=[];
    this.addMoreSubjectsBox=false;
 }

  onSave(isvalid){
    if(isvalid){
      this.isSaveProcessing=true;
       
    this.policyDocumentsService.createcontractSubject(this.newPolicySubject).subscribe(
          d => { 
              if (d.code == "200") {
                this.isSaveProcessing=false;
               this.addMoreSubjectsBox=false;
              //  for(let k of this.newContractSubject.contractSubjects){
              //    this.contract.contractSubjects.push(k);
              //  }
              //  this.newContractSubject.contractSubjects=[];
               this.router.navigate([`/app/company-admin/policy-documents`]);
                this.coreService.notify("Successful", d.message, 1); 
              }
              else{
                this.coreService.notify("Unsuccessful", d.message, 0); 
              } 
          },
          error => {
              this.coreService.notify("Unsuccessful", error.message, 0);
             
          }
         
      )
    }
 }
}
