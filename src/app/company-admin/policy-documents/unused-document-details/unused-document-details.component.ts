import { Component, OnInit } from '@angular/core';
import { PolicyDocumentsService } from '../policy-documents.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { CoreService } from '../../../shared/services/core.service';
import {APPCONFIG } from '../../../config';

@Component({
  selector: 'app-unused-document-details',
  templateUrl: './unused-document-details.component.html',
  styleUrls: ['./unused-document-details.component.scss']
})
export class UnusedDocumentDetailsComponent implements OnInit {
  public companyLogo:any;
  public isProcessing=false;
  public document:any;
  isActionProcessing=false;
  constructor(public policyDocumentsService:PolicyDocumentsService,
    public router:Router,public activatedRoute:ActivatedRoute,
    public coreService:CoreService) { }

  ngOnInit() {
    this.getDetails();
    this.companyLogo = this.coreService.getCompanyLogo();
  }

  getDetails(){
    this.isProcessing=true;
        this.policyDocumentsService.getUnusedDocumentDetails(this.activatedRoute.snapshot.params['id']).subscribe(
          d => {
            if (d.code == "200") {
              this.document=d.data;
              this.isProcessing=false;
            }
            else{
              this.coreService.notify("Unsuccessful", d.message, 0);
              this.isProcessing=false;
            } 
          },
          error => this.coreService.notify("Unsuccessful", error.message, 0), 
      )
  }

  onAcceptDocument(){
    this.isActionProcessing=true;
    let id=this.activatedRoute.snapshot.params['id'];
      let reqBody={
          companyPolicyDocumentID:this.activatedRoute.snapshot.params['id'],
          status:true
      }
       this.SubmitAcceptReject(reqBody);
      
 }
 
 SubmitAcceptReject(reqBody){
      this.policyDocumentsService.acceptUnusedDocument(reqBody).subscribe(
         d => {
           if (d.code == "200") {
             this.coreService.notify("Successful", d.message, 1);                
             this.router.navigate([`/app/company-admin/policy-documents//unused-documents`]);
           }
           else{
             this.coreService.notify("Unsuccessful", d.message, 0);
             this.isActionProcessing=false;
           } 
         },
         error => this.coreService.notify("Unsuccessful", error.message, 0), 
     )
 }

}
