import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UnusedDocumentDetailsComponent } from './unused-document-details.component';

describe('UnusedDocumentDetailsComponent', () => {
  let component: UnusedDocumentDetailsComponent;
  let fixture: ComponentFixture<UnusedDocumentDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UnusedDocumentDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UnusedDocumentDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
