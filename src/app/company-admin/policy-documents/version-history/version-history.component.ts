import { Component, OnInit,ViewContainerRef } from '@angular/core';
import { PolicyDocumentsService } from '../policy-documents.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { CoreService } from '../../../shared/services/core.service';
// import { CompanyContract } from '../contract';
import { MdDialog,MdDialogRef} from '@angular/material';
import { PreviewDocumentComponent} from '../preview-document/preview-document.component';
import { APPCONFIG } from '../../../config';
import { ConfirmService } from '../../../shared/services/confirm.service';


@Component({
  selector: 'app-version-history',
  templateUrl: './version-history.component.html',
  styleUrls: ['./version-history.component.scss']
})
export class VersionHistoryComponent implements OnInit {
  isSubCompany:any;
AppConfig: any;
public versionList=[];
public tabing=false;
public isContractDetailProcessing=false;
public contract={};
public policy={};
public currentTab=1.0;
public isProcessing=false;
  constructor(public policyDocumentsService:PolicyDocumentsService,
               public router:Router,public activatedRoute:ActivatedRoute,
               public coreService:CoreService) { }

  ngOnInit() {
    this.isSubCompany=JSON.parse(localStorage.getItem('happyhr-userInfo')).isSubCompany;            
    // this.activatedRoute.snapshot.params['id'];
    // this.activatedRoute.snapshot.params['ContractId']
    
    APPCONFIG.heading="Policy versions"
    this.getVersionList();
  }
 
 getVersionList(){
   let type="";
   let reqId="";
    if( this.activatedRoute.snapshot.params['companyPolicyDocumentID']) {
      type="ca";
      reqId=this.activatedRoute.snapshot.params['companyPolicyDocumentID'];
    }
    if( this.activatedRoute.snapshot.params['policyDocumentID']){
      type="sa";
      reqId=this.activatedRoute.snapshot.params['policyDocumentID'];      
    } 
    
     this.isProcessing=true;       
     this.policyDocumentsService.getPolicyVersionList(type,reqId).subscribe(
          d => {
              this.isProcessing=false;
              if(d.code == "200"){
              for(let i of d.data){
                if(i.status=='Active'){
                  i['localActive']=true;
                  this.getVersionDetails(i.companyPolicyDocumentID); 
                } 
                else i['localActive']=false;
              }
                this.versionList=d.data;
              }   
              else this.coreService.notify("Unsuccessful",d.message, 0); 
          },
          error =>{
            this.coreService.notify("Unsuccessful", "Error while getting notes", 0);
            this.isProcessing=false
          } 
      )
 }

 getVersionDetails(id){
    this.isContractDetailProcessing=true;
      let reqBody={isHHRContract:false,id:id};
       this.policyDocumentsService.getPolicyDetails(reqBody).subscribe(
          d => {
              if (d.code == "200") {
                this.policy=d.data;
                this.isContractDetailProcessing=false;        
              }
              else this.coreService.notify("Unsuccessful", "Error while getting policy", 0);    
          },
          error =>  this.coreService.notify("Unsuccessful", "Error while getting policy", 0)
      )
 }

onVersionSelect(companyPolicyDocumentID){
    this.versionList.forEach(element => {
      if(element.companyPolicyDocumentID==companyPolicyDocumentID){
        element['localActive']=true;
        this.getVersionDetails(element.companyPolicyDocumentID);
      }
      else  element['localActive']=false;
    });
}

 onRestoreVersion(id){
    this.isProcessing=true;       
     this.policyDocumentsService.restorePolicyVersion(id).subscribe(
          d => {
              this.isProcessing=false;
              if(d.code == "200") {
                this.coreService.notify("Successful",d.message, 1);  
                this.getVersionList();
              }
              else this.coreService.notify("Unsuccessful",d.message, 0); 
          },
          error =>{
            this.coreService.notify("Unsuccessful", "Error while getting policy", 0);
            this.isProcessing=false
          } 
      )
 }

}
