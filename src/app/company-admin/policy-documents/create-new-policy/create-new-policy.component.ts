import { Component, OnInit ,ViewContainerRef} from '@angular/core';
import { MdDialog,MdDialogRef} from '@angular/material';
import { PolicyDocumentsService } from '../policy-documents.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { CoreService } from '../../../shared/services/core.service';
import { Policy } from '../policy';
import { PreviewDocumentComponent} from '../preview-document/preview-document.component';
import {APPCONFIG } from '../../../config';


@Component({
  selector: 'app-create-new-policy',
  templateUrl: './create-new-policy.component.html',
  styleUrls: ['./create-new-policy.component.scss']
})
export class CreateNewPolicyComponent implements OnInit {
  
  public isAddEdit="Create";
  public isPolicyDetailProcessing=false;
  public isAddEditProcessing=false;
  public buttonClicked=false;
  public policyTitle="";
  public isProcessing=false;

// drop down options for Policy type
public dropdropdownListPolicyTypes=[
]

// drop down Setting for Policy type
public dropdownSettingsPolicyTypes={
  singleSelection: true,
  text: "Select policy type"
}
public selectedItemsPolicyTypes=[];
public id="";
public policy={
  
  companyPolicyDocumentID:"",
  policyDocumentID:"",
  policyTypeID:"",
  policyTitle:"",
  description:"",
  version:"",
  title:"",
  source:"",
  status:"Active",
  policySubjects:[
    {
        title:"",
        description:"",
        policySubjectID:"",
        status:"Accepted"
    }
  ]
}
public ddl=true;
  constructor( public dialog:MdDialog,public policyService:PolicyDocumentsService,
               public router:Router,public activatedRoute:ActivatedRoute,
               public coreService:CoreService) { }

  ngOnInit() {
    APPCONFIG.heading="Create policy";
    this.id=this.activatedRoute.snapshot.params['id'];
    if(this.id){
      this.isPolicyDetailProcessing=true;
      this.isAddEdit="Update";
      APPCONFIG.heading="Update policy";
    }
    this.getAllPolicyTypes();
  }

  onChangeStatus(){
    this.policy.status=(this.policy.status=="Active")?"Inactive":"Active"
  }

  onDiscard(){
    this.isAddEditProcessing=true;    
    let reqBody={
       companyPolicyDocumentID:this.id   
    }
     this.policyService.discardPolicy(reqBody).subscribe(
          d => {
              if (d.code == "200") {
                this.coreService.notify("Successful", d.message, 1);
                this.router.navigate([`/app/company-admin/policy-documents`]);
                
              }
              else{
                this.isAddEditProcessing=false;
                this.coreService.notify("Unsuccessful", d.message, 0); 
              } 
          },
          error => {
              this.coreService.notify("Unsuccessful", error.message, 0);
              this.isAddEditProcessing=false;
          }
         
      )
  }

  getAllPolicyTypes(){
     this.isProcessing=true;
     this.policyService.getPolicyType().subscribe(
          d => {
              if (d.code == "200") {
                if(this.id) this.getPolicyDetails();
                this.isProcessing=false;
                this.dropdropdownListPolicyTypes = d.data.filter(function(item){
                  item['itemName']=item.title;
                  item['id']=item.policyDocumentTypeID;
                  return item
                });
              }
              else this.coreService.notify("Unsuccessful", "Error while getting policy types", 0);
          },
          error => this.coreService.notify("Unsuccessful", "Error while getting policy types", 0),
          () => { }
      )
  }

  getPolicyDetails(){
      this.isPolicyDetailProcessing=true;
      let reqBody={isHHRPolicy:false,id:this.activatedRoute.snapshot.params['id']};
       this.policyService.getPolicyDetailsforEdit(reqBody).subscribe(
          d => {
              if (d.code == "200") {
                this.policy=d.data;
                
                this.selectedItemsPolicyTypes=this.dropdropdownListPolicyTypes.filter(function(item){
                  if(item.id==d.data.policyTypeID) return item;
                })
                this.isPolicyDetailProcessing=false;   
              }
              else{  
                this.isPolicyDetailProcessing=false;   
                this.coreService.notify("Unsuccessful", "Error while getting Policy", 0); 
              } 
          },
          error => {
               this.isPolicyDetailProcessing=false;
              this.coreService.notify("Unsuccessful", "Error while getting Policy", 0);  
          }
      )
  }

  onAddMoreSubjects(){
    this.policy.policySubjects.push({policySubjectID:"",title:"",status:"Accepted",description:""})
  }

OnDeleteSubject(i){
   this.policy.policySubjects.splice(i,1);
  
}

 onPreview(){
      let dialogRef=this.dialog.open(PreviewDocumentComponent,{height:'90%',width:'60%'});
      dialogRef.componentInstance.policy=this.policy;
      dialogRef.afterClosed().subscribe(result => {
        if(result){        
        }
      });      
  }


  createPolicy(isvalid){
  if(isvalid){
    this.isAddEditProcessing=true;    
    this.policy.policyTypeID=this.selectedItemsPolicyTypes[0].id;
     this.policyService.addEditCompanyPolicy(this.policy).subscribe(
          d => {
              if (d.code == "200") {
                this.coreService.notify("Successful", d.message, 1);
                this.router.navigate([`/app/company-admin/policy-documents`]);
                this.isAddEditProcessing=false;
              }
              else{
                this.isAddEditProcessing=false;
                this.coreService.notify("Unsuccessful", "Error while getting Policy types", 0); 
              } 
          },
          error => {
              this.coreService.notify("Unsuccessful", "Error while getting Policy types", 0);
              this.isAddEditProcessing=false;
          }
         
      )
   
  }
  }


}
