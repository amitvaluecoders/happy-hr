import { Component, OnInit ,ViewContainerRef} from '@angular/core';
import { MdDialog,MdDialogRef} from '@angular/material';
import { PolicyDocumentsService } from '../policy-documents.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { CoreService } from '../../../shared/services/core.service';
import { Policy } from '../policy';
import { PreviewDocumentComponent} from '../preview-document/preview-document.component';
import {APPCONFIG } from '../../../config';

@Component({
  selector: 'app-procedural-document-detail',
  templateUrl: './procedural-document-detail.component.html',
  styleUrls: ['./procedural-document-detail.component.scss']
})
export class ProceduralDocumentDetailComponent implements OnInit {

  public collapse={};
  public hrToggle=false;
  public toggleFilter: boolean;
  public isDetailProcessing=false;
  public document=false;
  public tgl={};
  selectedItemsPDs=[];
  invitelistCollapse=false;
  ngOnInit() {
    this.getDetails();
    APPCONFIG.heading="Procedural document detail";


  }


  constructor( public dialog:MdDialog,public policyService:PolicyDocumentsService,
    public router:Router,public activatedRoute:ActivatedRoute,
    public coreService:CoreService) { }
  

    getDetails(){
        this.isDetailProcessing=true;
         this.policyService.getProceduralDocumentDetails(this.activatedRoute.snapshot.params['id']).subscribe(
            d => {
                if (d.code == "200") {
                  this.document=d.data;
                  // this.selectedItemsPDs=this.dropdropdownListPDs.filter(function(item){
                  //   if(item.id==d.data.policyTypeID) return item;
                  // })getDetails
                  for(let c in d.data.documentSubjects){
                    let i=0;
                     this.tgl[i]=false;
                     i++;
                  }
                //   this.selectedItemsPDs=d.data.position;
                  this.isDetailProcessing=false;   
                }
                else{  
                  this.isDetailProcessing=false;   
                  this.coreService.notify("Unsuccessful", d.message, 0); 
                } 
            },
            error => {
                 this.isDetailProcessing=false;
                this.coreService.notify("Unsuccessful", error.message, 0);  
            }
        )
    }

    onDownloadPDF(url){
      window.open(url);
    }
}
