import {  ModuleWithProviders  } from '@angular/core';
import {  RouterModule,Routes  } from '@angular/router';

import { ContractsListComponent } from './contracts-list/contracts-list.component';
import { ContractDetailComponent } from './contract-detail/contract-detail.component';
import { VersionHistoryComponent } from './version-history/version-history.component';
import { AcceptRejectChangeComponent } from './accept-reject-change/accept-reject-change.component';
import { ViewOldNewVersionComponent } from './view-old-new-version/view-old-new-version.component';

export const EmployeeContractsPagesRoutes: Routes = [
    {
        path: '',
        canActivate:[],
        children: [
           
            { path: '', component: ContractsListComponent},
            { path: 'detail/:id/:employeeCompanyContractID', component: ContractDetailComponent},
            { path: 'subcontractor-contract-detail/:ContractId/:employeeCompanyContractID', component: ContractDetailComponent},
            { path: 'detail/:id', component: ContractDetailComponent},
            { path: 'version-history/:id/:employeeCompanyContractID', component: VersionHistoryComponent},
            { path: 'version/view/:id', component: ViewOldNewVersionComponent},
       ]
    }
];

export const EmployeeContractsRoutingModule = RouterModule.forChild(EmployeeContractsPagesRoutes);
