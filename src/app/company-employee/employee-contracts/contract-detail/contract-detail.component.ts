import { Component, OnInit } from '@angular/core';
import { APPCONFIG } from '../../../config';
import { ContractEmployeeService } from '../employee-contracts.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { CoreService } from '../../../shared/services/core.service';
import { MdSnackBar, MdDialog, MdDialogRef } from '@angular/material';
import { ResponseNoteComponent } from '../../../shared/components/response-note/response-note.component';

@Component({
  selector: 'app-contract-detail',
  templateUrl: './contract-detail.component.html',
  styleUrls: ['./contract-detail.component.scss']
})
export class ContractDetailComponent implements OnInit {
  public isProcessing=false;
  public unsubscriber:any;
  public isActionProcessing=false;
  public contractType='';
   public contract:any;
   public companyLogo:any;
  public loggedInUser:any;
   public id='';
   public reqBody={};
   public employeeCompanyContractID:any;

   role:string;
  constructor( public contractEmployeeService: ContractEmployeeService,public dialog: MdDialog,
               public router: Router, public activatedRoute: ActivatedRoute,
               public coreService: CoreService) { }

  ngOnInit() {
    this.role = this.coreService.getRole();
    this.companyLogo = this.coreService.getCompanyLogo();    
  this.loggedInUser =  JSON.parse(localStorage.getItem('happyhr-userInfo'));
    APPCONFIG.heading="Contract detail";
      this.contractType="";
    this.id="";
    if( this.activatedRoute.snapshot.params['id']) {
      this.contractType="ce";
      this.id=this.activatedRoute.snapshot.params['id'];
      this.employeeCompanyContractID = this.activatedRoute.snapshot.params['employeeCompanyContractID'];

    }
    if( this.activatedRoute.snapshot.params['ContractId']){
      this.contractType="ca";
      this.id=this.activatedRoute.snapshot.params['ContractId']; 
      this.employeeCompanyContractID = this.activatedRoute.snapshot.params['employeeCompanyContractID'];     
    } 
  
    this.getContractDetails();
  }
    getContractDetails(){
      this.isProcessing=true;
      let isEmployeeContract=true;
      let reqBody={};
      if(this.contractType=='ca'){isEmployeeContract=false;
        reqBody={isHHRContract:isEmployeeContract,id:this.id,
          employeeCompanyContractID:this.employeeCompanyContractID};
      }
      if(this.contractType =='ce'){isEmployeeContract=true;
         reqBody={isHHRContract:isEmployeeContract,id:this.id,
          employeeCompanyContractID:this.employeeCompanyContractID};
      }
      this.unsubscriber=this.contractEmployeeService.getContractDetails(reqBody).subscribe(
        d => {
          if (d.code == "200") {
            this.contract=d.data;
            this.isProcessing=false;
          }
          else{
            this.coreService.notify("Unsuccessful", d.message, 0);
            this.isProcessing=false;
          } 
        },
        error => this.coreService.notify("Unsuccessful", error.message, 0), 
    )
  
   
    }

    onViewVersions(){
      if(this.role &&  this.role == 'companyContractor'){
        this.router.navigate([`/app/sub-contractor/contracts/version-history/${this.activatedRoute.snapshot.params['ContractId']}/${this.activatedRoute.snapshot.params['employeeCompanyContractID']}`]);  
      }
      else this.router.navigate([`/app/employee/contracts/version-history/${this.activatedRoute.snapshot.params['id']}/${this.activatedRoute.snapshot.params['employeeCompanyContractID']}`]);  
    }

    // onAcceptContract(){
    //    this.isActionProcessing=true;
    //    let id=this.activatedRoute.snapshot.params['id'];
    //      let reqBody={
    //         "employeeUserID":this.loggedInUser.userID,
    //         "contractID":id,
    //         "status":"Accepted"
    //       }
    //       this.SubmitAcceptRejectContract(reqBody);
         
    // }

    onAccept(c){
        this.isActionProcessing = true;
        //if(this.activatedRoute.snapshot.params['employeeCompanyContractID']){
          let id=this.activatedRoute.snapshot.params['employeeCompanyContractID'];
          if(this.activatedRoute.snapshot.params['employeeCompanyContractID']=='null'){
                this.acceptContract();        
          }
          else{
          let body= {"employeeCompanyContractID":id,
                      "employeeUserID" : this.loggedInUser.userID,
                      "status":"Accept"}
          this.contractEmployeeService.acceptOrReject(body).subscribe(
            d => {
              if (d.code == "200") {
                this.coreService.notify("Successful", d.message, 1);   
                this.isActionProcessing = false;
                
                if(this.role &&  this.role == 'companyContractor') this.router.navigate([`/app/sub-contractor/contracts`]);
                else this.router.navigate([`/app/employee/contracts`]);
  
                }
              else{
                this.coreService.notify("Unsuccessful", d.message, 0);
                this.isActionProcessing=false;
              } 
            },
            error => this.coreService.notify("Unsuccessful", error.message, 0), 
        )
      
      }
    }
    
    acceptContract(){
      let contract=this.activatedRoute.snapshot.params['id'];
      let body= {"contractID":contract,
      "employeeUserID" : this.loggedInUser.userID,
      "status":"Accept"}
        this.contractEmployeeService.acceptContract(body).subscribe(
        d => {
                if (d.code == "200") {
                    this.coreService.notify("Successful", d.message, 1);   
                    this.isActionProcessing = false;
                    if(this.role &&  this.role == 'companyContractor') this.router.navigate([`/app/sub-contractor/contracts`]);
                    else this.router.navigate([`/app/employee/contracts`]);

                    }
                  else{
                    this.coreService.notify("Unsuccessful", d.message, 0);
                    this.isActionProcessing=false;
                  } 
                },
                error => this.coreService.notify("Unsuccessful", error.message, 0), 
              )
    }

    
    
    onRejectContract(){
        this.isActionProcessing=false;
       let id=this.activatedRoute.snapshot.params['employeeCompanyContractID'];
        if(id!='null'){     
           let dialogRef = this.dialog.open(ResponseNoteComponent);  
          let tempResponse={
            url:"employee/employee-reject-contract",
            note:{
              "employeeCompanyContractID" : id,
              "status":"Rejected",
              responseNote:""
            }
          }
            dialogRef.componentInstance.noteDetails=tempResponse;
            dialogRef.afterClosed().subscribe(result => {
             if(result){
                if(this.role &&  this.role == 'companyContractor') this.router.navigate([`/app/sub-contractor/contracts`]);
                 else this.router.navigate([`/app/employee/contracts`]);   
             }
             else this.isActionProcessing=false;
            });
     }
      else{
          this.contractReject();
      }
    
    }
    contractReject(){
      let contract=this.activatedRoute.snapshot.params['id'];
      let dialogRef = this.dialog.open(ResponseNoteComponent);  
      let tempResponse={
        url:"employee/employee-accept-contract",
        note:{
          "contractID" : contract,
          "status":"Rejected",
          responseNote:""
        }
      }
        dialogRef.componentInstance.noteDetails=tempResponse;
        dialogRef.afterClosed().subscribe(result => {
         if(result){
            if(this.role &&  this.role == 'companyContractor') this.router.navigate([`/app/sub-contractor/contracts`]);
             else this.router.navigate([`/app/employee/contracts`]);   
         }
         else this.isActionProcessing=false;
        });


    }

    onViewChangedLog(){
       if(this.role &&  this.role == 'companyContractor') this.router.navigate([`/app/sub-contractor/contracts/version/view/${this.id}`]);
        else this.router.navigate([`/app/employee/contracts/version/view/${this.id}`]);   
    }
}
