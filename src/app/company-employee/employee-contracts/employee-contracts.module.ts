import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { QuillModule } from 'ngx-quill';
import { EmployeeContractsRoutingModule } from './employee-contracts.routing';
import { ContractsListComponent } from './contracts-list/contracts-list.component';
import { ContractDetailComponent } from './contract-detail/contract-detail.component';
import { VersionHistoryComponent } from './version-history/version-history.component';
import { AcceptRejectChangeComponent } from './accept-reject-change/accept-reject-change.component';
import { ViewOldNewVersionComponent } from './view-old-new-version/view-old-new-version.component';
import { ContractEmployeeService } from './employee-contracts.service';
import { FormsModule } from '@angular/forms';
import { DataTableModule } from 'angular2-datatable';
import { SharedModule } from '../../shared/shared.module';
import { MaterialModule } from '@angular/material';

@NgModule({
  imports: [
    FormsModule,
    DataTableModule,
    SharedModule,
    MaterialModule,
    CommonModule,
    EmployeeContractsRoutingModule,
    QuillModule
  ],
  providers:[ContractEmployeeService],
  declarations: [ContractsListComponent, ContractDetailComponent, VersionHistoryComponent, AcceptRejectChangeComponent, ViewOldNewVersionComponent]
})
export class EmployeeContractsModule { }
