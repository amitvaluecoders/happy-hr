import { Component, OnInit } from '@angular/core';
import { MdSnackBar, MdDialog, MdDialogRef } from '@angular/material';
import { APPCONFIG } from '../../../config';
import { ContractEmployeeService } from '../employee-contracts.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { CoreService } from '../../../shared/services/core.service';

@Component({
  selector: 'app-view-old-new-version',
  templateUrl: './view-old-new-version.component.html',
  styleUrls: ['./view-old-new-version.component.scss']
})
export class ViewOldNewVersionComponent implements OnInit {
  public role:string;
  public isProcessing=false;
  public contracts:any;
  public loggedInUser:any;
  public isActionProcessing=false;
  constructor( public contractEmployeeService: ContractEmployeeService,public dialog: MdDialog,
               public router: Router, public activatedRoute: ActivatedRoute,
               public coreService: CoreService) { }

  ngOnInit() {
    this.role = this.coreService.getRole();
    APPCONFIG.heading="Accept/reject for happy HR contracts";
    this.getContractComparisonDetails();
      this.loggedInUser =  JSON.parse(localStorage.getItem('happyhr-userInfo'));

  }

     getContractComparisonDetails(){
       this.isProcessing=true;
         this.contractEmployeeService.getContractComparisonDetails(this.activatedRoute.snapshot.params['id']).subscribe(
            d => {
              if (d.code == "200") {
                  this.contracts=d.data;
                 this.isProcessing=false;       
              }
              else{
                this.coreService.notify("Unsuccessful", d.message, 0);
                this.isProcessing=false;
              } 
            },
            error => this.coreService.notify("Unsuccessful", error.message, 0), 
        )
    }

    onAccept(){
       this.isActionProcessing=true;
       let id=this.activatedRoute.snapshot.params['id'];
         let reqBody={
            "employeeUserID":this.loggedInUser.userID,
            "contractID":id,
            "status":"Accepted"
          }
          this.SubmitAcceptRejectContract(reqBody);
         
    }
    
    SubmitAcceptRejectContract(reqBody){
         this.contractEmployeeService.acceptContract(reqBody).subscribe(
            d => {
              if (d.code == "200") {
                this.coreService.notify("Successful", d.message, 1);
                if(this.role &&  this.role == 'companyContractor') this.router.navigate([`/app/sub-contractor/contracts`]);
                else this.router.navigate([`/app/employee/contracts`]);                 
              }
              else{
                this.coreService.notify("Unsuccessful", d.message, 0);
                this.isActionProcessing=false;
              } 
            },
            error => this.coreService.notify("Unsuccessful", error.message, 0), 
        )
    }

}
