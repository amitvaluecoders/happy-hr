import { Component, OnInit } from '@angular/core';
import { APPCONFIG } from '../../../config';
import { ContractEmployeeService } from '../employee-contracts.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { CoreService } from '../../../shared/services/core.service';
import { MdSnackBar, MdDialog, MdDialogRef } from '@angular/material';


@Component({
  selector: 'app-contracts-list',
  templateUrl: './contracts-list.component.html',
  styleUrls: ['./contracts-list.component.scss']
})
export class ContractsListComponent implements OnInit {
  public unsubscriber:any;
  public collapse={};
  public versionSearch = '';
  public isApplyBtnClicked = false;
  public hrToggle=false;
  role:String;
  public loggedInUser:any;
  public toggleFilter : boolean;
  public contracts = [];
  public isProcessing = false;
  public activityStatus = [];
  public isAcceptOrReject = false;
    tempReqBody = {
    "search" : "",
    "filter" : {
      "versions" : [],
      "status" : [
        {
          title : "Current",
          value : "Accepted",
          isChecked : false
        },
        {
          title : "Old",
          value : "Old",
          isChecked:false        
        }
      ]
    },
    
  }
    reqBody = {
    "search" : "",
    "filter" : {
      "versions" : {},
      "status" : {}
    }
  }

  
  constructor( public contractEmployeeService: ContractEmployeeService,public dialog: MdDialog,
               public router: Router, public activatedRoute: ActivatedRoute,
               public coreService: CoreService) { }

  ngOnInit() {
      this.role = this.coreService.getRole();
    if (this.role == "companyEmployee") this.getActivitiesWithStatusEmp();      
      this.loggedInUser =  JSON.parse(localStorage.getItem('happyhr-userInfo'));
    APPCONFIG.heading="List of contracts";
    this.getAllContracts();
  }

 onSearch(){
    this.unsubscriber.unsubscribe();
    this.reqBody.search=encodeURIComponent(this.tempReqBody.search);
    this.getAllContracts();
  }
    onApplyFilters(){
      this.reqBody.filter.versions={};
    for(let i of this.tempReqBody.filter.versions){
      if(i.isChecked) this.reqBody.filter.versions[i.title]=true;
    }
    this.reqBody.filter.status={};
    for(let i of this.tempReqBody.filter.status){
      if(i.isChecked) this.reqBody.filter.status[i.title]=true;
    }
    this.getAllContracts();
  }


  getAllContracts(){
        this.isProcessing=true;
        this.unsubscriber=this.contractEmployeeService.getAllContracts(JSON.stringify(this.reqBody)).subscribe(
          d => {
            if (d.code == "200") {
              this.contracts=d.data;
               if(!this.tempReqBody.filter.versions.length) {
                 d.data.filter(item=>{
                    this.tempReqBody.filter.versions.push({isChecked:false,title:item.version});
                  })
               }
              this.isProcessing=false;
            }
            else{
              this.coreService.notify("Unsuccessful", d.message, 0);
              this.isProcessing=false;
            } 
          },
          error => this.coreService.notify("Unsuccessful", error.message, 0), 
      )
  }

  // onContractDetails(c){
  // 
  // }
  onContractDetails(c){
  if(this.role &&  this.role == 'companyContractor'){
  c.pdfUrl? window.open(c.pdfUrl):this.router.navigate([`/app/sub-contractor/contracts/subcontractor-contract-detail/${c.companyContractID}/${c.employeeCompanyContract}`]);  
  }
  else this.router.navigate([`/app/employee/contracts/detail/${c.companyContractID}/${c.employeeCompanyContract}`]);  
  }


  // onAccept(c){

  //   this.isAcceptOrReject = true;
  //   let body= {"employeeCompanyContractID":c.employeeCompanyContract,
  //               "employeeUserID" : this.loggedInUser.userID,
  //               "status":"Accept"}
  //   this.contractEmployeeService.AcceptOrReject(body).subscribe(
  //     d => {
  //       if (d.code == "200") {
  //         this.coreService.notify("Successful", d.message, 1);   
  //         this.isAcceptOrReject = false;
  //         this.getAllContracts()
  //         }
  //       else{
  //         this.coreService.notify("Unsuccessful", d.message, 0);
  //         this.isAcceptOrReject=false;
  //       } 
  //     },
  //     error => this.coreService.notify("Unsuccessful", error.message, 0), 
  // )
  // }

  // onReject(c){

  //   this.isAcceptOrReject = true;
  //   let body= {"employeeCompanyContractID":c.employeeCompanyContract,
  //               "employeeUserID" : this.loggedInUser.userID,
  //               "status":"Accept"}
  //   this.contractEmployeeService.AcceptOrReject(body).subscribe(
  //     d => {
  //       if (d.code == "200") {
  //         this.coreService.notify("Successful", d.message, 1);   
  //         this.isAcceptOrReject = false;
  //         this.getAllContracts()
  //         }
  //       else{
  //         this.coreService.notify("Unsuccessful", d.message, 0);
  //         this.isAcceptOrReject=false;
  //       } 
  //     },
  //     error => this.coreService.notify("Unsuccessful", error.message, 0), 
  // )
  // }

  getActivitiesWithStatusEmp() {
    //this.activityStatusProcessing = true;
    this.contractEmployeeService.getAllNotificationStatusEmployee().subscribe(
      res => {
        if (res.code == 200) {
          this.activityStatus = res.data;
        } //else return this.coreService.notify("Unsuccessful", res.message, 0);
      },
      err => {
        if (err.code == 400) {
          //return this.coreService.notify("Unsuccessful", err.message, 0);
        }
        //this.coreService.notify("Unsuccessful", "Internal server error.", 0);
      }
    );
  }



  


}
