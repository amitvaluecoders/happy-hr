import { Injectable } from '@angular/core';
import { RestfulLaravelService} from '../../shared/services/restful-laravel.service';

@Injectable()
export class ContractEmployeeService {

  public empActivityData=[];
  constructor(public restfulWebService:RestfulLaravelService) { }

// method to get listing of all  development plans.
   getAllContracts(reqBody):any{
   return this.restfulWebService.get(`employee/employee-contract-list/${reqBody}`)
  }

  // getContractDetails(reqBody):any{
  //  return this.restfulWebService.get(`employee/employee-contract-detail/${reqBody}`)
  // }
   getContractDetails(reqBody):any{ 
     if(reqBody.employeeCompanyContractID!='null'){
      return this.restfulWebService.get(`employee/employee-contract-detail/${reqBody.id}/${reqBody.employeeCompanyContractID}`)    
     }
     else{
      return this.restfulWebService.get(`employee/employee-contract-detail/${reqBody.id}`)
     } 
  }
  // getContractDetailsCa(reqBody):any{
  //   // if(reqBody.isHHRContract) return this.restfulWebService.get(`get-contract-detail/${reqBody.id}`)
  //   // else  
  //   return this.restfulWebService.get(`employee/employee-contract-detail/${reqBody.id}`)
  // }


  acceptOrReject(reqBody):any{
    return this.restfulWebService.post('employee/employee-accept-reject-contract',reqBody)
  }
  


  getContractVersionList(contractType,reqId){
  let url='';
  if(contractType=='ce') url=`employee/employee-contract-version-detail/${reqId}`;
  if(contractType=='ca') url=`get-contract-version-detail/${reqId}`;
     return this.restfulWebService.get(url)
}
  acceptContract(reqBody):any{
    return this.restfulWebService.post(`employee/employee-accept-contract`,reqBody)
  }

  getContractComparisonDetails(id):any{
    return this.restfulWebService.get(`employee/compare-contract-version/${id}`)
  }

  getAllNotificationStatusEmployee(){
    return this.restfulWebService.get(`employee/get-employee-profile-completeness-detail`);
}

setActivitiesWithStatusEmp(data){
      this.empActivityData=data;
}
getActivitiesWithStatusEmp(){
  return this.empActivityData;
}


}
