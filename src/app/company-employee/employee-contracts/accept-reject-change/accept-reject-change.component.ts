import { Component, OnInit } from '@angular/core';
import { APPCONFIG } from '../../../config';

@Component({
  selector: 'app-accept-reject-change',
  templateUrl: './accept-reject-change.component.html',
  styleUrls: ['./accept-reject-change.component.scss']
})
export class AcceptRejectChangeComponent implements OnInit {

  constructor() { }

  ngOnInit() {
     APPCONFIG.heading="Accept/Reject for contracts";
  }

}
