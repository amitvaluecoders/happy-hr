import { Component, OnInit } from '@angular/core';
import { APPCONFIG } from '../../../config';
import { ContractEmployeeService } from '../employee-contracts.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { CoreService } from '../../../shared/services/core.service';
import { MdSnackBar, MdDialog, MdDialogRef } from '@angular/material';

@Component({
  selector: 'app-version-history',
  templateUrl: './version-history.component.html',
  styleUrls: ['./version-history.component.scss']
})
export class VersionHistoryComponent implements OnInit {

role:string;
AppConfig: any;
public versionList=[];
public tabing=false;
public contractType='';
public isContractDetailProcessing=false;
public contract={};
public currentTab=1.0;
public isProcessing=false;
   constructor( public contractEmployeeService: ContractEmployeeService,public dialog: MdDialog,
               public router: Router, public activatedRoute: ActivatedRoute,
               public coreService: CoreService) { }

  ngOnInit() {
    this.role = this.coreService.getRole();
    this.activatedRoute.snapshot.params['id'];
    this.activatedRoute.snapshot.params['ContractId']
    
    APPCONFIG.heading="Contract versions"
    this.getVersionList();
  }
 
 getVersionList(){
   this.contractType="";
   let reqId="";
    if( this.activatedRoute.snapshot.params['id']) {
      this.contractType="ce";
      reqId=this.activatedRoute.snapshot.params['id'];
    }
    if( this.activatedRoute.snapshot.params['ContractId']){
      this.contractType="ca";
      reqId=this.activatedRoute.snapshot.params['ContractId'];

    } 
    
     this.isProcessing=true;       
     this.contractEmployeeService.getContractVersionList(this.contractType,reqId).subscribe(
          d => {
              if(d.code == "200"){
              for(let i of d.data){
                if(i.status=='Active'){
                  i['localActive']=true;
                  this.getVersionDetails(i.companyContractID); 
                } 
                else i['localActive']=false;
              }
                this.versionList=d.data;
                console.log(this.versionList);
              }   
              else this.coreService.notify("Unsuccessful",d.message, 0); 
          },
          error =>{
            this.coreService.notify("Unsuccessful", "Error while getting notes", 0);
            this.isProcessing=false
          } 
      )
 }

 getVersionDetails(companyContractID){
    this.isContractDetailProcessing=true;
        let isEmployeeContract=true;
        if(this.contractType=='ca')isEmployeeContract=false;
      let reqBody={isHHRContract:isEmployeeContract,id:companyContractID, employeeCompanyContractID:this.activatedRoute.snapshot.params['employeeCompanyContractID']};
       this.contractEmployeeService.getContractDetails(reqBody).subscribe(
          d => {
              if (d.code == "200") {
                this.contract=d.data;
                this.isProcessing=false;                
                this.isContractDetailProcessing=false;        
              }
              else this.coreService.notify("Unsuccessful", "Error while getting contracts", 0);    
          },
          error =>  this.coreService.notify("Unsuccessful", "Error while getting contracts", 0)
      )
 }

onVersionSelect(companyContractID){
  console.log(this.versionList)
    this.versionList.forEach(element => {
      if(element.companyContractID==companyContractID){
        element['localActive']=true;
        console.log("cccc",element.companyContractID)
        this.getVersionDetails(element.companyContractID);
      }
      else  element['localActive']=false;
    });
}

//  onRestoreVersion(id){
//     this.isProcessing=true;       
//      this.contractsService.restoreContractVersion(id).subscribe(
//           d => {
//               this.isProcessing=false;
//               if(d.code == "200") {
//                 this.coreService.notify("Successful",d.message, 1);  
//                 this.getVersionList();
//               }
//               else this.coreService.notify("Unsuccessful",d.message, 0); 
//           },
//           error =>{
//             this.coreService.notify("Unsuccessful", "Error while getting notes", 0);
//             this.isProcessing=false
//           } 
//       )
//  }


}
