import { Component, OnInit } from '@angular/core';
import { PipEmployeeService } from '../pip-employee.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { CoreService } from '../../../shared/services/core.service';
import {APPCONFIG } from '../../../config';
import { MdSnackBar, MdDialog, MdDialogRef } from '@angular/material';
import { ResponseNoteComponent }  from '../../../shared/components/response-note/response-note.component';
import  { PipEmployeeResultComponent } from '../pip-employee-result/pip-employee-result.component';


@Component({
  selector: 'app-pip-employee-view',
  templateUrl: './pip-employee-view.component.html',
  styleUrls: ['./pip-employee-view.component.scss']
})
export class PipEmployeeViewComponent implements OnInit {

  public progress; 
  public originalPIPToggle=[];
  public isActionProcessing=false;
  public isButtonClicked=false;
  public isCreateProcessing=false;
  public isProcessing=false;
  public plan={
    result:'',
    employeeNotes:[]
  };
  public planOld={};

  constructor( public pipService: PipEmployeeService,public dialog: MdDialog,
               public router: Router, public activatedRoute: ActivatedRoute,
               public coreService: CoreService) { }

  ngOnInit() {
    this.progressBarSteps(2,66);
     this.getPipDetails();
     APPCONFIG.heading = "Performance improvement plan details"; // setting heading on header.
  }

// method to setup up progress bar information.
  progressBarSteps(currentStep,progressPercent) {
      this.progress={
          progress_percent_value:progressPercent, //progress percent
          total_steps:3,
          current_step:currentStep,
          circle_container_width:25, //circle container width in percent
          steps:[
               {num:1,data:"Acceptance"},
               {num:2,data:"Training"},
               {num:3,data:"Result"},          
          ]    
       }
       this.progress.steps[currentStep-1]['active']='true';         
  }

  onTime(time){return new Date(time);}


// method to get details of development plan
     getPipDetails() {
     this.isProcessing = true;
     this.pipService.getPipDetails(this.activatedRoute.snapshot.params['id']).subscribe(
          d => {
            if (d.code == "200") {
              this.plan = d.data;
              if(d.data.oldPip)this.planOld=d.data.oldPip.reverse();
              for(let c in this.planOld){this.originalPIPToggle[c]=false}
              let currentStep = 1;
              let progressPercent = 33;
              if (this.plan['trainingNotes'].length) {
                currentStep = 2;
                progressPercent = 66;
              }
              if (this.plan['assessmentNotes'].length) {
                currentStep = 3;
                progressPercent = 100;
              }
              this.progressBarSteps(currentStep, progressPercent);
              this.isProcessing = false;
            }
            else this.coreService.notify("Unsuccessful", d.message, 0);
          },
          error => this.coreService.notify("Unsuccessful", "Error while getting development details", 0),
          () => { }
      )
  }

  // method to show response note pop up.
  onResponseNote(plan){
          let dialogRef = this.dialog.open(ResponseNoteComponent);  
          let tempResponse={
            url:"employee/pip-acceptance",
            note:{
              pipID:plan.pipID,
              action:"Note",
              responseNote:""
            }
          }
            dialogRef.componentInstance.noteDetails=tempResponse;
            dialogRef.afterClosed().subscribe(result => {
             if(result) this.router.navigate([`/app/employee/activites`]);
            });   
    }

  // method to open  re-schedule pop up
  // onReSchedule(planID){
  //       let dialogRef = this.dialog.open(RescheduleDevelopmentPlanComponent,{height:'50%'});  
  //         let tempResponse={
  //           url:"employee/development-plan-acceptance",
  //           reSchedule:{
  //             developmentPlanID:developmentPlanID,
  //             action:"Reschedule",
  //             proposedTrainingTime:""
  //           }
  //         }
  //           dialogRef.componentInstance.reScheduleDetails=tempResponse;
  //           dialogRef.afterClosed().subscribe(result => {
  //            if(result) this.router.navigate([`/app/employee`]);
  //           });   
  // }

  // method on accept development plan 
    onAccept(plan) {
      this.isActionProcessing=true;
      let reqBody = {
        reSchedule: {
          pipID: plan.pipID,
          action: "Accept"
        }
      };
       this.pipService.employeeResponsePIP(reqBody).subscribe(
          d => {
            if (d.code == "200") {
              this.coreService.notify("Successful", d.message, 1);
              this.router.navigate([`/app/employee/activites`]);
            }
            else this.coreService.notify("Unsuccessful", d.message, 0);
          },
          error => this.coreService.notify("Unsuccessful", error.message, 0),
          () => { this.isActionProcessing = false;}
      )
  }

    onResultDetails(plan) {
      if (plan.result != 'Pass') {
        let dialogRef = this.dialog.open(PipEmployeeResultComponent);
        dialogRef.componentInstance.result = plan.resultDetail;
        dialogRef.afterClosed().subscribe(result => {
        });
      }
    }


}
