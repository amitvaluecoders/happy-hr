import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PipEmployeeViewComponent } from './pip-employee-view.component';

describe('PipEmployeeViewComponent', () => {
  let component: PipEmployeeViewComponent;
  let fixture: ComponentFixture<PipEmployeeViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PipEmployeeViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PipEmployeeViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
