import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PipEmployeeViewComponent } from './pip-employee-view/pip-employee-view.component';
import { EmployeePIPRoutingModule } from './pip-employee-routing';
import { PipEmployeeService } from './pip-employee.service';
import { FormsModule } from '@angular/forms';
import { MaterialModule} from '@angular/material';
import { SharedModule }  from '../../shared/shared.module';
import { PipEmployeeResultComponent } from './pip-employee-result/pip-employee-result.component';


@NgModule({
  imports: [
    FormsModule,
    SharedModule,
    MaterialModule,
    CommonModule,
    EmployeePIPRoutingModule
  ],
  declarations: [PipEmployeeViewComponent, PipEmployeeResultComponent],
  providers:[PipEmployeeService],
  entryComponents:[PipEmployeeResultComponent]
})
export class PipEmployeeModule { }
