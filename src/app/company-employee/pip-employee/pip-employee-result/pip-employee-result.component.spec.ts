import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PipEmployeeResultComponent } from './pip-employee-result.component';

describe('PipEmployeeResultComponent', () => {
  let component: PipEmployeeResultComponent;
  let fixture: ComponentFixture<PipEmployeeResultComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PipEmployeeResultComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PipEmployeeResultComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
