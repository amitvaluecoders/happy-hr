import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-pip-employee-result',
  templateUrl: './pip-employee-result.component.html',
  styleUrls: ['./pip-employee-result.component.scss']
})
export class PipEmployeeResultComponent implements OnInit {
 public result:any;
  public disciplinaryMeeting:any;

  constructor() { }

  ngOnInit() {
     this.disciplinaryMeeting=this.result;
  }

}
