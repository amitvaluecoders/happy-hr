import {  ModuleWithProviders  } from '@angular/core';
import {  RouterModule,Routes  } from '@angular/router';
import { PipEmployeeViewComponent } from './pip-employee-view/pip-employee-view.component';

export const EmployeePIPPagesRoutes: Routes = [
    {
        path: '',
        canActivate:[],
        children: [
            { path: 'view/:id', component:PipEmployeeViewComponent },
        ]
    }
];

export const EmployeePIPRoutingModule = RouterModule.forChild(EmployeePIPPagesRoutes);
