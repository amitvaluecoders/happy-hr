import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EmployeeGuardService } from './employee_guard.service';
import {  MaterialModule } from '@angular/material';
import { CompanyEmployeePagesRoutingModule } from './company-employee.routing';
import { FormsModule } from '@angular/forms';
import { ChangePasswordComponent } from './change-password/change-password.component';
// import { EmployeeProfileComponent } from './employee-profile/employee-profile.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    MaterialModule,
    CompanyEmployeePagesRoutingModule
  ],
  declarations: [ChangePasswordComponent],
  providers:[EmployeeGuardService]
})
export class CompanyEmployeeModule { }
