import { ModuleWithProviders } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { GrievanceDetailComponent } from './grievance-detail/grievance-detail.component';
import { ReportGrievanceComponent } from './report-grievance/report-grievance.component';
import { FirstMeetingComponent } from './first-meeting/first-meeting.component';
import { SecondMeetingEmployeeGrievanceAgainstComponent } from './second-meeting-employee-grievance-against/second-meeting-employee-grievance-against.component';
import { ThirdMeetingWithBothComponent } from './third-meeting-with-both/third-meeting-with-both.component';
import { ResultComponent } from './result/result.component';

export const EmployeeGrievancePagesRoutes: Routes = [
    { path: '', component: ReportGrievanceComponent },
    { path: 'detail/:id', component: GrievanceDetailComponent },
    { path: 'first-meeting/:id', component: FirstMeetingComponent },
    { path: 'second-meeting-grievance-against/:id', component: SecondMeetingEmployeeGrievanceAgainstComponent },
    { path: 'third-meeting-with-both/:id', component: ThirdMeetingWithBothComponent },
    { path: 'result/:id', component: ResultComponent }
];

export const EmployeeGrievanceRoutingModule = RouterModule.forChild(EmployeeGrievancePagesRoutes);
