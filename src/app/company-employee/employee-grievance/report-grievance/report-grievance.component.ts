import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CoreService } from '../../../shared/services/core.service';
import { APPCONFIG } from '../../../config';
import { IDatePickerConfig } from 'ng2-date-picker';
import { EmployeeGrievanceService } from "../employee-grievance.service";
import * as moment from "moment";

@Component({
  selector: 'app-report-grievance',
  templateUrl: './report-grievance.component.html',
  styleUrls: ['./report-grievance.component.scss']
})
export class ReportGrievanceComponent implements OnInit {
  progress;
  employeeList = [];
  managersList = [];
  selectedManagers = [];
  selectedAboutPerson = [];
  selectedWitness = [];

  multiSelectSettings = {};
  singleSelectSettings = {};
  config: IDatePickerConfig = {};
  report = { title: '', notes: '', incidentDateTime: '', managers: [], aboutPerson: [], witness: [], evidence: [] }
  isUploading: boolean;
  isProcessing: boolean;
  _validFileExtensions = [".jpg", ".jpeg", ".png", ".pdf", ".doc", ".docx"];
  incidentDateTime;

  constructor(public employeeService: EmployeeGrievanceService, public coreService: CoreService,
    private router: Router) {
    this.config.locale = "en";
    this.config.format = "YYYY-MM-DD hh:mm A";
    this.config.showMultipleYearsNavigation = true;
    this.config.weekDayFormat = 'dd';
    this.config.disableKeypress = true;
    this.config.monthFormat = "MMM YYYY";
    this.config.max = moment();
  }

  ngOnInit() {
    this.progressBarSteps();
    APPCONFIG.heading = "Report grievance";

    this.getEmployeeList();
    this.getManagersList();
    this.multiSelectSettings = {
      singleSelection: false,
      text: "Select manager",
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      enableSearchFilter: true,
      classes: "myclass"
    };

    this.singleSelectSettings = {
      singleSelection: true,
      text: "Select user",
      enableSearchFilter: true,
      classes: "myclass"
    };

    $('angular2-multiselect:eq(0) .list-area ul').addClass('listItems');
    $('angular2-multiselect:eq(1) .list-area ul').addClass('inviteListItems');
    $('angular2-multiselect:eq(2) .list-area ul').addClass('inviteListItems2');
  }

  progressBarSteps() {
    this.progress = {
      progress_percent_value: 20,
      total_steps: 5,
      current_step: 1,
      circle_container_width: 20,
      steps: [
        { num: 1, data: 'Report grievance', active: true },
        { num: 2, data: 'Meeting with complainer' },
        { num: 3, data: 'Meeting with grievance against' },
        { num: 4, data: 'Meeting with both parties' },
        { num: 5, data: 'Result' }
      ]
    }
  }

  getEmployeeList() {
    this.employeeService.getAllEmpolyee().subscribe(
      res => {
        if (res.code == 200) {
          this.employeeList = res.data;
          this.employeeList.filter((item, i) => { item['itemName'] = item.name; item['id'] = item.userId; });
          setTimeout(() => {
            this.employeeList.filter(function (item, i) {
              $('.inviteListItems li').eq(i).prepend(`<img src="${item.imageUrl}" alt= />`);
              $('.inviteListItems2 li').eq(i).prepend(`<img src="${item.imageUrl}" alt= />`);
              $('.inviteListItems li').eq(i).children('label').append('<span>' + item.designation + '</span>');
              $('.inviteListItems2 li').eq(i).children('label').append('<span>' + item.designation + '</span>');
            });
          }, 0);
        }
        else return this.coreService.notify("Unsuccessful", res.message, 0);
      },
      err => {
        if (err.code == 400) {
          return this.coreService.notify("Unsuccessful", err.message, 0);
        }
        this.coreService.notify("Unsuccessful", "Error while getting employee.", 0);
      }
    )
  }

  getManagersList() {
    let userInfo = JSON.parse(localStorage.getItem("happyhr-userInfo"));
    
    this.employeeService.getManagers(userInfo.userID).subscribe(
      res => {
        if (res.code == 200) {
          this.managersList = res.data;
          this.managersList.filter((item, i) => { item['itemName'] = item.name; item['id'] = item.userId; });
          setTimeout(() => {
            this.managersList.filter(function (item, i) {
              $('.listItems li').eq(i).prepend(`<img src="${item.imageUrl}" alt= />`);
              $('.listItems li').eq(i).children('label').append('<span>' + item.designation + '</span>');
            });
          }, 0);
        }
        else return this.coreService.notify("Unsuccessful", res.message, 0);
      },
      err => {
        if (err.code == 400) {
          return this.coreService.notify("Unsuccessful", err.message, 0);
        }
        this.coreService.notify("Unsuccessful", "Error while getting employee.", 0);
      }
    )
  }

  uploadEvidence($event) {
    this.isUploading = true;
    const files = $event.target.files || $event.srcElement.files;

    const formData = new FormData();
    for (let i = 0; i < files.length; i++) {
      formData.append('grievanceEvidence[]', files[i]);
      let ext = files[i].name.substring(files[i].name.lastIndexOf('.')).toLowerCase();
      if (this._validFileExtensions.indexOf(ext) == -1) {
        return this.coreService.notify("Unsuccessful", "Invalid file extension.", 0)
      }
    }

    this.employeeService.uploadFile(formData).subscribe(
      res => {
        if (res.code == 200) {
          this.isUploading = false;
          this.report.evidence = res.data.grievanceEvidence;
        }
        else return this.coreService.notify("Unsuccessful", res.message, 0);
      },
      err => {
        if (err.code == 400) {
          return this.coreService.notify("Unsuccessful", err.message, 0);
        }
        this.coreService.notify("Unsuccessful", "Error while uploading document.", 0);
      });
  }

  reportGrievance() {
    // if (!this.report.evidence.length) return;
    this.isProcessing = true;
    this.report.aboutPerson = this.selectedAboutPerson.map(i => { return i.id });
    this.report.managers = this.selectedManagers.map(i => { return i.id });
    this.report.witness = this.selectedWitness.map(i => { return i.id });
    this.report.incidentDateTime = this.incidentDateTime && moment(this.incidentDateTime).isValid() ? moment(this.incidentDateTime).format('YYYY-MM-DD HH:mm:ss') : '';
    console.log(this.report);
    this.employeeService.reportGrievance(this.report).subscribe(
      res => {
        this.isProcessing = false;
        if (res.code == 200) {
          this.coreService.notify("Successful", res.message, 1);
          this.router.navigate(['/app/employee/grievance/detail', res.data.grievanceID]);
        }
        else return this.coreService.notify("Unsuccessful", res.message, 0);
      },
      err => {
        this.isProcessing = false;
        if (err.code == 400) {
          return this.coreService.notify("Unsuccessful", err.message, 0);
        }
        this.coreService.notify("Unsuccessful", "Error while submitting report.", 0);
      }
    )
  }

}
