import { Component, OnInit } from '@angular/core';
import { APPCONFIG } from '../../../config';
import { Router, ActivatedRoute } from '@angular/router';
import { CoreService } from '../../../shared/services/core.service';
import { Grievance } from "../../../company-admin/grievance/grievance";
import { EmployeeGrievanceService } from "../employee-grievance.service";
import * as moment from 'moment';

@Component({
  selector: 'app-first-meeting',
  templateUrl: './first-meeting.component.html',
  styleUrls: ['./first-meeting.component.scss']
})
export class FirstMeetingComponent implements OnInit {
  public progress;
  isProcessing: boolean;
  grievance: Grievance;
  noResult = true;

  constructor(public employeeService: EmployeeGrievanceService, public coreService: CoreService, private router: Router,
    private route: ActivatedRoute) { this.grievance = new Grievance; }

  ngOnInit() {
    APPCONFIG.heading = "Initial meeting with complainer";
    this.progressBarSteps();

    if (this.route.snapshot.params['id']) {
      this.grievance.grievanceID = this.route.snapshot.params['id'];
      this.getGrievanceDetail();
    }
  }

  progressBarSteps() {
    this.progress = {
      progress_percent_value: 40, //progress percent
      total_steps: 5,
      current_step: 2,
      circle_container_width: 20, //circle container width in percent
      steps: [
        { num: 1, data: "Report grievance" },
        { num: 2, data: "Meeting with complainer", active: true },
        { num: 3, data: "Meeting with grievance against" },
        { num: 4, data: "Meeting with both parties" },
        { num: 5, data: "Result" }
      ]
    }
  }

  getGrievanceDetail() {
    this.isProcessing = true;
    this.employeeService.getGrievanceDetail(this.grievance.grievanceID).subscribe(
      res => {
        this.isProcessing = false;
        if (res.code == 200) {
          this.grievance = res.data;
          this.noResult = false;
        }
        else return this.coreService.notify("Unsuccessful", res.message, 0);
      },
      err => {
        this.isProcessing = false;
        if (err.code == 400) {
          return this.coreService.notify("Unsuccessful", err.message, 0);
        }
        this.coreService.notify("Unsuccessful", "Error while getting data.", 0)
      }
    );
  }

  formatDateTime(date) {
    return moment(date).isValid() ? moment(date).format('DD MMM, YYYY hh:mm A') : 'Not specified';
  }

}
