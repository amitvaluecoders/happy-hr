import { Injectable } from '@angular/core';
import { RestfulLaravelService } from '../../shared/services';

@Injectable()
export class EmployeeGrievanceService {

  constructor(private restfulWebService: RestfulLaravelService) { }

  getEmployee() {
    return this.restfulWebService.get('show-all-employee');
  }
  getAllEmpolyee(){
    return this.restfulWebService.get('show-all-employee/grievance');    
  }

  getManagers(id) {
    return this.restfulWebService.get(`show-manager-and-ceo/${id}`);
  }

  getGrievanceDetail(id) {
    return this.restfulWebService.get(`employee/employee-grievance-detail/${id}`);
  }

  reportGrievance(reqBody) {
    return this.restfulWebService.post('employee/report-grievance', reqBody);
  }

  uploadFile(formdata) {
    return this.restfulWebService.post('upload-multiple-files', formdata);
  }

  downloadPdf(id) {
    window.open(`${this.restfulWebService.baseUrl}generate-grievance-pdf/${id}`);
  }
}
