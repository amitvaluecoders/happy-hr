import { Component, OnInit } from '@angular/core';
import { APPCONFIG } from '../../../config';
import { Router, ActivatedRoute } from '@angular/router';
import { CoreService } from '../../../shared/services/core.service';
import { Grievance } from "../../../company-admin/grievance/grievance";
import { EmployeeGrievanceService } from "../employee-grievance.service";
import * as moment from 'moment';

@Component({
  selector: 'app-grievance-detail',
  templateUrl: './grievance-detail.component.html',
  styleUrls: ['./grievance-detail.component.scss']
})
export class GrievanceDetailComponent implements OnInit {
  isProcessing: boolean;
  grievance: Grievance;
  noResult = true;

  constructor(public employeeService: EmployeeGrievanceService, public coreService: CoreService, private router: Router,
    private route: ActivatedRoute) { this.grievance = new Grievance; }

  ngOnInit() {
    APPCONFIG.heading = "Grievance detail";

    if (this.route.snapshot.params['id']) {
      this.grievance.grievanceID = this.route.snapshot.params['id'];
      this.getGrievanceDetail();
    }
  }

  getGrievanceDetail() {
    this.isProcessing = true;
    this.employeeService.getGrievanceDetail(this.grievance.grievanceID).subscribe(
      res => {
        this.isProcessing = false;
        if (res.code == 200) {
          this.grievance = res.data;
          this.noResult = false;
        }
        else return this.coreService.notify("Unsuccessful", res.message, 0);
      },
      err => {
        this.isProcessing = false;
        if (err.code == 400) {
          return this.coreService.notify("Unsuccessful", err.message, 0);
        }
        this.coreService.notify("Unsuccessful", "Error while getting data.", 0)
      }
    );
  }

  formatDateTime(date) {
    return moment(date).isValid() ? moment(date).format('DD MMM, YYYY hh:mm A') : 'Not specified';
  }

  downloadPDF(){
    this.employeeService.downloadPdf(this.grievance.grievanceID);
  }

}
