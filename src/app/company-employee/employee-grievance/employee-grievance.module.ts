import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { MaterialModule } from '@angular/material';
import { EmployeeGrievanceRoutingModule } from './employee-grievance.routing';
import { GrievanceDetailComponent } from './grievance-detail/grievance-detail.component';
import { ReportGrievanceComponent } from './report-grievance/report-grievance.component';
import { FirstMeetingComponent } from './first-meeting/first-meeting.component';
import { SecondMeetingEmployeeGrievanceAgainstComponent } from './second-meeting-employee-grievance-against/second-meeting-employee-grievance-against.component';
import { ThirdMeetingWithBothComponent } from './third-meeting-with-both/third-meeting-with-both.component';
import { ResultComponent } from './result/result.component';
import { SharedModule } from "../../shared/shared.module";
import { AngularMultiSelectModule } from 'angular2-multiselect-checkbox-dropdown/angular2-multiselect-dropdown';
import { EmployeeGrievanceService } from "./employee-grievance.service";

@NgModule({
  imports: [
    CommonModule,
    EmployeeGrievanceRoutingModule,
    SharedModule,
    FormsModule,
    MaterialModule,
    AngularMultiSelectModule
  ],
  declarations: [GrievanceDetailComponent, ReportGrievanceComponent, FirstMeetingComponent, SecondMeetingEmployeeGrievanceAgainstComponent, ThirdMeetingWithBothComponent, ResultComponent],
  providers: [EmployeeGrievanceService]
})
export class EmployeeGrievanceModule { }
