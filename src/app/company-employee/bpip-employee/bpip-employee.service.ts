import { Injectable } from '@angular/core';
import { RestfulLaravelService} from '../../shared/services/restful-laravel.service';

@Injectable()
export class BpipEmployeeService {

  constructor(public restfulWebService:RestfulLaravelService) { }

// method to get listing of all  development plans.
   getAllActivitesAndApprovals():any{
   return this.restfulWebService.get('employee/dashboard-active-approval')
  }

//method to get details of develpment plan.
  getPipDetails(reqBody):any{
   return this.restfulWebService.get(`bpip-detail/${reqBody}`)
  }

  //method to send re-schedule request
  employeeResponsePIP(reqBody):any{
   return this.restfulWebService.post(`employee/bpip-acceptance`,reqBody.reSchedule)
  }

}
