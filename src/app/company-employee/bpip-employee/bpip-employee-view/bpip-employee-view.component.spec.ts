import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BpipEmployeeViewComponent } from './bpip-employee-view.component';

describe('BpipEmployeeViewComponent', () => {
  let component: BpipEmployeeViewComponent;
  let fixture: ComponentFixture<BpipEmployeeViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BpipEmployeeViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BpipEmployeeViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
