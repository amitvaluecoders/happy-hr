import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { MaterialModule} from '@angular/material';
import { SharedModule }  from '../../shared/shared.module';
import { BpipEmployeeService } from './bpip-employee.service';
import { BpipEmployeeViewComponent } from './bpip-employee-view/bpip-employee-view.component';
import { EmployeeBPIPRoutingModule } from './bpip-employee.routing';
import { BpipResultDetailComponent } from './bpip-result-detail/bpip-result-detail.component';

@NgModule({
  imports: [
    FormsModule,
    MaterialModule,
    SharedModule,
    CommonModule,
    EmployeeBPIPRoutingModule
  ],
  declarations: [BpipEmployeeViewComponent, BpipResultDetailComponent],
  providers:[BpipEmployeeService],
  entryComponents:[BpipResultDetailComponent]
  
})
export class BpipEmployeeModule { }
