import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-bpip-result-detail',
  templateUrl: './bpip-result-detail.component.html',
  styleUrls: ['./bpip-result-detail.component.scss']
})
export class BpipResultDetailComponent implements OnInit {

  public result:any;
  public disciplinaryMeeting:any;

  constructor() { }

  ngOnInit() {
    this.disciplinaryMeeting=this.result;
  }

}
