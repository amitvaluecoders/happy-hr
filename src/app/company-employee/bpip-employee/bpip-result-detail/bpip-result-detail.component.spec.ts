import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BpipResultDetailComponent } from './bpip-result-detail.component';

describe('BpipResultDetailComponent', () => {
  let component: BpipResultDetailComponent;
  let fixture: ComponentFixture<BpipResultDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BpipResultDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BpipResultDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
