import {  ModuleWithProviders  } from '@angular/core';
import {  RouterModule,Routes  } from '@angular/router';
import { BpipEmployeeViewComponent } from './bpip-employee-view/bpip-employee-view.component';

export const EmployeeBPIPPagesRoutes: Routes = [
    {
        path: '',
        canActivate:[],
        children: [
            { path: 'view/:id', component:BpipEmployeeViewComponent },
        ]
    }
];

export const EmployeeBPIPRoutingModule = RouterModule.forChild(EmployeeBPIPPagesRoutes);
