import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule }  from '@angular/router';
import { EmployeeMessagesComponent } from './employee-messages/employee-messages.component';

export const MessagePagesRoutes: Routes = [
    {
        path: '',
        children: [
            { path: '',  component: EmployeeMessagesComponent },
        ]
    }
];

export const MessagePagesRoutingModule = RouterModule.forChild(MessagePagesRoutes);
