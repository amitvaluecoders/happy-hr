import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../shared/shared.module'; 
import { EmployeeMessagesComponent } from './employee-messages/employee-messages.component';
import { MessagePagesRoutingModule } from './employee-messages.routing';
@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    MessagePagesRoutingModule
  ],
  declarations: [EmployeeMessagesComponent]
})
export class EmployeeMessagesModule { }
