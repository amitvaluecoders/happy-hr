import { Component, OnInit } from '@angular/core';
import { APPCONFIG } from '../../../config';

@Component({
  selector: 'app-resolved-email',
  templateUrl: './resolved-email.component.html',
  styleUrls: ['./resolved-email.component.scss']
})
export class ResolvedEmailComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
