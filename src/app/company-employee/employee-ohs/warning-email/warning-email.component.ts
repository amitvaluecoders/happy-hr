import { Component, OnInit } from '@angular/core';
import { APPCONFIG } from '../../../config';

@Component({
  selector: 'app-warning-email',
  templateUrl: './warning-email.component.html',
  styleUrls: ['./warning-email.component.scss']
})
export class WarningEmailComponent implements OnInit {

  constructor() { }

  ngOnInit() {
     APPCONFIG.heading = "Warning email";
  }

}
