import { RouterModule, Routes } from '@angular/router';
import { ReportIncidentComponent } from './report-incident/report-incident.component';
import { WarningEmailComponent } from './warning-email/warning-email.component';
import { ResolvedEmailComponent } from './resolved-email/resolved-email.component';

export const EmployeeOhsPagesRoutes:Routes=[
    {path:'', component:ReportIncidentComponent},
    {path:'email/warning', component:WarningEmailComponent},
    {path:'email/resolved', component:ResolvedEmailComponent},
];
export const EmployeeOhsRoutingModules=RouterModule.forChild(EmployeeOhsPagesRoutes);
