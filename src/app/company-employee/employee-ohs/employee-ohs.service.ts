import { Injectable } from '@angular/core';
import { RestfulLaravelService } from "../../shared/services/restful-laravel.service";

@Injectable()
export class EmployeeOhsService {

  constructor(private restfulService: RestfulLaravelService) { }

  getEmployees() {
    return this.restfulService.get('show-all-employee');
  }

  uplodDocument(formData) {
    return this.restfulService.post('upload-multiple-files', formData);
  }

  reportIncident(reqBody) {
    return this.restfulService.post('employee/ohs-report', reqBody);
  }
}
