import { NgModule } from '@angular/core';
import { MaterialModule } from '@angular/material';
import { CommonModule } from '@angular/common';
import { ReportIncidentComponent } from './report-incident/report-incident.component';
import { WarningEmailComponent } from './warning-email/warning-email.component';
import { ResolvedEmailComponent } from './resolved-email/resolved-email.component';
import { EmployeeOhsRoutingModules } from './employee-ohs.routing';
import { SharedModule } from '../../shared/shared.module';
import { FormsModule } from '@angular/forms';
import { AngularMultiSelectModule } from 'angular2-multiselect-checkbox-dropdown/angular2-multiselect-dropdown';
import { EmployeeOhsService } from "./employee-ohs.service";

@NgModule({
  imports: [
    CommonModule,
    EmployeeOhsRoutingModules,
    SharedModule,
    FormsModule,
    AngularMultiSelectModule,
    MaterialModule
  ],
  declarations: [ReportIncidentComponent, WarningEmailComponent, ResolvedEmailComponent],
  providers: [EmployeeOhsService]
})
export class EmployeeOhsModule { }
