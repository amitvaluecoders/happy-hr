import { Component, OnInit } from '@angular/core';
import { MdSnackBar, MdDialog, MdDialogRef } from '@angular/material';
import { LeaveApplyPopupComponent } from '../leave-apply-popup/leave-apply-popup.component';
import { APPCONFIG } from '../../../config';
import { EmployeeLeaves } from '../employee-leaves.service';
import { Router } from '@angular/router'; 
import { CoreService } from '../../../shared/services/core.service';
import * as moment from 'moment';
import { icsFormatter }  from './../../../../assets/vendor_js/icsFormatter';

@Component({
  selector: 'app-leave-list',
  templateUrl: './leave-list.component.html',
  styleUrls: ['./leave-list.component.scss']
})
export class LeaveListComponent implements OnInit {
  relPath:any;
  public calendarOptions = {
    height: '10%',
    width:'70%',
    eventColor: '#64c3fb',
    header: {
      left: 'prev,next today',
      center: 'title',
      right: 'month,agendaWeek,agendaDay'
      },
    fixedWeekCount : false,
    defaultDate: new Date(),
    eventLimit: true, 
    editable: true,
    events: [],
    eventRender: function( event, element, view ) { var title = element.find('.fc-title, .fc-list-item-title'); title.html(title.text());},
    eventClick: function(calEvent, jsEvent, view) {
        var calEntry = icsFormatter();

        var title = calEvent.title;
        var place = '';
        var begin = new Date(calEvent.start.toISOString());
        var end = new Date(calEvent.end.toISOString());

        var description = calEvent.title;

        calEntry.addEvent(title,description, place, begin.toUTCString(), begin.toUTCString());
        calEntry.download('vcart', '.ics');
 }
    
  };
  public leaveList = [];
  public leaveTypes = [];
  public toggleFilter = true;
  public isProcessing = false;
  public filter = { 'Approved': false, 'Rejected': false, 'Pending': false, 'Cancelled': false };

  constructor(public router:Router,public employeeService: EmployeeLeaves, public coreService: CoreService, public dialog: MdDialog) { }

  ngOnInit() {
    APPCONFIG.heading = "Leaves";
    this.getLeaveList();
       let role = this.coreService.getRole();
    this.relPath =(role &&  role == 'companyContractor') ?'sub-contractor':'employee';

  }

  onPopUp() {
    let dialogRef = this.dialog.open(LeaveApplyPopupComponent);
    // dialogRef.componentInstance.leaveTypes = this.leaveTypes;
    dialogRef.afterClosed().subscribe(message => {
      if (message) {
        this.getLeaveList();
        return this.coreService.notify("Successful", message, 1);
      }
    });
  }

  getLeaveList() {
    this.isProcessing = true;
    let params = { "filter": { "status": this.filter } };
    this.leaveList = [];
    this.employeeService.getLeaveList(JSON.stringify(params)).subscribe(
      res => {
        if (res.code == 200) {
          if (res.data instanceof Array) {
            this.leaveList = res.data;
            this.leaveList.filter(i => { i['count'] = this.countDays(i.startDate, i.endDate); });
          }
          this.setCalanderData();
          this.isProcessing = false;

        }
        else return this.coreService.notify("Unsuccessful", res.message, 0);
      },
      error => {
        this.isProcessing = false;
        if (error.code == 400) {
          return this.coreService.notify("Unsuccessful", error.message, 0);
        }
        this.coreService.notify("Unsuccessful", "Error while getting leave list.", 0)
      }
    )
  }

  cancelLeave(leave) {
    leave['isUpdating'] = true;
    let reqBody = { "status": "Cancelled", "leavesID": leave.leavesID }
    this.employeeService.cancelLeave(reqBody).subscribe(
      res => {
        leave['isUpdating'] = false;
        if (res.code == 200) {
          this.coreService.notify("Successful", res.message, 1);
          this.getLeaveList();
        }
        else return this.coreService.notify("Unsuccessful", res.message, 0);
      },
      error => {
        leave['isUpdating'] = false;
        if (error.code == 400) {
          return this.coreService.notify("Unsuccessful", error.message, 0);
        }
        this.coreService.notify("Unsuccessful", "Error while getting leave list.", 0)
      }
    )
  }

  isChecked() {
    return this.filter.Approved || this.filter.Cancelled || this.filter.Pending || this.filter.Rejected;
  }

  isAvailable(data) {
    return typeof data == 'object';
  }

  formatDate(date) {
    if (moment(date).isValid()) {
      return moment(date).format('DD MMM YYYY');
    }
  }

  countDays(start, end) {
    return moment(end).diff(moment(start), 'days') + 1;
  }

    setCalanderData(){
      this.calendarOptions.events=[];      
      this.leaveList.filter(d=>{
        let color='#fbe9b1';
        if(d.status=='Cancelled') color='#ffe4c3';
        if(d.status=='Approved') color='#b3f3dd';
        if(d.status=='Rejected') color='#f9d2d2';
        this.calendarOptions.events.push(
          {
        title:d.title,
            start:d.startDate,
            end:d.endDate2?d.endDate2:d.endDate,
            backgroundColor:color,
          }
        )

      })
  }

  onDetails(id){
    let url = `/app/${this.relPath}/leaves/details/${id}`;
    this.router.navigate([url])
  }

}
