import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { MaterialModule } from '@angular/material';
import { LeavesRouterModules } from './employee-leaves.routing';
import { LeaveListComponent } from './leave-list/leave-list.component';
import { LeaveApplyPopupComponent } from './leave-apply-popup/leave-apply-popup.component';
import { LeaveDetailComponent } from './leave-detail/leave-detail.component';
import { EmployeeLeaves } from "./employee-leaves.service";
import { AngularMultiSelectModule } from 'angular2-multiselect-checkbox-dropdown/angular2-multiselect-dropdown';
import { SharedModule } from "../../shared/shared.module";
import { DataTableModule } from "angular2-datatable";

@NgModule({
  imports: [
    CommonModule,
    LeavesRouterModules,
    MaterialModule,
    FormsModule,
    AngularMultiSelectModule,
    SharedModule,
    DataTableModule
  ],
  declarations: [LeaveListComponent, LeaveApplyPopupComponent, LeaveDetailComponent],
  entryComponents: [LeaveApplyPopupComponent],
  providers: [EmployeeLeaves]
})
export class EmployeeLeavesModule { }
