import { Injectable } from '@angular/core';
import { RestfulLaravelService } from '../../shared/services';
import { Http, Headers, Response, RequestOptions } from '@angular/http';


@Injectable()
export class EmployeeLeaves {

    constructor(
        private restfulWebService: RestfulLaravelService) { }

    getLeaveList(params) {
        return this.restfulWebService.get(`employee/leave-list/${params}`);
    }

    getLeaveTypes() {
        return this.restfulWebService.get('get-leave-type');
    }
    getLeaveTypesEmployee(id) {
        return this.restfulWebService.get(`get-leave-type/${id}`);
    }

    getLeaveDetail(id) {
        return this.restfulWebService.get(`employee/leave-detail/${id}`);
    }

    applyLeave(reqBody) {
        return this.restfulWebService.post('employee/apply-leave', reqBody);
    }

    cancelLeave(reqBody) {
        return this.restfulWebService.put('employee/cancelled-leave', reqBody);
    }
}