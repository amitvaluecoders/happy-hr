import { RouterModule, Routes } from '@angular/router';
import { LeaveListComponent } from './leave-list/leave-list.component';
import { LeaveDetailComponent } from './leave-detail/leave-detail.component';
import { LeaveApplyPopupComponent } from './leave-apply-popup/leave-apply-popup.component';
export const routes:Routes=[
    {path:'', component:LeaveListComponent},
    {path:'details/:id', component:LeaveDetailComponent},
    {path: 'apply/:id', component: LeaveApplyPopupComponent}
];
export const LeavesRouterModules=RouterModule.forChild(routes);
