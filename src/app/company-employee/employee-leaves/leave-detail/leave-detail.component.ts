import { Component, OnInit } from '@angular/core';
import { APPCONFIG } from '../../../config';
import { Router, ActivatedRoute } from '@angular/router';
import { EmployeeLeaves } from '../employee-leaves.service';
import { CoreService } from '../../../shared/services/core.service';
import * as moment from "moment";

@Component({
  selector: 'app-leave-detail',
  templateUrl: './leave-detail.component.html',
  styleUrls: ['./leave-detail.component.scss']
})
export class LeaveDetailComponent implements OnInit {
    relPath:any;
  isProcessing: boolean;
  leaveDetail = {
    "leavesID": "", "title": "", "managerNote": "", "leaveTypeID": "",
    "startDate": "", "endDate": "", "comment": "", "status": ""
  };

  constructor(public employeeService: EmployeeLeaves, public coreService: CoreService, public router: Router, public route: ActivatedRoute) { }

  ngOnInit() {
     let role = this.coreService.getRole();
    this.relPath =(role &&  role == 'companyContractor') ?'sub-contractor':'employee';
    APPCONFIG.heading = "Leave detail";
    if (this.route.snapshot.params['id']) {
      this.leaveDetail.leavesID = this.route.snapshot.params['id'];
      this.getLeaveDetail();
    }
  }

  getLeaveDetail() {
    this.isProcessing = true;
    this.employeeService.getLeaveDetail(this.leaveDetail.leavesID).subscribe(
      res => {
        this.isProcessing = false;
        if (res.code == 200) {
          console.log(res);
          this.leaveDetail = res.data;
        }
        else return this.coreService.notify('Unsuccessful', res.message, 0);
      },
      error => {
        this.isProcessing = false;
        if (error.code == 400) {
          return this.coreService.notify('Unsuccessful', error.message, 0);
        }
        this.coreService.notify('Unsuccessful', "Error while getting leave detail", 0);
      }
    )
  }

  formatDate(date) {
    if (moment(date).isValid()) {
      return moment(date).format('DD MMM YYYY');
    }
  }

}
