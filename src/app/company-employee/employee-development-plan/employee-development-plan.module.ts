import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EmployeeDevelopmentPlanService } from './employee-development-plan.service';
import { RestfulLaravelService} from '../../shared/services/restful-laravel.service';
import {EmployeeDevelopmentRoutingModule} from './employee-development-plan.routing';
import { EmployeeDevelopmentPlanReAssessmentViewComponent } from './employee-development-plan-re-assessment-view/employee-development-plan-re-assessment-view.component';
import { EmployeeDevelopmentPlanViewComponent } from './employee-development-plan-view/employee-development-plan-view.component';
import { ResponseNoteComponent }  from '../../shared/components/response-note/response-note.component';
import { RescheduleDevelopmentPlanComponent } from './employee-development-plan-view/reschedule-development-plan/reschedule-development-plan.component';
import { NguiDatetimePickerModule } from '@ngui/datetime-picker';
import { FormsModule } from '@angular/forms';
import { MaterialModule} from '@angular/material';
import { SharedModule }  from '../../shared/shared.module';


@NgModule({
  imports: [
    FormsModule,
    NguiDatetimePickerModule,
    MaterialModule,
    SharedModule,
    EmployeeDevelopmentRoutingModule,
    CommonModule
  ],
  declarations: [EmployeeDevelopmentPlanViewComponent, EmployeeDevelopmentPlanReAssessmentViewComponent, RescheduleDevelopmentPlanComponent],
  providers:[EmployeeDevelopmentPlanService,RestfulLaravelService,ResponseNoteComponent],
  entryComponents:[RescheduleDevelopmentPlanComponent]
})
export class EmployeeDevelopmentPLanModule { }
