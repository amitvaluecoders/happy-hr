import { Injectable } from '@angular/core';
import { RestfulLaravelService} from '../../shared/services/restful-laravel.service';

@Injectable()
export class EmployeeDevelopmentPlanService {

  constructor(public restfulWebService:RestfulLaravelService) { }

// method to get listing of all  development plans.
   getAllActivitesAndApprovals():any{
   return this.restfulWebService.get('employee/dashboard-active-approval')
  }

//method to get details of develpment plan.
  getDevelopmentPlanDetails(reqBody):any{
   return this.restfulWebService.get(`development-plan-detail/${reqBody}`)
  }

  //method to send re-schedule request
  sendReScheduleRequest(reqBody):any{
   return this.restfulWebService.post(`employee/development-plan-acceptance`,reqBody.reSchedule)
  }

}
