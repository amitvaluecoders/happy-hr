import { Component, OnInit } from '@angular/core';
import { EmployeeDevelopmentPlanService } from '../employee-development-plan.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { CoreService } from '../../../shared/services/core.service';
import {APPCONFIG } from '../../../config';
import { MdSnackBar, MdDialog, MdDialogRef } from '@angular/material';
import { ResponseNoteComponent }  from '../../../shared/components/response-note/response-note.component';
import { RescheduleDevelopmentPlanComponent } from './reschedule-development-plan/reschedule-development-plan.component';

@Component({
  selector: 'app-employee-development-plan-view',
  templateUrl: './employee-development-plan-view.component.html',
  styleUrls: ['./employee-development-plan-view.component.scss']
})
export class EmployeeDevelopmentPlanViewComponent implements OnInit {
  
  public progress; 
  public isActionProcessing=false;
  public isButtonClicked=false;
  public isCreateProcessing=false;
  public isProcessing=false;
  public originalPIPToggle =[];
  public developmentPlan={
    result:''
  };
  public developmentPlanOld=[];

  constructor( public developmentPlanService: EmployeeDevelopmentPlanService,public dialog: MdDialog,
               public router: Router, public activatedRoute: ActivatedRoute,
               public coreService: CoreService) { }

  ngOnInit() {
    this.progressBarSteps(2,66);
     this.getDevelopmentPlanDetails();
     APPCONFIG.heading = "Development plan details"; // setting heading on header.
  }

// method to setup up progress bar information.
  progressBarSteps(currentStep,progressPercent) {
      this.progress={
          progress_percent_value:progressPercent, //progress percent
          total_steps:3,
          current_step:currentStep,
          circle_container_width:25, //circle container width in percent
          steps:[
               {num:1,data:"Acceptance"},
               {num:2,data:"Training"},
               {num:3,data:"Result"},          
          ]    
       }
       this.progress.steps[currentStep-1]['active']='true';         
  }

// method to get details of development plan
     getDevelopmentPlanDetails() {
     this.isProcessing = true;
     this.developmentPlanService.getDevelopmentPlanDetails(this.activatedRoute.snapshot.params['id']).subscribe(
          d => {
            if (d.code == "200") {
              this.developmentPlan = d.data;
                if(d.data.oldDevelopment)this.developmentPlanOld=d.data.oldDevelopment.reverse();
              for(let c in this.developmentPlanOld){this.originalPIPToggle[c]=false}
              // this.developmentPlanOld=d.data.oldDevelopment;
              let currentStep = 1;
              let progressPercent = 33;
              if (this.developmentPlan['trainingNote'].length) {
                currentStep = 2;
                progressPercent = 66;
              }
              if (this.developmentPlan['assessmentNote'].length) {
                currentStep = 3;
                progressPercent = 100;
              }
              this.progressBarSteps(currentStep, progressPercent);
              this.isProcessing = false;
            }
            else this.coreService.notify("Unsuccessful", d.message, 0);
          },
          error => this.coreService.notify("Unsuccessful", "Error while getting development details", 0),
          () => { }
      )
  }

  // method to show response note pop up.
  onResponseNote(developmentPlanID){
          let dialogRef = this.dialog.open(ResponseNoteComponent);  
          let tempResponse={
            url:"employee/development-plan-acceptance",
            note:{
              developmentPlanID:developmentPlanID,
              action:"Note",
              responseNote:""
            }
          }
            dialogRef.componentInstance.noteDetails=tempResponse;
            dialogRef.afterClosed().subscribe(result => {
             if(result) this.router.navigate([`/app/employee/activites`]);
            });   
    }

  // method to open  re-schedule pop up
  onReSchedule(developmentPlanID){
        let dialogRef = this.dialog.open(RescheduleDevelopmentPlanComponent,{height:'50%'});  
          let tempResponse={
            url:"employee/development-plan-acceptance",
            reSchedule:{
              developmentPlanID:developmentPlanID,
              action:"Reschedule",
              proposedTrainingTime:""
            }
          }
            dialogRef.componentInstance.reScheduleDetails=tempResponse;
            dialogRef.afterClosed().subscribe(result => {
             if(result) this.router.navigate([`/app/employee/activites`]);
            });   
  }

  onTime(time){return new Date(time);}

  // method on accept development plan 
    onAccept(developmentPlanID) {
      this.isActionProcessing=true;
      let reqBody = {
        reSchedule: {
          developmentPlanID: developmentPlanID,
          action: "Accept"
        }
      };
       this.developmentPlanService.sendReScheduleRequest(reqBody).subscribe(
          d => {
            if (d.code == "200") {
              this.coreService.notify("Successful", d.message, 1);
              this.router.navigate([`/app/employee`]);
            }
            else this.coreService.notify("Unsuccessful", d.message, 0);
          },
          error => this.coreService.notify("Unsuccessful", error.message, 0),
          () => { this.isActionProcessing = false;}
      )
  }

}
