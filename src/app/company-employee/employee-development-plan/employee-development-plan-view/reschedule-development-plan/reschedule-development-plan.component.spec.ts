import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RescheduleDevelopmentPlanComponent } from './reschedule-development-plan.component';

describe('RescheduleDevelopmentPlanComponent', () => {
  let component: RescheduleDevelopmentPlanComponent;
  let fixture: ComponentFixture<RescheduleDevelopmentPlanComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RescheduleDevelopmentPlanComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RescheduleDevelopmentPlanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
