import { Component, OnInit } from '@angular/core';
import { EmployeeDevelopmentPlanService } from '../../employee-development-plan.service';
import { CoreService } from '../../../../shared/services/core.service';
import { MdSnackBar, MdDialog, MdDialogRef } from '@angular/material';
@Component({
  selector: 'app-reschedule-development-plan',
  templateUrl: './reschedule-development-plan.component.html',
  styleUrls: ['./reschedule-development-plan.component.scss']
})
export class RescheduleDevelopmentPlanComponent implements OnInit {
  
  public isProcessing=false;
  public isButtonClicked=false;
  public developmentPlan={
    trainingDate:'',
    trainingTime:''
  }
  public reScheduleDetails={
   reSchedule:{
      developmentPlanID:'',
      action:"Reschedule",
      proposedTrainingTime:""
    }
  };

  constructor(public employeeDevelopmentService:EmployeeDevelopmentPlanService, public coreService: CoreService,
        public dialogRef: MdDialogRef<RescheduleDevelopmentPlanComponent>) { }

  ngOnInit() {
  }


  // method to send re-schedule request.
  onSubmit(isvalid) {
    this.isButtonClicked=true;
    if(isvalid){
      this.isProcessing=true;
      this.reScheduleDetails.reSchedule.proposedTrainingTime=this.developmentPlan.trainingDate.toString()+' '+this.developmentPlan.trainingTime.toString()
       this.employeeDevelopmentService.sendReScheduleRequest(this.reScheduleDetails).subscribe(
          d => {
            if (d.code == "200") {
              this.coreService.notify("Successful", d.message, 1);
              this.isProcessing = false;
              this.dialogRef.close(true);
            }
            else this.coreService.notify("Unsuccessful", d.message, 0);
          },
          error => this.coreService.notify("Unsuccessful", error.message, 0),
          () => { }
      )
    }
  }

}
