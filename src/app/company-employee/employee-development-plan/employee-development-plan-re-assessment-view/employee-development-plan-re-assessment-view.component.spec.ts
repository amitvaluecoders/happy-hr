import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployeeDevelopmentPlanReAssessmentViewComponent } from './employee-development-plan-re-assessment-view.component';

describe('EmployeeDevelopmentPlanReAssessmentViewComponent', () => {
  let component: EmployeeDevelopmentPlanReAssessmentViewComponent;
  let fixture: ComponentFixture<EmployeeDevelopmentPlanReAssessmentViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmployeeDevelopmentPlanReAssessmentViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployeeDevelopmentPlanReAssessmentViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
