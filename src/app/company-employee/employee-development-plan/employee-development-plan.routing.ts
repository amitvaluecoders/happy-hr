import {  ModuleWithProviders  } from '@angular/core';
import {  RouterModule,Routes  } from '@angular/router';
import {EmployeeDevelopmentPlanViewComponent } from './employee-development-plan-view/employee-development-plan-view.component';

export const EmployeeDevelopmentPagesRoutes: Routes = [
    {
        path: '',
        canActivate:[],
        children: [
           
            { path: 'view/:id', component:EmployeeDevelopmentPlanViewComponent },

                        
        ]
    }
];

export const EmployeeDevelopmentRoutingModule = RouterModule.forChild(EmployeeDevelopmentPagesRoutes);
