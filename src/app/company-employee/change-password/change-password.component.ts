import { Component, OnInit } from '@angular/core';
import { RestfulLaravelService } from '../../shared/services/restful-laravel.service'
import { CoreService } from '../../shared/services/core.service'
import { Router } from '@angular/router';
import { APPCONFIG } from '../../config';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss']
})
export class ChangePasswordComponent implements OnInit {
  user={
    oldPassword:'',
    newPassword:'',
    confirmPassword:''
  }
  isProcessingForm = false;

  constructor(public restfulService:RestfulLaravelService,public router:Router,public coreService:CoreService) { }

  ngOnInit() {
    APPCONFIG.heading="Change password";
  }

  onChangePassword(){
    this.isProcessingForm = true;
    let reqBody = {
      oldPassword:this.user.oldPassword,
      newPassword:this.user.newPassword,
      confirmPassword:this.user.confirmPassword,
    }
    this.restfulService.post('update-password',reqBody).subscribe(
      d=>{
        if(d.code == '200'){
          this.coreService.notify("Successful",d.message,1);
          this.router.navigate(['/']);
        }
      },
      err=>{
        this.coreService.notify("Unsuccessful",err.message,0);
        this.isProcessingForm = false;
        
      })

  }
}
