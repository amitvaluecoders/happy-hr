import { Injectable } from '@angular/core';
import { RestfulLaravelService } from '../../shared/services';
import { Http, Headers, Response, RequestOptions } from '@angular/http';


@Injectable()
export class EmployeeTrophy {

    constructor(
        private restfulWebService: RestfulLaravelService) { }

    getTrophyList(params) {
        return this.restfulWebService.get(`employee/list-trophy/${params}`);
    }

    voteForTrophy(reqBody) {
        return this.restfulWebService.post('employee/vote-trophy', reqBody);
    }

    getTrophyDetail(id) {
        return this.restfulWebService.get(`employee/trophy-detail/${id}`);
    }

    viewPreviouslyVoted(id) {
        return this.restfulWebService.get(`employee/view-vote/${id}`);
    }

    getTrophyWon() {
        return this.restfulWebService.get('employee/won-trophy');
    }

    getOnlyEmployee() {
        return this.restfulWebService.get('show-only-employee');
    }

    getOnlyManagers() {
        return this.restfulWebService.get('show-manager-and-ceo');
    }

    getAllEmployees() {
        return this.restfulWebService.get('show-all-employee');
    }
}