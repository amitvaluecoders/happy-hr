import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { MaterialModule } from '@angular/material';
import { TrophyListComponent } from './trophy-list/trophy-list.component';
import { empTrophyRoutingModule } from './emp-trophy.routing';
import { VotingComponent } from './voting/voting.component';
import { ViewPreviouslyVotedComponent } from './view-previously-voted/view-previously-voted.component';
import { TrophyWinComponent, TrophyCertificateView } from './trophy-win/trophy-win.component';
import { EmployeeTrophy } from './emp-trophy.service';
import { DataTableModule } from "angular2-datatable";

@NgModule({
  imports: [
    CommonModule,
    empTrophyRoutingModule,
    FormsModule,
    MaterialModule,
    DataTableModule
  ],
  declarations: [TrophyListComponent, VotingComponent, ViewPreviouslyVotedComponent, TrophyWinComponent, TrophyCertificateView],
  entryComponents: [TrophyCertificateView],
  providers: [EmployeeTrophy,]
})
export class EmpTrophyModule { }
