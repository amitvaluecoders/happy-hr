import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { APPCONFIG } from '../../../config';
import { EmployeeTrophy } from '../emp-trophy.service';
import { CoreService } from '../../../shared/services/core.service';

@Component({
  selector: 'app-trophy-list',
  templateUrl: './trophy-list.component.html',
  styleUrls: ['./trophy-list.component.scss']
})
export class TrophyListComponent implements OnInit {
  public trophyList = [];
  public toggleFilter = true;
  public isProcessing: boolean;
  public searchText = '';
  public filter = { 'Pending': false, 'Voted': false };

  constructor(public employeeService: EmployeeTrophy, public coreService: CoreService, public router: Router) { }

  ngOnInit() {
    APPCONFIG.heading = "Trophy list";
    this.getTrophyList();
  }

  getTrophyList() {
    this.isProcessing = true;
    let params = { "search": encodeURIComponent(this.searchText), "filter": { "status": this.filter } };
    this.trophyList = [];
    this.employeeService.getTrophyList(encodeURIComponent(JSON.stringify(params))).subscribe(
      res => {
        if (res.code == 200) {
          this.isProcessing = false;
          // if (res.data instanceof Array) {
          this.trophyList = res.data;
          // }
        }
        else return this.coreService.notify("Unsuccessful", res.message, 0);
      },
      error => {
        this.isProcessing = false;
        if (error.code == 400) {
          return this.coreService.notify("Unsuccessful", error.message, 0);
        }
        this.coreService.notify("Unsuccessful", "Error while getting trophy list.", 0)
      }
    )
  }

  isChecked() {
    return this.filter.Pending || this.filter.Voted
  }

}
