import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { APPCONFIG } from '../../../config';
import { EmployeeTrophy } from '../emp-trophy.service';
import { CoreService } from '../../../shared/services/core.service';

@Component({
  selector: 'app-voting',
  templateUrl: './voting.component.html',
  styleUrls: ['./voting.component.scss']
})
export class VotingComponent implements OnInit {
  public trophy = {
    "trophyID": "", "title": "", "reason": "", "awardFor": "", "votingCloseTime": "",
    "icon": "", "nominatedUserIDs": [], "askEmployeeToNominate": ""
  };
  public vote = { "reason": "", "nomineeID": "", "trophyID": "", "askEmployeeToNominate": "" };
  isProcessing = false;
  isProcessingVote = false;
  isProcessingEmp = false;

  constructor(public employeeService: EmployeeTrophy, public coreService: CoreService, public router: Router, public route: ActivatedRoute) { }

  ngOnInit() {
    APPCONFIG.heading = "Employee voting";
    if (this.route.snapshot.params['id']) {
      this.vote.trophyID = this.route.snapshot.params['id']
      this.getTrophyDetail();
    }
  }

  getTrophyDetail() {
    this.isProcessing = true;
    this.employeeService.getTrophyDetail(this.vote.trophyID).subscribe(
      res => {
        this.isProcessing = false;
        if (res.code == 200) {
          this.trophy = res.data;
          if (this.trophy.askEmployeeToNominate == "Yes") this.getEmployees();
        }
      },
      error => {
        this.isProcessing = false;
        // this.coreService.notify("Error", "Error while getting trophy detail.", 0)
      }
    )
  }

  getSelectedEmployee() {
    if (this.trophy.awardFor == "Employee") {
      return this.employeeService.getOnlyEmployee();
    } else if (this.trophy.awardFor == "Manager") {
      return this.employeeService.getOnlyManagers();
    } else {
      return this.employeeService.getAllEmployees();
    }
  }

  getEmployees() {
    this.isProcessingEmp = true;
    this.getSelectedEmployee().subscribe(
      res => {
        this.isProcessingEmp = false;
        if (res.code == 200) {
          if (res.data instanceof Array) {
            this.trophy.nominatedUserIDs = res.data.map(i => {
              return {
                "NominieeMember": {
                  imageUrl: i.imageUrl, name: i.name, position: i.designation, userID: i.userId
                }, "nomineeID": i.userId
              }
            });
          }
        }
        else return this.coreService.notify("Unsuccessful", res.message, 0);
      },
      error => {
        this.isProcessingEmp = false;
        // if (error.code == 400) {
        //   return this.coreService.notify("Unsuccessful", error.message, 0);
        // }
        // this.coreService.notify("Unsuccessful", "Error while getting leave list.", 0)
      }
    )
  }

  onVote(emp) {
    this.vote.nomineeID = emp.nomineeID;
  }

  submitVote() {
    if (this.vote.nomineeID == "") return;

    this.vote.askEmployeeToNominate = this.trophy.askEmployeeToNominate;
    this.isProcessingVote = true;
    this.employeeService.voteForTrophy(this.vote).subscribe(
      res => {
        if (res.code == 200) {
          this.isProcessingVote = false;
          this.coreService.notify("Successful", res.message, 1);
          this.router.navigate(['/app/employee/trophy']);
        }
      },
      error => {
        this.isProcessingVote = false;
        if (error.code == 400) {
          return this.coreService.notify("Unsuccessful", error.message, 0);
        }
        this.coreService.notify("Unsuccessful", "Error while submitting vote.", 0);
      }
    )
  }
}
