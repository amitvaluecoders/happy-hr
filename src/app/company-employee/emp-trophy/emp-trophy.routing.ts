import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TrophyListComponent } from './trophy-list/trophy-list.component';
import { VotingComponent } from './voting/voting.component';
import { ViewPreviouslyVotedComponent } from './view-previously-voted/view-previously-voted.component';
import { TrophyWinComponent } from './trophy-win/trophy-win.component';
export const routes:Routes=[
    {path:'', component:TrophyListComponent},
    {path:'voting/:id', component:VotingComponent},
    {path:'view-voted/:id', component:ViewPreviouslyVotedComponent},
    {path:'win-trophy', component:TrophyWinComponent}
];

export const empTrophyRoutingModule= RouterModule.forChild(routes);