import { Component, OnInit } from '@angular/core';
import { MdDialog, MdDialogRef } from '@angular/material';
import { APPCONFIG } from '../../../config';
import { EmployeeTrophy } from '../emp-trophy.service';
import { CoreService } from '../../../shared/services/core.service';
import * as moment from 'moment';

@Component({
  selector: 'app-trophy-win',
  templateUrl: './trophy-win.component.html',
  styleUrls: ['./trophy-win.component.scss']
})
export class TrophyWinComponent implements OnInit {
  trophyList = [];
  isProcessing: boolean;

  constructor(public dialog: MdDialog, public employeeService: EmployeeTrophy, public coreService: CoreService) { }

  ngOnInit() {
    APPCONFIG.heading = "Trophy I won";
    this.getTrophyWon();
  }

  getTrophyWon() {
    this.isProcessing = true;
    this.employeeService.getTrophyWon().subscribe(
      res => {
        if (res.code == 200) {
          this.isProcessing = false;
          this.trophyList = res.data;
        }
      },
      error => {
        this.isProcessing = false;
        this.trophyList = [];
        if (error.code == 400) {
          return this.coreService.notify("Error", error.message, 0);
        }
        this.coreService.notify("Error", "Error while getting trophy list.", 0)
      }
    )
  }

  onTrophyDetails(trophy) {
    let dialogRef = this.dialog.open(TrophyCertificateView);
    dialogRef.componentInstance.trophy = trophy;
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
      }
    });
  }

  formatDate(date) {
    if (!date) return '--';
    return moment(date).format('DD MMM YYYY');
  }

}
@Component({
  selector: 'Trophy-certificate-view',
  template: `
    <div class="TrophyCertOuter" [style.background-image]="'url(' + trophy?.icon + ')'" >
      <div class="TrophyCertInner">
        <div class="TrophyCertImage" [style.background-image]="'url(' + trophy?.icon + ')'">
          <i class="fa fa-trophy"></i>
        </div>
        <div class="TrophyCertInfo">
          <h4>Certificate of Achievement</h4>
          <h5>Trophy Award certificate</h5>
          <p>Award category</p>
          <h4>{{trophy?.title}}</h4>
          <p>This certificate is presented to</p>
          <h2>{{trophy?.user?.name}}</h2>
          <p>For excellent performance in {{trophy?.title}} trophy competition.</p>
          <p class="dateRow">Date: {{formatDate(trophy.date)}}</p>
        </div>
      </div>
    </div>   
    
    `,
  styles: [
    `
      .TrophyCertOuter {max-width: 700px;min-width: 700px;background-size: cover;background-repeat: repeat;padding:15px;float:left;}
      .TrophyCertInner{width:100%;float:left;display:table;}
      .TrophyCertImage, .TrophyCertInfo{display: table-cell;vertical-align: middle;width:60%;text-align:center;}
      .TrophyCertImage{background-size: cover;background-repeat: repeat;width: 40%;box-shadow: inset 0px 0px 60px #000000;-webkit-box-shadow: inset 0px 0px 60px #000000;-moz-box-shadow: inset 0px 0px 60px #000000;-o-box-shadow: inset 0px 0px 60px #000000;-ms-box-shadow: inset 0px 0px 60px #000000;}
      .TrophyCertImage i.fa-trophy{font-size:200px;color:#ffffff;opacity: 0.8;}
      .TrophyCertInfo{background:#ffffff;padding: 20px;}
      .TrophyCertInfo h2{text-transform: uppercase;text-decoration: underline;font-size: 30px;font-weight: 600;}     
      .TrophyCertInfo h4{margin: 0 0 15px 0;border: none;padding: 0;font-weight: 600;}
       .TrophyCertInfo h5{margin: 0 0 15px 0;font-weight: 600;font-size: 18px;padding: 0;border: none;}
      .TrophyCertInfo p{ margin: 5px 0;line-height: 20px;}
      .TrophyCertInfo p.dateRow{margin: 20px 0;}
      `
  ]
})
export class TrophyCertificateView {
  trophy ={
    title:'',
    icon:'',
    date:'',
    user:{
      name:''
    }
  };

  constructor() {

  }

  formatDate(date) {
    if (!date) return '';
    return moment(date).format('DD MMM YYYY');
  }
}