import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { APPCONFIG } from '../../../config';
import { EmployeeTrophy } from '../emp-trophy.service';
import { CoreService } from '../../../shared/services/core.service';

@Component({
  selector: 'app-view-previously-voted',
  templateUrl: './view-previously-voted.component.html',
  styleUrls: ['./view-previously-voted.component.scss']
})
export class ViewPreviouslyVotedComponent implements OnInit {
  public trophy = {};
  public trophyID: string;
  isProcessing: boolean;

  constructor(public employeeService: EmployeeTrophy, public coreService: CoreService, public router: Router, public route: ActivatedRoute) { }

  ngOnInit() {
    APPCONFIG.heading = "Employee voting";
    if (this.route.snapshot.params['id']) {
      this.trophyID = this.route.snapshot.params['id']
      this.getTrophyDetail();
    }
  }

  getTrophyDetail() {
    this.isProcessing = true;
    this.employeeService.viewPreviouslyVoted(this.trophyID).subscribe(
      res => {
        if (res.code == 200) {
          this.isProcessing = false;
          this.trophy = res.data;
        }
      },
      error => {
        this.isProcessing = false;
        this.coreService.notify("Error", "Error while getting trophy detail.", 0)
      }
    )
  }

}
