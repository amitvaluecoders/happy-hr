import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { MdDialog, MdDialogRef } from '@angular/material';
import { CoreService } from '../../../shared/services/core.service';
import { APPCONFIG } from '../../../config';
import { DashboardEmployeeService } from '../employee-dashboard.service';
import { CreateShoutoutPopupComponent } from './create-shoutout-popup/create-shoutout-popup.component';
import { icsFormatter } from './../../../../assets/vendor_js/icsFormatter';
import { TrophyCommentsPopupComponent } from '../../../company-admin/company-admin-dashboard/trophy-comments-popup/trophy-comments-popup.component';


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  public dashBoard: any;
  isProcessing = false;
  public calendarOptions = {
    height: '10%',
    width: '70%',
    eventColor: '#64c3fb',
    header: {
      left: 'prev,next today',
      center: 'title',
      right: 'month,agendaWeek,agendaDay'
    },
    fixedWeekCount: false,
    defaultDate: new Date(),
    eventLimit: true,
    editable: true,
    events: [],
    eventRender: function (event, element, view) { var title = element.find('.fc-title, .fc-list-item-title'); title.html(title.text()); },
    eventClick: function (calEvent, jsEvent, view) {
      var calEntry = icsFormatter();

      var title = calEvent.description.substr(0, calEvent.description.search('\n'));
      var place = '';
      var begin = new Date(calEvent.start.utc()).toString();
      var end = new Date(calEvent.end.utc()).toString();

      var description = calEvent.description.substr(calEvent.description.search('\n') + 2, calEvent.description.length);

      calEntry.addEvent(title, description, place, begin, end);
      calEntry.download('vcart', '.ics');
    }

  };

  constructor(public router: Router, public activatedRoute: ActivatedRoute,
    public dashboardService: DashboardEmployeeService,
    public coreService: CoreService, public dialog: MdDialog) { }

  ngOnInit() {
    APPCONFIG.heading = 'Dashboard';
    this.getDashBoardDeatils();
  }

  onActivitiesAndApprovals() {
    this.router.navigate([`/app/employee/activites`]);
  }

     addCommentPopup(data){
      let dialogRef = this.dialog.open(TrophyCommentsPopupComponent, { width: '600px' });
      dialogRef.componentInstance.comments =   data.comments ; 
      dialogRef.componentInstance.reqBody =  {shoutoutID:data.shoutoutID,note:''} ; 
          dialogRef.afterClosed().subscribe(result => {
             let temp = this.coreService.getTrophyAndShoutouts();
            if(temp && temp.length>0)this.dashBoard.trophyAndShoutouts = this.coreService.getTrophyAndShoutouts(); 
        });
    }

  getDashBoardDeatils() {
    this.isProcessing = true;
    this.dashboardService.getDashboardDetails().subscribe(
      res => {
        if (res.code == 200) {
          this.dashBoard = res.data;
          this.isProcessing = false;
          this.dashBoard['trophyAndShoutouts'] = this.setTrophyANdSoutouts(res.data) || [];

          this.setCalanderData();
        }
        else return this.coreService.notify("Unsuccessful", res.message, 0);
      },
      error => {
        this.isProcessing = false;
        this.coreService.notify("Unsuccessful", error.message, 0);
      }
    )
  }

  setTrophyANdSoutouts(data) {
    let tempArray = data.shoutout.concat(data.trophy);
    tempArray.sort(function (a: any, b: any) {
      let firstValue: any = new Date(b.insertTime);
      let secondValue: any = new Date(a.insertTime);
      return firstValue - secondValue;
    });
    return tempArray;
  }


  onPopUp() {
    let dialogRef = this.dialog.open(CreateShoutoutPopupComponent);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {

      }
    });
  }

  setCalanderData() {
    this.calendarOptions.events = [];
    this.dashBoard.calendarData.filter(d => {
      let users = '';
      for (let c of d.employeeUserNames) { users += users ? "," + c : c }
      let color = 'yellow';
      // if(d.status=='Cancelled') color='orange';
      // if(d.status=='Approved') color='lightgreen';
      // if(d.status=='Rejected') color='red';
      this.calendarOptions.events.push(
        {
          title: "<div class='myEvent'>" + d.title + "<div class='footer clearfix'> <span class='user'>" + users + "</span> <span class='abb'>" + d.moduleAbbreviation + "</span></div>  </div>",
          start: d.startTime,
          end: d.endTime,
          backgroundColor: '#ebebebe',
          description: d.title + '\n Users:' + users
        }
      )
      // if(d.reminderTime){
      //   this.calendarOptions.events.push({
      //     title:"<div class='myEvent'>"+" Reminder "+" Performance appraisal "+ "<div class='footer clearfix'> <span class='user'>" +users+"</span> <span class='abb'>" +d.moduleAbbreviation+"</span></div>  </div>",
      //     start:d.reminderTime,
      //     end:d.endTime,
      //     backgroundColor:'#ebebebe',
      //     description : d.title + '\n Users:' +  users
      //   })
      // }




    })
  }

}
