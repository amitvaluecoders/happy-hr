import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-create-shoutout-popup',
  templateUrl: './create-shoutout-popup.component.html',
  styleUrls: ['./create-shoutout-popup.component.scss']
})
export class CreateShoutoutPopupComponent implements OnInit {
public selectEmployee=[];
public shoutoutEmploye:any;
  constructor() { }

  ngOnInit() {
        this.selectEmployee=[
          {id:0,title:"Employee 1"},
          {id:1,title:"Employee 2"},
          {id:2,title:"Employee 3"}
        ]

  }

}
