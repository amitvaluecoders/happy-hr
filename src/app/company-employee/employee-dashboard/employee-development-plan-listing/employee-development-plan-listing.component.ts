import { Component, OnInit } from '@angular/core';
import { EmployeeDevelopmentPlanService} from '../../employee-development-plan/employee-development-plan.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { CoreService } from '../../../shared/services/core.service';
import {APPCONFIG } from '../../../config'; 
import { MdDialogRef, MdDialog, MdSnackBar } from '@angular/material';



@Component({
  selector: 'app-employee-development-plan-listing',
  templateUrl: './employee-development-plan-listing.component.html',
  styleUrls: ['./employee-development-plan-listing.component.scss']
})

export class EmployeeDevelopmentPlanListingComponent implements OnInit {

 public toggleFilter:boolean;
  public employmentList;
  public positionList;
  public isProcessing=false;
  public developmentPlans=[];

  constructor( public developmentPlanService: EmployeeDevelopmentPlanService,
               public router: Router, public activatedRoute: ActivatedRoute,
               public coreService: CoreService,public dialog: MdDialog) { }

  ngOnInit() {
    // this.getDevelopmentPlans();
    APPCONFIG.heading="Activities And Approvals";    
  }

// method to get list of development plans.
  // getDevelopmentPlans() {
  //    this.isProcessing = true;
  //    this.developmentPlanService.getAllDevelopmentPlans().subscribe(
  //         d => {
  //             if (d.code == "200") {
  //               this.developmentPlans=d.data; 
  //               this.isProcessing = false;    
  //             }
  //             else this.coreService.notify("Unsuccessful", d.message, 0);
  //         },
  //         error => this.coreService.notify("Unsuccessful", error.message, 0),
  //         () => { }
  //     )
  // }

// when user clicks on any action button. 
  OnAction(d) {
    let url =`/app/employee/development-plan/view/${d.developmentPlanID}`;
    // if(d.mode=='isCopy') url =`/app/employee-admin/development-plan/re-assessment-view/${d.developmentPlanID}`;
    this.router.navigate([url]);
  }

 
}
