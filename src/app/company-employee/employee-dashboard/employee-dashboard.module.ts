import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EmployeeActivitiesAndApprovalComponent,EmployeeKPINote } from './employee-activities-and-approval/employee-activities-and-approval.component';
import { EmployeeDevelopmentPlanListingComponent } from './employee-development-plan-listing/employee-development-plan-listing.component';
import { DashBoardRoutingModule } from './employee-dashboard.routing';
import { EmployeeDevelopmentPlanService } from '../employee-development-plan/employee-development-plan.service';
import { PipEmployeeService } from '../pip-employee/pip-employee.service';
import { RestfulLaravelService } from '../../shared/services/restful-laravel.service';
import { shoutOutPopUpClass } from './employee-activities-and-approval/employee-activities-and-approval.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { BpipEmployeeService } from '../bpip-employee/bpip-employee.service';
import { ReschedulePlanPopupComponent } from './employee-activities-and-approval/reschedule-plan-popup/reschedule-plan-popup.component';
import { FormsModule } from '@angular/forms';
import { DataTableModule } from 'angular2-datatable';
import { SharedModule } from '../../shared/shared.module';
import { MaterialModule } from '@angular/material';
import { RestfulwebService } from '../../shared/services/restfulweb.service';
import { EmployeeTrainingDayService } from "../employee-training-day/employee-training-day.service";
import { CreateShoutoutPopupComponent } from './dashboard/create-shoutout-popup/create-shoutout-popup.component';
import { DashboardEmployeeService } from './employee-dashboard.service'; 
import { WarningPopup } from './employee-activities-and-approval/employee-activities-and-approval.component';
import { EmployeeIntroduction } from '../induction/induction.service';

@NgModule({
  imports: [
    FormsModule,
    SharedModule,
    DashBoardRoutingModule,
    CommonModule,
    MaterialModule,
    DataTableModule
  ],
  providers: [EmployeeIntroduction,EmployeeDevelopmentPlanService,DashboardEmployeeService, RestfulwebService, RestfulLaravelService, PipEmployeeService, BpipEmployeeService, EmployeeTrainingDayService],
  declarations: [EmployeeActivitiesAndApprovalComponent,EmployeeKPINote, EmployeeDevelopmentPlanListingComponent, shoutOutPopUpClass, DashboardComponent, ReschedulePlanPopupComponent,WarningPopup, CreateShoutoutPopupComponent],
  entryComponents: [EmployeeKPINote,shoutOutPopUpClass,WarningPopup, ReschedulePlanPopupComponent, CreateShoutoutPopupComponent]
})
export class EmployeeDashboardModule { }