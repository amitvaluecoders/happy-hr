import { Injectable } from '@angular/core';
import { RestfulLaravelService} from '../../shared/services/restful-laravel.service';

@Injectable()
export class DashboardEmployeeService {

  constructor(public restfulWebService:RestfulLaravelService) { }

   getDashboardDetails():any{
   return this.restfulWebService.get(`employee/dashboard`)
  }


}
