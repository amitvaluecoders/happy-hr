import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployeeActivitiesAndApprovalComponent } from './employee-activities-and-approval.component';

describe('EmployeeActivitiesAndApprovalComponent', () => {
  let component: EmployeeActivitiesAndApprovalComponent;
  let fixture: ComponentFixture<EmployeeActivitiesAndApprovalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmployeeActivitiesAndApprovalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployeeActivitiesAndApprovalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
