import { Component, OnInit } from '@angular/core';
import { EmployeeDevelopmentPlanService } from '../../employee-development-plan/employee-development-plan.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { CoreService } from '../../../shared/services/core.service';
import { APPCONFIG } from '../../../config';
import { MdDialogRef, MdDialog, MdSnackBar } from '@angular/material';
import { PipEmployeeService } from '../../pip-employee/pip-employee.service';
import { BpipEmployeeService } from '../../bpip-employee/bpip-employee.service';
import { ResponseNoteComponent } from '../../../shared/components/response-note/response-note.component';
import { ReschedulePlanPopupComponent } from './reschedule-plan-popup/reschedule-plan-popup.component'
import { RestfulLaravelService } from '../../../shared/services/restful-laravel.service';
import { EmployeeTrainingDayService } from "../../employee-training-day/employee-training-day.service";
import * as moment from "moment";
import { MyDocument } from '../../induction/induction';
import { UploadDocumentDialog } from '../../induction/complete-profile/complete-profile.component';

@Component({
  selector: 'app-employee-activities-and-approval',
  templateUrl: './employee-activities-and-approval.component.html',
  styleUrls: ['./employee-activities-and-approval.component.scss']
})
export class EmployeeActivitiesAndApprovalComponent implements OnInit {
  public collapse = {};
  public developmentPlanCollapse = true;
  public pipCollapse = true;
  public isPending = {};
  public valid=false;
  public isValid = {};
  public bpipCollapse = true;
  public disciplinaryActionCollapse = true;
  public toggleFilter: boolean;
  public employmentList;
  public positionList;
  public isProcessing = false;
  public developmentPlans = [];
  public employeeDocumentSubmit = [];
  public performanceIndicator = [];
  public activitesAndApprovals = {
    bpip: [],
    disciplinaryAction: []
  };

  public isPipActionProcessing = false;

  constructor(public developmentPlanService: EmployeeDevelopmentPlanService, public pipService: PipEmployeeService,
    public router: Router, public activatedRoute: ActivatedRoute, public bpipService: BpipEmployeeService,
    public coreService: CoreService, public dialog: MdDialog, public restfulwebService: RestfulLaravelService,
    public trainingDayService: EmployeeTrainingDayService) { }


  ngOnInit() {
    this.getAllActivitesAndApprovals();
    APPCONFIG.heading = "Activities and approvals";
  }
  // method to get listing of all the modules.
  getAllActivitesAndApprovals() {
    this.isProcessing = true;
    this.developmentPlanService.getAllActivitesAndApprovals().subscribe(
      d => {
        if (d.code == "200") {
          for (let i of d.data) {
            i['isCollapse'] = true;
          }
          this.activitesAndApprovals = d.data;

          this.isProcessing = false;
          this.setActionProcessing();
        }
        else this.coreService.notify("Unsuccessful", d.message, 0);
      },
      error => this.coreService.notify("Unsuccessful", error.message, 0),
      () => { }
    )
  }

  // set action processing
  setActionProcessing() {
    for (let i in this.activitesAndApprovals) {
      this.isPending[i] = false;
      if (this.activitesAndApprovals[i] && this.activitesAndApprovals[i].length) {
        for (let k of this.activitesAndApprovals[i]) {
          if(k){
             k['isActionProcessing'] = false;
              if (k.status && (k.status == 'Awaiting Employee Acceptance' || k.status == 'Pending')) this.isPending[i] = true;
              if (i == 'trainingPlan') {
                if (k.status && k.status == 'Awaiting Trainee Acceptance') this.isPending[i] = true;
              }
              // if (i == 'survey' || i == 'ohs' || i == 'trophy') {
              //   if (k.status && k.status == 'Pending') this.isPending[i] = true;
              // }
              console.log("iiiiiiiiiiiiiii "+ i+ "kkk",k);
              if (i == 'uploadedDocument' && (moment(k.addedIt).diff(moment(), 'days') < 30)) this.isPending[i] = true;

              if (i == 'grievance') {
                if (k.result && k.result == 'Pending') this.isPending[i] = true;
              }
          }
          
           if(i == "performanceIndicator" &&  k.Pending.length>0) this.isPending["performanceIndicator"] = true;
        }
      }
      else{
           for (let k of this.activitesAndApprovals[i]) {      
           if(i == "performanceIndicator" &&  k.Pending.length>0) this.isPending["performanceIndicator"] = true;
        }
      }
    }
    console.log("dddd", this.activitesAndApprovals['performanceIndicator']);
    this.activitesAndApprovals['performanceIndicator'].Current.find(item3=>{
      if(item3.lapseDuration==true){
          this.activitesAndApprovals['islapseDuration']=true;
          this.valid = true;
        this.isValid['performancIndicator'] = this.valid; 
        } 
  })
  
  console.log("ssss", this.isPending)
  }

  // **************************** DEVELOPMENT PLAN *********************************************************
  onDevelopmentPlanView(d) {
    let url = `/app/employee/development-plan/view/${d.developmentPlanID}`;
    this.router.navigate([url]);
  }

  // **************************** PIP *************************************************************************
  onPipView(d) {
    let url = `/app/employee/performance-improvement-plan/view/${d.pipID}`;
    this.router.navigate([url]);
  }

  // method to show response note pop up.
  onPipResponseNote(plan) {
    let dialogRef = this.dialog.open(ResponseNoteComponent);
    let tempResponse = {
      url: "employee/pip-acceptance",
      note: {
        pipID: plan.pipID,
        action: "Note",
        responseNote: ""
      }
    }
    dialogRef.componentInstance.noteDetails = tempResponse;
    dialogRef.afterClosed().subscribe(result => {

      for (let k of this.activitesAndApprovals['pip']) {
        if (k.pipID == plan.pipID) {
          k['isActionProcessing'] = false;
        }
      }

    });
  }

  // method on accept PIP plan 
  onPipAccept(plan) {
    this.isPipActionProcessing = true;
    let reqBody = {
      reSchedule: {
        pipID: plan.pipID,
        action: "Accept"
      }
    };
    this.pipService.employeeResponsePIP(reqBody).subscribe(
      d => {
        if (d.code == "200") {
          this.coreService.notify("Successful", d.message, 1);
          for (let k of this.activitesAndApprovals['pip']) {
            if (k.pipID == plan.pipID) {
              k['isActionProcessing'] = false;
              k['status'] = 'Accepted';
              this.setActionProcessing();
            }
          }
        }

      },
      error => this.coreService.notify("Unsuccessful", error.message, 0),
      () => { this.isPipActionProcessing = false; }
    )
  }


  // **************************** BPIP *************************************************************************
  onBpipView(d) {
    let url = `/app/employee/behavioral-performance-improvement-plan/view/${d.bpipID}`;
    this.router.navigate([url]);
  }

  // method to show response note pop up.
  onBpipResponseNote(plan) {
    let dialogRef = this.dialog.open(ResponseNoteComponent);
    let tempResponse = {
      url: "employee/bpip-acceptance",
      note: {
        bpipID: plan.bpipID,
        action: "Note",
        responseNote: ""
      }
    }
    dialogRef.componentInstance.noteDetails = tempResponse;
    dialogRef.afterClosed().subscribe(result => {
      for (let k of this.activitesAndApprovals['bpip']) {
        if (k.bpipID == plan.bpipID) {
          k['isActionProcessing'] = false;
        }
      }
    });
  }

  // method on accept PIP plan 
  onBpipAccept(plan) {
    this.isPipActionProcessing = true;
    let reqBody = {
      reSchedule: {
        bpipID: plan.bpipID,
        action: "Accept"
      }
    };
    this.bpipService.employeeResponsePIP(reqBody).subscribe(
      d => {
        if (d.code == "200") {
          this.coreService.notify("Successful", d.message, 1);
          for (let k of this.activitesAndApprovals['bpip']) {
            if (k.bpipID == plan.bpipID) {
              k['isActionProcessing'] = false;
              k['status'] = 'Accepted';
              this.setActionProcessing();
            }
          }
        }

      },
      error => this.coreService.notify("Unsuccessful", error.message, 0),
      () => { this.isPipActionProcessing = false; }
    )
  }


  // ************************************** Disciplinary meating **********************************

  onDisciplinaryActionReschedule(d) {
    let dialogRef = this.dialog.open(ReschedulePlanPopupComponent, { width: '700px', height: '400px' });
    let reqBody = {
      url: 'employee/reschedule-disciplinary-action',
      disciplinaryActionID: d.disciplinaryActionID,
      proposedTime: d.meetingTime
    }
    dialogRef.componentInstance.rescheduleDetails = reqBody;
    dialogRef.afterClosed().subscribe(result => {
      if (result) d.status = 'Reschedule Requested'
    });
  }

  onDisciplinaryActionView(d) {

    this.router.navigate([`/app/employee/disciplinary-meeting/view/${d.disciplinaryActionID}`]);
  }

  onDisciplinaryActionInvite(d) {

    this.router.navigate([`/app/employee/disciplinary-meeting/invite/${d.disciplinaryActionID}`]);
  }


  onDisciplinaryActionAccept(plan) {
    this.isPipActionProcessing = true;
    let reqBody = {
      disciplinaryActionID: plan.disciplinaryActionID
    };
    this.restfulwebService.put(`employee/disciplinary-action-employee-accept`, reqBody).subscribe(
      d => {
        if (d.code == "200") {
          this.coreService.notify("Successful", d.message, 1);

          plan['isActionProcessing'] = false;
          plan['status'] = 'Accepted';
          this.setActionProcessing();
        }

      },
      error => this.coreService.notify("Unsuccessful", error.message, 0),
      () => { this.isPipActionProcessing = false; }
    )
  }




  onPopUp() {
    let dialogRef = this.dialog.open(shoutOutPopUpClass);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {

      }
    });
  }

  onPopupReq(data) {
    let dialogRef = this.dialog.open(WarningPopup);
    dialogRef.componentInstance.letterDetails = data;
    dialogRef.afterClosed().subscribe(res => console.log(res));
  }

  //*************************** Performance indicator ****************************** */


    onRejectKPI(k){
       k.isActionProcessing = true;
        let dialogRef = this.dialog.open(ResponseNoteComponent);  
        let tempResponse={
          url:"employee/reject-performance-indicator",
          note:{
            companyPositionPerformanceIndicatorID:k.companyPositionPerformanceIndicatorID,
            employeePerformanceIndicatorID:k.employeePerformanceIndicatorID,
            responseNote:""
          }
        }
          dialogRef.componentInstance.noteDetails=tempResponse;
          dialogRef.afterClosed().subscribe(result => {
          if(result){
            this.activitesAndApprovals['performanceIndicator'].Pending = this.activitesAndApprovals['performanceIndicator'].Pending.filter(obj => obj.companyPositionPerformanceIndicatorID != k.companyPositionPerformanceIndicatorID);
            k.isActionProcessing = false;
          } 
          });   
    }

  onAcceptKPI(k){
    k.isActionProcessing = true;
    this.restfulwebService.put(`employee/accept-performance-indicator/${k.companyPositionPerformanceIndicatorID}`,{}).subscribe(
      res => {
        if (res.code == 200) {
          this.coreService.notify("Successful", res.message, 1);
           this.getAllActivitesAndApprovals();
          // this.activitesAndApprovals['performanceIndicator'].Pending = this.activitesAndApprovals['performanceIndicator'].Pending.filter(obj => obj.companyPositionPerformanceIndicatorID != k.companyPositionPerformanceIndicatorID);
          // this.activitesAndApprovals['performanceIndicator'].Current.unshift(k);
        }
        else this.coreService.notify("Unsuccessful", res.message, 0);
        k.isActionProcessing = false;
      },
      err => {
        k.isActionProcessing = false;
        this.coreService.notify("Unsuccessful", err.message, 0);
      }
    );
  }

  onViewKPI(k) {
    let dialogRef = this.dialog.open(EmployeeKPINote);
    dialogRef.componentInstance.kpi = k;
    dialogRef.afterClosed().subscribe(result => {
      if (result) {

      }
    });
  }

  //*************************** Training plan ****************************** */

  acceptTrainingPlan(plan, accepted) {
    let reqBody = {
      "trainningID": plan.trainningID, "status": accepted ? "Trainee Under Assessment" : "Rejected"
    };
    plan['processing'] = true;
    this.trainingDayService.acceptOrRejectPlan(reqBody).subscribe(
      res => {
        plan['processing'] = false;
        if (res.code == 200) {
          this.coreService.notify('Successful', res.message, 1);
          plan.status = accepted ? 'Trainee Under Assessment' : 'Rejected';
          this.setActionProcessing();
        }
        else return this.coreService.notify('Unsuccessful', res.message, 0);
      },
      error => {
        plan['processing'] = false;
        if (error.code == 400) {
          return this.coreService.notify('Unsuccessful', error.message, 0);
        }
        this.coreService.notify('Unsuccessful', "Error while submitting detail", 0);
      }
    )
  }

  trainingPlanDetail(plan) {
    if (plan.status == 'Awaiting Trainee Acceptance') {
      return this.router.navigate(['/app/employee/training-day/acceptance', plan.trainningID]);
    } else if (plan.status == 'Awaiting Trainee Acknowledgement') {
      return this.router.navigate(['/app/employee/training-day/acknowledgement', plan.trainningID]);
    } else {
      return this.router.navigate(['/app/employee/training-day/result', plan.trainningID]);
    }
  }

  formatDateTime(date) {
    return moment(date).isValid() ? moment(date).format('DD MMM YYYY hh:mm A') : 'Not specified';
  }

  formatDate(date) {
    return moment(date).isValid() ? moment(date).format('DD MMM YYYY') : 'Not specified';
  }

  isValidData(data) {
    return !(data instanceof Array);
  }

  // **************** Grievance ************************
  grievanceDetail(grievance) {
    switch (grievance.stage) {
      case '1': this.router.navigate(['/app/employee/grievance/first-meeting', grievance.grievanceID]);
        break;
      case '2': this.router.navigate(['/app/employee/grievance/second-meeting-grievance-against', grievance.grievanceID]);
        break;
      case '3': this.router.navigate(['/app/employee/grievance/third-meeting-with-both', grievance.grievanceID]);
        break;

      default: this.router.navigate(['/app/employee/grievance/detail', grievance.grievanceID]);
        break;
    }
  }

  viewPerformanceAppraisalDetail(pa) {
    if (pa.directAssessment == 'Yes') {
      this.router.navigate(['/app/employee/performance-appraisal/direct-assessment-result', pa.performaceAppraisalID])
    } else {
      this.router.navigate(['/app/employee/performance-appraisal/result', pa.performaceAppraisalID])
    }
  }

  onuploadPopup() {
    let dialogRef = this.dialog.open(UploadDocumentDialog);
    dialogRef.afterClosed().subscribe((result: MyDocument) => {
      if (result) {
        this.uploadEmployeeDoc(result);
      }
    });
  }

  uploadEmployeeDoc(result){

    let reqBody = {
      "title":result.title,
      "desc":result.desc,
      "expiryDate":result.expiryDate,
      "userDocument":result.edsFile
    }

    this.restfulwebService.post(`employee/upload-signed-document`, reqBody).subscribe(
      d => {
        if (d.code == "200") {
          this.coreService.notify("Successful", d.message, 1);
          this.activitesAndApprovals['uploadedDocument'] = d.data.uploadedDocument?d.data.uploadedDocument:[];
        }

      },
      error => this.coreService.notify("Unsuccessful", error.message, 0),
      () => { this.isPipActionProcessing = false; }
    )
  }

}





@Component({
  selector: 'shoutOutPopup',
  template: `
      <div class="createShououtsp">
            <h3 md-dialog-title class="heading">Create Shououtsp</h3>
        <div md-dialog-content>            
          <form >
          <div class="form-group">
            <div class="lftinpt10">
              <label class="form-inline">Title:</label>
            </div>  
            <div class="rghtinput10">
              <input type="text" class="form-control" placeholder="Title">
            </div>
          </div>
          <div class="form-group">
            <div class="lftinpt10">
              <label class="form-inline">Reson:</label>
            </div>  
            <div class="rghtinput10">
              <input type="text" class="form-control" placeholder="Reson">
            </div>
          </div>
          <div class="form-group">
              <div class="lftinpt10">
              <label class="form-inline">Employee:</label>
            </div>  
            <div class="rghtinput10">
              <select class="selectpicker">
                <option>Mustard</option>
                <option>Ketchup</option>
                <option>Relish</option>
              </select>
             </div> 

          </div>
          <div md-dialog-actions>
 <button type="submit" class="btn btn-primary">Create</button>
</div>
          
        </form>
</div>
</div>

      `,
  styles: [`
  .createShououtsp  label{
      font-size: 16px;
  }
  .createShououtsp .mat-dialog-content{
    max-height:100%;
    overflow:visible;
  }
  .createShououtsp .form-group .form-control{
    max-width{width:100%;}
  }
 .createShououtsp button{
   font-size:16px;
  }
  `]
})
export class shoutOutPopUpClass {
  constructor() {
  }
}

@Component({
  selector: 'warningpop',
  template: `<div class="form-group">
              <div class="lftinpt10">
                <label class="form-inline">Title:</label>
              </div>  
              <div class="rghtinput10">
                {{letterDetails?.title}}
              </div>
            </div>
            <div class="form-group">
            <div class="lftinpt10">
              <label class="form-inline">Reason:</label>
            </div>  
            <div class="rghtinput10">
              <div [innerHTML]='letterDetails.reason'>
              </div>              
            </div>
          </div>
          <div class="form-group">
          <div class="lftinpt10">
            <label class="form-inline">Requested On:</label>
          </div>  
          <div class="rghtinput10">
            {{letterDetails?.insertTime}}
          </div>
        </div>`
})
export class WarningPopup {
  public letterDetails: any;
  constructor() {
    console.log(this.letterDetails);
  }
}


@Component({
  selector: 'app-employee-kpi-note',
  templateUrl: './kpi-with-note-popup.html',
  styles: [``]
})
export class EmployeeKPINote implements OnInit {
  public kpi:any;
  constructor() { }
  ngOnInit() {}
}
