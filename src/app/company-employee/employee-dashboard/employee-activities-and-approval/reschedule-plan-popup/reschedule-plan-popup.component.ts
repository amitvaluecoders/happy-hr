import { Component, OnInit } from '@angular/core';
import { CoreService } from '../../../../shared/services/core.service';
import * as moment from 'moment';
import { IDatePickerConfig } from 'ng2-date-picker';
import {RestfulLaravelService} from '../../../../shared/services/restful-laravel.service';
import { MdDialogRef, MdDialog, MdSnackBar } from '@angular/material';


@Component({
  selector: 'app-reschedule-plan-popup',
  templateUrl: './reschedule-plan-popup.component.html',
  styleUrls: ['./reschedule-plan-popup.component.scss']
})
export class ReschedulePlanPopupComponent implements OnInit {
  
  public rescheduleDetails:any;
   public config: IDatePickerConfig = {};
    public tempDateTime:any; 
    public isProcessing=false;
    public isButtonClicked=false;

  constructor(public restfulService:RestfulLaravelService,public coreService:CoreService,
              public dialogRef:MdDialogRef<ReschedulePlanPopupComponent>) { }

  ngOnInit() {
    console.log(this.rescheduleDetails)
        this.config.locale = "en";
        this.config.format = "YYYY-MM-DD hh:mm A";
        this.config.showMultipleYearsNavigation = true;
        this.config.weekDayFormat = 'dd';
        this.config.min=moment().add(+1, 'days');
        this.config.disableKeypress = true;
        this.config.monthFormat="MMM YYYY";
    this.tempDateTime=moment(this.rescheduleDetails.proposedTime).format("YYYY-MM-DD hh:mm:ss"); 
  }

  onSave(){
    this.isButtonClicked=true;
    this.isProcessing=true;
    this.rescheduleDetails.proposedTime=moment(this.tempDateTime).format("YYYY-MM-DD hh:mm:ss"); 
    this.restfulService.post(this.rescheduleDetails.url,this.rescheduleDetails).subscribe(
      d=>{
        if(d.code=='200'){
    this.isProcessing=false;
         
            this.coreService.notify("Successful", d.message, 1);
            this.dialogRef.close(1);
        }
      },
       error => this.coreService.notify("Unsuccessful", error.message, 0),
          () => {this.isProcessing=false; }
      )
  }

}
