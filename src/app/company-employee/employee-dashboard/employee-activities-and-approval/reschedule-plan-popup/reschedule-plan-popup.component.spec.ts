import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReschedulePlanPopupComponent } from './reschedule-plan-popup.component';

describe('ReschedulePlanPopupComponent', () => {
  let component: ReschedulePlanPopupComponent;
  let fixture: ComponentFixture<ReschedulePlanPopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReschedulePlanPopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReschedulePlanPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
