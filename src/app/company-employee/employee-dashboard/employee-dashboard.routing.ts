import {  ModuleWithProviders  } from '@angular/core';
import {  RouterModule,Routes  } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import { EmployeeActivitiesAndApprovalComponent } from './employee-activities-and-approval/employee-activities-and-approval.component';
export const DashBoardPagesRoutes: Routes = [
    {
        path: '',
        canActivate:[],
        children: [
           
            { path: '', redirectTo:'dashboard'},
            { path : 'dashboard', component: DashboardComponent},            
            { path : 'activites', component: EmployeeActivitiesAndApprovalComponent},
                        
        ]
    }
];

export const DashBoardRoutingModule = RouterModule.forChild(DashBoardPagesRoutes);
