import { Component, OnInit } from '@angular/core';
import { MdDialogRef, MdDialog, MdSnackBar } from '@angular/material';
import { EmployeeIntroduction } from "../induction.service";

@Component({
  selector: 'app-approval-popup',
  templateUrl: './approval-popup.component.html',
  styles: [`
    .imgSignature{
      border:1px solid #d4d4d4;
      padding:14px;
    }
    .signDemo{
      font-size:50px;
    }
    .fig{
      margin:20px 0 30px;
    }
    `]
})
export class ApprovalPopupComponent implements OnInit {

  isProcessing = false;
  signature = { "text": "", "type": "" };
  constructor(public employeeService: EmployeeIntroduction, public dialog: MdDialogRef<ApprovalPopupComponent>) { }

  ngOnInit() {
    this.getSignature();
  }

  getSignature() {
    this.isProcessing = true;
    this.employeeService.getSignature().subscribe(
      res => {
        this.isProcessing = false;
        if (res.code == 200) {
          this.signature = res.data.signature;
        }
      },
      error => {
        this.isProcessing = false;
      }
    )
  }

  approve() {
    this.dialog.close(true);
  }

}