import { Injectable } from '@angular/core';
import { RestfulLaravelService } from '../../shared/services';
import { Http, Headers, Response, RequestOptions } from '@angular/http';


@Injectable()
export class EmployeeIntroduction {

    constructor(
        private restfulWebService: RestfulLaravelService) { }

    getEmployeeProfile(id) {
        return this.restfulWebService.get(`employee-profile/${id}`);
    }

    updateEmployeeProfile(reqBody) {
        return this.restfulWebService.post('employee/complete-profile', reqBody)
    }

    getQuestionnaire() {
        return this.restfulWebService.get('employee/get-employee-questionair')
    }

    getPositionDescription() {
        return this.restfulWebService.get('employee/get-employee-postion');
    }

    approvePositionDescription(reqBody) {
        return this.restfulWebService.post('employee/save-profile-position', reqBody)
    }

    getContract() {
        return this.restfulWebService.get('employee/get-employee-contract');
    }

    approveContract(reqBody) {
        return this.restfulWebService.post('employee/save-profile-contract', reqBody)
    }

    uploadDoc(formData) {
        return this.restfulWebService.post('upload-file', formData);
    }

    file_upload(formData) {
        return this.restfulWebService.post('file-upload', formData);
    }

    getCountries() {
        return this.restfulWebService.get('country-list');
    }

    getSignature() {
        return this.restfulWebService.get('employee/employee-signature');
    }

    getSuppernnuationList(reqBody) {
        return this.restfulWebService.get(`suppernnuation-list/${encodeURIComponent(JSON.stringify(reqBody))}`);
    }

    getPolicies() {
        let reqBody = {
            "search": "",
            "filter": {
            }
        }
        return this.restfulWebService.get(`employee/list-policy/${encodeURIComponent(JSON.stringify(reqBody))}`);
    }

    acceptAllPolicies() {
        return this.restfulWebService.get('employee/accept-all-policy');
    }

    getPolicyDetails(id) {
        return this.restfulWebService.get(`employee/policy-detail/${id}`)
    }

    downloadPdf(doc) {
        switch (doc.type) {
            case 'contract':
                window.open(`${this.restfulWebService.baseUrl}generate-company-contract-pdf/${doc.docID}`); break;
            case 'policy':
                window.open(`${this.restfulWebService.baseUrl}generate-company-policy-pdf/${doc.docID}`); break;
            case 'position':
                window.open(`${this.restfulWebService.baseUrl}generate-position-pdf/${doc.docID}`); break;
        }
    }
}