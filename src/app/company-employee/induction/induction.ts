export class Address {
    "streetAddress": string = "";
    "landMark": string = "";
    "cityName": string = "";
    "stateName": string = "";
    "postalCode": string = "";
    "countryID": string = "";
    "addressSuburb": string = "";
}
export class PersonalInformation {
    constructor() {
        this.address = new Address;
    }
    "firstName": string = "";
    "lastName": string = "";
    "email": string = "";
    "phoneNumber": string = "";
    "dob": string = "";
    "userGender": string = "";
    "taxFileNumeber": string = "";
    "linkedInUrl": string = "";
    "aboutMySelf": string = "";
    "address": Address;
    "profileImageUrl": string = "";
}
export class Superannuation {
    "fundName": string = "";
    "fundType": string = "";
    "membershipNumber": string = "";
    "electronicServices": string = "";
    "accountBsb": string = "";
    "accountName": string = "";
    "accountNumber": string = "";
}
export class BankDetail {
    "bankName": string = "";
    "bsbNumber": string = "";
    "accountName": string = "";
    "accountNumber": string = "";
    
    
}
export class EmployeeContact {
    "name": string = "";
    "email": string = "";
    "mobileNumber": string = "";
    "employeeRelation": string = "";
}
export class EmployeeLicence {
    "licenceTitle": string = "";
    "licenceNumber": string = "";
    "expiryDate": string = "";
    "stateName": string = "";
    "countryID": string = "";
}
export class Employeement {
    "contractEndDate": string = "";
    "contractStartDate": string
    "employeeType": string = "";
    "salary": any;
    "type": string = "";
}
export class Profile {
    constructor() {
        this.personalInformation = new PersonalInformation;
        this.superannuation = new Superannuation;
        this.bankDetail = new BankDetail;
        this.employeeContact = new EmployeeContact;
        this.employeeLicence = new EmployeeLicence;
        this.employeement = new Employeement;
    }
    mandatoryDocuments:any=[];
    employeeSince: string = "";
    lastLogin: string = "";
    name: string = "";
    positionTitle: string = "";
    role: string = "";
    status: string = "";
    personalInformation: PersonalInformation;
    superannuation: Superannuation;
    bankDetail: BankDetail;
    employeeContact: EmployeeContact;
    employeeLicence: EmployeeLicence;
    employeement: Employeement;
    signature: Signature;
    positionID: string="";
    // companyPositionLevelID: string="";
    // remunarationRate: string="";
    // contractType: string="";
    // employmentTypeID: string="";
    // contractStartDate: string="";
    // contractEndDate: string="";
}

export class Signature {
    text: string = ""; type: string = "";
}

export class MyDocument {
    title: string = "";
    desc: string = "";
    edsFile: string = "";
}

export class CompanyDocument extends MyDocument {
    expiryDate: string = "";
}