import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CompleteProfileComponent } from './complete-profile/complete-profile.component';
import { inductuionModuleRouter } from './induction.routing';
import { FormsModule } from '@angular/forms';
import { PolicyDocumentsComponent } from './policy-documents/policy-documents.component';
import { SharedModule } from '../../shared/shared.module';
import { PositionDescriptionComponent } from './position-description/position-description.component';
import { ContractApprovalComponent } from './contract-approval/contract.component';
import { EmpolyeeQuestionnairesComponent } from './empolyee-questionnaires/empolyee-questionnaires.component';
import { EmployeeIntroduction } from './induction.service';
import { RestfulLaravelService } from '../../shared/services';
import { MaterialModule } from '@angular/material';
import { ApprovalPopupComponent } from './approval-popup/approval-popup.component';
import { DataTableModule } from 'angular2-datatable';
import { CountryStateService } from "../../shared/services/country-state.service";
import { PolicyDocumentDetailsPopupComponent } from './policy-documents/policy-document-details-popup/policy-document-details-popup.component';
import { Ng2AutoCompleteModule } from 'ng2-auto-complete';
//import { NguiAutoCompleteModule } from '@ngui/auto-complete';
@NgModule({
  imports: [
    Ng2AutoCompleteModule,
    //NguiAutoCompleteModule,
    DataTableModule,
    CommonModule,
    inductuionModuleRouter,
    SharedModule,
    FormsModule,
    MaterialModule
  ],
  declarations: [CompleteProfileComponent, PolicyDocumentsComponent, PositionDescriptionComponent,
    ContractApprovalComponent, EmpolyeeQuestionnairesComponent, ApprovalPopupComponent, PolicyDocumentDetailsPopupComponent],
  entryComponents: [ ApprovalPopupComponent,PolicyDocumentDetailsPopupComponent],
  providers: [EmployeeIntroduction, RestfulLaravelService,CountryStateService]
})
export class InductionModule { }
