import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { APPCONFIG } from '../../../config';
import { EmployeeIntroduction } from '../induction.service';
import { CoreService } from '../../../shared/services/core.service';
import { MdDialogRef, MdDialog, MdSnackBar } from '@angular/material';
import { ApprovalPopupComponent } from "../approval-popup/approval-popup.component";

@Component({
  selector: 'app-position-description',
  templateUrl: './position-description.component.html',
  styleUrls: ['./position-description.component.scss']
})
export class PositionDescriptionComponent implements OnInit {
  public progress;
  public position = { "companyPosition": [], "userID": "" };
  public isProcessing: boolean;

  constructor(public employeeService: EmployeeIntroduction, public coreService: CoreService,
    public router: Router, public dialog: MdDialog) { }

  ngOnInit() {
    this.progressBarSteps();
    APPCONFIG.heading = "Position description approval";
    this.getPositionDescription()
  }
  progressBarSteps() {
    this.progress = {
      progress_percent_value: 75,
      total_steps: 4,
      current_step: 3,
      circle_container_width: 25,
      steps: [
        // {num:1,data:"Select Role"},
        { num: 1, data: 'Complete your profile' },
        { num: 2, data: 'Contract approval' },
        { num: 3, data: 'Position description approval', active: true  },
        { num: 4, data: 'Policy approval'},
        // { num: 5, data: 'Questionnaire' }
      ]
    }
  }

  getPositionDescription() {
    this.isProcessing = true;
    this.employeeService.getPositionDescription().subscribe(
      res => {
        if (res.code == 200) {
          this.isProcessing = false;
          this.position = res.data;
        }
      },
      error => {
        this.isProcessing = false;
        this.coreService.notify("Error", "Error while getting position details.", 0)
      }
    )
  }

  provideApproval() {
    let dialogRef = this.dialog.open(ApprovalPopupComponent);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.isProcessing = true;
        
        let reqBody = {
          "employeeUserID": this.position.userID,
          "positionID": this.position.companyPosition[0].companyPositionId,
          "status": "Accept"
        }
        this.employeeService.approvePositionDescription(reqBody).subscribe(
          res => {
            if (res.code == 200) {
              this.isProcessing = false;
              this.coreService.notify("Successful", res.message, 1);
              this.router.navigate(['/user/employee-induction/policy-approval']);
            }
            else this.coreService.notify("Unsuccessful", res.message, 0);
          },
          error => {
            this.isProcessing = false;
            if (error.code == 400) {
              return this.coreService.notify("Unsuccessful", error.message, 0);
            }
            this.coreService.notify("Error", "Error while getting position details.", 0)
          }
        )
      }
    });
  }
}
