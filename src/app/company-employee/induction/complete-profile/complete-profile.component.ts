import { Component, OnInit,ViewContainerRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { APPCONFIG } from '../../../config';
import { EmployeeIntroduction } from '../induction.service';
import { CoreService } from '../../../shared/services/core.service';
import { CountryStateService } from "../../../shared/services/country-state.service";
import { UploadMendatoryDocsPopupComponent } from '../../../user/subcontractor-induction/upload-mendatory-docs-popup/upload-mendatory-docs-popup.component';
import { IDatePickerConfig } from 'ng2-date-picker';
import { Profile, MyDocument, CompanyDocument } from '../induction';
import * as moment from 'moment';
import { ConfirmService } from '../../../shared/services/confirm.service';
import { MdDialog, MdDialogRef } from '@angular/material';

@Component({
  selector: 'app-complete-profile',
  templateUrl: './complete-profile.component.html',
  styleUrls: ['./complete-profile.component.scss']
})
export class CompleteProfileComponent implements OnInit {
  // public ducuSignText="";
  fundNameArr=[];
  progress;
  collapse = {};
  isSuppernnuationListProcessing=false;
  birthday: any;
  employmentTypeID: any;
  errors=[];
  licExpiry: any;
  docRequired=false;
  config: IDatePickerConfig = {};
  profile: Profile;
  employeeDocumentSigned = [];
  documentRequired=false;
  employeeDocumentSubmit = [];
  employeeId: string;
  isProcessing: boolean;
  isSaving: boolean;
  isProcessingSign: boolean;
  signatureText = "";
  adoptSign = true;
  countryList = [];
  isEditing = false;
  heading = "Complete profile";
  isProcessingImg = false;
  additionalInfo;
  profileImage = '';
  stateList=[];
  showFundNameField=true;
public unsubscriber:any;
  

  constructor( public countryService: CountryStateService,public employeeService: EmployeeIntroduction, public coreService: CoreService,
    public activatedRoute: ActivatedRoute,public viewContainerRef:ViewContainerRef,
     public confirmService: ConfirmService,  
    public router: Router, public dialog: MdDialog) {
    this.profile = new Profile;
    this.config.locale = "en";
    this.config.format = "DD MMM YYYY";
    this.config.showMultipleYearsNavigation = true;
    this.config.weekDayFormat = 'dd';
    this.config.disableKeypress = true;
    this.config.monthFormat = "MMM YYYY";
  }

  ngOnInit() {
    console.log("own key ==> ", this.activatedRoute.snapshot.params['key']);

    // this.getSuppernnuationList('');
    // this.ducuSignText="Shrikant kr. Ram";
    let user = this.coreService.getUser();
    this.stateList = this.countryService.getState(13);
    this.employeeId = user['userID'];
    this.getCountryList();
    if (this.employeeId) {
      this.getEmployeeProfile();
    }

    if (this.activatedRoute.snapshot.params['key']) {
      this.isEditing = true;
      this.heading = "Edit profile";
    } else {
      this.progressBarSteps();
    }

    APPCONFIG.heading = this.heading;
  }
  formatDateTime(date) {
    return moment(date).isValid() ? moment(date).format('DD MMM YYYY hh:mm A') : 'Not specified';
  }

  SearchSuppernnuationList(searchKey){
    if(this.unsubscriber)this.unsubscriber.unsubscribe();
    this.getSuppernnuationList(searchKey);
  }

  getSuppernnuationList(searchKey){
    searchKey = encodeURIComponent(searchKey);
    this.isSuppernnuationListProcessing = true;
    this.unsubscriber = this.employeeService.getSuppernnuationList({search:searchKey}).subscribe(
      res => {
        if (res.code == 200) {
          this.fundNameArr = res.data;
          this.profile.superannuation['isDropDown']=true;
          this.isSuppernnuationListProcessing = false;

        }
      },
      error => {
        this.isSuppernnuationListProcessing = false;
        this.coreService.notify("Error", error.message, 0)
      }
    )
  }

  downloadPDF(doc) {
    if (!doc.docID) return;
    this.employeeService.downloadPdf(doc);
  }

  progressBarSteps() {
    this.progress = {
      progress_percent_value: 25,
      total_steps: 4,
      current_step: 1,
      circle_container_width: 25,
      steps: [
        // {num:1,data:"Select Role"},
        { num: 1, data: 'Complete your profile', active: true },
        { num: 2, data: 'Contract approval' },
        { num: 3, data: 'Position description approval' },
        { num: 4, data: 'Policy approval' },

        // { num: 5, data: 'Questionnaire' }
      ]
    }
  }
  getCountryList() {
    this.employeeService.getCountries().subscribe(
      data => {
        if (data.code == 200) {
          this.countryList = data.data;
        }
      }
    );
  }

  getEmployeeProfile() {
    this.isProcessing = true;
    this.employeeService.getEmployeeProfile(this.employeeId).subscribe(
      res => {
        if (res.code == 200) {
          this.isProcessing = false;
          this.profile = res.data[0];
          this.birthday = moment(this.profile.personalInformation.dob).isValid() ? moment(this.profile.personalInformation.dob) : '';
          this.licExpiry = !(this.profile.employeeLicence.expiryDate) ? '' : moment(this.profile.employeeLicence.expiryDate).format('YYYY-MM-DD');
          this.employeeDocumentSigned = res.data[1].employeeDocumentSigned;
          this.employeeDocumentSubmit = res.data[2].employeeDocumentSubmit;
          this.employmentTypeID = res.data[4].additionalInformation.employmentTypeID;
          this.signatureText = this.profile.signature.text ? this.profile.signature.text : this.profile.name;
          this.profileImage = this.profile.personalInformation.profileImageUrl;
          this.additionalInfo = res.data[4].additionalInformation;
          if(this.profile.mandatoryDocuments.length){
           // this.documentRequired=true;
            this.flashRedWhenUpload();
          }
           this.profile.mandatoryDocuments =  this.profile.mandatoryDocuments.filter(k=>{
            k['isProcessing']= false;
            return k;
          });
          for (var _i = 1; _i < 13; _i++) {    
              this.collapse[`pfl${_i}`] = true ;
              this.collapse[`pfl10`] = false;
          }
        }
        else return this.coreService.notify("Unsuccessful", res.message, 0);
      },
      error => {
        this.isProcessing = false;
        if (error.code == 400) {
          return this.coreService.notify("Unsuccessful", error.message, 0);
        }
        this.coreService.notify("Error", "Error while getting profile details.", 0)
      }
    )
  }

  saveAs(type,profileForm) {
        this.errors = this.coreService.getFormValidationErrors(profileForm);
    if (this.profile.signature.text == '' || this.profile.signature.type == '' || !this.profile.personalInformation.profileImageUrl) return;
if(profileForm.form.valid && this.errors.length==0){
    this.profile.personalInformation.dob = !(this.birthday) ? '' : moment(this.birthday).format('YYYY-MM-DD');
    this.profile.employeeLicence.expiryDate = !(this.licExpiry) ? '' : moment(this.licExpiry).format('YYYY-MM-DD');
    
    let reqBody = this.profile;
    if (this.activatedRoute.snapshot.params['key']) {
      reqBody['isInduction']= false;
    }else{
      reqBody['isInduction']= true;;      
    }
    reqBody['saveType'] = type;
    reqBody['employeeUserID'] = this.employeeId;
    reqBody['employeeDocumentSubmit'] = this.employeeDocumentSubmit;
    reqBody['employmentTypeID'] = this.employmentTypeID;
    reqBody.employeement['employmentTypeID'] = this.employmentTypeID;
    this.isSaving = true;
    this.employeeService.updateEmployeeProfile(reqBody).subscribe(
      res => {
        this.isSaving = false;
        if (res.code == 200) {
          this.coreService.notify("Successful", "Profile details are updated successfully.", 1);
          if (this.profileImage) {
            APPCONFIG.imageUrl = this.profileImage;
            this.coreService.setProfileImage(this.profileImage);
          }
          if (this.isEditing) return this.router.navigate(['/app/employee']);
          else if (type == 'save') return this.router.navigate(['/user/employee-induction/contract-approval']);
        }
        else return this.coreService.notify("Unsuccessful", res.message, 0);
      },
      error => {
        this.isSaving = false;
        if (error.code == 400) {
          return this.coreService.notify("Unsuccessful", error.message, 0);
        }
        this.coreService.notify("Error", "Error while updating profile details.", 0)
      }
    )
  }
}

//flash red the mandatory field when file uploaded 
flashRedWhenUpload(){
  for(let item of this.profile.mandatoryDocuments){
    if(!item.file){
      this.docRequired = true;
      break;
    }
    else{
      this.docRequired = false;
    }
  } 
}
//flash red the mandatory field when file deleted 
flashRedWhenDelete(){
  this.profile.mandatoryDocuments.forEach(item=>{
    if(!item.file){
      this.docRequired=true;
      return;
    }
  })
}
  
    uploadDocs(doc) {  

    let dialogRef = this.dialog.open(UploadMendatoryDocsPopupComponent, { width: '500px' });
    dialogRef.componentInstance.doc=JSON.parse(JSON.stringify(doc));
      dialogRef.afterClosed().subscribe((result) => {
      if(result) {
      doc['description'] = result['desc']; 
      doc['file'] = result['edsFile']; 
      doc['expiryDate'] = result['expiryDate'];
        this.flashRedWhenUpload();
      }})       
  }
    onDeleteMandDocs(docs){
      this.confirmService.confirm(this.confirmService.tilte,this.confirmService.message,this.viewContainerRef)
      .subscribe(res => {
       
      let result = res;
      if (result){
        //this.documentRequired=true;
         docs.file='';
         docs['expiryDate']='';
        this.flashRedWhenDelete();
         
      } 
    })
    }

    checkValidations(elArray){
      for(let e of elArray){
        if(e.invalid){
          //this.isAmountExist=true;
          return true;
        }   
      }
      return false;
    }
  

  handleFileSelect($event) {
    const files = $event.target.files || $event.srcElement.files;
    const file: File = files[0];
    let validFileExtensions = [".jpg", ".jpeg", ".png"];
    console.log("dkjhkjdh",file.size);
    if(file.size>1961432){
      return this.coreService.notify("Unsuccessful", "The photo file is to large! Please use a file smaller that 2MB. Please try again.", 0)
    }
    else{
      let ext = file.name.substring(file.name.lastIndexOf('.')).toLowerCase();
    if (validFileExtensions.indexOf(ext) >= 0) {
      const formData = new FormData();
      formData.append('profileImage', file);

      this.isProcessingImg = true;
      this.employeeService.uploadDoc(formData).subscribe(res => {
        this.isProcessingImg = false;
        this.profileImage = res.data.profileImage;
        this.profile.personalInformation.profileImageUrl = res.data.fileName;
      });
    } else return this.coreService.notify("Unsuccessful", "Invalid file extension.", 0)
    }


  }

  selectSignature(sign) {
    this.profile.signature.type = sign;
    this.profile.signature.text = this.signatureText;
    console.log(this.profile.signature);
  }

  // uploadSignature($event) {
  //   this.isProcessingSign = true;
  //   const files = $event.target.files || $event.srcElement.files;
  //   const file: File = files[0];
  //   let ext = file.name.substring(file.name.lastIndexOf('.')).toLowerCase();
  //   if (ext == '.jpg' || ext == '.jpeg' || ext == '.png') {
  //     const formData = new FormData();
  //     formData.append('userSignature', file);

  //     this.employeeService.uploadDoc(formData).subscribe(res => {
  //       this.isProcessingSign = false;
  //       this.signature = res.data.userSignature;
  //     });
  //   } else return this.coreService.notify("Unsuccessful", "Invalid file extension.", 0)
  // }

  onpopup() {
    let dialogRef = this.dialog.open(UploadDocumentDialog, { width: '500px' });
    dialogRef.afterClosed().subscribe((result: MyDocument) => {
      if (result) {
        this.employeeDocumentSubmit.push(result);
      }
    });
  }

  removeDoc(index) {
    this.confirmService.confirm(this.confirmService.tilte,this.confirmService.message,this.viewContainerRef)
    .subscribe(res=>{
      if(res){
          this.employeeDocumentSubmit.splice(index, 1);                              
      }
    })        
  }
}

@Component({
  selector: 'upload-document',
  template: `
      <h3>Upload document</h3>
      <form name="docForm" #docForm="ngForm" (ngSubmit)="docForm.valid && upload()">
        <div class="form-group">
          <label>Title</label>
          <input type="text" name="title" #title="ngModel" [(ngModel)]="doc.title" placeholder="Title ie. Driver's Licence" required/>
          <div *ngIf="title.dirty || docForm.submitted">
              <div class="color-danger" [hidden]="title.valid"> Please enter the title. </div>
          </div>
        </div>
        <div>
        <div class="form-group">
          <label>Description</label>
          
          <textarea type="text" placeholder="Description" rows="4" name="description" #description="ngModel" [(ngModel)]="doc.desc" required></textarea>
          <div *ngIf="description.dirty || description.touched || docForm.submitted">
              <div class="color-danger" [hidden]="description.valid"> Please enter the description. </div>
          </div>
        </div>
        </div>
 
        <div>
        <div class="form-group">
          <label>Expiry</label>
          <dp-date-picker class="form-control" name="expbiryDate" #expiryDate="ngModel" [theme]="'dp-material'" [mode]="'day'" [(ngModel)]="licExpiry" [config]="config" placeholder="Choose a date"></dp-date-picker>
        </div>
      </div>

        <div>
        <div class="uploadBtn">
          <label>{{fileName}}</label>
          <input style="display:none;" id="uploadFL" type="file" accept=".doc,.docx,.pdf,image/png,image/jpg,image/jpeg" name="doc" (change)="handleFileSelect($event)" />
          <label style="display:block;" for="uploadFL" class="btn btn-lg btn-primary">
            Upload document
          </label>
          </div>
          <strong>{{uploadedFileName}}</strong>
          <p>Select file (JPEG, PNG, PDF or DOC files) </p>
         <div *ngIf="!doc.edsFile && docForm.submitted">
              <div class="color-danger"> Please select a document. </div>
          </div>
        </div>
        <md-progress-spinner *ngIf="isProcessingImg" mode="indeterminate" color=""></md-progress-spinner>
        <button *ngIf="!isProcessingImg" type="submit" class="btn btn-primary">Submit</button>
      </form>
          `,
  styles: [`
      h3{
        font-size:22px;
      }
      .form-group label{
        font-size: 15px;
        color: #4b4c4d;
        max-width: 600px;
        width: 100%;
        padding-right: 18px;
        font-family: inherit;
        display: block;
        padding-right: 12px;
      }
      .form-group input{
        display: block;
        width: 100%;
        padding: 0.5rem 0.75rem;
        font-size: 1rem;
        line-height: 1.25;
        color: #464a4c;
        background-color: #fff;
        background-image: none;
        background-clip: padding-box;
        border: 1px solid rgba(0, 0, 0, 0.15);
        border-radius: 0.2rem;
        transition: border-color ease-in-out 0.15s, box-shadow ease-in-out 0.15s;
      }
      .form-group textarea{
         display: block;
         width: 100%;
         border: 1px solid rgba(0, 0, 0, 0.15);
        border-radius: 0.2rem;
        transition: border-color ease-in-out 0
      }
      .uploadBtn{
        float:left;
        width:100%;
      }
      .uploadBtn .btn-primary{
          padding:6px;
          float:left;
          width:100%;
          position:relative;
      }
      .uploadBtn input[type="file"]{
          width: 100%;
          position: absolute;
          float: left;
          height: 100%;
          left: 0;
          top: 0;
          opacity: 0;
        }
      .uploadBtn label {
          display: none;
      }

  `]
})
export class UploadDocumentDialog {
  doc: CompanyDocument;
  uploadedFileName:any;
  fileName = "";
  config: IDatePickerConfig = {};
  licExpiry: any;
  isProcessingImg: boolean;
  _validFileExtensions = [".jpg", ".jpeg", ".png", ".pdf", ".doc", ".docx"];

  constructor(public dialog: MdDialogRef<UploadDocumentDialog>, public coreService: CoreService, public employeeService: EmployeeIntroduction) {
    this.doc = new CompanyDocument;
    this.config.locale = "en";
    this.config.format = "DD MMM YYYY";
    this.config.showMultipleYearsNavigation = true;
    this.config.weekDayFormat = 'dd';
    this.config.disableKeypress = true;
    this.config.monthFormat = "MMM YYYY";
    this.config.min = moment();
  }

  handleFileSelect($event) {
    this.isProcessingImg = true;
    const files = $event.target.files || $event.srcElement.files;
    const file: File = files[0];
    this.fileName = file.name;
    if(file.size>1961432){
      this.isProcessingImg
      this.dialog.close();
      return this.coreService.notify("Unsuccessful", " Uploaded document file is to large is must be a file smaller that 2MB. Please try again!  ",0)
    }
    
    let ext = this.fileName.substring(this.fileName.lastIndexOf('.')).toLowerCase();
    if (this._validFileExtensions.indexOf(ext) >= 0) {
      const formData = new FormData();
      formData.append('userDocument', file);

      this.employeeService.uploadDoc(formData).subscribe(res => {
        this.isProcessingImg = false;
        this.doc.edsFile = res.data.userDocument;
        this.uploadedFileName = res.data.fileName;
      });
    } else return this.coreService.notify("Unsuccessful", "Invalid file extension.", 0)

  }

  upload() {
    if (!this.doc.edsFile) return;
    this.doc.expiryDate = !(this.licExpiry) ? '' : moment(this.licExpiry).format('YYYY-MM-DD');
    this.dialog.close(this.doc);
  }
}