import { Component, OnInit } from '@angular/core';
import { APPCONFIG } from '../../../config';
import { EmployeeIntroduction } from '../induction.service';
import { CoreService } from '../../../shared/services/core.service';

@Component({
  selector: 'app-empolyee-questionnaires',
  templateUrl: './empolyee-questionnaires.component.html',
  styleUrls: ['./empolyee-questionnaires.component.scss']
})
export class EmpolyeeQuestionnairesComponent implements OnInit {
  public progress;
  public questions = [];
  public step = 0;

  constructor(
    public empolyeeService: EmployeeIntroduction, public coreService: CoreService) { }

  ngOnInit() {
    this.progressBarSteps();
    APPCONFIG.heading = "Questionnaire";
    this.getQuestionnaire();
  }

  progressBarSteps() {
    this.progress = {
      progress_percent_value: 100,
      total_steps: 5,
      current_step: 5,
      circle_container_width: 25,
      steps: [
        { num: 1, data: 'Complete your profile' },
        { num: 2, data: 'Policy approval' },
        { num: 3, data: 'Position description approval' },
        { num: 4, data: 'Contract approval' },
        { num: 5, data: 'Questionnaire', active: true }
      ]
    }
  }

  getQuestionnaire() {
    this.empolyeeService.getQuestionnaire().subscribe(
      res => {
        if (res.code == 200) {
          console.log(res);
          this.questions = res.data;
        }
      },
      error => {
        this.coreService.notify("Error", "Error while getting profile details.", 0)
      }
    )
  }

  saveAndNext() {
    if (this.step == (this.questions.length - 1)) return;
    this.step++;
  }

}
