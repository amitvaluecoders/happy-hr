import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { APPCONFIG } from '../../../config';
import { EmployeeIntroduction } from '../induction.service';
import { CoreService } from '../../../shared/services/core.service';
import { MdDialogRef, MdDialog, MdSnackBar } from '@angular/material';
import { ApprovalPopupComponent } from '../approval-popup/approval-popup.component';

@Component({
  selector: 'app-contract',
  templateUrl: './contract.component.html',
  styleUrls: ['./contract.component.scss']
})
export class ContractApprovalComponent implements OnInit {
  public progress;
  public contract = { "userID": "", "contractType": "", "contracts": [] };
  public isProcessing: boolean;

  constructor(public employeeService: EmployeeIntroduction, public coreService: CoreService,
    public router: Router, public dialog: MdDialog) { }

  ngOnInit() {
    this.progressBarSteps();
    APPCONFIG.heading = "Contract approval";
    this.getContractDetail();
  }
  progressBarSteps() {
    this.progress = {
      progress_percent_value: 50,
      total_steps: 4,
      current_step: 2,
      circle_container_width: 25,
      steps: [
        { num: 1, data: 'Complete your profile' },
        { num: 2, data: 'Contract approval', active: true },
        { num: 3, data: 'Position description approval' },
        { num: 4, data: 'Policy approval' },        
        // { num: 5, data: 'Questionnaire' }
      ]
    }
  }

  getContractDetail() {
    this.isProcessing = true;
    this.employeeService.getContract().subscribe(
      res => {
        if (res.code == 200) {
          this.isProcessing = false;
          this.contract = res.data;
        }
      },
      error => {
        this.isProcessing = false;
        this.coreService.notify("Error", error.message, 0)
      }
    )
  }

  contractApproval() {
    let dialogRef = this.dialog.open(ApprovalPopupComponent);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.isProcessing = true;
        let reqBody = {
          "employeeUserID": this.contract.userID,
          "contractID": this.contract.contracts[0]['companyContractID'],
          "status": "Accept"
        }
        this.employeeService.approveContract(reqBody).subscribe(
          res => {
            if (res.code == 200) {
              this.isProcessing = false;
              this.coreService.notify("Successful", res.message, 1);
              this.router.navigate(['/user/employee-induction/position-description']);
            }
            else this.coreService.notify("Unsuccessful", res.message, 0);
          },
          error => {
            this.isProcessing = false;
            if (error.code == 400) {
              return this.coreService.notify("Unsuccessful", error.message, 0);
            }
            this.coreService.notify("Error", "Error while getting position details.", 0)
          }
        )
      }
    });
  }
}
