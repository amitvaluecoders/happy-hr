import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CompleteProfileComponent } from './complete-profile/complete-profile.component';
import { PolicyDocumentsComponent } from './policy-documents/policy-documents.component';
import { PositionDescriptionComponent } from './position-description/position-description.component';
import { ContractApprovalComponent } from './contract-approval/contract.component';
import { EmpolyeeQuestionnairesComponent } from './empolyee-questionnaires/empolyee-questionnaires.component';

export const InductionRoutes: Routes = [
    { path: '', component: CompleteProfileComponent },
    { path: 'own/:key', component: CompleteProfileComponent },
    { path: 'policy-approval', component:PolicyDocumentsComponent },
    { path: 'position-description', component:PositionDescriptionComponent },
    { path: 'contract-approval', component:ContractApprovalComponent},
    // { path: 'questionnaires', component:EmpolyeeQuestionnairesComponent}
];

export const inductuionModuleRouter = RouterModule.forChild(InductionRoutes);
