
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { APPCONFIG } from '../../../config';
import { EmployeeIntroduction } from '../induction.service';
import { CoreService } from '../../../shared/services/core.service';
import { MdDialogRef, MdDialog, MdSnackBar } from '@angular/material';
import { ApprovalPopupComponent } from '../approval-popup/approval-popup.component';
import { PolicyDocumentDetailsPopupComponent } from './policy-document-details-popup/policy-document-details-popup.component';
@Component({
  selector: 'app-policy-documents',
  templateUrl: './policy-documents.component.html',
  styleUrls: ['./policy-documents.component.scss']
})
export class PolicyDocumentsComponent implements OnInit {
  public tglbutton=[];
  public progress;
  public collapse = {};
  completionDate: any;
  public hrPolicy=[];
  public generalHrPolicy=[];
  public performanceManagement=[];
  public policies:any;
  public viewDials;
  public actionProcessing=false;
  public isProcessing=false;
  public hrToggle=false;
  public ownerToggle = false;

  constructor(public employeeService: EmployeeIntroduction, public coreService: CoreService,
    public router: Router, public dialog: MdDialog) { }


  ngOnInit() {    
    this.progressBarSteps();
    this.getPolicies();
    APPCONFIG.heading = "Policy approval";
    let date = new Date();
    this.viewDials = false;
  }

  getPolicies(){
    this.isProcessing = true;
    this.employeeService.getPolicies().subscribe(
      d => {
        if (d.code == 200) {
          this.isProcessing = false;
          this.hrPolicy=d.data['HR policy']?d.data['HR policy']:[];
          this.generalHrPolicy=d.data['General HR policy']?d.data['General HR policy']:[];   
          this.performanceManagement=d.data['Performance management']?d.data['Performance management']:[];
        }
      },
      error => {
        this.isProcessing = false;
        this.coreService.notify("Error", error.message, 0)
      }
    )
  }

  onAcceptPolicies(){
    this.actionProcessing = true;
    this.employeeService.acceptAllPolicies().subscribe(
      d => {
        if (d.code == 200) {
          if (localStorage.getItem('happyhr-userInfo')) {
            let userInfo = JSON.parse(localStorage.getItem('happyhr-userInfo'));
            userInfo['incompletePage'] = -1;
            localStorage.setItem('happyhr-userInfo', JSON.stringify(userInfo));
          }
          let url="/app/employee";
          this.router.navigate([url]);  
        }
        else this.actionProcessing=false;
      },
      error => {
        this.actionProcessing = false;
        this.coreService.notify("Error", "Error while getting contract details.", 0)
      }
    )
  }

  progressBarSteps() {
    this.progress = {
      progress_percent_value: 100,
      total_steps: 4,
      current_step: 4,
      circle_container_width: 25,
      steps: [
        // {num:1,data:"Select Role"},
        { num: 1, data: 'Complete your profile' },
        { num: 2, data: 'Contract approval' },
        { num: 3, data: 'Position description approval' },
        { num: 4, data: 'Policy approval', active: true },
        // { num: 5, data: 'Questionnaire' }
      ]
    }
  }
  provideApproval() {
    let dialogRef = this.dialog.open(ApprovalPopupComponent);
    // dialogRef.componentInstance.moduleName='';
    dialogRef.afterClosed().subscribe(result => {
      if (result) {

      }
    });
  }

  onPolicyDetails(c){PolicyDocumentDetailsPopupComponent 
    let dialogRef = this.dialog.open(PolicyDocumentDetailsPopupComponent);
    console.log("hahha",c)
    dialogRef.componentInstance.policyID=c.companyPolicyDocumentID;
    dialogRef.afterClosed().subscribe(result => {
      if (result) {

      }
    });
  }


}
