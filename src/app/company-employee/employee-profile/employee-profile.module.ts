import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EmployeeProfileComponent } from './employee-profile.component';
import { EmployeeProfileRoutingModule } from './employee-profile.routing';
@NgModule({
    imports:[
        CommonModule,
        EmployeeProfileRoutingModule        
    ],
    exports:[],
    declarations:[EmployeeProfileComponent]

})
export class EmployeeProfileModule {

}