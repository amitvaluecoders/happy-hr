import { Component, OnInit } from '@angular/core';
import { APPCONFIG } from '../../config';
@Component({
  selector: 'app-employee-profile',
  templateUrl: './employee-profile.component.html',
  styleUrls: ['./employee-profile.component.scss']
})
export class EmployeeProfileComponent implements OnInit {
public collapse={};
  constructor() { }

  ngOnInit() {
    APPCONFIG.heading="Employee profile";
  }

}
