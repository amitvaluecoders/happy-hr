import {  ModuleWithProviders  } from '@angular/core';
import {  RouterModule,Routes  } from '@angular/router';
import { EmployeeProfileComponent } from './employee-profile.component';

export const EmployeeProfileRoutes: Routes = [
    {
        path: '',
        canActivate:[],
        children: [
           
            { path:'', component:EmployeeProfileComponent },

                        
        ]
    }
];

export const EmployeeProfileRoutingModule = RouterModule.forChild(EmployeeProfileRoutes);
