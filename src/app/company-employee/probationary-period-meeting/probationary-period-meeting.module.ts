import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProbationaryPeriodMeetingModuleRouter } from './probationary-period-meeting.routing';
import { MeetingResultComponent } from './meeting-result/meeting-result.component';
import { TerminationLetterComponent } from './termination-letter/termination-letter.component';
import { ProbationaryPeriodMeetingService } from './probationary-period-meeting.service';
import { SharedModule } from '../../shared/shared.module';


@NgModule({
  imports: [
    SharedModule,
    CommonModule,
    ProbationaryPeriodMeetingModuleRouter
  ],
  declarations: [MeetingResultComponent, TerminationLetterComponent],
  providers:[ProbationaryPeriodMeetingService]
})
export class ProbationaryPeriodMeetingModule { }
