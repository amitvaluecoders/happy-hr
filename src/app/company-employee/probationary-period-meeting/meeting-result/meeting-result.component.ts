import { Component, OnInit } from '@angular/core';
import { APPCONFIG } from '../../../config';
import { ProbationaryPeriodMeetingService } from '../probationary-period-meeting.service';
import { CoreService } from '../../../shared/services/core.service';

@Component({
  selector: 'app-meeting-result',
  templateUrl: './meeting-result.component.html',
  styleUrls: ['./meeting-result.component.scss']
})
export class MeetingResultComponent implements OnInit {

  public probationDetails;
  public isProcessing=false;
  constructor(
    private probationService:ProbationaryPeriodMeetingService,
    private coreService:CoreService
  ) { }

  ngOnInit() {
    APPCONFIG.heading = "Probationary meeting";
    this.getProbationDetails();
  }


  onTime(time){return new Date(time);}

  //GET PROBATION DETAILS
  getProbationDetails(){
    this.isProcessing=true;
    this.probationService.getProbationDetails()
    .subscribe(
      res=>{
        // console.log(res)
        if(res.code==200){
          this.isProcessing=false;
          this.probationDetails=res.data;
        }
      },
      error=>{
        console.log(error)
        // this.isProcessing=false;
        this.coreService.notify("Unsuccessful", error.message, 0)
      }
    );
  }

}
