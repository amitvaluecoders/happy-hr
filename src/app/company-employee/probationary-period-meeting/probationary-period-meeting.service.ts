import { Injectable } from '@angular/core';
import { RestfulLaravelService } from "../../shared/services/restful-laravel.service";

@Injectable()
export class ProbationaryPeriodMeetingService {

  constructor(private restfulService: RestfulLaravelService) { }

  //GET PROBATION MEETING DETAILS
  getProbationDetails() {
    return this.restfulService.get('employee/probationary-meeting-data');
  }
}


