import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MeetingResultComponent } from './meeting-result/meeting-result.component';
import { TerminationLetterComponent } from './termination-letter/termination-letter.component';

export const ProbationaryPeriodMeetingPagesRoutes: Routes = [
    { path: '', component: MeetingResultComponent },
    { path: 'termination-letter', component:TerminationLetterComponent },
];

export const ProbationaryPeriodMeetingModuleRouter = RouterModule.forChild(ProbationaryPeriodMeetingPagesRoutes);
