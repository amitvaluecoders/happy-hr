import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployeeAfterTerminationContractComponent } from './employee-after-termination-contract.component';

describe('EmployeeAfterTerminationContractComponent', () => {
  let component: EmployeeAfterTerminationContractComponent;
  let fixture: ComponentFixture<EmployeeAfterTerminationContractComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmployeeAfterTerminationContractComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployeeAfterTerminationContractComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
