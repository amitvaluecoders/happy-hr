import { Injectable } from '@angular/core';
import { RestfulLaravelService } from "../../shared/services/restful-laravel.service";

@Injectable()
export class EmployeeAfterTerminationService {

  constructor(private restfulService: RestfulLaravelService) { }

  getExitSurveyList() {
    return this.restfulService.get('employee/employee-exit-survey-list');
  }

  getExitSurveyDetail() {
    return this.restfulService.get(`employee/employee-exit-survey-detail`);
  }

  saveExitSurveyResponse(reqBody) {
    return this.restfulService.post('employee/save-exit-survey-response', reqBody);
  }
}
