import { ModuleWithProviders } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EmployeeOffboardChecklistComponent } from './employee-offboard-checklist/employee-offboard-checklist.component';
import { EmployeeAfterTerminationContractComponent } from './employee-after-termination-contract/employee-after-termination-contract.component';


export const EmployeeAfterTerminationPagesRoutes: Routes = [
    { path: '', component: EmployeeOffboardChecklistComponent },
    { path: 'contract', component: EmployeeAfterTerminationContractComponent },
];

export const EmployeeAfterTerminationRoutesModule = RouterModule.forChild(EmployeeAfterTerminationPagesRoutes);
