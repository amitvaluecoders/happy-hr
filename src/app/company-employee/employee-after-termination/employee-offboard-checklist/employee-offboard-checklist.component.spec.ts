import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployeeOffboardChecklistComponent } from './employee-offboard-checklist.component';

describe('EmployeeOffboardChecklistComponent', () => {
  let component: EmployeeOffboardChecklistComponent;
  let fixture: ComponentFixture<EmployeeOffboardChecklistComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmployeeOffboardChecklistComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployeeOffboardChecklistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
