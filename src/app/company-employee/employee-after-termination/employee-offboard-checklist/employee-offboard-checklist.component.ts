import { Component, OnInit } from '@angular/core';
import { APPCONFIG } from '../../../config';
import { Router, ActivatedRoute } from '@angular/router';
import { EmployeeAfterTerminationService } from '../employee-after-termination.service';
import { CoreService } from '../../../shared/services/core.service';

@Component({
  selector: 'app-employee-offboard-checklist',
  templateUrl: './employee-offboard-checklist.component.html',
  styleUrls: ['./employee-offboard-checklist.component.scss']
})
export class EmployeeOffboardChecklistComponent implements OnInit {
  isProcessing: boolean;
  isSubmitting: boolean;
  survey = {
    "exit_survey": {
      "companyExitSurveyID": "", "question": [], "title": "",
      "assignedBy": { "userID": "", "name": "", "imageUrl": "", "position": "" }
    }, "equipment": []

  };
  isDisable = true;
  submitted = false;

  public collapse = {};

  constructor(public employeeService: EmployeeAfterTerminationService, public coreService: CoreService, public router: Router) { }

  ngOnInit() {
    APPCONFIG.heading = "Employee off boarding checklist";
    this.getSurveyDetail();
  }

  getSurveyDetail() {
    this.isProcessing = true;
    this.employeeService.getExitSurveyDetail().subscribe(
      res => {
        this.isProcessing = false;
        if (res.code == 200) {
          console.log(res);
          this.survey = res.data;
        }
        else return this.coreService.notify('Unsuccessful', res.message, 0);
      },
      error => {
        this.isProcessing = false;
        if (error.code == 400) {
          return this.coreService.notify('Unsuccessful', error.message, 0);
        }
        this.coreService.notify('Unsuccessful', "Error while getting exit survey detail", 0);
      }
    )
  }

  isValid() {
    for (let i = 0; i < this.survey.exit_survey.question.length; i++) {
      if (!this.survey.exit_survey.question[i].answer.length || !this.survey.exit_survey.question[i].answer.trim().length) {
        return false;
      }
    }
    return true;
  }

  submitAnswer(isValid) {
    if (!isValid) return;
    let reqBody = { "companyExitSurveyID": this.survey.exit_survey.companyExitSurveyID, "data": this.survey.exit_survey.question };
    this.isSubmitting = true;
    this.employeeService.saveExitSurveyResponse(reqBody).subscribe(
      res => {
        this.isSubmitting = false;
        if (res.code == 200) {
          this.coreService.notify('Successful', res.message, 1);
          // this.router.navigate(['/app/employee/survey/survey-result', this.survey.exit_survey.companyExitSurveyID])
        }
        else return this.coreService.notify('Unsuccessful', res.message, 0);
      },
      error => {
        this.isSubmitting = false;
        if (error.code == 400) {
          return this.coreService.notify('Unsuccessful', error.message, 0);
        }
        this.coreService.notify('Unsuccessful', "Error while submitting answer", 0);
      }
    )
  }
}
