import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { MaterialModule } from '@angular/material';
import { EmployeeOffboardChecklistComponent } from './employee-offboard-checklist/employee-offboard-checklist.component';
import { EmployeeAfterTerminationContractComponent } from './employee-after-termination-contract/employee-after-termination-contract.component';
import { EmployeeAfterTerminationRoutesModule } from './employee-after-termination.routing'
import { EmployeeAfterTerminationService } from "./employee-after-termination.service";

@NgModule({
  imports: [
    EmployeeAfterTerminationRoutesModule,
    CommonModule,
    MaterialModule,
    FormsModule
  ],
  declarations: [EmployeeOffboardChecklistComponent, EmployeeAfterTerminationContractComponent],
  providers: [EmployeeAfterTerminationService]
})
export class EmployeeAfterTerminationModule { }
