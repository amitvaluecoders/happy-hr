import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from "@angular/material";
import { EmployeeOrganizationChartComponent, OrgnisationChartDialog } from './employee-organization-chart/employee-organization-chart.component';
import { EmployeeOrganizationChartService } from "./employee-organization-chart.service";
import { EmployeeOrgChartRoutingModules } from "./employee-organization-chart.routing";
import { SharedModule } from "../../shared/shared.module";

@NgModule({
  imports: [
    CommonModule,
    EmployeeOrgChartRoutingModules,
    MaterialModule,
    SharedModule
  ],
  declarations: [EmployeeOrganizationChartComponent, OrgnisationChartDialog],
  entryComponents: [OrgnisationChartDialog],
  providers: [EmployeeOrganizationChartService]
})
export class EmployeeOrganizationChartModule { }
