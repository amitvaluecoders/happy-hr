import { RouterModule, Routes } from '@angular/router';
import { EmployeeOrganizationChartComponent } from "./employee-organization-chart/employee-organization-chart.component";

export const EmployeeOrgChartPagesRoutes: Routes = [
    { path: '', component: EmployeeOrganizationChartComponent }
];
export const EmployeeOrgChartRoutingModules = RouterModule.forChild(EmployeeOrgChartPagesRoutes);
