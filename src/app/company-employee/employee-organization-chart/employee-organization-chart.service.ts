import { Injectable } from '@angular/core';
import { RestfulLaravelService } from "../../shared/services/restful-laravel.service";

@Injectable()
export class EmployeeOrganizationChartService {

  constructor(private restfulService: RestfulLaravelService) { }

  getOrganizationChart() {
    return this.restfulService.get('org-chart-get');
  }
}
