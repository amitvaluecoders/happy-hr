import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EmployeeSettingNotificationsComponent } from './employee-setting-notifications/employee-setting-notifications.component';
import { empSettingRouterModule } from './employee-setting.routing';
import { EmployeeSettingService } from './employee-setting.service';


@NgModule({
  imports: [
    CommonModule,
    empSettingRouterModule
  ],
  providers : [EmployeeSettingService],
  declarations: [EmployeeSettingNotificationsComponent]
})
export class EmployeeSettingModule { }
