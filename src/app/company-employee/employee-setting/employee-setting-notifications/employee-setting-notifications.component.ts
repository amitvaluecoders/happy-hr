import { Component, OnInit } from '@angular/core';
import { APPCONFIG } from '../../../config';
import { CoreService } from '../../../shared/services/core.service';
import { Router, ActivatedRoute } from '@angular/router';
import * as moment from 'moment';
import { EmployeeSettingService } from '../employee-setting.service';

@Component({
  selector: 'app-employee-setting-notifications',
  templateUrl: './employee-setting-notifications.component.html',
  styleUrls: ['./employee-setting-notifications.component.scss']
})
export class EmployeeSettingNotificationsComponent implements OnInit {

  public notiList = [];
  public isProcessing : boolean;
  constructor(public coreService: CoreService , public router:Router
  ,public employeeSettingService : EmployeeSettingService) { }
  public params = { "offset":0 , "limit":20};
  ngOnInit() {
    APPCONFIG.heading="Notifications";  
     this.getNotiList();
    
  }



  formateDate(v){
      return moment(v).calendar();
  }

  getNotiList(){
      this.isProcessing = true;
   this.employeeSettingService.getAllNotification(JSON.stringify(this.params)).subscribe(
      res => {
        this.isProcessing = false;
        if (res.code == 200) {
          this.params.offset = this.params.offset + this.params.limit;
          Array.prototype.push.apply(this.notiList, res.data)
        }
        else return this.coreService.notify("Unsuccessful", res.message, 0);
      },
      err => {
        this.isProcessing = false;
        if (err.code == 400) {
          return this.coreService.notify("Unsuccessful", err.message, 0);
        }
        this.coreService.notify("Unsuccessful", "Error while getting data.", 0)
      }
    );
  }

}
