import { RouterModule, Routes } from '@angular/router';
import { EmployeeSettingNotificationsComponent } from './employee-setting-notifications/employee-setting-notifications.component';
export const EmpsettingRoutes:Routes=[
    { path:'', component:EmployeeSettingNotificationsComponent },
    { path:'notifications', component:EmployeeSettingNotificationsComponent }
];

export const empSettingRouterModule=RouterModule.forChild(EmpsettingRoutes);