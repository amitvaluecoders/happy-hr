import { Injectable } from '@angular/core';
import { RestfulLaravelService } from "../../shared/services/restful-laravel.service";


@Injectable()
export class EmployeeSettingService {

  constructor(private restfulService: RestfulLaravelService) { }

  getAllNotification(params) {
    return this.restfulService.get(`notification-list/${encodeURIComponent(params)}`);
  }

}
