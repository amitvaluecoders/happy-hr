import { Component, OnInit } from '@angular/core';
import {APPCONFIG } from '../../../config';
import { EmployeeChecklistService } from '../employee-checklist.service';
import { CoreService } from '../../../shared/services/core.service';

@Component({
  selector: 'app-employee-on-boarding-checklist',
  templateUrl: './employee-on-boarding-checklist.component.html',
  styleUrls: ['./employee-on-boarding-checklist.component.scss']
})
export class EmployeeOnBoardingChecklistComponent implements OnInit {
  
  collapse = {};
  public checklistData=[];
  public isProcessing=false;

  constructor(
    private employeeChecklistService:EmployeeChecklistService,
    public coreService: CoreService
  ) { }

  ngOnInit() {
    APPCONFIG.heading="On boarding checklist";
    this.getEmployeeChecklist();
  }

  //GET EMPLOYEE ON BOARD CHECKLIST
  getEmployeeChecklist(){
    this.isProcessing=true;
    this.employeeChecklistService.getOnBoardCheckList()
      .subscribe(data=>{
        // console.log(data);
        this.isProcessing=false;
        if(data.code==200){
          this.checklistData=data.data;
        }
      },
    error=>{
      console.log(error)
      this.isProcessing=false;
      this.coreService.notify("Unsuccessful", "Error while getting checklist details", 0)
    }
  );
  }

}
