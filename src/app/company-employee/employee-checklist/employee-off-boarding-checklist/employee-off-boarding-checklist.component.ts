import { Component, OnInit } from '@angular/core';
import {APPCONFIG } from '../../../config';
import { EmployeeChecklistService } from '../employee-checklist.service';
import { CoreService } from '../../../shared/services/core.service';

@Component({
  selector: 'app-employee-off-boarding-checklist',
  templateUrl: './employee-off-boarding-checklist.component.html',
  styleUrls: ['./employee-off-boarding-checklist.component.scss']
})
export class EmployeeOffBoardingChecklistComponent implements OnInit {

  collapse = {};
  public checklistData=[];
  public isProcessing=false;

  constructor(
    private employeeChecklistService:EmployeeChecklistService,
    public coreService: CoreService
  ) { }

  ngOnInit() {
    APPCONFIG.heading="Off boarding checklist";
    this.getEmployeeChecklist();
  }

  //GET EMPLOYEE ON BOARD CHECKLIST
  getEmployeeChecklist(){
    this.isProcessing=true;
    this.employeeChecklistService.getOffBoardCheckList()
      .subscribe(data=>{
        // console.log(data);
        this.isProcessing=false;
        if(data.code==200){
          this.checklistData=data.data;
        }
      },
    error=>{
      console.log(error)
      this.isProcessing=false;
      this.coreService.notify("Unsuccessful", "Error while getting checklist details", 0)
    }
  );
  }

}
