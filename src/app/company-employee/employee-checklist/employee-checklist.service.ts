import { Injectable } from '@angular/core';
import { RestfulLaravelService } from "../../shared/services/restful-laravel.service";

@Injectable()
export class EmployeeChecklistService {

  constructor(private restfulService: RestfulLaravelService) { }

  //GET ON BOARD EMPLOYEE CHECKLIST
  getOnBoardCheckList() {
    return this.restfulService.get('employee/get-employee-checklist-onnboarding');
  }

  //GET OFF BOARD EMPLOYEE CHECKLIST
  getOffBoardCheckList() {
    return this.restfulService.get('employee/get-employee-checklist-offboarding');
  }
}
