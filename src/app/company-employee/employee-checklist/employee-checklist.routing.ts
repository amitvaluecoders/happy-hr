import { Routes, RouterModule }  from '@angular/router';
import { EmployeeOnBoardingChecklistComponent } from './employee-on-boarding-checklist/employee-on-boarding-checklist.component';
import { EmployeeOffBoardingChecklistComponent } from './employee-off-boarding-checklist/employee-off-boarding-checklist.component';

export const EmployeeChecklistPagesRoutes: Routes = [
    {
        path: '',
        children: [
            { path: '', component: EmployeeOnBoardingChecklistComponent },
            { path: 'offBoarding', component: EmployeeOffBoardingChecklistComponent },
          
        ]
    }
];

export const EmployeeChecklistPagesRoutingModule = RouterModule.forChild(EmployeeChecklistPagesRoutes);
