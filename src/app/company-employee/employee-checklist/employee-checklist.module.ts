import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EmployeeChecklistService } from './employee-checklist.service'

import { EmployeeChecklistPagesRoutingModule } from './employee-checklist.routing';
import { EmployeeOnBoardingChecklistComponent } from './employee-on-boarding-checklist/employee-on-boarding-checklist.component';
import { EmployeeOffBoardingChecklistComponent } from './employee-off-boarding-checklist/employee-off-boarding-checklist.component';

@NgModule({
  imports: [
    CommonModule,
    EmployeeChecklistPagesRoutingModule
  ],
  declarations: [EmployeeOnBoardingChecklistComponent, EmployeeOffBoardingChecklistComponent],
  providers:[EmployeeChecklistService]
})
export class EmployeeChecklistModule { }
