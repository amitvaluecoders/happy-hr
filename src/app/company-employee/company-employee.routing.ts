import { Routes, RouterModule } from '@angular/router';
import { EmployeeGuardService } from './employee_guard.service';
import { ChangePasswordComponent } from './change-password/change-password.component';

export const CompanyEmployeePagesRoutes: Routes = [
    {
        path: '',
        canActivate: [EmployeeGuardService],
        children: [

            { path: '', loadChildren: './employee-dashboard/employee-dashboard.module#EmployeeDashboardModule' },
            { path: 'change-password', component: ChangePasswordComponent },
            { path: 'development-plan', loadChildren: './employee-development-plan/employee-development-plan.module#EmployeeDevelopmentPLanModule' },
            { path: 'profile', loadChildren: './induction/induction.module#InductionModule' },
            { path: 'employee-profile', loadChildren: './employee-profile/employee-profile.module#EmployeeProfileModule' },
            { path: 'ohs', loadChildren: './employee-ohs/employee-ohs.module#EmployeeOhsModule' },
            { path: 'trophy', loadChildren: './emp-trophy/emp-trophy.module#EmpTrophyModule' },
            { path: 'survey', loadChildren: './employee-survey/employee-survey.module#EmployeeSurveyModule' },
            { path: 'leaves', loadChildren: './employee-leaves/employee-leaves.module#EmployeeLeavesModule' },
            { path: 'disciplinary-meeting', loadChildren: './employee-disciplinary-meeting/employee-disciplinary-meeting.module#EmployeeDisciplinaryMeetingModule' },
            { path: 'message', loadChildren: './employee-messages/employee-messages.module#EmployeeMessagesModule' },
            { path: 'performance-improvement-plan', loadChildren: './pip-employee/pip-employee.module#PipEmployeeModule' },
            { path: 'behavioral-performance-improvement-plan', loadChildren: './bpip-employee/bpip-employee.module#BpipEmployeeModule' },
            { path: 'grievance', loadChildren: './employee-grievance/employee-grievance.module#EmployeeGrievanceModule' },
            { path: 'training-day', loadChildren: './employee-training-day/employee-training-day.module#EmployeeTrainingDayModule' },
            { path: 'probationary-period-meeting', loadChildren: './probationary-period-meeting/probationary-period-meeting.module#ProbationaryPeriodMeetingModule' },
            { path: 'policy-documents', loadChildren: './employee-policy-documents/employee-policy-documents.module#EmployeePolicyDocumentsModule' },
            { path: 'contracts', loadChildren: './employee-contracts/employee-contracts.module#EmployeeContractsModule' },
            { path: 'checklist', loadChildren: './employee-checklist/employee-checklist.module#EmployeeChecklistModule' },
            { path: 'position-description', loadChildren: './employee-position-description/employee-position-description.module#EmployeePositionDescriptionModule' },
            { path: 'after-termination', loadChildren: './employee-after-termination/employee-after-termination.module#EmployeeAfterTerminationModule' },
            { path: 'performance-appraisal', loadChildren: './employee-performance-appraisal/employee-performance-appraisal.module#EmployeePerformanceAppraisalModule' },
            { path: 'setting', loadChildren: './employee-setting/employee-setting.module#EmployeeSettingModule' },
            { path: 'organisation-chart', loadChildren: './employee-organization-chart/employee-organization-chart.module#EmployeeOrganizationChartModule' }

        ]
    }
];
export const CompanyEmployeePagesRoutingModule = RouterModule.forChild(CompanyEmployeePagesRoutes);