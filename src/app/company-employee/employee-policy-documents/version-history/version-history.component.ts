import { Component, OnInit } from '@angular/core';
import { APPCONFIG } from '../../../config';
import { policyEmployeeService } from '../employee-policy.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { CoreService } from '../../../shared/services/core.service';
import { MdSnackBar, MdDialog, MdDialogRef } from '@angular/material';

@Component({
  selector: 'app-version-history',
  templateUrl: './version-history.component.html',
  styleUrls: ['./version-history.component.scss']
})
export class VersionHistoryComponent implements OnInit {

 
   public relPath:string;
AppConfig: any;
public versionList=[];
public tabing=false;
public policyType='';
public isDetailProcessing=false;
public policy={};
public currentTab=1.0;
public isProcessing=false;
   constructor( public policyService: policyEmployeeService,public dialog: MdDialog,
               public router: Router, public activatedRoute: ActivatedRoute,
               public coreService: CoreService) { }

  ngOnInit() {
    let role = this.coreService.getRole();
    this.relPath =(role &&  role == 'companyContractor') ?'sub-contractor':'employee';
    this.activatedRoute.snapshot.params['id'];
    this.activatedRoute.snapshot.params['PolicyId']
    
    APPCONFIG.heading="Policy versions"
    this.getVersionList();
  }
 
 getVersionList(){
   this.policyType="";
   let reqId="";
    if( this.activatedRoute.snapshot.params['id']) {
      this.policyType="ce";
      reqId=this.activatedRoute.snapshot.params['id'];
    }
    if( this.activatedRoute.snapshot.params['PolicyId']){
      this.policyType="ca";
      reqId=this.activatedRoute.snapshot.params['PolicyId'];      
    } 
    
     this.isProcessing=true;       
     this.policyService.getVersionList(this.policyType,reqId).subscribe(
          d => {
              if(d.code == "200"){
              for(let i of d.data){
                if(i.status=='Active'){
                  i['localActive']=true;
                  this.getVersionDetails(i.companyPolicyDocumentID); 
                } 
                else i['localActive']=false;
              }
                this.versionList=d.data;
                console.log(this.versionList);
              }   
              else this.coreService.notify("Unsuccessful",d.message, 0); 
          },
          error =>{
            this.coreService.notify("Unsuccessful", "Error while getting notes", 0);
            this.isProcessing=false
          } 
      )
 }

 getVersionDetails(companyPolicyDocumentID){
    this.isDetailProcessing=true;
        let isEmployee=true;
        if(this.policyType=='ca')isEmployee=false;
      let reqBody={isHHRContract:isEmployee,id:companyPolicyDocumentID};
       this.policyService.getDetails(reqBody).subscribe(
          d => {
              if (d.code == "200") {
                this.policy=d.data;
              this.isProcessing=false;                
                this.isDetailProcessing=false;        
              }
              else this.coreService.notify("Unsuccessful", "Error while getting contracts", 0);    
          },
          error =>  this.coreService.notify("Unsuccessful", "Error while getting contracts", 0)
      )
 }

onVersionSelect(companyPolicyDocumentID){
  console.log(this.versionList)
    this.versionList.forEach(element => {
      if(element.companyPolicyDocumentID==companyPolicyDocumentID){
        element['localActive']=true;
        // console.log("cccc",element.companyContractID)
        this.getVersionDetails(element.companyPolicyDocumentID);
      }
      else  element['localActive']=false;
    });
}

//  onRestoreVersion(id){
//     this.isProcessing=true;       
//      this.contractsService.restoreContractVersion(id).subscribe(
//           d => {
//               this.isProcessing=false;
//               if(d.code == "200") {
//                 this.coreService.notify("Successful",d.message, 1);  
//                 this.getVersionList();
//               }
//               else this.coreService.notify("Unsuccessful",d.message, 0); 
//           },
//           error =>{
//             this.coreService.notify("Unsuccessful", "Error while getting notes", 0);
//             this.isProcessing=false
//           } 
//       )
//  }

}
