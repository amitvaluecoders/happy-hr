import { Component, OnInit } from '@angular/core';
import { APPCONFIG } from '../../../config';
import { policyEmployeeService } from '../employee-policy.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { CoreService } from '../../../shared/services/core.service';
import { MdSnackBar, MdDialog, MdDialogRef } from '@angular/material';
import { ResponseNoteComponent } from '../../../shared/components/response-note/response-note.component';

@Component({
  selector: 'app-procedural-documents-detail',
  templateUrl: './procedural-documents-detail.component.html',
  styleUrls: ['./procedural-documents-detail.component.scss']
})
export class ProceduralDocumentsDetailComponent implements OnInit {

  public relPath :string;
  public isProcessing=false;
  public companyLogo:any;
  role:any;
  public document={};
  constructor( public policyService: policyEmployeeService,public dialog: MdDialog,
               public router: Router, public activatedRoute: ActivatedRoute,
               public coreService: CoreService) { }

  ngOnInit() {
    this.role = this.coreService.getRole();
    this.relPath =(this.role &&  this.role == 'companyContractor') ?'sub-contractor':'employee';
    this.companyLogo = this.coreService.getCompanyLogo();    
    APPCONFIG.heading="Procedural document detail";
    this.getDetails();
  }
  
   getDetails(){
      this.isProcessing=true;
          this.policyService.getProceduralDocumentDetails(this.activatedRoute.snapshot.params['id']).subscribe(
            d => {
              if (d.code == "200") {
                this.document=d.data;
                this.isProcessing=false;
              }
              else{
                this.coreService.notify("Unsuccessful", d.message, 0);
                this.isProcessing=false;
              } 
            },
            error => this.coreService.notify("Unsuccessful", error.message, 0), 
        )
    }
}
