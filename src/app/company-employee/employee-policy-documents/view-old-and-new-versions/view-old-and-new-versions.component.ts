import { Component, OnInit } from '@angular/core';
import { MdSnackBar, MdDialog, MdDialogRef } from '@angular/material';
import { APPCONFIG } from '../../../config';
import { policyEmployeeService } from '../employee-policy.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { CoreService } from '../../../shared/services/core.service';
@Component({
  selector: 'app-view-old-and-new-versions',
  templateUrl: './view-old-and-new-versions.component.html',
  styleUrls: ['./view-old-and-new-versions.component.scss']
})
export class ViewOldAndNewVersionsComponent implements OnInit {

  public relPath : string;
  public isProcessing=false;
  public policies:any;
  public loggedInUser:any;
  public isActionProcessing=false;
  constructor( public policyService: policyEmployeeService,public dialog: MdDialog,
               public router: Router, public activatedRoute: ActivatedRoute,
               public coreService: CoreService) { }

  ngOnInit() {
    let role = this.coreService.getRole();
    this.relPath =(role &&  role == 'companyContractor') ?'sub-contractor':'employee';
    APPCONFIG.heading="Accept/reject for happy HR policy";
    this.getComparisonDetails();
      this.loggedInUser =  JSON.parse(localStorage.getItem('happyhr-userInfo'));

  }

     getComparisonDetails(){
       this.isProcessing=true;
         this.policyService.getComparisonDetails(this.activatedRoute.snapshot.params['id']).subscribe(
            d => {
              if (d.code == "200") {
                  this.policies=d.data;
                 this.isProcessing=false;       
              }
              if (d.code == "201") {
                this.coreService.notify(" ", d.message, 0);
                this.policies=d.data;
               this.isProcessing=false;       
            }
              else{
                this.coreService.notify("Unsuccessful", d.message, 0);
                this.isProcessing=false;
              } 
            },
            error => this.coreService.notify("Unsuccessful", error.message, 0), 
        )
    }

   onAccept(){
       this.isActionProcessing=true;
       let id=this.activatedRoute.snapshot.params['id'];
         let reqBody={
                  companyPolicyDocumentID:this.activatedRoute.snapshot.params['id'],
                  accept:true
              }
          this.SubmitAcceptReject(reqBody);
         
    }
    
  SubmitAcceptReject(reqBody){
         this.policyService.acceptPolicy(reqBody).subscribe(
            d => {
              if (d.code == "200") {
                this.coreService.notify("Successful", d.message, 1);
                this.router.navigate([`/app/${this.relPath}/policy-documents`]);
              }
              else{
                this.coreService.notify("Unsuccessful", d.message, 0);
                this.isActionProcessing=false;
              } 
            },
            error => this.coreService.notify("Unsuccessful", error.message, 0), 
        )
    }


}
