import {  ModuleWithProviders  } from '@angular/core';
import {  RouterModule,Routes  } from '@angular/router';

import { PolicyDocumentsListComponent } from './policy-documents-list/policy-documents-list.component';
import { PolicyDetailComponent } from './policy-detail/policy-detail.component';
import { VersionHistoryComponent } from './version-history/version-history.component';
import { AcceptChangeComponent } from './accept-change/accept-change.component';
import { ViewOldAndNewVersionsComponent } from './view-old-and-new-versions/view-old-and-new-versions.component';
import { ProceduralDocumentsComponent } from './procedural-documents/procedural-documents.component';
import { ProceduralDocumentsDetailComponent } from './procedural-documents-detail/procedural-documents-detail.component';

export const EmployeePolicyDocumentsPagesRoutes: Routes = [
    {
        path: '',
        canActivate:[],
        children: [
           
            { path: '', component: PolicyDocumentsListComponent},
            { path: 'detail/:id', component: PolicyDetailComponent},
            { path: 'version-history/:id', component: VersionHistoryComponent},
            { path: 'procedural-document', component: ProceduralDocumentsComponent},
            { path: 'procedural-document/detail/:id', component: ProceduralDocumentsDetailComponent},
            { path: 'version/old/new/:id', component: ViewOldAndNewVersionsComponent},
       ]
    }
];

export const EmployeePolicyDocumentsRoutingModule = RouterModule.forChild(EmployeePolicyDocumentsPagesRoutes);
