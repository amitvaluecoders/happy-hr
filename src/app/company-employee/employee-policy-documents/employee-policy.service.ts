import { Injectable } from '@angular/core';
import { RestfulLaravelService} from '../../shared/services/restful-laravel.service';

@Injectable()

export class policyEmployeeService {

  constructor(public restfulWebService:RestfulLaravelService) { }

// method to get listing of all  development plans.
   getPolicyListing(reqBody):any{
   return this.restfulWebService.get(`employee/list-policy/${reqBody}`)
  }

  getPolicyFilterData():any{
   return this.restfulWebService.get(`list-employee-policy-document-filter-data`)   
    }
  // getContractDetails(reqBody):any{
  //  return this.restfulWebService.get(`employee/employee-contract-detail/${reqBody}`)
  // }
   getDetails(reqBody):any{
    // if(reqBody.isHHRContract) return this.restfulWebService.get(`get-contract-detail/${reqBody.id}`)
    // else  
    return this.restfulWebService.get(`employee/policy-detail/${reqBody.id}`)
  }

  getVersionList(contractType,reqId){
    let url='';
    if(contractType=='ce') url=`employee/policy-version/${reqId}`;
    if(contractType=='ca') url=`get-policy-version-detail/${reqId}`;
      return this.restfulWebService.get(url)
  }

  acceptPolicy(reqBody):any{
    return this.restfulWebService.post(`employee/policy-approve`,reqBody)
  }

  getProceduralDocumentList():any{
   return this.restfulWebService.get(`employee/list-procedural-document`);
  }

  getComparisonDetails(id):any{
    return this.restfulWebService.get(`employee/policy-comparision/${id}`)
  }

  getProceduralDocumentDetails(id):any{
    return this.restfulWebService.get(`employee/employee-procedural-document-detail/${id}`)
  }

  acceptAllPolicies(reqBody):any{
    return this.restfulWebService.post(`employee/accept-all-policy`,reqBody)
  }

}
