import { Component, OnInit } from '@angular/core';
import { APPCONFIG } from '../../../config';
import { Router, ActivatedRoute } from '@angular/router';
import { EmployeeSurvey } from '../employee-survey.service';
import { CoreService } from '../../../shared/services/core.service';
import * as moment from "moment";


@Component({
  selector: 'app-employee-answer-response',
  templateUrl: './employee-answer-response.component.html',
  styleUrls: ['./employee-answer-response.component.scss']
})
export class EmployeeAnswerResponseComponent implements OnInit {
  isProcessing: boolean;
  isSubmitting: boolean;
  survey = {
    "companySurveyID": "", "employeeSurveyID": "", "data": [], "dueDate": "",
    "assignedBy": { "userID": "", "name": "", "imageUrl": "", "position": "" }
  };
  isDisable = true;
  submitted = false;
  anonymous= false;
  isAnonymous=false;

  constructor(public employeeService: EmployeeSurvey, public coreService: CoreService, public router: Router, public route: ActivatedRoute) { }

  ngOnInit() {
    APPCONFIG.heading = "Employee survey answer/response";
    if (this.route.snapshot.params['id']) {
      this.survey.companySurveyID = this.route.snapshot.params['id'];
      this.getSurveyDetail();
    }
  }

  getSurveyDetail() {
    this.isProcessing = true;
    this.employeeService.getSurveyDetail(this.survey.companySurveyID).subscribe(
      res => {
        this.isProcessing = false;
        if (res.code == 200) {
          this.survey = res.data;
          this.anonymous = (res.data.anonymous=="Yes") ? true : false;
          if(this.anonymous==true){this.isAnonymous=true;}
        }
        else return this.coreService.notify('Unsuccessful', res.message, 0);
      },
      error => {
        this.isProcessing = false;
        if (error.code == 400) {
          return this.coreService.notify('Unsuccessful', error.message, 0);
        }
        this.coreService.notify('Unsuccessful', "Error while getting survey detail", 0);
      }
    )
  }

  formatDate(date) {
    if (moment(date).isValid()) {
      return moment(date).format('DD MMM YYYY');
    }
    return 'Not specified';
  }
  formatTime(date) {
    if (moment(date).isValid()) {
      return moment(date).format('HH:mm');
    }
    return 'Not specified';
  }

  isValid() {
    for (let i = 0; i < this.survey.data.length; i++) {
      if (!this.survey.data[i].answer.length || !this.survey.data[i].answer.trim().length) {
        return false;
      }
    }
    return true;
  }

  submitAnswer() {
    this.isSubmitting = true;
    this.survey['anonymous']= (this.anonymous==true) ? 'Yes' : 'No';
    //if(this.anonymous==true){this.isAnonymous==true;}
    //this.anonymous = 
    this.employeeService.saveSurvayResponse(this.survey).subscribe(
      res => {
        this.isSubmitting = false;
        if (res.code == 200) {
          this.coreService.notify('Successful', res.message, 1);
          this.router.navigate(['/app/employee/survey/survey-result', this.survey.companySurveyID])
        }
        else return this.coreService.notify('Unsuccessful', res.message, 0);
      },
      error => {
        this.isSubmitting = false;
        if (error.code == 400) {
          return this.coreService.notify('Unsuccessful', error.message, 0);
        }
        this.coreService.notify('Unsuccessful', "Error while submitting answer", 0);
      }
    )
  }
  toggle(){

    this.anonymous = !this.anonymous;
    console.log("asdf",this.anonymous);
 }


}
