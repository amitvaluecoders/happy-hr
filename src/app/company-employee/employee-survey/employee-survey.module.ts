import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { MaterialModule } from '@angular/material';
import { EmployeeAnswerResponseComponent } from './employee-answer-response/employee-answer-response.component';
import { employeeSurveyrouting } from './employee-survey.module.routing';
import { SurveyResultComponent } from './survey-result/survey-result.component';
import { EmployeeSurvey } from "./employee-survey.service";
import { DataTableModule } from "angular2-datatable";
import { EmployeeSurveyListComponent } from './employee-survey-list/employee-survey-list.component';

@NgModule({
  imports: [
    CommonModule,
    employeeSurveyrouting,
    MaterialModule,
    DataTableModule,
    FormsModule
  ],
  declarations: [EmployeeAnswerResponseComponent, SurveyResultComponent, EmployeeSurveyListComponent],
  providers: [EmployeeSurvey]
})
export class EmployeeSurveyModule { }
