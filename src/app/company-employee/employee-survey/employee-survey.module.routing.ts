import { RouterModule, Routes } from '@angular/router';
import { EmployeeAnswerResponseComponent } from './employee-answer-response/employee-answer-response.component';
import { SurveyResultComponent } from './survey-result/survey-result.component';
import { EmployeeSurveyListComponent } from './employee-survey-list/employee-survey-list.component';
export const routes: Routes = [
    { path: '', component: EmployeeSurveyListComponent },
    { path: 'survey-response/:id', component: EmployeeAnswerResponseComponent },
    { path: 'survey-result/:id', component: SurveyResultComponent }
];

export const employeeSurveyrouting = RouterModule.forChild(routes);