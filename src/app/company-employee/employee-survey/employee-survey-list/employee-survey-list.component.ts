import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { APPCONFIG } from '../../../config';
import { EmployeeSurvey } from '../employee-survey.service';
import { CoreService } from '../../../shared/services/core.service';
import * as moment from "moment";

@Component({
  selector: 'app-employee-survey-list',
  templateUrl: './employee-survey-list.component.html',
  styleUrls: ['./employee-survey-list.component.scss']
})
export class EmployeeSurveyListComponent implements OnInit {

  public surveyList = [];
  public toggleFilter = true;
  public isProcessing: boolean;
  public searchText = '';
  public filter = { 'Pending': false, 'Completed': false };
  isChecked = false;

  constructor(public employeeService: EmployeeSurvey, public coreService: CoreService, public router: Router) { }

  ngOnInit() {
    APPCONFIG.heading = "360 degree survey";
    this.getSurveyList();
  }

  getSurveyList() {
    this.isProcessing = true;
    let params = { "search": encodeURIComponent(this.searchText), "filter": { "status": this.filter } };
    this.surveyList = [];
    this.employeeService.getSurveyList(encodeURIComponent(JSON.stringify(params))).subscribe(
      res => {
        if (res.code == 200) {
          this.isProcessing = false;
          if (res.data instanceof Array) {
            this.surveyList = res.data;
          }
        }
        else return this.coreService.notify("Unsuccessful", res.message, 0);
      },
      error => {
        this.isProcessing = false;
        if (error.code == 400) {
          return this.coreService.notify("Unsuccessful", error.message, 0);
        }
        this.coreService.notify("Unsuccessful", "Error while getting Survey list.", 0);
      }
    )
  }

  formatDate(date) {
    if (moment(date).isValid()) {
      return moment(date).format('DD MMM YYYY');
    }
    return 'Not specified';
  }

}
