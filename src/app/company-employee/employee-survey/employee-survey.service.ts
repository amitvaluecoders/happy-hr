import { Injectable } from '@angular/core';
import { RestfulLaravelService } from '../../shared/services';
import { Http, Headers, Response, RequestOptions } from '@angular/http';


@Injectable()
export class EmployeeSurvey {

    constructor(
        private restfulWebService: RestfulLaravelService) { }

    getSurveyList(params) {
        return this.restfulWebService.get(`employee/list-all-survey/${params}`);
    }

    getSurveyDetail(id) {
        return this.restfulWebService.get(`employee/previous-survey-detail/${id}`);
    }

    saveSurvayResponse(reqBody) {
        return this.restfulWebService.post('employee/save-survey-response', reqBody);
    }
}