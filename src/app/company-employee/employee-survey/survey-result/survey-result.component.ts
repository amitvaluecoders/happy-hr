import { Component, OnInit } from '@angular/core';
import { APPCONFIG } from '../../../config';
import { Router, ActivatedRoute } from '@angular/router';
import { EmployeeSurvey } from '../employee-survey.service';
import { CoreService } from '../../../shared/services/core.service';
import * as moment from "moment";

@Component({
  selector: 'app-survey-result',
  templateUrl: './survey-result.component.html',
  styleUrls: ['./survey-result.component.scss']
})
export class SurveyResultComponent implements OnInit {
  isProcessing: boolean;
  isSubmitting: boolean;
  survey = {
    "companySurveyID": "", "data": [], "dueDate": "",
    "assignedBy": { "userID": "", "name": "", "imageUrl": "", "position": "" }
  };
  noResult = true;

  constructor(public employeeService: EmployeeSurvey, public coreService: CoreService, public router: Router, public route: ActivatedRoute) { }

  ngOnInit() {
    APPCONFIG.heading = "Employee survey result"
    if (this.route.snapshot.params['id']) {
      this.survey.companySurveyID = this.route.snapshot.params['id'];
      this.getSurveyDetail();
    }
  }

  getSurveyDetail() {
    this.isProcessing = true;
    this.employeeService.getSurveyDetail(this.survey.companySurveyID).subscribe(
      res => {
        this.isProcessing = false;
        if (res.code == 200) {
          console.log(res);
          if (!res.data) return this.noResult = true;
          this.noResult = false;
          this.survey = res.data;
        }
        else return this.coreService.notify('Unsuccessful', res.message, 0);
      },
      error => {
        this.isProcessing = false;
        if (error.code == 400) {
          return this.coreService.notify('Unsuccessful', error.message, 0);
        }
        this.coreService.notify('Unsuccessful', "Error while getting survey detail", 0);
      }
    )
  }

  formatDate(date) {
    if (moment(date).isValid()) {
      return moment(date).format('DD MMM YYYY');
    }
  }
  formatTime(date) {
    if (moment(date).isValid()) {
      return moment(date).format('HH:mm');
    }
  }

}
