import { Component, OnInit } from '@angular/core';
import { APPCONFIG } from '../../../config';
import { Router, ActivatedRoute } from '@angular/router';
import { EmployeeTrainingDayService } from '../employee-training-day.service';
import { CoreService } from '../../../shared/services/core.service';
import * as moment from "moment";
import { MdDialog, MdDialogRef } from '@angular/material';

@Component({
  selector: 'app-training-plan-acknowledgement',
  templateUrl: './training-plan-acknowledgement.component.html',
  styleUrls: ['./training-plan-acknowledgement.component.scss']
})
export class TrainingPlanAcknowledgementComponent implements OnInit {
  isProcessingDetail = false;
  isProcessing = false;
  noResult = true;
  trainingPlan = {
    "trainningID": "", "title": "", "reason": "", "actionPlan": "", "objective": "", "trainningTime": "", "status": "",
    "result": "", "managerNote": "", "completedOn": "", "acknowledgementSignature": { "text": "", "type": "" },
    "assignedTo": { "userID": "", "name": "", "imageUrl": "", "position": "" },
    "assignedBy": { "userID": "", "name": "", "imageUrl": "", "position": "" }
  };
  signature = { "text": "", "type": "" };

  constructor(public employeeService: EmployeeTrainingDayService, public coreService: CoreService, public router: Router,
    public route: ActivatedRoute, public dialog: MdDialog) { }

  ngOnInit() {
    APPCONFIG.heading = "Training plan detail";
    if (this.route.snapshot.params['id']) {
      this.trainingPlan.trainningID = this.route.snapshot.params['id'];
      this.getTrainingDetail();
    }
    this.getSignature();
  }

  getTrainingDetail() {
    this.isProcessingDetail = true;
    this.employeeService.getTrainingResult(this.trainingPlan.trainningID).subscribe(
      res => {
        this.isProcessingDetail = false;
        if (res.code == 200) {
          this.noResult = false;
          this.trainingPlan = res.data;
        }
        else return this.coreService.notify('Unsuccessful', res.message, 0);
      },
      error => {
        this.isProcessingDetail = false;
        //   if (error.code == 400) {
        //     return this.coreService.notify('Unsuccessful', error.message, 0);
        //   }
        //   this.coreService.notify('Unsuccessful', "Error while getting detail", 0);
      }
    )
  }

  formatDateTime(date) {
    if (moment(date).isValid()) {
      return moment(date).format('DD MMM YYYY hh:mm A');
    }
    return 'Not specified';
  }

  getSignature() {
    this.isProcessingDetail = true;
    this.employeeService.getSignature().subscribe(
      res => {
        this.isProcessingDetail = false;
        if (res.code == 200) {
          this.signature = res.data.signature;
        }
      },
      error => {
        this.isProcessingDetail = false;
      }
    )
  }

  acknowledge() {
    let dialogRef = this.dialog.open(EnterNoteDialog);
    dialogRef.afterClosed().subscribe((note) => {
      if (note) {

        let reqBody = {
          "trainningID": this.trainingPlan.trainningID, "note": note, "acknowledgementSignature": this.signature
        };
        this.isProcessing = true;
        this.employeeService.acknowledgeTraining(reqBody).subscribe(
          res => {
            this.isProcessing = false;
            if (res.code == 200) {
              this.coreService.notify('Successful', res.message, 1);
              this.router.navigate(['/app/employee']);
            }
            else return this.coreService.notify('Unsuccessful', res.message, 0);
          },
          error => {
            this.isProcessing = false;
            if (error.code == 400) {
              return this.coreService.notify('Unsuccessful', error.message, 0);
            }
            this.coreService.notify('Unsuccessful', "Error while submitting detail", 0);
          }
        )
      }
    });
  }

}


@Component({
  selector: 'enter-note',
  template: `
      <div style="width:500px">
        <h5 class="heading" style="margin-top:0px; margin-bottom:15px;">Enter note</h5>
        <div class="form-group">
          <textarea class="form-control full-width" type="text" rows="5" [(ngModel)]="note"></textarea>
        </div>
        <button (click)="submitNote()" type="submit" class="btn btn-primary">Submit</button>
      </div>
          `,
})
export class EnterNoteDialog {
  note = '';

  constructor(public dialog: MdDialogRef<EnterNoteDialog>) { }

  submitNote() {
    if (!this.note.trim().length) return;
    this.dialog.close(this.note);
  }
}