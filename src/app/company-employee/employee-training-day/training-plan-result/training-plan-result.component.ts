import { Component, OnInit } from '@angular/core';
import { APPCONFIG } from '../../../config';
import { Router, ActivatedRoute } from '@angular/router';
import { EmployeeTrainingDayService } from '../employee-training-day.service';
import { CoreService } from '../../../shared/services/core.service';
import * as moment from "moment";

@Component({
  selector: 'app-training-plan-result',
  templateUrl: './training-plan-result.component.html',
  styleUrls: ['./training-plan-result.component.scss']
})
export class TrainingPlanResultComponent implements OnInit {
  isProcessingDetail = false;
  isProcessing = false;
  noResult = true;
  trainingPlan = {
    "trainningID": "", "title": "", "reason": "", "actionPlan": "", "objective": "", "trainningTime": "", "status": "",
    "result": "", "managerNote": "", "completedOn": "", "acknowledgementSignature": "", "assignedTo": [],
    "assignedBy": { "userID": "", "name": "", "imageUrl": "", "position": "" }
  };

  constructor(public employeeService: EmployeeTrainingDayService, public coreService: CoreService, public router: Router,
    public route: ActivatedRoute) { }

  ngOnInit() {
    APPCONFIG.heading = "Training plan detail";
    if (this.route.snapshot.params['id']) {
      this.trainingPlan.trainningID = this.route.snapshot.params['id'];
      this.getTrainingDetail();
    }
  }

  getTrainingDetail() {
    this.isProcessingDetail = true;
    this.employeeService.getTrainingResult(this.trainingPlan.trainningID).subscribe(
      res => {
        this.isProcessingDetail = false;
        if (res.code == 200) {
          this.noResult = false;
          this.trainingPlan = res.data;
        }
        else return this.coreService.notify('Unsuccessful', res.message, 0);
      },
      error => {
        this.isProcessingDetail = false;
        // if (error.code == 400) {
        //   return this.coreService.notify('Unsuccessful', error.message, 0);
        // }
        // this.coreService.notify('Unsuccessful', "Error while getting detail", 0);
      }
    )
  }

  formatDateTime(date) {
    if (moment(date).isValid()) {
      return moment(date).format('DD MMM YYYY hh:mm A');
    }
    return 'Not specified';
  }

}
