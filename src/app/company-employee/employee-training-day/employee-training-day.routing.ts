import { ModuleWithProviders } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TrainingPlanAcceptanceComponent } from './training-plan-acceptance/training-plan-acceptance.component';
import { TrainingPlanAcknowledgementComponent } from './training-plan-acknowledgement/training-plan-acknowledgement.component';
import { TrainingPlanResultComponent } from './training-plan-result/training-plan-result.component';
import { EmployeeParticipationCertificateComponent } from './employee-participation-certificate/employee-participation-certificate.component';

export const EmployeeTrainingDayPagesRoutes: Routes = [
    { path: 'employee-participation-certificate/:id', component: EmployeeParticipationCertificateComponent },
    { path: 'acknowledgement/:id', component: TrainingPlanAcknowledgementComponent },
    { path: 'result/:id', component: TrainingPlanResultComponent },
    { path: 'acceptance/:id', component: TrainingPlanAcceptanceComponent },
];
export const EmployeeTrainingDayRoutingModule = RouterModule.forChild(EmployeeTrainingDayPagesRoutes);
