import { Injectable } from '@angular/core';
import { RestfulLaravelService } from "../../shared/services/restful-laravel.service";

@Injectable()
export class EmployeeTrainingDayService {

  constructor(private restfulService: RestfulLaravelService) { }

  getTrainingDetail(id) {
    return this.restfulService.get(`employee/trainning-plan-detail/${id}`);
  }

  getTrainingPlanList() {
    return this.restfulService.get('employee/employee-training-plan');
  }

  getTrainingResult(id) {
    return this.restfulService.get(`employee/trainning-plan-result/${id}`);
  }

  acceptOrRejectPlan(reqBody) {
    return this.restfulService.post('employee/accept-reject-training-day', reqBody);
  }

  acknowledgeTraining(reqBody) {
    return this.restfulService.post('employee/save-training-acknowledgement', reqBody);
  }

  getSignature() {
    return this.restfulService.get('employee/employee-signature');
  }
}
