import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { EmployeeTrainingDayRoutingModule } from './employee-training-day.routing';
import { TrainingPlanAcceptanceComponent } from './training-plan-acceptance/training-plan-acceptance.component';
import { TrainingPlanAcknowledgementComponent, EnterNoteDialog } from './training-plan-acknowledgement/training-plan-acknowledgement.component';
import { TrainingPlanResultComponent } from './training-plan-result/training-plan-result.component';
import { EmployeeParticipationCertificateComponent } from './employee-participation-certificate/employee-participation-certificate.component';
import { EmployeeTrainingDayService } from './employee-training-day.service';
import { MaterialModule } from "@angular/material";
import { SharedModule } from "../../shared/shared.module";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    MaterialModule,
    EmployeeTrainingDayRoutingModule,
    SharedModule
  ],
  entryComponents: [EnterNoteDialog],
  declarations: [TrainingPlanAcceptanceComponent, TrainingPlanAcknowledgementComponent, TrainingPlanResultComponent,
    EmployeeParticipationCertificateComponent, EnterNoteDialog],
  providers: [EmployeeTrainingDayService]
})
export class EmployeeTrainingDayModule { }
