import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EmployeePositionDescriptionRoutingModule } from './employee-position-description.routing';
import { PositionDescriptionListComponent } from './position-description-list/position-description-list.component';
import { PositionDescriptionDetailComponent } from './position-description-detail/position-description-detail.component';
import { VersionHistoryComponent } from './version-history/version-history.component';
import { ViewVersionsComponent } from './view-versions/view-versions.component';
import { EmployeePositionService } from './employee-position-description.service';
import { FormsModule } from '@angular/forms';
import { DataTableModule } from 'angular2-datatable';
import { SharedModule } from '../../shared/shared.module';
import { MaterialModule } from '@angular/material';

@NgModule({
  imports: [
    FormsModule,
    DataTableModule,
    SharedModule,
    MaterialModule,
    CommonModule,
    EmployeePositionDescriptionRoutingModule
  ],
  providers:[EmployeePositionService],
  declarations: [PositionDescriptionListComponent, PositionDescriptionDetailComponent, VersionHistoryComponent, ViewVersionsComponent]
})
export class EmployeePositionDescriptionModule { }
