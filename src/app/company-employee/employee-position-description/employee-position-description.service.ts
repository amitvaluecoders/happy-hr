import { Injectable } from '@angular/core';
import { RestfulLaravelService} from '../../shared/services/restful-laravel.service';

@Injectable()

export class EmployeePositionService {

  constructor(public restfulWebService:RestfulLaravelService) { }

// method to get listing of all  development plans.
    
    getAllPDs(reqBody):any{
        return this.restfulWebService.get(`employee/get-employee-company-position/${encodeURIComponent(reqBody)}`)
    }

    getPositionDetails(id):any{
        return this.restfulWebService.get(`employee/get-employee-company-position-detail/${id}`);
    }

    AcceptPosition(id):any{
        return this.restfulWebService.get(`employee/employee-accept-position/${id}`)
    }

}
