import { Component, OnInit } from '@angular/core';
import { APPCONFIG } from '../../../config';
import { EmployeePositionService } from '../employee-position-description.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { CoreService } from '../../../shared/services/core.service';
import { MdSnackBar, MdDialog, MdDialogRef } from '@angular/material';

@Component({
  selector: 'app-position-description-detail',
  templateUrl: './position-description-detail.component.html',
  styleUrls: ['./position-description-detail.component.scss']
})
export class PositionDescriptionDetailComponent implements OnInit {

  public isProcessing=false;
  public isAccept = false;
  employeeCompanyPositionID:any;
  status:'';
  public position:any;
  constructor( public employeePositionService: EmployeePositionService,public dialog: MdDialog,
    public router: Router, public activatedRoute: ActivatedRoute,
    public coreService: CoreService) { }

    ngOnInit() {
        APPCONFIG.heading = "Position description details";
        this.employeeCompanyPositionID = this.activatedRoute.snapshot.params['employeeCompanyPositionID'];
        this.status = this.activatedRoute.snapshot.params['status'];
        this.getDetails();
    }

    getDetails(){
      this.isProcessing=true;
      this.employeePositionService.getPositionDetails(this.activatedRoute.snapshot.params['id']).subscribe(
        d => {
          if (d.code == "200") {
            this.position=d.data;
            this.isProcessing=false;
          }
          else{
            this.coreService.notify("Unsuccessful", d.message, 0);
            this.isProcessing=false;
          } 
        },
        error => this.coreService.notify("Unsuccessful", error.message, 0), 
    )
  }
  onAcceptPosition(){
    this.isAccept=true;
        this.employeePositionService.AcceptPosition(this.employeeCompanyPositionID).subscribe(
          d => {
            if (d.code == "200") {
              this.coreService.notify("Successful", d.message, 1);  
              this.isAccept=false;
              this.router.navigate([`/app/employee/position-description`]);
              //this.getAllContracts();
            }
            else{
              this.coreService.notify("Unsuccessful", d.message, 0);
              this.isAccept=false;
            } 
          },
          error =>{
            this.isAccept=false;
            this.coreService.notify("Unsuccessful", error.message, 0);
          }  
      )

    }




}
