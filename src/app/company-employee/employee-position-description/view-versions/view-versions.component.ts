import { Component, OnInit } from '@angular/core';
import { MdSnackBar, MdDialog, MdDialogRef } from '@angular/material';
import { APPCONFIG } from '../../../config';

@Component({
  selector: 'app-view-versions',
  templateUrl: './view-versions.component.html',
  styleUrls: ['./view-versions.component.scss']
})
export class ViewVersionsComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    APPCONFIG.heading="Accept/reject for position description";
  }

}
