import { Component, OnInit } from '@angular/core';
import { APPCONFIG } from '../../../config';

@Component({
  selector: 'app-version-history',
  templateUrl: './version-history.component.html',
  styleUrls: ['./version-history.component.scss']
})
export class VersionHistoryComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    APPCONFIG.heading="Version history";
  }

}
