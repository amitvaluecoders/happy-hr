import {  ModuleWithProviders  } from '@angular/core';
import {  RouterModule,Routes  } from '@angular/router';

import { PositionDescriptionListComponent } from './position-description-list/position-description-list.component';
import { PositionDescriptionDetailComponent } from './position-description-detail/position-description-detail.component';
import { VersionHistoryComponent } from './version-history/version-history.component';
import { ViewVersionsComponent } from './view-versions/view-versions.component';

export const EmployeePositionDescriptionPagesRoutes: Routes = [
    {
        path: '',
        canActivate:[],
        children: [
           
            { path: '', component: PositionDescriptionListComponent},
            { path: 'detail/:id', component: PositionDescriptionDetailComponent},
            { path: 'detail/:id/:employeeCompanyPositionID/:status', component:PositionDescriptionDetailComponent},
            { path: 'version-history', component: VersionHistoryComponent},
            { path: 'version/view', component: ViewVersionsComponent},
       ]
    }
];

export const EmployeePositionDescriptionRoutingModule = RouterModule.forChild(EmployeePositionDescriptionPagesRoutes);
