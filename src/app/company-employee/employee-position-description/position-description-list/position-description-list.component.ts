import { Component, OnInit } from '@angular/core';
import { APPCONFIG } from '../../../config';
import { EmployeePositionService } from '../employee-position-description.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { CoreService } from '../../../shared/services/core.service';
import { MdSnackBar, MdDialog, MdDialogRef } from '@angular/material';

@Component({
  selector: 'app-position-description-list',
  templateUrl: './position-description-list.component.html',
  styleUrls: ['./position-description-list.component.scss']
})
export class PositionDescriptionListComponent implements OnInit {

  public toggleFilter=false;
  public unsubscriber:any;
  public collapse={};
  public versionSearch='';
  public isApplyBtnClicked=false;
  public hrToggle=false;
  public contracts=[];
  public isAccept= false;
  public positions=[];
  public isProcessing=false;
    tempReqBody={
    "search":"",
    "filter":{
      "versions":[],
      "status":[
        {
          title:"Current",
          value:"Accept",
          isChecked:false
        },
        {
          title:"Old",
          value:"Old",
          isChecked:false        
        },
        {
          title:"Pending",
          value:"Accept",
          isChecked:false        
        }
      ]
    },
    
  }
    reqBody={
    "search":"",
    "filter":{
      "versions":{ },
      "status":{}
    }
  }

  
  ngOnInit() {
    this.getAllContracts();    
    APPCONFIG.heading="Position description";
  }

  constructor( public employeePositionService: EmployeePositionService,public dialog: MdDialog,
               public router: Router, public activatedRoute: ActivatedRoute,
               public coreService: CoreService) { }



 onSearch(){
    this.unsubscriber.unsubscribe();
    this.reqBody.search=encodeURIComponent(this.tempReqBody.search);
    this.getAllContracts();
  }
    onApplyFilters(){
      this.reqBody.filter.versions={};
    for(let i of this.tempReqBody.filter.versions){
      if(i.isChecked) this.reqBody.filter.versions[i.title]=true;
    }
    this.reqBody.filter.status={};
    for(let i of this.tempReqBody.filter.status){
      if(i.isChecked) this.reqBody.filter.status[i.title]=true;
    }
    this.getAllContracts();
  }


  getAllContracts(){
        this.isProcessing=true;
        this.unsubscriber=this.employeePositionService.getAllPDs(JSON.stringify(this.reqBody)).subscribe(
          d => {
            if (d.code == "200") {
              this.positions=d.data;
              //  if(!this.tempReqBody.filter.versions.length) {
              //    d.data.filter(item=>{
              //       this.tempReqBody.filter.versions.push({isChecked:false,title:item.version});
              //     })
              //  }
              this.isProcessing=false;
            }
            else{
              this.coreService.notify("Unsuccessful", d.message, 0);
              this.isProcessing=false;
            } 
          },
          error => this.coreService.notify("Unsuccessful", error.message, 0), 
      )
  }

  onDetails(c){
  this.router.navigate([`/app/employee/position-description/detail/${c.companyPositionID}`]);  
  }

  acceptOrReject(c){
    this.router.navigate([`/app/employee/position-description/detail/${c.companyPositionID}/${c.employeeCompanyPositionID}/${c.status}`]);
  }      



}
