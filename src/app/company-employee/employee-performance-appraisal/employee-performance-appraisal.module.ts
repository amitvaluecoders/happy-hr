import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { MaterialModule } from '@angular/material';
import { EmployeeAppraisalListingComponent } from './employee-appraisal-listing/employee-appraisal-listing.component';
import { EmployeeDirectAssessmentResultComponent } from './employee-direct-assessment-result/employee-direct-assessment-result.component';
import { appraisalRouterModule } from './employee-performance-appraisal.routing';
import { EmployeeSelfAssessmentComponent } from './employee-self-assessment/employee-self-assessment.component';
import { EmployeePerformanceAppraisalService } from "./employee-performance-appraisal.services";

@NgModule({
  imports: [
    CommonModule,
    appraisalRouterModule,
    FormsModule,
    MaterialModule
  ],
  declarations: [EmployeeAppraisalListingComponent, EmployeeDirectAssessmentResultComponent, EmployeeSelfAssessmentComponent],
  providers: [EmployeePerformanceAppraisalService]
})
export class EmployeePerformanceAppraisalModule { }
