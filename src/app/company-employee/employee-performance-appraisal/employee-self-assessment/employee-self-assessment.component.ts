import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from "@angular/router";
import { APPCONFIG } from '../../../config';
import { EmployeePerformanceAppraisalService } from "../employee-performance-appraisal.services";
import { CoreService } from '../../../shared/services/core.service';
import * as moment from "moment";

@Component({
  selector: 'app-employee-self-assessment',
  templateUrl: './employee-self-assessment.component.html',
  styleUrls: ['./employee-self-assessment.component.scss']
})
export class EmployeeSelfAssessmentComponent implements OnInit {
  public collapse = {};
  public isProcessing = false;
  public isSubmitting = false;
  public submitted = false;
  public noResult = true;
  public performaceAppraisalID = "";
  public pa = {
    "performaceAppraisalID": "", "title": "", "assessmentDate": "",
    "managerID": { "userID": "", "name": "", "imageUrl": "", "position": "" },
    "userID": { "userID": "", "name": "", "imageUrl": "", "position": "" },
    "performanceIndicator": [], "question": [], "thirdParty": []
  }

  constructor(public employeeService: EmployeePerformanceAppraisalService, public coreService: CoreService,
    private router: Router, private route: ActivatedRoute, ) { }

  ngOnInit() {
    APPCONFIG.heading="Performance appraisal";
    if (this.route.snapshot.params['id']) {
      this.performaceAppraisalID = this.route.snapshot.params['id'];
      this.getPerformanceAppraisal();
    }
  }

  isValidData(data) {
    return !(data instanceof Array);
  }

  formatDate(date) {
    return (date && moment(date).isValid()) ? moment(date).format('DD MMM YYYY') : 'Not specified';
  }

  getPerformanceAppraisal() {
    this.isProcessing = true;
    this.employeeService.getPerformanceAppraisalDetail(this.performaceAppraisalID).subscribe(
      res => {
        this.isProcessing = false;
        if (res.code == 200) {
          this.pa = res.data;
          this.noResult = false;
          this.pa.performanceIndicator.filter(item => { item['isEditing'] = !item.response })
          this.pa.question.filter(item => { item['isEditing'] = !item.response })
        }
        else return this.coreService.notify("Unsuccessful", res.message, 0);
      },
      err => {
        this.isProcessing = false;
        this.coreService.notify("Unsuccessful", err.message, 0);
      }
    )
  }

  isValid() {
    let pi = true; let qsn = true;

    for (let i = 0; i < this.pa.performanceIndicator.length; i++) {
      if (!this.pa.performanceIndicator[i].response) {
        pi = false; break;
      }
    }
    for (let i = 0; i < this.pa.question.length; i++) {
      if (!this.pa.question[i].response) {
        qsn = false; break;
      }
    }

    return pi && qsn;
  }

  saveResponse() {
    this.submitted = true;
    if (!this.isValid()) return;

    this.isSubmitting = true;
    this.employeeService.saveResponse(this.pa).subscribe(
      res => {
        this.isSubmitting = false;
        if (res.code == 200) {
          this.coreService.notify("Successful", res.message, 1);
          this.router.navigate(['/app/employee/dashboard']);
        }
        else return this.coreService.notify("Unsuccessful", res.message, 0);
      },
      err => {
        this.isSubmitting = false;
        this.coreService.notify("Unsuccessful", err.message, 0);
      }
    )
  }
}
