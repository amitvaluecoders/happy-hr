import { Injectable } from '@angular/core';
import { RestfulLaravelService } from "../../shared/services/restful-laravel.service";

@Injectable()
export class EmployeePerformanceAppraisalService {

    constructor(private restfulService: RestfulLaravelService) { }

    getPerformanceAppraisalDetail(params) {
        
        return this.restfulService.get(`employee/employee-performance-appraisal-detail/${params}`);
    }

    getPerformanceAppraisalList(params) { //{"search":"","filter":{"status":{"Awaiting Employee Acceptance":false,"Complete":false}}
        return this.restfulService.get(`employee/employee-performance-appraisal-list/${params}`);
    }

    saveResponse(reqBody) {
        return this.restfulService.post('employee/employee-performance-appraisal-response', reqBody);
    }
}