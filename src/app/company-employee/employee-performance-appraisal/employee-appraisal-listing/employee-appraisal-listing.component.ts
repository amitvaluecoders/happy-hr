import { Component, OnInit } from '@angular/core';
import { APPCONFIG } from '../../../config';
@Component({
  selector: 'app-employee-appraisal-listing',
  templateUrl: './employee-appraisal-listing.component.html',
  styleUrls: ['./employee-appraisal-listing.component.scss']
})
export class EmployeeAppraisalListingComponent implements OnInit {
public toggleFilter:boolean;

  constructor() { }

  ngOnInit() {
    this.toggleFilter=true;
    APPCONFIG.heading="Performance indicators listing";
  }

}
