import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from "@angular/router";
import { EmployeePerformanceAppraisalService } from "../employee-performance-appraisal.services";
import { CoreService } from '../../../shared/services/core.service';
import { APPCONFIG } from '../../../config';
import * as moment from "moment";

@Component({
  selector: 'app-employee-direct-assessment-result',
  templateUrl: './employee-direct-assessment-result.component.html',
  styleUrls: ['./employee-direct-assessment-result.component.scss']
})
export class EmployeeDirectAssessmentResultComponent implements OnInit {
  public collapse = [];
  public isProcessing = false;
  public noResult = true;
  public performaceAppraisalID = "";
  public pa = {
    "performaceAppraisalID": "", "title": "", "assessmentDate": "",
    "managerID": { "userID": "", "name": "", "imageUrl": "", "position": "" },
    "userID": { "userID": "", "name": "", "imageUrl": "", "position": "" },
    "performanceIndicator": [], "question": [], "thirdParty": []
  }
  directAssessment = false;
  loggedInUser:any
  constructor(public employeeService: EmployeePerformanceAppraisalService, public coreService: CoreService,
    private router: Router, private route: ActivatedRoute, ) { }

  ngOnInit() {
    APPCONFIG.heading = "Performance appraisal";
    let type = this.route.snapshot.url[0].path;
    this.loggedInUser = JSON.parse(localStorage.getItem('happyhr-userInfo'));
    this.directAssessment = (type == 'direct-assessment-result');
    if (this.route.snapshot.params['id']) {
      this.performaceAppraisalID = this.route.snapshot.params['id'];
      this.getPerformanceAppraisal();
    }
  }

  isValidData(data) {
    return !(data instanceof Array);
  }

  formatDate(date) {
    return (date && moment(date).isValid()) ? moment(date).format('DD MMM YYYY') : 'Not specified';
  }

  getPerformanceAppraisal() {
    this.isProcessing = true;
    let body={
        userID:this.loggedInUser.userID,
        performanceAppraisalID:JSON.parse(this.performaceAppraisalID)
    }
    // console.log("before",body);
    // console.log("after",JSON.stringify(body));
    
    this.employeeService.getPerformanceAppraisalDetail(JSON.stringify(body)).subscribe(
      res => {
        this.isProcessing = false;
        if (res.code == 200) {
          this.pa = res.data;
          this.noResult = false;
        }
        else return this.coreService.notify("Unsuccessful", res.message, 0);
      },
      err => {
        this.isProcessing = false;
        this.coreService.notify("Unsuccessful", err.message, 0);
      }
    )
  }
}
