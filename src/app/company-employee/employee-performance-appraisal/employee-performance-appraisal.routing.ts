import { RouterModule, Routes } from '@angular/router';
import { EmployeeAppraisalListingComponent } from './employee-appraisal-listing/employee-appraisal-listing.component';
import { EmployeeDirectAssessmentResultComponent } from './employee-direct-assessment-result/employee-direct-assessment-result.component';
import { EmployeeSelfAssessmentComponent } from "./employee-self-assessment/employee-self-assessment.component";

export const appriasalRoutes: Routes = [
    // { path: '', component: EmployeeAppraisalListingComponent },
    { path: 'self-assessment/:id', component: EmployeeSelfAssessmentComponent },
    { path: 'result/:id', component: EmployeeDirectAssessmentResultComponent },
    { path: 'direct-assessment-result/:id', component: EmployeeDirectAssessmentResultComponent }
];

export const appraisalRouterModule = RouterModule.forChild(appriasalRoutes);