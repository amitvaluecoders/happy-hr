import { Component, OnInit } from '@angular/core';
import { APPCONFIG } from '../../../config';
import { IDatePickerConfig } from 'ng2-date-picker';

@Component({
  selector: 'app-employee-formal-disciplinary-meeting',
  templateUrl: './employee-formal-disciplinary-meeting.component.html',
  styleUrls: ['./employee-formal-disciplinary-meeting.component.scss']
})
export class EmployeeFormalDisciplinaryMeetingComponent implements OnInit {
  
  public progress;
  birthday: any;
  config: IDatePickerConfig = {};
  constructor() { }

 ngOnInit() {
     APPCONFIG.heading="Formal disciplinary meeting";
     this.progressBarSteps();
  }
  progressBarSteps() {
      this.progress={
          progress_percent_value:75, //progress percent
          total_steps:4,
          current_step:3,
          circle_container_width:20, //circle container width in percent
          steps:[
               {num:1,data:"Disciplinary actiion"},
               {num:2,data:"Employee: Invite third party"},
               {num:3,data:"Formal disciplinary meeting",active:true},
               {num:4,data:"Result"},
          ]  
       }         
   }

}
