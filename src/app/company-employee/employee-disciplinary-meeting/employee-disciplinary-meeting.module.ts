import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EmployeeDisciplinaryMeetingRoutingModule } from './employee-disciplinary-meeting.routing';
import { InviteThirdPartyEmployeeComponent } from './invite-third-party-employee/invite-third-party-employee.component';
import { EmployeeFormalDisciplinaryMeetingComponent } from './employee-formal-disciplinary-meeting/employee-formal-disciplinary-meeting.component';
import { ResultComponent } from './result/result.component';
import { SharedModule } from '../../shared/shared.module';
import { FormsModule } from '@angular/forms';
import { MaterialModule} from '@angular/material';

@NgModule({
  imports: [
    EmployeeDisciplinaryMeetingRoutingModule,
    CommonModule,
    SharedModule,
    FormsModule,
    MaterialModule
  ],
  declarations: [InviteThirdPartyEmployeeComponent, EmployeeFormalDisciplinaryMeetingComponent, ResultComponent]
})
export class EmployeeDisciplinaryMeetingModule { }
