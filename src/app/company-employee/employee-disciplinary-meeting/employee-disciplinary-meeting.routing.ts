import {  ModuleWithProviders  } from '@angular/core';
import {  RouterModule,Routes  } from '@angular/router';

import { InviteThirdPartyEmployeeComponent } from './invite-third-party-employee/invite-third-party-employee.component';
import { EmployeeFormalDisciplinaryMeetingComponent } from './employee-formal-disciplinary-meeting/employee-formal-disciplinary-meeting.component';
import { ResultComponent } from './result/result.component';

export const EmployeeDisciplinaryMeetingPagesRoutes: Routes = [
    {
        path: '',
        canActivate:[],
        children: [
           
         
           { path: 'invite/:damID', component: InviteThirdPartyEmployeeComponent},
        //    { path: 'formal', component: EmployeeFormalDisciplinaryMeetingComponent}, 
           { path: 'view/:damID', component: ResultComponent}, 
         ]
    }
];

export const EmployeeDisciplinaryMeetingRoutingModule = RouterModule.forChild(EmployeeDisciplinaryMeetingPagesRoutes);
