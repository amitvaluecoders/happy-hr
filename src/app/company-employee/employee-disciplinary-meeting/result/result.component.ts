import { Component, OnInit } from '@angular/core';
import { APPCONFIG } from '../../../config';
import { IDatePickerConfig } from 'ng2-date-picker';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { RestfulLaravelService } from '../../../shared/services/restful-laravel.service';
import { Cookie } from 'ng2-cookies/ng2-cookies';
import { CoreService } from '../../../shared/services/core.service';
import * as moment from 'moment';
import { ConfirmService } from '../../../shared/services/confirm.service';
import { MdSnackBar, MdDialog, MdDialogRef } from '@angular/material';



@Component({
  selector: 'app-result',
  templateUrl: './result.component.html',
  styleUrls: ['./result.component.scss']
})
export class ResultComponent implements OnInit {

    public damID:any;
    public heading='';
    public isResultProcessing=false;
    public progress={
          progress_percent_value:66, //progress percent
          total_steps:3,
          current_step:2,
          circle_container_width:20, //circle container width in percent
          steps:[
               {num:1,data:"Create disciplinary meeting",active:true},
               {num:2,data:"Formal disciplinary meeting",active:true},
               {num:3,data:"Result",active:false},
          ]  
       };
    birthday: any;
    config: IDatePickerConfig = {};
    disciplinaryMeeting={
      disciplinaryActionID:'',
      employee:{},
      manager:{},
      thirdParty:[],
      statements:{
        Employee:[],
        ThirdParty:[],
        Manager:[]
      }
    };
    isProcessing=false;

    constructor(public  restfulService: RestfulLaravelService, public confirmService: ConfirmService,
    public router: Router, public activatedRoute: ActivatedRoute,public dialog: MdDialog,
    public coreService: CoreService){
    this.config.locale = "en";
    this.config.format = "YYYY-MM-DD hh:mm A";
    this.config.showMultipleYearsNavigation = true;
    this.config.weekDayFormat = 'dd';
    this.config.disableKeypress = true;
    this.config.monthFormat="MMM YYYY";
    }

  ngOnInit() {
    this.disciplinaryMeeting['Manager']=[];
    this.disciplinaryMeeting['Employee']=[];
    this.disciplinaryMeeting['ThirdParty']=[];
     APPCONFIG.heading="";
    //  this.progressBarSteps(); 
     this.damID=this.activatedRoute.snapshot.params['damID'];
     this.getDisciplinaryDetails();
  }
//  progressBarSteps(percent,currentStep,) {
//       this.progress={
//           progress_percent_value:100, //progress percent
//           total_steps:3,
//           current_step:3,
//           circle_container_width:20, //circle container width in percent
//           steps:[
//                {num:1,data:"Create disciplinary meeting",active:false},
//                {num:2,data:"Formal disciplinary meeting",active:false},
//                {num:3,data:"Result",active:false},
//           ]  
//        }         
//    }
   onEdit(){
      this.router.navigate([`/app/company-admin/disciplinary-meeting/update/${this.damID}`]);
   }

// method to get details of disciplinary ations. 
  getDisciplinaryDetails(){ 
    this.isProcessing=true;       
      this.restfulService.get(`employee/disciplinary-action-detail/${this.damID}`).subscribe(
          d => {
        if (d.code == "200") {
            this.setProgressBar(d.data); 
            this.disciplinaryMeeting=d.data;       
            // if(this.disciplinaryMeeting.statements[''])this.disciplinaryMeeting.statements['ThirdParty']=this.disciplinaryMeeting.statements[''];
            this.isProcessing=false; 
        }
        else this.coreService.notify("Unsuccessful", d.message, 0);
    },
    error => this.coreService.notify("Unsuccessful", error.message, 0),
    () => { }                
    )     
  }

  setProgressBar(data){
   
    APPCONFIG.heading='Formal disciplinary meeting';
      if(data.statements.Employee || data.statements.Manager || data.statements.ThirdParty){
        this.progress.progress_percent_value=66;        
        this.progress.current_step=2;
        this.progress.steps[1].active=true;
        APPCONFIG.heading="Formal disciplinary meeting";
      }
       if(data.result){
        this.progress.progress_percent_value=100;
        this.progress.current_step=3;
        this.progress.steps[1].active=true;
        this.progress.steps[2].active=true;
        APPCONFIG.heading="Result";     
      }
      this.heading=APPCONFIG.heading;     
  }  





}
