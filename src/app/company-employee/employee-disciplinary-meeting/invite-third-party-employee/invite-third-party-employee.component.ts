import { Component, OnInit } from '@angular/core';
import { APPCONFIG } from '../../../config';
import { IDatePickerConfig } from 'ng2-date-picker';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { RestfulLaravelService } from '../../../shared/services/restful-laravel.service';
import { Cookie } from 'ng2-cookies/ng2-cookies';
import { CoreService } from '../../../shared/services/core.service';
import * as moment from 'moment';
import { ConfirmService } from '../../../shared/services/confirm.service';
import { MdSnackBar, MdDialog, MdDialogRef } from '@angular/material';


@Component({
  selector: 'app-invite-third-party-employee',
  templateUrl: './invite-third-party-employee.component.html',
  styleUrls: ['./invite-third-party-employee.component.scss']
})
export class InviteThirdPartyEmployeeComponent implements OnInit {

  public progress;
  birthday: any;
  config: IDatePickerConfig = {};

  public isCreateProcessing=false;
  public isThirdPartyAlreadyExist = false;
  public isEnterPressed = false;
  public thirdPartyName = ''; 
  public thirdPartyList = [];
  public dropdownListEmployees = [];
  public isProcessing=false;
  public damID:any;
  public disciplinaryMeeting:any;
  public isNewSelectedThirdPartyBlank=false;

 
    constructor(public  restfulService: RestfulLaravelService, public confirmService: ConfirmService,
    public router: Router, public activatedRoute: ActivatedRoute,public dialog: MdDialog,
    public coreService: CoreService)
     { 
     this.config.locale = "en";
    this.config.format = "YYYY-MM-DD hh:mm A";
    this.config.showMultipleYearsNavigation = true;
    this.config.weekDayFormat = 'dd';
    this.config.disableKeypress = true;
    this.config.monthFormat="MMM YYYY";
  }

  ngOnInit() {
     APPCONFIG.heading="Invite third party";
     this.damID=this.activatedRoute.snapshot.params['damID'];
     this.progressBarSteps();
     this.getEmployeeeList();
  }
 progressBarSteps() {
      this.progress={
          progress_percent_value:50, //progress percent
          total_steps:4,
          current_step:2,
          circle_container_width:20, //circle container width in percent
          steps:[
               {num:1,data:"Disciplinary action"},
               {num:2,data:"Employee: Invite third party",active:true},
               {num:3,data:"Formal disciplinary meeting"},
               {num:4,data:"Result"},
          ]  
       }         
   }

     getEmployeeeList() {
     this.isProcessing = true;
     this.restfulService.get('show-all-employee').subscribe(
          d => {
              if (d.code == "200") {
                    this.isProcessing = false; 
                  if(this.damID) this.getDisciplinaryDetails();
                  this.dropdownListEmployees=d.data.filter(item =>{
                      item['id'] = item.userId;
                      item['itemName'] = item.name;
                      item['isSelected'] = false;
                      return item;
                  })

                  this.thirdPartyList=JSON.parse(JSON.stringify(this.dropdownListEmployees));
               
              }
              else this.coreService.notify("Unsuccessful", "Error while getting contract types", 0);
          },
          error => this.coreService.notify("Unsuccessful", "Error while getting contract types", 0),
          () => { }
      )
  }


 getDisciplinaryDetails(){ 
    this.isProcessing=true;       
      this.restfulService.get(`employee/disciplinary-action-detail/${this.damID}`).subscribe(
          d => {
        if (d.code == "200") {
            this.disciplinaryMeeting=d.data;       
            this.isProcessing=false; 
        }
        else this.coreService.notify("Unsuccessful", d.message, 0);
    },
    error => this.coreService.notify("Unsuccessful", error.message, 0),
    () => { }                
    )     
  }

     onEnterExternalThirdParty(isvalid){
      this.isThirdPartyAlreadyExist=false;
      this.isEnterPressed=true;
      if(isvalid){
            for(let c of this.thirdPartyList ){
                if(c.email==this.thirdPartyName)this.isThirdPartyAlreadyExist=true;
            }  
            if(!this.isThirdPartyAlreadyExist){
                this.thirdPartyList.splice(0,0,{
                    isSelected:true,
                    name:this.thirdPartyName,
                    email:this.thirdPartyName,
                    external:true,
                    imageUrl:"http://happyhrapi.n1.iworklab.com/public/user-profile-image/user.png"
                })
                this.thirdPartyName='';
                this.isEnterPressed=false;
            } 
      }
  }

  onInviteThirdParty(){

      let tempThirdParty=[];
      this.thirdPartyList.filter(item=>{
            if(item.isSelected){
            let tempItem={
                type:(item.external)?"External":"Internal",
                userID:(!item.external)?item.userId:'',
                email:(item.external)?item.email:''
                };
                      tempThirdParty.push(tempItem);            
            }
        })
      if(tempThirdParty.length>0)this.saveThirdParty(tempThirdParty);
      else this.isNewSelectedThirdPartyBlank=true;
  }

  saveThirdParty(tempThirdParty){
      this.isCreateProcessing=true;
      let reqBody=JSON.parse(JSON.stringify(this.disciplinaryMeeting))
      reqBody['employeeID']=reqBody['employee'].userID;
      reqBody['managerID']=reqBody['manager'].userID;
      reqBody['thirdParty']=tempThirdParty;
       this.restfulService.post(`disciplinary-action-save`,reqBody).subscribe(
          d => {
        if (d.code == "200") {       
            this.router.navigate([`/app/employee/activites`]);
            this.coreService.notify("Successful", d.message, 1);
        }
        else {
         this.coreService.notify("Unsuccessful", d.message, 0);
         this.isCreateProcessing=false;   
        }
    },
    error =>{ this.coreService.notify("Unsuccessful", error.message, 0),this.isCreateProcessing=false; }                
    )     
  }

}
