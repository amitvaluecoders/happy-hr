import { NgModule, ApplicationRef } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LayoutModule } from './layout/layout.module';
import { CommonModule } from '@angular/common';
import { CoreService } from './shared/services';
import { SimpleNotificationsModule } from 'angular2-notifications';
import { ConfirmService } from './shared/services/confirm.service';
import { SharedModule} from './shared/shared.module';



@NgModule({
    imports: [BrowserAnimationsModule,SharedModule,SimpleNotificationsModule.forRoot(),AppRoutingModule,CommonModule,LayoutModule],
    declarations: [AppComponent],
    bootstrap: [AppComponent],
    providers:[CoreService, ConfirmService]
})

export class AppModule {
    constructor() { }
}
