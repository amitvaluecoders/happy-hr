import { Component } from '@angular/core';
import * as jQuery from 'jquery';
import { Cookie } from 'ng2-cookies/ng2-cookies';
import * as _ from "lodash";
import { DomSanitizer } from '@angular/platform-browser';
import { Router, NavigationEnd } from '@angular/router';
import { APPCONFIG } from './config'
import { LayoutService } from './layout/layout.service';
import { Location } from '@angular/common';
import { CoreService } from './shared/services/core.service';


// 3rd
import 'styles/font-awesome.scss';
import 'styles/material2-theme.scss';
import 'styles/bootstrap.scss';
// custom
import 'styles/layout.scss';
import 'styles/theme.scss';
import 'styles/ui.scss';
import 'styles/app.scss';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    providers: [LayoutService],
})
export class AppComponent {
    AppConfig: any;
    constructor(public coreService:CoreService, private location: Location,private router: Router) { }
    
    public options = {
    position: ["bottom", "right"],
    timeOut: 5000,
    lastOnBottom: true,
    animate:"fromRight"
}

    ngOnInit() {
        let user =JSON.parse(localStorage.getItem('happyhr-userInfo'))
        let permission =JSON.parse(localStorage.getItem('happyhr-permission'));
        let cok =this.coreService.getRole();
        let tok = Cookie.get('happyhr-token')
        if(this.location.path() ==''){
        if(tok && user && permission && cok){ this.redirectUserToDashboard(user);}
        else {
            Cookie.deleteAll();
            localStorage.clear();
            this.router.navigate(["/user/login"]);
        }
        }
        this.AppConfig = APPCONFIG;
        this.router.events.subscribe((evt) => {
            if (!(evt instanceof NavigationEnd)) {
                return;
            }
            document.body.scrollTop = 0;
        });
    }

      redirectUserToDashboard(user){
        let url='';
        if(this.location.path() =='') {
            if(user.userRole=='superAdmin')  url = '/app/super-admin';    
            if(user.userRole=='companyAdmin')  url = '/app/company-admin';
            if(user.userRole=='companyManager')  url = '/app/company-manager';
            if(user.userRole=='companyEmployee'){
              if (user.terminated) url = '/app/employee/after-termination';
              else url = '/app/employee';
            }  
            this.router.navigate([url]);    
        }    
      }
}
