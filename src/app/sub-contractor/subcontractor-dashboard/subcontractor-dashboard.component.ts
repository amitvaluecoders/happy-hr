import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { MdDialog, MdDialogRef } from '@angular/material';
import { CoreService } from '../../shared/services/core.service';
import { APPCONFIG } from '../../config';
import { RestfulLaravelService } from '../../shared/services/restful-laravel.service';
import { icsFormatter } from './../../../assets/vendor_js/icsFormatter';

@Component({
  selector: 'app-subcontractor-dashboard',
  templateUrl: './subcontractor-dashboard.component.html',
  styleUrls: ['./subcontractor-dashboard.component.scss']
})
export class SubcontractorDashboardComponent implements OnInit {

 
  public dashBoard: any;
  isProcessing = false;
  public calendarOptions = {
    height: '10%',
    width: '70%',
    eventColor: '#64c3fb',
    header: {
      left: 'prev,next today',
      center: 'title',
      right: 'month,agendaWeek,agendaDay'
    },
    fixedWeekCount: false,
    defaultDate: new Date(),
    eventLimit: true,
    editable: true,
    events: [],
    eventRender: function (event, element, view) { var title = element.find('.fc-title, .fc-list-item-title'); title.html(title.text()); },
    eventClick: function (calEvent, jsEvent, view) {
      var calEntry = icsFormatter();

      var title = calEvent.description.substr(0, calEvent.description.search('\n'));
      var place = '';
      var begin = new Date(calEvent.start.utc()).toString();
      var end = new Date(calEvent.end.utc()).toString();

      var description = calEvent.description.substr(calEvent.description.search('\n') + 2, calEvent.description.length);

      calEntry.addEvent(title, description, place, begin, end);
      calEntry.download('vcart', '.ics');
    }

  };

  constructor(public router: Router, public activatedRoute: ActivatedRoute,
    public service: RestfulLaravelService,
    public coreService: CoreService, public dialog: MdDialog) { }

  ngOnInit() {
    APPCONFIG.heading = 'Dashboard';
    this.getDashBoardDeatils();
  }

  onActivitiesAndApprovals() {
    this.router.navigate([`/app/employee/activites`]);
  }

  getDashBoardDeatils() {
    this.isProcessing = true;
    this.service.get('contractor/dashboard').subscribe(
      res => {
        if (res.code == 200) {
          this.dashBoard = res.data;
          this.isProcessing = false;
        }
        else return this.coreService.notify("Unsuccessful", res.message, 0);
      },
      error => {
        this.isProcessing = false;
        this.coreService.notify("Unsuccessful", error.message, 0);
      }
    )
  }

}
