import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from "@angular/material";
import { EmployeeOrganizationChartComponent, OrgnisationChartDialog } from './employee-organization-chart/employee-organization-chart.component';
import { EmployeeOrganizationChartService } from "./employee-organization-chart.service";
import { EmployeeOrgChartRoutingModules } from "./employee-organization-chart.routing";
import { SharedModule } from "../../shared/shared.module";
import { EmployeeOrganizationChartPopupComponent } from './employee-organization-chart-popup/employee-organization-chart-popup.component';
import { QuillModule } from 'ngx-quill';
import { FormsModule } from '@angular/forms';
import { AngularMultiSelectModule } from 'angular2-multiselect-checkbox-dropdown/angular2-multiselect-dropdown';

@NgModule({
  imports: [
    CommonModule,
    QuillModule,
    EmployeeOrgChartRoutingModules,
    MaterialModule,
    FormsModule,
    SharedModule,
    AngularMultiSelectModule
  ],
  declarations: [EmployeeOrganizationChartComponent, OrgnisationChartDialog, EmployeeOrganizationChartPopupComponent],
  entryComponents: [OrgnisationChartDialog,EmployeeOrganizationChartPopupComponent],
  providers: [EmployeeOrganizationChartService]
})
export class EmployeeOrganizationChartModule { }
