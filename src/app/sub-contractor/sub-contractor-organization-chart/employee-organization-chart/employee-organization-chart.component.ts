import { Component, OnInit, ElementRef, ViewChild, Renderer2 } from '@angular/core';
import { Router,ActivatedRoute } from "@angular/router";
import { APPCONFIG } from '../../../config';
import { CoreService } from "../../../shared/services/core.service";
import { EmployeeOrganizationChartService } from "../employee-organization-chart.service";
import {EmployeeOrganizationChartPopupComponent} from "../employee-organization-chart-popup/employee-organization-chart-popup.component"
import { MdSnackBar, MdDialog, MdDialogRef } from '@angular/material';

@Component({
  selector: 'app-employee-organization-chart',
  templateUrl: './employee-organization-chart.component.html',
  styleUrls: ['./employee-organization-chart.component.scss']
})
export class EmployeeOrganizationChartComponent implements OnInit {

  public actionEvntId = {}
  public isProcessing = false;
  role:any;
  public contentWith;
  candidateID:any;
  // count;
  noResult = true;
  public chart = { "companyID": "", "nodeID": "", "title": "", "type": "", "subNodes": [], logo: '' }

  @ViewChild('root') root: ElementRef;
  @ViewChild('node') node: ElementRef;
  constructor(public renderer: Renderer2, public coreService: CoreService, public dialog: MdDialog,
    public employeeService: EmployeeOrganizationChartService, public router: Router,public route: ActivatedRoute) { }

  ngOnInit() {
    APPCONFIG.heading = "Organisation chart";
    if (this.route.snapshot.params['id']) {
      this.candidateID = this.route.snapshot.params['id'];
    }
    this.role = this.coreService.getRole();
    this.getOrganizationChart();
  }

  getOrganizationChart() {
    this.root.nativeElement.querySelector('.tree').innerHTML = '';
    // this.count = 1;
    this.isProcessing = true;
    this.employeeService.getOrganizationChart().subscribe(
      res => {
        this.isProcessing = false;
        if (res.code == 200) {
          this.noResult = false;
          this.chart = res.data;
          // this.chart['count'] = this.count;

          if (this.chart.subNodes && this.chart.subNodes.length) {
            for (let i = 0; i < this.chart.subNodes.length; i++) {
              this.chart.subNodes[i]['parentNodeID'] = this.chart.nodeID;
              this.addNode(this.chart.subNodes[i], this.root.nativeElement)
            }
            console.log('chart ===>> ', this.chart);
          }
        }
        else return this.coreService.notify("Unsuccessful", res.message, 0);
      },
      err => {
        this.isProcessing = false;
        if (err.code == 400) {
          return this.coreService.notify("Unsuccessful", err.message, 0);
        }
        this.coreService.notify("Unsuccessful", "Error while getting data.", 0)
      }
    )
  }

  addNode(node, parent) {
    let child = this.node.nativeElement.cloneNode(true);
    this.renderer.setStyle(child, "display", "");
    // this.renderer.listen(child.querySelector('.action'), 'click', this.onClick.bind(this, node));
    // this.renderer.listen(child.querySelector('.action-btn'), 'click', this.onPopUp.bind(this, node));
    // node['count'] = ++this.count;
    // child.querySelector('.positionTabg').innerHTML = node['count'];

    if (node.type == "SubCompany" || node.type == "Company" || node.type == "Department") {
      this.renderer.addClass(child.querySelector('figure'), 'unsignned');
      child.querySelector('span').innerHTML = node.title;
      child.querySelector('figcaption small').innerHTML = node.type;
      this.renderer.setStyle(child.querySelector('img'), "display", "none");
      this.renderer.setStyle(child.querySelector('.designation'), "display", "none");
      this.renderer.setStyle(child.querySelector('.profile-btn'), "display", "none");

      if (node.type == "SubCompany" && node.title !== 'Unassigned') {
        // this.renderer.setStyle(child.querySelector('.action'), "display", "none");
        this.renderer.setStyle(child.querySelector('.starBtn'), "display", "");
        this.renderer.setAttribute(child.querySelector('.starBtn'), 'title', 'Sub company invited');
      }
    }

    

    else if (node.type == "Position") {
      this.renderer.setStyle(child.querySelector('span'), "display", "none");
      this.renderer.listen(child.querySelector('.profile-btn'), 'click', this.redirectUser.bind(this, node));
      // this.renderer.listen(child.querySelector('.action-btn'), 'click', this.onPopUp.bind(this, node));
      
      // Employee
      if (node.employeeUserID && node.employee instanceof Object) {
        node['positionType'] = 'employee';

        this.renderer.setStyle(child.querySelector('.action'), "display", "");
        this.renderer.listen(child.querySelector('.action'), 'click', this.onClick.bind(this, node));

        child.querySelector('img').src = (node.employee && node.employee.imageUrl) ? node.employee.imageUrl : 'assets/images/color-yellow.png';
        child.querySelector('figcaption small').innerHTML = (node.employee && node.employee.name) ? node.employee.name : 'User';
        child.querySelector('.designation small').innerHTML = (node.employee && node.employee.position) ? node.employee.position : 'Not specified';
        // child.querySelector('.designation small').innerHTML = node.title;

        this.renderer.setStyle(child.querySelector('.labelsBtn'), "display", "");
        if (node.subRole) {
          (node.subRole['isFao']) ? child.querySelector('.labelsBtn').appendChild(this.getLabel('FA')) : false;
          (node.subRole['isFw']) ? child.querySelector('.labelsBtn').appendChild(this.getLabel('FW')) : false;
          (node.subRole['isOhso']) ? child.querySelector('.labelsBtn').appendChild(this.getLabel('OH&S')) : false;
          (node.subRole['isSho']) ? child.querySelector('.labelsBtn').appendChild(this.getLabel('SHO')) : false;
        }
      }
      // Invited Employee
      else if (node.employeeInvitedID && node.employeeInvited instanceof Object) {
        node['positionType'] = 'invitedEmployee';
        child.querySelector('img').src = (node.employeeInvited && node.employeeInvited.imageUrl) ? node.employeeInvited.imageUrl : 'assets/images/color-yellow.png';
        child.querySelector('figcaption small').innerHTML = (node.employeeInvited && node.employeeInvited.name) ? node.employeeInvited.name : 'User';
        child.querySelector('.designation small').innerHTML = (node.employeeInvited && node.employeeInvited.position) ? node.employeeInvited.position : 'Not specified';
        // child.querySelector('.designation small').innerHTML = node.title;
        this.renderer.setStyle(child.querySelector('.starBtn'), "display", "");
        this.renderer.setAttribute(child.querySelector('.starBtn'), 'title', 'Employee invited');
      }
  
      // Unassigned position
      else {
        node['positionType'] = 'unassigned';
        this.renderer.addClass(child.querySelector('figure'), 'unsignned');
        this.renderer.setStyle(child.querySelector('span'), "display", "");
        child.querySelector('span').innerHTML = 'Unassigned position';
        child.querySelector('.designation small').innerHTML = node.title;
        this.renderer.setStyle(child.querySelector('img'), "display", "none");
        this.renderer.setStyle(child.querySelector('figcaption'), "display", "none");
        this.renderer.setStyle(child.querySelector('.profile-btn'), "display", "none");
      }
    }
    // Contractor
    else if (node.type == "Contractor") {
      this.renderer.setStyle(child.querySelector('span'), "display", "none");
      if (!node.contractorUserID && node.contractorInvitedID){
        this.renderer.setStyle(child.querySelector('.starBtn'), "display", "");
        this.renderer.setStyle(child.querySelector('.starBtn'), "display", (node.showSmiley=="Yes")?"":"none");
        this.renderer.setAttribute(child.querySelector('.starBtn'), 'title', 'Subcontractor invited');
        child.querySelector('figcaption small').innerHTML =node.contractor? node.contractor.name:'Sub contractor';
      }  
      this.renderer.setStyle(child.querySelector('.profile-btn'), "display", "none"); 
      this.renderer.setStyle(child.querySelector('.designation'), "display", "none");
      child.querySelector('figcaption small').innerHTML =node.contractor? node.contractor.name:'Sub contractor';  
    }
    
    parent.querySelector('.tree').appendChild(child);

    if (node.subNodes && node.subNodes.length) {
      this.renderer.addClass(child, 'hasChild');
      for (let i = 0; i < node.subNodes.length; i++) {
        node.subNodes[i]['parentNodeID'] = node.nodeID;
        this.addNode(node.subNodes[i], child);
      }
    }
  }

  getLabel(name) {
    let label = this.renderer.createElement('label');
    this.renderer.addClass(label, 'lblInfo');
    label.innerHTML = name;
    return label;
  }

  onClick(node, event) {
    console.log(event, '====action====', node);
    let actionDiv = event.target.parentNode.nextElementSibling;
    if (actionDiv.style.display == "none") {
      $('.actionDiv').hide();
      this.renderer.setStyle(actionDiv, "display", "");
    } else {
      this.renderer.setStyle(actionDiv, "display", "none");
    }
  }

  redirectUser(node, event) {
    console.log(event, '====profile====', node);
    let dialogRef = this.dialog.open(OrgnisationChartDialog);
    dialogRef.componentInstance.userData = node;
    dialogRef.afterClosed().subscribe(result => {
       $('.actionDiv').hide();
      if (result) {
        // if (result == 'reload') return this.getOrganizationChart();
      }
    });
  }
  onPopUp(node, event) {
    console.log(event, '====popup====', node)
    console.log("hkjdsafkj");
    let dialogRef = this.dialog.open(EmployeeOrganizationChartPopupComponent);
    dialogRef.componentInstance.userData = this.coreService.copyObject(dialogRef.componentInstance.userData, node);
    dialogRef.componentInstance.chart = this.chart;
    dialogRef.componentInstance.candidateID = this.candidateID;
    dialogRef.afterClosed().subscribe(result => {
      $('.actionDiv').hide();
      if (result) {
        if (result == 'reload') return this.getOrganizationChart();
      }
    });
  }
}

@Component({
  selector: 'profile-dialog',
  template: `<div class="profileMainPop" style="max-width:500px;">
               <h3 style="margin-bottom: 15px;" md-dialog-title>{{userData?.employee?.name}}</h3>
        <div md-dialog-content style="padding-bottom: 20px;">
            <img style="max-width:120px;margin-bottom:15px;" [src]="userData?.employee?.imageUrl">
            <p style="margin-bottom:5px;"> <strong>LinkedIn url:</strong> {{userData?.linkedInUrl || 'Not specified'}} </p>
            <p style="margin-bottom:5px;"> <strong>About:</strong> {{userData?.aboutMe || 'Not specified'}} </p>
        </div>
        <div class="text-center">
            <button class="btn btn-default btn-lg" type="button" (click)="dialogRef.close()">Close</button>
        </div>
            </div>
        `,
})
export class OrgnisationChartDialog implements OnInit {

  public userData;
  constructor(public dialogRef: MdDialogRef<OrgnisationChartDialog>) { }

  ngOnInit() { }
}
