import { Injectable } from '@angular/core';
import { RestfulLaravelService } from "../../shared/services/restful-laravel.service";

@Injectable()
export class EmployeeOrganizationChartService {

  constructor(private restfulService: RestfulLaravelService) { }

  getOrganizationChart() {
    return this.restfulService.get('org-chart-get');
  }

  saveOrganizationChart(reqBody) {
    return this.restfulService.put('org-chart-save', reqBody);
  }

  // getOrganizationChart() {
  //   return this.restfulService.get('org-chart-get');
  // }

  terminate(userId, isInvited,nodeID,isInductionComplete) {
    if (isInvited || isInductionComplete == 'No') {
      return this.restfulService.get(`org-chart-action-invited-letter/${nodeID}/revoke-invite-offer`);
    }
    return this.restfulService.get(`org-chart-action-get-terminate-letter/${userId}`);
  }

  makeRedundant(userId, isInvited) {
    if (isInvited) {
      return this.restfulService.get(`org-chart-action-invited-letter/${userId}/invited-redundancy-letter`);
    }
    return this.restfulService.get(`org-chart-action-get-redundant-letter/${userId}`);
  }

  userLeft(userId, isInvited) {
    if (isInvited) {
      return this.restfulService.get(`org-chart-action-invited-letter/${userId}/invited-left-on-accord-letter`);
    }
    return this.restfulService.get(`org-chart-action-get-resignation-letter/${userId}`);
  }

  sendLatter(reqBody) { // {"userID":17,"subject":"","body":""}
    return this.restfulService.post(`org-chart-action-send-letter`, reqBody);
  }

  getPositions() {
    return this.restfulService.get(`org-chart-action-get-position-list`);
  }

  reAssignPosition(reqBody) { // {"employeeUserID":17,"companyPositionID":4}
    return this.restfulService.post(`org-chart-action-reposition`, reqBody);
  }

  moveToNewPosition(reqBody) { // {"nodeID":37,"underParentNodeID":36}
    return this.restfulService.post(`org-chart-action-relocation`, reqBody);
  }

  toggleAssignManager(reqBody) { // {"employeeUserID":17, "assign":"true"}
    return this.restfulService.post(`org-chart-action-toggle-assign-manager`, reqBody);
  }

  getRoles() {
    return this.restfulService.get(`org-chart-action-get-role-list`);
  }

  updateRole(reqBody) {
    return this.restfulService.post(`org-chart-action-update-role`, reqBody);
  }

  updateSubRole(reqBody) {
    return this.restfulService.post(`org-chart-action-update-special-role`, reqBody);
  }

  addDepartment(reqBody) { // {"title":"Development","parentNodeID":36} // {"title":"Mobile Development","parentNodeID":36,"nodeID":38}
    return this.restfulService.post('org-chart-add-edit-department', reqBody);
  }

  addPosition(reqBody) { // {"title":"Development","parentNodeID":36} // {"title":"Mobile Development","parentNodeID":36,"nodeID":38}
    return this.restfulService.post('org-chart-add-edit-position', reqBody)
  }

  addSubCompany(reqBody) { // {"parentNodeID":36} // {"parentNodeID":36,"nodeID":38}
    return this.restfulService.post('org-chart-add-edit-subcompany', reqBody)
  }

  addSubContractor(reqBody) { // {"parentNodeID":36} // {"parentNodeID":36,"nodeID":38}
    return this.restfulService.post('org-chart-action-add-contractor-node', reqBody)
  }

  removeNode(id) {
    return this.restfulService.delete(`org-chart-action-remove-node/${id}`);
  }

  removeSubCompany(id) {
    return this.restfulService.delete(`org-chart-action-remove-sub-company/${id}`);
  }

  resendOffer(id){
    return this.restfulService.get(`org-chart-action-resend-offer/${id}`);
  }
  reminder(reqBody){
    return this.restfulService.post('reminder-induction',reqBody);
  }


}
