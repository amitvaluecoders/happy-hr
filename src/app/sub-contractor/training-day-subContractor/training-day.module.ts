import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { MaterialModule } from '@angular/material';
import { CommonModule } from '@angular/common';
import { TrainingDayRoutingModule } from './training-day.routing';
import { TrainingDayService } from './training-day.service';
import { CreateMasterTrainingPlanComponent } from './create-master-training-plan/create-master-training-plan.component';
import { MasterTrainingPlanDetailComponent } from './master-training-plan-detail/master-training-plan-detail.component';
import { MasterTrainingPlansComponent } from './master-training-plans/training-plans.component';
import { PreviousTrainingPlansComponent } from './previous-training-plans/previous-training-plans.component';
import { CreateTrainingPlanComponent } from './create-training-plan/create-training-plan.component';
import { TrainingPlanListingComponent } from './training-plan-listing/training-plan-listing.component';
import { TrainingDayComponent } from './training-day/training-day.component';
import { EmployeeCertificateOfParticipationComponent } from './employee-certificate-of-participation/employee-certificate-of-participation.component';
import { TrainingPlanDetailComponent } from './training-plan-detail/training-plan-detail.component';
import { DataTableModule } from 'angular2-datatable';
import { QuillModule } from 'ngx-quill';

import { SharedModule } from '../../shared/shared.module';
import { AngularMultiSelectModule } from 'angular2-multiselect-checkbox-dropdown/angular2-multiselect-dropdown';
import { TrainingDayGuardService } from './training-day-guard.service';

@NgModule({
  imports: [
    CommonModule,
    TrainingDayRoutingModule,
    SharedModule,
    AngularMultiSelectModule,
    FormsModule,
    MaterialModule,
    DataTableModule,
    QuillModule
  ],
  declarations: [CreateMasterTrainingPlanComponent, MasterTrainingPlanDetailComponent, MasterTrainingPlansComponent,
    PreviousTrainingPlansComponent, CreateTrainingPlanComponent, TrainingPlanListingComponent, TrainingDayComponent,
    EmployeeCertificateOfParticipationComponent, TrainingPlanDetailComponent],
  providers: [TrainingDayService, TrainingDayGuardService]
})
export class TrainingDayModule { }
