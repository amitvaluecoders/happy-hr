import { Injectable } from '@angular/core';
import { RestfulLaravelService } from "../../shared/services/restful-laravel.service";

@Injectable()
export class TrainingDayService {

  constructor(private restfulService: RestfulLaravelService) { }

  createMasterTrainingPlan(reqBody) {
    return this.restfulService.post('create-update-master-trainning-plan', reqBody);
  }

  getMasterTrainingPlanList() {
    return this.restfulService.get('list-master-trainning-plan');
  }

  getMasterTrainingPlanDetail(id) {
    return this.restfulService.get(`detail-master-trainning-plan/${id}`);
  }

  deleteMasterTrainingPlan(id) {
    return this.restfulService.delete(`delete-master-training-plan/${id}`);
  }

  getPreviousTrainningPlan(masterId) {
    return this.restfulService.get(`previous-master-trainning-plan/${masterId}`)
  }


  createTrainingPlan(reqBody) {
    return this.restfulService.post('create-update-trainning-plan', reqBody);
  }

  getTrainingPlanList(params) {
    return this.restfulService.get(`list-trainning-plan/${params}`);
  }

  getTrainingPlanDetail(id) {
    return this.restfulService.get(`detail-trainning-plan/${id}`);
  }

  deleteTrainingPlan(id) {
    return this.restfulService.delete(`delete-trainning-plan/${id}`);
  }

  rollCall(reqBody) {
    return this.restfulService.post('roll-call', reqBody);
  }

  getTrainingDayDetail(id) {
    return this.restfulService.get(`present-trainee-list/${id}`);
  }

  saveTrainingNotes(reqBody) {
    return this.restfulService.post('save-training-result', reqBody);
  }

  getTraineeDetail(params) {
    return this.restfulService.get(`trainee-detail/${params}`);
  }


  getEmployee() {
    return this.restfulService.get('show-all-employee');
  }

  getManagers() {
    return this.restfulService.get('show-manager-and-ceo');
  }
}
