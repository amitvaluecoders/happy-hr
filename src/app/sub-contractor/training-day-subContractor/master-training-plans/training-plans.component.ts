import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { APPCONFIG } from '../../../config';
import { CoreService } from '../../../shared/services/core.service';
import { TrainingDayService } from "../training-day.service";
import * as moment from 'moment';
import { ConfirmService } from '../../../shared/services/confirm.service';

@Component({
  selector: 'app-training-plans',
  templateUrl: './training-plans.component.html',
  styleUrls: ['./training-plans.component.scss']
})
export class MasterTrainingPlansComponent implements OnInit {

  isProcessing = false;
  masterPlanList = [];
  permission;

  constructor(public companyService: TrainingDayService, public coreService: CoreService, public confirmService: ConfirmService,
    public viewContainerRef: ViewContainerRef) { }

  ngOnInit() {
    APPCONFIG.heading = "Master training plan listing";
    this.permission = this.coreService.getNonRoutePermission();
    this.getMasterPlanList();
  }

  getMasterPlanList() {
    this.isProcessing = true;
    this.companyService.getMasterTrainingPlanList().subscribe(
      res => {
        this.isProcessing = false;
        if (res.code == 200) {
          this.masterPlanList = res.data;
        }
        else return this.coreService.notify("Unsuccessful", res.message, 0);
      },
      err => {
        this.isProcessing = false;
        // if (err.code == 400) {
        //   return this.coreService.notify("Unsuccessful", err.message, 0);
        // }
        // this.coreService.notify("Unsuccessful", "Error while getting data.", 0);
      }
    )
  }

  formatDateTime(date) {
    return moment(date).isValid() ? moment(date).format('DD MMM YYYY hh:mm A') : 'Not specified';
  }

  deletePlan(plan) {
    if(plan.haveChild.length>0){
      //this.coreService.notify("Unsuccessful", "This master plan contain a training plan !!", 1); 
       this.confirmService.confirm("This master plan contain a training plan !!" , "You can not delete this plan.",this.viewContainerRef).subscribe(
         confirm=>{},
              err => {
                if (err.code == 400) {
                  return this.coreService.notify("Unsuccessful", err.message, 0);
                }
                this.coreService.notify("Unsuccessful", "Error while deleting plan.", 0);
              }
            )
          }

    else{
        this.confirmService.confirm("Delete confirmation", "Do you want to delete this plan?", this.viewContainerRef).subscribe(
          confirm => {
            if (confirm) {
              this.companyService.deleteMasterTrainingPlan(plan.masterTrainningPlanID).subscribe(
                res => {
                  if (res.code == 200) {
                    this.coreService.notify("Successful", res.message, 1);
                    this.getMasterPlanList();
                  }
                  else return this.coreService.notify("Unsuccessful", res.message, 0);
                },
                err => {
                  if (err.code == 400) {
                    return this.coreService.notify("Unsuccessful", err.message, 0);
                  }
                  this.coreService.notify("Unsuccessful", "Error while deleting plan.", 0);
                }
              )
            }
          })
      }
}
}