import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { Router } from '@angular/router';
import { APPCONFIG } from '../../../config';
import { CoreService } from '../../../shared/services/core.service';
import { TrainingDayService } from "../training-day.service";
import * as moment from 'moment';
import { ConfirmService } from '../../../shared/services/confirm.service';

@Component({
  selector: 'app-training-plan-listing',
  templateUrl: './training-plan-listing.component.html',
  styleUrls: ['./training-plan-listing.component.scss']
})
export class TrainingPlanListingComponent implements OnInit {

  public toggleFilter = false;
  public progress;

  trainingPlanList = [];
  isProcessing = false;
  loggenInUser:any;
  managerList = [];
  employeeList = [];
  userData:any;
  trainingList=[];
  search = {};
  isChecked = false;
  filters = {
    "filter": { "status": { "Pending": false, "Completed": false }, "assignedBy": [], "assignedTo": [] }
  };
  permission;

  constructor(public companyService: TrainingDayService, public coreService: CoreService, public confirmService: ConfirmService,
    public viewContainerRef: ViewContainerRef, public router: Router) { }

  ngOnInit() {
    // this.progressBarSteps();
    let user=JSON.parse(localStorage.getItem('happyhr-userInfo'));
    this.userData=user;
    this.loggenInUser = user.userRole;
    APPCONFIG.heading = "Training plan listing";
    this.permission = this.coreService.getNonRoutePermission();

    this.getEmployees();
    // this.getManagers();
    this.getTrainingPlanList();
  }

  // progressBarSteps() {
  //   this.progress = {
  //     progress_percent_value: 50, //progress percent
  //     total_steps: 4,
  //     current_step: 2,
  //     circle_container_width: 20, //circle container width in percent
  //     steps: [
  //       { num: 1, data: "Create training plan" },
  //       { num: 2, data: "Employee acceptance", active: true },
  //       { num: 3, data: "Training day" },
  //       { num: 4, data: "Employee certificate" },
  //     ]
  //   }
  // }

  // getManagers() {
  //   this.companyService.getManagers().subscribe(
  //     res => {
  //       if (res.code == 200) {
  //         this.managerList = res.data;
  //       }
  //       else return this.coreService.notify("Unsuccessful", res.message, 0);
  //     },
  //     err => {
  //       if (err.code == 400) {
  //         return this.coreService.notify("Unsuccessful", err.message, 0);
  //       }
  //       this.coreService.notify("Unsuccessful", "Error while getting managers list.", 0);
  //     })
  // }

  getEmployees() {
    this.companyService.getEmployee().subscribe(
      res => {
        if (res.code == 200) {
          this.employeeList = res.data;
        }
        else return this.coreService.notify("Unsuccessful", res.message, 0);
      },
      err => {
        if (err.code == 400) {
          return this.coreService.notify("Unsuccessful", err.message, 0);
        }
        this.coreService.notify("Unsuccessful", "Error while getting employee list.", 0);
      })
  }
// get the training plan list of 
  getTrainingPlanList() {
    console.log(JSON.stringify(this.filters));
    this.isProcessing = true;
    this.companyService.getTrainingPlanList(JSON.stringify(this.filters)).subscribe(
      res => {
        this.isProcessing = false;
        if (res.code == 200) {
          this.trainingPlanList = res.data;

          //filtering the training plan assign to subcontractor
          this.trainingPlanList.filter((item,i)=>{ 
                for(let k of item.assignedTo){
                      if(k.traineeDetail.userID==this.userData.userID){
                                this.trainingList.push(item);
                      }
              }
          })
          // console.log("hsdfghjsajdf",this.trainingList);
        }
        else return this.coreService.notify("Unsuccessful", res.message, 0);
      },
      err => {
        this.isProcessing = false;
        if (err.code == 400) {
          return this.coreService.notify("Unsuccessful", err.message, 0);
        }
        this.coreService.notify("Unsuccessful", "Error while getting data.", 0);
      }
    )
  }

  viewDetail(plan) {
    if (plan.status == 'Pending') {
      this.router.navigate(['/app/company-admin/training-day/training-day', plan.trainningID]);
    } else {
      this.router.navigate(['/app/company-admin/training-day/training-plan/detail', plan.trainningID]);
    }
  }

  validateTrainingDay(plan) {
    let disable = false;

    if (plan.trainningTime == null) {
      disable = true;
    } else if (moment().isBefore(moment(plan.trainningTime))) {
      disable = true;
    }
    return disable;
  }

  formatDateTime(date) {
    return moment(date).isValid() ? moment(date).format('DD MMM YYYY hh:mm A') : 'Not specified';
  }

  onCheckEmp(event, id, type) {
    this.isChecked = true;
    let ind = this.filters.filter[type].indexOf(id);

    if (event.target.checked) {
      (ind >= 0) ? null : this.filters.filter[type].push(id);
    } else {
      (ind >= 0) ? this.filters.filter[type].splice(ind, 1) : null;
    }
  }

  deletePlan(id) {
    this.confirmService.confirm("Delete confirmation", "Do you want to delete this plan?", this.viewContainerRef).subscribe(
      confirm => {
        if (confirm) {
          this.companyService.deleteTrainingPlan(id).subscribe(
            res => {
              if (res.code == 200) {
                this.coreService.notify("Successful", res.message, 1);
                this.getTrainingPlanList();
              }
              else return this.coreService.notify("Unsuccessful", res.message, 0);
            },
            err => {
              if (err.code == 400) {
                return this.coreService.notify("Unsuccessful", err.message, 0);
              }
              this.coreService.notify("Unsuccessful", "Error while deleting plan.", 0);
            }
          )
        }
      })
  }

}
