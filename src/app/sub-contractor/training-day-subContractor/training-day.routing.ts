import { ModuleWithProviders } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CreateMasterTrainingPlanComponent } from './create-master-training-plan/create-master-training-plan.component';
import { MasterTrainingPlanDetailComponent } from './master-training-plan-detail/master-training-plan-detail.component';
import { MasterTrainingPlansComponent } from './master-training-plans/training-plans.component';
import { PreviousTrainingPlansComponent } from './previous-training-plans/previous-training-plans.component';
import { CreateTrainingPlanComponent } from './create-training-plan/create-training-plan.component';
import { TrainingPlanListingComponent } from './training-plan-listing/training-plan-listing.component';
import { TrainingDayComponent } from './training-day/training-day.component';
import { EmployeeCertificateOfParticipationComponent } from './employee-certificate-of-participation/employee-certificate-of-participation.component';
import { TrainingPlanDetailComponent } from './training-plan-detail/training-plan-detail.component';
import { TrainingDayGuardService } from "./training-day-guard.service";

export const TrainingDayPagesRoutes: Routes = [
    {
        path: '',
        canActivate: [],
        children: [
            { path: '', component: MasterTrainingPlansComponent, canActivate: [TrainingDayGuardService] },
            { path: 'master-training-plan/add', component: CreateMasterTrainingPlanComponent, canActivate: [TrainingDayGuardService] },
            { path: 'master-training-plan/edit/:id', component: CreateMasterTrainingPlanComponent, canActivate: [TrainingDayGuardService] },
            { path: 'master-training-plan/detail/:id', component: MasterTrainingPlanDetailComponent, canActivate: [TrainingDayGuardService] },
            { path: 'training-plans/previous/:id', component: PreviousTrainingPlansComponent, canActivate: [TrainingDayGuardService] },
            // { path: 'training-plan/add', component: CreateTrainingPlanComponent, canActivate: [TrainingDayGuardService] },
            { path: 'training-plan/add/:id', component: CreateTrainingPlanComponent, canActivate: [TrainingDayGuardService] },
            { path: 'training-plan/edit/:id', component: CreateTrainingPlanComponent, canActivate: [TrainingDayGuardService] },
            { path: 'training-plan/list', component: TrainingPlanListingComponent, canActivate: [TrainingDayGuardService] },
            { path: 'training-day/:id', component: TrainingDayComponent, canActivate: [TrainingDayGuardService] },
            { path: 'employee-participation/certificate/:id/:uid', component: EmployeeCertificateOfParticipationComponent, canActivate: [TrainingDayGuardService] },
            { path: 'training-plan/detail/:id', component: TrainingPlanDetailComponent, canActivate: [TrainingDayGuardService] },
        ]
    }
];
export const TrainingDayRoutingModule = RouterModule.forChild(TrainingDayPagesRoutes);
