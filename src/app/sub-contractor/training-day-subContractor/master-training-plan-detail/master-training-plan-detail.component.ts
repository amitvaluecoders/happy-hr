import { Component, OnInit } from '@angular/core';
import { APPCONFIG } from '../../../config';
import { Router, ActivatedRoute } from '@angular/router';
import { CoreService } from '../../../shared/services/core.service';
import { TrainingDayService } from "../training-day.service";
import * as moment from 'moment';

@Component({
  selector: 'app-master-training-plan-detail',
  templateUrl: './master-training-plan-detail.component.html',
  styleUrls: ['./master-training-plan-detail.component.scss']
})
export class MasterTrainingPlanDetailComponent implements OnInit {

  isProcessing = false;
  noResult = true;
  masterPlan = {
    "masterTrainningPlanID": "", "title": "", "reason": "", "actionPlan": "", "objective": ""
  }
  constructor(public companyService: TrainingDayService, public route: ActivatedRoute, public coreService: CoreService) { }

  ngOnInit() {
    APPCONFIG.heading = "Master training plan detail";

    if (this.route.snapshot.params['id']) {
      this.masterPlan.masterTrainningPlanID = this.route.snapshot.params['id'];
      this.getMasterPlanDetail();
    }
  }

  getMasterPlanDetail() {
    this.isProcessing = true;
    this.companyService.getMasterTrainingPlanDetail(this.masterPlan.masterTrainningPlanID).subscribe(
      res => {
        this.isProcessing = false;
        if (res.code == 200) {
          this.masterPlan = res.data;
          this.noResult = false;
        }
        else return this.coreService.notify("Unsuccessful", res.message, 0);
      },
      err => {
        this.isProcessing = false;
        // if (err.code == 400) {
        //   return this.coreService.notify("Unsuccessful", err.message, 0);
        // }
        // this.coreService.notify("Unsuccessful", "Error while getting data.", 0);
      }
    )
  }

  formatDateTime(date) {
    return moment(date).isValid() ? moment(date).format('YYYY-MM-DD hh:mm A') : 'Not specified';
  }

}