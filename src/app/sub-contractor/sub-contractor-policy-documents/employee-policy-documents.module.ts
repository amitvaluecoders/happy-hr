import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { QuillModule } from 'ngx-quill';

import { EmployeePolicyDocumentsRoutingModule } from './employee-policy-documents.routing';
import { PolicyDocumentsListComponent } from './policy-documents-list/policy-documents-list.component';
import { PolicyDetailComponent } from './policy-detail/policy-detail.component';
import { VersionHistoryComponent } from './version-history/version-history.component';
import { AcceptChangeComponent } from './accept-change/accept-change.component';
import { ViewOldAndNewVersionsComponent } from './view-old-and-new-versions/view-old-and-new-versions.component';
import { ProceduralDocumentsComponent } from './procedural-documents/procedural-documents.component';
import { ProceduralDocumentsDetailComponent } from './procedural-documents-detail/procedural-documents-detail.component';
import { policyEmployeeService } from './employee-policy.service';
import { DataTableModule } from 'angular2-datatable';
import { SharedModule } from '../../shared/shared.module';
import { MaterialModule } from '@angular/material';
import { FormsModule } from '@angular/forms';


@NgModule({
  imports: [
    FormsModule,
    MaterialModule,
    SharedModule,
    DataTableModule,
    CommonModule,
    EmployeePolicyDocumentsRoutingModule,
    QuillModule
  ],
  providers:[policyEmployeeService],
  declarations: [PolicyDocumentsListComponent, PolicyDetailComponent, VersionHistoryComponent, AcceptChangeComponent, ViewOldAndNewVersionsComponent, ProceduralDocumentsComponent, ProceduralDocumentsDetailComponent]
})
export class EmployeePolicyDocumentsModule { }
