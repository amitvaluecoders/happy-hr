import { Component, OnInit } from '@angular/core';
import { APPCONFIG } from '../../../config';
import { policyEmployeeService } from '../employee-policy.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { CoreService } from '../../../shared/services/core.service';
import * as moment from "moment";
import { MdSnackBar, MdDialog, MdDialogRef } from '@angular/material';
import { ResponseNoteComponent } from '../../../shared/components/response-note/response-note.component';


@Component({
  selector: 'app-policy-detail',
  templateUrl: './policy-detail.component.html',
  styleUrls: ['./policy-detail.component.scss']
})
export class PolicyDetailComponent implements OnInit {
  public relPath:string;
  public isProcessing = false;
  public unsubscriber: any;
  public isActionProcessing = false;
  public policyType = '';
  public policy: any;
  public loggedInUser: any;
  public id = '';
  companyLogo = '';

  constructor(public policyService: policyEmployeeService, public dialog: MdDialog,
    public router: Router, public activatedRoute: ActivatedRoute,
    public coreService: CoreService) { }

  ngOnInit() {
    let role = this.coreService.getRole();
    this.relPath =(role &&  role == 'companyContractor') ?'sub-contractor':'employee';
    this.loggedInUser = JSON.parse(localStorage.getItem('happyhr-userInfo'));
    this.companyLogo = this.coreService.getCompanyLogo();
    console.log('logo ==> ', this.companyLogo);
    APPCONFIG.heading = "Policy details";
    this.policyType = "";
    this.id = "";
    if (this.activatedRoute.snapshot.params['id']) {
      this.policyType = "ce";
      this.id = this.activatedRoute.snapshot.params['id'];
    }
    if (this.activatedRoute.snapshot.params['policyId']) {
      this.policyType = "ca";
      this.id = this.activatedRoute.snapshot.params['policyId'];
    }

    this.getDetails();
  }
  formatDateTime(date) {
    return moment(date).isValid() ? moment(date).format('DD MMM YYYY hh:mm A') : 'Not specified';
  }
  getDetails() {
    this.isProcessing = true;
    let isEmployeeContract = true;
    if (this.policyType == 'ca') isEmployeeContract = false;
    let reqBody = { isHHRContract: isEmployeeContract, id: this.id };
    this.unsubscriber = this.policyService.getDetails(reqBody).subscribe(
      d => {
        if (d.code == "200") {
          this.policy = d.data;
          this.isProcessing = false;
        }
        else {
          this.coreService.notify("Unsuccessful", d.message, 0);
          this.isProcessing = false;
        }
      },
      error => this.coreService.notify("Unsuccessful", error.message, 0),
    )
  }

  onViewVersions() {
   this.router.navigate([`/app/${this.relPath}/policy-documents/version-history/${this.activatedRoute.snapshot.params['id']}`]);
  }


  onAcceptPolicy() {
    this.isActionProcessing = true;
    let id = this.activatedRoute.snapshot.params['id'];
    let reqBody = {
      companyPolicyDocumentID: this.activatedRoute.snapshot.params['id'],
      accept: true
    }
    this.SubmitAcceptReject(reqBody);

  }

  SubmitAcceptReject(reqBody) {
    this.policyService.acceptPolicy(reqBody).subscribe(
      d => {
        if (d.code == "200") {
          this.coreService.notify("Successful", d.message, 1);
          this.router.navigate([`/app/${this.relPath}/policy-documents`]);
        }
        else {
          this.coreService.notify("Unsuccessful", d.message, 0);
          this.isActionProcessing = false;
        }
      },
      error => this.coreService.notify("Unsuccessful", error.message, 0),
    )
  }
  // onRejectContract(){
  //     this.isActionProcessing=true;
  //    let id=this.activatedRoute.snapshot.params['id'];     
  //        let dialogRef = this.dialog.open(ResponseNoteComponent);  
  //       let tempResponse={
  //         url:"employee/employee-accept-contract",
  //         note:{
  //           "employeeUserID":this.loggedInUser.userID,
  //           "contractID":id,
  //           "status":"Rejected",
  //           responseNote:""
  //         }
  //       }
  //         dialogRef.componentInstance.noteDetails=tempResponse;
  //         dialogRef.afterClosed().subscribe(result => {
  //          if(result) this.router.navigate([`/app/employee/contracts`]);
  //          else this.isActionProcessing=false;
  //         });  
  // }

  onViewChangedLog() {
    this.router.navigate([`/app/${this.relPath}/policy-documents/version/old/new/${this.id}`]);
  }
}
