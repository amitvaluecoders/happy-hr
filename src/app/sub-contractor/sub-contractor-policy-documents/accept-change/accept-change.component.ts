import { Component, OnInit } from '@angular/core';
import { APPCONFIG } from '../../../config';

@Component({
  selector: 'app-accept-change',
  templateUrl: './accept-change.component.html',
  styleUrls: ['./accept-change.component.scss']
})
export class AcceptChangeComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    APPCONFIG.heading="Accept new policy version";
  }

}
