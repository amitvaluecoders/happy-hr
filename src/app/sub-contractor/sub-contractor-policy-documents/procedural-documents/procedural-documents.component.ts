import { Component, OnInit } from '@angular/core';
import { APPCONFIG } from '../../../config';
import { policyEmployeeService } from '../employee-policy.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { CoreService } from '../../../shared/services/core.service';
import { MdSnackBar, MdDialog, MdDialogRef } from '@angular/material';
import { ResponseNoteComponent } from '../../../shared/components/response-note/response-note.component';

@Component({
  selector: 'app-procedural-documents',
  templateUrl: './procedural-documents.component.html',
  styleUrls: ['./procedural-documents.component.scss']
})
export class ProceduralDocumentsComponent implements OnInit {
 
  public relPath :string;
   public collapse={};
  public hrToggle=false;
  public unsubscriber:any;
  public toggleFilter: boolean;
  public isProcessing=false;
  public documents=[];
  constructor( public policyService: policyEmployeeService,public dialog: MdDialog,
    public router: Router, public activatedRoute: ActivatedRoute,
    public coreService: CoreService) { }

  ngOnInit() {
    let role = this.coreService.getRole();
    this.relPath =(role &&  role == 'companyContractor') ?'sub-contractor':'employee';
    APPCONFIG.heading="List of procedural documents";
    this.getList();
  }

  getList(){
    this.isProcessing=true;
        this.unsubscriber=this.policyService.getProceduralDocumentList().subscribe(
          d => {
            if (d.code == "200") {
              let doc=d.data;
              doc.filter(item=>{
                if(item.pdFor=='companyContractor'){
                      this.documents.push(item);
                }

              })
              
              this.isProcessing=false;
            }
            else{
              this.coreService.notify("Unsuccessful", d.message, 0);
              this.isProcessing=false;
            } 
          },
          error => this.coreService.notify("Unsuccessful", error.message, 0), 
      )
  }

  onShowDetails(id){
    let url=`/app/${this.relPath}/policy-documents/procedural-document/detail/`+id;
    this.router.navigate([url]);        
  }


}
