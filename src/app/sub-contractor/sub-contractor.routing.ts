import { ModuleWithProviders } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SubContractorGuardService } from './sub-contractor-guard.service';
import { SubcontractorDashboardComponent } from './subcontractor-dashboard/subcontractor-dashboard.component';
export const SubContractorModulePagesRoutes: Routes = [
    {
        path: '',
        canActivate: [SubContractorGuardService],
        children: [
            { path: '', component : SubcontractorDashboardComponent },       
            { path: 'profile/:type', loadChildren: '../user/subcontractor-induction/subcontractor-induction.module#SubcontractorInductionModule' },
            { path: 'contracts', loadChildren: '../company-employee/employee-contracts/employee-contracts.module#EmployeeContractsModule' },
            { path: 'ohs', loadChildren: '../company-employee/employee-ohs/employee-ohs.module#EmployeeOhsModule' },
            { path: 'leaves', loadChildren: '../company-employee/employee-leaves/employee-leaves.module#EmployeeLeavesModule' },
            { path: 'policy-documents', loadChildren: './sub-contractor-policy-documents/employee-policy-documents.module#EmployeePolicyDocumentsModule' },
            { path: 'messages', loadChildren: '../company-employee/employee-messages/employee-messages.module#EmployeeMessagesModule' },
            { path: 'organisation-chart', loadChildren: './sub-contractor-organization-chart/employee-organization-chart.module#EmployeeOrganizationChartModule' },
            { path: 'user-access', loadChildren: '../company-admin/user-access-control/user-access-control.module#UserAccessControlModule'},
            //{ path: 'training-plan', loadChildren: '../company-admin/user-access-control/user-access-control.module#UserAccessControlModule'},
            { path: 'training-day', loadChildren: './training-day-subContractor/training-day.module#TrainingDayModule' },
            
            
        ]
    }
];

export const SubcontractorSectionRoutingModule = RouterModule.forChild(SubContractorModulePagesRoutes);
