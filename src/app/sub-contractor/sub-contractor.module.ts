import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { MaterialModule } from '@angular/material';
import { SharedModule } from '../shared/shared.module';
import { AngularMultiSelectModule } from 'angular2-multiselect-checkbox-dropdown/angular2-multiselect-dropdown';
import { RestfulwebService } from '../shared/services';
import { RestfulLaravelService } from '../shared/services';
import { SubContractorGuardService } from './sub-contractor-guard.service';
import { SubcontractorSectionRoutingModule } from './sub-contractor.routing';
// import {TrainingDayModule} from './training-day-subContractor/training-day.module';
import { SubcontractorDashboardComponent } from './subcontractor-dashboard/subcontractor-dashboard.component';
// import {TrainingPlanListingComponent} from './training-day-subContractor/training-plan-listing/training-plan-listing.component'
@NgModule({
  imports: [
    CommonModule,MaterialModule,
    FormsModule,SharedModule,
    AngularMultiSelectModule,
    SubcontractorSectionRoutingModule,
  
  ],
  declarations: [SubcontractorDashboardComponent],
  providers:[ SubContractorGuardService, RestfulLaravelService, RestfulwebService]
})
export class SubContractorModule { }
