
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule }  from '@angular/router';
import { ManagerMessagesComponent } from './manager-messages/manager-messages.component';

export const MessagePagesRoutes: Routes = [
    {
        path: '',
        children: [
            { path: '',  component: ManagerMessagesComponent },
        ]
    }
];

export const MessagePagesRoutingModule = RouterModule.forChild(MessagePagesRoutes);
