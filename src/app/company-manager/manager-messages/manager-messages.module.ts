import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ManagerMessagesComponent } from './manager-messages/manager-messages.component';
import { MessagePagesRoutingModule } from './manager-messages.routing';
@NgModule({
  imports: [
    CommonModule,
    MessagePagesRoutingModule
  ],
  declarations: [ManagerMessagesComponent]
})
export class ManagerMessagesModule { }
