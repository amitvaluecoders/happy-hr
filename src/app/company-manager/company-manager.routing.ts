import { Routes, RouterModule }  from '@angular/router';            
import { ManagerGuardService } from './manager_guard.service';

export const CompanyEmployeePagesRoutes: Routes = [
    {
        path: '',
        children: [ 
           
            { path: 'manager',loadChildren: '../company-admin/company-admin-dashboard/company-admin-dashboard.module#CompanyAdminDashboardModule' },
            { path: '',loadChildren: './employee-dashboard/employee-dashboard.module#EmployeeDashboardModule' },
            { path:'message', loadChildren:'./employee-messages/employee-messages.module#EmployeeMessagesModule'}
            
        ]
    }
];
export const CompanyEmployeePagesRoutingModule = RouterModule.forChild(CompanyEmployeePagesRoutes);
// console.log("manager tak aaya..........")