import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ManagerGuardService } from './manager_guard.service';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [],
  providers:[ManagerGuardService]
})
export class CompanyManagerModule { }
