import { Routes, RouterModule } from '@angular/router';

import { Page404Component } from './404/404.component';
import { Page500Component } from './500/500.component';
import { PageLockScreenComponent } from './lock-screen/lock-screen.component';
import { PageMaintenanceComponent } from './maintenance/maintenance.component';
import { ThirdPartyNotesComponent } from './third-party-notes/third-party-notes.component';
import { EmployeeWelcomeComponent } from './employee-welcome/employee-welcome.component';
import { UnauthorizedAccessComponent } from './unauthorized-access/unauthorized-access.component';

export const ExtraPagesRoutes: Routes = [
    {
        path: '',
        children: [

            { path: '404', component: Page404Component },
            { path: '500', component: Page500Component },
            { path: 'lock-screen', component: PageLockScreenComponent },
            { path: 'maintenance', component: PageMaintenanceComponent },
            { path: 'third-party', component: ThirdPartyNotesComponent },
            { path: 'employee-welcome', component: EmployeeWelcomeComponent },
            { path: 'unauthorized', component: UnauthorizedAccessComponent }
            
        ]
    }
];

export const ExtraPagesRoutingModule = RouterModule.forChild(ExtraPagesRoutes);
