import { Component, OnInit } from '@angular/core';
import { RestfulLaravelService } from '../../shared/services/restful-laravel.service';
import { CoreService } from '../../shared/services/core.service';
import { Router, ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-employee-welcome',
  templateUrl: './employee-welcome.component.html',
  styleUrls: ['./employee-welcome.component.scss']
})
export class EmployeeWelcomeComponent implements OnInit {

  public isProcessing: boolean;

  constructor(public coreService: CoreService, public restful: RestfulLaravelService,
    public router: Router, public activatedRoute: ActivatedRoute, ) { }

  ngOnInit() {
  }

  getStarted() {

  }
}
