import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ThirdPartyNotesComponent } from './third-party-notes.component';

describe('ThirdPartyNotesComponent', () => {
  let component: ThirdPartyNotesComponent;
  let fixture: ComponentFixture<ThirdPartyNotesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ThirdPartyNotesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ThirdPartyNotesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
