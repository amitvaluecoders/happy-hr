import { Component, OnInit } from '@angular/core';
import { RestfulLaravelService } from '../../shared/services/restful-laravel.service';
import { CoreService } from '../../shared/services/core.service';
import { Router, ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-third-party-notes',
  templateUrl: './third-party-notes.component.html',
  styleUrls: ['./third-party-notes.component.scss']
})
export class ThirdPartyNotesComponent implements OnInit {

  public note='';
  public title = '';
  public isCompleted=false;
  public isButtonClicked=false;
  public params:any;
  public reqBody:any;
  public isCreateProcessing=false;
  constructor(public coreService:CoreService,public restful:RestfulLaravelService,
              public router: Router, public activatedRoute: ActivatedRoute,) { }

  ngOnInit() {
    this.params =this.activatedRoute.queryParams['value'];
    this.title = atob(this.params['title']);
  }

  onThirdPartyNote() {
    this.reqBody ={
      note:this.note,
      title:this.params['title'],
      token:this.params['token'],
      type:this.params['type']
    }
    // console.log("req",reqBody);

    this.isCreateProcessing = true;
     this.restful.post('add-third-party-note',this.reqBody).subscribe(
          d => {
            this.isCreateProcessing = false;
              if (d.code == "200") {
                this.isCompleted=true;
                this.coreService.notify("Successful", d.message, 1);
                this.isCreateProcessing = false; 
              }
              else this.coreService.notify("Unsuccessful", d.message, 0);
          },
          error =>{
            this.coreService.notify("Unsuccessful", error.message, 0);
            this.isCreateProcessing = false;
          } 
          
      )
  }

}
