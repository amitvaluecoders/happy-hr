import { NgModule } from '@angular/core';
import { MaterialModule } from '@angular/material';
import { ExtraPagesRoutingModule } from './extra-pages-routing.module';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms'
import { Page404Component } from './404/404.component';
import { Page500Component } from './500/500.component';

import { PageLockScreenComponent } from './lock-screen/lock-screen.component';
import { PageMaintenanceComponent } from './maintenance/maintenance.component';
import { ThirdPartyNotesComponent } from './third-party-notes/third-party-notes.component';
import { EmployeeWelcomeComponent } from './employee-welcome/employee-welcome.component';
import { UnauthorizedAccessComponent } from './unauthorized-access/unauthorized-access.component';


@NgModule({
    imports: [
        ExtraPagesRoutingModule,
        MaterialModule,
        CommonModule,
        FormsModule
    ],
    declarations: [
        Page404Component,
        Page500Component,
        PageLockScreenComponent,
        PageMaintenanceComponent,
        ThirdPartyNotesComponent,
        EmployeeWelcomeComponent,
        UnauthorizedAccessComponent,
    ]
})

export class ExtraPagesModule {}
