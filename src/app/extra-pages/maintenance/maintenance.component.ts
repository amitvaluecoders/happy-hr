import { Component } from '@angular/core';
import { APPCONFIG } from '../../config'
import { CoreService } from '../../shared/services/core.service'; 

@Component({
    selector: 'my-page-maintenance',
    styles: [],
    templateUrl: './maintenance.component.html'
})

export class PageMaintenanceComponent {
    AppConfig: any;
    message='';

constructor(public coreService:CoreService){

}
    ngOnInit() {
        this.message=this.coreService.getSiteUnderMaintenanceMessage();
        this.AppConfig = APPCONFIG;
    }
}
