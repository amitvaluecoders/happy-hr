import { Routes, RouterModule }  from '@angular/router';
import { AdminLoginComponent } from './login/login.component';
import { SignupComponent } from './signup/signup.component';
import { ForgetPasswordComponent } from './forget-password/forget-password.component';
import { ConfirmEmailComponent } from './confirm-email/confirm-email.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { PasswordAddComponent } from './password-add/password-add.component';
import { UserOfferAcceptComponent } from './user-offer-accept/user-offer-accept.component';

export const TestPagesRoutes: Routes = [
    {
        path: '',
        children: [
            { path: 'login', component: AdminLoginComponent },
            { path: 'signup', component: SignupComponent },
            { path: 'forgot-password', component: ForgetPasswordComponent },
            { path: 'confirm-email/:email', component: ConfirmEmailComponent },
            { path: 'reset-password/:token', component: ResetPasswordComponent },
            { path: 'password-add/:token', component: PasswordAddComponent },
            { path: 'accept-offer/:token', component: UserOfferAcceptComponent },
            { path: 'company-registration', loadChildren: './company-registration/company-registration.module#CompanyRegistrationModule' },
            { path: 'sub-company-registration', loadChildren: './company-registration/company-registration.module#CompanyRegistrationModule' },
            { path: 'employee-induction', loadChildren: '../company-employee/induction/induction.module#InductionModule' },
            { path: 'subcontractor-induction', loadChildren: './subcontractor-induction/subcontractor-induction.module#SubcontractorInductionModule' },
        ]
    }
];

export const ExtraPagesRoutingModule = RouterModule.forChild(TestPagesRoutes);
