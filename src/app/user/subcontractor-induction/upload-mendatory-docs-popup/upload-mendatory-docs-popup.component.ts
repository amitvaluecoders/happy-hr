import { Component, OnInit } from '@angular/core';
import { APPCONFIG } from "../../../config";
import { Router, ActivatedRoute } from "@angular/router";
import { CoreService } from "../../../shared/services/core.service";
import { SubcontractorInductionService } from "../subcontractor.service";
import { RestfulLaravelService } from "../../../shared/services/restful-laravel.service";
import { CountryStateService } from "../../../shared/services/country-state.service";
import { RequestChangePopupComponent } from '../request-change-popup/request-change-popup.component';
import * as moment from 'moment';
import { IDatePickerConfig } from 'ng2-date-picker';
import { MdDialog, MdDialogRef } from '@angular/material';
import { Profile, MyDocument, CompanyDocument } from '../../../company-employee/induction/induction';

@Component({
  selector: 'app-upload-mendatory-docs-popup',
  templateUrl: './upload-mendatory-docs-popup.component.html',
  styleUrls: ['./upload-mendatory-docs-popup.component.scss']
})

export class UploadMendatoryDocsPopupComponent{
doc: CompanyDocument;
  fileName = "";
  config: IDatePickerConfig = {};
  licExpiry: any;
  isProcessingImg: boolean;
  _validFileExtensions = [".jpg", ".jpeg", ".png", ".pdf", ".doc", ".docx"];

  constructor(public dialog: MdDialogRef<UploadMendatoryDocsPopupComponent>, 
               public restfulWebService: RestfulLaravelService,
              public coreService: CoreService) {
    // this.doc = new CompanyDocument;
    this.config.locale = "en";
    this.config.format = "DD MMM YYYY";
    this.config.showMultipleYearsNavigation = true;
    this.config.weekDayFormat = 'dd';
    this.config.disableKeypress = true;
    this.config.monthFormat = "MMM YYYY";
    this.config.min = moment();
  }

  handleFileSelect($event) {
    // this.isProcessingImg = true;
    const files = $event.target.files || $event.srcElement.files;
    const file: File = files[0];
    this.fileName = file.name;
    if(file.size>1961432){
      this.dialog.close();
      return this.coreService.notify("Unsuccessful", " Uploaded document file is to large is must be a file smaller that 2MB. Please try again! ", 0)
    }
  
    let ext = this.fileName.substring(this.fileName.lastIndexOf('.')).toLowerCase();
    if (this._validFileExtensions.indexOf(ext) >= 0) {
      const formData = new FormData();
      formData.append('mandatoryDocuments', file);

      this.restfulWebService.post("file-upload", formData).subscribe(res => {
        this.isProcessingImg = false;
        this.doc.edsFile = res.data.mandatoryDocuments;
      });
    } else return this.coreService.notify("Unsuccessful", "Invalid file extension.", 0)

  }

  upload() {
    if (!this.doc.edsFile) return;
    this.doc.expiryDate = !(this.licExpiry) ? '' : moment(this.licExpiry).format('YYYY-MM-DD');
    this.dialog.close(this.doc);
  }

}
