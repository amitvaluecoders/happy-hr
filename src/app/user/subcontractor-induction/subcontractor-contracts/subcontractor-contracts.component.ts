import { Component, OnInit } from "@angular/core";
import { SubcontractorInductionService } from "../subcontractor.service";
import { CoreService } from "../../../shared/services/core.service";
import { Router } from "@angular/router";
@Component({
  selector: "app-subcontractor-contracts",
  templateUrl: "./subcontractor-contracts.component.html",
  styleUrls: ["./subcontractor-contracts.component.scss"]
})
export class SubcontractorContractsComponent implements OnInit {
  contractURL = "";
  isActionProcessing = false;
  progress:any;
  constructor(
    public router :Router,
    public service: SubcontractorInductionService,
    public coreService: CoreService
  ) {}

  ngOnInit() {
    this.progressBarSteps();
    this.getcontractURL();
  }

  getcontractURL() {
    this.service.getContractURL().subscribe(
      d => {
        if (d.code == "200") this.contractURL = d.data.url;
        else this.coreService.notify("Unsuccessful", d.message, 0);
      },
      err => {
        this.coreService.notify("Unsuccessful", err.message, 0);
      }
    );
  }

  progressBarSteps() {
    this.progress = {
      progress_percent_value: 66,
      total_steps: 3,
      current_step: 2,
      circle_container_width: 25,
      steps: [
        // {num:1,data:"Select Role"},
        { num: 1, data: 'Complete your profile' },
        { num: 2, data: 'Contract approval',active: true },
        { num: 3, data: 'Policy approval' },
        // { num: 5, data: 'Questionnaire' }
      ]
    }
  }

  onAcceptContract() {
    this.isActionProcessing = true;
    this.service.aceeptContract().subscribe(
      d => {
        if (d.code == "200") {
          this.coreService.notify("Successful", d.message, 1);
          this.router.navigate(["/user/subcontractor-induction/policy-approval"]);
          this.isActionProcessing = false;
        }
        else this.coreService.notify("Unsuccessful", d.message, 0);
      },
      err => {
        this.isActionProcessing = false;
        this.coreService.notify("Unsuccessful", err.message, 0);
      }
    );
  }
}
