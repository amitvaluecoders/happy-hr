import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubcontractorContractsComponent } from './subcontractor-contracts.component';

describe('SubcontractorContractsComponent', () => {
  let component: SubcontractorContractsComponent;
  let fixture: ComponentFixture<SubcontractorContractsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubcontractorContractsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubcontractorContractsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
