import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { APPCONFIG } from '../../../../config';
import { SubcontractorInductionService } from "../../subcontractor.service";
import { CoreService } from '../../../../shared/services/core.service';
import { MdDialogRef, MdDialog, MdSnackBar } from '@angular/material';

@Component({
  selector: 'app-policy-document-details-popup',
  templateUrl: './policy-document-details-popup.component.html',
  styleUrls: ['./policy-document-details-popup.component.scss']
})
export class PolicyDocumentDetailsPopupComponent implements OnInit {
public policyID:any;
public isProcessing=false;
public unsubscriber:any;
public isActionProcessing=false;
public policyType='';
 public policy:any;
public loggedInUser:any;
 public id='';
constructor(public service: SubcontractorInductionService, public coreService: CoreService,
  public router: Router, public dialog: MdDialog) { }


  ngOnInit() {
    this.getDetails();
  }

  getDetails(){
    this.isProcessing=true;
        this.unsubscriber=this.service.getPolicyDetails(this.policyID).subscribe(
          d => {
            if (d.code == "200") {
              this.policy=d.data;
              this.isProcessing=false;
            }
            else{
              this.coreService.notify("Unsuccessful", d.message, 0);
              this.isProcessing=false;
            } 
          },
          error => this.coreService.notify("Unsuccessful", error.message, 0), 
      )
  }

}
