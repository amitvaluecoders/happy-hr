import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PolicyDocumentDetailsPopupComponent } from './policy-document-details-popup.component';

describe('PolicyDocumentDetailsPopupComponent', () => {
  let component: PolicyDocumentDetailsPopupComponent;
  let fixture: ComponentFixture<PolicyDocumentDetailsPopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PolicyDocumentDetailsPopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PolicyDocumentDetailsPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
