import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubcontractorPoliciesComponent } from './subcontractor-policies.component';

describe('SubcontractorPoliciesComponent', () => {
  let component: SubcontractorPoliciesComponent;
  let fixture: ComponentFixture<SubcontractorPoliciesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubcontractorPoliciesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubcontractorPoliciesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
