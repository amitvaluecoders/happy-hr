import { Component, OnInit } from '@angular/core';
import { RestfulLaravelService } from "../../../shared/services/restful-laravel.service";
import { Router, ActivatedRoute } from "@angular/router";
import { CoreService } from "../../../shared/services/core.service";
import { MdDialogRef, MdDialog, MdSnackBar } from '@angular/material';


@Component({
  selector: 'app-request-change-popup',
  templateUrl: './request-change-popup.component.html',
  styleUrls: ['./request-change-popup.component.scss']
})
export class RequestChangePopupComponent implements OnInit {
listContractType:any;
listRateType:any;
//listContractType = [];
ownContract:any={};
ownContractList:any;
  contractTypeID:any;
reqBody:any;
listInvoiceType:any;
isButtonClicked:Boolean;
fileName='';
tempRequest:any;
dropdownSettings = {
      singleSelection: true,
      text: "Select",
      enableSearchFilter: true,
      classes: "myclass"
    };;
selectedContractType = [];
selectedRateType = [];
selectedInvoiceType =[];
selectedOwnContractType=[];
isProcessing:boolean=false;

  constructor(public dialogRef: MdDialogRef<RequestChangePopupComponent>,public coreService:CoreService,public restfulWebService:RestfulLaravelService) { }

  ngOnInit() {
    this.tempRequest = JSON.parse(JSON.stringify(this.reqBody));
    this.setInitialValues(); 
  }

  setInitialValues(){
    if(this.reqBody.type=='Rate') this.selectedRateType = this.listRateType.filter(i=>i.value==this.reqBody.newValue);
    if(this.reqBody.type=='Contract') this.selectedContractType = this.listContractType.filter(i=>i.value==this.reqBody.newValue);
    if(this.reqBody.type=='Invoice date') this.selectedInvoiceType = this.listInvoiceType.filter(i=>i.value==this.reqBody.newValue);
  }
  
  onSelectContractType(item, selected) {
    this.contractTypeID = (selected) ? item.value : null;
  }

  onSave(isValid){
    if(this.reqBody.type=='Rate') this.reqBody.newValue =  this.selectedRateType[0].value;
    if(this.reqBody.type=='Invoice date') this.reqBody.newValue =  this.selectedInvoiceType[0].value;
    if(this.reqBody.type=='Contract'){
      this.reqBody.newValue =  this.selectedContractType[0].value;
      if(this.reqBody.newValue=='x') this.reqBody.newValue=this.selectedOwnContractType[0].value;
      if(this.reqBody.newValue == 'Upload') this.reqBody.uploadFile = this.fileName;
    } 

    this.sendRequest();
    console.log(isValid,"req body:",this.reqBody)
  }

  sendRequest(){
    this.isProcessing= true;
    this.restfulWebService.post('contractor/change-request',this.reqBody).subscribe(
        d => {
          if ((d.code = "200")) {
             this.dialogRef.close(d.data);
            this.coreService.notify("Successful", d.message, 1);
             this.isProcessing= false;
          }
          else  this.isProcessing= false;
        },
        error => {
        this.coreService.notify("Unsuccessful", error.message, 0);
         this.isProcessing= false;
        }
      );
    }
  

  fileChange(event) {
    this.fileName = "";
    let fileList: FileList = event.target.files;
    if (fileList.length > 0) {
      let file: File = fileList[0];
      let formData: FormData = new FormData();
      formData.append("contractorContract", file);
      this.restfulWebService.post("file-upload", formData).subscribe(
        d => {
          if ((d.code = "200")) {
            this.fileName = d.data.contractorContract;
          }
        },
        error => {
        
        }
      );
    }
  }

}
