import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RequestChangePopupComponent } from './request-change-popup.component';

describe('RequestChangePopupComponent', () => {
  let component: RequestChangePopupComponent;
  let fixture: ComponentFixture<RequestChangePopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RequestChangePopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RequestChangePopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
