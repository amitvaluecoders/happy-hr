import {  ModuleWithProviders  } from '@angular/core';
import {  RouterModule,Routes  } from '@angular/router';
import { SubcontractorCompleteProfileComponent } from "./subcontractor-complete-profile/subcontractor-complete-profile.component";
import { SubcontractorContractsComponent } from "./subcontractor-contracts/subcontractor-contracts.component";
import { SubcontractorPoliciesComponent } from "./subcontractor-policies/subcontractor-policies.component";

export const SubcontractorInductionPagesRoutes: Routes = [
    {
        path: '',
        children: [
           
            { path: '', component: SubcontractorCompleteProfileComponent},
            { path: 'contract-approval', component: SubcontractorContractsComponent},
            { path: 'policy-approval', component: SubcontractorPoliciesComponent},

        ]
    }
];

export const SubcontractorInductionRoutingModule = RouterModule.forChild(SubcontractorInductionPagesRoutes);
