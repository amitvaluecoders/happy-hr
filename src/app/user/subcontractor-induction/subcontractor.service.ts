import { Injectable } from '@angular/core';
import { RestfulLaravelService } from '../../shared/services';
import { Http, Headers, Response, RequestOptions } from '@angular/http';


@Injectable()
export class SubcontractorInductionService {

  constructor(
    private restfulWebService: RestfulLaravelService) { }


  registerSubcontractor(details) {
    return this.restfulWebService.post(`contractor/update-profile`, details);
  }

  getProfileInfo() {
    return this.restfulWebService.get(`contractor/get-profile`);
  }

  getContractURL() {
    return this.restfulWebService.get(`contractor/get-contract`);
  }

  aceeptContract(){
    return this.restfulWebService.put(`contractor/accept-contract`,{});    
  }


  getPolicies(){
    return this.restfulWebService.get(`contractor/get-policy-list-for-contractor`);    
  }


  aceeptPolicies(){
    return this.restfulWebService.put(`contractor/accept-policy-by-contractor`,{});    
  }

  getPolicyDetails(id){
    return this.restfulWebService.get(`employee/policy-detail/${id}`);    
  }
   
   acceptRejectRequest(reqBody){
     return this.restfulWebService.post(`contractor-approve-request`,reqBody);
  }


}