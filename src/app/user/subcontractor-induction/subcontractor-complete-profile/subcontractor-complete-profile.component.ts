import { Component, OnInit, ViewContainerRef } from "@angular/core";
import { APPCONFIG } from "../../../config";
import { Router, ActivatedRoute } from "@angular/router";
import { CoreService } from "../../../shared/services/core.service";
import { SubcontractorInductionService } from "../subcontractor.service";
import { RestfulLaravelService } from "../../../shared/services/restful-laravel.service";
import { CountryStateService } from "../../../shared/services/country-state.service";
import { ConfirmService } from '../../../shared/services/confirm.service';
import { RequestChangePopupComponent } from '../request-change-popup/request-change-popup.component';
import {  UploadMendatoryDocsPopupComponent } from '../upload-mendatory-docs-popup/upload-mendatory-docs-popup.component';
import * as moment from 'moment';
import { IDatePickerConfig } from 'ng2-date-picker';
import { MdDialog, MdDialogRef } from '@angular/material';
import { Profile, MyDocument, CompanyDocument } from '../../../company-employee/induction/induction';

@Component({
  selector: "app-subcontractor-complete-profile",
  templateUrl: "./subcontractor-complete-profile.component.html",
  styleUrls: ["./subcontractor-complete-profile.component.scss"]
})
export class SubcontractorCompleteProfileComponent implements OnInit {
  public errors=[];
  public progress;
  public docRequired=false;
  collapse:any;
  constructor(public viewContainerRef:ViewContainerRef,
    public confirmService: ConfirmService,  
    public dialog:MdDialog,
    public countryService: CountryStateService,
    public restfulWebService: RestfulLaravelService,
    public coreService: CoreService,
    public router: Router,
    public activatedRoute: ActivatedRoute,
    public service: SubcontractorInductionService
  ) {} 
    public stateList =[];
  public isProcessing: Boolean;
  public isButtonClicked = false;
  profileImage='';
  public payment = "company";
  isActionProcessing = false;
  isGetDetailsProcessing = false;
  signatureText= '';
  public invite = {
    companyName: "",
    ABN: "",
    address: "",
    suburb: "",
    stateName: "",
    email: "",
    phoneNumber: "",
    firstName: "",
    lastName: "",
    rateType: "",
    rateValue: "",
    scope: "",
    invoicePaid: "",
    postalCode: "",
    abnContractType: "",
    invoicePaidOther: "",
    contractFileName: "",
    nodeID: "",
    sign:"", 
    signature:{
      type:'',
      text:''
    }, 
     mandatoryDocuments:[],
     employeeDocumentSubmit:[],
     bankDetail: {
          "bankName": "",
          "bsbNumber": "",
          "accountName": "",
          "accountNumber": ""
      }
  };
  adoptSign = true;  
  public isInvited = false;
  public docPath = "";
  public docError = false;
  public fileName: any;

  dropdownSettings = {};

  listRateType = [];
  selectedRateType = [];

  listInvoiceType = [];
  selectedInvoiceType = [];

  listContractType = [];
  selectedContractType = [];
  employeeDocumentSigned=[];
  employeeDocumentSubmit=[];
  isProcessingImg=false;
  profile ={ 
    leaveType:[]
  };
  ngOnInit() {  
    this.collapse={      
      c1:true,c2:true,c3:true,c4:true,c5:true,c6:false
    }
    this.stateList = this.countryService.getState(13);
    APPCONFIG.heading = "General information";
    this.progressBarSteps();
    this.setDropdownItems();
    this.dropdownSettings = {
      singleSelection: true,
      text: "Select",
      enableSearchFilter: true,
      classes: "myclass"
    };
    this.getProfileInfo();
    this.isInvited = !this.activatedRoute.snapshot.params['type']; 
  }
  formatDateTime(date) {
    return moment(date).isValid() ? moment(date).format('DD MMM YYYY hh:mm A') : 'Not specified';
  }


 
    getProfileInfo(){
     this.isGetDetailsProcessing = true;
    this.service.getProfileInfo().subscribe(
      d => {
        if (d.code == "200") {
          this.invite = d.data;
          this.profile = d.data['0'];
          this.profile['leaveType'] = []; 
          this.selectedRateType = this.listRateType.filter(k=> k.value == this.invite.rateType);
          this.selectedInvoiceType = this.listInvoiceType.filter(k=> k.value == this.invite.invoicePaid);
          this.selectedContractType = this.listContractType.filter(k=> k.value == this.invite.abnContractType);  
          this.signatureText = this.invite.signature.text ? this.invite.signature.text : this.invite.firstName+" "+ this.invite.lastName;
          if(this.invite.mandatoryDocuments.length){
              this.flashRedWhenUpload();   
          }
          this.invite.mandatoryDocuments =  this.invite.mandatoryDocuments.filter(k=>{
            k['isProcessing']= false;
            return k;
          });
          this.profileImage=this.profile['personalInformation'].profileImageUrl;
          this.isGetDetailsProcessing = false;
        }
      },
      err => {
        this.isGetDetailsProcessing = false;
        this.coreService.notify("Unsuccessful", err.message, 0);
      }
    );
  }

  selectSignature(sign) {
    this.invite.signature.type = sign;
    this.invite.signature.text = this.signatureText;
  }



  progressBarSteps() {
    this.progress = {
      progress_percent_value: 25, //progress percent
      total_steps: 3,
      current_step: 1,
      circle_container_width: 25, //circle container width in percent
      steps: [
        // {num:1,data:"Select Role"},
        { num: 1, data: "General informtion", active: true },
        { num: 2, data: "Contract approval" },
        { num: 3, data: "Policy approval" }
      ]
    };
  }
 
      setDropdownItems(){
        this.listRateType = [
          { id : 0, itemName : "Rate per hour including GST" , value : "Per hour" },
          { id : 1, itemName : "Rate per hour +GST" , value : "Per hour GST" },
          { id : 2, itemName : "Rate per day including GST" , value : "Per day" },
          { id : 3, itemName : "Rate per day +GST" , value : "Per day GST" },
          { id : 4, itemName : "Project fee including GST" , value : "Project"},
          { id : 5, itemName : "Project fee + GST" , value : "Project GST"},
          { id : 6, itemName : "Percentage of sales", value : "Percentage"},
          { id : 7, itemName : "Percentage of consultation", value : "Percentage consulation"},
          { id : 8, itemName : "Percentage of GP", value : "Percentage GP"},
        ]

        this.listInvoiceType = [
          { id : 0, itemName : "Day of invoice" , value : "0" },
          { id : 1, itemName : "7 Days from invoice date" , value : "7" },
          { id : 2, itemName : "14 Days from invoice date" , value : "14" },
          { id : 3, itemName : "30 Days from invoice date" , value : "30" },
          { id : 4, itemName : "45 Days from invoice date" , value : "45" },
          { id : 5, itemName : "60 Days from invoice date" , value : "60" },
          { id : 6, itemName : "Other days from invoice date" , value : "-1" },
        ]

        this.listContractType = [
          { id : 0, itemName : "Use Happy HR ABN contract" , value : "HHR" },
          { id : 1, itemName : "Use own ABN contract" , value : "Own" },
          { id : 2, itemName : "Upload of current contract" , value : "Upload" },
          { id : 3, itemName : "Own contract" , value: "Own_1"},
          { id : 3, itemName : "Own contract 2" , value: "Own_2"},
          { id : 3, itemName : "Own contract 3" , value: "Own_3"},
          { id : 3, itemName : "Own contract 4" , value: "Own_4"},
          
        ]
    }
    checkValidations(elArray){
      for(let e of elArray){
        if(e.invalid){
          //this.isAmountExist=true;
          return true;
        }   
      }
      return false;
    }

  fileChange(event) {
    this.isProcessing = true;
    this.docError = false;
    this.fileName = "";
    this.docPath = "";
    let fileList: FileList = event.target.files;
    if (fileList.length > 0) {
      let file: File = fileList[0];
      let formData: FormData = new FormData();
      formData.append("eligibilityDoc", file);
      this.restfulWebService.post("file-upload", formData).subscribe(
        d => {
          if ((d.code = "200")) {
            this.isProcessing = false;
            this.docPath = d.data.eligibilityDoc;
            this.fileName = file.name;
          } else {
            this.docError = true;
            this.isProcessing = false;
          }
        },
        error => {
          this.docError = true;
          this.isProcessing = false;
        }
      );
    }
  }

    onDowloadMAndatoryDocument(doc){
      window.open(doc.fileUrl);    
    }


   

  handleFileSelect($event) {
    const files = $event.target.files || $event.srcElement.files;
    const file: File = files[0];
    let validFileExtensions = [".jpg", ".jpeg", ".png"];
    
    if(file.size>1961432){
      return this.coreService.notify("Unsuccessful", "The photo file is to large! Please use a file smaller that 2MB. Please try again.", 0)
    }
    let ext = file.name.substring(file.name.lastIndexOf('.')).toLowerCase();
    if (validFileExtensions.indexOf(ext) >= 0) {
      const formData = new FormData();
      formData.append('profileImage', file);
    
    this.isProcessingImg = true;
    this.restfulWebService.post("upload-file", formData).subscribe(res => {
      this.isProcessingImg = false;
      this.profileImage = res.data.profileImage;
      this.profile['personalInformation'].profileImage = res.data.fileName;
    });
    } else return this.coreService.notify("Unsuccessful", "Invalid file extension.", 0)
  }


  RequestForChange(req){
      let dialogRef = this.dialog.open(RequestChangePopupComponent);
      dialogRef.componentInstance.listContractType = this.listContractType;
      dialogRef.componentInstance.listRateType = this.listRateType;
      dialogRef.componentInstance.listInvoiceType = this.listInvoiceType;
      dialogRef.componentInstance.reqBody = JSON.parse(JSON.stringify(req));
      dialogRef.afterClosed().subscribe(r => {
        if (r) {
          this.profile['contractorInformation'] = r;
        }
      })

  } 

   onAcceptReject(req,action){
      req['isProcessing'] = true;
      this.service.acceptRejectRequest({
      contractorChangeRequestID:req.contractorChangeRequestID,
      action:action}).subscribe(
      d => {
        if (d.code == "200") {
          this.coreService.notify("Successful", d.message, 1);  
          this.profile['contractorInformation'].Historical= d.data.Historical?d.data.Historical:[];
          this.profile['contractorInformation'].Pending= d.data.Pending?d.data.Pending:[];
          this.profile['contractorInformation'].Current= d.data.Current?d.data.Current:[];
          req['isProcessing'] = true;
          return false;
        }
      },
      err => {
          req['isProcessing'] = true;
        this.coreService.notify("Unsuccessful", err.message, 0);
      }
    );
    return;
  }
 

  register(form) {
    
    this.errors=[];
    this.errors = this.coreService.getFormValidationErrors(form);
    if(this.errors.length>0){
     
    }
    if (this.errors && this.errors.length==0) {

      this.invite.rateType = this.selectedRateType[0].value;
      this.invite.invoicePaid = this.selectedInvoiceType[0].value;
      this.invite.abnContractType = this.selectedContractType[0].value; 
      this.invite.contractFileName = this.fileName;
      this.invite['employeeDocuments'] = this.employeeDocumentSubmit;
      this.invite.nodeID = this.activatedRoute.snapshot.params["nodeID"];
      this.isActionProcessing = true;
      this.invite['profile']= this.profile;
      this.service.registerSubcontractor(this.invite).subscribe(
        d => {
          if (d.code == "200") {
            let url ='';
            let tempType = this.activatedRoute.snapshot.params["type"] ;
            url = (tempType && tempType=='own') ?'/app/sub-contractor':'/user/subcontractor-induction/contract-approval'
            this.router.navigate([url]);
            this.coreService.notify("Successful", d.message, 1);
            this.isActionProcessing = false;
          }
        },
        err => {
        this.isActionProcessing = false;
          this.coreService.notify("Unsuccessful", err.message, 0);
        }
      );
    }
  }


  onViewContract(req){
    window.open(req.uploadFileUrl);
  }

onDelete(data){
  this.confirmService.confirm(
    this.confirmService.tilte,
    this.confirmService.message,
    this.viewContainerRef
  )
    .subscribe(res => {
      let result = res;
      if (result) {
        this.restfulWebService.delete(`contractor/change-request-delete/${data.contractorChangeRequestID}`)
          .subscribe(
          res => {
            if (res.code == 200) {
              this.coreService.notify("Success", res.message, 1);
              this.profile['contractorInformation'] = res.data;
            }
            else return this.coreService.notify("Unsuccessful", res.message, 0);
          },
          err => {
            if (err.code == 400) {
              return this.coreService.notify("Unsuccessful", err.message, 0);
            }
            this.coreService.notify("Unsuccessful", "Problem while performing action.", 0)
          }
          );
      }
    })
}

    onDeleteMandDocs(docs){
      this.confirmService.confirm(this.confirmService.tilte,this.confirmService.message,this.viewContainerRef)
      .subscribe(res => {
      let result = res;
       if (result){
       // this.docRequired =true;
         docs.file='';
         docs['expiryDate']='';
         this.flashRedWhenDelete();
      } 
    })
    }

    removeDoc(index) {
        this.confirmService.confirm(this.confirmService.tilte,this.confirmService.message,this.viewContainerRef)
      .subscribe(res => {
      let result = res;
      if (result)  this.invite.employeeDocumentSubmit.splice(index, 1);
    })
     
    }

    onpopup() {
    let dialogRef = this.dialog.open(UploadSubcontractorDocumentDialog, { width: '500px' });
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        console.log("res",result);
       this.invite.employeeDocumentSubmit.push(result);
      }
    });
  }

  //upload mandatory
  uploadDocs(doc) {  

    let dialogRef = this.dialog.open(UploadMendatoryDocsPopupComponent, { width: '500px' });
    dialogRef.componentInstance.doc=JSON.parse(JSON.stringify(doc));
      dialogRef.afterClosed().subscribe((result) => {
      if(result) { 
      doc['description'] = result['desc']; 
      doc['file'] = result['edsFile']; 
      doc['expiryDate'] = result['expiryDate'];
      this.flashRedWhenUpload();
      }})       
  }



//flash red the mandatory field when file uploaded 
flashRedWhenUpload(){
  for(let item of this.invite.mandatoryDocuments){
    if(!item.file){
      this.docRequired = true;
      break;
    }
    else{
      this.docRequired = false;
    }
  } 
}
//flash red the mandatory field when file deleted 
flashRedWhenDelete(){
  this.invite.mandatoryDocuments.forEach(item=>{
    if(!item.file){
      this.docRequired=true;
      return;
    }
  })
}




}



@Component({
  selector: 'upload-document-subcontractor',
  template: `
      <h3>Upload document</h3>
      <form name="docForm" #docForm="ngForm" (ngSubmit)="doc.edsFile && docForm.valid && upload()">
        <div class="form-group">
          <label>Title</label>
          <input type="text" name="title" #title="ngModel" [(ngModel)]="doc.title" placeholder="Title ie. Driver's Licence" required/>
          <div *ngIf="title.dirty || docForm.submitted">
              <div class="color-danger" [hidden]="title.valid"> Please enter the title. </div>
          </div>
        </div>
        <div>
        <div class="form-group">
          <label>Description</label>
          
          <textarea type="text" placeholder="Description" rows="4" name="description" #description="ngModel" [(ngModel)]="doc.desc" required></textarea>
          <div *ngIf="description.dirty || description.touched || docForm.submitted">
              <div class="color-danger" [hidden]="description.valid"> Please enter the description. </div>
          </div>
        </div>
        </div>
 
        <div>
        <div class="form-group">
          <label>Expiry</label>
          <dp-date-picker class="form-control" name="expbiryDate" #expiryDate="ngModel" [theme]="'dp-material'" [mode]="'day'" [(ngModel)]="licExpiry" [config]="config" placeholder="Choose a date"></dp-date-picker>
        </div>
      </div>

        <div>
        <div class="uploadBtn">
          <label>{{fileName}}</label>
          <input style="display:none;" id="uploadFL" type="file" accept=".doc,.docx,.pdf,image/png,image/jpg,image/jpeg" name="doc" (change)="handleFileSelect($event)" />
          <label style="display:block;" for="uploadFL" class="btn btn-lg btn-primary">
            Upload document
          </label>
          </div>
          <strong>{{uploadeDocName}}</strong>
          <p>Select file (JPEG, PNG, PDF or DOC files) </p>
         <div *ngIf="!doc.edsFile && docForm.submitted">
              <div class="color-danger"> Please select a document. </div>
          </div>
        </div>
        <md-progress-spinner *ngIf="isProcessingImg" mode="indeterminate" color=""></md-progress-spinner>
        <button *ngIf="!isProcessingImg" [disabled]='!doc.edsFile' type="submit" class="btn btn-primary">Submit</button>
      </form>
          `,
  styles: [`
      h3{
        font-size:22px;
      }
      .form-group label{
        font-size: 15px;
        color: #4b4c4d;
        max-width: 600px;
        width: 100%;
        padding-right: 18px;
        font-family: inherit;
        display: block;
        padding-right: 12px;
      }
      .form-group input{
        display: block;
        width: 100%;
        padding: 0.5rem 0.75rem;
        font-size: 1rem;
        line-height: 1.25;
        color: #464a4c;
        background-color: #fff;
        background-image: none;
        background-clip: padding-box;
        border: 1px solid rgba(0, 0, 0, 0.15);
        border-radius: 0.2rem;
        transition: border-color ease-in-out 0.15s, box-shadow ease-in-out 0.15s;
      }
      .form-group textarea{
         display: block;
         width: 100%;
         border: 1px solid rgba(0, 0, 0, 0.15);
        border-radius: 0.2rem;
        transition: border-color ease-in-out 0
      }
      .uploadBtn{
        float:left;
        width:100%;
      }
      .uploadBtn .btn-primary{
          padding:6px;
          float:left;
          width:100%;
          position:relative;
      }
      .uploadBtn input[type="file"]{
          width: 100%;
          position: absolute;
          float: left;
          height: 100%;
          left: 0;
          top: 0;
          opacity: 0;
        }
      .uploadBtn label {
          display: none;
    
    
  `]
})
export class UploadSubcontractorDocumentDialog {
  doc: CompanyDocument;
  fileName = "";
  uploadeDocName:any;
  config: IDatePickerConfig = {};
  licExpiry: any;
  isProcessingImg: boolean;
  _validFileExtensions = [".jpg", ".jpeg", ".png", ".pdf", ".doc", ".docx"];

  constructor(public dialog: MdDialogRef<UploadSubcontractorDocumentDialog>, 
               public restfulWebService: RestfulLaravelService,
              public coreService: CoreService) {
    this.doc = new CompanyDocument;
    this.config.locale = "en";
    this.config.format = "DD MMM YYYY";
    this.config.showMultipleYearsNavigation = true;
    this.config.weekDayFormat = 'dd';
    this.config.disableKeypress = true;
    this.config.monthFormat = "MMM YYYY";
    this.config.min = moment();
  }

  handleFileSelect($event) {
    // this.isProcessingImg = true;
    const files = $event.target.files || $event.srcElement.files;
    const file: File = files[0];
    this.fileName = file.name;
    if(file.size>1961432){
      //this.isProcessingImg
      this.dialog.close();
      return this.coreService.notify("Unsuccessful", " Uploaded document file is to large is must be a file smaller that 2MB. Please try again!  ",0)
    }
    let ext = this.fileName.substring(this.fileName.lastIndexOf('.')).toLowerCase();
    if (this._validFileExtensions.indexOf(ext) >= 0) {
      const formData = new FormData();
      formData.append('userDocument', file);

      this.restfulWebService.post("upload-file", formData).subscribe(res => {
        this.isProcessingImg = false;
        this.doc.edsFile = res.data.userDocument;
        this.uploadeDocName =res.data.fileName;
      });
    } else return this.coreService.notify("Unsuccessful", "Invalid file extension.", 0)
  }

  upload() {
    if (!this.doc.edsFile) return;
    this.doc.expiryDate = !(this.licExpiry) ? '' : moment(this.licExpiry).format('YYYY-MM-DD');
    this.dialog.close(this.doc);
  }
}