import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SubcontractorCompleteProfileComponent,UploadSubcontractorDocumentDialog } from './subcontractor-complete-profile/subcontractor-complete-profile.component';
import { SubcontractorContractsComponent } from './subcontractor-contracts/subcontractor-contracts.component';
import { SubcontractorPoliciesComponent } from './subcontractor-policies/subcontractor-policies.component';
import { SubcontractorInductionService } from "./subcontractor.service";
import { SubcontractorInductionRoutingModule } from "./subcontractor.routing";
import { MaterialModule } from '@angular/material';
import { FormsModule } from '@angular/forms';
import { SharedModule } from '../../shared/shared.module';
import { AngularMultiSelectModule } from 'angular2-multiselect-checkbox-dropdown/angular2-multiselect-dropdown';
import { DataTableModule } from 'angular2-datatable';
import { PolicyDocumentDetailsPopupComponent } from './subcontractor-policies/policy-document-details-popup/policy-document-details-popup.component';
import { CountryStateService } from "../../shared/services/country-state.service";
import { ConfirmService } from '../../shared/services/confirm.service';

@NgModule({
  imports: [
    DataTableModule,
    AngularMultiSelectModule,
    SharedModule,
    FormsModule,
    MaterialModule,
    SubcontractorInductionRoutingModule,
    CommonModule
  ],
  providers:[SubcontractorInductionService,CountryStateService,ConfirmService],
  entryComponents:[PolicyDocumentDetailsPopupComponent ,UploadSubcontractorDocumentDialog],
  declarations: [SubcontractorCompleteProfileComponent, SubcontractorContractsComponent, SubcontractorPoliciesComponent,PolicyDocumentDetailsPopupComponent,UploadSubcontractorDocumentDialog]
})
export class SubcontractorInductionModule { }
