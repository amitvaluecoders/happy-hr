import { Component, OnInit } from "@angular/core";
import { UserService } from "../../shared/services/user.service";
import { Cookie } from "ng2-cookies/ng2-cookies";
import { Router, ActivatedRoute, Params } from "@angular/router";
import { CoreService } from "../../shared/services";
import { APPCONFIG } from "../../config";
import { Device } from "ng2-device-detector";

@Component({
  providers: [Device],
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.scss"]
})
export class AdminLoginComponent implements OnInit {
  public isProcessing = false;
  public user = {
    email: "",
    password: "",
    rememberme: false
  };

  public role;
  public permission = [];

  public invalidUser = false;

  constructor(
    private userservice: UserService,
    private router: Router,
    private device: Device,
    private core: CoreService // private socketServiceService:SocketServiceService
  ) {}

  ngOnInit() {
    this.checkUserAuth();
    if (Cookie.get("happyhr-user"))
      this.user = JSON.parse(Cookie.get("happyhr-user"));
  }

  checkUserAuth() {
       Cookie.deleteAll();
      localStorage.clear();
      
    // let user = JSON.parse(localStorage.getItem("happyhr-userInfo"));
    // let permission = JSON.parse(localStorage.getItem("happyhr-permission"));
    // let cok = this.core.getRole();
    // let tempuserInfo = JSON.parse(localStorage.getItem("happyhr-userInfo"));
    // let incompletePage = tempuserInfo ? tempuserInfo.incompletePage : 0;
    // if (user && permission && cok && incompletePage == -1) {
    //   this.redirectUserToDashboard(user);
    // } else {
    //   Cookie.deleteAll();
    //   localStorage.clear();
    // }
  }



  submit(isvalid) {
    if (isvalid) {
      this.isProcessing = true;
      if (this.user.rememberme)
        Cookie.set("happyhr-user", JSON.stringify(this.user));
      this.userservice.login(this.user).subscribe(
        data => {
          if (data.code == "200") {
            // APPCONFIG.canSwitch = false;
            this.core.setCanSwitch(false);
            if (data.data.userRole == "") this.invalidUser = true;
            else {
              Cookie.set("happyhr-token", data.token);
              Cookie.set("happyhr-userInfo", JSON.stringify(data.data));
              localStorage.setItem(
                "happyhr-userInfo",
                JSON.stringify(data.data)
              );
              localStorage.setItem("happyhr-companyLogo", data.companyLogo);
              let url = "";
              if (
                data.data.userRole == "superAdmin" ||
                data.data.userRole == "superSubAdmin"
              ) {
                this.role = "superAdmin";
                this.permission = data.data.modules;
                this.core.setRole(this.role);
                this.core.setPermission(data.data.modules);
                url = "/app/super-admin";
              } else if (data.data.userRole == "companyAdmin") {
                 //*** if user have assigned mulitple role like(employee and admin or manager)**// 
                if(data.data.oldUserRole){
                  if(data.data.oldUserRole=="companyContractor"){
                    switch (data.data.incompletePage) {
                      case 0: // basic details
                        url = "/user/subcontractor-induction";
                        break;
                      case 1: // contracts
                        url = "/user/subcontractor-induction/contract-approval";
                        break;
                      case 2: //policy
                        url = "/user/subcontractor-induction/policy-approval";
                        break;
                      default: 
                          url = "/app/sub-contractor";
                    }
                    APPCONFIG.oldUserRole=data.data.oldUserRole;  
                    localStorage.setItem("oldUserRole",data.data.oldUserRole);
                                  
                    this.role = "companyContractor";
                    this.core.setRole(this.role);
                    Cookie.set("happyhr-token", data.token);
                    Cookie.set("happyhr-userInfo", JSON.stringify(data.data));
                    localStorage.setItem(
                      "happyhr-userInfo",
                      JSON.stringify(data.data)
                    );
                    this.permission = data.data.modules;
                  this.core.setRole(this.role);
                  this.core.setPermission(data.data.modules);

                  }
                  
                  else if(data.data.oldUserRole="companyEmployee"){
                    switch (data.data.incompletePage) {
                      case 1: // basic details
                        url = "/user/employee-induction";
                        break;
                      case 2: // contracts
                        url = "/user/employee-induction/contract-approval";
                        break;
                      case 3: //position
                        url = "/user/employee-induction/position-description";
                        break;
                      case 4: //policy
                        url = "/user/employee-induction/policy-approval";
                        break;
                      default: {
                        if (data.data.terminated)
                          url = "/app/employee/after-termination";
                        else url = "/app/employee";
                      }
                    }
                    APPCONFIG.oldUserRole=data.data.oldUserRole;
                    localStorage.setItem("oldUserRole",data.data.oldUserRole);                                
                    this.role = "companyEmployee";
                    this.core.setRole(this.role);
                    Cookie.set("happyhr-token", data.token);
                    Cookie.set("happyhr-userInfo", JSON.stringify(data.data));
                    localStorage.setItem(
                      "happyhr-userInfo",
                      JSON.stringify(data.data)
                    );
                    this.permission = data.data.modules;
                  this.core.setRole(this.role);
                  this.core.setPermission(data.data.modules);

                  }
                 }
                //*******
                else{
                switch (data.data.incompletePage) {
                  case 0:
                    url = "/user/company-registration/payment-options";
                    break;
                  case 1:
                    url = "/user/company-registration/questionnaire";
                    break;
                  case 2:
                    url = "/user/company-registration/questionnaire/step/2";
                    break;
                  case 3:
                    url = "/user/company-registration/questionnaire/step/3";
                    break;
                  case 4:
                    url = "/user/company-registration/questionnaire/step/4";
                    break;
                  default:
                    url = "/app/company-admin";
                }
                APPCONFIG.oldUserRole=data.data.oldUserRole;
                localStorage.setItem("oldUserRole",data.data.oldUserRole);
                this.role = "companyAdmin";
                this.core.setRole(this.role);
                this.permission = data.data.modules;
                this.core.setRole(this.role);
                this.core.setPermission(data.data.modules);
             }
              } else if (data.data.userRole == "companyManager") {
                //*** if user have assigned mulitple role like(employee and admin or manager)**// 

                // if(data.data.oldUserRole){
                //   if(data.data.oldUserRole=="companyContractor"){
                //     switch (data.data.incompletePage) {
                //       case 0: // basic details
                //         url = "/user/subcontractor-induction";
                //         break;
                //       case 1: // contracts
                //         url = "/user/subcontractor-induction/contract-approval";
                //         break;
                //       case 2: //policy
                //         url = "/user/subcontractor-induction/policy-approval";
                //         break;
                //       default: 
                //           url = "/app/sub-contractor";
                //     }
                //     APPCONFIG.oldUserRole=data.data.oldUserRole;  
                //     localStorage.setItem("oldUserRole",data.data.oldUserRole);
                                  
                //     this.role = "companyContractor";
                //     this.core.setRole(this.role);
                //     Cookie.set("happyhr-token", data.token);
                //     Cookie.set("happyhr-userInfo", JSON.stringify(data.data));
                //     localStorage.setItem(
                //       "happyhr-userInfo",
                //       JSON.stringify(data.data)
                //     );

                //   }
                  
                    switch (data.data.incompletePage) {
                      case 1: // basic details
                        url = "/user/employee-induction";
                        break;
                      case 2: // contracts
                        url = "/user/employee-induction/contract-approval";
                        break;
                      case 3: //position
                        url = "/user/employee-induction/position-description";
                        break;
                      case 4: //policy
                        url = "/user/employee-induction/policy-approval";
                        break;
                      default: {
                        if (data.data.terminated)
                          url = "/app/employee/after-termination";
                        else url = "/app/employee";
                      }
                    }
                    APPCONFIG.oldUserRole=data.data.oldUserRole;
                    localStorage.setItem("oldUserRole",data.data.oldUserRole);                                
                    this.role = "companyEmployee";
                    this.core.setRole(this.role);
                    Cookie.set("happyhr-token", data.token);
                    Cookie.set("happyhr-userInfo", JSON.stringify(data.data));
                    localStorage.setItem(
                      "happyhr-userInfo",
                      JSON.stringify(data.data)
                    );
                    this.core.setRole(this.role);
                    this.permission = data.data.modules;
                    this.core.setPermission(data.data.modules);
                    //this.core.setPermission(data.response.data.permission);
                    // APPCONFIG.canSwitch = true;
                    this.core.setCanSwitch(true);

                  
                
                //*******
                // APPCONFIG.oldUserRole=data.data.oldUserRole; 
                // localStorage.setItem("oldUserRole",data.data.oldUserRole);                               
                // this.role = "companyManager";
                // this.core.setRole(this.role);
                // this.permission = data.data.modules;
                // this.core.setPermission(data.data.modules);
               
                // this.core.setCanSwitch(true);
                // url = "/app/company-manager";
                
              } else if (data.data.userRole == "companyEmployee") {
                switch (data.data.incompletePage) {
                  case 1: // basic details
                    url = "/user/employee-induction";
                    break;
                  case 2: // contracts
                    url = "/user/employee-induction/contract-approval";
                    break;
                  case 3: //position
                    url = "/user/employee-induction/position-description";
                    break;
                  case 4: //policy
                    url = "/user/employee-induction/policy-approval";
                    break;
                  default: {
                    if (data.data.terminated)
                      url = "/app/employee/after-termination";
                    else url = "/app/employee";
                  }
                }
                APPCONFIG.oldUserRole=data.data.oldUserRole;
                localStorage.setItem("oldUserRole",data.data.oldUserRole);                                
                this.role = "companyEmployee";
                this.core.setRole(this.role);
                Cookie.set("happyhr-token", data.token);
                Cookie.set("happyhr-userInfo", JSON.stringify(data.data));
                localStorage.setItem(
                  "happyhr-userInfo",
                  JSON.stringify(data.data)
                );
              }

              else if (data.data.userRole == "companyContractor") {
                switch (data.data.incompletePage) {
                  case 0: // basic details
                    url = "/user/subcontractor-induction";
                    break;
                  case 1: // contracts
                    url = "/user/subcontractor-induction/contract-approval";
                    break;
                  case 2: //policy
                    url = "/user/subcontractor-induction/policy-approval";
                    break;
                  default: 
                      url = "/app/sub-contractor";
                }
                APPCONFIG.oldUserRole=data.data.oldUserRole;  
                localStorage.setItem("oldUserRole",data.data.oldUserRole);
                              
                this.role = "companyContractor";
                this.core.setRole(this.role);
                Cookie.set("happyhr-token", data.token);
                Cookie.set("happyhr-userInfo", JSON.stringify(data.data));
                localStorage.setItem(
                  "happyhr-userInfo",
                  JSON.stringify(data.data)
                );
              }

              // console.log("url",url)
              this.core.setRole(this.role);
              // Cookie.set('happyhr-role', this.role);
              Cookie.set("happyhr-permission", JSON.stringify(this.permission));
              localStorage.setItem(
                "happyhr-permission",
                JSON.stringify(this.permission)
              );

              // this.socketServiceService.connectSocket();
              // let socket = io.connect('http://10.0.1.85:8000');
              // socket.on('connect', () => {
              //   //console.log('Successfully connected!');
              //   });
              //   let socketRequest={
              //     token:data.token,
              //     userID:data.data.userID
              //   }
              //   socket.emit('add user',socketRequest);
              //console.log("length of cookie", Cookie.get('happyhr-permission').)
              Cookie.set("happyhr-userInfo", JSON.stringify(data.data));
              APPCONFIG.role = this.role;
              this.router.navigate([url]);
            }
            this.core.initializeAppConstractor();
          } else if (data.code == "503") {
            this.router.navigate(["/extra/maintenance"]);
          }
          APPCONFIG.subRole = data.data.subRole;
        },
        error => {
          this.isProcessing = false;
          this.core.notify("Unsuccessful", error.message, 0);
          if (error.code == 503) this.router.navigate(["/extra/maintenance"]);
        },
        () => console.log("a")
      );
    }
  }
}
