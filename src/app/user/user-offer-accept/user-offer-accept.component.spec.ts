import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserOfferAcceptComponent } from './user-offer-accept.component';

describe('UserOfferAcceptComponent', () => {
  let component: UserOfferAcceptComponent;
  let fixture: ComponentFixture<UserOfferAcceptComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserOfferAcceptComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserOfferAcceptComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
