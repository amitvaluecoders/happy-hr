import { Component, OnInit } from '@angular/core';
import { UserService } from '../../shared/services/user.service';
import { Cookie } from 'ng2-cookies/ng2-cookies';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { CoreService } from '../../shared/services/core.service';


@Component({
  selector: 'app-user-offer-accept',
  templateUrl: './user-offer-accept.component.html',
  styleUrls: ['./user-offer-accept.component.scss']
})
export class UserOfferAcceptComponent implements OnInit {

  public isProcessing=false;
  constructor(private userservice: UserService,public activatedRoute:ActivatedRoute, 
            private router:Router,public coreService:CoreService) { }

  ngOnInit() {
    this.onAcceptOffer();
  }

  onAcceptOffer(){
    this.isProcessing=true;
    this.userservice.userAcceptOffer(this.activatedRoute.snapshot.params['token']).subscribe(
      data => {
          if(data.code=='200'){
            this.isProcessing=false;
            this.coreService.notify("Successful",data.message,1);
          }
          else this.coreService.notify("Unsuccessful",data.message,0);
      },
      error=>{
        this.router.navigate(["/extra/404"])
        this.coreService.notify("Unsuccessful",error.message,0);
      }
    ); 
  }

  submit(){
    
  }

}
