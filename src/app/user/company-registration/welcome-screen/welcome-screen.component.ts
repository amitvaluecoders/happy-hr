import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-welcome-screen',
  templateUrl: './welcome-screen.component.html',
  styleUrls: ['./welcome-screen.component.scss']
})
export class WelcomeScreenComponent implements OnInit {
public userName="";
public progress;
  constructor(private router: Router) { }

  ngOnInit() {
    let tempInfo = JSON.parse(localStorage.getItem('happyhr-userInfo'));
  if(tempInfo && tempInfo.fullName) this.userName=JSON.parse(localStorage.getItem('happyhr-userInfo')).fullName;    
     this.progressBarSteps();
  }
  submit(){
     this.router.navigate(['/user/company-registration/questionnaire']);
  }

 progressBarSteps() {
      this.progress = {
      progress_percent_value: 100, //progress percent
      sevicetotal_steps: 3,
      current_step: 3,
      circle_container_width: 33, //circle container width in percent
      steps: [
        { num: 1, data: "Sign up" },
        { num: 2, data: "Pick a plan"},
        { num: 3, data: "Confirmation" , active: true }
      ]
    }         
  }

}
