import {  ModuleWithProviders  } from '@angular/core';
import {  RouterModule,Routes  } from '@angular/router';
import {  CompanyRegistrationComponent  } from './company-registration/company-registration.component';
import {  PaymentOptionsComponent  } from './payment-options/payment-options.component';
import {  WelcomeScreenComponent  } from './welcome-screen/welcome-screen.component';
import { QuestionnaireComponent } from './questionnaires/questionnaire/questionnaire.component';
import { QuestionnaireStepTwoComponent } from './questionnaires/questionnaire-step-two/questionnaire-step-two.component';
import { QuestionnaireStepThreeComponent } from './questionnaires/questionnaire-step-three/questionnaire-step-three.component';
import { QuestionnaireStepFourComponent } from './questionnaires/questionnaire-step-four/questionnaire-step-four.component';

export const CompanyRegisterationPagesRoutes: Routes = [
    {
        path: '',
        children: [
           
            { path: '', component: CompanyRegistrationComponent},
            //{ path: ':nid', component: CompanyRegistrationComponent},
            { path: 'payment-options', component: PaymentOptionsComponent },
            { path: 'payments/:id', component: PaymentOptionsComponent },
            { path: 'welcome-screen', component: WelcomeScreenComponent },
            { path: 'questionnaire', component: QuestionnaireComponent },
            { path: 'questionnaire/step/2', component: QuestionnaireStepTwoComponent },
            { path: 'questionnaire/step/3', component: QuestionnaireStepThreeComponent },
            { path: 'questionnaire/step/4', component: QuestionnaireStepFourComponent },

            // { path: 'checklist', loadChildren: './checklist-management/checklist-management.module#ChecklistManagementModule' },
            // { path: 'company', loadChildren: './company/company.module#CompanyModule' },
            // { path: 'email-template', loadChildren: './email-template/email-template.module#EmailTemplateModule' },
            // { path: 'user-access', loadChildren: './user-access/user-access.module#UserAccessModule'},
            // { path: 'position-description', loadChildren: './position-description/position-description.module#PositionDescriptionModule' },
            // { path: 'feedback-management', loadChildren: './feedback-management/feedback-management.module#FeedbackManagementModule'},
            // { path: 'ohs', loadChildren: './ohs/ohs.module#OHSModule'},
            // { path: 'site-setting', loadChildren: './site-setting/site-setting.module#SiteSettingModule'},
            // { path: 'policy', loadChildren: './policy-document-management/policy-document-management.module#PolicyDocumentManagementModule'},
            // { path: 'contract', loadChildren: './contracts/contracts.module#ContractsModule'},
            // { path: 'letters', loadChildren: './letters/letters.module#LettersModule'},
            // { path: 'exit-survey', loadChildren: './exit-survey/exit-survey.module#ExitSurveyModule'},
            // { path: 'discount-coupon-management', loadChildren: './discount-coupon-management/discount-coupon-management.module#DiscountCouponManagementModule'},
            // { path: 'message', loadChildren: './message/message.module#MessageModule'},
            // { path: 'broadcast', loadChildren: './broadcast/broadcast.module#BroadcastModule'},
            // { path: 'survey', loadChildren: './survey-questionnare/survey-questionnare.module#SurveyQuestionnareModule'},
            // { path: 'subscription-plans', loadChildren: './subscription-plans/subscription-plans.module#SubscriptionPlansModule'}
            
        ]
    }
];

export const CompanyRegisterationRoutingModule = RouterModule.forChild(CompanyRegisterationPagesRoutes);
