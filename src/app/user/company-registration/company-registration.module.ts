import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MyDatePickerModule } from 'mydatepicker';
import { CompanyRegisterationRoutingModule } from './company-registration.routing';
import { CompanyRegistrationComponent } from './company-registration/company-registration.component';
import { PaymentOptionsComponent, ViewDetailPopupComponent } from './payment-options/payment-options.component';
import { WelcomeScreenComponent } from './welcome-screen/welcome-screen.component';
import { SharedModule } from '../../shared/shared.module';
import { FormsModule } from '@angular/forms';
import { CompanyRegistration } from './company-registration.service';
import { RestfulLaravelService } from '../../shared/services';
import { QuestionnaireComponent, CompanyAwardDialog } from './questionnaires/questionnaire/questionnaire.component';
import { MaterialModule } from '@angular/material';
import { QuillModule } from 'ngx-quill';
import { NguiDatetimePickerModule } from '@ngui/datetime-picker';
import { QuestionnaireStepTwoComponent, AddValueDialog } from './questionnaires/questionnaire-step-two/questionnaire-step-two.component';
import { QuestionnaireStepThreeComponent } from './questionnaires/questionnaire-step-three/questionnaire-step-three.component';
import { QuestionnaireStepFourComponent } from './questionnaires/questionnaire-step-four/questionnaire-step-four.component';
import { CountryStateService } from "../../shared/services/country-state.service";

@NgModule({
  imports: [
    QuillModule,
    CommonModule,
    CompanyRegisterationRoutingModule,
    SharedModule,
    MyDatePickerModule,
    FormsModule,
    MaterialModule,
    NguiDatetimePickerModule
  ],
  entryComponents: [AddValueDialog, ViewDetailPopupComponent, CompanyAwardDialog],
  declarations: [CompanyRegistrationComponent, PaymentOptionsComponent, WelcomeScreenComponent, QuestionnaireComponent, AddValueDialog, QuestionnaireStepTwoComponent,
    QuestionnaireStepThreeComponent, QuestionnaireStepFourComponent, ViewDetailPopupComponent, CompanyAwardDialog],
  providers: [CompanyRegistration, RestfulLaravelService, CountryStateService]
})
export class CompanyRegistrationModule { }
