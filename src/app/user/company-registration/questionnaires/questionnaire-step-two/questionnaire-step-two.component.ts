import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MdSnackBar, MdDialog, MdDialogRef } from '@angular/material';
import { Cookie } from "ng2-cookies/ng2-cookies";
import { CompanyRegistration } from '../../company-registration.service';
import { CoreService } from '../../../../shared/services/core.service';
import { IDatePickerConfig } from 'ng2-date-picker';
import * as moment from "moment";

@Component({
  selector: 'app-questionnaire-step-two',
  templateUrl: './questionnaire-step-two.component.html',
  styleUrls: ['./questionnaire-step-two.component.scss']
})
export class QuestionnaireStepTwoComponent implements OnInit {

  public progress;
  isProcessing = false;
  isProcessingForm = false;
  questionData = [];
  public options = {};
  public errors = {};
  dateTime = {};
  dateConfig: IDatePickerConfig = {};
  timeConfig: IDatePickerConfig = {};


  constructor(public router: Router, public dialog: MdDialog, public companyRegistration: CompanyRegistration, public coreService: CoreService) {
    this.timeConfig.locale = this.dateConfig.locale = "en";
    this.dateConfig.format = "DD MMM YYYY hh:mm A";
    this.timeConfig.format = "hh:mm A";
    this.dateConfig.showMultipleYearsNavigation = true;
    this.dateConfig.weekDayFormat = 'dd';
    this.timeConfig.disableKeypress = this.dateConfig.disableKeypress = true;
    this.dateConfig.monthFormat = "MMM YYYY";
  }

  ngOnInit() {
    this.progressBarSteps();
    this.getQuestions()
  }

  onClickLogo(){
    Cookie.deleteAll();
    localStorage.clear();
    this.router.navigate(['/']);
  }


  progressBarSteps() {
    this.progress = {
      progress_percent_value: 50, //progress percent
      total_steps: 4,
      current_step: 2,
      circle_container_width: 25, //circle container width in percent
      steps: [
        { num: 1, data: "Questionnaire 1" },
        { num: 2, data: "Questionnaire 2", active: true },
        { num: 3, data: "Questionnaire 3" },
        { num: 4, data: "Questionnaire 4" },
      ]
    }
  }

  getQuestions() {
    if (this.questionData.length > 0) return;
    this.isProcessing = true;
    this.companyRegistration.getQuestions(1).subscribe(
      res => {
        this.isProcessing = false;
        this.questionData = res.data;
        this.validateOptions();
        console.log("Question Data", res);
      },
      error => {
        this.isProcessing = false;
        this.coreService.notify("Unsuccessful", error.message, 0)
      })
  }

  validateOptions() {
    for (let i = 0; i < this.questionData.length; i++) {
      this.questionData[i]['other'] = this.questionData[i]['other'] ? this.questionData[i]['other'] : null;
      if (this.questionData[i].field === "Option") {
        this.options[i] = this.questionData[i].answer;
        if (this.questionData[i].companyQuestionairID === 4) {
          for (let j = 0; j < this.questionData[i].answer.length; j++) {
            if (this.questionData[i].options.indexOf(this.questionData[i].answer[j]) == -1) {
              this.questionData[i].options.push(this.questionData[i].answer[j]);
            }
          }
        }
      }
      if (this.questionData[i].field == "Time") this.dateTime[i] = !this.questionData[i].answer[0] ? moment() : moment(this.questionData[i].answer[0], 'hh:mm A');
      // if (this.profile.questionair[i].field == "Date") this.dateTime[i] = moment(this.profile.questionair[i].answer[0], 'DD MMM YYYY hh:mm A') || '';
    }
  }

  saveAsDraft() {
    for (let key in this.options) {
      this.questionData[key].answer = this.options[key];
    }
    this.saveAs('draft');
  }

  saveAndNext() {
    for (let key in this.options) {
      this.questionData[key].answer = this.options[key];
    }
    if (this.isValid()) {
      this.saveAs('save');
    }
  }

  saveAs(type) {
    this.isProcessingForm = true;
    this.companyRegistration.saveQuestion({ 'saveType': type, 'data': this.questionData })
      .subscribe(
      res => {
        this.isProcessingForm = false;
        if (res.code == 200) {
          this.coreService.notify("Successful", res.message, 1);
          if (type == 'save') {
            this.router.navigate(['/user/company-registration/questionnaire/step/3']);
          }
        }
      },
      error => {
        this.isProcessingForm = false;
        this.coreService.notify('Unsuccessful', error.message, 0);
      })
  }

  onCheckOption(event, Qindex) {
    this.errors['answer' + Qindex] = '';
    if (!this.options.hasOwnProperty(Qindex)) this.options[Qindex] = [];
    if (event.target.checked) {
      if (this.options[Qindex].length == 10) {
        this.coreService.notify('', 'You can only choose maximum ten options', 0);
        event.target.checked = false;
        return false;
      }
      this.options[Qindex].push(event.target.value);
    } else {
      let ind = this.options[Qindex].indexOf(event.target.value);
      if (ind >= 0) {
        this.options[Qindex].splice(ind, 1);
        this.errors['answer' + Qindex] = this.options[Qindex].length == 0 ? 'Required' : '';
      }
    }
  }

  validate(value, field) {
    console.log('value', value, field)
    this.errors[field] = ''
    this.errors[field] = (value == '' || value == undefined || value == null) ? 'Required' : '';
  }

  isValidQuestion(i) {
    let index;
    let qsn = this.questionData[i];
    switch (qsn.companyQuestionairID) {
      case 36:
        index = this.questionData.findIndex(i => i.companyQuestionairID == 35);
        return this.questionData[index].answer.length && this.questionData[index].answer[0] == 'Yes';
      case 37:
        index = this.questionData.findIndex(i => i.companyQuestionairID == 35);
        return this.questionData[index].answer.length && this.questionData[index].answer[0] == 'Yes';
      case 38:
        index = this.questionData.findIndex(i => i.companyQuestionairID == 35);
        return this.questionData[index].answer.length && this.questionData[index].answer[0] == 'Yes';
      case 40:
        index = this.questionData.findIndex(i => i.companyQuestionairID == 39);
        return this.questionData[index].answer.length && this.questionData[index].answer[0] == 'Yes';
      case 42:
        index = this.questionData.findIndex(i => i.companyQuestionairID == 41);
        return this.questionData[index].answer.length && this.questionData[index].answer[0] == 'Yes';
      case 44:
        index = this.questionData.findIndex(i => i.companyQuestionairID == 43);
        return this.questionData[index].answer.length && this.questionData[index].answer[0] == 'Yes';
      case 46:
        index = this.questionData.findIndex(i => i.companyQuestionairID == 45);
        return this.questionData[index].answer.length && this.questionData[index].answer[0] == 'Yes';
      case 48:
        index = this.questionData.findIndex(i => i.companyQuestionairID == 47);
        return this.questionData[index].answer.length && this.questionData[index].answer[0] == 'Yes';
      case 50:
        index = this.questionData.findIndex(i => i.companyQuestionairID == 49);
        return this.questionData[index].answer.length && this.questionData[index].answer[0] == 'Yes';
      case 52:
        index = this.questionData.findIndex(i => i.companyQuestionairID == 51);
        return this.questionData[index].answer.length && this.questionData[index].answer[0] == 'Yes';

      default: return true;
    }
  }

  isValid() {
    let count = 0;
    for (let i = 0; i < this.questionData.length; i++) {
      if (this.isValidQuestion(i) && (this.questionData[i].answer.length == 0 || this.questionData[i].answer[0] == '')) {
        count++;
        this.validate(this.questionData[i].answer[0], 'answer' + i);
      }
    }
    return count == 0;
  }

  isChecked(option, index) {
    return this.questionData[index].answer.indexOf(option) >= 0;
  }

  setTime(Qindex) {
    let answer = this.dateTime[Qindex];
    if (this.questionData[Qindex].field == 'Date') {
      this.questionData[Qindex].answer[0] = moment(answer).format('YYYY-MM-DD HH:mm:ss');
    } else if (this.questionData[Qindex].field == 'Time') {
      this.questionData[Qindex].answer[0] = moment(answer).format('HH:mm:ss');
    }
  }

  addValue() {
    let dialogRef = this.dialog.open(AddValueDialog, { width: '400px' });
    dialogRef.afterClosed().subscribe(value => {
      if (value) {
        this.questionData.filter(i => i.companyQuestionairID === 4).shift().options.push(value);
      }
    })
  }

}

@Component({
  selector: 'add-value-dialog',
  template: `<h3 md-dialog-title>Add your own value</h3>
        <div md-dialog-content style="padding-bottom: 20px;">
         <form #qsnForm="ngForm" name="material_signup_form" class="md-form-auth form-validation" (ngSubmit)="qsnForm.valid && onSubmit(value)">  
            <div class="full-width">
              <input mdInput  required rows="4" name="name" [(ngModel)]="value" #name="ngModel" placeholder="Enter Name">
            </div> 
            <div class="alert alert-danger" *ngIf="name.invalid && qsnForm.submitted">
                Name is required !
            </div>
          </form>       
        </div>
        <div>
            <button class="btn btn-primary btn-lg" type="submit" [disabled]="qsnForm.invalid" (click)="qsnForm.valid && onSubmit(value)">Submit</button>
            <button class="btn btn-default btn-lg" type="button" (click)="dialogRef.close()">Close</button>
        </div>`,
})
export class AddValueDialog implements OnInit {

  public value = "";
  constructor(public dialogRef: MdDialogRef<AddValueDialog>) { }

  ngOnInit() { }

  onSubmit(value) {
    this.dialogRef.close(value);
  }
}
