import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QuestionnaireStepTwoComponent } from './questionnaire-step-two.component';

describe('QuestionnaireStepTwoComponent', () => {
  let component: QuestionnaireStepTwoComponent;
  let fixture: ComponentFixture<QuestionnaireStepTwoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QuestionnaireStepTwoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuestionnaireStepTwoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
