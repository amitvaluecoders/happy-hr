import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Cookie } from "ng2-cookies/ng2-cookies";
import { CoreService } from '../../../../shared/services/core.service';
import { CompanyRegistration } from '../../company-registration.service';
import { Questionnaire1 } from '../../company-registration';
import { CountryStateService } from "../../../../shared/services/country-state.service";
import { MdSnackBar, MdDialog, MdDialogRef } from '@angular/material';

@Component({
  selector: 'app-questionnaire',
  templateUrl: './questionnaire.component.html',
  styleUrls: ['./questionnaire.component.scss']
})
export class QuestionnaireComponent implements OnInit {

  public progress;
  public isButtonClicked = false;
  public question: Questionnaire1;
  public isProcessing = false; 
  public isProcessingForm = false;
  public isProcessingImg = false;
  public countryList = [];
  public stateList = [];
  disableState = true;

  constructor(private router: Router, private companyRegistration: CompanyRegistration, public coreService: CoreService,
    public countryStateService: CountryStateService, public dialog: MdDialog) {
    this.question = new Questionnaire1;
  }

  ngOnInit() {

    this.progressBarSteps();
    this.countriesLists();
    this.getQuestionnaire1();
  }

  onClickLogo(){
    Cookie.deleteAll();
    localStorage.clear();
    this.router.navigate(['/']);
  }

  progressBarSteps() {
    this.progress = {
      progress_percent_value: 25, //progress percent
      total_steps: 4,
      current_step: 1,
      circle_container_width: 25, //circle container width in percent
      steps: [
        { num: 1, data: "Questionnaire 1", active: true },
        { num: 2, data: "Questionnaire 2" },
        { num: 3, data: "Questionnaire 3" },
        { num: 4, data: "Questionnaire 4" },
      ]
    }
  }

  //-------- Getting Countries List -------//  
  countriesLists() {
    // this.companyRegistration.getCountries().subscribe(
    //   data => {
    //     if (data.code == 200) {
    //       this.countryList = data.data;
    //     }
    //   });
    this.countryList = this.countryStateService.getCountries();
  }

  onSelectCountry() {
    this.disableState = false;
    this.question.state = '';
    this.stateList = this.countryStateService.getState(this.question.countryID);
  }

  getQuestionnaire1() {
    this.isProcessing = true;
    this.companyRegistration.getQuestionnaire1().subscribe(
      res => {
        this.isProcessing = false;
        if (res.code == 200) {
          this.question = {
            ABN: (res.data.ABN) ? res.data.ABN : '',
            city: (res.data.address && res.data.address.city) ? res.data.address.city : '',
            companyLogo: (res.data.companyLogo) ? res.data.companyLogo : '',
            companyWebsite: (res.data.companyWebsite) ? res.data.companyWebsite : '',
            countryID: (res.data.address && res.data.address.countryID) ? res.data.address.countryID : '',
            phoneNumber: (res.data.phoneNumber) ? res.data.phoneNumber : '',
            postalCode: (res.data.address && res.data.address.postalCode) ? res.data.address.postalCode : '',
            saveType: '',
            state: (res.data.address && res.data.address.state) ? res.data.address.state : '',
            streetAddress: (res.data.address && res.data.address.streetAddress) ? res.data.address.streetAddress : '',
            suburb: (res.data.address && res.data.address.suburb) ? res.data.address.suburb : '',
            companyAbout: (res.data.companyAbout) ? res.data.companyAbout : '',
            awardID: (res.data.awardID) ? res.data.awardID : '',
            awardName: (res.data.awardName) ? res.data.awardName : ''
          }
          if (this.question.countryID) {
            this.disableState = false;
            this.stateList = this.countryStateService.getState(this.question.countryID);
          }
        }
      },
      error => {
        this.isProcessing = false;
      }
    )
  }

  handleFileSelect($event) {
    const files = $event.target.files || $event.srcElement.files;
    const file: File = files[0];
    let ext = file.name.substring(file.name.lastIndexOf('.')).toLowerCase();
    let _validFileExtensions = [".jpg", ".jpeg", ".png"];
    if (_validFileExtensions.indexOf(ext) >= 0) {
      const formData = new FormData();
      formData.append('companyLogo', file);

      this.isProcessingImg = true;
      this.companyRegistration.uploadLogo(formData).subscribe(res => {
        this.isProcessingImg = false;
        this.question.companyLogo = res.data.companyLogo;
      });
    } else return this.coreService.notify("Unsuccessful", "Invalid file extension.", 0)
  }

  saveQuestionDraft() {
    this.question.saveType = "draft";
    this.save();
  }

  saveAndNext() {
    this.question.saveType = "save";
    this.save();
  }

  save() {
    if (!this.question.awardID || !this.question.awardName) return;
    this.isProcessingForm = true;
    console.log("Data", this.question);
    this.companyRegistration.setQuestionInDraft(this.question).subscribe(
      res => {
        this.isProcessingForm = false;
        if (res.code == 200) {
          this.coreService.notify("Successful", res.message, 1);
          if (this.question.saveType == 'save') {
            this.router.navigate(['/user/company-registration/questionnaire/step/2']);
          }
        }
        else return this.coreService.notify("Unsuccessful", res.message, 0);
      },
      error => {
        this.isProcessingForm = false;
        this.coreService.notify('Unsuccessful', error.message, 0);
      }
    )
  }

  onClick() {
    let dialogRef = this.dialog.open(CompanyAwardDialog, { width: '900px', height: '500px' });
    // dialogRef.componentInstance.awardList = this.awardList;
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        console.log('award ==> ', result)
        this.question.awardID = result.awardID;
        this.question.awardName = result.title;
      }
    });
  }
}


@Component({
  selector: 'company-award',
  template: `
  <md-progress-spinner *ngIf="isProcessing" mode="indeterminate" color=""></md-progress-spinner>
  <div *ngIf="!isProcessing" class="filter-wrapp box box-default contentBox">
    <div class="row">
        <div class="col-md-12">
            <ul class="awardsFilter">
                <li style="display:inline-block; margin:0px 7px 7px 0px;" *ngFor="let alpha of awards" (click)="toggleCollapse(alpha)">
                  <span class="badge badge-primary cursor" style="min-width:26px;">{{alpha}}</span>
                </li>
            </ul>
        </div>
    </div>
    <div class="AwardAlphabet-list">
      <div *ngFor="let alpha of awards" class="row">
        <div class="col-md-12" *ngIf="collapse[alpha]">
            <div class="box box-default full-width">
                <div class="box-header box-dark">{{alpha}}</div>
                <div class="box-body">
                    <ul>
                        <li *ngFor="let award of awardList[alpha]; let i=index;">
                            <div class="customchkbox">
                                <input [id]="'award'+award.awardID" type="radio" name="award" [value]="award.awardID" (change)="onCheckOption(i,alpha)"
                                    class="required">&nbsp;
                                <label [for]="'award'+award.awardID"></label>
                            </div>{{award.title}}
                        </li>
                    </ul>
                </div>
            </div>
        </div>
      </div>
    </div>
  </div>
  `,
  styles: [`
    .customchkbox{
      display:inline-block;
    }
    .customchkbox input + label {
        display: block;
    }
    
  `]
})
export class CompanyAwardDialog implements OnInit {
  public awards = [];
  public awardList = {};
  public isProcessing = false;
  public collapse = {};

  constructor(public dialogRef: MdDialogRef<CompanyAwardDialog>, public awardService: CompanyRegistration) { }

  ngOnInit() {
    this.getAwards();
    this.collapse['A'] = true;
  }

  toggleCollapse(alpha) {
    this.collapse = {};
    this.collapse[alpha] = true;
  }

  getAwards() {
    let params = { "filter": { "awards": {} } };
    this.isProcessing = true;
    this.awardService.getAwards(JSON.stringify(params)).subscribe(
      res => {
        this.isProcessing = false;
        if (res.code == 200) {
          this.awardList = res.data;

          let arr = Object.keys(this.awardList);
          this.awards = arr.filter(i => { if (this.awardList[i].length > 0) return i; })
        }
      })
  }

  onCheckOption(index, alpha) {
    this.dialogRef.close(this.awardList[alpha][index]);
  }
}
