import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QuestionnaireStepThreeComponent } from './questionnaire-step-three.component';

describe('QuestionnaireStepThreeComponent', () => {
  let component: QuestionnaireStepThreeComponent;
  let fixture: ComponentFixture<QuestionnaireStepThreeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QuestionnaireStepThreeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuestionnaireStepThreeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
