import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QuestionnaireStepFourComponent } from './questionnaire-step-four.component';

describe('QuestionnaireStepFourComponent', () => {
  let component: QuestionnaireStepFourComponent;
  let fixture: ComponentFixture<QuestionnaireStepFourComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QuestionnaireStepFourComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuestionnaireStepFourComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
