import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Cookie } from "ng2-cookies/ng2-cookies";
import { CompanyRegistration } from '../../company-registration.service';
import { CoreService } from '../../../../shared/services/core.service';
import { IDatePickerConfig } from 'ng2-date-picker';
import * as moment from "moment";

@Component({
  selector: 'app-questionnaire-step-four',
  templateUrl: './questionnaire-step-four.component.html',
  styleUrls: ['./questionnaire-step-four.component.scss']
})
export class QuestionnaireStepFourComponent implements OnInit {

  public progress;
  isProcessing = false;
  isProcessingForm = false;
  questionData = [];
  public options = {};
  public errors = {};
  dateTime = {};
  dateConfig: IDatePickerConfig = {};
  timeConfig: IDatePickerConfig = {};

  constructor(public router: Router, public companyRegistration: CompanyRegistration, public coreService: CoreService) { }

  ngOnInit() {

    this.progressBarSteps();
    this.getQuestions();
  }

  progressBarSteps() {
    this.progress = {
      progress_percent_value: 100, //progress percent
      total_steps: 4,
      current_step: 4,
      circle_container_width: 25, //circle container width in percent
      steps: [
        { num: 1, data: "Questionnaire 1" },
        { num: 2, data: "Questionnaire 2" },
        { num: 3, data: "Questionnaire 3" },
        { num: 4, data: "Questionnaire 4", active: true },
      ]
    }
  }

  getQuestions() {
    // if (this.questionData.length > 0) return;
    this.isProcessing = true;
    this.companyRegistration.getQuestions(3).subscribe(
      res => {
        this.isProcessing = false;
        this.questionData = res.data;
        this.validateOptions();
        console.log("Question Data", this.questionData);
      },
      error => {
        this.isProcessing = false;
        this.coreService.notify("Unsuccessful", error.message, 0)
      })
  }

  validateOptions() {
    for (let i = 0; i < this.questionData.length; i++) {
      this.questionData[i]['other'] = this.questionData[i]['other'] ? this.questionData[i]['other'] : null;
      if (this.questionData[i].field === "Option") {
        this.options[i] = this.questionData[i].answer;
      }
      if (this.questionData[i].field == "Time") this.dateTime[i] = !this.questionData[i].answer[0] ? moment() : moment(this.questionData[i].answer[0], 'hh:mm A');
      // if (this.profile.questionair[i].field == "Date") this.dateTime[i] = moment(this.profile.questionair[i].answer[0], 'DD MMM YYYY hh:mm A') || '';
    }
  }

  saveAsDraft() {
    for (let key in this.options) {
      this.questionData[key].answer = this.options[key];
    }
    this.saveAs('draft');
  }

  saveAndNext() {
    for (let key in this.options) {
      this.questionData[key].answer = this.options[key];
    }
    if (this.isValid()) {
      this.saveAs('save');
    }
  }

  saveAs(type) {
    this.isProcessingForm = true;
    this.companyRegistration.saveQuestion({ 'saveType': type, 'data': this.questionData })
      .subscribe(
      res => {
        if (res.code == 200) {
          this.isProcessingForm = false;
          if (type == 'save') {
            if (localStorage.getItem('happyhr-userInfo')) {
              let userInfo = JSON.parse(localStorage.getItem('happyhr-userInfo'));
              userInfo['incompletePage'] = -1;
              localStorage.setItem('happyhr-userInfo', JSON.stringify(userInfo));
            }
            this.router.navigate(['/app/company-admin']);
          }
          this.coreService.notify("Successful", res.message, 1);
        }
      },
      error => {
        this.isProcessingForm = false;
        this.coreService.notify('Unsuccessful', error.message, 0);
      })
  }

  onClickLogo(){
    Cookie.deleteAll();
    localStorage.clear();
    this.router.navigate(['/']);
  }

  onCheckOption(event, Qindex) {

    this.errors['answer' + Qindex] = '';
    if (!this.options.hasOwnProperty(Qindex)) this.options[Qindex] = [];
    if (event.target.checked) {
      if (this.options[Qindex].length == 10) {
        this.coreService.notify('', 'You can only choose maximum ten options', 0);
        event.target.checked = false;
        return false;
      }
      this.options[Qindex].push(event.target.value);
    } else {
      let ind = this.options[Qindex].indexOf(event.target.value);
      if (ind >= 0) {
        this.options[Qindex].splice(ind, 1);
        this.errors['answer' + Qindex] = this.options[Qindex].length == 0 ? 'Required' : '';
      }
    }
  }

  isValidQuestion(i) {
    let index;
    let qsn = this.questionData[i];
    switch (qsn.companyQuestionairID) {
      case 36:
        index = this.questionData.findIndex(i => i.companyQuestionairID == 35);
        return this.questionData[index].answer.length && this.questionData[index].answer[0] == 'Yes';
      case 37:
        index = this.questionData.findIndex(i => i.companyQuestionairID == 35);
        return this.questionData[index].answer.length && this.questionData[index].answer[0] == 'Yes';
      case 38:
        index = this.questionData.findIndex(i => i.companyQuestionairID == 35);
        return this.questionData[index].answer.length && this.questionData[index].answer[0] == 'Yes';
      case 40:
        index = this.questionData.findIndex(i => i.companyQuestionairID == 39);
        return this.questionData[index].answer.length && this.questionData[index].answer[0] == 'Yes';
      case 42:
        index = this.questionData.findIndex(i => i.companyQuestionairID == 41);
        return this.questionData[index].answer.length && this.questionData[index].answer[0] == 'Yes';
      case 44:
        index = this.questionData.findIndex(i => i.companyQuestionairID == 43);
        return this.questionData[index].answer.length && this.questionData[index].answer[0] == 'Yes';
      case 46:
        index = this.questionData.findIndex(i => i.companyQuestionairID == 45);
        return this.questionData[index].answer.length && this.questionData[index].answer[0] == 'Yes';
      case 48:
        index = this.questionData.findIndex(i => i.companyQuestionairID == 47);
        return this.questionData[index].answer.length && this.questionData[index].answer[0] == 'Yes';
      case 50:
        index = this.questionData.findIndex(i => i.companyQuestionairID == 49);
        return this.questionData[index].answer.length && this.questionData[index].answer[0] == 'Yes';
      case 52:
        index = this.questionData.findIndex(i => i.companyQuestionairID == 51);
        return this.questionData[index].answer.length && this.questionData[index].answer[0] == 'Yes';

      default: return true;
    }
  }

  validate(value, field) {
    console.log('value', value, field)
    this.errors[field] = '';
    this.errors[field] = (value == '' || value == undefined || value == null) ? 'Required' : '';
  }

  isValid() {
    let count = 0;
    for (let i = 0; i < this.questionData.length; i++) {
      if (this.isValidQuestion(i) && (this.questionData[i].answer.length == 0 || this.questionData[i].answer[0] == '')) {
        count++;
        this.validate(this.questionData[i].answer[0], 'answer' + i);
      }
    }
    return count == 0;
  }

  isChecked(option, index) {
    return this.questionData[index].answer.indexOf(option) >= 0;
  }

  setTime(Qindex) {
    let answer = this.dateTime[Qindex];
    if (this.questionData[Qindex].field == 'Date') {
      this.questionData[Qindex].answer[0] = moment(answer).format('YYYY-MM-DD HH:mm:ss');
    } else if (this.questionData[Qindex].field == 'Time') {
      this.questionData[Qindex].answer[0] = moment(answer).format('HH:mm:ss');
    }
  }

}
