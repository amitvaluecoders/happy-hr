import { Component, OnInit } from '@angular/core';
import { UserService } from '../../../shared/services';
import { Cookie } from 'ng2-cookies/ng2-cookies';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { CompanyRegistration } from '../company-registration.service';
import { CoreService } from '../../../shared/services';
import { Company } from '../company-registration';


@Component({
  selector: 'app-company-registration',
  templateUrl: './company-registration.component.html',
  styleUrls: ['./company-registration.component.scss']
})
export class CompanyRegistrationComponent implements OnInit {

  public progress;
  public securityQuestions;
  // public sub = false;
  public invalidUser;
  // public role;
  public user: Company;
  public errors = {};
  orgNodeID = '';
  isProcessing = false;

  constructor(
    private userservice: UserService,
    private router: Router,
    private route: ActivatedRoute,
    private companyRegistration: CompanyRegistration,
    private core: CoreService) {

    this.user = new Company;
  }

  ngOnInit() {
    if (this.route.snapshot.params['nid']) {
      this.orgNodeID = this.route.snapshot.params['nid'];
    }
    this.progressBarSteps();
    this.securityQuestionData();
  }

  progressBarSteps() {
    this.progress = {
      progress_percent_value: 33, //progress percent
      total_steps: 3,
      current_step: 1,
      circle_container_width: 33, //circle container width in percent
      steps: [
        { num: 1, data: "Sign up", active: true },
        { num: 2, data: "Pick a plan" },
        { num: 3, data: "Confirmation" }
      ]
    }
  }

  securityQuestionData() {
    this.userservice.getSecurityQuestions().subscribe(
      data => {
        console.log(data)
        if (data.code == 200) {
          this.securityQuestions = data.data;
        }
      }
    );
  }


  submit() {
    // if (v) {
    // this.sub = true;
    this.user['orgNodeID'] = this.orgNodeID;
    this.isProcessing = true;
    this.companyRegistration.companyRegistration(this.user).subscribe(
      data => {
        this.isProcessing = false;
        if (data.code == "200") {

          if (data.data.userRole == '') this.invalidUser = true;

          else {
            //set token
            Cookie.set('happyhr-token', data.token);
            let role = data.data.userRole;
            if (this.core.getRoles().indexOf(role) >= 0) {
              this.core.setRole(role);
              // Cookie.set('happyhr-role', role);
              Cookie.set('happyhr-userInfo', JSON.stringify(data.data));
              localStorage.setItem('happyhr-userInfo', JSON.stringify(data.data));
              Cookie.set('happyhr-permission', JSON.stringify(data.data.modules));
              localStorage.setItem('happyhr-permission', JSON.stringify(data.data.modules));
              this.core.setPermission(data.data.modules);
              this.router.navigate(['/user/company-registration/payment-options']);
            } else {
              this.invalidUser = true;
            }
          }
        }
      },
      error => {
        this.isProcessing = false;
        this.core.notify('Unsuccessful', error.message, 0);
        if (error.code == 400) {
          this.handleError(error.errors);
        }
      },
    );
    // }
  }


  //     let url = '';
  //     if (data.data.userRole == "superAdmin") {
  //       this.role = "superAdmin";
  //       this.core.setRole(this.role);
  //       //this.core.setPermission(data.response.data.permission);
  //       url = '/app/super-admin';
  //     }
  //     else if (data.data.userRole == "companyAdmin") {
  //       this.role = "companyAdmin";
  //       this.core.setRole(this.role);
  //       //this.core.setPermission(data.response.data.permission);
  //       url = '/app/company-admin';
  //     }
  //     else if (data.data.userRole == "companyManager") {
  //       this.role = "companyManager";
  //       this.core.setRole(this.role);
  //       //this.core.setPermission(data.response.data.permission);
  //       url = '/app/manager-admin';
  //     }
  //     else if (data.data.userRole == "companyEmployee") {
  //       this.role = "companyEmployee";
  //       this.core.setRole(this.role);
  //       //this.core.setPermission(data.response.data.permission);
  //       url = '/app/employee-admin';
  //     }

  //     // console.log("url",url)
  //     Cookie.set('happyhr-role', this.role);
  //     // this.router.navigate([url]);
  //   }
  // }
  // this.router.navigate(['/user/company-registration/payment-options']);
  //     },
  //     error => {
  //       // console.log('Got error')
  //       console.log(error)
  //       this.sub = false;
  //       this.core.notify('Error', error.message, 0);
  //     },
  //   );
  // }
  // }

  handleError(err) {
    Object.keys(err).forEach(key => {
      this.errors[key] = err[key][0];
    })
  }

  resetErrors(field) {
    this.errors[field] = '';
  }

}
