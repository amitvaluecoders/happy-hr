export class Questionnaire1 {
    companyWebsite: string = "";
    ABN: string = "";
    phoneNumber: string = "";
    streetAddress: string = "";
    suburb: string = "";
    city: string = "";
    state: string = "";
    postalCode: string = "";
    countryID: string = "";
    companyLogo: string = "";
    saveType: string = "";
    companyAbout: String = "";
    awardID: string = "";
    awardName: string = "";
}

export class Company {
    firstName: string = "";
    lastName: string = "";
    companyName: string = "";
    email: string = "";
    password: string = "";
    confirmPassword: string = "";
    securityQuestionID: string = "";
    securityAnswer: string = "";
}