import { Injectable } from '@angular/core';
import { RestfulLaravelService } from '../../shared/services';
import { Http, Headers, Response, RequestOptions } from '@angular/http';


@Injectable()
export class CompanyRegistration {

  constructor(
    private restfulWebService: RestfulLaravelService) { }


  companyRegistration(details) {
    return this.restfulWebService.post(`company-registration`, details);
  }

  getCountries() {
    return this.restfulWebService.get('country-list');
  }

  setQuestionInDraft(saveDraft) {
    return this.restfulWebService.post(`questionair/step1`, saveDraft);
  }

  getQuestionnaire1() {
    return this.restfulWebService.get(`get-questionair/step1`);
  }

  getQuestions(step) {
    return this.restfulWebService.get(`get-questionair/step/${step}`);
  }

  saveQuestion(reqBody) {
    return this.restfulWebService.post(`save-questionair`, reqBody);
  }

  uploadLogo(formData) {
    return this.restfulWebService.post('file-upload', formData);
  }

  submitPaymentDetails(formData) {
    return this.restfulWebService.post('make-payment', formData);
  }

  checkCouponCode(code) {
    return this.restfulWebService.get(`validate-coupon/${code}`);
  }

  getAwards(params) {
    return this.restfulWebService.get(`admin/award-list/${params}`);
  }
}