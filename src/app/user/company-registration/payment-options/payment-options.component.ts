import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { CompanyRegistration } from '../company-registration.service';
import { CoreService } from '../../../shared/services';
import { IDatePickerConfig } from 'ng2-date-picker';
import { Cookie } from "ng2-cookies/ng2-cookies";
import * as moment from 'moment';
import { CountryStateService } from '../../../shared/services/country-state.service';
import { MdDialogRef, MdDialog, MdSnackBar } from '@angular/material';
@Component({
  selector: 'app-payment-options',
  templateUrl: './payment-options.component.html',
  styleUrls: ['./payment-options.component.scss']
})
export class PaymentOptionsComponent implements OnInit {
  public progress;
  public notvalid = false;
  public tabbing = true;
  public iAgree = false;
  public isPlanSelected = false;
  public selectedPlan: any;
  public isCouponCode = false;
  public isCouponCodeValid = false;
  public countries = [];
  public states = [];
  public isChkProcessing=false;
  public epirDate: any;
  public plans = [];
  years = []; months = [];
  public isActionProcessing = false;
  public reqbody = {
    subscriptionPlanID: '',
    creditCardInfo: {
      full_number: '',
      expiration_month: '',
      expiration_year: '',
      billing_address: '',
      billing_city: '',
      billing_state: '',
      billing_country: '',
      billing_zip: '',
    },
    coupon: ""
  }
  public model = {
    cardName: '',
    cardNumber: '',
    zipCode: '',
    epirDate: '',
    sCode: '',
    city: '',
    address: '',
    state: '',
    country: ''
  }
  config: IDatePickerConfig = {};
  disableState = true;

  constructor(
    public countryService: CountryStateService,
    public router: Router, public activatedRoute: ActivatedRoute,
    public sevice: CompanyRegistration,
    public core: CoreService,
    public dialog: MdDialog
  ) {
    this.config.locale = "en";
    this.config.format = "YYYY-MM";
    this.config.showMultipleYearsNavigation = true;
    this.config.weekDayFormat = 'dd';
    this.config.disableKeypress = true;
    this.config.monthFormat = "MMM YYYY";
    this.config.min = moment().add(+1, 'days');
  }

  getExpiryDates() {
    for (let i = 0; i <= 20; i++) {
      this.years.push(moment().year() + i);
    }
    this.months = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'];
    // this.countries= []
  }

  submit(isValid) {
    if (isValid) {
      this.isActionProcessing = true;
      this.reqbody['subscriptionPlanID'] = this.selectedPlan['id'];
      // this.reqbody.creditCardInfo['expiration_month']=moment(this.epirDate).month().toString();
      // this.reqbody.creditCardInfo['expiration_year']=moment(this.epirDate).year().toString();
      this.reqbody['companyID'] = this.activatedRoute.snapshot.params['id'];
      this.sevice.submitPaymentDetails(this.reqbody).subscribe(
        d => {
          if (d.code == "200") {
            this.core.notify("Successful", d.message, 1);
            if(this.reqbody['companyID'])this.router.navigate(['/app/company-admin/organisation-chart']);
            else this.router.navigate(['/user/company-registration/welcome-screen']);
          }
          else {
            if (d.completeError[0]) this.core.notify("Unsuccessful", d.completeError[0], 0);
            if (d.completeError[1]) this.core.notify("Unsuccessful", d.completeError[1], 0);
            if (d.completeError[2]) this.core.notify("Unsuccessful", d.completeError[2], 0);
            if (d.completeError[3]) this.core.notify("Unsuccessful", d.completeError[3], 0);
            if (d.completeError[4]) this.core.notify("Unsuccessful", d.completeError[4], 0);
            this.isActionProcessing = false;
          }
        },
        error => {
          this.isActionProcessing = false;
          if (error.completeError[0]) this.core.notify("Unsuccessful", error.completeError[0], 0);
          if (error.completeError[1]) this.core.notify("Unsuccessful", error.completeError[1], 0);
          if (error.completeError[2]) this.core.notify("Unsuccessful", error.completeError[2], 0);
          if (error.completeError[3]) this.core.notify("Unsuccessful", error.completeError[3], 0);
          if (error.completeError[4]) this.core.notify("Unsuccessful", error.completeError[4], 0);
        })
    } else {
      this.notvalid = !this.notvalid;
    }
  }

  // public plan1={
  //   id:1,
  //   name:"p1",
  //   active:false,
  //   title:"Small business",
  //   employeeCount:"1-24",
  //   price:25,
  //   features:[
  //     { class:"fa fa-check-circle"},
  //     { class:"fa fa-check-circle"},
  //     { class:"fa fa-check-circle"},
  //     { class:"fa fa-check-circle"},
  //     { class:"fa fa fa-times-circle"},
  //     { class:"fa fa fa-times-circle"},
  //     { class:"fa fa fa-times-circle"},
  //   ]
  // }
  public plan1 = {
    id: 1,
    title: 'Plan 1',
    active: false,
    pricePerEmployee: 7.50 * 10,
    priceAcountSetup: null,
    duration: '12'
  }
  public plan2 = {
    id: 2,
    title: 'Plan 2',
    active: false,
    pricePerEmployee: 7.50 * 10,
    priceAcountSetup: 599.00,
    duration: '12'
  }
  public plan3 = {
    id: 3,
    title: 'Plan 3',
    active: false,
    pricePerEmployee: 5.00 * 10,
    priceAcountSetup: null,
    duration: '24'
  }
  public plan4 = {
    id: 4,
    title: 'Plan 4',
    active: false,
    pricePerEmployee: 5.00 * 10,
    priceAcountSetup: 599.00,
    duration: '24'
  }


  ngOnInit() {
    this.countries = this.countryService.getCountries();
    this.getExpiryDates();
    this.plans.push(this.plan1, this.plan2, this.plan3, this.plan4)
    this.progress = {
      progress_percent_value: 67, //progress percent
      sevicetotal_steps: 3,
      current_step: 2,
      circle_container_width: 33, //circle container width in percent
      steps: [
        { num: 1, data: "Sign up" },
        { num: 2, data: "Pick a plan", active: true },
        { num: 3, data: "Confirmation" }
      ]
    }
  }

  onClickLogo(){
    Cookie.deleteAll();
    localStorage.clear();
    this.router.navigate(['/']);
  }


  onCountrySelect() {
    console.log('on country ===>>>>>', this.reqbody.creditCardInfo.billing_country)
    this.disableState = false;
    this.states = this.countryService.getState(this.reqbody.creditCardInfo.billing_country);
    this.reqbody.creditCardInfo['billing_state'] = '';
  }

  onApplyCouponChange() {

  }

  onApplyCouponCode() {
    if(this.reqbody.coupon){
      this.isCouponCodeValid = false;
          this.isChkProcessing = true;
          this.sevice.checkCouponCode(this.reqbody.coupon).subscribe(
            d => {
              if (d.code == "200") {
                this.core.notify("Successful", d.message, 1);
                this.isCouponCodeValid = true;
                this.isChkProcessing = false;    
              }
              else {
                this.core.notify("Unsuccessful", d.message, 0);
                this.isChkProcessing = false;
              }
            },
            error => {
              this.isChkProcessing = false;
              this.core.notify("Unsuccessful", error.message, 0);              
            })
        }
  }

  onPlanSelect(id) {
    this.plans = this.plans.filter(d => {
      d.active = false;
      if (d.id == id) {
        d.active = true;
        this.selectedPlan = d;
        let accountSetupCahnges = 0;
        if (d.priceAcountSetup) {
          accountSetupCahnges = (d.priceAcountSetup + (d.priceAcountSetup * 0.1));
          this.selectedPlan['withoutTax'] = (d.pricePerEmployee + d.priceAcountSetup).toFixed(2);
          this.selectedPlan['totalTax'] = ( (d.priceAcountSetup * 0.1) + (d.pricePerEmployee * 0.1) ).toFixed(2);
        }
        else {
          this.selectedPlan['withoutTax'] = ( d.pricePerEmployee ).toFixed(2);;
          this.selectedPlan['totalTax'] = ( d.pricePerEmployee * 0.1).toFixed(2); ;
        }

        let tempTotal = ((d.pricePerEmployee + (d.pricePerEmployee * 0.1)) + accountSetupCahnges).toFixed(2);
       let  temp = ((d.pricePerEmployee + (d.pricePerEmployee * 0.1)) ).toFixed(2);
       this.selectedPlan['totalWithoutSetup'] = (Number(temp) + (Number(temp) * (1.5/100)) ).toFixed(2);
        this.selectedPlan['total'] = tempTotal;
        this.selectedPlan['total'] = (Number(tempTotal) + (Number(tempTotal) * (1.5/100)) ).toFixed(2);
       
    }
      return d;
    })
    this.isPlanSelected = true;
  }

  progressBarSteps() {
    this.progress = {
      progress_percent_value: 67, //progress percent
      total_steps: 3,
      current_step: 2,
      circle_container_width: 33, //circle container width in percent
      steps: [
        { num: 1, data: "Sign up" },
        { num: 2, data: "Pick a plan", active: true },
        { num: 3, data: "Confirmation" }
      ]
    }
  }

  tagglePay(tabb) {
    this.tabbing = !this.tabbing;
  }
  onClick() {
    let dialogRef = this.dialog.open(ViewDetailPopupComponent);
    // dialogRef.componentInstance.moduleName='developmentPlan';
    dialogRef.afterClosed().subscribe(result => {
      if (result) {

      }
    });
  }
}

@Component({
  selector: 'Development-reschedule-popup',
  template: `<div class="reschedule-popup">           
            <div class="reschedule-popupInner">
              <h4><strong>About the account set up and HR consultation process</strong></h4>
              <p>One of our degree qualified HR consultants will be working with you successfully induct you into Happy HR!</p>
              <div class="steps_abt">
                <h4><strong>Step 1.</strong> Introduction call</h4>
                <p>We will call you to discuss your business goals, what positions you have in the company, what these people do and how you want to measure them. We will also have a general discussion about HR at your company. We will also book in times for the next 2 stages of induction.</p>
                <h4><strong>Step 2.</strong> Pre-induction</h4>
                <p>Our meetings are scheduled via Go-to-meeting, which is a screen sharing software. We will take you through a pre-induction where we will work on finalising your company position descriptions, company profile, policies and other features of the system. All of your position descriptions created will be aligned to your operational, financial and strategic goals!</p>
                <h4><strong>Step 3.</strong> Induction</h4>
                <p>Induction is making any final changes required by yourself and once this is complete we will invite you and your staff into Happy HR.</p>
                <p>At this time we will also integrate Xero, MYOB, QuickBooks, Key pay, Tanda or Deputy if you are already a subscriber to any of these software’s.</p>
                <h4><strong>Step 4.</strong> Training</h4>
                <p>We offer training for you and your management team to ensure they get the most out of the software. After this time our team of qualified HR professionals we will be here to assist you!</p>
                <p>We do advise purchasing the account set up and HR consultation when you become a Happy HR subscriber.</p>
                <p>Any questions? Call <strong><a href="tel:1300 730 880"> 1300 730 880 </a></strong> or email <strong><a href="mailto:happy@happyhr.com"> happy@happyhr.com </a></strong></p>
             </div>
            </div>           
            </div>
            `,
  styles: [`.reschedule-popup{max-width:600px; width:100%;}
            img.d-flex.align-self-center.mr-3{
              max-width: 50px; 
              margin-top: -17px;
              max-height: 40px;
            }
            .reschedule-popupInner h4{
              font-size: 18px;
              padding: 0;
              margin: 10px 0;
              border: 0;
            }
            .reschedule-popupInner p{
                font-size:14px;
                margin: 0 0 10px;
                line-height: 20px;
            }
           .reschedule-popupInner a {
              color: #337ab7;
            }
          `]

})
export class ViewDetailPopupComponent implements OnInit {

  constructor(public dialogRef: MdDialogRef<ViewDetailPopupComponent>,
    public coreService: CoreService) { }

  ngOnInit() {
  }

}
