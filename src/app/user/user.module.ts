import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from '@angular/material';
import { FormsModule } from '@angular/forms';
import { AdminLoginComponent } from './login/login.component';
import { ExtraPagesRoutingModule } from './user-routing.module';
// import { RestfulwebService } from '../shared/services/restfulweb.service';
import { UserService } from '../shared/services/user.service';
import { SignupComponent } from './signup/signup.component';
import { ForgetPasswordComponent } from './forget-password/forget-password.component';
import { ConfirmEmailComponent } from './confirm-email/confirm-email.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { Ng2DeviceDetector } from 'ng2-device-detector';
import { RestfulLaravelService } from '../shared/services';
import { SharedModule } from '../shared/shared.module';
import { AngularMultiSelectModule } from 'angular2-multiselect-checkbox-dropdown/angular2-multiselect-dropdown';
import { PasswordAddComponent } from './password-add/password-add.component';
import { UserOfferAcceptComponent } from './user-offer-accept/user-offer-accept.component';


@NgModule({
  imports: [
  //  Ng2DeviceDetector,
    FormsModule,
    MaterialModule,
    ExtraPagesRoutingModule,
    CommonModule,
    AngularMultiSelectModule
  ],
  declarations: [AdminLoginComponent, SignupComponent, ForgetPasswordComponent, ConfirmEmailComponent, ResetPasswordComponent, PasswordAddComponent, UserOfferAcceptComponent],
  providers:[UserService,RestfulLaravelService]
})
export class UserModule { }
