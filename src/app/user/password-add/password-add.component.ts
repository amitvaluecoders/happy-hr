import { Component, OnInit } from '@angular/core';
import { UserService } from '../../shared/services';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { CoreService } from '../../shared/services';

@Component({
  selector: 'app-password-add',
  templateUrl: './password-add.component.html',
  styleUrls: ['./password-add.component.scss']
})
export class PasswordAddComponent implements OnInit {
  public sub = {
    "securityAnswer":"",
    "securityQuestionID":"",
    "password":"",
    "token":""
}
    public progress;
    public securityQuestions;
    public invalidUser;
    public errors = {};

    public confirmPasswordVal = "";

  constructor( private userservice: UserService,
    private router: Router,
    private route: ActivatedRoute,
    private core: CoreService) { }

  ngOnInit() {
    if (this.route.snapshot.params['token']) {
      this.sub.token = this.route.snapshot.params['token'];
    } else {
      //this.router.navigate(['/user/login']);
    }
    this.securityQuestionData();
  }

  securityQuestionData() {
    this.userservice.getSecurityQuestions().subscribe(
      data => {
        console.log(data)
        if (data.code == 200) {
          this.securityQuestions = data.data;
        }
      }
    );
  }

  submit() {
    this.userservice.subCompanyRegistration(this.sub).subscribe(
      data => {
        console.log(data);
        if (data.code == "200") {
          this.core.notify("Successful", data.message, 1);
          this.router.navigate(['/user/login']);
        }
        else return this.core.notify("Unsuccessful", data.message, 0);
      },
      error => {
        this.core.notify('Error', error.message, 0);
        console.log(error);
        if (error.code == 400) {
          return this.core.notify("Unsuccessful", error.message, 0);
        }
    
      },
    );
    // }
  }

  resetErrors(field) {
    this.errors[field] = '';
  }


}
