import { Component, OnInit } from '@angular/core';
  import { UserService , CoreService} from '../../shared/services';
  import { Router, ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.scss']
})
export class ResetPasswordComponent implements OnInit {
// public passwordMatch;
  public resetPasswordSuccess=false;
  public user={
    password:"",
    confirmPassword:""
  }
  public token;

  constructor(
    public core:CoreService,
    private userservice: UserService,
    private router:Router,
    private activatedRoute:ActivatedRoute
    ) { }

  ngOnInit() {
    // this.passwordMatch=true;
    if (typeof this.activatedRoute.snapshot.params['token'] != 'undefined') {
     this.checkResetToken();
   } 
  }
//check token laravel
  checkResetToken(){
    this.token=this.activatedRoute.snapshot.params['token'];
      console.log(this.token)
      var token={token:this.token};
       this.userservice
        .resetTokenCheck({"passwordResetToken":this.token})
        .subscribe(
            data => {
              console.log(data);
              // this.showPass=true;
               },
            error =>{
              console.log(error);
              //  this.core.notify(this.core.ERROR_TITLE,error.slice(17),0),
              //  this.router.navigate(['/login']) 
            }
        );
  }

  submit(isvalid){
    // console.log(isvalid);
    if(isvalid) {
      // this.userservice.resetPassword(this.user).subscribe(
      //   data => {
      //       if(data.status){
      //         console.log(data.message);
      //         this.resetPasswordSuccess=true;
      //         //this.router.navigate([`/`]);
      //       }
      //   }
      // );

      //laravel
      var tok={
        passwordResetToken: this.token,
        newPassword: this.user.password,
        confirmPassword:this.user.confirmPassword 
      };
      // console.log(tok)
      this.userservice.resetAccPassword(tok).subscribe(
          data => {
              console.log(data);
              if(data.code ==200){
                this.core.notify("Successful", data.message, 1);
                this.router.navigate([`/`]);
              }
              else this.core.notify("Unsuccessful", data.message, 0);
          },
          error =>{
            this.core.notify("Unsuccessful", error.message, 0);
          }
        );
      
    }
  }

}
