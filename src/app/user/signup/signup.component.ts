import { Component, OnInit } from '@angular/core';
import { UserService } from '../../shared/services/user.service';
import { Cookie } from 'ng2-cookies/ng2-cookies';
import { Router, ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {

  public user={
    username:"",
    email:"",
    password:""
  }
  constructor(private userservice: UserService,private router:Router) { }

  ngOnInit() {
  }

  submit(isvalid){
    console.log(isvalid);
    
     if(isvalid){
    console.log(this.user);
    this.userservice.signup(this.user).subscribe(
      data => {
          if(data.status){
            console.log(data)
            Cookie.set('happyhr-token',"1234567890");
            this.router.navigate(['/app/dashboard']);
          }
      }
    );
    } 
    
  }
}
