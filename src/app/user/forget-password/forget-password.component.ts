import { Component, OnInit } from '@angular/core';
import { UserService, CoreService } from '../../shared/services';
import { Router, ActivatedRoute, Params } from '@angular/router';


@Component({
  selector: 'app-forget-password',
  templateUrl: './forget-password.component.html',
  styleUrls: ['./forget-password.component.scss']
})
export class ForgetPasswordComponent implements OnInit {
  email: boolean;
  securityQuestion: boolean;
  securityAnswer: boolean = false;
  taxFileNumber: boolean;
  public userTypeArr = [];
  public user = {
    email: "",
    securityQuestionID: "",
    answer: "",
    taxFileNumber: '',
    type:""
  }
  // selectUserType;

  public submitForm = true;

  public securityQuestions;

  constructor(
    private userservice: UserService,
    private router: Router,
    private core: CoreService
  ) { }

  ngOnInit() {
    this.email = true;
    //laravel get secuirty questions
    this.userTypeArr = [{ id: 0, type: 'superAdmin', title: "Super admin" }, { id: 1, type: 'companyAdmin', title: "Company admin" }, { id: 2, type: 'companyEmployee', title: "Employee" }]
    this.userservice.getSecurityQuestions().subscribe(
      data => {
        console.log(data)
        if (data.code == 200) {
          this.securityQuestions = data.data;
        }
      }
    );
  }

  //on submit
  submit(isvalid) {
    console.log(this.user)
    if (isvalid) {
      this.submitForm = false;
      this.userservice.forgetPassword(this.user).subscribe(
        data => {
          // console.log(data.response);
          //   if(data.response.code=="200"){
          //     this.router.navigate([`/user/confirm-email/${this.user.email}`]);
          //   }

          //laravel

          if (data.code == 200) {
            // console.log(data)
            this.core.notify('Success', data.message, 1);
            this.router.navigate([`/user/confirm-email/${this.user.email}`]);
          }
          else {
            this.submitForm = true;
            this.core.notify('Error', "Something went wrong.", 0);
            // console.log(data)
          }

        },
        error => {
          // console.log(error)
          this.core.notify('Error', error.message, 0);
          this.submitForm = true;

        }

      );
    }
  }
  userSelect(id) {
    if (id == 0) {
      this.email = true;
      this.securityQuestion = false;
      this.securityAnswer = false;
      this.taxFileNumber = false;
    } else if (id == 1) {
      this.email = true;
      this.securityQuestion = true;
      this.securityAnswer = true;
      this.taxFileNumber = false;
    }
    else {
      this.email = true;
      this.securityQuestion = false;
      this.securityAnswer = false;
      this.taxFileNumber = true;
    }
  }

}
