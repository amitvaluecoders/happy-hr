import { Injectable } from '@angular/core';
import { Subject }    from 'rxjs/Subject';
import { RestfulLaravelService} from '../shared/services/restful-laravel.service';

@Injectable()
    export class LayoutService {
    private preloaderStateSource = new Subject<string>();

    preloaderState$ = this.preloaderStateSource.asObservable();

    constructor(public restfulWebService:RestfulLaravelService) { }
  
    updatePreloaderState(state: string) {
        // console.log('preloader state: ' + state)
        this.preloaderStateSource.next(state);
    }


    private searchOverlaySource = new Subject<string>();

    searchOverlayState$ = this.searchOverlaySource.asObservable();

    updateSearchOverlayState(state: string) {
        // console.log('overlay state: ' + state)
        this.searchOverlaySource.next(state);
    }

    countryList(){
        return this.restfulWebService.get('admin/operating-country-list');  
    }

    selectCountry(reqBody){
    return this.restfulWebService.put('admin/select-operating-country',reqBody);          
    }

    getCountry(){
        return this.restfulWebService.get('admin/get-selected-country');  
    }

    getAllNotification(params) {
        return this.restfulWebService.get(`notification-list/${encodeURIComponent(params)}`);
    }

    
    sendSeenStatus(reqBody) {
        return this.restfulWebService.post(`notification-update-seen-list` , reqBody);
    }

    sendFeedback(reqBody) {
        return this.restfulWebService.post(`save-feedback` , reqBody);
    }

    getAllNotificationStatus(){
        return this.restfulWebService.get(`get-ca-profile-completeness-detail`);
    }
    getAllNotificationStatusEmployee(){
        return this.restfulWebService.get(`employee/get-employee-profile-completeness-detail`);
        
    }
    
}