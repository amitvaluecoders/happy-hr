import { Component, OnInit,Input } from '@angular/core';
import { Cookie } from 'ng2-cookies/ng2-cookies';
import { CoreService } from '../shared/services/core.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { SocketServiceService } from '../shared/services/socket-service.service';

@Component({
    selector: 'my-app-layout',
    templateUrl: './layout.component.html',
})
export class LayoutComponent {
    
    public logData;
    public role;
    public navBar;
    public permission;
    public modulePermission={}; 

    constructor(public coreService: CoreService,private router:Router,
    private route:ActivatedRoute,public socketServiceService:SocketServiceService) {
        this.socketServiceService.connectSocket();
        this.coreService.initializeAppConstractor();
        this.logData = this.coreService.getRole();


    }
}
