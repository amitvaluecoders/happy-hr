import { Component } from "@angular/core";
import { APPCONFIG } from "../../config";
import { Router, ActivatedRoute, Params } from "@angular/router";
import { Cookie } from "ng2-cookies/ng2-cookies";
import { CoreService } from "../../shared/services/core.service";
import { MessagesService } from "../../shared/services/messages.service";
import { LayoutService } from "../layout.service";
import { SocketServiceService } from "../../shared/services/socket-service.service";
import { SideNavService } from "../sidenav/sidenav-menu/side-nav.service";
import {ContractEmployeeService} from "../../company-employee/employee-contracts/employee-contracts.service";
import * as moment from "moment";
import { MdSnackBar, MdDialog, MdDialogRef } from "@angular/material";

@Component({
  selector: "my-app-header",
  styles: [],
  templateUrl: "./header.component.html"
})
export class AppHeaderComponent {
  public toogleHeaderMenu: boolean;
  AppConfig: any;
  public role = "";
  loginUserInfo = {};
  isSubCompany = false;
  name = "";
  // imageUrl = '';
  userUnreadMessagesCount = 0;
  countryList = [];
  selectedCountry = "";
  activityStatusProcessing = false;
  activityStatus: any;
  oldUserRole:any;
  country = "";
  public socketIO: any;
  localeRole: any;
  public notiList = [];
  public params = { offset: 0, limit: 20 };
  public unSeenNoti = [];
  public messagePermission = true;
  constructor(
    private router: Router,
    public coreService: CoreService,
    private messageService: MessagesService,
    private layoutService: LayoutService,
    public socketServiceService: SocketServiceService,
    public sideNavService: SideNavService,
    public contractEmployeeService: ContractEmployeeService,
    public dialog: MdDialog
  ) {}

  ngOnInit() {
    this.socketIO = this.socketServiceService.socket;
    console.log("s", this.socketIO);
    this.AppConfig = APPCONFIG;
    this.AppConfig.canSwitch = this.coreService.getCanSwitch();
    this.role = this.coreService.getRole();
    APPCONFIG.role = this.role;
    this.getMessageUnreadCount();
    this.getCountryList();
    this.getCountry();
    this.getNotiList();
    APPCONFIG.subRole = this.coreService.subRole;
    this.oldUserRole = localStorage.getItem("oldUserRole");
    
    JSON.parse(localStorage.getItem("happyhr-permission")).filter(i => {
      if (i.module == "message") {
        if (!i.permission.view) this.messagePermission = false;
      }
    });
    if (this.role == "companyAdmin") this.getActivitiesWithStatus();
    if (this.role == "companyEmployee") this.getActivitiesWithStatusEmp();
    this.loginUserInfo = JSON.parse(localStorage.getItem("happyhr-userInfo"));
    this.isSubCompany = this.loginUserInfo && this.loginUserInfo["isSubCompany"] ? true : false;
    this.socketIO.on("recive noti", data => {
      console.log(data);
      this.notiList.splice(0, 0, data);
      this.unSeenNoti.push(data.logActivityID);
    });

    this.name = JSON.parse(localStorage.getItem("happyhr-userInfo")).fullName;
    this.AppConfig.imageUrl = JSON.parse(
      localStorage.getItem("happyhr-userInfo")
    ).imageUrl;
    if (
      Math.max(document.documentElement.clientWidth, window.innerWidth || 0) <
      676
    )
      this.toogleHeaderMenu = false;
    else this.toogleHeaderMenu = true;
    console.log("apppppppp ->",APPCONFIG)
  }

  checkName() {
    if (APPCONFIG.subRole == "genericCeo") return "CEO";
    if (APPCONFIG.subRole == "genericFinance") return "Finance";
    if (APPCONFIG.subRole == "genericHrmanager") return "HR Manager";
    if (APPCONFIG.subRole == "genericOperation") return "Operation";
    if (APPCONFIG.subRole =="genericManager") return "Manager";
  }

  seeMoreRoute() {
    return this.router.navigate([
      `/app/${this.coreService.getSubPath()}/setting/notifications`
    ]);
  }

  getActivitiesWithStatusEmp() {
    this.activityStatusProcessing = true;
    this.layoutService.getAllNotificationStatusEmployee().subscribe(
      res => {
        if (res.code == 200) {
          this.activityStatus = res.data;
          this.activityStatusProcessing = false;
        } else return this.coreService.notify("Unsuccessful", res.message, 0);
      },
      err => {
        if (err.code == 400) {
          return this.coreService.notify("Unsuccessful", err.message, 0);
        }
        this.coreService.notify("Unsuccessful", "Internal server error.", 0);
      }
    );
  }

  getActivitiesWithStatus() {
    this.activityStatusProcessing = true;
    this.layoutService.getAllNotificationStatus().subscribe(
      res => {
        if (res.code == 200) {
          this.activityStatus = res.data;
          this.activityStatusProcessing = false;
        } else return this.coreService.notify("Unsuccessful", res.message, 0);
      },
      err => {
        if (err.code == 400) {
          return this.coreService.notify("Unsuccessful", err.message, 0);
        }
        this.coreService.notify("Unsuccessful", "Internal server error.", 0);
      }
    );
  }

  changeUnseenStatus() {
    if (this.unSeenNoti.length) {
      this.layoutService.sendSeenStatus({notificationIDs: this.unSeenNoti})
        .subscribe(res => {
          this.unSeenNoti = [];
        });
    }
  }

  onChangePassword(){
    this.router.navigate(['/app/employee/change-password']);
  }

  onPendingActivitesAction(moduleName) {
    let url = "";
    switch (moduleName) {
      case "CONTRACTS": {
        url = "/app/company-admin/contract";
        break;
      }
      case "POLICIES": {
        url = "/app/company-admin/policy-documents";
        break;
      }
      case "POSITIONS": {
        url = "/app/company-admin/position-description/add";
        break;
      }
      case "ORGCHART": {
        url = "/app/company-admin/organisation-chart";
        break;
      }
      default:
        url = "/";
    }
    this.router.navigate([url]);
  }

  onPendingActivitesActionEmp(moduleName) {
    let url = "";
    switch (moduleName) {
      case "CONTRACTS": {
        url = "/app/employee/contracts";
        break;
      }
      case "POSITIONS": {
        url = "/app/employee/position-description";
        break;
      }
      default:
        url = "/";
    }
    this.router.navigate([url]);
  }

  getNotiList() {
    this.layoutService.getAllNotification(JSON.stringify(this.params))
      .subscribe(
        res => {
          if (res.code == 200) {
            this.params.offset = this.params.offset + this.params.limit;
            Array.prototype.push.apply(this.notiList, res.data);
            this.unSeenNoti = this.notiList.filter(v => v.status == "Unviewed").map(v => v.logActivityID);
          } else return this.coreService.notify("Unsuccessful", res.message, 0);
        },
        err => {
          if (err.code == 400) {
            return this.coreService.notify("Unsuccessful", err.message, 0);
          }
          this.coreService.notify("Unsuccessful","Error while getting data.",0);
        });
  }

  formateDate(v) {
    return moment(v).calendar();
  }

  getMessageUnreadCount() {
    this.userUnreadMessagesCount = 0;
    this.messageService.getUserUnreadMessagesCount().subscribe(
      data => {
        if (data.code == 200) {
          if (data.data.unreadCount)
            this.userUnreadMessagesCount = data.data.unreadCount;
        }
      },
      error => {
        this.coreService.notify("Unsuccessful", error.message, 0);
      },
      () => {}
    );
  }

  onMessageIconClick() {
    console.log("role ",this.coreService.role);
    if (this.coreService.role == "companyAdmin")
      this.router.navigate(["/app/company-admin/message"]);
    if (this.coreService.role == "superAdmin")
      this.router.navigate(["/app/super-admin/message"]);
    if (this.coreService.role == "companyEmployee" )
      this.router.navigate(["/app/employee/message"]);
    if (this.coreService.role == "companyManager")
      this.router.navigate(["/app/company-manager/message"]);
    if (this.coreService.role == "companyContractor")
      this.router.navigate(["/app/sub-contractor/messages"]);
  }

  myProfileClick() {
    if (this.coreService.role == "superAdmin")
      this.router.navigate(["/app/super-admin/account-setting"]);
    else if (this.coreService.role == "companyAdmin")
      this.router.navigate(["/app/company-admin/setting"]);
    else if (this.coreService.role == "companyEmployee")
      this.router.navigate(["/app/employee/profile/own/edit"]);
    else if (this.coreService.role == "companyContractor")
      this.router.navigate(["/app/sub-contractor/profile/own"]);
  }

  UACClick() {
    if (this.coreService.role == "superAdmin")
      this.router.navigate(["/app/super-admin/user-access/view-roles"]);
  }

  settingClick() {
    if (this.coreService.role == "superAdmin")
      this.router.navigate(["/app/super-admin/site-setting"]);
  }

  paymentClick() {
    //if (this.coreService.role=="superAdmin") this.router.navigate(['/app/super-admin/']);
  }

  subscriptionClick() {
    if (this.coreService.role == "superAdmin")
      this.router.navigate(["/app/super-admin/subscription-plans"]);
  }

  suppportClick() {
    if (this.coreService.role == "superAdmin")
      this.router.navigate([
        "/app/super-admin/app/super-admin/subscription-plans/support-services"
      ]);
  }

  onDownloadUserManual() {
    window.open("http://api.happyhr.com/public/pdf/manual.pdf");
  }

  getCountryList() {
    if (this.coreService.role == "superAdmin") {
      this.layoutService.countryList().subscribe(
        data => {
          if (data.code == 200) {
            this.countryList = data.data;
          }
        },
        error => {
          this.coreService.notify("Unsuccessful", error.message, 0);
        },
        () => {}
      );
    }
  }

  getCountry() {
    if (this.coreService.role == "superAdmin") {
      this.layoutService.getCountry().subscribe(
        data => {
          if (data.code == 200) {
            this.selectedCountry = data.data.countryID;
          }
        },
        error => {
          this.coreService.notify("Unsuccessful", error.message, 0);
        },
        () => {}
      );
    }
  }
  selectCountry(country) {
    if (this.coreService.role == "superAdmin") {
      this.selectedCountry = country.countryID;
      let reqBody = {
        countryID: country.countryID
      };
      this.layoutService.selectCountry(reqBody).subscribe(
        data => {
          if (data.code == 200) {
            Cookie.set("happyhr-token", data.token);
            window.location.reload();
          }
        },
        error => {
          this.coreService.notify("Unsuccessful", error.message, 0);
        },
        () => {}
      );
    }
  }

  logout() {
    Cookie.deleteAll();
    localStorage.clear();
    this.router.navigate(["/user/login"]);
  }

  onRoleChange(type) {
    let url = "";
    //if(type=="genericManager"){type= "manager";}

    if (type == "employee") {
      this.role = "companyEmployee";
      this.coreService.setRole(this.role);
      APPCONFIG.role = this.role;
      //             Cookie.set('happyhr-token',data.token);
      //             Cookie.set('happyhr-userInfo',JSON.stringify(data.data));
      //             localStorage.setItem('happyhr-userInfo',JSON.stringify(data.data));
      //             //this.core.setPermission(data.response.data.permission);
      url = "/app/employee";
    }
    else if(type=='SubContractor'){
      this.role = "companyContractor";
      this.coreService.setRole(this.role);
      APPCONFIG.role = this.role;
      // console.log("sdkjhf",APPCONFIG);
      url = "/app/sub-contractor";

    }
    
    
    else if (type == "manager") {
      this.role = "companyManager";
      this.coreService.setRole(this.role);
      APPCONFIG.role = this.role;
      // this.coreService.setPermission(data.response.data.permission);
      url = "/app/company-manager";
    } else {
      this.role = "companyAdmin";
      this.coreService.setRole(this.role);
      APPCONFIG.role = this.role;
      // console.log("admin app config", APPCONFIG);
      // this.coreService.setPermission(data.response.data.permission);
      url = "/app/company-admin";
    }
    localStorage.setItem(
      "happyhr-temp-role",
      JSON.stringify({ role: this.role })
    );
    this.sideNavService.emitConfig(this.role);
    this.router.navigate([url]);
  }

  onClick() {
    let dialogRef = this.dialog.open(feedbackDialog, { width: "600px" });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
      }
    });
  }
}

@Component({
  selector: "feedback-dialog",
  template: `
  <h5 class="heading">Feedback</h5>
  <div class="feedbackMain">
    <div class="row">
   <form #feedbackForm="ngForm" class="full-width" (submit)="isButtonClicked = true;feedbackForm.form.valid && sendFeedback()">
      <md-progress-spinner style="margin-top:20px;" *ngIf="isProcessing" mode="indeterminate" color=""></md-progress-spinner>
     <div class="col-md-12 form-group">
       <label>Happy</label>
       <div class="customChoice">
          <input id ="feedback2" value="Happy"  #feedbacks="ngModel" [(ngModel)]="feedback.feedback" required type="radio" name="feedback">
          <label for="feedback2"><i class="material-icons happy">sentiment_very_satisfied</i></label>
       </div>
       </div>           
    <div class="col-md-12 form-group">
      <label>Unhappy</label>
       <div class="customChoice">
          <input id ="feedback1" value="Unhappy"  #feedbacks="ngModel" [(ngModel)]="feedback.feedback" required type="radio" name="feedback">
          <label for="feedback1"><i class="material-icons unhappy">sentiment_very_dissatisfied</i></label>
       </div>
       </div>
       <div class="col-md-12 form-group">
       <label>Average</label>
       <div class="customChoice">
         <input id ="feedback3" value="Average"  #feedbacks="ngModel" [(ngModel)]="feedback.feedback" required type="radio" name="feedback">
         <label for="feedback3"><i class="material-icons average">sentiment_neutral</i></label>
       </div>
     </div>
     <div *ngIf="feedbacks.errors && ( (feedbacks.dirty || feedbacks.touched) || isButtonClicked )">
                                        <div class="alert alert-danger" [hidden]="!(feedbacks.errors.required || feedbacks.errors.whitespace)">
                                            feedback is required.
     </div>
     </div>
     <div class="col-md-12 form-group">
        <label style="text-align:left;margin-bottom:8px;">Note</label>
        <textarea name="note" [(ngModel)]="feedback.note" #note="ngModel" required class="form-control"></textarea>
        <div *ngIf="note.errors && ( (note.dirty || note.touched) || isButtonClicked )">
                                        <div class="alert alert-danger" [hidden]="!(note.errors.required || note.errors.whitespace)">
                                            Note is required.
     </div>
     </div>
     </div>
      
     <div class="col-md-12 form-group text-right">
       <input type="submit" class="btn btn-primary" value="Submit"/>
     </div>
    </form>
  </div>
  </div>
 
  `,
  styles: [
    `
      .material-icons.unhappy{
    color: #ff8383;
}
.material-icons.happy {
    color: #4cdaaa;
}
.material-icons.average{
    color: #FF8C00;
}
.material-icons{
        position: relative;
    top: -2px;
    left: -4px;
}
label{
    float:left;
    margin-right:10px;
    font-weight:bold;
    min-width:56px;
    text-align:right;
    margin-bottom:0px;
}
textarea{
    resize:none;
    height:90px;
}
   `
  ]
})
export class feedbackDialog {
  constructor(
    public dialogRef: MdDialogRef<feedbackDialog>,
    public coreService: CoreService,
    public layoutService: LayoutService
  ) {}
  public feedback = {
    feedback: "",
    note: ""
  };
  public isButtonClicked: boolean;
  public isProcessing: boolean;
  ngOnInit() {}

  sendFeedback() {
    this.isProcessing = true;
    this.layoutService.sendFeedback(this.feedback).subscribe(
      d => {
        this.isButtonClicked = false;
        this.isProcessing = false;
        if (d.code == "200") {
          this.dialogRef.close();
          this.coreService.notify("Successful", d.message, 1);
        } else this.coreService.notify("Unsuccessful", d.message, 0);
      },
      error => {
        this.isButtonClicked = false;
        this.isProcessing = false;
        this.coreService.notify("Unsuccessful", "Error while getting data", 0);
      },
      () => {}
    );
  }
}
