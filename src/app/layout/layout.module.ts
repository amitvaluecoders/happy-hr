import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';

import 'hammerjs';
import {DataTableModule} from "angular2-datatable";
import { LayoutRoutingModule } from './layout-routing.module';

// Layout
import { LayoutComponent } from './layout.component';
import { PreloaderDirective } from './preloader.directive';
// Header
import { AppHeaderComponent, feedbackDialog } from './header/header.component';
// Sidenav
import { AppSidenavComponent } from './sidenav/sidenav.component';
import { ToggleOffcanvasNavDirective } from './sidenav/toggle-offcanvas-nav.directive';
import { AutoCloseMobileNavDirective } from './sidenav/auto-close-mobile-nav.directive';
import { AppSidenavMenuComponent } from './sidenav/sidenav-menu/sidenav-menu.component';
import { AccordionNavDirective } from './sidenav/sidenav-menu/accordion-nav.directive';
import { AppendSubmenuIconDirective } from './sidenav/sidenav-menu/append-submenu-icon.directive';
import { HighlightActiveItemsDirective } from './sidenav/sidenav-menu/highlight-active-items.directive';
// Customizer
import { AppCustomizerComponent } from './customizer/customizer.component';
import { ToggleQuickviewDirective } from './customizer/toggle-quickview.directive';
// Footer
import { AppFooterComponent } from './footer/footer.component';
// Search Overaly
import { AppSearchOverlayComponent } from './search-overlay/search-overlay.component';
import { SearchOverlayDirective } from './search-overlay/search-overlay.directive';
import { OpenSearchOverlaylDirective } from './search-overlay/open-search-overlay.directive';
// import { SlimScrollDirective } from '../shared/slim-scroll.directive';
import { CommonModule } from '@angular/common';
import { MaterialModule } from '@angular/material';
import { SideNavService } from './sidenav/sidenav-menu/side-nav.service';
import { LayoutService } from './layout.service';
import { SharedModule } from '../shared/shared.module';
import {ContractEmployeeService} from "../company-employee/employee-contracts/employee-contracts.service";

@NgModule({
    imports: [
        LayoutRoutingModule,
        CommonModule,
        MaterialModule,
        HttpModule,
        FormsModule,
        SharedModule,
        DataTableModule
    ],
    declarations: [
        LayoutComponent,
        PreloaderDirective,
        // Header
        AppHeaderComponent,
        // Sidenav
        AppSidenavComponent,
        ToggleOffcanvasNavDirective,
        AutoCloseMobileNavDirective,
        AppSidenavMenuComponent,
        AccordionNavDirective,
        AppendSubmenuIconDirective,
        HighlightActiveItemsDirective,
        // Customizer
        AppCustomizerComponent,
        ToggleQuickviewDirective,
        // Footer
        AppFooterComponent,
        // Search overlay
        AppSearchOverlayComponent,
        SearchOverlayDirective,
        OpenSearchOverlaylDirective,
        // SlimScrollDirective,
        feedbackDialog
    ],
    providers:[SideNavService, LayoutService,ContractEmployeeService],
    entryComponents: [feedbackDialog]
})

export class LayoutModule {}
