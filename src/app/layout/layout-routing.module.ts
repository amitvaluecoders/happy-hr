import { RouterModule, Routes } from '@angular/router';

import { LayoutComponent } from './layout.component';

const routes: Routes = [
    {
        path: 'app',
        component: LayoutComponent,
        children: [
            //{ path: '', redirectTo: '/app/super-admin', pathMatch: 'full' },           
            { path: 'super-admin', loadChildren: '../super-admin/super-admin.module#SuperAdminModule' },
            { path: 'company-admin', loadChildren: '../company-admin/company-admin.module#CompanyAdminModule' },
            { path: 'company-manager', loadChildren: '../company-admin/company-admin.module#CompanyAdminModule' },
            { path: 'employee', loadChildren: '../company-employee/company-employee.module#CompanyEmployeeModule' },                
            { path: 'sub-contractor', loadChildren: '../sub-contractor/sub-contractor.module#SubContractorModule' },                
        ]
    },
    
];

export const LayoutRoutingModule = RouterModule.forChild(routes);
