import { Component, Input } from '@angular/core';
import { APPCONFIG } from '../../config';
import { CoreService } from "../../shared/services/core.service";
import { Router } from '@angular/router';


@Component({
    selector: 'my-app-sidenav',
    styles: [],
    templateUrl: './sidenav.component.html'
})

export class AppSidenavComponent {
    constructor(public coreService: CoreService,public router:Router){}
    AppConfig;
    role:any;

    ngOnInit() {
        this.AppConfig = APPCONFIG;
            this.role = this.coreService.getRole();
    }

    toggleCollapsedNav() {
        this.AppConfig.navCollapsed = !this.AppConfig.navCollapsed;
    }

    redirectToHome(){
        if (this.coreService.role == "companyAdmin")this.router.navigate(["/app/company-admin"]);
        if (this.coreService.role == "superAdmin")this.router.navigate(["/app/super-admin"]);
        if (this.coreService.role == "companyEmployee")this.router.navigate(["/app/employee"]);
        if (this.coreService.role == "companyManager")this.router.navigate(["/app/company-manager"]);
        if (this.coreService.role == "companyContractor")this.router.navigate(["/app/sub-contractor"]);
    }


}
