import { Component, Input } from '@angular/core';
import { Cookie } from 'ng2-cookies/ng2-cookies';
import { CoreService } from '../../../shared/services/core.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { APPCONFIG } from '../../../config';
import { SideNavService } from './side-nav.service';

@Component({
    selector: 'my-app-sidenav-menu',
    styles: [],
    templateUrl: './sidenav-menu.component.html'
})

export class AppSidenavMenuComponent {

    public configJson = [];
    //public navOpen;
    constructor(public sideNavService:SideNavService){
         this.sideNavService.configObservable.subscribe(value => {
            this.configJson = this.sideNavService.configJSON();
        })
    }
    ngOnInit() {
        this.configJson = this.sideNavService.configJSON();
    }

    
}
