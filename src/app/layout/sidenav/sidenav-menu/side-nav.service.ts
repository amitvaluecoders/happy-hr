import { Injectable } from '@angular/core';
import { CoreService } from '../../../shared/services/core.service';
import { APPCONFIG } from '../../../config';
import { Router, ActivatedRoute } from '@angular/router';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class SideNavService {
    private role;
    private AppConfig: any;
    private permission = [];
    public loggedInUser: any;
    private configValues = {
        superAdmin: [],
        companyAdmin: [],
        companyManager: [],
        companyEmployee: [],
        companyContractor:[]
    };
    private localeRole: any;

    public configObservable = new Subject<string>();

    emitConfig(v) {
        this.configObservable.next(v);
    }

    constructor(private router: Router, private coreService: CoreService) {
        this.role = this.coreService.getRole();
        this.loggedInUser = JSON.parse(localStorage.getItem('happyhr-userInfo'));
        this.localeRole = JSON.parse(localStorage.getItem('happyhr-temp-role'));
        !this.localeRole && localStorage.setItem('happyhr-temp-role', JSON.stringify({ role: this.role }));
    }
    private modulePermission: any;

    configJSON() {
        this.AppConfig = APPCONFIG;
        this.localeRole = JSON.parse(localStorage.getItem('happyhr-temp-role'));
        this.loggedInUser = JSON.parse(localStorage.getItem('happyhr-userInfo'));
        this.localeRole && this.localeRole.role && this.coreService.setRole(this.localeRole.role);
        this.role = this.coreService.getRole();
        this.AppConfig.role = this.role;
        this.permission = JSON.parse(localStorage.getItem('happyhr-permission'));
        this.modulePermission = {};
        for (let permission of this.permission) {
            this.modulePermission[permission.module] = {};
            this.modulePermission[permission.module] = permission.permission;
        }
        //console.log("logged in user details",this.loggedInUser);

        this.configValues.companyContractor = [
         
                     
            {
                name: "Contracts",
                condition: true,
                urlRoute: ['/app/sub-contractor/contracts'],
                icon: "fa fa-pencil-square-o",
                iconName: "",
            },
            {
                name: "Dashboard",
                condition: true,
                urlRoute: ['/app/sub-contractor'],
                icon: "material-icons",
                iconName: "home",
            },
             {
                name: "Message",
                condition: true,
                urlRoute: ['/app/sub-contractor/messages'],
                icon: "material-icons",
                iconName: "message",
            },
            {
                name: "Organisation chart",
                condition: true,
                urlRoute: ['/app/sub-contractor/organisation-chart'],
                icon: "fa fa-sitemap",
                iconName: " ",
            },
            {
                name: "Profile",
                condition: true,
                urlRoute: ['/app/sub-contractor/profile/own'],
                icon: "material-icons",
                iconName: "account_circle",
            },
            
             {
                name: "Procedural documents",
                condition: true,
                urlRoute: ['/app/sub-contractor/policy-documents/procedural-document'],
                icon: "material-icons",
                iconName: "trending_up",
                // child: [
                //     {
                //         name: 'Policy documents',
                       
                //     },
                //     {
                //         name: 'Procedural documents',
                //         urlRoute: ['/app/sub-contractor/policy-documents/procedural-document']
                //     }
                // ]
            },
            {
                name: "Leaves",
                condition: true,
                urlRoute: ['/app/sub-contractor/leaves'],
                icon: "material-icons",
                iconName: "assignment",
            },

            {
                name: "OH&S",
                condition: true,
                urlRoute: ['/app/sub-contractor/ohs'],
                icon: "material-icons",
                iconName: "assignment",
            },
            // {
            //     name: "User access",
            //     condition:  true,
            //     urlRoute: ['/app/sub-contractor/user-access'],
            //     icon: "material-icons",
            //     iconName: "accessibility",
            // },
            {
                name: "Training plan",
                condition:  true,
                urlRoute: ['/app/sub-contractor/training-day/training-plan/list'],
                icon: "material-icons",
                iconName: "group",
                // child: [
                //     {
                //         name: 'Create master training plan',
                //         urlRoute: ['/app/company-admin/training-day/master-training-plan/add']
                //     },
                //     // {
                //     //     name : 'Create training plan',
                //     //     urlRoute : ['/app/company-admin/training-day/training-plan/add']
                //     // },
                //     {
                //         name: 'Master training plans',
                //         urlRoute: ['/app/company-admin/training-day']
                //     },
                //     {
                //         name: 'Training plan',
                //         urlRoute: ['/app/company-admin/training-day/training-plan/list']
                //     }
                // ]
            },
            
        

        ];

        this.configValues.superAdmin = [

            {
                name: "Account setting",
                condition: this.modulePermission.accountSetting && this.modulePermission.accountSetting.view,
                urlRoute: ['/app/super-admin/account-setting'],
                icon: "material-icons",
                iconName: "account_box",
                // child : [
                //     {
                //         name : 'Account setting',
                //         urlRoute : ['/app/super-admin/account-setting']
                //     },
                //     {
                //         name : 'Zoho account settings',
                //         urlRoute : ['/app/super-admin/account-setting/zoho']
                //     },

                // ]
            },

            {
                name: "Awards",
                condition: this.modulePermission.award && this.modulePermission.award.view,
                urlRoute: ['/app/super-admin/awards'],
                icon: "fa fa-shield",
                iconName: "",
            },
            {
                name: "Broadcast",
                condition: this.modulePermission.broadcast && this.modulePermission.broadcast.view,
                urlRoute: ['/app/super-admin/broadcast'],
                icon: "material-icons",
                iconName: "settings_remote",
            },
            {
                name: "Checklist management",
                condition: this.modulePermission.checklist && this.modulePermission.checklist.view,
                urlRoute: [],
                icon: "material-icons",
                iconName: "done",
                child: [
                    {
                        name: 'On boarding checklist',
                        urlRoute: ['/app/super-admin/checklist']
                    },
                    {
                        name: 'Off boarding checklist',
                        urlRoute: ['/app/super-admin/checklist/offBoarding']
                    },
                    {
                        name: 'Create new category',
                        urlRoute: ['/app/super-admin/checklist/new-category']
                    },
                ]
            },
            {
                name: "Company management",
                condition: this.modulePermission.companyManagement && this.modulePermission.companyManagement.view,
                urlRoute: ['/app/super-admin/company'],
                icon: "material-icons",
                iconName: "business",
            },
            {
                name: "Contracts",
                condition: this.modulePermission.contract && this.modulePermission.contract.view,
                urlRoute: ['/app/super-admin/contract'],
                icon: "fa fa-pencil-square-o",
                iconName: "",
            },
            {
                name: "Dashboard",
                condition: true,
                urlRoute: ['/app/super-admin'],
                icon: "material-icons",
                iconName: "home",
            },
            {
                name: "Discount coupons",
                condition: false,
                urlRoute: ['/app/super-admin/discount-coupon-management'],
                icon: "material-icons",
                iconName: "settings_applications",
            },
            {
                name: "Email management",
                condition: this.modulePermission.emailManagement && this.modulePermission.emailManagement.view,
                urlRoute: [],
                icon: "material-icons",
                iconName: "email",
                child: [
                    {
                        name: 'Company admin email',
                        urlRoute: ['/app/super-admin/email-template', 'company-admin-email']
                    },
                    {
                        name: 'Company manager email',
                        urlRoute: ['/app/super-admin/email-template', 'company-manager-email']
                    },
                    {
                        name: 'Employee email',
                        urlRoute: ['/app/super-admin/email-template', 'employee-email']
                    },
                    {
                        name: 'Super admin email',
                        urlRoute: ['/app/super-admin/email-template', 'super-admin-email']
                    },
                    {
                        name: 'Third party email',
                        urlRoute: ['/app/super-admin/email-template', 'third-party-email']
                    },
                ]
            },
            {
                name: "Exit survey",
                condition: this.modulePermission.exitSurvey && this.modulePermission.exitSurvey.view,
                urlRoute: ['/app/super-admin/exit-survey'],
                icon: "material-icons",
                iconName: "description",
            },
            {
                name: "Feedback management",
                condition: this.modulePermission.feedback && this.modulePermission.feedback.view,
                urlRoute: ['/app/super-admin/feedback-management'],
                icon: "material-icons",
                iconName: "feedback",
            },
            {
                name: "Interview guide",
                condition: this.modulePermission.interviewGuide && this.modulePermission.interviewGuide.view,
                urlRoute: ['/app/super-admin/interview-guide'],
                icon: "material-icons",
                iconName: "settings_remote",
            },
            {
                name: "Jobs component",
                condition: this.modulePermission.job && this.modulePermission.job.view,
                urlRoute: [],
                icon: "material-icons",
                iconName: "description",
                child: [
                    {
                        name: 'Industry & job functions',
                        urlRoute: ['/app/super-admin/jobs-component']
                    },
                    {
                        name: 'Employment type',
                        urlRoute: ['/app/super-admin/jobs-component/employement']
                    },
                ]
            },
            {
                name: "Letters",
                condition: this.modulePermission.letter && this.modulePermission.letter.view,
                urlRoute: ['/app/super-admin/letters'],
                icon: "fa fa-pencil-square-o",
                iconName: "",
            },
            {
                name: "Message",
                condition: this.modulePermission.message && this.modulePermission.message.view,
                urlRoute: ['/app/super-admin/message'],
                icon: "material-icons",
                iconName: "message",
            },
            {
                name: "Policy management",
                condition: this.modulePermission.policyDocument && this.modulePermission.policyDocument.view,
                urlRoute: ['/app/super-admin/policy'],
                icon: "material-icons",
                iconName: "assignment",
            },
            {
                name: "Position description",
                condition: (this.modulePermission.positionDescription && this.modulePermission.positionDescription.view) || (this.modulePermission.industry && this.modulePermission.industry.view),
                urlRoute: [],
                icon: "fa fa-male",
                iconName: "",
                child: [
                    {
                        name: 'Industry management',
                        urlRoute: ['/app/super-admin/position-description/industry-management']
                    },
                    {
                        name: 'Position description',
                        urlRoute: ['/app/super-admin/position-description/pd-list']
                    },
                ]
            },
            {
                name: "Reports",
                condition: this.modulePermission.report && this.modulePermission.report.view,
                urlRoute: [],
                icon: "material-icons",
                iconName: "description",
                child: [
                    {
                        name: 'User report',
                        urlRoute: ['/app/super-admin/reports']
                    },
                    // {
                    //     name: 'Coupon report',
                    //     urlRoute: ['/app/super-admin/reports/coupon']
                    // },
                    // {
                    //     name: 'Transaction report',
                    //     urlRoute: ['/app/super-admin/reports/transaction']
                    // },
                    // {
                    //     name: 'Payment report',
                    //     urlRoute: ['/app/super-admin/reports/payment']
                    // },
                    // {
                    //     name: 'Revenue report',
                    //     urlRoute: ['/app/super-admin/reports/revenue']
                    // },
                    {
                        name: 'API report',
                        urlRoute: ['/app/super-admin/reports/API']
                    },
                ]
            },
            {
                name: "Site setting",
                condition: this.modulePermission.siteSetting && this.modulePermission.siteSetting.view,
                urlRoute: ['/app/super-admin/site-setting'],
                icon: "material-icons",
                iconName: "settings_applications",
            },
            {
                name: "Subscription plan",
                condition: false,
                urlRoute: [],
                icon: "material-icons",
                iconName: "subscriptions",
                child: [
                    {
                        name: 'Subscription list',
                        urlRoute: ['/app/super-admin/subscription-plans']
                    },
                    {
                        name: 'Subscription detail',
                        urlRoute: ['/app/super-admin/subscription-plans/detail']
                    },
                    {
                        name: 'Add new plan',
                        urlRoute: ['/app/super-admin/subscription-plans/add']
                    },
                    {
                        name: 'Support services',
                        urlRoute: ['/app/super-admin/subscription-plans/support-services']
                    },
                ]
            },
            {
                name: "Survey questionnaires",
                condition: this.modulePermission.surveyQuestion && this.modulePermission.surveyQuestion.view,
                urlRoute: ['/app/super-admin/survey/detail/1'],
                icon: "material-icons",
                iconName: "description",
            },
            {
                name: "Trophy",
                condition: this.modulePermission.trophy && this.modulePermission.trophy.view,
                urlRoute: ['/app/super-admin/trophy'],
                icon: "fa fa-trophy",
                iconName: "",
            },
            {
                name: "User access",
                condition: (this.modulePermission.user && this.modulePermission.user.view) || (this.modulePermission.role && this.modulePermission.role.view),
                urlRoute: [],
                icon: "material-icons",
                iconName: "accessibility",
                child: [
                    {
                        name: 'View roles',
                        urlRoute: ['/app/super-admin/user-access/view-roles']
                    },
                    {
                        name: 'Users list',
                        urlRoute: ['/app/super-admin/user-access/users-list']
                    }
                ]
            },

        ];
        this.configValues.companyAdmin = [
           
          
            {
                name: "Activities & approvals",
                condition: true,
                urlRoute: ['/app/company-admin/activities'],
                icon: "fa fa-tasks",
                iconName: "",
            },
            {
                name: "Advertisement",
                condition: (this.modulePermission.advertisement && this.modulePermission.advertisement.view) ,
                urlRoute: [],
                icon: "material-icons",
                iconName: "cast_connected",
                child: [
                    {
                        name: 'Advertisement list',
                        urlRoute: ['/app/company-admin/advertisement/']
                    },
                    {
                        name: 'Create advertisement',
                        urlRoute: ['/app/company-admin/advertisement/create']
                    }
                ]
            },
             {
                name: "Assets",
                condition: true ,
                urlRoute: [],
                icon: "material-icons",
                iconName: "folder",
                child: [
                    {
                        name: 'Assets listing',
                        urlRoute: ['/app/company-admin/listing-page/assets-listing']
                    },
                    {
                        name: 'Assets approval listing',
                        urlRoute: ['/app/company-admin/listing-page/assets-approval']
                    }
                ]
            },
            
            {
                name: "Checklist",
                condition: (this.modulePermission.checklist && this.modulePermission.checklist.view) ,
                urlRoute: [],
                icon: "material-icons",
                iconName: "check",
                child: [
                    {
                        name: 'On boarding checklist',
                        urlRoute: ['/app/company-admin/checklist']
                    },
                    {
                        name: 'Off boarding checklist',
                        urlRoute: ['/app/company-admin/checklist/offBoarding']
                    },
                    {
                        name: 'Create category',
                        urlRoute: ['/app/company-admin/checklist/add-category']
                    }
                ]
            },
            {
                name: "Contracts",
                condition:  (this.modulePermission.contract && this.modulePermission.contract.view),
                urlRoute: ['/app/company-admin/contract'],
                icon: "fa fa-pencil-square-o",
                iconName: "",
            },
            {
                name: "Dashboard",
                condition: true,
                urlRoute: ['/app/company-admin'],
                icon: "fa fa-home",
                iconName: "",
            },
            {
                name: "Employee management",
                condition:   (this.modulePermission.employee && this.modulePermission.employee.view),
                urlRoute: ['/app/company-admin/employee-management'],
                icon: "fa fa-address-card",
                iconName: "",
            },
            {
                name: "Leaves",
                condition:  (this.modulePermission.leaves && this.modulePermission.leaves.view),
                urlRoute: ['/app/company-admin/leaves'],
                icon: "fa fa-clipboard",
                iconName: "",
            },
            {
                name: "Listing",
                condition: true,
                urlRoute: [],
                icon: "fa fa-list",
                iconName: "",
                child: [
                    {
                        name: '360 survey listing',
                        urlRoute: ['/app/company-admin/survey']
                    },
                    {
                        name: 'Asset listing',
                        urlRoute: ['/app/company-admin/listing-page/assets-listing']
                    },
                  
                    {
                        name: 'BPIP listing',
                        urlRoute: ['/app/company-admin/listing-page/behavior-performance-improvement-listing']
                    },
                    {
                        name: 'Development plan listing',
                        urlRoute: ['/app/company-admin/listing-page/development-plan-list']
                    },
                    {
                        name: 'Disciplinary action listing',
                        urlRoute: ['/app/company-admin/listing-page/disciplinary-action-listing']
                    },
                   
                    {
                        name: 'Grievance listing',
                        urlRoute: ['/app/company-admin/listing-page/grievance-listing']
                    },
                    {
                        name: 'Induction onboarding listing',
                        urlRoute: ['/app/company-admin/listing-page/induction-onboarding-listing']
                    },
                    // {
                    //     name : 'New Contract listing',
                    //     urlRoute : ['/app/company-admin/listing-page/new-contract-list']
                    // },
                    {
                        name : 'Mandatory documents listing',
                        urlRoute : ['/app/company-admin/docs-mandatory']
                    },
                    {
                        name: 'New employee approval listing',
                        urlRoute: ['/app/company-admin/listing-page/new-emloyee-approval']
                    },
                        // {
                        //     name: 'Off-boarding listing',
                        //     urlRoute: ['/app/company-admin/listing-page/off-boarding-listing']
                        // },
                    {
                        name: 'OH&S listing',
                        urlRoute: ['/app/company-admin/listing-page/ohs-listing']
                    },
                    {
                        name: 'Performance appraisal listing',
                        urlRoute: ['/app/company-admin/listing-page/performance-appraisal-listing']
                    },
                    {
                        name: 'Performance indicator listing',
                        urlRoute: ['/app/company-admin/listing-page/performance-indicator-listing']
                    },
                    {
                        name: 'Probationary meeting listing',
                        urlRoute: ['/app/company-admin/listing-page/probationary-meeting']
                    },
                    {
                        name: 'PIP listing',
                        urlRoute: ['/app/company-admin/listing-page/']
                    },
                    // {
                    //     name : 'Training plan listing',
                    //     urlRoute : ['/app/company-admin/training-day/training-plan/list']
                    // },
                    // {
                    //     name : 'Trophy listing',
                    //     urlRoute : ['/app/company-admin/trophy/trophy-list']
                    // },
                    {
                        name: 'Trophy certificates listing',
                        urlRoute: ['/app/company-admin/listing-page/trophy-certificates']
                    },
                    {
                        name: 'Uploaded documents',
                        urlRoute: ['/app/company-admin/docs-uploaded']
                    },
                    {
                        name: 'Warning listing',
                        urlRoute: ['/app/company-admin/listing-page/warning-list']
                    },

                ]
            },
            {
                name: "Messaging",
                condition: true,
                urlRoute: [],
                icon: "material-icons",
                iconName: "message",
                child:[
                    {
                        name: "Broadcast messages",
                        condition: (this.modulePermission.broadcast && this.modulePermission.broadcast.view) ,
                        urlRoute: ['/app/company-admin/broadcast'],
                        icon: "fa fa-bullhorn",
                        iconName: "",
                    },
                    {
                        name: "Message",
                        condition: true,
                        urlRoute: ['/app/company-admin/message'],
                        icon: "material-icons",
                        iconName: "message",
                    },
                    
                ]

            },
            
               {
                name: "Notes",
                condition: (this.modulePermission.note && this.modulePermission.note.view) ,
                urlRoute: [],
                icon: "material-icons",
                iconName: "description",
                child: [
                    {
                        name: 'File notes',
                        urlRoute: ['/app/company-admin/file-notes']
                    },
                    {
                        name: 'General notes',
                        urlRoute: ['/app/company-admin/general-notes']
                    }
                ]
            },
            {
                name: "Organisation chart",
                condition:  true,
                urlRoute: ['/app/company-admin/organisation-chart'],
                icon: "fa fa-sitemap",
                iconName: "",
            },
            {
                name: "Policy documents",
                condition:  (this.modulePermission.policyDocument && this.modulePermission.policyDocument.view),
                urlRoute: [],
                icon: "material-icons",
                iconName: "trending_up",
                child: [
                    {
                        name: 'Policy documents',
                        urlRoute: ['/app/company-admin/policy-documents']
                    },
                    {
                        name: 'Procedural documents',
                        urlRoute: ['/app/company-admin/policy-documents/procedural-document']
                    },
                    {
                        name: 'Unused documents',
                        urlRoute: ['/app/company-admin/policy-documents/unused-documents']
                    }
                ]
            },
            {
                name: "Position description",
                // condition:  (this.modulePermission.position && this.modulePermission.position.view),
                condition: true,
                urlRoute: ['/app/company-admin/position-description'],
                icon: "fa fa-male",
                iconName: "",
            },
            {

                name: "Settings",
                condition:  true,
                urlRoute: [],
                icon: "material-icons",
                iconName: "settings",
                child: [
                    {
                        name: 'Account settings',
                        urlRoute: ['/app/company-admin/setting/accounts-settings']
                    },
                    {
                        name: 'API integration',
                        urlRoute: ['/app/company-admin/setting/api-integration']
                    },
                    {
                        name: 'Company profile settings',
                        urlRoute: ['/app/company-admin/setting']
                    },
                    // {
                    //     name : 'Module management',
                    //     urlRoute : ['/app/company-admin/setting/module-management']
                    // },
                    {
                        name: 'Notifications',
                        urlRoute: ['/app/company-admin/setting/notifications']
                    },
                    {
                        name: 'Support',
                        urlRoute: ['/app/company-admin/setting/support']
                    },
                    {
                        name: 'Transaction',
                        urlRoute: ['/app/company-admin/setting/transaction']
                    },
                    {
                        name: 'Upcoming payments',
                        urlRoute: ['/app/company-admin/setting/upcomming-payments']
                    },
                    {
                        name: "User access",
                        condition:  true,
                        urlRoute: ['/app/company-admin/user-access'],
                        icon: "material-icons",
                        iconName: "accessibility",
                    }
                ]
            },
            // {
            
            //     condition:   (this.modulePermission.subContractor && this.modulePermission.subContractor.view),
            //     urlRoute: ['/app/company-admin/subcontractor-management'],
            //     
            //     iconName: "",
            // },
             {

                name: "Sub contractor",
                condition:  (this.modulePermission.subContractor && this.modulePermission.subContractor.view),
                urlRoute: [],
                icon: "fa fa-address-card",
                iconName: "",
                child: [
                    {
                        name: 'Contracts',
                        urlRoute: ['/app/company-admin/sc/contract']
                    },
                    {
                        name: 'Contractor management',
                        urlRoute: ['/app/company-admin/subcontractor-management']
                    },
                   
                ]
            },
             {
                name: "Survey",
                condition: (this.modulePermission['360Survey'] && this.modulePermission['360Survey'].view),
                urlRoute: [],
                icon: "material-icons",
                iconName: "description",
                child: [
                    {
                        name: '360 survey',
                        urlRoute: ['/app/company-admin/survey']
                    },
                     {
                        name: 'Exit survey',
                        urlRoute: ['/app/company-admin/listing-page/exit-survey']
                    }
                ]
            },
            {
                name: "Training day",
                condition:  (this.modulePermission.trainingPlan && this.modulePermission.trainingPlan.view),
                urlRoute: [],
                icon: "material-icons",
                iconName: "group",
                child: [
                    {
                        name: 'Create master training plan',
                        urlRoute: ['/app/company-admin/training-day/master-training-plan/add']
                    },
                    // {
                    //     name : 'Create training plan',
                    //     urlRoute : ['/app/company-admin/training-day/training-plan/add']
                    // },
                    {
                        name: 'Master training plans',
                        urlRoute: ['/app/company-admin/training-day']
                    },
                    {
                        name: 'Training plan',
                        urlRoute: ['/app/company-admin/training-day/training-plan/list']
                    }
                ]
            },
            {
                name: "Trophy",
                condition:  (this.modulePermission.trophy && this.modulePermission.trophy.view),
                urlRoute: [],
                icon: "fa fa-trophy",
                iconName: "",
                child: [
                    {
                        name: 'Create trophy',
                        urlRoute: ['/app/company-admin/trophy/create-trophy']
                    },
                    {
                        name: 'Trophy list',
                        urlRoute: ['/app/company-admin/trophy/trophy-list']
                    }
                ]
            },
            {
                name: "Sub company",
                condition: !this.loggedInUser.isSubCompany &&  (this.modulePermission.subCompany && this.modulePermission.subCompany.view),
                urlRoute: [],
                icon: "material-icons",
                iconName: "layers",
                child: [
                    {
                        name: 'Listing',
                        urlRoute: ['/app/company-admin/sub-company']
                    },
                    {
                        name: 'Payments',
                        urlRoute: ['/app/company-admin/sub-company/payments']
                    }
                ]
            },
           
        ];

        // this.configValues.companyManager = this.configValues.companyAdmin; 

        this.configValues.companyManager = [

          
            {
                name: "Activities & approvals",
                condition: true,
                urlRoute: ['/app/company-manager/activities'],
                icon: "fa fa-tasks",
                iconName: "",
            },
            {
                name: "Advertisement",
                condition: true,
                urlRoute: [],
                icon: "material-icons",
                iconName: "cast_connected",
                child: [
                    {
                        name: 'Advertisement list',
                        urlRoute: ['/app/company-manager/advertisement/']
                    },
                    {
                        name: 'Create advertisement',
                        urlRoute: ['/app/company-manager/advertisement/create']
                    }
                ]
            },
             {
                name: "Assets",
                condition: true ,
                urlRoute: [],
                icon: "material-icons",
                iconName: "folder",
                child: [
                    {
                        name: 'Assets listing',
                        urlRoute: ['/app/company-manager/listing-page/assets-listing']
                    },
                    {
                        name: 'Assets approval listing',
                        urlRoute: ['/app/company-manager/listing-page/assets-approval']
                    }
                ]
            },
            {
                name: "Broadcast messages",
                condition: true,
                urlRoute: ['/app/company-manager/broadcast'],
                icon: "fa fa-bullhorn",
                iconName: "",
            },
            {
                name: "Checklist",
                condition: true,
                urlRoute: [],
                icon: "material-icons",
                iconName: "check",
                child: [
                    {
                        name: 'On boarding checklist',
                        urlRoute: ['/app/company-manager/checklist']
                    },
                    {
                        name: 'Off boarding checklist',
                        urlRoute: ['/app/company-manager/checklist/offBoarding']
                    },

                ]
            },

            {
                name: "Dashboard",
                condition: true,
                urlRoute: ['/app/company-manager'],
                icon: "fa fa-home",
                iconName: "",
            },
            {
                name: "Employee management",
                condition: true,
                urlRoute: ['/app/company-manager/employee-management'],
                icon: "fa fa-address-card",
                iconName: "",
            },
            {
                name: "File notes",
                condition: true,
                urlRoute: ['/app/company-manager/file-notes'],
                icon: "material-icons",
                iconName: "description",
            },
            {
                name: "Leaves",
                condition: true,
                urlRoute: ['/app/company-manager/leaves'],
                icon: "fa fa-clipboard",
                iconName: "",
            },
              {
                name: "Listing",
                condition: true,
                urlRoute: [],
                icon: "fa fa-list",
                iconName: "",
                child: [
                   
                    {
                        name: 'BPIP listing',
                        urlRoute: ['/app/company-manager/listing-page/behavior-performance-improvement-listing']
                    },
                    {
                        name: 'Development plan listing',
                        urlRoute: ['/app/company-manager/listing-page/development-plan-list']
                    },
                    {
                        name: 'Disciplinary action listing',
                        urlRoute: ['/app/company-manager/listing-page/disciplinary-action-listing']
                    },
                  
                    {
                        name: 'Grievance listing',
                        urlRoute: ['/app/company-manager/listing-page/grievance-listing']
                    },
                    {
                        name: 'Induction onboarding listing',
                        urlRoute: ['/app/company-manager/listing-page/induction-onboarding-listing']
                    },

                    {
                        name: 'New employee approval listing',
                        urlRoute: ['/app/company-manager/listing-page/new-emloyee-approval']
                    },
                    // {
                    //     name: 'Off-boarding listing',
                    //     urlRoute: ['/app/company-manager/listing-page/off-boarding-listing']
                    // },
                    {
                        name: 'OH&S listing',
                        urlRoute: ['/app/company-manager/listing-page/ohs-listing']
                    },
                    {
                        name: 'Performance appraisal listing',
                        urlRoute: ['/app/company-manager/listing-page/performance-appraisal-listing']
                    },
                    {
                        name: 'Performance indicator listing',
                        urlRoute: ['/app/company-manager/listing-page/performance-indicator-listing']
                    },
                    {
                        name: 'Probationary meeting listing',
                        urlRoute: ['/app/company-manager/listing-page/probationary-meeting']
                    },
                    {
                        name: 'PIP listing',
                        urlRoute: ['/app/company-manager/listing-page/']
                    },
                    {
                        name: 'Trophy certificates listing',
                        urlRoute: ['/app/company-manager/listing-page/trophy-certificates']
                    },
                    {
                        name: 'Warning listing',
                        urlRoute: ['/app/company-manager/listing-page/warning-list']
                    },

                ]
            },
            {
                name: "Message",
                condition: true,
                urlRoute: ['/app/company-manager/message'],
                icon: "material-icons",
                iconName: "message",
            },
            {
                name: "Organisation chart",
                condition: true,
                urlRoute: ['/app/company-manager/organisation-chart'],
                icon: "fa fa-sitemap",
                iconName: "",
            },
            {
                name: "Policy documents",
                condition: true,
                urlRoute: [],
                icon: "material-icons",
                iconName: "trending_up",
                child: [

                    {
                        name: 'Procedural documents',
                        urlRoute: ['/app/company-manager/policy-documents/procedural-document']
                    },

                ]
            },
            {
                name: "Position description",
                condition: true,
                urlRoute: ['/app/company-manager/position-description'],
                icon: "fa fa-male",
                iconName: "",
            },
            // {

            //     name : "Settings",
            //     condition : true,
            //     urlRoute : [],
            //     icon : "material-icons",
            //     iconName : "settings",
            //     child : [

            //         {
            //             name : 'Notifications',
            //             urlRoute : ['/app/company-manager/setting/notifications']
            //         },

            //     ]
            // },
             {
                name: "Survey",
                condition: true,
                urlRoute: [],
                icon: "material-icons",
                iconName: "description",
                child: [
                    {
                        name: '360 survey',
                        urlRoute: ['/app/company-manager/survey']
                    },
                      {
                        name: 'Exit survey',
                        urlRoute: ['/app/company-manager/listing-page/exit-survey']
                    },
                ]
            },
            {
                name: "Training day",
                condition: true,
                urlRoute: [],
                icon: "material-icons",
                iconName: "group",
                child: [
                    {
                        name: 'Create master training plan',
                        urlRoute: ['/app/company-manager/training-day/master-training-plan/add']
                    },
                    // {
                    //     name : 'Create training plan',
                    //     urlRoute : ['/app/company-manager/training-day/training-plan/add']
                    // },
                    {
                        name: 'Master training plans',
                        urlRoute: ['/app/company-manager/training-day']
                    },
                    {
                        name: 'Training plan',
                        urlRoute: ['/app/company-manager/training-day/training-plan/list']
                    }
                ]
            },
            {
                name: "Trophy",
                condition: true,
                urlRoute: [],
                icon: "fa fa-trophy",
                iconName: "",
                child: [

                    {
                        name: 'Trophy list',
                        urlRoute: ['/app/company-manager/trophy/trophy-list']
                    }
                ]
            },

        ];


        this.configValues.companyEmployee = [
            {
                name: "Trophy",
                condition: true,
                urlRoute: [],
                icon: "fa fa-trophy",
                iconName: "",
                child: [
                    {
                        name: 'Trophies I won',
                        urlRoute: ['/app/employee/trophy/win-trophy']
                    },
                    {
                        name: 'Trophy list',
                        urlRoute: ['/app/employee/trophy']
                    }
                ]
            },
            {
                name: "Position description",
                condition: true,
                urlRoute: ['/app/employee/position-description'],
                icon: "fa fa-male",
                iconName: " ",
            }, {
                name: "Probationary meeting",
                condition: true,
                urlRoute: ['/app/employee/probationary-period-meeting'],
                icon: "fa fa-user-o",
                iconName: " ",
            }, {
                name: "Profile",
                condition: true,
                urlRoute: ['/app/employee/profile/own/edit'],
                icon: "material-icons",
                iconName: "account_circle",
            },
            {
                name: "Notification",
                condition: true,
                urlRoute: ['/app/employee/setting'],
                icon: "fa fa-bell",
                iconName: " ",
            },
            {
                name: "Policy documents",
                condition: true,
                urlRoute: [],
                icon: "material-icons",
                iconName: "trending_up",
                child: [
                    {
                        name: 'Policy documents',
                        urlRoute: ['/app/employee/policy-documents']
                    },
                    {
                        name: 'Procedural documents',
                        urlRoute: ['/app/employee/policy-documents/procedural-document']
                    }
                ]
            },
            {
                name: "Organisation chart",
                condition: true,
                urlRoute: ['/app/employee/organisation-chart'],
                icon: "fa fa-sitemap",
                iconName: " ",
            },
            {
                name: "OH&S",
                condition: true,
                urlRoute: ['/app/employee/ohs'],
                icon: "fa fa-h-square",
                iconName: " ",
            },
            {
                name: "Message",
                condition: true,
                urlRoute: ['/app/employee/message'],
                icon: "material-icons",
                iconName: "message",
            },
            {
                name: "Leaves",
                condition: true,
                urlRoute: ['/app/employee/leaves'],
                icon: "fa fa-clipboard",
                iconName: " ",
            },
            {
                name: "Grievance",
                condition: true,
                urlRoute: ['/app/employee/grievance'],
                icon: "fa fa-user-o",
                iconName: " ",
            },
            {
                name: "Dashboard",
                condition: true,
                urlRoute: ['/app/employee'],
                icon: "fa fa-address-card",
                iconName: " ",
            },
            {
                name: "Contracts",
                condition: true,
                urlRoute: ['/app/employee/contracts'],
                icon: "fa fa-pencil-square-o",
                iconName: " ",
            },
            // {
            //     name : "Complete profile",
            //     condition : true,
            //     urlRoute : [],
            //     icon : "fa fa-address-book-o",
            //     iconName : "",
            //     child : [
            //         {
            //             name : 'Contract approval',
            //             urlRoute : ['/app/employee/profile/contract-approval']
            //         },
            //         {
            //             name : 'Policy approval',
            //             urlRoute : ['/app/employee/profile/policy-approval']
            //         },
            //         {
            //             name : 'Position description',
            //             urlRoute : ['/app/employee/profile/position-description']
            //         },
            //         {
            //             name : 'Profile',
            //             urlRoute : ['/app/employee/profile']
            //         },
            //         {
            //             name : 'Questionnaires',
            //             urlRoute : ['/app/employee/profile/questionnaires']
            //         }
            //     ]
            // },
            {
                name: "Checklist",
                condition: true,
                urlRoute: [],
                icon: "fa fa-check",
                iconName: "",
                child: [
                    {
                        name: 'On boarding checklist',
                        urlRoute: ['/app/employee/checklist']
                    },
                    // {
                    //     name: 'Off boarding checklist',
                    //     urlRoute: ['/app/employee/checklist/offBoarding']
                    // }
                ]
            },
            {
                name: "Awards",
                condition: this.modulePermission.award && this.modulePermission.award.view,
                urlRoute: ['/app/super-admin/awards'],
                icon: "fa fa-shield",
                iconName: "",
            },
            {
                name: "Activities & approvals",
                condition: true,
                urlRoute: ['/app/employee/activites'],
                icon: "fa fa-tasks",
                iconName: " ",
            },
            {
                name: "360 survey",
                condition: true,
                urlRoute: ['/app/employee/survey'],
                icon: "fa fa-clipboard",
                iconName: " ",
            },
        ];

        if (JSON.parse(localStorage.getItem('happyhr-userInfo')).terminated) {
            this.configValues[this.AppConfig.role] = this.configValues[this.AppConfig.role].filter((v, i) => {
                if (~["Contracts"].indexOf(v.name)) return true;
            });
        }
        if(this.AppConfig.role == "companyAdmin" && APPCONFIG.subRole){
            if(APPCONFIG.subRole == 'genericFinance'){
                this.configValues[this.AppConfig.role] = this.configValues[this.AppConfig.role].filter((v, i) => {
                if (~["Employee management"].indexOf(v.name)) return true;
                });
            } else if(APPCONFIG.subRole == 'genericOperation'){
                this.configValues[this.AppConfig.role] = this.configValues[this.AppConfig.role].filter((v, i) => {
                if (!~["Employee management" , 'User access'].indexOf(v.name)) return true;
                });
            }  
        }
        return this.configValues[this.AppConfig.role].filter((v, i) => v.condition)
            .sort(function (a, b) {
                var nameA = a.name.toLowerCase(), nameB = b.name.toLowerCase()
                if (nameA < nameB)
                    return -1;
                if (nameA > nameB)
                    return 1;
                return 0
            });
      }
}


