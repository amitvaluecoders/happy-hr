import { Directive, ElementRef } from '@angular/core';
import { Location } from '@angular/common';
import { Router, NavigationEnd } from '@angular/router';

@Directive({ selector: '[myHighlightActiveItems]' })

export class HighlightActiveItemsDirective {
    constructor(private el: ElementRef, private location: Location, private router: Router) {}

    ngAfterViewInit() {
        const $el = $(this.el.nativeElement);
        let $links = $el.find('a');
        //console.log($el.find('li'));
        function highlightActive(links) {
            let path = location.hash
             

            links.each( (i, link) => {               
                let $link = $(link);
                let $li = $link.parent('li');
                let href = $link.attr('href');
            //console.log("Current :"+JSON.stringify($links));
                if ($li.hasClass('active')) {
                    $li.removeClass('active');
                }
                if (href==path) {
                    $li.addClass('active');
                }
            } )
        }

       // highlightActive($links);

        this.router.events.subscribe((evt) => {
            if (!(evt instanceof NavigationEnd)) {
                return;
            }
            highlightActive($links);
        });
    }
}

