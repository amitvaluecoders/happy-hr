
function makeAppConfig() {
    let date = new Date();
    let year = date.getFullYear();

    let AppConfig = {
        brand: 'Happy Hr',
        user: 'Lisa',
        year: year,
        layoutBoxed: false,                             // true, false
        navCollapsed: false,                            // true, false
        navBehind: false,                               // true, false
        fixedHeader: true,                              // true, false
        sidebarWidth: 'middle',                         // small, middle, large      
        theme: 'light',                                 // light, gray, dark
        colorOption: '34',                              // 11,12,13,14,15,16; 21,22,23,24,25,26; 31,32,33,34,35,36
        AutoCloseMobileNav: true,                       // true, false. Automatically close sidenav on route change (Mobile only)
        productLink: 'http://www.google.com',
        heading: '',
        role: '',
        canSwitch: false,
        subRole: '',
        imageUrl: '',
        tempNote:'',
        oldUserRole:''
    };

    return AppConfig;
}

export const CHARTCONFIG = {
    primary: 'rgba(33,150,243,.85)', // #2196F3
    success: 'rgba(102,187,106,.85)', // #66BB6A
    info: 'rgba(0,188,212,.85)', // #00BCD4
    infoAlt: 'rgba(126,87,194,.85)', // #7E57C2
    warning: 'rgba(255,202,40,.85)', // #FFCA28
    danger: 'rgba(233,75,59,.85)', // #E94B3B
    gray: 'rgba(221,221,221,.3)',
    textColor: '#989898',
    splitLineColor: 'rgba(0,0,0,.05)',
    splitAreaColor: ['rgba(250,250,250,0.035)', 'rgba(200,200,200,0.1)'],
}

export const APPCONFIG = makeAppConfig();