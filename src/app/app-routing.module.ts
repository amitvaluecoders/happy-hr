import { RouterModule, Routes } from '@angular/router';

import { LayoutComponent } from './layout/layout.component';


const AppRoutes: Routes = [
    
    { path: '', redirectTo: '/user/login', pathMatch: 'full' },     
    { path: 'app',loadChildren: './layout/layout.module#LayoutModule'},
    { path: 'user', loadChildren: './user/user.module#UserModule' },    
    { path: 'extra', loadChildren: './extra-pages/extra-pages.module#ExtraPagesModule' },
    { path: '**', redirectTo: '/extra/404' },
    
];

export const AppRoutingModule = RouterModule.forRoot(AppRoutes, {useHash: true});
