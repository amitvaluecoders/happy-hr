import { Routes, RouterModule }  from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import { SuperAdminGuardService  } from './super_admin-guard.service';
import { AwardsGuardService } from './awards/awards-guard.service';
export const SuperAdminPagesRoutes: Routes = [
    {
        path: '',
        canActivate:[SuperAdminGuardService],
        children: [ 
           
            { path: '', loadChildren: './dashboard/dashboard.module#DashboardModule'},
            { path: 'country', loadChildren: './select-country/select-country.module#SelectCountryModule' },
            { path: 'checklist', loadChildren: './checklist-management/checklist-management.module#ChecklistManagementModule' },
            { path: 'company', loadChildren: './company/company.module#CompanyModule' },
            { path: 'email-template', loadChildren: './email-template/email-template.module#EmailTemplateModule' },
            { path: 'user-access', loadChildren: './user-access/user-access.module#UserAccessModule'},
            { path: 'position-description', loadChildren: './position-description/position-description.module#PositionDescriptionModule' },
            { path: 'feedback-management', loadChildren: './feedback-management/feedback-management.module#FeedbackManagementModule'},
            { path: 'ohs', loadChildren: './ohs/ohs.module#OHSModule'},
            { path: 'site-setting', loadChildren: './site-setting/site-setting.module#SiteSettingModule'},
            { path: 'policy', loadChildren: './policy-document-management/policy-document-management.module#PolicyDocumentManagementModule'},
            { path: 'contract', loadChildren: './contracts/super-contracts.module#ContractsModule'},
            { path: 'letters', loadChildren: './letters/letters.module#LettersModule'},
            { path: 'exit-survey', loadChildren: './exit-survey/exit-survey.module#ExitSurveyModule'},
            { path: 'discount-coupon-management', loadChildren: './discount-coupon-management/discount-coupon-management.module#DiscountCouponManagementModule'},
            { path: 'message', loadChildren: './message/message.module#MessageModule'},
            { path: 'broadcast', loadChildren: './broadcast/broadcast.module#BroadcastModule'},
            { path: 'survey', loadChildren: './survey-questionnare/survey-questionnare.module#SurveyQuestionnareModule'},
            { path: 'subscription-plans', loadChildren: './subscription-plans/subscription-plans.module#SubscriptionPlansModule'},
            { path: 'trophy', loadChildren: './trophy/trophy.module#TrophyModule'},
            { path: 'awards', loadChildren: './awards/awards.module#AwardsModule'},
            { path: 'reference-check', loadChildren: './reference-check/reference-check.module#ReferenceCheckModule'},
            { path: 'interview-guide', loadChildren: './interview-guide/interview-guide.module#InterviewGuideModule'},
            { path: 'jobs-component', loadChildren: './jobs-component/jobs-component.module#JobsComponentModule'},
            { path: 'reports', loadChildren: './reports/reports.module#ReportsModule'},
            { path: 'account-setting', loadChildren: './account-setting/account-setting.module#AccountSettingModule'},
            { path: 'setting', loadChildren: '../company-admin/settings/settings.module#SettingsModule' },
        ]
    }
];

export const SuperAdminPagesRoutingModule = RouterModule.forChild(SuperAdminPagesRoutes);
