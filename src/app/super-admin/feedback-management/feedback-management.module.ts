import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FeedbackListingComponent,FeedbackDetailDialog } from './feedback-listing/feedback-listing.component';
import { FeedbackPagesRoutingModule } from './feedback-routing.module';
import { MaterialModule } from '@angular/material';
import {DataTableModule} from "angular2-datatable";
import { FormsModule } from '@angular/forms';
import { FeedbackService } from './feedback.service';
import { SharedModule } from '../../shared/shared.module';
import { FeedbackGuardService } from './feedback-guard.service';


@NgModule({
  imports: [
    FormsModule,
    DataTableModule,
    MaterialModule,
    FeedbackPagesRoutingModule,
    CommonModule,
    SharedModule
  ],
  providers:[FeedbackService,FeedbackGuardService],
  entryComponents:[FeedbackDetailDialog],
  declarations: [FeedbackListingComponent ,FeedbackDetailDialog]
})
export class FeedbackManagementModule { }

