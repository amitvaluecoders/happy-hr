import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { MdSnackBar, MdDialog, MdDialogRef } from '@angular/material';
import { FeedbackService } from '../feedback.service';
import { CoreService } from '../../../shared/services/core.service';

@Component({
  selector: 'app-feedback-listing',
  templateUrl: './feedback-listing.component.html',
  styleUrls: ['./feedback-listing.component.scss']
})


export class FeedbackListingComponent implements OnInit {

public companyStatus={
  Active:false,
  Inactive:false,
}
public companyType={
  subCompany:false,
  company:false,
}
public feedbackType={
  Happy:false,
  Average:false,
  Unhappy:false
}
public company={
  feedbackID:null,
  companyName:"",
  companyType:"",
  userName:"",
  date:"",
  time:"",
  feedback:"",
  note:"",
  status:"Active"
};

public filter={
  status:{},
  companyType:{},
  feedback:{}
}
public status=[];
public type=[];
public feedback=[];
public isProcessing=false;
public isDataProcessing=false;
public feedbacks=[];
permission;

constructor(private router: Router,  public dialog: MdDialog,private feedbackService:FeedbackService,public coreService:CoreService) { }

  ngOnInit() {
      this.status=Object.keys(this.companyStatus);
        for(let i of this.status){
            this.filter.status[i]=false;
          }
      this.type=Object.keys(this.companyType);
      for(let i of this.type){
            this.filter.companyType[i]=false;
          }
      this.feedback=Object.keys(this.feedbackType);
      for(let i of this.feedback){
            this.filter.feedback[i]=false;
          }
      this.getAllFeedbacks();
      this.permission=this.coreService.getNonRoutePermission();
  }

// get all feedbacks .
  getAllFeedbacks() {
    this.isProcessing=true;
    let reqBody=JSON.stringify({filter:this.filter})
    this.feedbackService.getFeedbacks(reqBody).subscribe(
      d => {
            if(d.code=="200"){
              this.feedbacks=d.data;
              this.isProcessing=false;
            }
        },
        e=>{
              this.isProcessing=false;          
          this.coreService.notify("Unsuccessful","Error while getting the data",0);
        }
    );
  }

 // filter feedback list
 onFilter(){
   this.getAllFeedbacks();
 }

  // opens dialog box which shows feedback details
  onClick(feedbackID) {
    this.isDataProcessing=true;    
    let feedback;
    let dialogRef = this.dialog.open(FeedbackDetailDialog, { width: '800px' });
    dialogRef.componentInstance.isDataProcessing = this.isDataProcessing; 
    this.feedbackService.getFeedbackDetails(feedbackID).subscribe(
      d => {
            if(d.code=="200"){
              feedback=d.data;
              this.isDataProcessing=false;
               dialogRef.componentInstance.feedback = feedback;
               dialogRef.componentInstance.isDataProcessing = this.isDataProcessing;                
            }
        },
        e=>{
              this.isDataProcessing=false; 
               dialogRef.componentInstance.isDataProcessing = this.isDataProcessing;                                               
          this.coreService.notify("Unsuccessful","Error while getting the data",0);
        }
    );
  
    dialogRef.afterClosed().subscribe(result => {
      this.getAllFeedbacks();
    });
  }
  
  // change status of feedback
  statusChange(feedback) {
    let s = (feedback.status == 'Active') ? 'Inactive' : 'Active'
    let reqBody={
      feedbackID:feedback.feedbackID,
      status:s
}
    this.feedbackService.updateStatus(reqBody).subscribe(
      data => {
        this.feedback["status"] = s;
        this.coreService.notify("Successful", data.message, 1);
        this.getAllFeedbacks();
      },
      error => {
        this.coreService.notify("Unsuccessful", error.message, 0)
      }
    )
  }
}



@Component({
    selector: 'feedback-detail',
    template: `
    <md-progress-spinner *ngIf="isDataProcessing" mode="indeterminate" color=""></md-progress-spinner> 
    <ng-container *ngIf="!isDataProcessing">
    <div md-dialog-content style="padding-bottom: 20px;">
        <div class="feedback_status">
        <span *ngIf="feedback.feedback=='Happy'"><i class="material-icons happy">sentiment_very_satisfied</i></span>
                        <span *ngIf="feedback.feedback=='Average'"><i class="material-icons">sentiment_neutral</i></span>
                        <span *ngIf="feedback.feedback=='Unhappy'"><i class="material-icons unhappy">sentiment_very_dissatisfied</i></span>
        </div>
        <div class="feedbackDtl">
            <div class="listRow">
                <label>Company name</label>
                <div class="datacol">{{feedback?.companyName}}</div>
            </div>
            <div class="listRow">
                <label>Company type</label>
                <div class="datacol">{{feedback?.companyType}}</div>
            </div>
            <div class="listRow">
                <label>Admin name</label>
                <div class="datacol">{{feedback?.adminName}}</div>
            </div>
            <div class="listRow">
                <label>User ID</label>
                <div class="datacol">{{feedback?.userID}}</div>
            </div>
            <div class="listRow">
                <label>Date & time</label>
                <div class="datacol">{{feedback?.feedbackTime}}</div>
            </div>
            <div class="listRow">
                <label>Admin email ID</label>
                <div class="datacol">{{feedback?.adminEmail}}</div>
            </div>
            <div class="listRow">
                <label>Website</label>
                <div class="datacol">{{feedback?.website||'http://happyhr.com'}}</div>
            </div>
            <div class="listRow">
                <label>Note</label>
                <div class="datacol">{{feedback?.note}}</div>
            </div>
        </div>
    
</div>
<md-progress-spinner *ngIf="isProcessing" mode="indeterminate" color=""></md-progress-spinner> 

<div md-dialog-actions class="text-center" *ngIf="!isProcessing">
    <button md-raised-button color="primary" (click)="statusChange(feedback)">Broadcast</button>
    <button disabled md-raised-button color="{{color}}" >{{feedback?.status}}</button>
</div>
</ng-container>`,
        styles:[`

          .feedback_status{
            position:absolute;
            top:5%;
            right:5%;
            
          }
          .material-icons{
                height:100px;
                width:100px;
                font-size:100px;
            }
            .happy{
              color:#4cdaaa;
            }
            .unhappy{
              color:#ff8383;
            }
            md-progress-spinner {
    height: 40px !important;
    width: 40px !important;
    margin: 0 auto !important;
    margin-top: 20px;
}
        
        `]
})
export class FeedbackDetailDialog implements OnInit{
 
 public feedback;
 public isDataProcessing;
 color='';
 status= "";
    constructor(public dialogRef: MdDialogRef<FeedbackDetailDialog>,public feedbackService:FeedbackService,public coreService:CoreService) {
    }
    isProcessing=false;
    
    ngOnInit(){
      let interval=setInterval(()=>{
        if(this.feedback){
          this.status=this.feedback["status"];
      this.color=(this.feedback.status == 'Active') ? 'primary' : 'default';
      clearInterval(interval);
        }
      },1000);
    }

    onBroadcast(){}
    
    // change status of feedback
    statusChange(feedback) {
      this.isProcessing=true;
      let s = (feedback.status == 'Active') ? 'Inactive' : 'Active'
      let reqBody={
        feedbackID:this.feedback.feedbackID,
        status:s
       }
      this.feedbackService.updateStatus(reqBody).subscribe(
        data => {
          this.feedback.status = s;
          this.color = (this.feedback.status == 'Active') ? 'primary' : 'default';
          this.isProcessing=false;
          this.dialogRef.close();
          this.coreService.notify("Successful", data.message, 1);
          
        },
        error => {
          this.isProcessing=false;          
          this.coreService.notify("Unsuccessful", error.message, 0)
        }
      )
    }    
}




