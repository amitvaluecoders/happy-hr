import { Routes, RouterModule }  from '@angular/router';
import { FeedbackListingComponent } from './feedback-listing/feedback-listing.component';
import { FeedbackGuardService } from './feedback-guard.service';

export const FeedbackPagesRoutes: Routes = [
    {
        path: '',
        children: [
            { path: '', component: FeedbackListingComponent, canActivate:[FeedbackGuardService] },
         
        ]
    }
];

export const FeedbackPagesRoutingModule = RouterModule.forChild(FeedbackPagesRoutes);
