import { Injectable } from '@angular/core';
import { RestfulwebService } from '../../shared/services/restfulweb.service'
import { RestfulLaravelService } from '../../shared/services/restful-laravel.service'

@Injectable()
export class FeedbackService {

  constructor(
    private restfulWebService: RestfulwebService, 
    private restfulLaravelService: RestfulLaravelService,     
  ){}

   getFeedbacks(reqBody){
    return this.restfulLaravelService.get(`admin/feedback-management/${reqBody}`);     
  }

  getFeedbackDetails(id){
    return this.restfulLaravelService.get(`admin/get-feedback-detail/${id}`);         
  }

   broadcastFeedback(reqBody){
    return this.restfulWebService.post(`broadcastfeedback`,reqBody);     
  }

  updateStatus(reqBody){
    return this.restfulLaravelService.put(`admin/update-feedback-status`,reqBody);     
    
  }

}