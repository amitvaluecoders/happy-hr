export interface Contract{
     
    contractID?:String,
    category?:String,
    contracttype:[{
    contractTypeID:String,
    title:String  
  }],
    version?:String,
    type?:String,
    title?:String,
    description?:String,
    status?:String,
    contractSubjects?:[{
      subject?:String,
      description?:String,
    }]  
    insertTime?:String, 
    updateTime?:String

}