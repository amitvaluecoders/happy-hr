import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { ContractService } from '../contracts.service';
import { ActivatedRoute,Route,Router } from '@angular/router';
import { Contract } from '../contract';
import { CoreService } from '../../../shared/services/core.service';
import { ConfirmService } from '../../../shared/services/confirm.service';

@Component({
  selector: 'app-new-contract',
  templateUrl: './new-contract.component.html',
  styleUrls: ['./new-contract.component.scss']
})
export class NewContractComponent implements OnInit {
  
 public contracttype:[{
    contractTypeID:null,
    title:''  
  }]
  public version="1.0";
  public contract={
    contractID:'',
    contractImage:'',
    contractTypeID:'',
    title:'',
    description:'',
    contractSubjects:[{
      contractSubjectID:'',
      title:'',
      description:'',
    }]   
  };

  public collapsebox=[];

  public head="Create new contract";
  public buttonText="Add";
  public isProcessing=false;
  public isButtonClicked=false;
  public status="Active";
  public id;
  public color="primary";

  constructor(private contractService:ContractService,
              public activatedRoute:ActivatedRoute,
              public coreService:CoreService,
              public router:Router,
              private confirmService:ConfirmService, 
              private viewContainerRef:ViewContainerRef) { }

  ngOnInit() {
    this.getContractTypes();
    this.id=this.activatedRoute.snapshot.params['id'];
    if(this.id) {
      this.head="Edit contract";
      this.buttonText="Save";
      this.getContractDetails(this.id);}
  }
  
  // get latest contract version 
  getLatestContractVersion(){
    this.contractService.getLatestContractVersion().subscribe(
      d => {
        if (d.code == "200") {      
        }
      },
      e => this.coreService.notify("Unsuccessful",e.message,0)
    )
  }

  // get contract types
  getContractTypes(){
    this.isProcessing=true;
    this.contractService.getContractTypes().subscribe(
      d => {
        if (d.code == "200") {
          this.contracttype=d.data;
          console.log("contract type",this.contracttype);
          if (!this.id) this.isProcessing=false; 
        }
        else {this.isProcessing=false;
          this.coreService.notify("Unsuccessful",d.message,0); 
      }},
      e => {this.isProcessing=false;
        this.coreService.notify("Unsuccessful",e.message,0)
      })
  }

 // add contract subject 
  addSubject(){
  this.contract.contractSubjects.push({contractSubjectID:'',title:"",description:""}); 
  }

  // delete contract subject
  deleteSubject(subject){
    this.confirmService.confirm(this.confirmService.tilte,this.confirmService.message,this.viewContainerRef)
    .subscribe(res=>{
       let result=res;
    if(result){
    this.contract.contractSubjects.splice(subject,1);
    }})
  }

 // get contract details
  getContractDetails(id) {
    this.contractService.getContractDetails( id).subscribe(
      d => {
        if (d.code == "200") {
          this.contract=d.data;
          this.status=d.data.status;
          this.version=this.contract["version"];
          this.contract.contractTypeID=d.data.contracttype.contractTypeID;
          this.color=(this.status == 'Active') ? 'primary' : 'default';
          this.isProcessing=false;
        }
        else {
          this.isProcessing=false;
          this.coreService.notify("Unsuccessful",d.message,0); 
      }},
      e => {this.isProcessing=false;
        this.coreService.notify("Unsuccessful",e.message,0)
      })
  }
  
  // add or update contract 
  addEditContract(isValid){
    this.isButtonClicked=true;
    if(isValid){  
      this.contractService.addEditContract(this.contract).subscribe(
        d=>{
          if(d.code=="200"){
            this.coreService.notify("Successful",d.message,1);
            this.router.navigate(['/app/super-admin/contract']);
          }
          else {
            this.isButtonClicked=false;
            this.coreService.notify("Unsuccessful",d.message,0);
          }            
        },
        e=>{
          this.isButtonClicked=false;
          this.coreService.notify("Unsuccessful",e.message,0)
        }
         )     
      }
  }

  onChangeStatus(){
  }

  // navigates to contract list page
  onCancel(){
     this.router.navigate([`/app/super-admin/contract`]);
  }
 
 // change sattus of contract
  statusChange(status) {
    let s = (status == 'Active') ? 'Inactive' : 'Active'
    let reqBody={
      contractID:this.contract.contractID,
      status:s
}
    this.contractService.updateStatus(reqBody).subscribe(
      data => {
        this.status = s;
        this.color = (this.status == 'Active') ? 'primary' : 'default';
        this.coreService.notify("Successful", data.message, 1)
      },
      error => {
        this.coreService.notify("Unsuccessful", error.message, 0)
      }
    )
  }
}