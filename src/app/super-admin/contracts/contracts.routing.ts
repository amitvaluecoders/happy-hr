import { Routes, RouterModule }  from '@angular/router';
import { NewContractComponent }  from './new-contract/new-contract.component';
import { ContractDetailComponent }  from './contract-detail/contract-detail.component';
import { ContractListingComponent }  from './contract-listing/contract-listing.component';
import { ContractVersionComponent }  from './contract-version/contract-version.component';
import { ContractsGuardService }  from './contracts-guard.service';

export const ContractsPagesRoutes: Routes = [
    {
        path: '',
        children: [
            { path: '', component: ContractListingComponent,canActivate:[ContractsGuardService] },
            { path: 'add', component: NewContractComponent,canActivate:[ContractsGuardService] },
            { path: 'edit/:id', component: NewContractComponent,canActivate:[ContractsGuardService] },
            { path: 'detail/:id', component: ContractDetailComponent,canActivate:[ContractsGuardService] },
            { path: 'version/:id', component: ContractVersionComponent,canActivate:[ContractsGuardService] },
       ]
    }
];

export const ContractsPagesRoutingModule = RouterModule.forChild(ContractsPagesRoutes);