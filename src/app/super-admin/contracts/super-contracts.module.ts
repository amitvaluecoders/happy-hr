import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from '@angular/material';
import {DataTableModule} from "angular2-datatable";
import { FormsModule } from '@angular/forms'; 
import { NewContractComponent } from './new-contract/new-contract.component';
import { ContractDetailComponent } from './contract-detail/contract-detail.component';
import { ContractListingComponent } from './contract-listing/contract-listing.component';
import { ContractVersionComponent } from './contract-version/contract-version.component';
import { ContractsPagesRoutingModule } from './contracts.routing';
import { QuillModule } from 'ngx-quill';
import { ContractService } from './contracts.service';
//  import { SearchPipe } from '../../shared/pipes/search.pipe';
import { SharedModule } from '../../shared/shared.module';
import { ContractsGuardService } from './contracts-guard.service';

@NgModule({
  imports: [
    CommonModule,
    ContractsPagesRoutingModule,
    QuillModule,
    MaterialModule,
    DataTableModule,
    FormsModule,
    SharedModule
  ],
  declarations: [NewContractComponent, ContractDetailComponent, ContractListingComponent, ContractVersionComponent],
  providers:[ContractService,ContractsGuardService]
})
export class ContractsModule { }
