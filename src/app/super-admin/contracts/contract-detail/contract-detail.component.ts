import { Component, OnInit } from '@angular/core';
import { ContractService } from '../contracts.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { CoreService } from '../../../shared/services/core.service';
import { Contract } from '../contract';

@Component({
  selector: 'app-contract-detail',
  templateUrl: './contract-detail.component.html',
  styleUrls: ['./contract-detail.component.scss']
})
export class ContractDetailComponent implements OnInit {
  
  public collapsebox=[];
  public opencollapse;
  public status="Active";
  public color="primary";
  public isProcessing=false;
  permission;

  constructor(private contractService:ContractService,public router:Router,private route:ActivatedRoute,public coreService:CoreService) { }

  public contractDetails={};
  ngOnInit() {
    this.getContractDetails();
    this.permission=this.coreService.getNonRoutePermission();  
  }
  
  //get contract details
  getContractDetails(){
    this.isProcessing=true;
    this.contractService.getContractDetails(this.route.snapshot.params['id']).subscribe(
      d=>{
        if(d.code=="200"){
          this.contractDetails=d.data;
          this.status=d.data.status;
          this.color=(this.status == 'Active') ? 'primary' : 'default';
          this.isProcessing=false;
        }
        else {
          this.isProcessing=false;
          this.coreService.notify("Unsuccessful",d.message,0);
        }  
    },
      e=>{
        this.isProcessing=false;        
        this.coreService.notify("Unsuccessful",e.message,0)}
        )        
  }

  // navigates to contrat versions page
  showContractVersions(id){
      this.router.navigate([`/app/super-admin/contract/version/${id}`]);
   } 

  // navigates to edit contract page 
  onEdit(id){
      this.router.navigate([`/app/super-admin/contract/edit/${id}`]);
  }

  // change the status of contract
  statusChange(status) {
    let s = (status == 'Active') ? 'Inactive' : 'Active'
    let reqBody={
      contractID:this.contractDetails["contractID"],
      status:s
}
    this.contractService.updateStatus(reqBody).subscribe(
      data => {
        this.status = s;
        this.color = (this.status == 'Active') ? 'primary' : 'default';
        this.coreService.notify("Successful", data.message, 1)
      },
      error => {
        this.coreService.notify("Unsuccessful", error.message, 0)
      }
    )
  }

}
