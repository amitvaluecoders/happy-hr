import { Injectable } from '@angular/core';
import { RestfulLaravelService } from '../../shared/services/restful-laravel.service'; 
import { RestfulwebService } from '../../shared/services/restfulweb.service'


@Injectable()
export class ContractService {

  constructor(
    private restfulWebService: RestfulwebService,
    private restfulLaravelService:RestfulLaravelService 
  ){}

  getContracts(reqBody){
    return this.restfulLaravelService.get(`admin/list-contract/${encodeURIComponent(reqBody)}`);     
  }

  getContractDetails(reqBody){
    return this.restfulLaravelService.get(`admin/get-contract-detail/${reqBody}`);     
  }

  updateContractStatus(reqBody){
      return this.restfulLaravelService.post('admin/update-contract-status',reqBody)
  }
 
  addEditContract(reqBody){
      return this.restfulLaravelService.post(`admin/add-edit-contract`,reqBody)
  }

  getContractVersions(){
     return this.restfulLaravelService.get('admin/list-contract-version-number')
  }

  getLatestContractVersion(){
     return this.restfulLaravelService.get(`latestContractVersion`)    
  }

  getContractAllVersions(id){
    return this.restfulLaravelService.get(`admin/get-contract-version/${id}`)
  }

  getContractTypes(){
    return this.restfulLaravelService.get('admin/list-contract-type');
  }

  restoreContractVersion(id){
    return this.restfulLaravelService.get(`admin/restore-contract-version/${id}`)
  }

  updateStatus(policy){
    return this.restfulLaravelService.put('admin/update-contract-status',policy);
  }

}