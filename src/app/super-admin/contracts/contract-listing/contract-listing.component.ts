import { Component, OnInit ,Input} from '@angular/core';
import { ContractService } from '../contracts.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Contract } from '../contract'
import {Observable} from "rxjs/Rx";
import { CoreService } from '../../../shared/services/core.service';


@Component({
  selector: 'app-contract-listing',
  templateUrl: './contract-listing.component.html',
  styleUrls: ['./contract-listing.component.scss']
})
export class ContractListingComponent implements OnInit {
  
  public toggleFilter:boolean;
  constructor(private contractService:ContractService,private router: Router,public coreService:CoreService) {
   }

  public contracts:Contract[];
  public searchKeyPdType='';
  public searchKeyStatusType='';
  public searchKeyType='';
  public searchKeyVersion='';
  public isProcessing=true;

  public searchKey='';
  public limit="10";
  public offset="0-9"

  public filter = {
      category:{
        HHR:false, 
        Own:false, 
      },
      contractTypeIDs:{},
      status:{
        Active:false, 
        Inactive:false
      },
      version:{}
    }
    
    public category = [{title:"Happy HR",value:"HHR"},{title:"Own",value:"Own"}];
    public status = []; 
    public type = [];
    public versions = [];
    public differentContractsData=[];
    public isFiltering=false;
    public cancelRequest;
    public contractData=[];
    permission;
    
  ngOnInit() { 
    this.toggleFilter=false;
    this.getContractType();
    this.permission=this.coreService.getNonRoutePermission();
  }
  
  // filter
  onFilterChange(){
    this.getContractListing();
  }

  // get contract list
  getContractListing(){
    this.isFiltering=true;
    this.differentContractsData=[];
   let searchText=encodeURIComponent(this.searchKey);  
    let reqBody={search:searchText,filter:this.filter};
    this.contractData=[];
    this.cancelRequest=this.contractService.getContracts(JSON.stringify(reqBody)).subscribe(
      d =>{      
        if(d.code=="200"){
          this.contracts=d.data;
          for (let t of this.category) {
              for (let d of this.contracts) {
                if (d.category==t.value ){
                this.contractData.push(d);             
                this.differentContractsData[t.title]=this.contractData; 
              }
              }
              this.contractData=[];
            } 
          this.isFiltering=false;      
          this.isProcessing=false;
        }
        else {
          this.isFiltering=false;      
          this.isProcessing=false;
          this.coreService.notify("Unsuccessful","Error while getting contract list",0);
        }
      },
      e=>{
        this.isFiltering=false;      
        this.isProcessing=false;
        this.coreService.notify("Unsuccessful",e.message,0)}
      )
  }
  

  onSerchKeyChange(){
    this.cancelRequest.unsubscribe();
    this.getContractListing(); 
  }
  
  //get contracts versions
  getContractVersions(){
    this.contractService.getContractVersions().subscribe(
      d =>{
        if(d.code=="200"){      
          for(let c of d.data){
            this.filter.version[c.version]=false;
          }
          this.status = Object.keys(this.filter.status);
          this.versions = Object.keys(this.filter.version);
          this.getContractListing();
        } 
        else this.coreService.notify("Unsuccessful","error while getting contract versions",0); 
      },
      e=>this.coreService.notify("Unsuccessful",e.message,0)
      )
  }

  // get contract types
  getContractType(){
    this.contractService.getContractTypes().subscribe(
      d =>{
        if(d.code=="200"){
         for(let c of d.data){
            this.filter.contractTypeIDs[c.contractTypeID]=false;
          }
          this.type = d.data;
        this.getContractVersions();
         }
        else this.coreService.notify("Unsuccessful","error while getting contract types",0); 
      },
      e=>this.coreService.notify("Unsuccessful",e.message,0)
      )
  }
  
  // navigates to contract details page
  onClick(contractId){
      this.router.navigate([`/app/super-admin/contract/detail/${contractId}`]);
  }
}
