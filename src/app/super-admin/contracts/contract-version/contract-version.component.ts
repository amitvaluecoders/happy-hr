import { Component, OnInit } from '@angular/core';
import { ContractService } from '../contracts.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Contract } from '../contract';
import { CoreService } from '../../../shared/services/core.service';


@Component({
  selector: 'app-contract-version',
  templateUrl: './contract-version.component.html',
  styleUrls: ['./contract-version.component.scss']
})
export class ContractVersionComponent implements OnInit {

  public collapsebox=[];
  public opencollapse;
  
  public contractVersions:Contract[];
  public isProcessing:Boolean=false;
  public id;
  permission;
  constructor(public contractService:ContractService,public coreService:CoreService,public activatedRoute:ActivatedRoute) { }

  ngOnInit() {
     if (this.activatedRoute.snapshot.params['id'] != undefined) {
    this.id = this.activatedRoute.snapshot.params['id'];
    this.getContractVersions();
    this.permission=this.coreService.getNonRoutePermission();
  }  
  }
 
  // get contract versions
  getContractVersions(){
    this.isProcessing=true;
    this.contractService.getContractAllVersions(this.id).subscribe( 
      d=>{
        if(d.code=="200"){
          this.contractVersions=d.data;
          this.isProcessing=false;
        }
        else {this.isProcessing=false;
          this.coreService.notify("Unsuccessful","Error while getting contact versions",0);
        }
      },
      e=>{this.isProcessing=false;
        this.coreService.notify("Unsuccessful",e.message,0);        
      })
  }

  // restores the contract version to previous version
  onRestore(id){
    this.isProcessing=true;
    this.contractService.restoreContractVersion(id).subscribe( 
      d=>{
        if(d.code=="200"){
           this.getContractVersions();
          this.coreService.notify("Successful",d.message,1);
          this.isProcessing=false;
        }
        else {
          this.isProcessing=false;          
          this.coreService.notify("Unsuccessful",d.message,1);
        }
      },
      e=>{
          this.isProcessing=false;        
        this.coreService.notify("Unsuccessful","Already restored to this version",2);        
      })
  }

}
