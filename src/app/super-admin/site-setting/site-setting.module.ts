import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from '@angular/material';
import { FormsModule } from '@angular/forms';
import { SettingComponent } from './setting/setting.component';
import { SiteSettingPagesRoutingModule } from './site-setting-routing.module';
import { SiteSettingService }  from './site-setting.service';
import { SiteSettingGuardService } from './site-setting-guard.service';

@NgModule({
  imports: [
    FormsModule,
    CommonModule,
    SiteSettingPagesRoutingModule,
    MaterialModule
  ],
  declarations: [SettingComponent],
providers:[SiteSettingService,SiteSettingGuardService]

})
export class SiteSettingModule { }
