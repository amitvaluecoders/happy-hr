import { Component, OnInit } from '@angular/core';
import { SiteSettingService } from '../site-setting.service';
import { CoreService } from '../../../shared/services/core.service';

@Component({
  selector: 'app-setting',
  templateUrl: './setting.component.html',
  styleUrls: ['./setting.component.scss']
})
export class SettingComponent implements OnInit {
  public isAddButtonClicked=false;
  public editSiteSetting=false;
  public isProcessing=false;
   siteSetting={
   siteMode:'' ,
   siteModeOfflineMessage:'',
   }
  oldMessage='';
  oldSiteMode='';
  permission;
  public submitProcessing=false;
  constructor(public coreService:CoreService,public siteSettingService:SiteSettingService) { }

  ngOnInit() {
    this.getSiteSettingDetails(); 
    this.permission=this.coreService.getNonRoutePermission();
  }

  // get site setting details
  getSiteSettingDetails(){
    this.isProcessing=true;
    this.siteSettingService.getSiteSettingDetails().subscribe(
      d=>{
        if(d.code=="200") {
          this.oldMessage=d.data.siteModeOfflineMessage;
          this.oldSiteMode=d.data.siteMode;
          this.isProcessing=false;
          this.siteSetting=d.data;
        }
        else {
          this.isProcessing=false;
          this.coreService.notify("Unsuccessful",d.message,0)
        }
      },
      e=>{this.isProcessing=false;
        this.coreService.notify("Unsuccessful",e.message,0)}) 
  }

  // saves site setting
  onSave(){
    this.submitProcessing=true;
    this.isAddButtonClicked=true;
    this.siteSettingService.setSiteSettingDetails(this.siteSetting).subscribe(
      d=>{
        if(d.code=="200") {
          this.siteSetting=d.data;
          this.getSiteSettingDetails(); 
          this.editSiteSetting=false;
          this.submitProcessing=false;
          this.coreService.notify("Successful",d.message,1);
        }
        else {this.submitProcessing=false;this.coreService.notify("Unsuccessful","Error while saving site setting details !",0)}
      },
      e=>{this.submitProcessing=false;this.coreService.notify("Unsuccessful","Error while saving site setting details !",0)})    
  }

}
