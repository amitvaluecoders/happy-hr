
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule }  from '@angular/router';
import { SettingComponent } from './setting/setting.component';
import { SiteSettingGuardService } from './site-setting-guard.service';

export const SiteSettingPagesRoutes: Routes = [
    {
        path: '',
        children: [
            { path: '',  component: SettingComponent,canActivate:[SiteSettingGuardService] }
        ]
    }
];

export const SiteSettingPagesRoutingModule = RouterModule.forChild(SiteSettingPagesRoutes);
