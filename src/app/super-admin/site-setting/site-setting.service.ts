import { Injectable } from '@angular/core';
import { RestfulwebService} from '../../shared/services/restfulweb.service';
import { RestfulLaravelService } from '../../shared/services/restful-laravel.service';

@Injectable()
export class SiteSettingService {

  constructor(public restfulWebService:RestfulwebService,public restfulLaravelService:RestfulLaravelService) { }

  getSiteSettingDetails(){
   return this.restfulLaravelService.get('admin/get-site-setting')
  }

  setSiteSettingDetails(reqBody){
    return this.restfulLaravelService.post('admin/site-setting',reqBody);
  }

}
