import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from '@angular/material';
import { OHSFormComponent } from './ohs-form/ohs-form.component';
import { Routes, RouterModule }  from '@angular/router';
import { OHSFormPagesRoutingModule } from './ohs-routing.module';
import { FormsModule } from '@angular/forms';
import  { SharedModule} from '../../shared/shared.module'; 
import { InlineEditorModule} from 'ng2-inline-editor';
import { OhsService } from './ohs.service'
import { OhsGuardService } from './ohs-guard.service';


@NgModule({
  imports: [
    InlineEditorModule,
    SharedModule,
    FormsModule,
    CommonModule,
    OHSFormPagesRoutingModule,
    MaterialModule
  ],
  declarations: [OHSFormComponent],
  providers:[OhsService,OhsGuardService]
})
export class OHSModule { }
