
import { Injectable } from '@angular/core';
import { RestfulwebService} from '../../shared/services/restfulweb.service';
import { RestfulLaravelService } from '../../shared/services/restful-laravel.service';

@Injectable()
export class OhsService {
  constructor(public restfulWebService:RestfulwebService,
              public restfulLaravelService:RestfulLaravelService) { }

getOHSList():any{
return this.restfulLaravelService.get('admin/ohs-get')
}

// getPolicy(reqBody):any{
// return this.restfulLaravelService.get(`admin/get-policy-document-detail/${reqBody}`)
// }

// deletePolicy(policy):any{
// return this.restfulWebService.delete(`delete/${policy}`);
// }

editOHSList(request):any{
return this.restfulLaravelService.post('admin/create-ohs',request);
}


// updateStatus(policy){
// return this.restfulWebService.put('updateStatus',policy);
// }

// getPolicyTypes(){
// return this.restfulLaravelService.get('admin/list-policy-document-type')
// }

// getPolicyVersion(){
// return this.restfulLaravelService.get('admin/list-policy-document-version-number')
// }

// getPolicyVersions(reqBody){
// return this.restfulLaravelService.get(`admin/get-policy-document-version/${reqBody}`)
// }

// restorePolicyVersion(reqBody){
// return this.restfulLaravelService.get(`admin/restore-policy-document-version/${reqBody}`)
// }

}