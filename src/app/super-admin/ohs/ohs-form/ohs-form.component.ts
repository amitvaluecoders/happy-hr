import { Component, OnInit } from '@angular/core';
import { OhsService } from '../ohs.service';
import { InlineEditorComponent } from 'ng2-inline-editor';
import { CoreService } from '../../../shared/services/core.service';

@Component({
  selector: 'app-ohs-form',
  templateUrl: './ohs-form.component.html',
  styleUrls: ['./ohs-form.component.scss']
})
export class OHSFormComponent implements OnInit {
  
  public username="abc";
  // public ohs={
  //   part1:{
  //     heading:"Type of issue being reported",
  //     field1:"unselected",
  //     field2:"unselected",
  //     field3:"unselected",
  //     field4:"unselected",
  //     field5:"unselected",
  //     field6:"unselected",
  //     field7:"unselected"
  //   },
  //   part2:{
  //     heading:"When and where did it happen",
  //     field1:"Date",
  //     field2:"Location",
  //     field3:"Time",
  //   },
  //    part3:{
  //     heading:"Person Involved",
  //     field1:"Reported By",
  //     field2:"Signature",
  //     field3:"Date",
  //     field4:"Reported To",
  //     field5:"Signature",
  //     field6:"Date",
  //     field7:"Involved/injured",
  //     field8:"Signature",
  //     field9:"Date",
  //     field10:"Witness",
  //     field11:"Signature",
  //     field12:"Date"
  //   },
  //   part4:{
  //     heading:"Discribe what happened/What is being reported",
  //   },
  //    part5:{
  //     heading:"Injury details",
  //     field1:"Name of Injury",
  //     field2:"First Aid Treatment Provided",
  //     field3:{
  //       heading:"Further Action Taken",
  //       subfield1:"Employee Resumed Duties After Treatment",
  //       subfield2:"Employee Taken To Doctor by 'Name'",
  //       subfield3:"Employee Send Home",
  //       subfield4:"Ambulance Called",
  //       subfield5:"Employee Reffered to Doctor",
  //       subfield6:"other (Please Specifys)"
  //     },
  //     field4:"No. of Works Hours Losts",
  //     field5:"No. of First Aid Officer",
  //     field6:"Signature Of First Aid Box Officer",
  //     field7:"Date"
  //   },
  //   part6:{
  //     heading:"To be completed by the Team Leader or Superior",
  //     field1:{
  //       heading:"Name of Injury"
  //     },
  //     field2:{
  //       heading:"If Matter Related to an injury requiring further treatment, a breach of condition of employement and suits rule or is otherwise serious. 'Shan Manickam' adviced immediately.",
  //       subfield1:"In Person",
  //       subfield2:"By Phone",
  //       subfield3:"By Email",
  //       subfield4:"Date Adviced",
  //     },
  //     field3:{
  //       heading:"If there is a serious notable incident (eg. Injury Requiring Hospitalization)",
  //       subfield1:"Accident Site Preserved",
  //       subfield2:"Work Space Notified on____________By___________",
  //     },
  //     field4:{
  //       heading:" List of Actions taken to prevent a similar occurence in future",
  //       subfield1:"Date Followup Action Completed",
  //       subfield2:"Work Cover Claim from lodged?",
  //       subfield3:"Supervisor Name",
  //       subfield4:"Signature",
  //       subfield5:"Date",
  //     }
  //   },
  //   part7:{
  //     heading:"Signed in Acknowledgement of Recipt of Claim",
  //     field1:"Supervisor Name",
  //     field2:"Signature",
  //     field3:"Date",
  //   },
  // }

  public isProcessing=false;
  public part1Processing=false;
  public part2Processing=false;
  public part3Processing=false;
  public part4Processing=false;
  public part5Processing=false;
  public part6Processing=false;
  public part7Processing=false;  
  

    ngOnInit() {
    this.getOHSList();
  }

  public ohs=[];
  oldOhs=[];

  constructor(public ohsService:OhsService,public coreService:CoreService) { }

  getOHSList(){
    this.isProcessing=true;
  this.ohsService.getOHSList().subscribe(
    data => {
      if (data.code == "200") {
         //setTimeout(()=>{
        this.ohs=data.data;    
        this.oldOhs=JSON.parse(JSON.stringify(data.data));
        console.log("OHS::",this.ohs);
        
       // this.listProcessing=false;
        this.isProcessing=false;
       // a.unsubscribe();
       // this.positionDescriptions=data;
      //  },100);
      }
    },
    error => {this.isProcessing=false;this.coreService.notify("Unsuccessful",error.message,0)},
    () => { })
  }

  editOHSList(ohs,ohsData,part){
    console.log("OHS EDITITITITIITTII",ohs);
    if(ohsData.title||(ohsData.data&&ohs.heading)){
   // this.isProcessing=true;
    
    if(part=="part1") this.part1Processing=true;
    if(part=="part2") this.part2Processing=true;
    if(part=="part3") this.part3Processing=true;
    if(part=="part4") this.part4Processing=true;
    if(part=="part5") this.part5Processing=true;
    if(part=="part6") this.part6Processing=true;
    if(part=="part7") this.part7Processing=true;
    
    let reqParams={
      ohsID: ohs.ohsID,
      heading: ohs.heading,
      data: ohs.data
    };
    console.log("req",reqParams)
    this.ohsService.editOHSList(reqParams).subscribe(
      data => {
        if (data.code == "200") {
           //setTimeout(()=>{
         // this.ohs=data.data;
         // console.log("OHS::",this.ohs);
          
         // this.listProcessing=false;
         // this.isProcessing=false;
         if(part=="part1") this.part1Processing=false;
         if(part=="part2") this.part2Processing=false;
         if(part=="part3") this.part3Processing=false;
         if(part=="part4") this.part4Processing=false;
         if(part=="part5") this.part5Processing=false;
         if(part=="part6") this.part6Processing=false;
         if(part=="part7") this.part7Processing=false;
          this.coreService.notify("Successful",data.message,1)
         // a.unsubscribe();
         // this.positionDescriptions=data;
        //  },100);
        }
      },
      error => {
        //this.isProcessing=false;
        if(part=="part1") this.part1Processing=false;
        if(part=="part2") this.part2Processing=false;
        if(part=="part3") this.part3Processing=false;
        if(part=="part4") this.part4Processing=false;
        if(part=="part5") this.part5Processing=false;
        if(part=="part6") this.part6Processing=false;
        if(part=="part7") this.part7Processing=false;
        this.coreService.notify("Unsuccessful",error.message,0)},
      () => { })
  }else{
    if(!ohs.heading){ for(let q of this.oldOhs){
       if(ohs.ohsID==q.ohsID) ohs.heading=q.heading;
        console.log("Q!!!",ohs,"q22222",q)
         this.coreService.notify("Unsuccessful","Heading is required !",0);
     }
    }else{
     for(let o of this.oldOhs){
       if(ohs.ohsID==o.ohsID){
         for(let oo of o.data){
       if(ohsData.pkOhsKeyID==oo.pkOhsKeyID)
         ohsData.title=oo.title;
         this.coreService.notify("Unsuccessful","Field is required !",0);      
     }
       }
     }
     for(let o of this.oldOhs){
       if(ohs.ohsID==o.ohsID){
         for(let oo of o.data){
       if(ohsData.pkOhsKeyID==oo.pkOhsKeyID){
           for(let sub of oo.subfields){
             if(ohsData.pkOhsKeyID==sub.pkOhsKeyID){
             ohsData.title=sub.title;
             this.coreService.notify("Unsuccessful","Field is required !",0);
           }
           }
       }
         //
         //this.coreService.notify("Unsuccessful","Field is required !",0);      
     }
       }
     }
  }
    //alert ("This is a warning message!");
            
  }
  }
  
  onPrint(){
     let printContents, popupWin;
    printContents = document.getElementById('printBox').innerHTML;
    popupWin = window.open();
    popupWin.document.open();
    popupWin.document.write(`
      <html>
        <head>
          <title>Print tab</title>
          <style>
          //........Customized style.......
          </style>
        </head>
    <body onload="window.print();window.close()">${printContents}</body>
      </html>`
    );
    popupWin.document.close();

  }



  title = 'My component!';
 
  editableText = 'myText';
  editablePassword = 'myPassword';
  editableTextArea = 'Text in text area';
  editableSelect = 2;
  editableSelectOptions =[
    {value: 1, text: 'status1'},
    {value: 2, text: 'status2'},
    {value: 3, text: 'status3'},
    {value: 4, text: 'status4'}
  ];
 
  
}
