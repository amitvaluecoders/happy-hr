import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OHSFormComponent } from './ohs-form.component';

describe('OHSFormComponent', () => {
  let component: OHSFormComponent;
  let fixture: ComponentFixture<OHSFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OHSFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OHSFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
