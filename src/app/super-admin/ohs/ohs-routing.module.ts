
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule }  from '@angular/router';
import { OHSFormComponent } from './ohs-form/ohs-form.component';
import { OhsGuardService } from './ohs-guard.service';

export const OHSFormPagesRoutes: Routes = [
    {
        path: '',
        children: [
            { path: '',  component: OHSFormComponent,canActivate:[OhsGuardService] },
            //{ path: 'ohs', component: OHSFormComponent },
        ]
    }
];

export const OHSFormPagesRoutingModule = RouterModule.forChild(OHSFormPagesRoutes);
