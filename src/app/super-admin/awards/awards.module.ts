import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from '@angular/material';
import { FormsModule } from '@angular/forms';
import { AwardsListingComponent,AddEditAwardDialog } from './awards-listing/awards-listing.component';
import { AwardsPagesRoutingModule } from './awards.routing';
import { AwardService } from './award.service'
import { AwardsGuardService } from './awards-guard.service';
@NgModule({
  imports: [
    CommonModule,
    MaterialModule,
    FormsModule,
    AwardsPagesRoutingModule
  ],
  declarations: [AwardsListingComponent,AddEditAwardDialog],
  entryComponents:[AddEditAwardDialog],
  providers:[AwardService,AwardsGuardService]
})
export class AwardsModule { }
