import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AwardsListingComponent } from './awards-listing.component';

describe('AwardsListingComponent', () => {
  let component: AwardsListingComponent;
  let fixture: ComponentFixture<AwardsListingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AwardsListingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AwardsListingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
