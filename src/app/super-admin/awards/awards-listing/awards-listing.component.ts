import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { CoreService } from '../../../shared/services/core.service';
import { ConfirmService } from '../../../shared/services/confirm.service';
import { MdDialog, MdDialogRef } from '@angular/material';
import { AwardService } from '../award.service';
@Component({
  selector: "app-awards-listing",
  templateUrl: "./awards-listing.component.html",
  styleUrls: ["./awards-listing.component.scss"]
})
export class AwardsListingComponent implements OnInit {
  public isProcessing = true;
  public filterProcessing = false;
  public awards = {};
  public characters = [];
  public award = {
    title: "",
    awardID: "",
    category: ""
  };
  public first = "A";
  public last = "Z";
  public chars = [];
  public selected = [];
  public filter = { awards: {} };
  public bool = true;

  constructor(
    public dialog: MdDialog,
    public confirmService: ConfirmService,
    public coreService: CoreService,
    public awardService: AwardService,
    public viewContainerRef: ViewContainerRef
  ) { }
  permission;
  ngOnInit() {
    this.getCharacters();
    this.getAwards();
    this.permission = this.coreService.getNonRoutePermission();
  }

  // get characters A to Z
  getCharacters() {
    for (var i = this.first.charCodeAt(0); i <= this.last.charCodeAt(0); i++) {
      this.chars.push(String.fromCharCode(i));
    }
    let a = this.chars;
    for (let i of a) {
      this.filter.awards[i] = false;
      this.selected[i] = false;
    }
  }

  //list the awards
  getAwards() {
    this.characters = [];
    this.filterProcessing = true;
    let reqParams = { filter: this.filter };
    this.awardService.getAwards(JSON.stringify(reqParams)).subscribe(
      data => {
        if (data.code == "200") {
          if (data.data) {
            this.awards = data.data;
            for (let i in data.data) {
              if (data.data[i].length)
                if (this.characters.indexOf(i) == -1) this.characters.push(i);
            }
            this.isProcessing = false;
            this.filterProcessing = false;
          }
        }
      },
      error => {
        this.isProcessing = false;
        this.filterProcessing = false;
        this.coreService.notify(
          "Unsuccessful",
          "Error while getting the data",
          0
        )
      },
      () => { }
    );
  }

   // opens the dialog box to add award
  addAward() {
    this.filter = { awards: {} };
    let dialogRef = this.dialog.open(AddEditAwardDialog, { width: "400px" });
    dialogRef.componentInstance.category = this.chars;
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.isProcessing = true;
        this.award.title = result.title;
        this.award.category = result.category;
        this.awardService.addEditAward(this.award).subscribe(
          data => {
            if (data.code == "200") {
              this.getAwards();
              this.coreService.notify("Successful", "Award added successfully", 1);
            }
          },
          error => {
            this.isProcessing = false;
            this.coreService.notify("Unsuccessful", error.message, 0);
          },
          () => { }
        );
      }
    });
  }

  //opens the dialog box to edit award
  editAward(award, character) {
    this.filter = { awards: {} };
    let dialogRef = this.dialog.open(AddEditAwardDialog, { width: "400px" });
    dialogRef.componentInstance.award = award;
    dialogRef.componentInstance.character = character;
    dialogRef.componentInstance.category = this.chars;
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.isProcessing = true;
        this.awardService.addEditAward(result).subscribe(
          data => {
            if (data.code == "200") {
              this.getAwards();
              this.coreService.notify("Successful", "Award updated successfully", 1);
            }
          },
          error => {
            this.isProcessing = false;
            this.coreService.notify("Unsuccessful", error.message, 0);
          },
          () => { }
        );
      }
    });
  }

  sortArray(json, index) {
    if (json.length - 1 < index + 1) return json;
    if (json[index].title > json[index + 1].title) {
      var tmp = json[index];
      json[index] = json[index + 1];
      json[index + 1] = tmp;
      return this.sortArray(json, index - 1);
    }
    return this.sortArray(json, index + 1);
  }

  //delete award
  deleteAward(awardID, index) {
    this.confirmService
      .confirm(
      this.confirmService.tilte,
      this.confirmService.message,
      this.viewContainerRef
      )
      .subscribe(res => {
        let result = res;
        if (result) {
          this.isProcessing = true;
          this.awardService.deleteAward(awardID).subscribe(
            data => {
              if (data.code == "200") {
                console.log(data);
                this.getAwards();
                this.coreService.notify("Successful", "Award deleted successfully", 1);
              }
            },
            error => {
              this.isProcessing = false;
              this.coreService.notify(
                "Unsuccessful",
                "Error while deleting the data",
                0
              );
            },
            () => { }
          );
        }
      });
  }
  
  //filter award
  onFilter(char, index) {
    this.selected[char] = !this.selected[char];
    this.getAwards();
  }
}

@Component({
  selector: 'add-edit-award-dialog',
  template: `<h1 md-dialog-title>{{title}}</h1>
        <div md-dialog-content style="padding-bottom: 20px;">
         <form #awardForm="ngForm" name="material_signup_form" class="md-form-auth form-validation" >  
          <md-input-container class="full-width">
          <input mdInput  required rows="4" name="award_name" (keyup)="onkeyPress(addEditAward.title)" [(ngModel)]="addEditAward.title" #awardName="ngModel" placeholder="Enter award title">
          </md-input-container> 
          <div *ngIf="awardName.errors && (awardName.dirty || awardName.touched)">
                    <div style="color:red" [hidden]="!awardName.errors.required ">
                    Award title is required !
                    </div>
                </div>
            <md-select style="width:42%;margin-top:10px;" placeholder="Select category" (change)="onChange(addEditAward.category)" [(ngModel)]="addEditAward.category" #cat="ngModel" name="cat" required>
            <md-option *ngFor="let cat of category" value="{{cat}}">{{cat}}</md-option>
        </md-select>
        <div style="margin-top:5%;" *ngIf="cat.errors && (cat.dirty || cat.touched)">
            <div style="color:red" [hidden]="!cat.errors.required">
                Category is required !
            </div>
        </div>
          </form>       
        </div>
        <div md-dialog-actions>
            <button md-raised-button color="primary" [disabled]="disableEdit || awardForm.form.invalid" (click)="onSubmit(awardForm.form.valid);false">{{buttonText}}</button>
            <button md-raised-button color="default" (click)="onCancel();">Cancel</button>            
            </div>`,
})
export class AddEditAwardDialog implements OnInit {
  public award = {
    title: "",
    awardID: "",
    category: ""
  };
  public addEditAward = {
    title: "",
    awardID: "",
    category: ""
  };
  public category;
  public character;
  public oldAwardTitle = '';
  oldAwardCategory = '';
  public title = "Add award";
  public buttonText = "Add";
  public whiteSpaceError = false;
  public disableEdit = true;
  constructor(public dialogRef: MdDialogRef<AddEditAwardDialog>) { }

  ngOnInit() {
    this.addEditAward = JSON.parse(JSON.stringify(this.award));
    this.addEditAward.category = this.character;
    this.oldAwardTitle = this.addEditAward.title;
    this.oldAwardCategory = this.addEditAward.category;
    if (this.addEditAward.awardID) {
      this.title = "Edit award";
      this.buttonText = "Save";
    }
  }

 // sends award data when dialog box is closed 
  onSubmit(isvalid) {
    if (this.oldAwardTitle == this.addEditAward.title && this.oldAwardCategory == this.addEditAward.category) {
      this.disableEdit = true;
    } else {
      this.disableEdit = false;
      if (this.addEditAward.title) {
        let trimmedMessgae = this.addEditAward.title.trim();
        let valid = (isvalid && (trimmedMessgae != ""))
        if (valid) {
          this.dialogRef.close(this.addEditAward);
        }
      }
    }
  }
 
 //close the dialog box when click on cancel button
  onCancel() {
    this.addEditAward.title = this.addEditAward.title;
    this.dialogRef.close(null);
  }

  //checks if award title is different from pervious title or not
  onkeyPress(awardTitle) {
    if (awardTitle != this.oldAwardTitle) this.disableEdit = false;
    else this.disableEdit = true;
  }

  //checks if award category is different from pervious category or not  
  onChange(awardCategory) {
    if (awardCategory != this.oldAwardCategory) this.disableEdit = false;
    else this.disableEdit = true;
  }
}
