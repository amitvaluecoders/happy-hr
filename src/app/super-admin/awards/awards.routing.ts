import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from '@angular/material';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule, CanActivateChild }  from '@angular/router';
import { AwardsListingComponent } from './awards-listing/awards-listing.component';
import { AwardsGuardService } from './awards-guard.service';


export const AwardsPagesRoutes: Routes = [
    {
        path: '',       
        children: [
            { path: '', component: AwardsListingComponent,canActivate:[AwardsGuardService] },
        ]
    }
];

export const AwardsPagesRoutingModule = RouterModule.forChild(AwardsPagesRoutes);
