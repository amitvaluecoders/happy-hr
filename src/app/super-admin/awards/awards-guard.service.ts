import { Injectable } from '@angular/core';
import {Route,CanActivate,CanActivateChild, Router,RouterStateSnapshot,ActivatedRouteSnapshot} from "@angular/router";
import {CoreService} from '../../shared/services/core.service';
import {Observable} from 'rxjs/Rx'; 
import 'rxjs/add/operator/map'

@Injectable()
export class AwardsGuardService {
public module = "award";
  
        constructor(private _core: CoreService, private router: Router){ }
  
        canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {   
         // console.log("Route path",route['_routeConfig']['path'])     
          let url: string = state.url; 
            if(this.checkHavePermission(url)){
              return true;
            }else{
              this.router.navigate(['/extra/unauthorized']);
              return false;
            }
        }

//         canLoad(route: Route): boolean {
//   let url = `/${route.path}`;
// console.log("load",url)
//   return this.checkHavePermission(url);
// }

  //        canActivateChild(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
  //          console.log("CHILD ROUTE")
  //   return this.canActivate(route, state);
  // }

        private checkHavePermission(path) {
            switch (path) {
              // case "/awards":
              //   console.log("in")
              //       return this._core.getModulePermission(this.module, 'view');
                case "/app/super-admin/awards":
                console.log("in")
                    return this._core.getModulePermission(this.module, 'view');
                default:
                    return false;
            }
                   // return this._core.getModulePermission(this.module, 'view');
        }

}


