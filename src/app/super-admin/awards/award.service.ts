import { Injectable } from '@angular/core';
import { RestfulLaravelService} from '../../shared/services/restful-laravel.service';

@Injectable()
export class AwardService {

  constructor(public restfulWebService:RestfulLaravelService) { }

  getAwards(reqParams):any{
   return this.restfulWebService.get(`admin/award-list/${reqParams}`);
  }

  deleteAward(awardID):any{
    return this.restfulWebService.delete(`admin/award-delete/${awardID}`);
  }

  addEditAward(award):any{
    return this.restfulWebService.post('admin/create-award',award);
  }

}
