
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule }  from '@angular/router';
import { MessageListComponent } from './message-list/message-list.component';
import { MessageGuardService } from './message-guard.service';

export const MessagePagesRoutes: Routes = [
    {
        path: '',
        children: [
            { path: '',  component: MessageListComponent,canActivate:[MessageGuardService] },
        ]
    }
];

export const MessagePagesRoutingModule = RouterModule.forChild(MessagePagesRoutes);
