import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from '@angular/material';
import { DataTableModule} from "angular2-datatable";
import { MessageListComponent } from './message-list/message-list.component';
import { MessagePagesRoutingModule } from './message.routing';
import { MessageService} from './message.service';
import { SharedModule } from '../../shared/shared.module';
import { MessageGuardService } from './message-guard.service'; 
@NgModule({
  imports: [
    CommonModule,
    MaterialModule,
    DataTableModule,
    MessagePagesRoutingModule,
    SharedModule
  ],
  declarations: [MessageListComponent],
  providers:[MessageService, MessageGuardService]
})
export class MessageModule { }
