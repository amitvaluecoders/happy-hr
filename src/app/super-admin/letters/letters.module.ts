import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { MaterialModule } from '@angular/material';
import { DataTableModule} from "angular2-datatable";

import { LetterDetailComponent } from './letter-detail/letter-detail.component';
import { LettersPagesRoutingModule } from './letters.routing';
import { QuillModule } from 'ngx-quill';
import { LettersComponent } from './letters/letters.component';
import { AddEditLetterComponent } from './add-edit-letter/add-edit-letter.component';
import { LettersService } from './letters.service';
import { SharedModule } from '../../shared/shared.module';
import { LettersGuardService} from './letters-guard.service';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    MaterialModule,
    DataTableModule,
    LettersPagesRoutingModule,
    QuillModule,
    SharedModule
  ],
  declarations: [ LetterDetailComponent, LettersComponent, AddEditLetterComponent],
  providers:[LettersService,LettersGuardService]
})
export class LettersModule { }
