import { Component, OnInit } from '@angular/core';
import { LettersService } from '../letters.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { CoreService } from '../../../shared/services/core.service';
import { Letter, LetterType } from '../letter';

@Component({
  selector: 'app-letter-detail',
  templateUrl: './letter-detail.component.html',
  styleUrls: ['./letter-detail.component.scss']
})
export class LetterDetailComponent implements OnInit {

  public letter: Letter;
  public color:any;

  public letterId;
  public isProcessing: boolean;
  permission;

  constructor(private lettersService: LettersService, private router: Router, private activatedRoute: ActivatedRoute, public coreService: CoreService) {
    this.letter = new Letter;
  }

  ngOnInit() {
    if (this.activatedRoute.snapshot.params['id'] != undefined) {
      this.letterId = this.activatedRoute.snapshot.params['id'];
      this.getLetter();
    }
        this.permission=this.coreService.getNonRoutePermission();
  }

  // get letter details
  getLetter() {
    this.isProcessing = true;
    this.lettersService.getLetterDetail(this.letterId).subscribe(
      res => {
        if (res.code == 200) {
          this.letter = res.data;
          this.color = (this.letter.status == 'Active') ? 'primary' : 'default';
          this.isProcessing = false;
        }
      },
      error => {
        this.isProcessing = false;
        this.coreService.notify("Unsuccessful", error.message, 0)
      }
    )
  }
  
  // changes status of letter 
  statusChange() {
    let status = (this.letter.status == 'Active') ? 'Inactive' : 'Active'
    let reqBody = {
      "letterID": this.letter.letterID,
      "status": status
    }
    this.lettersService.updateLetterStatus(reqBody).subscribe(
      res => {
        this.letter.status = status;
        this.color = (this.letter.status == 'Active') ? 'primary' : 'default';
        this.coreService.notify("Successful", res.message, 1);
      },
      error => {
        this.coreService.notify("Unsuccessful", error.message, 0)
      }
    )
  }
  
  // navigates to letters list page
  onCancel() {
    this.router.navigate(['/app/super-admin/letters']);
  }

}
