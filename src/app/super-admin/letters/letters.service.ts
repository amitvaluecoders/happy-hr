import { Injectable } from '@angular/core';
import { RestfulLaravelService } from '../../shared/services/restful-laravel.service';

@Injectable()
export class LettersService {

  constructor(public restfulWebService: RestfulLaravelService) { }


  getLetters(requestParams): any {
    return this.restfulWebService.get(`admin/list-letter/${encodeURIComponent(requestParams)}`);
  }

  getLetterType() {
    return this.restfulWebService.get(`admin/list-letter-type`);
  }

  getLetterDetail(requestParams): any {
    return this.restfulWebService.get(`admin/get-letter-detail/${requestParams}`);
  }

  addEditLetter(requestParams): any {
    return this.restfulWebService.post('admin/add-edit-letter', requestParams);
  }

  updateLetterStatus(reqBody) {
    return this.restfulWebService.post('admin/update-letter-status', reqBody);
  }

}
