import { Routes, RouterModule }  from '@angular/router';
import { LetterDetailComponent } from './letter-detail/letter-detail.component';
import { LettersComponent } from './letters/letters.component';
import { AddEditLetterComponent } from './add-edit-letter/add-edit-letter.component';
import { LettersGuardService} from './letters-guard.service';

export const LettersPagesRoutes: Routes = [
    {
        path: '',
        children: [
            { path: '', component: LettersComponent,canActivate:[LettersGuardService] },
            { path: 'detail/:id', component: LetterDetailComponent,canActivate:[LettersGuardService] },
            { path: 'add', component: AddEditLetterComponent,canActivate:[LettersGuardService] },
            { path: 'edit/:id', component: AddEditLetterComponent,canActivate:[LettersGuardService] },
          
        ]
    }
];

export const LettersPagesRoutingModule = RouterModule.forChild(LettersPagesRoutes);
