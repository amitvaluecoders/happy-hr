import { Component, OnInit, OnChanges } from '@angular/core';
import { LettersService } from '../letters.service';
import { FormsModule } from '@angular/forms';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { CoreService } from '../../../shared/services/core.service';
import { Letter, LetterType } from '../letter';

@Component({
  selector: 'app-add-edit-letter',
  templateUrl: './add-edit-letter.component.html',
  styleUrls: ['./add-edit-letter.component.scss']
})
export class AddEditLetterComponent implements OnInit {
  public letter: Letter;

  public color: string;
  public title = "Add new letter";
  public letterId: any;
  public isProcessing: boolean=false;
  public isEditProcessing:boolean=false;
  public lettersType: Array<LetterType>;

  constructor(private lettersService: LettersService, private router: Router, private activatedRoute: ActivatedRoute, public coreService: CoreService) {
    this.letter = new Letter;
    this.lettersType = new Array<LetterType>();
  }

  ngOnInit() {
    this.getLetterTypes();
    if (this.activatedRoute.snapshot.params['id'] != undefined) {		
      this.letterId = this.activatedRoute.snapshot.params['id'];		
      this.title = "Edit letter";		
    }
  }
  
  // get letter types
  getLetterTypes() {
    this.isProcessing=true;
    this.lettersService.getLetterType()
      .subscribe(
      res => {
        if (res.code === 200) {
          this.lettersType = res.data;
          if (this.activatedRoute.snapshot.params['id'] != undefined) {
            this.letterId = this.activatedRoute.snapshot.params['id'];
            this.getLetterForUpdate();
          }else this.isProcessing=false;
        }
      },
      error => {
        this.isProcessing=false;
        this.coreService.notify("Unsuccessful", error.message, 0);        
      }
      )
  }
  
  // get letter details
  getLetterForUpdate() {
    //this.isProcessing = true;
    this.lettersService.getLetterDetail(this.letterId).subscribe(
      res => {
        if (res.code == 200) {
          this.letter = res.data;
          this.color = (this.letter.status == 'Active') ? 'primary' : 'default';
          this.isProcessing = false;
        }
      },
      error => {
        this.isProcessing = false;
        this.coreService.notify("Unsuccessful", error.message, 0);
      }
    )
  }
  
  // add/edit letter
  addEditLetter(isValid) {
    this.isEditProcessing = true;
    if (isValid) {
      let reqBody = {
        "letterID": this.letter.letterID,
        "letterTypeID": this.letter.lettertype.letterTypeID,
        "title": this.letter.title,
        "description": this.letter.description
      };
      this.lettersService.addEditLetter(reqBody).subscribe(
        res => {
          if (res.code == "200") {
            this.isEditProcessing = false;
            this.coreService.notify("Successful", res.message, 1)
            this.router.navigate([`/app/super-admin/letters/detail/${this.letterId}`]);
          }
        },
        error => {
          this.isEditProcessing = false;
          this.coreService.notify("Unsuccessful", error.message, 0)
        }
      );
    }
  }

  // changes the status of letter
  statusChange() {
    let status = (this.letter.status == 'Active') ? 'Inactive' : 'Active'
    let reqBody = {
      "letterID": this.letter.letterID,
      "status": status
    }
    this.lettersService.updateLetterStatus(reqBody).subscribe(
      res => {
        this.letter.status = status;
        this.color = (this.letter.status == 'Active') ? 'primary' : 'default';
        this.coreService.notify("Successful", res.message, 1);
      },
      error => {
        this.coreService.notify("Unsuccessful", error.message, 0)
      }
    )
  }

  // navigates to letters list page
  onCancel() {
      this.router.navigate(['/app/super-admin/letters']);
  }

}
