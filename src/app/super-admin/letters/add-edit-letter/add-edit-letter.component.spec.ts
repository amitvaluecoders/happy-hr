import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddEditLetterComponent } from './add-edit-letter.component';

describe('AddEditLetterComponent', () => {
  let component: AddEditLetterComponent;
  let fixture: ComponentFixture<AddEditLetterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddEditLetterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddEditLetterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
