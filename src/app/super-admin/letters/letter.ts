
export class LetterType {
    letterTypeID: number;
    title: string;
}

export class Letter {
    constructor() {
        this.lettertype = new LetterType;
    }
    description: string;
    insertTime: string;
    letterID: number
    lettertype: LetterType
    status: string;
    title: string;
    updateTime: Date | null;
}