import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { LettersService } from '../letters.service';
import { Router } from '@angular/router';
import { CoreService } from '../../../shared/services/core.service';
import { ConfirmService } from '../../../shared/services/confirm.service';

@Component({
  selector: 'app-letters',
  templateUrl: './letters.component.html',
  styleUrls: ['./letters.component.scss']
})
export class LettersComponent implements OnInit {
 public toggleBtn:boolean;
  public letters = [];
  public searchText;
  public filters = {
    status: {},
    letterTypeID: {}
  }

  // types = ["Written Warning", "Final Warning", "Termination Letter", "Statement of Service", "Redundancy", "Stand Down", "Resignation"]

  public status = ["Active", "Inactive"];
  public lettersType = [];
  public searchKeyStatusType = '';
  public searchKeyLetterType = '';
  public searchKey = '';
  public isProcessing= false;
  public letterProcessing= false;
  public differentLettersData=[];
  public letterData;
  
  public cancelRequest;

  constructor(public lettersService: LettersService, public router: Router, public confirmService: ConfirmService, public coreService: CoreService, public viewContainerRef: ViewContainerRef) { }

  ngOnInit() {
    this.getLetterTypes();
    this.toggleBtn=false;
  }
  
  // get letter types
  getLetterTypes() {
    this.isProcessing = true;
    this.lettersService.getLetterType().subscribe(
      res => {
        if (res.code === 200) {
          this.lettersType = res.data;
          this.getLetters();
        }
      },
      error => {
        this.isProcessing = false;
      }
    )
  }
  
  // get letters list
  getLetters() {
    this.letterProcessing = true;
    this.differentLettersData=[];
    let searchText=encodeURIComponent(this.searchKey);
    let getLettersParameters = {
      search: searchText,
      filter: this.filters
    };
     this.letterData=[];
    this.cancelRequest=this.lettersService.getLetters(JSON.stringify(getLettersParameters)).subscribe(
      res => {
        if (res.code == "200") {
          this.letters = res.data;
          for (let t of this.lettersType) {
              for (let d of this.letters) {
                if (d.lettertype.letterTypeID ==t.letterTypeID )
                this.letterData.push(d);             
                this.differentLettersData[t.title]=this.letterData; 
              }
              this.letterData=[];
            }
          this.isProcessing = false;
          this.letterProcessing = false;
        }
      },
      error => {
        this.isProcessing = false;
        this.letterProcessing = false;
        this.coreService.notify("Unsuccessful", error.message, 0)
      });
  }
  
  // naviagets to edit letter page
  onEdit(item) {
    this.router.navigate([`/app/super-admin/letters/edit/${item.id}`]);
  }
  
  // search filter
  onSearchKeyChange() {
    this.cancelRequest.unsubscribe();
    this.getLetters();
  }
  
  // filter letters list
  onFilter() {
    this.getLetters();
  }
  
  // naviagtes to letter details page
  onClick(letterId) {
    this.router.navigate([`/app/super-admin/letters/detail/${letterId}`]);
  }

}
