import { Component, OnInit } from '@angular/core';
import { PolicyService } from '../policy.service';
import { FormsModule } from '@angular/forms';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { CoreService } from '../../../shared/services/core.service';

@Component({
  selector: 'app-policy-version',
  templateUrl: './policy-version.component.html',
  styleUrls: ['./policy-version.component.scss']
})
export class PolicyVersionComponent implements OnInit {
  
  public collapsebox=[];
  public opencollapse;
  
  public policyVersions;
  public isProcessing:Boolean=false;
  public id;
  
  constructor(public policyService:PolicyService,public coreService:CoreService,private activatedRoute:ActivatedRoute) { }

  ngOnInit() {
    if (this.activatedRoute.snapshot.params['id'] != undefined) {
    this.id = this.activatedRoute.snapshot.params['id'];
    this.getPolicyVersions();
  }    
  }
  
  // get policy versions
  getPolicyVersions(){
    this.isProcessing=true;
    this.policyService.getPolicyVersions(this.id).subscribe( 
      d=>{
        if(d.code=="200"){
          this.policyVersions=d.data;
          this.isProcessing=false;
        }
        else this.coreService.notify("Unsuccessful","error while getting policy versions",0);
      },
      e=>{
        this.coreService.notify("Unsuccessful",e.message,0);        
      })
  }

  // restore policy version to previous version
  restorePolicyVersion(policyID){
    this.isProcessing=true;
    this.policyService.restorePolicyVersion(policyID).subscribe( 
      d=>{
        if(d.code=="200"){
          this.isProcessing=false;
          this.getPolicyVersions();
          this.coreService.notify("Successful",d.message,1);
        }
        else {this.isProcessing=false;this.coreService.notify("Unsuccessful",d.message,2);}
      },
      e=>{
        this.isProcessing=false;
        this.coreService.notify("Unsuccessful",e.message,2);        
      })
  }

}
