import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PolicyVersionComponent } from './policy-version.component';

describe('PolicyVersionComponent', () => {
  let component: PolicyVersionComponent;
  let fixture: ComponentFixture<PolicyVersionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PolicyVersionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PolicyVersionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
