import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { PolicyService } from '../policy.service';
import { FormsModule } from '@angular/forms';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { CoreService } from '../../../shared/services/core.service';
import { ConfirmService } from '../../../shared/services/confirm.service';

@Component({
  selector: 'app-new-policy',
  templateUrl: './new-policy.component.html',
  styleUrls: ['./new-policy.component.scss']
})
export class NewPolicyComponent implements OnInit {
  
  public collapsebox=[];
  public policyTypes=[];
  public isButtonClicked=false;
  public isProcessing=false;
  public status="";
  public version='1.0';  

public policy = {
    policyDocumentID:null,
    policyDocumentTypeID:"",
    title:"",
    description:"",
    policyDocumentSubjects:[],
};

public color="primary";
public title="Add new policy";

subject=[{
  title:"",
  description:""
}];

  public id;
  public policies=[]
  public isprocessing = true;
  public buttonText = "Add";

  constructor(private policyService: PolicyService, private router: Router, private activatedRoute: ActivatedRoute,public coreService:CoreService, private confirmService:ConfirmService, private viewContainerRef:ViewContainerRef) { }
  
  ngOnInit() {
    this.getPolicyTypes();
    if (this.activatedRoute.snapshot.params['id'] != undefined) {
          this.id = this.activatedRoute.snapshot.params['id'];
          this.buttonText = "Save";
          this.title="Edit policy";
    }
  }
 
  // get policy types
  getPolicyTypes(){
    this.isProcessing=true;
    this.policyService.getPolicyTypes().subscribe(
        data => {
          if (data.code == "200") {
            this.policyTypes=data.data;
          if (this.activatedRoute.snapshot.params['id'] != undefined) {
          this.getPolicyForUpdate();
    }else this.isProcessing=false;      
          }
        },
        error=>{
            this.isProcessing=false;
            this.coreService.notify("Unsuccessful",error.message,0);
        }
      );
  }
  
  // add/edit policy
  addEditPolicy(isValid) {
    this.isButtonClicked=true;
    if(isValid){
    this.policy.policyDocumentSubjects=this.subject;
      this.policyService.addEditPolicy(this.policy).subscribe(
        data => {
          if (data.code == "200") {
            this.isButtonClicked=false;
            /* this.isprocessing=true;  
             this.snackBar.open("Template Added Successfully","", {
             duration: 3000,
         });*/
            this.coreService.notify("Successful",data.message,1),
            this.router.navigate(['/app/super-admin/policy']);
          }
        },
        error=>{
          this.isButtonClicked=false;
          this.coreService.notify("Unsuccessful",error.message,0);
        }
      );
    }
  }
  
  // navigates to policies list page  
  onCancel() {
    this.router.navigate(['/app/super-admin/policy']);
  }

  // get policy details
  getPolicyForUpdate() {
    this.policyService.getPolicy(this.id ).subscribe(
      data => {
        if (data.code == "200") {
          let d=data.data;
          this.policy.policyDocumentID=d.policyDocumentID;
          this.policy.policyDocumentTypeID=d.policyDocumentType.policyDocumentTypeID;
          this.policy.title=d.title;
          this.policy.description=d.description;
          this.subject=d.policyDocumentSubjects;
          this.status=data.data.status;
          this.version=data.data.version;
          this.color=(this.status == 'Active') ? 'primary' : 'default';
          this.isProcessing=false;        
        }
      },
      error => { },
      () => { }
    )
  }
  
  // add policy subject
  addSubject(){
  this.subject.push({title:"",description:""});
  }

  // delete policy subject
  deleteSubject(subject){
    this.confirmService.confirm(this.confirmService.tilte,this.confirmService.message,this.viewContainerRef)
    .subscribe(res=>{
       let result=res;
    if(result){
    this.subject.splice(subject,1);
    if(this.id) {
      this.policy.policyDocumentSubjects=this.subject;
    }
    }})
  }
  
  // changes the status of policy
  statusChange(status) {
    let s = (status == 'Active') ? 'Inactive' : 'Active'
    let reqBody={
      policyDocumentID:this.policy.policyDocumentID,
      status:s
}
    this.policyService.updateStatus(reqBody).subscribe(
      data => {
        this.status = s;
        this.color = (this.status == 'Active') ? 'primary' : 'default';
        this.coreService.notify("Successful", data.message, 1)
      },
      error => {
        this.coreService.notify("Unsuccessful", error.message, 0)
      }
    )
  }

  public editorOptions={
toolbar: [
    ['bold', 'italic', 'underline', 'strike'],        // toggled buttons
    ['blockquote', 'code-block'],

    [{ 'header': 1 }, { 'header': 2 }],               // custom button values
    [{ 'list': 'ordered'}, { 'list': 'bullet' }],
    [{ 'script': 'sub'}, { 'script': 'super' }],      // superscript/subscript
    [{ 'indent': '-1'}, { 'indent': '+1' }],          // outdent/indent
    [{ 'direction': 'rtl' }],                         // text direction

    [{ 'size': ['small', false, 'large', 'huge'] }],  // custom dropdown
    [{ 'header': [1, 2, 3, 4, 5, 6, false] }],

    [{ 'color': [] }, { 'background': [] }],          // dropdown with defaults from theme
    [{ 'font': [] }],
    [{ 'align': [] }],
     
    ['link'],

    ['clean'],                                         // remove formatting button

                           // link and image, video
  ]
 }
}
