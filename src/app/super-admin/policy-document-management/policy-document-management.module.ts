import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { MaterialModule } from '@angular/material';
import { DataTableModule} from "angular2-datatable";
import { PolicyDocumentManagementComponent } from './policy-document-management/policy-document-management.component';
import { NewPolicyComponent } from './new-policy/new-policy.component';
import { PolicyDetailComponent } from './policy-detail/policy-detail.component';
import { PolicyVersionComponent } from './policy-version/policy-version.component';
import { PolicyDocumentManagementPagesRoutingModule } from './policy-document-management.routing';
import { PolicyService} from './policy.service' ;
import { QuillModule } from 'ngx-quill';
import { SharedModule } from '../../shared/shared.module';
import { PolicyGuardService } from './policy-guard.service';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    PolicyDocumentManagementPagesRoutingModule,
    MaterialModule,
    DataTableModule,
    QuillModule,
    SharedModule
  ],
  declarations: [PolicyDocumentManagementComponent, NewPolicyComponent, PolicyDetailComponent, PolicyVersionComponent],
  providers:[PolicyService,PolicyGuardService] 
})
export class PolicyDocumentManagementModule { }
