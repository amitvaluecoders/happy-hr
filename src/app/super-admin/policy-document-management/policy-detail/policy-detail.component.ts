import { Component, OnInit } from '@angular/core';
import { PolicyService} from '../policy.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { CoreService } from '../../../shared/services/core.service';

@Component({
  selector: 'app-policy-detail',
  templateUrl: './policy-detail.component.html',
  styleUrls: ['./policy-detail.component.scss']
})
export class PolicyDetailComponent implements OnInit {
  
  public collapsebox=[];
  public opencollapse;
  public status="";
  public color="primary";
  public policy = {
    // id:"",
    // type:"",
    // title:"",
    // version:"",
    // description:"",
    // createdDate:"",
    // lastUpdatedDate:"",
    // image:"",
    // status:"",
    // subjects:[]
    
    description: "",
    policyDocumentID: null,
    policyDocumentSubjects: [{
    description: "",
    title: ""}],
    policyDocumentType: {
    policyDocumentTypeID: 1,
    title: ""},
    status: "",
    title: "",
    version: null
    };

  public id;
  public isProcessing=false;

  public policies=[]
  permission;

 constructor(private policyService:PolicyService,private router:Router,private activatedRoute:ActivatedRoute,public coreService:CoreService) { }
 
 ngOnInit() {
  if (this.activatedRoute.snapshot.params['id'] != undefined) {
    this.id = this.activatedRoute.snapshot.params['id'];
    this.getPolicy();
  }
  this.permission=this.coreService.getNonRoutePermission();
}

// get policy details
getPolicy() {
  this.isProcessing=true;
  this.policyService.getPolicy(this.id).subscribe(
    data => {
      if (data.code == "200") {
        this.policy=data.data;
        this.status=data.data.status;
        this.color=(this.status == 'Active') ? 'primary' : 'default';
        this.isProcessing=false;
        
      }
    },
    error => { this.isProcessing=false;this.coreService.notify("Unsuccessful",error.message,0)},
    () => { }
  )
}

 // navigates to policy versions page
 showPolicyVersions(policyID){
     this.router.navigate([`app/super-admin/policy/version/${policyID}`]);
   }

// navigates to policies list page 
onCancel() {
  this.router.navigate(['/app/super-admin/policy']);
}

// changes the status of policy
statusChange(status) {
  let s = (status == 'Active') ? 'Inactive' : 'Active'
  let reqBody={
    policyDocumentID:this.policy.policyDocumentID,
    status:s
}
  this.policyService.updateStatus(reqBody).subscribe(
    data => {
      this.status = s;
      this.color = (this.status == 'Active') ? 'primary' : 'default';
      this.coreService.notify("Successful", data.message, 1)
    },
    error => {
      this.coreService.notify("Unsuccessful", error.message, 0)
    }
  )
}
}


