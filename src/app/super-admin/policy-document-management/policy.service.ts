import { Injectable } from '@angular/core';
import { RestfulwebService} from '../../shared/services/restfulweb.service';
import { RestfulLaravelService } from '../../shared/services/restful-laravel.service';

@Injectable()
export class PolicyService {

    constructor(public restfulWebService:RestfulwebService,
                public restfulLaravelService:RestfulLaravelService) { }

  getPoliciesList(reqBody):any{
   return this.restfulLaravelService.get(`admin/list-policy-document/${encodeURIComponent(reqBody)}`)
  }

  getPolicy(reqBody):any{
   return this.restfulLaravelService.get(`admin/get-policy-document-detail/${reqBody}`)
  }

  deletePolicy(policy):any{
    return this.restfulWebService.delete(`delete/${policy}`);
  }

  addEditPolicy(policy):any{
    return this.restfulLaravelService.post('admin/add-edit-policy-document',policy);
  }

  updateStatus(policy){
    return this.restfulLaravelService.put('admin/policy-status-change',policy);
  }

  getPolicyTypes(){
    return this.restfulLaravelService.get('admin/list-policy-document-type')
  }

  getPolicyVersion(){
    return this.restfulLaravelService.get('admin/list-policy-document-version-number')
  }

  getPolicyVersions(reqBody){
    return this.restfulLaravelService.get(`admin/get-policy-document-version/${reqBody}`)
  }

  restorePolicyVersion(reqBody){
    return this.restfulLaravelService.get(`admin/restore-policy-document-version/${reqBody}`)
  }
}
