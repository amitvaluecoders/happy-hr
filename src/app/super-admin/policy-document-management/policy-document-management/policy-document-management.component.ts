import { Component, OnInit } from '@angular/core';
import { PolicyService} from '../policy.service';
import { Router} from '@angular/router';
import { CoreService } from '../../../shared/services/core.service';

@Component({
  selector: "app-policy-document-management",
  templateUrl: "./policy-document-management.component.html",
  styleUrls: ["./policy-document-management.component.scss"]
})
export class PolicyDocumentManagementComponent implements OnInit {
  public searchKeyPolicyType = "";
  public searchKeyStatusType = "";
  public searchKeyVersionType = "";
  public policies = [];
  public searchText = "";
  public s = {
    Active: false,
    Inactive: false
  };
  public filters = {
    status: {},
    policyDocumentTypeID: {},
    version: {}
  };
  public status = [];
  public policyDocumentTypes = [];
  public versions = [];
  public isProcessing = false;
  public listProcessing = false;
  public cancelRequest;
  public differentPoliciesData=[];
  public arrayIsEmpty=false;
  public policyData;
  permission;

  constructor(
    public policyService: PolicyService,
    public router: Router,
    public coreService: CoreService
  ) {}

  ngOnInit() {
    this.getPolicyTypes();
    this.getPolicyVersion();
    this.permission=this.coreService.getNonRoutePermission();
  }
  
  // get policies list
  getPoliciesList() {
    this.listProcessing = true;
    this.differentPoliciesData=[];
    let searchText = encodeURIComponent(this.searchText);
    let getPoliciesParameters = {
      search: searchText,
      filter: this.filters
    };
    this.policyData=[];
    this.cancelRequest = this.policyService
      .getPoliciesList(JSON.stringify(getPoliciesParameters))
      .subscribe(
        data => {
          if (data.code == "200") {
            
            this.policies = data.data;
            for (let t of this.policyDocumentTypes) {
              for (let d of this.policies) {
                if (d.policyDocumentType.policyDocumentTypeID ==t.policyDocumentTypeID )
                this.policyData.push(d);             
                this.differentPoliciesData[t.title]=this.policyData; 
              }
              this.policyData=[];
            }
            if(this.differentPoliciesData.length==0) {           
              this.arrayIsEmpty=true;}
            else {                       
              this.arrayIsEmpty=false;}
            this.listProcessing = false;
            this.isProcessing = false;
          }
        },
        error => {
          this.listProcessing = false;
          this.isProcessing = false;
          this.coreService.notify(
            "Unsuccessful",
            "Error while getting the data",
            0
          );
        },
        () => {}
      );
  }

  // get policies type
  getPolicyTypes() {
    this.isProcessing = true;
    this.policyService.getPolicyTypes().subscribe(
      data => {
        let policyTypes = {};
        if (data.code == "200") {
          this.policyDocumentTypes = data.data;
          for (let i of data.data) {
            policyTypes[i.policyDocumentTypeID] = false;
          }
          this.filters.policyDocumentTypeID = policyTypes;
          this.status = Object.keys(this.s);
          for (let s of this.status) {
            this.filters.status[s] = false;
          }
          this.getPoliciesList();
        }
      },
      error => {
        this.isProcessing = false;
        this.coreService.notify(
          "Unsuccessful",
          "Error while getting the data",
          0
        );
      }
    );
  }

 // get policies versions
  getPolicyVersion() {
    this.policyService.getPolicyVersion().subscribe(
      data => {
        if (data.code == "200") {
          for (let i of data.data) {
            this.filters.version[i.version] = false;
          }
          this.versions = data.data;
        }
      },
      error => {
        this.isProcessing = false;
        this.coreService.notify(
          "Unsuccessful",
          "Error while getting the data",
          0
        );
      }
    );
  }
  
  // navigates to edit policy page
  onEdit(item) {
    this.router.navigate([`/app/super-admin/policy/editPolicy/${item.id}`]);
  }

  // filter policies list
  onFilter() {
    this.cancelRequest.unsubscribe();
    this.getPoliciesList();
  }

  // navigates to policy details page
  getPolicyDetails(policyID) {
    this.router.navigate([`app/super-admin/policy/detail/${policyID}`]);
  }
}
