import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PolicyDocumentManagementComponent } from './policy-document-management.component';

describe('PolicyDocumentManagementComponent', () => {
  let component: PolicyDocumentManagementComponent;
  let fixture: ComponentFixture<PolicyDocumentManagementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PolicyDocumentManagementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PolicyDocumentManagementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
