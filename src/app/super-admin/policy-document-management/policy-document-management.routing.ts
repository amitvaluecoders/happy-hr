import { Routes, RouterModule }  from '@angular/router';
import { NewPolicyComponent } from './new-policy/new-policy.component';
import { PolicyDetailComponent } from './policy-detail/policy-detail.component';
import { PolicyDocumentManagementComponent } from './policy-document-management/policy-document-management.component';
import { PolicyVersionComponent } from './policy-version/policy-version.component';
import { PolicyGuardService } from './policy-guard.service';

export const PolicyDocumentManagementPagesRoutes: Routes = [
    {
        path: '',
        children: [
            { path: '', component: PolicyDocumentManagementComponent,canActivate:[PolicyGuardService] },
            { path: 'newPolicy', component: NewPolicyComponent,canActivate:[PolicyGuardService] },
            { path: 'detail/:id', component: PolicyDetailComponent,canActivate:[PolicyGuardService] },
            { path: 'editPolicy/:id', component: NewPolicyComponent,canActivate:[PolicyGuardService] },
            { path: 'version/:id', component: PolicyVersionComponent,canActivate:[PolicyGuardService] },
       ]
    }
];

export const PolicyDocumentManagementPagesRoutingModule = RouterModule.forChild(PolicyDocumentManagementPagesRoutes);
