import { Component, OnInit } from '@angular/core';
import {ReportsService}  from '../reports.service';
import { CoreService } from '../../../shared/services/core.service';
import { thisTypeAnnotation } from 'babel-types';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-api-reports',
  templateUrl: './api-reports.component.html',
  styleUrls: ['./api-reports.component.scss']
})
export class ApiReportsComponent implements OnInit {

 public myDatePickerOptions={};
 dateFrom="";
 isProcessing:boolean=false;
 dateTo="";
 apiList=[];

  constructor(public reportService: ReportsService, public coreService: CoreService,public router:Router) { }

  ngOnInit() {
    this.getApiReportList();
  }

  onDateChangedFrom(e){
    this.dateFrom=e.formatted;
    //this.getApiReportList();
  }
  onDateChangedTo(e){
    this.dateTo=e.formatted;
    this.getApiReportList(); 
  }
  // api report list
  getApiReportList(){
    this.isProcessing = true;
    let body={"toDate":this.dateTo,"fromDate":this.dateFrom}
    //console.log("bodyy....",body);
    this.reportService.getApiReportList(JSON.stringify(body)).subscribe(
      res=>{
            if(res.code==200){
                if(res.data){
                  this.apiList=res.data;
                } 
                //console.log("data.........",this.apiList);
                this.isProcessing=false;
            }
            else{
              this.coreService.notify("Unsuccessful", res.message, 0);
              this.isProcessing=false;
            }

      },
      error => this.coreService.notify("Unsuccessful", error.message, 0),

    )
  }

  showCompnayWiseReport(api){
      console.log("api..",api);
      this.router.navigate([`/app/super-admin/reports/API/company-wise/${api.apiID}/${api.name}`]);       
  }
  showLogs(apiId){
    this.router.navigate([`/app/super-admin/reports/API/log/${apiId}`]);
  }



}
