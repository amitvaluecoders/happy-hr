import { Component, OnInit } from '@angular/core';
import { ReportsService } from "../reports.service";
import { CoreService } from "../../../shared/services/core.service";

@Component({
  selector: 'app-user-report',
  templateUrl: './user-report.component.html',
  styleUrls: ['./user-report.component.scss']
})
export class UserReportComponent implements OnInit {
  isProcessing = false;
  search = {}
  reportList = [];
  filters = {
    "filter": {
      "status": { "Active": true, "Inactive": false },
      "employeeCount": { "100": false, "200": false },
      "subCompanyCount": { "5": false, "10": false, "15": false }
    }
  }
  unSubscriber: any;

  constructor(private reportService: ReportsService, private coreService: CoreService) { }

  ngOnInit() {
    this.getSurveyList();
  }

  onApplyFilter() {
    this.unSubscriber.unsubscribe();
    this.getSurveyList();
  }

  getSurveyList() {
    console.log(JSON.stringify(this.filters));
    this.isProcessing = true;
    this.unSubscriber = this.reportService.getReportList(JSON.stringify(this.filters)).subscribe(
      res => {
        this.isProcessing = false;
        if (res.code == 200) {
          this.reportList = res.data;
        }
        else return this.coreService.notify("Unsuccessful", res.message, 0);
      },
      error => {
        this.isProcessing = false;
        this.coreService.notify("Unsuccessful", error.message, 0);
      }
    )
  }
}
