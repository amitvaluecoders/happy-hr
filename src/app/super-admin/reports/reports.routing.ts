import { Routes, RouterModule }  from '@angular/router';
import { UserReportComponent } from './user-report/user-report.component';
import { CouponReportComponent } from './coupon-report/coupon-report.component';
import { TransactionReportComponent } from './transaction-report/transaction-report.component';
import { PaymentReportComponent } from './payment-report/payment-report.component';
import { RevenueReportComponent } from './revenue-report/revenue-report.component';
import { ApiReportsComponent } from './api-reports/api-reports.component';
import { CompanyWiseApiComponent } from './company-wise-api/company-wise-api.component';
import { LogComponent } from './log/log.component';




export const ReportsPagesRoutes: Routes = [
    {
        path: '',
        children: [
            { path: '', component: UserReportComponent },
            { path: 'coupon', component: CouponReportComponent },
            { path: 'transaction', component: TransactionReportComponent },
            { path: 'payment', component: PaymentReportComponent },
            { path: 'revenue', component: RevenueReportComponent },
            { path: 'API', component: ApiReportsComponent },
            { path: 'API/company-wise/:id/:name', component: CompanyWiseApiComponent },
            { path: 'API/log/:id', component: LogComponent },
            { path: 'API/logs/:id/:cid', component: LogComponent },
            
            
            
            
            
            
       ]
    }
];

export const ReportsPagesRoutingModule = RouterModule.forChild(ReportsPagesRoutes);
