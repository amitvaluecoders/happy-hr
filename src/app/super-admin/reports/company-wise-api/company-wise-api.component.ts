import { Component, OnInit } from '@angular/core';
import { CoreService } from '../../../shared/services/core.service';
import {ReportsService}  from '../reports.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-company-wise-api',
  templateUrl: './company-wise-api.component.html',
  styleUrls: ['./company-wise-api.component.scss']
})
export class CompanyWiseApiComponent implements OnInit {
public myDatePickerOptions={};
unsubscriber:any;
dateFrom="";
dateTo="";
data=[];
logo:any;
search={
  name:""
}
reqBody={
  "apiID":null,
  "name": "",
  "toDate": "",
  "fromDate": ""
  };
isProcessing:boolean= false;
  constructor(public reportService: ReportsService, public coreService: CoreService,public activatedRoute: ActivatedRoute,public router:Router) { }

  ngOnInit() {
    this.getCompanyWiseReport();
  }

  onDateChangedFrom(e){
    this.dateFrom=e.formatted;
  }
  onDateChangedTo(e){
    this.dateTo=e.formatted; 
    this.getCompanyWiseReport();
  }
onSearch(){
  //this.unsubscriber.unsubscribe();
    this.reqBody.name=encodeURIComponent(this.search.name);
    this.getCompanyWiseReport();

}
getCompanyWiseReport(){ 
  this.isProcessing=true;
  
    this.reqBody.apiID=this.activatedRoute.snapshot.params['id'],
    this.reqBody.name=this.search.name,
    this.reqBody.toDate=this.dateTo,
    this.reqBody.fromDate=this.dateFrom
      console.log("req.......",this.reqBody);      
      this.reportService.getCompanyWiseReport(JSON.stringify(this.reqBody)).subscribe(
        res=>{
                if(res.code==200){
                  res.data.filter(item=>{
                      this.logo=item.logo;
                      this.data=item.companyData;
                  })
                  this.isProcessing = false;
                }
                else{
                  this.coreService.notify("Unsuccessful", res.message, 0);
                  this.isProcessing=false;
                }

        },
          error => this.coreService.notify("Unsuccessful", error.message, 0),
      )
  }
  showLogs(data){
    this.router.navigate([`/app/super-admin/reports/API/logs/${this.activatedRoute.snapshot.params['id']}/${data.companyID}`]);
  }
}
