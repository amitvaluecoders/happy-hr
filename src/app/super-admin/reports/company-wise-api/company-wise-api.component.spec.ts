import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CompanyWiseApiComponent } from './company-wise-api.component';

describe('CompanyWiseApiComponent', () => {
  let component: CompanyWiseApiComponent;
  let fixture: ComponentFixture<CompanyWiseApiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CompanyWiseApiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompanyWiseApiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
