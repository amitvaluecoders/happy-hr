import { Component, OnInit } from '@angular/core';
import { CoreService } from '../../../shared/services/core.service';
import {ReportsService}  from '../reports.service';
import { Router, ActivatedRoute } from '@angular/router';
import * as moment from "moment";


@Component({
  selector: 'app-log',
  templateUrl: './log.component.html',
  styleUrls: ['./log.component.scss']
})
export class LogComponent implements OnInit {
  isProcessing:boolean=false;
  logsData=[];
  reqBody={};
  constructor(public reportService: ReportsService, public coreService: CoreService,public activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    if(this.activatedRoute.snapshot.params['cid']){
       this.reqBody={apiID: this.activatedRoute.snapshot.params['id'],
                  companyID: this.activatedRoute.snapshot.params['cid'] };    
    }
    else{
      this.reqBody={apiID: this.activatedRoute.snapshot.params['id']}
    }
    this.getLogs();

  }

  formatDate(date){
    return moment(date).isValid()? moment(date).format('DD MMM YYYY') : 'Not specified';
  }
  formatTime(date){
    return moment(date).isValid()? moment(date).format('hh:mm:ss A') : 'Not specified';    
  }

  getLogs(){
      this.isProcessing=true;
      
      this.reportService.getLogs(this.reqBody).subscribe(
        res=>{
                if(res.code==200){
                  this.logsData= res.data.logData;
                  this.isProcessing = false;
                }
                else{
                  this.coreService.notify("Unsuccessful", res.message, 0);
                  this.isProcessing=false;
                }
               //console.log("logs.....",this.logsData);
    
        },
          error => this.coreService.notify("Unsuccessful", error.message, 0),
      )
  }
}
