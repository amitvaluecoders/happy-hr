import { Injectable } from '@angular/core';
import { RestfulLaravelService } from "../../shared/services/restful-laravel.service";

@Injectable()
export class ReportsService {

  constructor(private restfulService: RestfulLaravelService) { }

  getReportList(params) {
    return this.restfulService.get(`admin/report-user/${params}`);
  }
  getApiReportList(params){
    return this.restfulService.get(`admin/get-api-reports-list/${params}`);
  }
  getLogs(params){
    if(params.companyID){
      return this.restfulService.get(`admin/get-api-reports-log/${params.apiID}/${params.companyID}`)
    }
    else{
      return this.restfulService.get(`admin/get-api-reports-log/${params.apiID}`)      
    }
  }
  getCompanyWiseReport(params){
    return this.restfulService.get(`admin/get-api-company-reports/${params}`)
    
  }
}
