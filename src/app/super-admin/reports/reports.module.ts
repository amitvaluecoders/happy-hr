import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from '@angular/material';
import { FormsModule } from '@angular/forms';
import { MyDatePickerModule } from 'mydatepicker';
import { DataTableModule } from "angular2-datatable";
import { UserReportComponent } from './user-report/user-report.component';
import { CouponReportComponent } from './coupon-report/coupon-report.component';
import { TransactionReportComponent } from './transaction-report/transaction-report.component';
import { PaymentReportComponent } from './payment-report/payment-report.component';
import { RevenueReportComponent } from './revenue-report/revenue-report.component';
import { ApiReportsComponent } from './api-reports/api-reports.component';
import { CompanyWiseApiComponent } from './company-wise-api/company-wise-api.component';
import { LogComponent } from './log/log.component';
import { ReportsPagesRoutingModule } from './reports.routing';
import { ReportsService } from "./reports.service";

@NgModule({
  imports: [
    CommonModule,
    MaterialModule,
    FormsModule,
    MyDatePickerModule,
    DataTableModule,
    ReportsPagesRoutingModule
  ],
  declarations: [UserReportComponent, CouponReportComponent, TransactionReportComponent, PaymentReportComponent, RevenueReportComponent, ApiReportsComponent, CompanyWiseApiComponent, LogComponent],
  providers: [ReportsService]
})
export class ReportsModule { }
