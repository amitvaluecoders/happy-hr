import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { DiscountCouponService } from '../discount-coupon.service';
import { DiscountCoupon  } from '../discount-coupon';
import { CoreService } from '../../../shared/services/core.service';
import {ActivatedRoute,Router} from '@angular/router';
import { ConfirmService } from '../../../shared/services/confirm.service';

@Component({
  selector: 'app-discount-coupon-management',
  templateUrl: './discount-coupon-management.component.html',
  styleUrls: ['./discount-coupon-management.component.scss']
})
export class DiscountCouponManagementComponent implements OnInit {

  public discountCoupons:DiscountCoupon[];
  isProcessing=false;
  constructor(public discountCouponService:DiscountCouponService,public coreService:CoreService,private router:Router,public confirmService:ConfirmService,private viewContainerRef:ViewContainerRef) { }

  ngOnInit() {
    this.getDiscountcoupons();
  }

  getDiscountcoupons(){
    this.isProcessing=true;
    this.discountCouponService.getAllDiscountCoupons().subscribe(
      d=>{
        console.log("Discount data",d.data);
        if(d.code=="200") {this.discountCoupons=d.data;this.isProcessing=false;}
        else {this.isProcessing=false;this.coreService.notify("Unsuccessful",d.message,0)}
      },
      e=>{this.isProcessing=false;this.coreService.notify("Unsuccessful",e.message,0)})
  }

  onEdit(id){
    this.router.navigate(['/app/super-admin/discount-coupon-management/edit-coupon',id])
  }

   // delete position description
   onDelete(id) {
  this.confirmService.confirm(this.confirmService.tilte,this.confirmService.message,this.viewContainerRef)
    .subscribe(res=>{   
    if(res){
    this.discountCouponService.deleteCoupon(id).subscribe(
      data => {
        if (data.code == "200") {
         this.getDiscountcoupons();
          this.coreService.notify("Successful",data.message,1)
        }
      },
      error =>{this.isProcessing=false;this.coreService.notify("Unsuccessful",error.message,0);},
      () => { }
    )
    }
    });  
  }

}
