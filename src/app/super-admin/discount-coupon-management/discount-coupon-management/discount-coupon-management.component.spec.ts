import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DiscountCouponManagementComponent } from './discount-coupon-management.component';

describe('DiscountCouponManagementComponent', () => {
  let component: DiscountCouponManagementComponent;
  let fixture: ComponentFixture<DiscountCouponManagementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DiscountCouponManagementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DiscountCouponManagementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
