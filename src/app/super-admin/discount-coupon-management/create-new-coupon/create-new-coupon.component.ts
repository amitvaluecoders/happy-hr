import { Component, OnInit } from '@angular/core';
import {IMyDpOptions, IMyDateModel} from 'mydatepicker';
import { DiscountCoupon } from '../discount-coupon';
import { SubscriptionPlanService } from '../../subscription-plans/subscription-plan.service';
import { CoreService } from '../../../shared/services/core.service';
import { DiscountCouponService } from '../discount-coupon.service';
import { IDatePickerConfig } from 'ng2-date-picker';
import * as moment from "moment";
import {ActivatedRoute,Router} from '@angular/router';
@Component({
  selector: 'app-create-new-coupon',
  templateUrl: './create-new-coupon.component.html',
  styleUrls: ['./create-new-coupon.component.scss']
})
export class CreateNewCouponComponent implements OnInit {

  public isAddButtonClicked=false;
  id;
  title="Create coupon";
  buttonText="Generate coupon code"

  // public discountCoupon:DiscountCoupon = {  
  //   id:'',
  //   couponName:'',
  //   type:"",
  //   couponCode:"",
  //   Amount:null,
  //   maxUserCount:null,
  //   userTillNow:null,
  //   creationDate:"",
  //   validTill:"",
  //   lastUsed:"",
  //   status:"",
  //   validFrom:"",
  //   discountPerUserType:"",
  //   selectedPlan:[],
  //   discountPerUserAmount:null,
  //   discountToSupportType:"",
  //   selectedSupportService:[],
  //   discountToSupportAmount:null
  // }

  discountCoupon={
    //discountCouponID:"",
    couponName: "",
    couponValidFrom: "",
    couponTypeValidTill: "",
    couponMaxUseCount: null,
    discountToSupport: 1,
    discountToEmployee: 1,
    discountToSupportType: "",
    discountToEmployeeType: "",
    discountToSupportValue: null,
    discountToEmployeeValue: null,
    supportServiceIDs: [],
    planIDs: []
}

  config: IDatePickerConfig = {};
  discountToSupport=true;
  discountToEmployee=true;
  

// single select  dropdown options for ' type of discount '  field.
  public dropDownListType=[
    {id:"Percentage",itemName:"%-Baised"},
    {id:"Flat fee",itemName:"Fixed-Price"}
  ]
  public selectedItemsOfDiscountPerUserType=[];
  public selectedItemsOfDiscountOnServicesType=[];
  selectedItemsOfSubscriptionPlanDropdownSelect=[];
  selectedItemsOfSupportServiceDropdownSelect=[];
  

// object having properties for the settings of dropdown.
  public dropDownSettingSingleSelect={
     singleSelection: true,
     text: "Select Type"
  }

// multi select dropdown arrays and setting object.
  public dropDownListSubscriptionPlans=[
    {id:1,itemName:"Plan 1"},
    {id:2,itemName:"Plan 2"}
  ];
  public dropDownListSupportServices=[
    {id:1,itemName:"Service 1"},
    {id:2,itemName:"Service 2"}
  ];
  public dropDownSettingMultiSelect={
      singleSelection: false,
      text: "Select",
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      enableSearchFilter: true,
      classes: "myclass custom-class",
                
  }

//  object having datepicker options. 
  myDatePickerOptions: IMyDpOptions = {
      todayBtnTxt: 'Today',
      dateFormat: 'yyyy-mm-dd',
      firstDayOfWeek: 'mo',
      sunHighlight: true,
      inline: false
  };

   constructor(public subscriptionPlanService:SubscriptionPlanService,
               public coreService:CoreService,
               private router:Router,
               private activatedRoute :ActivatedRoute,
               public discountCouponService:DiscountCouponService) {
                this.config.locale = "en";
                this.config.format = "DD MMM, YYYY";
                this.config.showMultipleYearsNavigation = true;
                this.config.weekDayFormat = 'dd';
                this.config.disableKeypress = true;
                this.config.monthFormat = "MMM YYYY";
                this.config.min = moment();
                }

  ngOnInit() {

   // this.getAllSupportServices();
    //this.getAllSubscriptionPlans(); 
    this.id=this.activatedRoute.snapshot.params['id'];
    if(this.id) {
      this.title="Edit coupon";
      this.buttonText="Generate Coupon Code";
      this.getCouponDetailsForUpdate();}
  }
  

// on date change of "VALID FROM" field. 
  onValidFromDateChange(e){
    this.discountCoupon.couponValidFrom=e.formatted;
  }

// on date change of "VALID TILL" field.   
  onValidTillDateChange(e){
    this.discountCoupon.couponTypeValidTill=e.formatted;
  }

// on selecting any "TYPE" of user discount from dropdown. 
  onDiscountPerUserDropdownSelect(e){
      this.discountCoupon.discountToEmployeeType=e.id;
  }

// on selecting any "TYPE" of user discount from dropdown.   
  onDiscountPerUserDropdownDeSelect(e){
    this.discountCoupon.discountToEmployeeType='';
  }
  
// on selecting any "TYPE" of service discount from dropdown. 
  onServiceDiscountDropdownSelect(e){
      this.discountCoupon.discountToSupportType=e.id;
  }

// on selecting any "TYPE" of service discount from dropdown.   
  onServiceDiscountDropdownDeSelect(e){
    this.discountCoupon.discountToSupportType='';
  }

  // on selecting any subscription plan from dropdown  
  onSubscriptionPlanDropdownSelect(e){
    this.discountCoupon.planIDs.push(e.id)
  }

  // on deselecting any subscription plan from dropdown  
  onSubscriptionPlanDropdownDeSelect(e){
    this.discountCoupon.planIDs=this.discountCoupon.planIDs.filter(item=>item!=e.id);
  }

  // on selecting any subscription plan from dropdown  
  onSupportServiceDropdownSelect(e){
    this.discountCoupon.supportServiceIDs.push(e.id)
  }

  // on deselecting any subscription plan from dropdown  
  onSupportServiceDropdownDeSelect(e){
    this.discountCoupon.supportServiceIDs=this.discountCoupon.supportServiceIDs.filter(item=>item!=e.id);
  }

  onSubmit(isValid){
    this.isAddButtonClicked=true;
    if(isValid){
      if(!this.discountToEmployee && !this.discountToSupport){
        this.coreService.notify("Unsuccessful","Select atleast one discount",0);
      }else{
      this.discountCoupon.discountToEmployee=(this.discountToEmployee)?1:0;
      this.discountCoupon.discountToSupport=(this.discountToSupport)?1:0;      
    this.discountCouponService.addDiscountCoupon(this.discountCoupon).subscribe(
      d=>{
        if(d.code="200"){
          this.coreService.notify("Successful",d.message,1)
          this.router.navigate(['/app/super-admin/discount-coupon-management']);
        }
        else this.coreService.notify("Unsuccessful",d.message,0)
      },
      e=>this.coreService.notify("Unsuccessful",e.message,0))
    }    
  }
  }

  // method to get details of a discount coupon
  getCouponDetailsForUpdate(){
    this.discountCouponService.getCouponDetails(this.id).subscribe(
      d=>{
        if(d.code="200") {
          console.log("Discount data details",d);
          this.discountCoupon=d.data;
        }
        else this.coreService.notify("Unsuccessful",d.message,0);
      },
      e=>this.coreService.notify("Unsuccessful",e.message,0))
  }

  // method to get all support services for dropdown options
    getAllSupportServices(){
      this.subscriptionPlanService.getAllSupportServices().subscribe(
        d=>{
          if(d.code="200") {
            this.dropDownListSupportServices=d.response.data.filter(function(item){
              item['itemName']=item.title;
              return item;
            })
          }
          else this.coreService.notify("Unsuccessful","error while getting support services",0);
        },
        e=>this.coreService.notify("Unsuccessful","error while getting support services",0))
    }

    // method to get all subscription plans for dropdown list
     getAllSubscriptionPlans(){
      this.subscriptionPlanService.getAllSubscriptionPlans().subscribe(
        d=>{
          if(d.code="200"){
            this.dropDownListSubscriptionPlans=d.response.data.filter(function(item){
              item['itemName']=item.title;
              return item;
            })
          } 
          else this.coreService.notify("Unsuccessful","error while getting subscription Plans",0);
        },
        e=>this.coreService.notify("Unsuccessful","error while getting subscription plans",0))
     }
}
