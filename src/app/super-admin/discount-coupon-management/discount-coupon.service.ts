import { Injectable } from '@angular/core';
import { RestfulLaravelService } from '../../shared/services/restful-laravel.service';

@Injectable()
export class DiscountCouponService {

  constructor(
    private restfulWebService: RestfulLaravelService, 
  ){}

  addDiscountCoupon(reqBody){
    return this.restfulWebService.post(`admin/create-update-discount-coupon`,reqBody);     
  }

  getAllDiscountCoupons(){
    return this.restfulWebService.get(`admin/get-discount-coupon-list`);     
  }

  getCouponDetails(reqBody){
    return this.restfulWebService.get(`admin/get-discount-coupon-details/${reqBody}`);         
  }

  deleteCoupon(reqBody){
    return this.restfulWebService.delete(`admin/create-update-discount-coupon/${reqBody}`);         
  }

 
}