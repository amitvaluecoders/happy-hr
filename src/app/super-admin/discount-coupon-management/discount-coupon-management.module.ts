import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from '@angular/material';
import { DataTableModule} from "angular2-datatable";
import { DiscountCouponManagementComponent } from './discount-coupon-management/discount-coupon-management.component';
import { CreateNewCouponComponent } from './create-new-coupon/create-new-coupon.component';
import { discountCouponManagementPagesRoutingModule } from './discount-coupon-management.routing';
import { MyDatePickerModule } from 'mydatepicker';
import { FormsModule} from '@angular/forms';
import { SharedModule } from '../../shared/shared.module';
import { AngularMultiSelectModule } from 'angular2-multiselect-checkbox-dropdown/angular2-multiselect-dropdown';
import { SubscriptionPlanService } from '../subscription-plans/subscription-plan.service';
import { DiscountCouponService } from './discount-coupon.service';
import { DiscountCouponGuardService } from './discount-coupon-guard.service';

@NgModule({
  imports: [
    SharedModule,
    AngularMultiSelectModule,
    CommonModule,
    MaterialModule,
    DataTableModule,
    discountCouponManagementPagesRoutingModule,
    MyDatePickerModule,
    FormsModule
  ],
  declarations: [DiscountCouponManagementComponent, CreateNewCouponComponent],
  providers:[SubscriptionPlanService,DiscountCouponService,DiscountCouponGuardService]
})
export class DiscountCouponManagementModule { }
