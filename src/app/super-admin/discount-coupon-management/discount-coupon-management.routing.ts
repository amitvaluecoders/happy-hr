import { Routes, RouterModule }  from '@angular/router';
import { DiscountCouponManagementComponent } from './discount-coupon-management/discount-coupon-management.component';
import { CreateNewCouponComponent } from './create-new-coupon/create-new-coupon.component';




export const discountCouponManagementPagesRoutes: Routes = [
    {
        path: '',
        children: [
            { path: '', component: DiscountCouponManagementComponent },
            { path: 'new-coupon', component: CreateNewCouponComponent },
            { path: 'edit-coupon/:id', component: CreateNewCouponComponent }            
       ]
    }
];

export const discountCouponManagementPagesRoutingModule = RouterModule.forChild(discountCouponManagementPagesRoutes);
