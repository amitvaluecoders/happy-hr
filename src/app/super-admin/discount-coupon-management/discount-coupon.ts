export interface DiscountCoupon {

    id?:String,
    couponName?:String,
    type?:String,
    couponCode?:String,
    Amount?:Number,
    maxUserCount?:Number,
    userTillNow?:Number,
    creationDate?:String,
    validTill?:String,
    lastUsed?:String,
    status?:String,
    validFrom?:String
    discountPerUserType?:String,
    selectedPlan?:Object,
    discountPerUserAmount?:Number,
    discountToSupportType?:String,
    selectedSupportService?:Object,
    discountToSupportAmount?:Number

}
