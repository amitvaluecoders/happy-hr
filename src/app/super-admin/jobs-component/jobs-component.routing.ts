import { Routes, RouterModule }  from '@angular/router';
import { JobsListingComponent } from './jobs-listing/jobs-listing.component';
import { EmployementListingComponent } from './employement-listing/employement-listing.component';
import { JobsGuardService } from './jobs-guard.service';

export const JobsComponentPagesRoutes: Routes = [
    {
        path: '',
        children: [
            { path: '', component: JobsListingComponent,canActivate:[JobsGuardService] },
            { path: 'employement', component: EmployementListingComponent},
         ]
    }
];

export const JobsComponentPagesRoutingModule = RouterModule.forChild(JobsComponentPagesRoutes);
