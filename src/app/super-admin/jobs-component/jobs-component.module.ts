import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from '@angular/material';
import { FormsModule } from '@angular/forms';
import { DataTableModule} from "angular2-datatable";
import { JobsListingComponent } from './jobs-listing/jobs-listing.component';
import { EmployementListingComponent } from './employement-listing/employement-listing.component';
import { InlineEditorModule} from 'ng2-inline-editor';
import { JobsComponentPagesRoutingModule } from './jobs-component.routing';
import { JobsComponentService } from './jobs-component.service';
import { JobsGuardService } from './jobs-guard.service';

@NgModule({
  imports: [
    CommonModule,
    MaterialModule,
    FormsModule,
    DataTableModule,
    JobsComponentPagesRoutingModule,
    InlineEditorModule
  ],
  declarations: [JobsListingComponent, EmployementListingComponent],
  providers:[JobsComponentService,JobsGuardService]
})
export class JobsComponentModule { }
