import { Component, OnInit } from '@angular/core';
import { JobsComponentService } from '../jobs-component.service';
import { InlineEditorComponent } from 'ng2-inline-editor';
import { CoreService } from '../../../shared/services/core.service';

@Component({
  selector: 'app-employement-listing',
  templateUrl: './employement-listing.component.html',
  styleUrls: ['./employement-listing.component.scss']
})
export class EmployementListingComponent implements OnInit {

  constructor(private jobsComponentService:JobsComponentService, private coreService:CoreService) { }
  
  employmentTypes=[];
  isProcessing=false;
  isUpdateProcessing=false;
  status=["Active","Inactive"];
  selectedStatus;
  showEditFields=false;
  //permission;
  ngOnInit() {
    this.getEmploymentTypes();
    //this.permission=this.coreService.getNonRoutePermission();
  }
  
  // get employment types
  getEmploymentTypes(){
    this.isProcessing=true;
    this.jobsComponentService.getEmployementTypes().subscribe(
      data => {
        if (data.code == "200") {
          this.employmentTypes = data.data;
          this.isProcessing = false;
        }
      },
      error => {
        this.isProcessing = false;
        this.coreService.notify("Unsuccessful",error.message,0);
      },
      () => {}
    );
  }

  // update employment type
  updateEmployementType(employmentTypes){
    this.isUpdateProcessing=true;
    let reqBody={employmentTypes:employmentTypes}
    this.jobsComponentService.updateEmployementType(reqBody).subscribe(
      data => {
        if (data.code == "200") {
          // this.employmentTypes = data.data;
          this.isUpdateProcessing = false;
          this.showEditFields=!this.showEditFields;
          this.coreService.notify("Successful",data.message,1);
        }
      },
      error => {
        this.isUpdateProcessing = false;
        this.coreService.notify("Unsuccessful",error.message,0);
      },
      () => {}
    );
  }

  onEdit(){
   this.showEditFields=!this.showEditFields;
  }

  onCancel(){
    this.showEditFields=!this.showEditFields;
  }

}
