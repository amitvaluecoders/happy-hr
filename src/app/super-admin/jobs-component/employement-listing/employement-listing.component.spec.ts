import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployementListingComponent } from './employement-listing.component';

describe('EmployementListingComponent', () => {
  let component: EmployementListingComponent;
  let fixture: ComponentFixture<EmployementListingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmployementListingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployementListingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
