import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { JobsComponentService } from '../jobs-component.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { CoreService } from '../../../shared/services/core.service';
import { ConfirmService } from '../../../shared/services/confirm.service';

@Component({
  selector: "app-jobs-listing",
  templateUrl: "./jobs-listing.component.html",
  styleUrls: ["./jobs-listing.component.scss"]
})
export class JobsListingComponent implements OnInit {
  public limit = "10";
  public offset = "0-9";
  public lists = { industry: [], location: [], function: [] };
  public industryList: any = [];
  public functionList: any = [];
  public locationList: any = [];
  public editList: any;
  public showEditIndustryField = false;
  public showEditFunctionField = false;
  public showEditLocationField = false;
  public hideIndustryField = false;
  public showIndustryEditField = false;
  public hideFunctionField = false;
  public showFunctionEditField = false;
  public hideLocationField = false;
  public showLocationEditField = false;
  public oldTitle;
  public sort=false;
  public addEditLocationProcessing=false;
  public addEditFunctionProcessing=false;
  public addEditIndustryProcessing=false;
  
  public deleteLocationProcessing=false;
  public deleteFunctionProcessing=false;
  public deleteIndustryProcessing=false;
  
  

  public jobFunctions = [];
  public functionListProcessing = false;
  public locations = [];
  public locationListProcessing = false;
  public industries = [];
  public industryListProcessing = false;

  permission;
  
  constructor(
    public jobsComponentService: JobsComponentService,
    public router: Router,
    public coreService: CoreService,
    public confirmService: ConfirmService,
    public viewContainerRef: ViewContainerRef
  ) {}

  ngOnInit() {
    this.getJobFunctions();
    this.getLocations();
    this.getIndustries();
    this.permission=this.coreService.getNonRoutePermission();
  }

  // get all job functions
  getJobFunctions() {
    this.functionListProcessing = true;
    this.jobsComponentService.getJobFunctions().subscribe(
      data => {
        if (data.code == "200") {
          this.jobFunctions = data.data;
          this.functionListProcessing = false;
        }
      },
      error => {
        this.functionListProcessing = false;
        this.coreService.notify("Unsuccessful",error.message,0);
      },
      () => {}
    );
  }
 
  // shows add job function panel
  addJobFunctionField() {
    this.functionList = [];
    this.locationList = [];
      this.hideFunctionField = true;
      this.showFunctionEditField = false;
      this.functionList.push({ jobFunctionID: "", title: "", jobIndustryID:'' });
  }
  
  // get job function detail
  getDataForJobFunctionUpdate(jobFunction){
      this.functionList = [];
      this.functionList.push(jobFunction);
      this.showIndustryEditField = false;
      this.showFunctionEditField = true;
      this.showLocationEditField = false;
      this.hideFunctionField = false;
      this.jobFunctions.filter(item => {
        if (item.title == jobFunction.title) {
          this.oldTitle = item.title;
        }
      });
  }
  
  // hides the edit panel
  onJobFunctionEditCancel(jobFunction){
       this.jobFunctions.filter(item => {
        if (item.jobFunctionID == jobFunction.jobFunctionID) {
          jobFunction.title = this.oldTitle;
        }
      });
      this.showFunctionEditField = false;
  }

  // add job function
  addEditJobFunctionData() {
    this.addEditFunctionProcessing=true;
    this.jobsComponentService.addEditJobFunctionData(this.functionList[0]).subscribe(
      data => {
          if (!this.showFunctionEditField) {
            for (let i of this.functionList) this.lists.function.unshift(i);
          }
          this.showFunctionEditField = false;
          this.hideFunctionField = false;
          this.addEditFunctionProcessing=false;
         this.getJobFunctions();
        this.coreService.notify("Successful", data.message, 1);
      },
      error =>{
        this.addEditFunctionProcessing=false;        
        this.coreService.notify("Unsuccessful",error.message,0)},
      () => {}
    );
  }
  
  // delete job function
  deleteJobFunction(jobFunctionID,index){
    this.confirmService.confirm(
        this.confirmService.tilte,
        this.confirmService.message,
        this.viewContainerRef
      )
      .subscribe(res => {
        let result = res;
        if (result) {
          
          this.functionListProcessing = true;
          this.jobsComponentService.deleteJobFunction(jobFunctionID).subscribe(
      data => {
        if (data.code == "200") {
          this.functionListProcessing = false;
          this.getJobFunctions();
          this.coreService.notify("Successful",data.message,1);
        }
      },
      error => {
        this.functionListProcessing = false;
        this.coreService.notify("Unsuccessful",error.message,0);
      },
      () => {}
    );
        }})
  }

  // get all locations
  getLocations() {
    this.locationListProcessing = true;
    this.jobsComponentService.getLocations().subscribe(
      data => {
        if (data.code == "200") {
          this.locations = data.data;
          this.locationListProcessing = false;
        }
      },
      error => {
        this.locationListProcessing = false;
        this.coreService.notify("Unsuccessful",error.message,0);
      },
      () => {}
    );
  }

  // shows add location panel
  addLocationField() {
    this.locationList = [];
      this.hideLocationField = true;
      this.showLocationEditField = false;
      this.locationList.push({ jobLocationID: "", title: "" });
  }
  
  // get location detail
  getDataForLocationUpdate(location){
      this.locationList = [];
      this.locationList.push(location);
      this.showIndustryEditField = false;
      this.showFunctionEditField = false;
      this.showLocationEditField = true;
      this.hideLocationField = false;
      this.locations.filter(item => {
        if (item.title == location.title) {
          this.oldTitle = item.title;
        }
      });
  }
  
  // hides edit location panel
  onLocationEditCancel(location){
       this.locations.filter(item => {
        if (item.jobLocationID == location.jobLocationID) {
          location.title = this.oldTitle;
        }
      });
      this.showLocationEditField = false;
  }

  // add/edit location
  addEditLocationData() {
    this.addEditLocationProcessing=true;
    this.jobsComponentService.addEditLocationData(this.locationList[0]).subscribe(
      data => {
          this.showLocationEditField = false;
          this.hideLocationField = false;
          this.addEditLocationProcessing=false;
         this.getLocations();
        this.coreService.notify("Successful", data.message, 1);
      },
      error =>{
        this.addEditLocationProcessing=false;
        this.coreService.notify("Unsuccessful",error.message,0)},
      () => {}
    );
  }

  // delete location
  deleteLocation(locationID,index){
    this.confirmService.confirm(
        this.confirmService.tilte,
        this.confirmService.message,
        this.viewContainerRef
      )
      .subscribe(res => {
        let result = res;
        if (result) {
          this.locationListProcessing = true;          
    this.jobsComponentService.deleteLocation(locationID).subscribe(
      data => {
        if (data.code == "200") {
         this.locationListProcessing = false;
          this.getLocations();
          this.coreService.notify("Successful",data.message,1);
        }
      },
      error => {  
        this.locationListProcessing = false;
        
        this.coreService.notify("Unsuccessful",error.message,0);
      },
      () => {}
    );
        }})
  }

  // get all industries
  getIndustries() {
    this.industryListProcessing = true;
    let reqBody={search:''};
    this.jobsComponentService.getIndustries(JSON.stringify(reqBody)).subscribe(
      data => {
        if (data.code == "200") {
          this.industries = data.data;
          this.industryListProcessing = false;
        }
      },
      error => {
        this.industryListProcessing = false;
        this.coreService.notify("Unsuccessful",error.message,0);
      },
      () => {}
    );
  }

  // shows add industry panel 
  addIndustryField() {
      this.industryList=[];
      this.hideIndustryField = true;
      this.showIndustryEditField = false;
      this.industryList.push({ jobIndustryID: "", title: "" });
  }
 
 // get industry details
  getDataForIndustryUpdate(industry){
      this.industryList = [];
      this.industryList.push(industry);
      this.showIndustryEditField = true;
      this.showFunctionEditField = false;
      this.showLocationEditField = false;
      this.hideIndustryField = false;
      this.industries.filter(item => {
        if (item.title == industry.title) {
          this.oldTitle = item.title;
        }
      });
  }
 
  // hides the edit industry panel
  onIndustryEditCancel(industry){
       this.industries.filter(item => {
        if (item.jobIndustryID == industry.jobIndustryID) {
          industry.title = this.oldTitle;
        }
      });
      this.showIndustryEditField = false;
  }
  
  // add/edit industry
  addEditIndustryData() {
    this.addEditIndustryProcessing=true;
    let reqBody={
      industryTitle:this.industryList[0].title,
      jobIndustryID:this.industryList[0].jobIndustryID
    }
    this.jobsComponentService.addEditIndustryData(reqBody).subscribe(
      data => {
          this.showIndustryEditField = false;
          this.hideIndustryField = false;
         this.addEditIndustryProcessing=false;
         this.getIndustries();
        this.coreService.notify("Successful", data.message, 1);
      },
      error =>{
        this.addEditIndustryProcessing=false;
        this.coreService.notify("Unsuccessful",error.message,0)},
      () => {}
    );
  }
 
 // delete industry
  deleteIndustry(industryID,index){
    this.confirmService.confirm(
        this.confirmService.tilte,
        this.confirmService.message,
        this.viewContainerRef
      )
      .subscribe(res => {
        let result = res;
        if (result) {
          this.industryListProcessing = true;
          
    this.jobsComponentService.deleteIndustry(industryID).subscribe(
      data => {
        if (data.code == "200") { 
          this.industryListProcessing = false;
          this.getIndustries();
          this.getJobFunctions();
          this.coreService.notify("Successful",data.message,1);
        }
      },
      error => {  
        this.industryListProcessing = false;
        
        this.coreService.notify("Unsuccessful",error.message,0);
      },
      () => {}
    );
        }})
  }

 // sort data
sortData(sort,type){
  let sortArray;
  if(type=='jobFunction') sortArray=this.jobFunctions;
  else sortArray=this.locations;
  if(type=='industry'){
  this.industries.sort(function (a, b) {
  a = (a.title || '').toLowerCase();
  b = (b.title || '').toLowerCase(); 
  if(sort) return (a > b) ? 1 : ((a < b) ? -1 : 0);
  else return (a > b) ? -1 : ((a < b) ? 1 : 0);
  });
  return;
  }
  
  sortArray.sort(function (a, b) {
  a = (a.title || '').toLowerCase();
  b = (b.title || '').toLowerCase(); 
  if(sort) return (a > b) ? 1 : ((a < b) ? -1 : 0);
  else return (a > b) ? -1 : ((a < b) ? 1 : 0);
  });
}

onIndustryCancel(){
   this.industryList = [];
   this.hideIndustryField = false;
}

onFunctionCancel(){
   this.functionList = [];
   this.hideFunctionField = false;
}

onLocationCancel(){
   this.locationList = [];
   this.hideLocationField = false;
}
}
