import { Injectable } from '@angular/core';
import { RestfulwebService} from '../../shared/services/restfulweb.service';
import { RestfulLaravelService} from '../../shared/services/restful-laravel.service';


@Injectable()
export class JobsComponentService {

  constructor(public restfulWebService:RestfulwebService,public restfulLaravelService:RestfulLaravelService) { }

  getLists(location):any{
   return this.restfulWebService.get(`jobFunctionsLocationsIndustries/${location}`)
  }

  deleteData(parameter):any{
    return this.restfulWebService.delete(`delete/${parameter}`);
  }

  addEditData(parameter):any{
    return this.restfulWebService.post('addEditData',parameter);
  }

  getLocation(location):any{
   return this.restfulWebService.get(`locationDetails/${location}`);
  }

  getJobFunctions(){
    return this.restfulLaravelService.get('admin/list-job-function');
  }

  addEditJobFunctionData(jobFunction){
    return this.restfulLaravelService.post('admin/create-job-function',jobFunction);
  }

  deleteJobFunction(id){
    return this.restfulLaravelService.delete(`admin/delete-job-function/${id}`);
  }

  getLocations(){
    return this.restfulLaravelService.get('admin/list-location');
  }

  addEditLocationData(location){
    return this.restfulLaravelService.post('admin/create-job-location',location);
  }

  deleteLocation(id){
    return this.restfulLaravelService.delete(`admin/delete-job-location/${id}`);
  }

   getIndustries(reqBody){
    return this.restfulLaravelService.get(`admin/job-industry-list/${reqBody}`);
  }

  addEditIndustryData(industry){
    return this.restfulLaravelService.post('admin/job-industry-add-edit',industry);
  }

  deleteIndustry(id){
    return this.restfulLaravelService.delete(`admin/job-industry-delete/${id}`);
  }

  getEmployementTypes(){
    return this.restfulLaravelService.get('admin/employment-type-list');
  }

  updateEmployementType(type){
    return this.restfulLaravelService.put('admin/update-employment-type-all',type);
  }

}
