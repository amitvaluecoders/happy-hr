import { Injectable } from '@angular/core';
import { RestfulwebService} from '../../shared/services/restfulweb.service';
import { RestfulLaravelService } from '../../shared/services/restful-laravel.service';


@Injectable()
export class TrophyService {

  constructor(public restfulWebService:RestfulwebService,public restfulLaravelService:RestfulLaravelService) { }

  getTrophies(trophy):any{
   return this.restfulLaravelService.get(`admin/list-base-trophy/${encodeURIComponent(trophy)}`);
  }

  deleteTrophy(trophy):any{
    return this.restfulLaravelService.delete(`admin/delete-base-trophy/${trophy}`);
  }

  addEditTrophy(trophy):any{
    return this.restfulLaravelService.post('admin/add-update-base-trophy',trophy);
  }

  getTrophy(trophy):any{
   return this.restfulLaravelService.get(`admin/get-trophy-detail/${trophy}`);
  }

}
