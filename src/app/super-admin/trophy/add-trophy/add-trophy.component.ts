import { Component, OnInit } from '@angular/core';
import { TrophyService } from '../trophy.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { CoreService } from '../../../shared/services/core.service';
import { AwardService } from '../../awards/award.service';

@Component({
  selector: 'app-add-trophy',
  templateUrl: './add-trophy.component.html',
  styleUrls: ['./add-trophy.component.scss']
})
export class AddTrophyComponent implements OnInit {
  
  public hideCheck1=true;
  public hideCheck2=true;
  public hideCheck3=true;
  public hideCheck4=true;
  public hideCheck5=true;
  public hideCheck6=true;
  public hideCheck7=true;
  public hideCheck8=true;
  public hideCheck9=true;
  public hideCheck10=true;
  
  public isProcessing=false;
  public isEditProcessing=false;

  public trophy={
    baseTrophyID:"",
    title:"",
    trophyReason:"",
    trophyIcon:"0",
    trophyAwardFor:[]
  }

    public first = "A";
  public last = "Z";
  public chars = [];

  constructor( public awardService: AwardService,public trophyService:TrophyService,private router:Router,private activatedRoute:ActivatedRoute,public coreService:CoreService) { }
  public id;
  public buttonText="Add";
  public pageTitle="Create";

  ngOnInit() {
    for (var i = this.first.charCodeAt(0); i <= this.last.charCodeAt(0); i++) {
      this.chars.push(String.fromCharCode(i));
    }
    if(this.activatedRoute.snapshot.params['id']!= undefined){
      this.id=this.activatedRoute.snapshot.params['id'];
      this.buttonText="Save";
      this.pageTitle="Edit"
      this.getTrophyForUpdate();
    }
  }

   public dropDownListType=[{id:"Manager",itemName:"Manager"},
    {id:"Employee",itemName:"Employee"}]
  
  public selectedRolesForAwards=[];

// multi select dropdown arrays and setting object.
  public dropDownListSubscriptionPlans=[];
  public dropDownListSupportServices=[];
  public dropDownSettingMultiSelect={
      singleSelection: false,
      text: "Select role(s) *",
      enableCheckAll:false,
      enableSearchFilter: true,
      classes: "myclass custom-class"               
  }

  // on selecting any role(s) from dropdown. 
   onSelectTrophyFor(e){
      this.trophy.trophyAwardFor.push(e.itemName);
  }

// on deselecting role(s) from dropdown.   
   onDeSelectTrophyFor(e){
    this.trophy.trophyAwardFor=this.trophy.trophyAwardFor.filter(item=>item!=e.itemName);
    //this.trophy.trophyFor=[];
  }

// on selecting all roles from dropdown.
  onSelectAlltrophies(e){
    for(let name of this.dropDownListType)
  this.trophy.trophyAwardFor.push(name.itemName);
}
  

  onTrophySelect(e){ 
    if(e.srcElement.id=="blue") {
      this.hideCheck1=!this.hideCheck1;
      this.hideCheck2=true;
      this.hideCheck3=true;
      this.hideCheck4=true;
      this.hideCheck5=true;
      this.hideCheck6=true;
      this.hideCheck7=true;
      this.hideCheck8=true;
      this.hideCheck9=true;
      this.hideCheck10=true;      
      this.trophy.trophyIcon='';
      if(!this.hideCheck1) this.trophy.trophyIcon=e.srcElement.name;}
    if(e.srcElement.id=="pink") {
      this.hideCheck2=!this.hideCheck2;
      this.hideCheck1=true;
      this.hideCheck3=true;
      this.hideCheck4=true;
      this.hideCheck5=true;
      this.hideCheck6=true;
      this.hideCheck7=true;
      this.hideCheck8=true;
      this.hideCheck9=true;
      this.hideCheck10=true;     
      this.trophy.trophyIcon='';
      if(!this.hideCheck2) this.trophy.trophyIcon=e.srcElement.name;}
    if(e.srcElement.id=="orange") {
      this.hideCheck3=!this.hideCheck3;
      this.hideCheck1=true;
      this.hideCheck2=true;
      this.hideCheck4=true;
      this.hideCheck5=true;
      this.hideCheck6=true;
      this.hideCheck7=true;
      this.hideCheck8=true;
      this.hideCheck9=true;
      this.hideCheck10=true;     
      this.trophy.trophyIcon='';
      if(!this.hideCheck3) this.trophy.trophyIcon=e.srcElement.name;}
    if(e.srcElement.id=="yellow") {
      this.hideCheck4=!this.hideCheck4;
      this.hideCheck1=true;
      this.hideCheck2=true;
      this.hideCheck3=true;
      this.hideCheck5=true;
      this.hideCheck6=true;
      this.hideCheck7=true;
      this.hideCheck8=true;
      this.hideCheck9=true;
      this.hideCheck10=true;     
      this.trophy.trophyIcon='';
      if(!this.hideCheck4) this.trophy.trophyIcon=e.srcElement.name;}
      if(e.srcElement.id=="cyan") {
        this.hideCheck5=!this.hideCheck5;
        this.hideCheck1=true;
        this.hideCheck2=true;
        this.hideCheck3=true;
        this.hideCheck4=true;
        this.hideCheck6=true;
        this.hideCheck7=true;
        this.hideCheck8=true;
        this.hideCheck9=true;
        this.hideCheck10=true;     
        this.trophy.trophyIcon='';
        if(!this.hideCheck5) this.trophy.trophyIcon=e.srcElement.name;}
        if(e.srcElement.id=="skyblue") {
          this.hideCheck6=!this.hideCheck6;
          this.hideCheck1=true;
          this.hideCheck2=true;
          this.hideCheck3=true;
          this.hideCheck5=true;
          this.hideCheck4=true;
          this.hideCheck7=true;
          this.hideCheck8=true;
          this.hideCheck9=true;
          this.hideCheck10=true;     
          this.trophy.trophyIcon='';
          if(!this.hideCheck6) this.trophy.trophyIcon=e.srcElement.name;}
          if(e.srcElement.id=="red") {
            this.hideCheck7=!this.hideCheck7;
            this.hideCheck1=true;
            this.hideCheck2=true;
            this.hideCheck3=true;
            this.hideCheck5=true;
            this.hideCheck6=true;
            this.hideCheck4=true;
            this.hideCheck8=true;
            this.hideCheck9=true;
            this.hideCheck10=true;     
            this.trophy.trophyIcon='';
            if(!this.hideCheck7) this.trophy.trophyIcon=e.srcElement.name;}
            if(e.srcElement.id=="green") {
              this.hideCheck8=!this.hideCheck8;
              this.hideCheck1=true;
              this.hideCheck2=true;
              this.hideCheck3=true;
              this.hideCheck5=true;
              this.hideCheck6=true;
              this.hideCheck7=true;
              this.hideCheck4=true;
              this.hideCheck9=true;
              this.hideCheck10=true;     
              this.trophy.trophyIcon='';
              if(!this.hideCheck8) this.trophy.trophyIcon=e.srcElement.name;}
              if(e.srcElement.id=="grey") {
                this.hideCheck9=!this.hideCheck9;
                this.hideCheck1=true;
                this.hideCheck2=true;
                this.hideCheck3=true;
                this.hideCheck5=true;
                this.hideCheck6=true;
                this.hideCheck7=true;
                this.hideCheck8=true;
                this.hideCheck4=true;
                this.hideCheck10=true;     
                this.trophy.trophyIcon='';
                if(!this.hideCheck9) this.trophy.trophyIcon=e.srcElement.name;}
                if(e.srcElement.id=="purple") {
                  this.hideCheck10=!this.hideCheck10;
                  this.hideCheck1=true;
                  this.hideCheck2=true;
                  this.hideCheck3=true;
                  this.hideCheck5=true;
                  this.hideCheck6=true;
                  this.hideCheck7=true;
                  this.hideCheck8=true;
                  this.hideCheck9=true;
                  this.hideCheck4=true;     
                  this.trophy.trophyIcon='';
                  if(!this.hideCheck10) this.trophy.trophyIcon=e.srcElement.name;}
  }
  
  // add/edit trophy
  saveEditTrophy(){
    this.isProcessing=true;
     this.trophyService.addEditTrophy(this.trophy).subscribe(
      data=>{
        this.router.navigate(['/app/super-admin/trophy']);
        this.coreService.notify("Successful",data.message,1),
        this.isProcessing=false;        
      },
      error=>{
        this.isProcessing=false;                
        this.coreService.notify("Unsuccessful",error.message,0);},
      ()=>{}
    )
  }

  // get trophy details
  getTrophyForUpdate(){
  this.isEditProcessing=true;
    this.trophyService.getTrophy(this.id).subscribe(
      data=>{
      if (data.code == "200") {
                  this.trophy=data.data;
                  let icon=this.trophy.trophyIcon.split('/');
                  this.trophy.trophyIcon=icon[icon.length-1];
                  this.trophy.trophyAwardFor.filter(item=>{ 
                  this.selectedRolesForAwards.push({id:item,itemName:item});
                })

      if(this.trophy.trophyIcon.includes("color-blue.png")) {      
      this.hideCheck1=false;
      this.hideCheck2=true;
      this.hideCheck3=true;
      this.hideCheck4=true;
      this.hideCheck5=true;
      this.hideCheck6=true;
      this.hideCheck7=true;
      this.hideCheck8=true;
      this.hideCheck9=true;
      this.hideCheck10=true;  
      }
      if(this.trophy.trophyIcon.includes("color-pink.png")) {            
      this.hideCheck2=false;
      this.hideCheck1=true;
      this.hideCheck3=true;
      this.hideCheck4=true;
      this.hideCheck5=true;
      this.hideCheck6=true;
      this.hideCheck7=true;
      this.hideCheck8=true;
      this.hideCheck9=true;
      this.hideCheck10=true;  
      }
      if(this.trophy.trophyIcon.includes("color-orange.png")) {            
      this.hideCheck3=false;
      this.hideCheck1=true;
      this.hideCheck2=true;
      this.hideCheck4=true;
      this.hideCheck5=true;
      this.hideCheck6=true;
      this.hideCheck7=true;
      this.hideCheck8=true;
      this.hideCheck9=true;
      this.hideCheck10=true;  
      }
      if(this.trophy.trophyIcon.includes("color-yellow.png")) {      
      this.hideCheck4=false;
      this.hideCheck1=true;
      this.hideCheck2=true;
      this.hideCheck3=true;
      this.hideCheck5=true;
      this.hideCheck6=true;
      this.hideCheck7=true;
      this.hideCheck8=true;
      this.hideCheck9=true;
      this.hideCheck10=true;  
      }

      if(this.trophy.trophyIcon.includes("color-cyan.png")) {      
        this.hideCheck1=true;
        this.hideCheck2=true;
        this.hideCheck3=true;
        this.hideCheck4=true;
        this.hideCheck5=false;
        this.hideCheck6=true;
        this.hideCheck7=true;
        this.hideCheck8=true;
        this.hideCheck9=true;
        this.hideCheck10=true;  
        }

        if(this.trophy.trophyIcon.includes("color-skyblue.png")) {      
          this.hideCheck1=true;
          this.hideCheck2=true;
          this.hideCheck3=true;
          this.hideCheck4=true;
          this.hideCheck5=true;
          this.hideCheck6=false;
          this.hideCheck7=true;
          this.hideCheck8=true;
          this.hideCheck9=true;
          this.hideCheck10=true;  
          }

          if(this.trophy.trophyIcon.includes("color-red.png")) {      
            this.hideCheck1=true;
            this.hideCheck2=true;
            this.hideCheck3=true;
            this.hideCheck4=true;
            this.hideCheck5=true;
            this.hideCheck6=true;
            this.hideCheck7=false;
            this.hideCheck8=true;
            this.hideCheck9=true;
            this.hideCheck10=true;  
            }

            if(this.trophy.trophyIcon.includes("color-green.png")) {      
              this.hideCheck1=true;
              this.hideCheck2=true;
              this.hideCheck3=true;
              this.hideCheck4=true;
              this.hideCheck5=true;
              this.hideCheck6=true;
              this.hideCheck7=true;
              this.hideCheck8=false;
              this.hideCheck9=true;
              this.hideCheck10=true;  
              }

              if(this.trophy.trophyIcon.includes("color-grey.png")) {      
                this.hideCheck1=true;
                this.hideCheck2=true;
                this.hideCheck3=true;
                this.hideCheck4=true;
                this.hideCheck5=true;
                this.hideCheck6=true;
                this.hideCheck7=true;
                this.hideCheck8=true;
                this.hideCheck9=false;
                this.hideCheck10=true;  
                }

                if(this.trophy.trophyIcon.includes("color-purple.png")) {      
                  this.hideCheck1=true;
                  this.hideCheck2=true;
                  this.hideCheck3=true;
                  this.hideCheck4=true;
                  this.hideCheck5=true;
                  this.hideCheck6=true;
                  this.hideCheck7=true;
                  this.hideCheck8=true;
                  this.hideCheck9=true;
                  this.hideCheck10=false;  
                  }
     this.isEditProcessing=false;      
        }
                else {
                  this.isEditProcessing=false;
                  this.coreService.notify("Unsuccessful",data.message,0);
      }},
      error=>{this.isEditProcessing=false;this.coreService.notify("Unsuccessful",error.message,0);},
      ()=>{}
    )
  }
 
 // navigates to trophy list page
  onCancel(){
    this.router.navigate(['/app/super-admin/trophy']);
  }

  public awards=[];

  // get all awards
  getAwards() {
    this.isEditProcessing=true;
    let i=0;
   let reqParams={filter: { awards: {} }};
    this.awardService.getAwards(JSON.stringify(reqParams)).subscribe(
      data => {
        if (data.code == "200") {
          if(data.data){
          this.awards = data.data;
        }
        if(this.dropDownListType.length==0) this.isEditProcessing=true;
        else this.isEditProcessing=false;
        }
      },
      error =>
        {
          this.isEditProcessing=false;        
          this.coreService.notify(
          "Unsuccessful",
         error.message,
          0
        )},
      () => {}
    );
  }

}
