import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TrophyListingComponent } from './trophy-listing.component';

describe('TrophyListingComponent', () => {
  let component: TrophyListingComponent;
  let fixture: ComponentFixture<TrophyListingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TrophyListingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TrophyListingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
