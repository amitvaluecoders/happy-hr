import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { TrophyService } from '../trophy.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { CoreService } from '../../../shared/services/core.service';
import { ConfirmService } from '../../../shared/services/confirm.service';

@Component({
  selector: 'app-trophy-listing',
  templateUrl: './trophy-listing.component.html',
  styleUrls: ['./trophy-listing.component.scss']
})
export class TrophyListingComponent implements OnInit {
  
  public trophies=[];
  public searchKey='';
  public isProcessing=false;
  public isDeleting=false;  
  public cancelRequest;
  permission;
  constructor(public trophyService:TrophyService,public router:Router, public coreService:CoreService, public confirmService:ConfirmService, public viewContainerRef:ViewContainerRef ) { }

  ngOnInit() {
    this.getTrophies();
    this.permission=this.coreService.getNonRoutePermission();
  }
  
  // get all trophies
  getTrophies(){
    let searchText='';
    if(this.searchKey) searchText=encodeURIComponent(this.searchKey);  
  this.isProcessing=true;
  let trophyParameter={
    offset:'0',
    limit:"1000000",
    search:searchText
  }  
  this.cancelRequest=this.trophyService.getTrophies(JSON.stringify(trophyParameter)).subscribe(
      data=>{
         if (data.code == "200") {
        this.trophies=data.data;
        this.isProcessing=false;
         }
         else {
           this.isProcessing=false;this.coreService.notify("Unsuccessful",data.message,0);
         }      
    },
    error=>{
      this.isProcessing=false;      
      this.coreService.notify("Unsuccessful",error.message,0);
    },
    ()=>{});
  }

  // delete trophy
  deleteTrophy(pd, index) {
  this.confirmService.confirm(this.confirmService.tilte,this.confirmService.message,this.viewContainerRef)
    .subscribe(res=>{
       let result=res;
    if(result){
      this.isProcessing=true;
    this.trophyService.deleteTrophy(JSON.stringify(pd)).subscribe(
      data => {
        if (data.code == "200") {
          this.getTrophies();
          this.coreService.notify("Successful",data.message,1)
        }
      },
      error =>{this.coreService.notify("Unsuccessful",error.message,0); this.isProcessing=false;},
      () => { }
    )
    }
    });  
  }

  // search trophies list
  onSearch(){
    this.cancelRequest.unsubscribe();
    this.getTrophies();
  }

}
