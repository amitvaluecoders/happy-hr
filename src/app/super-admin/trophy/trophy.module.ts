import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { MaterialModule } from '@angular/material';
import { DataTableModule} from "angular2-datatable";
import { TrophyListingComponent } from './trophy-listing/trophy-listing.component';
import { AddTrophyComponent } from './add-trophy/add-trophy.component';
import { AngularMultiSelectModule } from 'angular2-multiselect-checkbox-dropdown/angular2-multiselect-dropdown';
import { TrophyPagesRoutingModule } from './trophy.routing';
import { TrophyService } from './trophy.service';
import { AwardService } from '../awards/award.service';
import { TrophyGuardService } from './trophy-guard.service';
 
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    MaterialModule,
    DataTableModule,
    AngularMultiSelectModule,
    TrophyPagesRoutingModule
  ],
  declarations: [TrophyListingComponent, AddTrophyComponent],
  providers:[TrophyService,AwardService,TrophyGuardService]
})
export class TrophyModule { }
