import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from '@angular/material';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule }  from '@angular/router';
import { TrophyListingComponent } from './trophy-listing/trophy-listing.component';
import { AddTrophyComponent } from './add-trophy/add-trophy.component';
import { TrophyGuardService } from './trophy-guard.service';

export const TrophyPagesRoutes: Routes = [
    {
        path: '',
        children: [
            { path: '', component: TrophyListingComponent,canActivate:[TrophyGuardService] },
            { path: 'add', component: AddTrophyComponent,canActivate:[TrophyGuardService] },
            { path: 'edit/:id', component: AddTrophyComponent,canActivate:[TrophyGuardService] },
        ]
    }
];

export const TrophyPagesRoutingModule = RouterModule.forChild(TrophyPagesRoutes);
