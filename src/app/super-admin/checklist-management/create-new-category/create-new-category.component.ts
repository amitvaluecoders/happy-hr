import { Component, OnInit } from '@angular/core';
import { MdSnackBar, MdDialog, MdDialogRef } from '@angular/material';
import { CheckListService } from '../checklist-management.service';
import { CoreService} from '../../../shared/services';


@Component({
  selector: 'app-create-new-category',
  templateUrl: './create-new-category.component.html',
  styleUrls: ['./create-new-category.component.scss']
})
export class CreateNewCategoryComponent implements OnInit {
  
  // public category={
  //   type:"",
  //   name:""
  // }
public types=[];
  public category={
    title: "",
    sortOrder: 1,
    masterChecklistID:'',
    masterChecklistCategoryID:''
  }

  public isProcessing=false;
  public addProcessing=false;

  constructor(//public dialogRef: MdDialogRef<CreateNewCategoryComponent>,
              public checkListService:CheckListService,
              public coreServices:CoreService) { }

  ngOnInit() {
    this.getCategoryTypes();
  }

  getCategoryTypes(){
    this.isProcessing=true;
  this.checkListService.getCategoryTypes().subscribe( d=>{
      if(d.code=="200"){
        this.types=d.data;
        console.log("Types",this.types);
       //this.coreServices.notify("Sucessfull",d.message,1);
      // this.dialogRef.close(this.category.title);
       this.isProcessing=false;
      }else {this.isProcessing=false;this.coreServices.notify("Unsuccessful",d.message,0)}
    },
    e=>{this.isProcessing=false;this.coreServices.notify("Unsuccessful",e.message,0)}) 
  }

  onSave(isvalid,form){
    this.addProcessing=true
    if(isvalid){
      console.log("catrgory on ",this.category)
      this.checkListService.addCategory(this.category).subscribe( d=>{
      if(d.code=="200"){
        form.reset();
        this.addProcessing=false;
       this.coreServices.notify("Successful",d.message,1);
      // this.dialogRef.close(this.category.title);
       
      }else {this.addProcessing=false;this.coreServices.notify("Unsuccessful",d.message,0)}
    },
    e=>{this.addProcessing=false;this.coreServices.notify("Unsuccessful",e.message,0)})      
    }
  }

  onCancel(form){
    form.reset();
    //this.dialogRef.close(false);
  }

}
