import { Component, OnInit } from '@angular/core';
import { CheckListService } from '../checklist-management.service';
import { MdSnackBar, MdDialog, MdDialogRef } from '@angular/material';
import { CoreService} from '../../../shared/services';

@Component({
  selector: 'app-create-list',
  templateUrl: './create-list.component.html',
  styleUrls: ['./create-list.component.scss']
})
export class CreateListComponent implements OnInit {

  public item={
        masterChecklistCategoryID:null,
        columnA:"",
        columnB:"",
        columnC:"",
        masterChecklistCategoryRowID:''
}

  public categoryId:String;
  public rowData:any;
  public type:String;
  public listName:String;
  public isProcessing=false;
  title="Create new question";
  buttonText="Add";

  constructor(public dialog: MdDialog,public checkListService: CheckListService,
              public coreService:CoreService,
              public dialogRef: MdDialogRef<CreateListComponent>) { }

  ngOnInit() {
   if(this.categoryId) {this.item.masterChecklistCategoryID=this.categoryId;}
   if(this.rowData) {
    this.item.columnA=this.rowData.columnA;
    this.item.columnB=this.rowData.columnB;
    this.item.columnC=this.rowData.columnC;
    this.item.masterChecklistCategoryRowID=this.rowData.masterChecklistCategoryRowID;
     this.title="Edit question";
     this.buttonText="Save";
   }
  }

  // for adding list item  
  onAdd(isvalid){
    if(isvalid){
      this.isProcessing=true;
      console.log("item",this.item);
      //let reqBody={categoryId:this.categoryId,type:this.type,listName:this.listName};
    this.checkListService.addCheckListCategoryItem(this.item).subscribe(
      d=>{
        if(d.code="200"){
          this.isProcessing=false; 
          console.log("data",d);         
          this.dialogRef.close(d.data);
          this.coreService.notify("Successful",d.message,1)
        }
        else this.coreService.notify("Unsuccessful",d.message,0)
      },
      e=>{
        this.isProcessing=false;        
        this.coreService.notify("Unsuccessful",e.message,0)}
      )
    }
  }
}
