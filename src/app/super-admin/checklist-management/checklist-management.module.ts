import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from '@angular/material';
import { MdSnackBar, MdDialog, MdDialogRef } from '@angular/material';
import {DataTableModule} from "angular2-datatable";
import { FormsModule } from '@angular/forms'; 
import { MasterOnBoardingChecklistComponent } from './master-on-boarding-checklist/master-on-boarding-checklist.component';
import { MasterOffBoardingChecklistComponent } from './master-off-boarding-checklist/master-off-boarding-checklist.component';
import { CreateNewCategoryComponent } from './create-new-category/create-new-category.component';
import { CreateListComponent } from './create-list/create-list.component';
import { ColumnComponent } from './column/column.component';
import { AddItemsComponent } from './add-items/add-items.component';
import { ChecklistManagementPagesRoutingModule } from './checklist-management.routing';
import { CheckListService } from './checklist-management.service';
import { SharedModule } from '../../shared/shared.module';
import { InlineEditorModule} from 'ng2-inline-editor';
import { ChecklistManagementGuardService } from './checklist-management-guard.service';

@NgModule({
  imports: [
    SharedModule,
    InlineEditorModule,
    FormsModule,
    CommonModule,
    MaterialModule,
    DataTableModule,
    DataTableModule,
    ChecklistManagementPagesRoutingModule
  ],
  entryComponents: [
    CreateNewCategoryComponent,
    ColumnComponent,AddItemsComponent
  ],
  providers:[CheckListService,ChecklistManagementGuardService],
  declarations: [MasterOnBoardingChecklistComponent, MasterOffBoardingChecklistComponent, CreateNewCategoryComponent, CreateListComponent, ColumnComponent, AddItemsComponent]
})
export class ChecklistManagementModule { }
