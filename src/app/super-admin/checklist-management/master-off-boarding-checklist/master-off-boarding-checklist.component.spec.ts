import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MasterOffBoardingChecklistComponent } from './master-off-boarding-checklist.component';

describe('MasterOffBoardingChecklistComponent', () => {
  let component: MasterOffBoardingChecklistComponent;
  let fixture: ComponentFixture<MasterOffBoardingChecklistComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MasterOffBoardingChecklistComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MasterOffBoardingChecklistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
