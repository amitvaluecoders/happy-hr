import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { CoreService} from '../../../shared/services';
import { CheckListService } from '../checklist-management.service';
import { MdSnackBar, MdDialog, MdDialogRef } from '@angular/material';
import { CreateNewCategoryComponent } from '../create-new-category/create-new-category.component'; 
import { ColumnComponent } from '../column/column.component';
import { CreateListComponent } from '../create-list/create-list.component';
import { ConfirmService } from '../../../shared/services/confirm.service';
@Component({
    selector: 'app-master-off-boarding-checklist',
    templateUrl: './master-off-boarding-checklist.component.html',
    styleUrls: ['./master-off-boarding-checklist.component.scss']
  })
  export class MasterOffBoardingChecklistComponent implements OnInit {
  
  public collapsebox = [];
  public opencollapse;

  public checkList: any;
  public typeID = '';
  public isProcessing = false;
  public addColumnProcessing = false;
  public oldChecklist = [];
  permission;
  constructor(public coreService: CoreService,
    public checkListService: CheckListService,
    public snackBar: MdSnackBar,
    public confirmService: ConfirmService,
    public viewContainerRef: ViewContainerRef,
    public dialog: MdDialog, ) { }

  ngOnInit() {
    this.getCheckListDetails();
    this.permission = this.coreService.getNonRoutePermission();
  }
 
  //get checklist details
  getCheckListDetails() {
    this.isProcessing = true;
    this.checkListService.getMasterCheckListOffBoardDetails().subscribe(
      d => {
        if (d.code == "200") {
          this.checkList = d.data;
          this.oldChecklist = JSON.parse(JSON.stringify(this.checkList));
          this.typeID = d.data.masterChecklistID;
          this.isProcessing = false;
        } else { this.isProcessing = false; this.coreService.notify("Unsuccessful", d.message, 0) }
      },
      e => { this.isProcessing = false; this.coreService.notify("Unsuccessful", e.message, 0) })
  }

 // delete category
  onDeleteCheckListCategory(categoryID) {
    this.confirmService.confirm(
      this.confirmService.tilte,
      this.confirmService.message,
      this.viewContainerRef
    )
      .subscribe(res => {
        let result = res;
        if (result) {
          this.isProcessing = true;
          this.checkListService.deleteCheckListCategory(categoryID).subscribe(
            d => {
              if (d.code == "200") {
                this.getCheckListDetails();
                this.coreService.notify("Successful", d.message, 1)
              } else this.coreService.notify("Unsuccessful", d.message, 0)
            },
            e => { this.isProcessing = false; this.coreService.notify("Unsuccessful", "error while deleteing column", 0) })
        }
      })
  }

  // delete column
  onDeleteColumns(columnID) {
    this.confirmService.confirm(
      this.confirmService.tilte,
      this.confirmService.message,
      this.viewContainerRef
    )
      .subscribe(res => {
        let result = res;
        if (result) {
          this.checkListService.deleteCheckListCategoryColumn(columnID).subscribe(
            d => {
              if (d.code == "200") {
                this.getCheckListDetails();
                this.coreService.notify("Successful", d.message, 1)
              } else this.coreService.notify("Unsuccessful", d.message, 0)
            },
            e => this.coreService.notify("Unsuccessful", e.message, 0))
        }
      })
  }

  // delete list item
  onDeleteListitems(rowID) {
    this.confirmService.confirm(
      this.confirmService.tilte,
      this.confirmService.message,
      this.viewContainerRef
    )
      .subscribe(res => {
        if (res) {
          this.isProcessing = true;
          this.checkListService.deleteCheckListCategoryItem(rowID).subscribe(
            d => {
              if (d.code == "200") {
                this.getCheckListDetails();
                this.coreService.notify("Successful", d.message, 1)
              } else this.coreService.notify("Unsuccessful", d.message, 0)
            },
            e => { this.isProcessing = false; this.coreService.notify("Unsuccessful", e.message, 0) })
        }
      })
  }

  // add column in category
  onAddColumn(categoryId) {
    let dialogRef = this.dialog.open(ColumnComponent, {
      width: '600px'
    });
    dialogRef.componentInstance.type = "1";
    dialogRef.componentInstance.categoryId = categoryId;
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.addColumnProcessing = true;
        this.getCheckListDetails();
        this.addColumnProcessing = false;
      }
    });
  }
  
  // opens dialog box to add item in category
  onAddItem(categoryId) {
    let dialogRef = this.dialog.open(CreateListComponent, {
      width: '600px'
    });
    dialogRef.componentInstance.type = "onboard";
    dialogRef.componentInstance.categoryId = categoryId;
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        for (let c of this.checkList["categories"]) {
          if (c["masterChecklistCategoryID"] == result["masterChecklistCategoryID"]) {
          }
        }
      }
    });
  }
  
  // opens dialog box to edit item
  onEditItem(categoryId, rowData) {
    let dialogRef = this.dialog.open(CreateListComponent, {
      width: '600px'
    });
    dialogRef.componentInstance.type = "onboard";
    dialogRef.componentInstance.categoryId = categoryId;
    dialogRef.componentInstance.rowData = rowData;
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        for (let c of this.checkList["categories"]) {
          if (c["masterChecklistCategoryID"] == result["masterChecklistCategoryID"]) {
            for (let r of c["rows"]) {
              if (r["masterChecklistCategoryRowID"] == result["masterChecklistCategoryRowID"]) {
                r = result;
              }
            }
          }
        }
      }
    });
  }

  // edit category title
  onHeadingChange(headData) {
    let category = {
      title: headData.title,
      sortOrder: 1,
      masterChecklistID: this.typeID,
      masterChecklistCategoryID: headData.masterChecklistCategoryID
    }
    if (headData.title) {
      this.checkListService.addCategory(category).subscribe(d => {
        if (d.code == "200") {
          this.oldChecklist = JSON.parse(JSON.stringify(this.checkList));
          this.coreService.notify("Successful", d.message, 1);

        } else this.coreService.notify("Unsuccessful", d.message, 0)
      },
        e => this.coreService.notify("Unsuccessful", e.message, 0))
    } else {
      for (let c of this.oldChecklist["categories"]) {
        if (headData.masterChecklistCategoryID == c.masterChecklistCategoryID) {
          headData.title = c.title;
          this.coreService.notify("Unsuccessful", "Category title is required", 0)

        }
      }
    }
  }

 // edit column title
  onColumnChange(columnData) {

    let column = {
      columnID: columnData.columns.columnID,
      columnA: columnData.columns.columnA,
      columnB: columnData.columns.columnB
    }
    if (columnData.columns.columnA && columnData.columns.columnB) {
      this.checkListService.addCheckListCategoryColumn(column).subscribe(
        d => {
          if (d.code = "200") {
            this.oldChecklist = JSON.parse(JSON.stringify(this.checkList));

            this.coreService.notify("Successful", d.message, 1);
          }
          else this.coreService.notify("Unsuccessful", d.message, 0);
        },
        e => this.coreService.notify("Unsuccessful", e.message, 0))
    } else {

      for (let c of this.oldChecklist["categories"]) {
        if (c["columns"]) {
          if (columnData.columns.columnID == c.columns.columnID) {
            columnData.columns.columnA = c.columns.columnA;
            columnData.columns.columnB = c.columns.columnB;

            this.coreService.notify("Unsuccessful", "Column title is required", 0)
          }
        }
      }
    }
  }

  // edit item title
  onItemChange(categoryID, rowData) {
    let item = {
      masterChecklistCategoryID: categoryID,
      columnA: rowData.columnA,
      columnB: "",
      columnC: "",
      masterChecklistCategoryRowID: rowData.masterChecklistCategoryRowID
    }
    if (rowData.columnA) {
      this.checkListService.addCheckListCategoryItem(item).subscribe(
        d => {
          if (d.code = "200") {
            this.oldChecklist = JSON.parse(JSON.stringify(this.checkList));
            this.coreService.notify("Successful", d.message, 1)
          }
          else this.coreService.notify("Unsuccessful", d.message, 0)
        },
        e => this.coreService.notify("Unsuccessful", e.message, 0)
      )
    } else {

      for (let category of this.oldChecklist["categories"]) {
        if (category["rows"].length) {
          for (let r of category["rows"]) {
            if (rowData.masterChecklistCategoryRowID == r.masterChecklistCategoryRowID) {
              rowData.columnA = r.columnA;
              this.coreService.notify("Unsuccessful", "Item title is required", 0)
            }
          }
        }
      }
    }
  }
}

