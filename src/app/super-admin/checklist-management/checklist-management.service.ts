import { Injectable } from '@angular/core';
import { RestfulwebService } from '../../shared/services/restfulweb.service';
import { RestfulLaravelService } from '../../shared/services/restful-laravel.service';
// 

@Injectable()
export class CheckListService {

  
 constructor(
    private restfulWebService: RestfulwebService,
    private restfulLaravelService: RestfulLaravelService, 
  ){}

getCategoryTypes(){
  return this.restfulLaravelService.get('admin/master-checklist-get-list');
}

getMasterCheckListOnBoardDetails(){
  return this.restfulLaravelService.get('admin/master-checklist-onboarding-detail');
 }

 getMasterCheckListOffBoardDetails(){
  return this.restfulLaravelService.get('admin/master-checklist-offboarding-detail');
 }


addCategory(reqBody){
  return this.restfulLaravelService.post(`admin/master-checklist-category-addEdit`,reqBody);
 }

 deleteCheckListCategoryColumn(reqBody){
 return this.restfulLaravelService.delete(`admin/master-checklist-category-delete/${reqBody}`);
 }

 deleteCheckListCategoryItem(reqBody){
   return this.restfulLaravelService.delete(`admin/master-checklist-row-delete/${reqBody}`);
 }

 deleteCheckListCategory(reqBody){
   return this.restfulLaravelService.delete(`admin/master-checklist-category-delete/${reqBody}`);
 }

addCheckListCategoryColumn(reqBody){
   console.log("REq BOdy column",reqBody)
    return this.restfulLaravelService.put(`admin/master-checklist-update-column`,reqBody);
 }

 addCheckListCategoryItem(reqBody){
  return this.restfulLaravelService.post(`admin/master-checklist-row-addEdit`,reqBody);
}
 
}
