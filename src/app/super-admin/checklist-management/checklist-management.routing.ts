import { Routes, RouterModule }  from '@angular/router';
import { MasterOnBoardingChecklistComponent } from './master-on-boarding-checklist/master-on-boarding-checklist.component';
import { MasterOffBoardingChecklistComponent } from './master-off-boarding-checklist/master-off-boarding-checklist.component';
import { CreateNewCategoryComponent } from './create-new-category/create-new-category.component';
import { CreateListComponent } from './create-list/create-list.component';
import { ColumnComponent } from './column/column.component';
import { AddItemsComponent } from './add-items/add-items.component';
import { ChecklistManagementGuardService } from './checklist-management-guard.service';
export const ChecklistManagementPagesRoutes: Routes = [
    {
        path: '',
        children: [
            { path: '', component: MasterOnBoardingChecklistComponent,canActivate:[ChecklistManagementGuardService] },
            { path: 'offBoarding', component: MasterOffBoardingChecklistComponent,canActivate:[ChecklistManagementGuardService] },
            { path: 'create-list', component: CreateListComponent,canActivate:[ChecklistManagementGuardService] },
            { path: 'column', component: ColumnComponent,canActivate:[ChecklistManagementGuardService] },
            { path: 'add-item', component: AddItemsComponent,canActivate:[ChecklistManagementGuardService] },
            { path: 'new-category', component: CreateNewCategoryComponent,canActivate:[ChecklistManagementGuardService] },
          
        ]
    }
];

export const ChecklistManagementPagesRoutingModule = RouterModule.forChild(ChecklistManagementPagesRoutes);
