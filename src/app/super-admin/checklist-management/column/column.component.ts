import { Component, OnInit } from '@angular/core';
import { MdSnackBar, MdDialog, MdDialogRef } from '@angular/material';
import { CheckListService } from '../checklist-management.service';
import { CoreService} from '../../../shared/services';

@Component({
  selector: 'app-column',
  templateUrl: './column.component.html',
  styleUrls: ['./column.component.scss']
})
export class ColumnComponent implements OnInit {
   
  public type:String; 
  public categoryId:String;
  public column={
        title: "",
        sortOrder: 1,
        masterChecklistCategoryID:null,
        masterChecklistCategoryColumnID:''
}

  constructor(public dialog: MdDialog,public checkListService: CheckListService,
              public coreService:CoreService,
              public dialogRef: MdDialogRef<ColumnComponent>) { }
  
 
 // column:String;
  
  ngOnInit() {
   this.column.masterChecklistCategoryID=this.categoryId;
  }

  onAdd(isvalid){
    if(isvalid){
      console.log("COLUMNNNN",this.column);
      //let reqBody={type:this.type,categoryId:this.categoryId,column:this.column};
      this.checkListService.addCheckListCategoryColumn(this.column).subscribe(
        d=>{
          if(d.code="200") {
            this.coreService.notify("Successful","column added successfully",1);
            this.dialogRef.close(this.column.title);
          }
          else this.coreService.notify("Unsuccessful","wrror while adding coloum",0);
        },
        e=>this.coreService.notify("Unsuccessful","wrror while adding coloum",0))
      
    }
    
  }
}
