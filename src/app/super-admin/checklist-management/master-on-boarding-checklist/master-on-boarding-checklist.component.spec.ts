import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MasterOnBoardingChecklistComponent } from './master-on-boarding-checklist.component';

describe('MasterOnBoardingChecklistComponent', () => {
  let component: MasterOnBoardingChecklistComponent;
  let fixture: ComponentFixture<MasterOnBoardingChecklistComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MasterOnBoardingChecklistComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MasterOnBoardingChecklistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
