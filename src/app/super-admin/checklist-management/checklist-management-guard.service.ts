import { Injectable } from '@angular/core';
import {CanActivate,Router,RouterStateSnapshot,ActivatedRouteSnapshot} from "@angular/router";
import {CoreService} from '../../shared/services/core.service';
import {Observable} from 'rxjs/Rx'; 
import 'rxjs/add/operator/map';

@Injectable()
export class ChecklistManagementGuardService {

  public module = "checklist";
  
        constructor(private _core: CoreService, private router: Router){ }
  
        canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {   
            let url: string = state.url; 
            if(this.checkHavePermission(url)){
              return true;
            }else{
              this.router.navigate(['/extra/unauthorized']);
              return false;
            }
        }
  
        private checkHavePermission(path) {
          console.log("path",path)
            switch (path) {
                case "/app/super-admin/checklist":
                    return this._core.getModulePermission(this.module, 'view');
                case '/app/super-admin/checklist/offBoarding':
                    return this._core.getModulePermission(this.module, 'view');
                    case '/app/super-admin/checklist/create-list':
                    return this._core.getModulePermission(this.module, 'add');
                    case '/app/super-admin/checklist/column':
                    return this._core.getModulePermission(this.module, 'add');
                    case '/app/super-admin/checklist/new-category':
                    return this._core.getModulePermission(this.module, 'add');
                default:
                    return false;
            }
        }

}
