import { Injectable } from '@angular/core';
import { RestfulwebService } from '../../shared/services/restfulweb.service'


@Injectable()
export class CompanyService {

  constructor(
    private restfulWebService: RestfulwebService, 
  ){}

  editCompany(company){
    return this.restfulWebService.put(`company/edit`,company);    
  }

   getMessages(){
    return this.restfulWebService.get(`all/messages`);     
  }

  addMessages(message) {
    return this.restfulWebService.post(`send/messages`,message);         
  }
    

}