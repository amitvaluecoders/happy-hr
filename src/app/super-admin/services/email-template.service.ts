import { Injectable } from '@angular/core';
import { RestfulwebService } from '../../shared/services/restfulweb.service';

@Injectable()
export class EmailTemplateService {

 constructor(
    private restfulWebService: RestfulwebService, 
  ){}

  addEmailTemplate(template){
    return this.restfulWebService.post(`email-template/add`,template);
  }

  editEmailTemplate(template){
    return this.restfulWebService.put(`email-template/edit`,template);
  }
  
  deleteEmailTemplate(template){
    return this.restfulWebService.delete(`email-template/delete/${template}`);
  }

}
