import { Injectable } from '@angular/core';
import { RestfulLaravelService} from '../../shared/services/restful-laravel.service';

@Injectable()
export class AccountSettingService {

  constructor(public restfulWebService:RestfulLaravelService) { }
  
    getAdminDetails():any{
     return this.restfulWebService.get(`admin/account-detail`);
    }
  
    //  getAwards():any{
    //  return this.restfulWebService.get('admin/award-list');
    // }
  
    updatePassword(reqBody):any{
      return this.restfulWebService.put('admin/update-password',reqBody);
    }


    updateAdmin(reqBody):any{
      return this.restfulWebService.put('admin/update-account-detail',reqBody);
    }

  
    // addEditAward(award):any{
    //   return this.restfulWebService.post('admin/create-award',award);
    // }
  

}
