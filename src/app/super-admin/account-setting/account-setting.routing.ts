import { Routes, RouterModule }  from '@angular/router';
import { AccountSettingComponent } from './account-setting/account-setting.component';
import { ZohoAccountSettingsComponent } from './zoho-account-settings/zoho-account-settings.component';
import { AccountSettingGuardService } from './account-setting-guard.service';
export const AccountSettingPagesRoutes: Routes = [
    {
        path: '',
        children: [
            { path: '', component: AccountSettingComponent,canActivate:[AccountSettingGuardService]},
            { path: 'zoho', component: ZohoAccountSettingsComponent,canActivate:[AccountSettingGuardService]},
        ]
    }
];

export const AccountSettingPagesRoutingModule = RouterModule.forChild(AccountSettingPagesRoutes);
