import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ZohoAccountSettingsComponent } from './zoho-account-settings.component';

describe('ZohoAccountSettingsComponent', () => {
  let component: ZohoAccountSettingsComponent;
  let fixture: ComponentFixture<ZohoAccountSettingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ZohoAccountSettingsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ZohoAccountSettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
