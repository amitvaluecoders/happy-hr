import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from '@angular/material';
import { FormsModule } from '@angular/forms';
import { DataTableModule} from "angular2-datatable";
import { AccountSettingComponent, UpdatePasswordDialog,EditAdminDialog} from './account-setting/account-setting.component';
import { ZohoAccountSettingsComponent } from './zoho-account-settings/zoho-account-settings.component';
import { AccountSettingPagesRoutingModule } from './account-setting.routing';
import { AccountSettingService } from './account-setting.service';
import { AccountSettingGuardService } from './account-setting-guard.service';
@NgModule({
  imports: [
    CommonModule,
    MaterialModule,
    FormsModule,
    DataTableModule,
    AccountSettingPagesRoutingModule
  ],
  entryComponents:[UpdatePasswordDialog,EditAdminDialog],
  declarations: [AccountSettingComponent, ZohoAccountSettingsComponent, UpdatePasswordDialog,EditAdminDialog],
  providers:[AccountSettingService, AccountSettingGuardService]
})
export class AccountSettingModule { }
