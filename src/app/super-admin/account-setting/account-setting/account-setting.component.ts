import { Component, OnInit } from '@angular/core';
import { MdSnackBar, MdDialog, MdDialogRef } from '@angular/material';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { AccountSettingService } from '../account-setting.service';
import { CoreService } from '../../../shared/services/core.service';
import { Cookie } from 'ng2-cookies';
@Component({
    selector: 'app-account-setting',
    templateUrl: './account-setting.component.html',
    styleUrls: ['./account-setting.component.scss']
})
export class AccountSettingComponent implements OnInit {

    admin = {
        firstName: '',
        lastName: '',
        email: ''
    }

    isProcessing = false;
    permission;
    constructor(private router: Router, public dialog: MdDialog, private accountSettingService: AccountSettingService, private coreService: CoreService) { }

    ngOnInit() {
        this.getAdminDetails();
        this.permission = this.coreService.getNonRoutePermission();
    }

    //opens the dialog box to update password
    onClick() {
        let dialogRef = this.dialog.open(UpdatePasswordDialog, { width: '500px' });
        dialogRef.afterClosed().subscribe(result => {
            if (result) {
                if (result.value == "success") {
                    this.coreService.notify('Successful', result.message, 1);
                } else if (result.value == "failure") {
                    this.coreService.notify('Unsuccessful', result.message, 0)
                } else { }
            }
        });
    }

    // opens the dialog box to update admin details
    editDialog() {
        let dialogRef = this.dialog.open(EditAdminDialog, { width: '500px' });
        dialogRef.componentInstance.admin = this.admin;
        dialogRef.afterClosed().subscribe(result => {
            if (result) {
                if (result.value == "success") {
                    this.getAdminDetails();
                    this.coreService.notify('Successful', result.message, 1);
                } else if (result.value == "failure") {
                    this.coreService.notify('Unsuccessful', result.message, 0)
                } else { }
            }
        });
    }

    //gets the admin details
    getAdminDetails() {
        this.isProcessing = true;
        this.accountSettingService.getAdminDetails().subscribe(
            data => {
                if (data.code == "200") {
                    if (data.data) {
                        this.admin = data.data;
                        console.log("Admin details", data);
                        this.isProcessing = false;
                    }
                }
            },
            error => {
                this.isProcessing = false;
                this.coreService.notify(
                    "Unsuccessful",
                    "Error while getting the data",
                    0
                )
            },
            () => { }
        );
    }

}

@Component({
    selector: 'update-password',
    template: `       
    <div class="card-content">
    <h4>Update password</h4>
    <form #updatePasswordForm="ngForm" class="md-form-auth form-validation">
        <fieldset>
            <div class="form-group">
                <md-input-container class="full-width md-icon-left">
                    <md-icon class="material-icons">lock_outline</md-icon>
                    <input required mdInput type="password" name="oldPassword" placeholder="Current password" [(ngModel)]="password.oldPassword" #oldPassword="ngModel">
                </md-input-container>
                <div *ngIf="oldPassword.errors && (oldPassword.dirty || oldPassword.touched)">
                    <div style="color:red" [hidden]="!oldPassword.errors.required ">
                        Current password is required !
                    </div>
                </div>
            </div>
            <div class="form-group">
                <md-input-container class="full-width md-icon-left">
                    <md-icon class="material-icons">lock_outline</md-icon>
                    <input required mdInput type="password" name="newPassword" placeholder="New password" (keyup)="matchPassword()" [(ngModel)]="password.newPassword" #newPassword="ngModel">
                </md-input-container>
                <div *ngIf="newPassword.errors && (newPassword.dirty || newPassword.touched)">
                    <div style="color:red" [hidden]="!newPassword.errors.required ">
                        Password is required !
                    </div>
                </div>
            </div>
            <div class="form-group">
                <md-input-container class="full-width md-icon-left">
                    <md-icon class="material-icons">lock_outline</md-icon>
                    <input required mdInput type="password" name="confirmPassword" placeholder="Confirm password" (keyup)="matchPassword()" [(ngModel)]="password.confirmPassword" #confirmPassword="ngModel" >
                </md-input-container>
                <div *ngIf="confirmPassword.errors && (confirmPassword.dirty || confirmPassword.touched)">
                    <div style="color:red" [hidden]="!confirmPassword.errors.required ">
                        Confirm password is required !
                    </div>
                </div>
            </div>

            <div *ngIf="passwordNotMatch">
                <div style="color:red">
                    Password and confirm password do not match !
                </div>
            </div>
            
            </fieldset>
    </form>
    <md-progress-spinner *ngIf="isProcessing" mode="indeterminate" color=""></md-progress-spinner>            
    <div *ngIf="!isProcessing">
    <button md-raised-button type="submit" color="default" class="float-right" (click)="dialogRef.close(false,false);false">Cancel</button>
    <button md-raised-button [disabled]="updatePasswordForm.invalid || passwordNotMatch" type="submit" color="primary" class="float-right" (click)="updatePassword()">Submit</button>
    </div>
</div>
<div *ngIf="false" class="card bg-color-white">
    <div class="card-content">
        <h6>Your Password has been changed successfully</h6>
        <div class="additional-info">
            <span>Return to <a [routerLink]="['/user/login']">Login</a></span>
        </div>
    </div>
</div>
        `,
    styles: [`
         
              h4{
                 margin-top:0px;
              }
              button{
                margin-left:10px;
              }        
        `]
})

export class UpdatePasswordDialog {

    password = {
        oldPassword: '',
        newPassword: '',
        confirmPassword: '',
    }
    passwordNotMatch = false;
    isProcessing = false;
    constructor(public dialogRef: MdDialogRef<UpdatePasswordDialog>, private coreService: CoreService, private accountSettingService: AccountSettingService, ) { }

    matchPassword() {
        if (this.password.newPassword != this.password.confirmPassword) {
            this.passwordNotMatch = true;
        } else this.passwordNotMatch = false;
    }

    //update the password
    updatePassword() {
        this.isProcessing = true;
        this.accountSettingService.updatePassword(this.password).subscribe(
            data => {
                if (data.code == 200) {
                    this.isProcessing = false;
                    this.dialogRef.close({ value: "success", message: data.message });
                }
            }, error => {
                this.isProcessing = false;
                this.dialogRef.close({ value: "failure", message: error.message });
            })
    }
}


@Component({
    selector: 'edit-admin',
    template: `       
    <div class="card-content">
    <h4>Edit admin</h4>
    <form #editAdminForm="ngForm" class="md-form-auth form-validation">
        <fieldset>
            <div class="form-group">
                <md-input-container class="full-width">
                    <input required mdInput name="firstName" placeholder="First name" [(ngModel)]="adminDetails.firstName" #firstName="ngModel">
                </md-input-container>
                <div *ngIf="firstName.errors && (firstName.dirty || firstName.touched)">
                    <div style="color:red" [hidden]="!firstName.errors.required ">
                        First name is required !
                    </div>
                </div>
            </div>
            <div class="form-group">
                <md-input-container class="full-width">
                    <input mdInput name="lastName" placeholder="Last name" [(ngModel)]="adminDetails.lastName" #lastName="ngModel" required>
                </md-input-container>
                <div *ngIf="lastName.errors && (lastName.dirty || lastName.touched)">
                    <div style="color:red" [hidden]="!lastName.errors.required ">
                        Last name is required !
                    </div>
                </div>
            </div>
            <div class="form-group">
                <md-input-container class="full-width">
                    <input required mdInput name="email" placeholder="Email" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" [(ngModel)]="adminDetails.email" #Email="ngModel">
                </md-input-container>
                <div *ngIf="Email.errors && (Email.dirty || Email.touched)">
                    <div style="color:red" [hidden]="!Email.errors.required">
                        Email is required !
                    </div>
                    <div style="color:red" [hidden]="!Email.errors.pattern">
                    Email is not valid !
                </div>
                </div>
            </div>

            <md-progress-spinner *ngIf="isProcessing" mode="indeterminate" color=""></md-progress-spinner>            
            <div *ngIf="!isProcessing">
            <button md-raised-button type="submit" color="default" class="float-right" (click)="dialogRef.close(false,false)">Cancel</button>
            <button md-raised-button [disabled]="editAdminForm.invalid" type="submit" color="primary" class="float-right" (click)="updateAdmin()">Save</button>
            </div>
            </fieldset>
    </form>
</div>
        `,
    styles: [`
         
              h4{
                 margin-top:0px;
              }
              button{
                margin-left:10px;
              }      
        `]
})

export class EditAdminDialog {

    admin = {
        firstName: '',
        lastName: '',
        email: ''
    }
    adminDetails = {
        firstName: '',
        lastName: '',
        email: ''
    }

    isProcessing = false;
    constructor(public dialogRef: MdDialogRef<UpdatePasswordDialog>, private coreService: CoreService, private accountSettingService: AccountSettingService, ) { }
    ngOnInit() {
        this.adminDetails = JSON.parse(JSON.stringify(this.admin));
    }

    //edit admin details
    updateAdmin() {
        this.isProcessing = true;
        this.accountSettingService.updateAdmin(this.adminDetails).subscribe(
            data => {
                if (data.code == 200) {
                    this.isProcessing = false;
                    this.dialogRef.close({ value: "success", message: data.message });
                }
            }, error => {
                this.isProcessing = false;
                this.dialogRef.close({ value: "failure", message: error.message });
            })
    }
}