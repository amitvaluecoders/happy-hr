import { Component, OnInit } from '@angular/core';
import { UserService } from '../../../shared/services/user.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { MdSnackBar, MdDialog, MdDialogRef } from '@angular/material';
import { UserAccessService } from '../user-access.service';
import { CoreService } from '../../../shared/services/core.service';

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.scss']
})
export class AddUserComponent implements OnInit {
public editable:Boolean=false;
public title="Add new user";
public buttonText="Add";
 
  user={
    firstName:"",
    lastName:"",
    email:"",
    phoneNumber:null,
    extensionNumber:null,
    profileImageName:'',
    roles:[],
    
}
  isProcessing=false;
  roles=[];
  uploadProcessing=false;
  isButtonProcessing=false;
  id='';
  userRole='';
  status=false;
  constructor(public snackBar: MdSnackBar,private userservice: UserService,private router:Router,private activatedRoute:ActivatedRoute,private userAccessService:UserAccessService,private coreService:CoreService) { }
  
  ngOnInit() {
    this.getRoles();
this.id=this.activatedRoute.snapshot.params['id'];
if(this.id){
  this.buttonText="Save";
  this.title="Edit user";
  this.user["userID"]=this.id; 
}
  }

  // get roles list
  getRoles(){
    this.isProcessing=true;
    this.userAccessService.getRoles().subscribe(
      data=>{
        if (data.code == "200") {
          this.roles=data.data;
          if(this.id) this.getUserForUpdate();
          else this.isProcessing=false;     
        }else{
          this.isProcessing=false;
          this.coreService.notify("Warning",data.message,2)
        }
      },
      error=>{this.isProcessing=false;this.coreService.notify("Unsuccessful",error.message,0)},
      ()=>{}
    )
  }

  // add/ edit user 
  onSubmit(isvalid){
    this.isButtonProcessing=true;
    if(isvalid){
      if(this.id) this.user["status"]=this.status;
      this.user.roles=[];
      this.user.roles[0]=this.userRole;
      
    this.userAccessService.addEditUser(this.user).subscribe(
      data => {
          if(data.code==200){
            this.isProcessing=false;
          this.coreService.notify("Successful",data.message,1) 
            this.router.navigate(['/app/super-admin/user-access/users-list']);
          }
      },
      error=>{
        this.isButtonProcessing=false;
        this.coreService.notify("Unsuccessful",error.message,0)
      }
    );
    } 
  }

  // get user details
  getUserForUpdate(){
    this.userAccessService.getUserDetails(this.id).subscribe(
   data=>{
     if(data.code==200){
     if(data.data.status=="true") this.status=Boolean(data.data.status);
     else if(data.data.status=="false") this.status=!Boolean(data.data.status);
          
     this.user=data.data;
     this.userRole=this.user.roles[0].roleID;
     this.isProcessing=false;
     }
   },
   error=>{
     this.isProcessing=false;
     this.coreService.notify('Unsuccessful',error.message,0);
   }
 )
   }

  // upload user image
  handleFileSelect($event) {
    this.uploadProcessing=true;
    const files = $event.target.files || $event.srcElement.files;
    const file = files[0];
    const formData = new FormData();
    formData.append('profileImage', file);
    this.userAccessService.uploadFile(formData).subscribe(
      data => {
        this.user.profileImageName=data.data.profileImage;
        this.uploadProcessing=false;
      },error=>{		
        this.uploadProcessing=false;        
        this.coreService.notify("Unsuccessful",error.message,0);
    });
  }

  // navigates to users list page
  onCancel(){
    this.router.navigate(['/app/super-admin/user-access/users-list']);
  }

}
