import { Injectable } from '@angular/core';
import { RestfulLaravelService } from '../../shared/services/restful-laravel.service';

@Injectable()
export class UserAccessService {

  constructor(private restfulLaravelService:RestfulLaravelService) { }
  
  addEditRole(reqBody):any{
    return this.restfulLaravelService.post('admin/create-update-role',reqBody);
  }

  getRoles():any{
    return this.restfulLaravelService.get('admin/role-list');
  }

  deleteRole(reqBody):any{
    return this.restfulLaravelService.delete(`admin/role-delete/${reqBody}`);     
  }

  addEditUser(reqBody):any{
    return this.restfulLaravelService.post('admin/create-update-user',reqBody);
  }

  uploadFile(reqBody){
    return this.restfulLaravelService.post('admin/file-upload',reqBody);  
  }

  getUserDetails(reqBody){
    return this.restfulLaravelService.get(`admin/get-sub-admin-detail/${reqBody}`);     
  }

  getAllUsers(reqBody){
    return this.restfulLaravelService.get(`admin/sub-admin-list/${encodeURIComponent(reqBody)}`); 
  }

  deleteUser(reqBody){
    return this.restfulLaravelService.delete(`admin/sub-admin-delete/${reqBody}`);  
  }

  userStatusChange(reqBody){
    return this.restfulLaravelService.put('admin/sub-admin-status-update',reqBody);
    
  }

  getAllModules(reqBody){
    return this.restfulLaravelService.get(`admin/get-role-permission/${reqBody}`);     
  }

  updatePermission(reqBody){
    return this.restfulLaravelService.put('admin/set-role-permission',reqBody);    
  }
}
