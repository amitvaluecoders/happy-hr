import { Component, OnInit } from '@angular/core';
import { UserAccessService } from '../user-access.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { CoreService } from '../../../shared/services/core.service';

@Component({
  selector: "app-set-permission",
  templateUrl: "./set-permission.component.html",
  styleUrls: ["./set-permission.component.scss"]
})
export class SetPermissionComponent {
  selectedRole;

  // roles = [];
  modules = [];
  id = '';
  isProcessing = false;
  isUpdateProcessing = false;

  constructor(private userAccessService: UserAccessService, private coreService: CoreService, private router: Router, private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.id = this.activatedRoute.snapshot.params['id'];
    if (this.id) this.getAllModulesPermission();
    // this.getRoles();
  }

  // // get all roles
  // getRoles() {
  //   this.userAccessService.getRoles().subscribe(
  //     data => {
  //       if (data.code == "200") {
  //         this.roles = data.data;
  //       } else {
  //       }
  //     },
  //     error => {
  //     },
  //     () => { }
  //   );
  // }

  // get modules permission
  getAllModulesPermission() {
    this.isProcessing = true;
    this.userAccessService.getAllModules(this.id).subscribe(
      data => {
        if (data.code == "200") {
          this.modules = data.data;
          this.isProcessing = false;
        } else {
        }
      },
      error => {
        this.isProcessing = false;
      },
      () => { }
    );
  }

  // // update permission
  // updatePermission() {
  //   this.isUpdateProcessing = true;
  //   let reqBody = {
  //     roleID: this.id,
  //     modules: this.modules
  //   };
  //   this.userAccessService.updatePermission(reqBody).subscribe(
  //     data => {
  //       this.isUpdateProcessing = false;
  //       this.router.navigate(['/app/super-admin/user-access/view-roles']);
  //       this.coreService.notify('Successful', data.message, 1)
  //     }, error => {
  //       this.isUpdateProcessing = false;
  //       this.coreService.notify('Unsuccessful', error.message, 0)
  //     })
  // }

  // // navigates to roles list page 
  // onCancel() {
  //   this.router.navigate(['/app/super-admin/user-access/view-roles']);
  // }
}
