import { Component, OnInit } from '@angular/core';
import { UserService } from '../../../shared/services/user.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { MdSnackBar, MdDialog, MdDialogRef } from '@angular/material';
import { UserAccessService } from '../user-access.service';
import { CoreService } from '../../../shared/services/core.service';


@Component({
  selector: 'app-user-detail',
  templateUrl: './user-detail.component.html',
  styleUrls: ['./user-detail.component.scss']
})
export class UserDetailComponent implements OnInit {
public editable:Boolean=false;
 public user={
    firstName:'',
    lastName:'',
    phoneNumber:null,
    email:'',
    extensionNumber:null,
    profileImageName:'',
    roles:[],
    status:'' 
  };
  public isProcessing=true;
  public users=[];

  id;
  color;
  permission;
  status='';

  constructor(public snackBar: MdSnackBar,private userservice: UserService,
    private router:Router,private activatedRoute:ActivatedRoute,
    private userAccessService:UserAccessService,
  private coreService:CoreService) { }

  ngOnInit() {
 this.id=this.activatedRoute.snapshot.params['id'];
 if(this.id) this.getUserDetails();
this.permission=this.coreService.getNonRoutePermission();

  }

  // get user details
  getUserDetails(){
    this.isProcessing=true;
   this.userAccessService.getUserDetails(this.id).subscribe(
  data=>{
    if(data.code==200){
    this.user=data.data;
    if(data.data.status=="true")
    this.status="Active"; 
    else this.status="Inactive"; 
    (this.status=="Active")?this.color='primary':this.color='default';
    this.isProcessing=false;
    }
  },
  error=>{
    this.isProcessing=false;
    this.coreService.notify('Unsuccessful',error.message,0);
  }
)
  }

  // navigates to users list page
  onCancel(){
    this.router.navigate(['/app/super-admin/user-access/users-list']);
  }

  // changes the status of user
  onStatusChange(){ 
    (this.user.status=="Active")?this.user.status='Active':this.user.status='Inactive';
    let reqBody={
      userID:this.id,
      status:status
    }
    this.userAccessService.userStatusChange(reqBody).subscribe(
      data=>{
        if(data.code==200){
        this.user.status=data.data;
        (this.user.status=="Active")?this.color='primary':this.color='default';
        }
      },
      error=>{
        this.coreService.notify('Unsuccessful',error.message,0);
      }
    )
  
  }

}
