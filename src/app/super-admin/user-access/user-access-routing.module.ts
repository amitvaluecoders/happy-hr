import { Routes, RouterModule }  from '@angular/router';
import { UsersListingComponent } from './users-listing/users-listing.component';
import { ViewRolesComponent } from './view-roles/view-roles.component';
import { AddUserComponent } from './add-user/add-user.component';
import { UserDetailComponent } from './user-detail/user-detail.component';
import { SetPermissionComponent } from './set-permission/set-permission.component';
import { UserAccessGuardService } from './user-access-guard.service';
import { RoleGuardService } from './role-guard.service';
export const UserAccessPagesRoutes: Routes = [
    {
        path: '',
        children: [
            { path: 'users-list', component: UsersListingComponent,canActivate:[UserAccessGuardService] },
            { path: 'view-roles', component: ViewRolesComponent,canActivate:[RoleGuardService] },
            { path: 'permission/:id', component: SetPermissionComponent,canActivate:[RoleGuardService] },
            { path: 'add-user', component: AddUserComponent,canActivate:[UserAccessGuardService] },
            { path: 'detail/:id', component: UserDetailComponent,canActivate:[UserAccessGuardService] },
            { path: 'edit/:id', component: AddUserComponent,canActivate:[UserAccessGuardService] }
        ]
    }
];

export const UserAccessPagesRoutingModule = RouterModule.forChild(UserAccessPagesRoutes);
