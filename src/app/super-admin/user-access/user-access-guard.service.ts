import { Injectable } from '@angular/core';
import {CanActivate,Router,RouterStateSnapshot,ActivatedRouteSnapshot} from "@angular/router";
import {CoreService} from '../../shared/services/core.service';
import {Observable} from 'rxjs/Rx'; 
import 'rxjs/add/operator/map'

@Injectable()
export class UserAccessGuardService {

  public module = "user";
  
        constructor(private _core: CoreService, private router: Router){ }
  
        canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {  
            let url: string;
            if(route['_routeConfig']['path'])
            url= route['_routeConfig']['path']
            else url = state.url;
            if(this.checkHavePermission(url)){
              return true;
            }else{
              this.router.navigate(['/extra/unauthorized']);
              return false;
            }
        }
  
        private checkHavePermission(path) {
            switch (path) {
                case 'users-list':
                    return this._core.getModulePermission(this.module, 'view');
                case 'add-user':
                    return this._core.getModulePermission(this.module, 'add');
                case 'detail/:id':
                    return this._core.getModulePermission(this.module, 'view');
                case 'edit/:id':
                    return this._core.getModulePermission(this.module, 'edit');
                default:
                    return false;
            }
        }

}
