import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ViewRolesComponent } from './view-roles/view-roles.component';
import { UsersListingComponent } from './users-listing/users-listing.component';
import { UserAccessPagesRoutingModule } from './user-access-routing.module';
import { DataTableModule} from "angular2-datatable";
import { MaterialModule } from '@angular/material';
import { AddUserComponent } from './add-user/add-user.component';
import { UserDetailComponent } from './user-detail/user-detail.component';
import { FormsModule } from '@angular/forms';
import { RestfulwebService } from '../../shared/services/restfulweb.service';
import { UserService } from '../../shared/services/user.service';
import { SetPermissionComponent } from './set-permission/set-permission.component';
import { UserAccessService } from './user-access.service';
import { UserAccessGuardService } from '././user-access-guard.service';
import { RoleGuardService } from './role-guard.service';
import { SharedModule } from '../../shared/shared.module';
 
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    MaterialModule,
    UserAccessPagesRoutingModule,
    DataTableModule,
    SharedModule
  ],
  declarations: [ViewRolesComponent, UsersListingComponent, AddUserComponent, UserDetailComponent, SetPermissionComponent],
  providers:[UserService, RestfulwebService, UserAccessService, UserAccessGuardService, RoleGuardService]
})
export class UserAccessModule { }
