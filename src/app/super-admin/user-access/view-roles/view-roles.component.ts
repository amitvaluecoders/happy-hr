import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { MdSnackBar, MdDialog, MdDialogRef } from '@angular/material';
import { UserAccessService } from '../user-access.service';
import { CoreService } from '../../../shared/services/core.service';
import { ConfirmService } from '../../../shared/services/confirm.service';
import { Router, ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-view-roles',
  templateUrl: './view-roles.component.html',
  styleUrls: ['./view-roles.component.scss']
})
export class ViewRolesComponent implements OnInit {
  roles = [];
  isProcessing = false;
  permission;
  constructor(public dialog: MdDialog, private userAccessService: UserAccessService, private coreService: CoreService, public confirmService: ConfirmService, public viewContainerRef: ViewContainerRef) { }

  ngOnInit() {
    this.getRoles();
    this.permission = this.coreService.getNonRoutePermission();
  }

  // get all roles
  getRoles() {
    this.isProcessing = true;
    this.userAccessService.getRoles().subscribe(
      data => {
        if (data.code == "200") {
          this.isProcessing = false;
          this.roles = data.data;
        } else {
          this.isProcessing = false;
          this.coreService.notify("Warning", data.message, 2)
        }
      },
      error => { this.isProcessing = false; this.coreService.notify("Unsuccessful", error.message, 0) },
      () => { }
    )
  }

  role = {
    roleID: '',
    title: ''
  };

  // set role variable and opens dialog box for add/edit role
  // onEdit(role) {
  //   this.role = role
  //   this.openDialogAddRole();
  // }

  // opens dialog box to add/edit role
  // openDialogAddRole() {
  //   let dialogRef = this.dialog.open(addNewRoleDialog, {
  //     width: '400px'
  //   });
  //   if (this.role) dialogRef.componentInstance.role = this.role;
  //   dialogRef.afterClosed().subscribe(result => {
  //     if (result) {
  //       this.isProcessing = true;
  //       this.userAccessService.addEditRole(result).subscribe(
  //         data => {
  //           if (data.code == "200") {
  //             this.getRoles();
  //             this.coreService.notify("Successful", data.message, 1)
  //           } else {
  //             this.getRoles();
  //             this.coreService.notify("Warning", data.message, 2)
  //           }
  //         },
  //         error => this.coreService.notify("Unsuccessful", error.message, 0),
  //         () => { }
  //       )
  //     }
  //   });
  // }

  // delete role
  // onDelete(id) {
  //   this.confirmService.confirm(this.confirmService.tilte, this.confirmService.message, this.viewContainerRef)
  //     .subscribe(res => {
  //       let result = res;
  //       if (result) {
  //         this.isProcessing = true;
  //         this.userAccessService.deleteRole(id).subscribe(
  //           data => {
  //             if (data.code == "200") {
  //               this.getRoles();
  //               this.coreService.notify("Successful", data.message, 1)
  //             } else {
  //               this.isProcessing = false;
  //               this.coreService.notify("Warning", data.message, 2)
  //             }
  //           },
  //           error => {
  //             this.isProcessing = false;
  //             this.coreService.notify("Unsuccessful", error.message, 0)
  //           },
  //           () => { }
  //         )
  //       }
  //     })
  // }

}

// @Component({
//   selector: 'add-new-role-dialog',
//   template: `<h1 md-dialog-title> {{title}}</h1>
//         <div md-dialog-content style="padding-bottom: 20px;">
//           <form #roleDialogForm="ngForm">
//             <div class="form-group">
//               <md-input-container class="full-width">
//                   <input mdInput placeholder="Enter role name" name="roleName" [(ngModel)]="role.title">
//               </md-input-container>
              
//             </div>
//           </form>
//         </div>
//         <div md-dialog-actions>
//             <button md-raised-button color="primary" (click)="onSubmit(roleDialogForm.valid)">{{buttonText}}</button>
//             <button md-button (click)="onCancel();">Cancel</button>
//         </div>`,
// })
// export class addNewRoleDialog implements OnInit {

//   role = {
//     roleID: '',
//     title: ''
//   };
//   oldRole = '';
//   title = "Add new role";
//   buttonText = 'Add';
//   ngOnInit() {
//     if (this.role.roleID) {
//       this.title = 'Edit role';
//       this.buttonText = 'Save';
//       this.oldRole = JSON.parse(JSON.stringify(this.role.title));
//     }
//   }
//   constructor(public dialogRef: MdDialogRef<addNewRoleDialog>) { }

//   onSubmit(isValid) {
//     if (isValid) {
//       this.dialogRef.close(this.role);
//     }
//   }

//   // close dialog box 
//   onCancel() {
//     this.role.title = this.oldRole;
//     this.dialogRef.close();
//   }
// }
