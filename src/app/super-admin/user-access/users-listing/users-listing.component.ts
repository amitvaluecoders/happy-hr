import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { UserService } from '../../../shared/services/user.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { MdSnackBar} from '@angular/material';
import { UserAccessService } from '../user-access.service';
import { CoreService } from '../../../shared/services/core.service';
import { ConfirmService } from '../../../shared/services/confirm.service';

@Component({
  selector: 'app-users-listing',
  templateUrl: './users-listing.component.html',
  styleUrls: ['./users-listing.component.scss']
})
export class UsersListingComponent implements OnInit {

  constructor(public snackBar: MdSnackBar,private router:Router,private userAccessService:UserAccessService,private coreService:CoreService, public confirmService:ConfirmService, public viewContainerRef:ViewContainerRef) { }

  public users=[];
  isProcessing=false;
  searchText='';
  cancelRequest;
  permission;

  ngOnInit() {
    this.getUsers();
    this.permission=this.coreService.getNonRoutePermission();
  }

  // get users list
  getUsers(){
    this.isProcessing=true;
    let searchkey=encodeURIComponent(this.searchText);
    let reqBody={
      search:searchkey
    }
    this.cancelRequest=this.userAccessService.getAllUsers(JSON.stringify(reqBody)).subscribe(
      data=>{
        if(data.code=200){
          console.log("users data",data);
        this.users=data.data;
        this.isProcessing=false;
        }
      },
      error=>{
        this.isProcessing=false;
      this.coreService.notify('Unsuccessful',error.message,0);
      }
    )
  }

  // navigates to user details page 
  onDetail(id){
    this.router.navigate([`app/super-admin/user-access/detail/${id}`])
  }

  // navigates to edit user page
  onEdit(id){
    this.router.navigate([`app/super-admin/user-access/edit/${id}`])
  }

  // delete user
  onDelete(id) { 
    this.confirmService.confirm(this.confirmService.tilte,this.confirmService.message,this.viewContainerRef)
    .subscribe(res=>{
    if(res){
      this.isProcessing=true
    this.userAccessService.deleteUser(id).subscribe(
      data => {
        if (data.code==200) {
          console.log(data);
          this.getUsers();
        }
      },error=>{
        this.isProcessing=false;
        this.coreService.notify('Unsuccessful',error.message,0);
      }
    );
  }})
  }

  // filter users list
  onFilter(){
    this.cancelRequest.unsubscribe();    
    this.getUsers();
  }

}
