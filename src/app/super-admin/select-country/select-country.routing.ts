import { Routes, RouterModule }  from '@angular/router';
import { SelectCountryComponent } from './select-country/select-country.component';
export const SelectCountryPagesRoutes: Routes = [
    {
        path: '',
        children: [
            { path: '', component: SelectCountryComponent },
           
        ]
    }
];

export const SelectCountryPagesRoutingModule = RouterModule.forChild(SelectCountryPagesRoutes);
