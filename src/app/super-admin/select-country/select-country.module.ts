import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SelectCountryComponent } from './select-country/select-country.component';
import { SelectCountryPagesRoutingModule } from './select-country.routing';

@NgModule({
  imports: [
    CommonModule,
    SelectCountryPagesRoutingModule
  ],
  declarations: [SelectCountryComponent]
})
export class SelectCountryModule { }
