import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { SubscriptionPlanService } from '../subscription-plan.service';
import { CoreService } from '../../../shared/services/core.service';
import { ConfirmService } from '../../../shared/services/confirm.service'

@Component({
  selector: 'app-support-services',
  templateUrl: './support-services.component.html',
  styleUrls: ['./support-services.component.scss']
})
export class SupportServicesComponent implements OnInit {
  public addSupportServiceForm=false;
  public supportServices=[];
  public supportService={
    
  }
  serviceList={};
  showServiceField=false;
  buttonText="Add";
  oldSupportService={};
  constructor(public subscriptionPlanService:SubscriptionPlanService,public coreService:CoreService,private confirmService:ConfirmService, private viewContainerRef:ViewContainerRef) { }

  ngOnInit() {
    this.getAllServices();
  }

  getAllServices(){
    this.subscriptionPlanService.getAllSupportServices().subscribe(
      d=>{
        if(d.code="200"){
            this.supportServices=d.data;
            // this.oldSupportServices=JSON.parse(JSON.stringify(this.supportServices));
        }
        else this.coreService.notify("Unsuccessful",d.message,0);
      },
      e=>this.coreService.notify("Unsuccessful",e.message,0))
  }


  onAddEditService(isvalid,form){
    if(isvalid){
      this.subscriptionPlanService.addEditSupportService(this.supportService).subscribe(
        d=>{
          if(d.code=="200") {
            this.coreService.notify("Successful",d.message,1);
           // this.supportServices.push({title:this.supportService.title,description:this.supportService.description});
            console.log("ss",this.supportServices);
            form.reset();
            this.addSupportServiceForm=false;
            this.getAllServices();         
          }
          else this.coreService.notify("Unsuccessful",d.message,0)
        },
        e=>this.coreService.notify("Unsuccessful",e.message,0)
        )
    }
  }

  onDeleteService(id,index){
    this.confirmService.confirm(this.confirmService.tilte,this.confirmService.message,this.viewContainerRef)
    .subscribe(res=>{   
    if(res){
        this.subscriptionPlanService.deleteService(JSON.stringify(id)).subscribe(
        d=>{
          if(d.code=="200") {
            this.coreService.notify("Successful",d.message,1);
            this.getAllServices();         
          }
          else this.coreService.notify("Unsuccessful",d.message,0)
        },
        e=>this.coreService.notify("Unsuccessful",e.message,0)
        )
    }})
  }


  addServiceField() {
    this.supportService ={} ;
      this.showServiceField = true;
      // this.showLocationEditField = false;
    //   this.serviceList={ 
    // serviceID:'',
    // title:'',
    // description:'' };
    this.buttonText="Add";
    // this.list.push({id:'',title:''});
  }

  onCancel(form,service){
   service.title=this.oldSupportService["title"];
   service.description=this.oldSupportService["description"];   
  this.showServiceField = false;
  //form.reset();   
}

editService(service){ 
  this.buttonText="Save";
  this.supportService=service;
  this.oldSupportService=JSON.parse(JSON.stringify(this.supportService));
  this.showServiceField = true;    
}

}
