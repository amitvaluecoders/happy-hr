import { Component, OnInit } from '@angular/core';
import { Router,ActivatedRoute } from '@angular/router'
import { SubscriptionPlanService } from '../subscription-plan.service';
import { CoreService } from '../../../shared/services/core.service';

@Component({
  selector: 'app-create-plan',
  templateUrl: './create-plan.component.html',
  styleUrls: ['./create-plan.component.scss']
})
export class CreatePlanComponent implements OnInit {

   public isAddButtonClicked=false;
   public addUserDiscoutToggle=true;
   public servicesToggle=true;
   public addSupportServicesToggle=true;
   public dd=true;

   // dropdown set duration 
   public dropdropdownListSetDuration=[];
   public selectedItemsSetDuration = []

   // dropdown add discout user 
   public dropdropdownListDiscoutUser=[];
   public selectedItemsDiscoutUser = [];
   
   // dropdown support services   
   public dropdownListServices=[];
   public selectedItemsServices=[];

   // dropdown add discout support
  //  public dropdropdownListDiscountSupport=[];
   public selectedItemsDiscountSupport = [];
   
   public dropdownSettingsServices = {};
   public dropdownSettingsSetDuration = {};
   public dropdownSettingsDiscoutUser ={};
   
   public subscriptionPlan={
     title:'',
     id:'',
     duration:"",
     employeeFee:null,
     userCount:null,
     userDiscoutType:'',
     userDiscoutAmount:null,
     supportServices:[],
     supportDiscoutType:'',
     discoutSupportAmount:null,
     status:"0"
   }

  constructor(public coreService:CoreService,
              public subscriptionPlanService:SubscriptionPlanService,
              public router:Router,
              public activatedRoute:ActivatedRoute) { }

  ngOnInit() {

    this.getSupportServices();
    let id=this.activatedRoute.snapshot.params['id'];
    if(id)this.getSubscriptionPlanById(id);

    this.dropdropdownListSetDuration = [
      { "id": 1, "itemName": "12-months" },
      { "id": 2, "itemName": "24-months" },
    ];

    this.dropdropdownListDiscoutUser=[
      { "id": 1, "itemName": "%-Baised" },
      { "id": 2, "itemName": "Fixed-price" },
    ]
   
   this.dropdownSettingsSetDuration={
     singleSelection: true,
     text: "Select Duration"
   }
    
   this.dropdownSettingsDiscoutUser={
     singleSelection: true,
     text: "Select discout type"
   }
   
    this.dropdownSettingsServices = {
      singleSelection: false,
      text: "Select Duration",
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      enableSearchFilter: true,
      classes: "myclass custom-class",
    };            
  }

// get subscription plan by id for edit

  getSubscriptionPlanById(id){
    this.subscriptionPlanService.getSubscriptionPlan(JSON.stringify({"id":id})).subscribe(
      d=>{
        if(d.response.code="200")  this.setFormValues(d.response.data);
        else this.coreService.notify("Unsuccessful","error while getting subscription plan",0); 
      },
      e=>this.coreService.notify("Unsuccessful","error while getting subscription plan",0))
  }

  setFormValues(data){
    this.subscriptionPlan=data;
          this.selectedItemsServices=data.supportServices;
          for(let c of this.dropdropdownListSetDuration){          
            if(c.itemName==data.duration) this.selectedItemsSetDuration.push(c);
          }
          for(let c of this.dropdropdownListDiscoutUser){          
            if(c.itemName==data.userDiscoutType) this.selectedItemsDiscoutUser.push(c);
            if(c.itemName==data.supportDiscoutType) this.selectedItemsDiscountSupport.push(c);
          }
  }

// get all support services

  getSupportServices(){
    this.subscriptionPlanService.getAllSupportServices().subscribe(
      d=>{
        if(d.response.code="200"){
          this.dropdownListServices=d.response.data.filter(function(item){
            item["itemName"]=item.title;
            return item;
          });
          console.log(this.dropdownListServices)
        }
        else this.coreService.notify("Unsuccessful","error while getting support services",0); 
      },
      e=>this.coreService.notify("Unsuccessful","error while getting support services",0))
  }


onItemSelectServices($event){
  this.subscriptionPlan.supportServices=this.selectedItemsServices;
  console.log("ss",this.selectedItemsServices)
}
// dropdown 'onselect' && 'onDeSelect'  methods

//    on select duration
    onSetDurationSelect(obj){
        this.subscriptionPlan.duration=obj.itemName;
    }

//   on de-select duration
    onSetDurationDeSelect(obj){   
      this.subscriptionPlan.duration='';
    }
 
 // on select user discount type
    onDiscoutUserSelect(obj){
      this.subscriptionPlan.userDiscoutType=obj.itemName;
    }
 
 // on de-select user discount type
    onDiscoutUserDeSelect(obj){
      this.subscriptionPlan.userDiscoutType='';
    }

 // on select support discount type
    onItemSelectServicesDiscountType(obj){
      this.subscriptionPlan.supportDiscoutType=obj.itemName;
    }
 
 // on de-select support discount type
    onItemDeSelectServicesDiscountType(obj){
      this.subscriptionPlan.supportDiscoutType='';
    }
    
     onAddSubscriptionPlan(isvalid){
      this.isAddButtonClicked=true;
      if(isvalid){

        this.subscriptionPlanService.addEditSubscriptionPlan(this.subscriptionPlan).subscribe(
          d=>{
            if(d.response.code="200"){
                this.coreService.notify("Successful","New Subscription plan added",1)
                this.router.navigate(['app/super-admin/subscription-plans/']);
                
            } 
            else this.coreService.notify("Unsuccessful","error wgile adding subscription plan",0)
          },
          e=>this.coreService.notify("Unsuccessful","error while adding subscription plan",0))
        
      }
    }

// On Toggle changes

    addUserDiscoutToggleChange(tag){
      if(!tag){
        this.selectedItemsDiscoutUser=[];
        this.subscriptionPlan.userDiscoutAmount=null;
      }
    }

    servicesToggleChange(tag){
      if(!tag){
        this.selectedItemsServices=[];
        this.subscriptionPlan.supportServices=[];
      }
    }
    addSupportServicesToggleChange(tag){
      if(!tag){
        this.selectedItemsDiscountSupport=[];
        this.subscriptionPlan.discoutSupportAmount=null;
      }
    }

}
