import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditSubscriptionPlanComponent } from './edit-subscription-plan.component';

describe('EditSubscriptionPlanComponent', () => {
  let component: EditSubscriptionPlanComponent;
  let fixture: ComponentFixture<EditSubscriptionPlanComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditSubscriptionPlanComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditSubscriptionPlanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
