import { Component, OnInit } from '@angular/core';
import { SubscriptionPlanService } from '../subscription-plan.service';
import { CoreService } from '../../../shared/services/core.service';

@Component({
  selector: 'app-subscription-detail',
  templateUrl: './subscription-detail.component.html',
  styleUrls: ['./subscription-detail.component.scss']
})
export class SubscriptionDetailComponent implements OnInit {

   public subscriptionPlan={
     title:'',
     id:'',
     duration:"",
     employeeFee:null,
     userCount:null,
     userDiscoutType:'',
     userDiscoutAmount:null,
     supportServices:[],
     supportDiscoutType:'',
     discoutSupportAmount:null,
     status:"0",
     originalPrice:null,
     discountedPrice:null,
     youSave:null
   }


  constructor(public subscriptionPlanService:SubscriptionPlanService,public coreService:CoreService) { }

  ngOnInit() {
    let id
      this.getSubscriptionPlanById(id)
  }

    getSubscriptionPlanById(id){
    this.subscriptionPlanService.getSubscriptionPlan(JSON.stringify({"id":id})).subscribe(
      d=>{
        if(d.response.code="200") this.subscriptionPlan=d.response.data;  
        else this.coreService.notify("Unsuccessful","error while getting subscription plan",0); 
      },
      e=>this.coreService.notify("Unsuccessful","error while getting subscription plan",0))
  }

}
