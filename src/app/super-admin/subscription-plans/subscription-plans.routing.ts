import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from '@angular/material';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule }  from '@angular/router';
import { SubscriptionListingComponent } from './subscription-listing/subscription-listing.component';
import { SubscriptionDetailComponent } from './subscription-detail/subscription-detail.component';
import { EditSubscriptionPlanComponent } from './edit-subscription-plan/edit-subscription-plan.component';
import { CreatePlanComponent } from './create-plan/create-plan.component';
import { SupportServicesComponent } from './support-services/support-services.component';

export const SubscriptionPlansPagesRoutes: Routes = [
    {
        path: '',
        children: [
            { path: '', component: SubscriptionListingComponent },
            { path: 'detail', component: SubscriptionDetailComponent },
            { path: 'edit/:id', component: CreatePlanComponent },
            { path: 'add', component: CreatePlanComponent },
            { path: 'support-services', component: SupportServicesComponent },
         
        ]
    }
];

export const SubscriptionPlansPagesRoutingModule = RouterModule.forChild(SubscriptionPlansPagesRoutes);
