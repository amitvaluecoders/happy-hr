import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { MaterialModule } from '@angular/material';
import { DataTableModule} from "angular2-datatable";
import { SubscriptionListingComponent } from './subscription-listing/subscription-listing.component';
import { SubscriptionDetailComponent } from './subscription-detail/subscription-detail.component';
import { EditSubscriptionPlanComponent } from './edit-subscription-plan/edit-subscription-plan.component';
import { CreatePlanComponent } from './create-plan/create-plan.component';
import { SupportServicesComponent } from './support-services/support-services.component';
import { SubscriptionPlansPagesRoutingModule } from './subscription-plans.routing';
import { AngularMultiSelectModule } from 'angular2-multiselect-checkbox-dropdown/angular2-multiselect-dropdown';
import { SubscriptionPlanService } from './subscription-plan.service';
import { SharedModule } from '../../shared/shared.module';
import { SubscriptionPlanGuardService } from './subscription-plan-guard.service';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    MaterialModule,
    DataTableModule,
    SubscriptionPlansPagesRoutingModule,
    AngularMultiSelectModule,
    SharedModule

  ],
  declarations: [SubscriptionListingComponent, 
                 SubscriptionDetailComponent, 
                 EditSubscriptionPlanComponent,
                 CreatePlanComponent, 
                 SupportServicesComponent],
  providers:[SubscriptionPlanService,SubscriptionPlanGuardService]
})
export class SubscriptionPlansModule { }
