import { Injectable } from '@angular/core';
import { RestfulLaravelService } from '../../shared/services/restful-laravel.service';


@Injectable()
export class SubscriptionPlanService {

  constructor(
    private RestfulLaravelService: RestfulLaravelService, 
  ){}

  addEditSupportService(reqBody){
    return this.RestfulLaravelService.post('admin/subscription-service-add-edit',reqBody);     
  }

  getAllSupportServices(){
    return this.RestfulLaravelService.get(`admin/subscription-service-list`);     
  }

  deleteService(reqBody){
    return this.RestfulLaravelService.delete(`admin/subscription-service-delete/${reqBody}`);     
  }

  addEditSubscriptionPlan(reqBody){
    return this.RestfulLaravelService.post('admin/subscription-plan-add-edit',reqBody);     
  }

  getSubscriptionPlan(reqBody){
    return this.RestfulLaravelService.get(`admin/subscription-plan-detail/${reqBody}`);     
  }

  getAllSubscriptionPlans(){
    return this.RestfulLaravelService.get('admin/subscription-plan-list');         
  }

  deleteSubscriptionPlan(reqBody){
    return this.RestfulLaravelService.delete(`admin/subscription-plan-delete/${reqBody}`);     
  }

  subscriptionPlanStatusUpdate(reqBody){
    return this.RestfulLaravelService.put('admin/subscription-plan-status-update',reqBody);       
  }
  
}