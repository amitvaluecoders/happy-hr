import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmailTemplatesListingComponent } from './email-templates-listing.component';

describe('EmailTemplatesListingComponent', () => {
  let component: EmailTemplatesListingComponent;
  let fixture: ComponentFixture<EmailTemplatesListingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmailTemplatesListingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmailTemplatesListingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
