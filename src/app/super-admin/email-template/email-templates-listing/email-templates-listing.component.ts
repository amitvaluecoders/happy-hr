import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { EmailTemplateService} from '../email-template.service';
import { ConfirmService} from '../../../shared/services/confirm.service'; 
import { CoreService } from '../../../shared/services/core.service';

@Component({
  selector: 'app-email-templates-listing',
  templateUrl: './email-templates-listing.component.html',
  styleUrls: ['./email-templates-listing.component.scss']
})
export class EmailTemplatesListingComponent implements OnInit {
 public toggleFilter:boolean;
  constructor(private emailTemplateService:EmailTemplateService,private activatedRoute:ActivatedRoute,
              private confirmService:ConfirmService, private router:Router, private viewContainerRef:ViewContainerRef,
              private coreService:CoreService) { 
                activatedRoute.params.subscribe(val => {
                  if (this.activatedRoute.snapshot.params['type'] != undefined) {
                    this.type = this.activatedRoute.snapshot.params['type'];
                  }
                  this.status = Object.keys(this.state);
                  this.getEmailTemplates();
                });
              }
  
  public statusString = [];
  public filterItems;
  public notificationString = [];

  public emailTemplates = [];

  searchText='';
  state={
    Active:false,
    Inactive:false
 }
  status=[];
  cancelRequest;
  isProcessing=false;
  type='';
  permission;
  ngOnInit() {
    this.toggleFilter=false;
    this.permission=this.coreService.getNonRoutePermission();
  }

  onClick(item) {
    this.router.navigate(['/app/super-admin/email-template/',this.type,'email-template-detail',item.emailTemplateID]);
  }

  statusChange(e) {
    if (e.checked == true) {
      this.statusString.push(e.source.name);
    } else {
      for (let i = 0; i < this.statusString.length; i++) {
        if (this.statusString[i] == e.source.name) this.statusString.splice(i, 1);
      }
    }
    //this.filterItems = { status: this.statusString, notification: this.notificationString };
    this.getEmailTemplates();
  }
  
  //get email template list
  getEmailTemplates() {
    this.isProcessing=true;
    let searchKey=encodeURIComponent(this.searchText);
    let getEmailTemplatesParameters = {
      search: searchKey,
      status: this.state
    };
    this.cancelRequest=this.emailTemplateService.getEmailTemplates({reqBody:getEmailTemplatesParameters,type:this.type}).subscribe(
      data => {
        if (data.code == "200") {
          console.log("templates data",data);
          this.emailTemplates=data.data;
          this.isProcessing=false;
        }
      },error=>{
        this.isProcessing=false;
        this.coreService.notify('Unsuccessful',error.message,0)
      }
    );
  }
 
 // delete email template
  onDelete(id, index) {   
    let result;
    this.confirmService.confirm(this.confirmService.tilte,this.confirmService.message,this.viewContainerRef)
    .subscribe(res=>{
    if(res){
      this.isProcessing=true;
     this.emailTemplateService.deleteEmailTemplate(id).subscribe(
      data => {
        if (data.code == "200") {
          console.log(data);
          this.getEmailTemplates();
        this.coreService.notify('Successful',data.message,1)        
        }
      },error=>{
        this.isProcessing=false;
        this.coreService.notify('Unsuccessful',error.message,0)                
      }
    );
    }
    })
  }

  //filter email template list
  onFilter(){
    this.cancelRequest.unsubscribe();
    this.getEmailTemplates();
    }
    
    // navigates to add email template page
    onAddButtonClick(){
      this.router.navigate(['/app/super-admin/email-template/',this.type,'add-email-template'])
    }

   // navigates to edit email template page
    onEditButtonClick(emailTemplateID){
      this.router.navigate(['/app/super-admin/email-template/',this.type,'edit-email-template',emailTemplateID])
    }

}
