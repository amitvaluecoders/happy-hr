import { Component, OnInit } from '@angular/core';
import { EmailTemplateService } from '../email-template.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { MdSnackBar, MdDialog, MdDialogRef } from '@angular/material';
import { CoreService } from '../../../shared/services/core.service';

@Component({
  selector: 'app-email-template-detail',
  templateUrl: './email-template-detail.component.html',
  styleUrls: ['./email-template-detail.component.scss']
})
export class EmailTemplateDetailComponent implements OnInit {

constructor(public snackBar: MdSnackBar,private emailTemplateService:EmailTemplateService,private router:Router,private activatedRoute:ActivatedRoute,private coreService:CoreService) { }
public template = {
  emailTemplateID:'',
  notification:"",
  title:"",
  subject:"",
  message:"",
  status:""
}

public id;
public color;
isProcessing=false;
type='';
permission;
ngOnInit() {
  if (this.activatedRoute.snapshot.params['id'] != undefined) {
    this.id = this.activatedRoute.snapshot.params['id'];
    this.getEmailTemplate();
  }
  if (this.activatedRoute.snapshot.params['type'] != undefined) {
    this.type = this.activatedRoute.snapshot.params['type'];
  }
 this.permission=this.coreService.getNonRoutePermission();

}

// get email template details
getEmailTemplate() {
  this.isProcessing=true;
  this.emailTemplateService.getEmailTemplate(this.id).subscribe(
    data => {
      if (data.code == "200") {
        this.template=data.data;
        this.isProcessing=false;
      }
    },
    error => { this.isProcessing=false;this.coreService.notify('Unsuccessful',error.data,0) },
    () => { }
  )
}

// navigates to email template list
onCancel() {
  this.router.navigate(['/app/super-admin/email-template']);
}
}
