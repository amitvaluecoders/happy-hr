import { Component, OnInit } from '@angular/core';
import { EmailTemplateService } from '../email-template.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { MdSnackBar, MdDialog, MdDialogRef } from '@angular/material';
import { CoreService } from '../../../shared/services/core.service';

@Component({
  selector: 'app-add-email-template',
  templateUrl: './add-email-template.component.html',
  styleUrls: ['./add-email-template.component.scss']
})
export class AddEmailTemplateComponent implements OnInit {
  public template = {
    emailTemplateID:'',
    notification:{value:'',title:''},
    title:"",
    subject:"",
    message:"",
    status:""
}

  notificationList=[];

  public isProcessing = false;
  isButtonProcessing=false;
  public id;
  public buttonText = "Add";
  color;
  title="Add new email template";
  type='';
  constructor(public snackBar: MdSnackBar, public coreService:CoreService,  private emailTemplateService: EmailTemplateService, private router: Router, private activatedRoute: ActivatedRoute, ) { }

  ngOnInit() {
    if (this.activatedRoute.snapshot.params['type'] != undefined) {
      this.type = this.activatedRoute.snapshot.params['type'];
      this.getEmailNotificationList();
    }
    
    if (this.activatedRoute.snapshot.params['id'] != undefined) {
      this.id = this.activatedRoute.snapshot.params['id'];
      this.buttonText = "Save";
      this.title="Edit template";
    }
  }
  
  // add/edit email template 
  addEditEmailTemplate(isvalid) {
    if (isvalid) { 
    let template = {
        emailTemplateID:this.template.emailTemplateID,
        notification:this.template.notification.value,
        title:this.template.title,
        subject:this.template.subject,
        message:this.template.message,
        status:this.template.status
    }
    if(this.type=='super-admin-email') template["group"]="superAdmin";
    else if(this.type=='company-admin-email') template["group"]="companyAdmin";
    else if(this.type=='company-manager-email') template["group"]="manager";
    else if(this.type=='company-employee-email') template["group"]="employee" 
    else template["group"]="thirdParty";

      this.isButtonProcessing=true;
      this.emailTemplateService.addEditEmailTemplate(template).subscribe(
        data => {
          if (data.code == "200") {
      this.isButtonProcessing=false;
      this.coreService.notify("Successful",data.message,1)
            this.router.navigate(['/app/super-admin/email-template',this.type]);
          }
        },error=>{
          this.isButtonProcessing=false;
          this.coreService.notify("Unsuccessful",error.message,0)
        }
      );
    }
  }
 
  // navigates to email template list page
  onCancel() {
    this.router.navigate(['/app/super-admin/email-template',this.type]);
  }

  // get email template details
  getEmailTemplateForUpdate() {
    this.isProcessing=true;
    this.emailTemplateService.getEmailTemplate(this.id).subscribe(
      data => {
        if (data.code == "200") {
          this.template=data.data;
          this.template.status=data.data.status;
          this.color=(this.template.status == 'Active') ? 'primary' : 'default';
          this.isProcessing=false;
        }
      },
      error => { this.isProcessing=false;this.coreService.notify('Unsuccessful',error.data,0) },
      () => { }
    )
  }
 
  // get notification list for email template
  getEmailNotificationList(){
    this.isProcessing=true;
    this.emailTemplateService.getEmailNotificationList().subscribe(
      data => {
        if (data.code == "200") {
          if(this.type){
            if(this.type=='super-admin-email') this.notificationList=data.data.superAdmin;
            else if(this.type=='company-admin-email') this.notificationList=data.data.companyAdmin;
            else if(this.type=='company-manager-email') this.notificationList=data.data.manager;
            else if(this.type=='company-employee-email') this.notificationList=data.data.employee; 
            else this.notificationList=data.data.thirdParty;
          }
          if(this.id) this.getEmailTemplateForUpdate();
            else this.isProcessing=false;
        }
      },
      error => { this.isProcessing=false;this.coreService.notify('Unsuccessful',error.data,0) },
      () => { }
    )
  }
  
  // change status of email template 
  statusChange(status) {
    let s = (status == 'Active') ? 'Inactive' : 'Active'
    let reqBody={emailTemplateID:this.id,status:s}
    this.emailTemplateService.updateStatus(reqBody).subscribe(
      data => {
        this.template.status = s;
        this.color = (this.template.status == 'Active') ? 'primary' : 'default';
        this.coreService.notify("Successful", data.message, 1)
      },
      error => {
        this.coreService.notify("Unsuccessful", error.message, 0)
      }
    )
  }

  }


  


