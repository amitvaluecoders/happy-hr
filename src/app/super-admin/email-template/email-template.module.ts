import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from '@angular/material';
import { FormsModule } from '@angular/forms';
import { DataTableModule} from "angular2-datatable";
import { EmailTemplatesListingComponent } from './email-templates-listing/email-templates-listing.component';
import { EmailTemplatesRoutingModule} from './email-templates-routing.module';
import { AddEmailTemplateComponent } from './add-email-template/add-email-template.component';
import { EmailTemplateDetailComponent } from './email-template-detail/email-template-detail.component';
import { EmailTemplateService} from './email-template.service';
import { EmailManagementGuardService } from './email-management-guard.service';
import { SharedModule } from '../../shared/shared.module';

@NgModule({
  imports: [
    SharedModule,
    CommonModule,
    MaterialModule,
    FormsModule,
    DataTableModule,
    EmailTemplatesRoutingModule,
  ],
  declarations: [EmailTemplatesListingComponent, AddEmailTemplateComponent, EmailTemplateDetailComponent],
  providers:[EmailTemplateService,EmailManagementGuardService]

})
export class EmailTemplateModule { }
