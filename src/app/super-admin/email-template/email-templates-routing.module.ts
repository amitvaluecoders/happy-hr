import { Routes, RouterModule }  from '@angular/router';
import { EmailTemplatesListingComponent } from './email-templates-listing/email-templates-listing.component';
import { AddEmailTemplateComponent } from './add-email-template/add-email-template.component';
import { EmailTemplateDetailComponent } from './email-template-detail/email-template-detail.component';
import { EmailManagementGuardService } from './email-management-guard.service';

export const EmailTemplatesRoutes: Routes = [
    {
        path: '',
        children: [    
          { path: ':type', component: EmailTemplatesListingComponent,canActivate:[EmailManagementGuardService] },
          { path: ':type/add-email-template', component: AddEmailTemplateComponent,canActivate:[EmailManagementGuardService]  },
          { path: ':type/email-template-detail/:id', component: EmailTemplateDetailComponent,canActivate:[EmailManagementGuardService]  },
          { path: ':type/edit-email-template/:id', component: AddEmailTemplateComponent,canActivate:[EmailManagementGuardService] }        ]
    }
];

export const EmailTemplatesRoutingModule = RouterModule.forChild(EmailTemplatesRoutes);
