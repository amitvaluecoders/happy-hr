import { Injectable } from '@angular/core';
import { RestfulwebService } from '../../shared/services/restfulweb.service'
import { RestfulLaravelService } from '../../shared/services/restful-laravel.service'; 

@Injectable()
export class EmailTemplateService {

  
 constructor(
    private restfulWebService: RestfulwebService, 
    private restfulLaravelService: RestfulLaravelService, 
    
  ){}


 getEmailTemplates(template){
   let type;
   if(template.type=='super-admin-email') type="superAdmin";
   else if(template.type=='company-admin-email') type="companyAdmin";
   else if(template.type=='company-manager-email') type="manager";
   else if(template.type=='employee-email') type="companyEmployee"; 
   else type="thirdParty";
  return this.restfulLaravelService.get(`admin/email-template-list/${type}/${encodeURIComponent(JSON.stringify(template.reqBody))}`);
 }

  addEditEmailTemplate(template){
    return this.restfulLaravelService.post('admin/email-template-add-edit',template);
  }

  getEmailTemplate(template){
  return this.restfulLaravelService.get(`admin/email-template-detail/${template}`);
  }
  
  deleteEmailTemplate(template){
    return this.restfulLaravelService.delete(`admin/email-template-delete/${template}`);
  }

  
  getEmailNotificationList(){
  return this.restfulLaravelService.get('admin/email-template-list-notification');
  }



  updateStatus(template){
    return this.restfulLaravelService.put('admin/email-template-update-status',template);
  }

  


}
