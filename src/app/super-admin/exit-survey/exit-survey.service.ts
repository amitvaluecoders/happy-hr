import { Injectable } from '@angular/core';
import { RestfulwebService } from '../../shared/services/restfulweb.service';
import { RestfulLaravelService } from '../../shared/services/restful-laravel.service';


@Injectable()
export class ExitSurveyService {

constructor(public restfulWebService:RestfulwebService,public restfulLaravelService:RestfulLaravelService) { }
  
addCategory(category):any{
    //return this.restfulLaravelService.post('admin/add-edit-exit-survey',category);
  }

 getCategories(requestParameters){
    //return this.restfulWebService.get(`getCategories/${requestParameters}`);
 }

  getAllExitSurveyQuestions(){
    return this.restfulLaravelService.get('admin/list-exist-survey');
 }
  
  addExitSurveyQuestion(category){
   return this.restfulLaravelService.post('admin/add-edit-exit-survey',category);
  }

  deleteQuestion(question){
     return this.restfulLaravelService.delete(`admin/delete-exit-survey/${question}`);
  }

}
