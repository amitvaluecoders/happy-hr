import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { MaterialModule } from '@angular/material';
import { DataTableModule} from "angular2-datatable";
import { ExitSurveyComponent } from './exit-survey/exit-survey.component';
import { exitSurveyPagesRoutingModule } from './exit-survey.routing';
import { ExitSurveyService } from './exit-survey.service';
import {InlineEditorModule} from 'ng2-inline-editor';
import { ExitSurveyGuardService } from './exit-survey-guard.service';
@NgModule({
  imports: [
    CommonModule,
    MaterialModule,
    DataTableModule,
    FormsModule,
    exitSurveyPagesRoutingModule,
    InlineEditorModule
  ],
  declarations: [ExitSurveyComponent],
  providers:[ExitSurveyService,ExitSurveyGuardService]
})
export class ExitSurveyModule { }
