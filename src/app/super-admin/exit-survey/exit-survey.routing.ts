import { Routes, RouterModule }  from '@angular/router';
import { ExitSurveyComponent } from './exit-survey/exit-survey.component';
import { ExitSurveyGuardService } from './exit-survey-guard.service';

export const exitSurveyPagesRoutes: Routes = [
    {
        path: '',
        children: [
            { path: '', component: ExitSurveyComponent, canActivate:[ExitSurveyGuardService] }
       ]
    }
];

export const exitSurveyPagesRoutingModule = RouterModule.forChild(exitSurveyPagesRoutes);
