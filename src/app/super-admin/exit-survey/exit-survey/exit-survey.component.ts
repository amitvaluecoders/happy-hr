import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { ExitSurveyService } from '../exit-survey.service';
import { CoreService } from '../../../shared/services/core.service';
import { ConfirmService } from '../../../shared/services/confirm.service';
import { InlineEditorComponent } from 'ng2-inline-editor';

@Component({
  selector: 'app-exit-survey',
  templateUrl: './exit-survey.component.html',
  styleUrls: ['./exit-survey.component.scss']
})
export class ExitSurveyComponent implements OnInit {

  public hideAddPanel=false;
  public category= {exitSurveyID:"",title:""};;
  public categories=[];
  public questions:any;
  public exitSurveyQuestion="";
  public selectedQuestionForDeletion={id:[],index:[]};
  public disableDeleteButton=true;
  public isProcessing=false;
  public isButtonClicked=false;
  public deleteProcessing=false;
  oldQuestions=[];
  permission;
    
  constructor(public exitSurveyService:ExitSurveyService,
              private router:Router,
              private activatedRoute:ActivatedRoute,
              public coreService:CoreService,
              public confirmService: ConfirmService,
              public viewContainerRef: ViewContainerRef) { }

  ngOnInit() {
    this.getAllExitSurveyQuestions();
    this.permission=this.coreService.getNonRoutePermission();
  }

  // add new question
  addNewQuestion(){
  this.hideAddPanel=true;
  this.category= {exitSurveyID:"",title:""};
  }
 
  // hide add question panel
  onCancel(){
    this.category=null;
    this.hideAddPanel=false;
  }

  // add exit survey question
  saveQuestion(){
    this.isButtonClicked=true;
    this.category.title=this.exitSurveyQuestion;
    
    this.exitSurveyService.addExitSurveyQuestion(this.category).subscribe(
      data=>{
        if (data.code == "200") {
        this.hideAddPanel=false;
        this.category=null;
        this.exitSurveyQuestion=null;
        this.isButtonClicked=false;
        this.getAllExitSurveyQuestions();
        this.coreService.notify("Successful",data.message,1)
        }
      },
      error=>{
        this.isButtonClicked=false;        
        this.coreService.notify("Unsuccessful",error.message,0);
        },  ()=>{}
    )
  }
 
 // get list of exit survey questions
  getAllExitSurveyQuestions(){
    this.isProcessing=true;
    this.exitSurveyService.getAllExitSurveyQuestions().subscribe(
      data=>{
        if (data.code == "200") {
           this.questions=data.data;
           this.oldQuestions=JSON.parse(JSON.stringify(data.data));
           this.isProcessing=false;
           this.disableDeleteButton=true;
        }      
      },
      error=>{this.isProcessing=false;
        this.coreService.notify("Unsuccessful",error.message,0);
      },
      ()=>{}
    )
  }

  // gives selected questions for delete
  selectQuestionsForDelete(id,e,index){
  if(e.checked) {
    this.selectedQuestionForDeletion.id.push(id); 
    this.selectedQuestionForDeletion.index.push(index);
    this.disableDeleteButton=false;
  }
  else {
    this.disableDeleteButton=true;
    this.selectedQuestionForDeletion.index=this.selectedQuestionForDeletion.index.filter(item=>item!=index);//this.selectedQuestionForDeletion.index.splice(index,1);
    this.selectedQuestionForDeletion.id=this.selectedQuestionForDeletion.id.filter(item=>item!=id);//this.selectedQuestionForDeletion.index.splice(id,1);  
  }
  if(this.selectedQuestionForDeletion.index.length==0) {
    this.selectedQuestionForDeletion={id:[],index:[]}; this.disableDeleteButton=true;}
  }

  // delete questions
  deleteQuestion(){
    this.confirmService.confirm(
      this.confirmService.tilte,
      this.confirmService.message,
      this.viewContainerRef
    )
    .subscribe(res => {
      let result = res;
      if (result) {
        this.deleteProcessing=true;
        let deleteArray={data:this.selectedQuestionForDeletion.id};
        this.exitSurveyService.deleteQuestion(JSON.stringify(deleteArray)).subscribe(
      data=>{
        if (data.code == "200") {
           this.selectedQuestionForDeletion={id:[],index:[]}
           this.getAllExitSurveyQuestions();
           this.deleteProcessing=false;
        this.coreService.notify("Successful",data.message,1)
        }      
      },
      error=>{
        this.deleteProcessing=false;
        this.coreService.notify("Unsuccessful",error.message,0),
        this.selectedQuestionForDeletion={id:[],index:[]}
        },
      ()=>{}
    )
  }})
  }

 // edit exit survey question
 editSurveyQuestion(question,e){     
    if(e){  
    let editedQuestion={
      title:question.title,
      exitSurveyID:question.exitSurveyID
    }
    this.exitSurveyService.addExitSurveyQuestion(editedQuestion).subscribe(
      data=>{
        if (data.code == "200") {
           this.oldQuestions=JSON.parse(JSON.stringify(this.questions));          
        this.hideAddPanel=false;
        this.category=null;
        this.exitSurveyQuestion=null;
        this.coreService.notify("Successful","Exit survey is updated successfully.",1)
        }
      },
      error=>{
        this.coreService.notify("Unsuccessful",error.message,0);
        },  ()=>{}
    )
  }else{
     for(let q of this.oldQuestions){
       if(question.exitSurveyID==q.exitSurveyID){
         question.title=q.title;
         this.coreService.notify("Unsuccessful","Question is required !",0);
       }
     }
            
  }
 }
}
