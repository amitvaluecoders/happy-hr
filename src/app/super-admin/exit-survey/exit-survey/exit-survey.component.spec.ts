import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExitSurveyComponent } from './exit-survey.component';

describe('ExitSurveyComponent', () => {
  let component: ExitSurveyComponent;
  let fixture: ComponentFixture<ExitSurveyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExitSurveyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExitSurveyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
