  import {
      Injectable      
  } from '@angular/core';
  import {
      CanActivate,
      Router,
      Route,      
      RouterStateSnapshot,
      ActivatedRouteSnapshot
  } from "@angular/router";
  import {
      CoreService
  } from '../shared/services';
  import {
      Observable
  } from 'rxjs/Rx'; 
  import 'rxjs/add/operator/map'

  @Injectable()
  export class SuperAdminGuardService implements CanActivate {

      public role="superAdmin";
      constructor(private _core: CoreService, private router: Router)
        { }

      canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {         
          if(this.checkHavePermission(route['_routeConfig']['path'])){
            return true;
          }else{
            this.router.navigate(['/extra/404']);
            return false;
          }
      }

      private checkHavePermission(path) {
          switch (path) {
              case "":
                   return this._core.checkRole(this.role);
              default:
                  return false;
          }
      }
  }