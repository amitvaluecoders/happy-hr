import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { SurveyQuestionnareService} from '../survey-questionnare.service';
import { CoreService } from '../../../shared/services/core.service';


@Component({
  selector: "app-questionnaires-list",
  templateUrl: "./questionnaires-list.component.html",
  styleUrls: ["./questionnaires-list.component.scss"]
})
export class QuestionnairesListComponent implements OnInit {
  public searchKeySurveyType = "";
  public categories = [];
  public offset = "0-9";
  public limit = "10";
  public filter={
    surveyCatID:{}
  };
  public searchKeyCategoryType='';
  public categoryTitles=[];
  public searchText='';
  public isProcessing=false;
  public isFilterProcessing=false;
  public categoryType = [];
  public status = [];
  public cancelRequest
  constructor(
    public surveyQuestionnareService: SurveyQuestionnareService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    public coreService: CoreService,
  ) {}
  ngOnInit() {
    this.getCategoryTitles();
  }

  getCategoryTitles(){
    this.isProcessing=true;
    this.surveyQuestionnareService.getCategories().subscribe(
      data=>{
        if (data.code == "200") {
           this.categoryTitles=data.data;
           if(this.categoryTitles.length){
           for(let category of this.categoryTitles) this.filter.surveyCatID[category.surveyCatID]=false;
           this.getCategories();
           }else{
             this.isProcessing=false;
           }
        }      
      },
      error=>{this.isProcessing=false;this.coreService.notify("Unsuccessful",error.message,0)},
      ()=>{}
    )
  }

  getCategories(){
    this.isFilterProcessing=true;
    let reqParameters={
      search:this.searchText,
      filter:this.filter};
       this.cancelRequest = this.surveyQuestionnareService.getAllCategories(JSON.stringify(reqParameters)).subscribe(
      data=>{
        if (data.code == "200") {
           this.categories=data.data;
            this.isFilterProcessing=false;
           this.isProcessing=false;
        }      
      },
      error=>{this.isFilterProcessing=false;
        this.isProcessing=false;
        this.coreService.notify("Unsuccessful",error.message,0);},
      ()=>{}
    )
  }

  onFilter() {
    this.categories=[];
    this.cancelRequest.unsubscribe();
    this.getCategories();
  }
}
