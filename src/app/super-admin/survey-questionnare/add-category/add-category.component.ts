import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { SurveyQuestionnareService} from '../survey-questionnare.service';
import { CoreService } from '../../../shared/services/core.service';

@Component({
  selector: 'app-add-category',
  templateUrl: './add-category.component.html',
  styleUrls: ['./add-category.component.scss']
})
export class AddCategoryComponent implements OnInit {

  public category;
  public categories=[];
  public numberOfCategories;
  public hideAddPanel=false;
  public searchText='';
  public searchKeyCategoryType='';
  public filter={
    surveyCatID:{title:''}
  };
  public categoryTitles=[];
  public isProcessing=false;
  public isAddProcessing=false;
  constructor(public surveyQuestionnareService:SurveyQuestionnareService,private router:Router,private activatedRoute:ActivatedRoute,public coreService:CoreService) { }

  ngOnInit() {
    this.getCategories();
  }

  addNewCategory(){
  this.hideAddPanel=true;
  this.category= {title:""};
  }

  cancelCategory(){
    this.category=null;
    this.hideAddPanel=false;
  }

  saveCategory(){
    this.isAddProcessing=true;
    let addCategory={title:this.category.name}
    this.surveyQuestionnareService.addCategory(this.category).subscribe(
      data=>{
        if (data.code == "200") {
        this.hideAddPanel=false;
        this.category=null;
        this.getCategories();
        this.coreService.notify("Successful",data.message,1)
        }else{
          this.hideAddPanel=false;
          this.category=null;
          this.coreService.notify("Warning",data.message,2)
        }
      },
      error=>this.coreService.notify("Unsuccessful",error.message,0),
      ()=>{}
    )
  }

  getCategories(){
    this.isProcessing=true;
    let reqParameters={
      search:this.searchText,
      filter:this.filter};
        this.surveyQuestionnareService.getAllCategories(JSON.stringify(reqParameters)).subscribe(
      data=>{
        if (data.code == "200") {
           this.categories=data.data;
           this.isAddProcessing=false;           
           this.isProcessing=false;
        }      
      },
      error=>{this.isAddProcessing=false;this.isProcessing=false;this.coreService.notify("Unsuccessful",error.message,0);},
      ()=>{}
    )
  }

  onFilter(){
    this.getCategories();
  }

}
