import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { MaterialModule } from '@angular/material';
import { DataTableModule} from "angular2-datatable";
import { QuestionnairesListComponent } from './questionnaires-list/questionnaires-list.component';
import { QuestionnareDetailComponent } from './questionnare-detail/questionnare-detail.component';
import { AddCategoryComponent } from './add-category/add-category.component';
import { SurveyQuestionnarePagesRoutingModule } from './survey-questionnare.routing';
import { SurveyQuestionnareService } from './survey-questionnare.service';
import { SharedModule } from '../../shared/shared.module';
import { InlineEditorModule } from 'ng2-inline-editor';
import { SurveyQuestionnaireGuardService } from './survey-questionnaire-guard.service';

@NgModule({
  imports: [
    CommonModule,
    SharedModule ,
    FormsModule,
    MaterialModule,
    DataTableModule,
    DataTableModule,
    SurveyQuestionnarePagesRoutingModule,
    InlineEditorModule
  ],
  providers:[SurveyQuestionnareService,SurveyQuestionnaireGuardService],
  declarations: [QuestionnairesListComponent, QuestionnareDetailComponent, AddCategoryComponent]
})
export class SurveyQuestionnareModule { }
