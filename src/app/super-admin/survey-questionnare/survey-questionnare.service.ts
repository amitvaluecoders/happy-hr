import { Injectable } from '@angular/core';
import { RestfulwebService} from '../../shared/services/restfulweb.service';
import { RestfulLaravelService } from '../../shared/services/restful-laravel.service';

@Injectable()
export class SurveyQuestionnareService {

  constructor(public restfulWebService:RestfulwebService,public restfulLaravelService:RestfulLaravelService) { }
  
  addCategory(category):any{
    return this.restfulLaravelService.post('admin/create-survey-category',category);
  }

   getCategories(){
    return this.restfulLaravelService.get('admin/list-survey-category');
 }

 getCategoryDetails(categoryID){
    return this.restfulLaravelService.get(`admin/detail-survey-category/${categoryID}`);   
 }

  getAllCategories(reqParams){
    return this.restfulLaravelService.get(`admin/category-questionner-count/${reqParams}`);
 }
  
  addQuestion(category){
   return this.restfulLaravelService.post('admin/add-update-survey-questionner',category);
  }

  deleteQuestion(category){
     return this.restfulLaravelService.delete(`admin/delete-survey-questionner/${category}`);
  }
}
