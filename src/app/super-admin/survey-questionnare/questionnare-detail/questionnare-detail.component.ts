import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { SurveyQuestionnareService } from '../survey-questionnare.service';
import { CoreService } from '../../../shared/services/core.service';
import { ConfirmService } from '../../../shared/services/confirm.service';
import { InlineEditorComponent } from 'ng2-inline-editor';

@Component({
  selector: 'app-questionnare-detail',
  templateUrl: './questionnare-detail.component.html',
  styleUrls: ['./questionnare-detail.component.scss']
})
export class QuestionnareDetailComponent implements OnInit {

  public hideAddPanel = false;
  public category;
  public addQuestion;
  public categories = [];
  public questions: any;
  public surveyQuestion = "";
  public selectedQuestionForDeletion = { id: "", index: [] };
  public disableDeleteButton = true;
  public numberOfCategories;
  public editable = true;
  public id;
  public isProcessing = false;
  public isListProcessing = false;
  public deleteProcessing = false;
  public color = '';
  public selectedIndex: number;
  public tooltipValue="Click to Edit";
  public saveProcessing=false;
  public disableAddButton = false;
  oldQuestions=[];
  permission;
  
  constructor(public surveyQuestionnareService: SurveyQuestionnareService, private router: Router, private activatedRoute: ActivatedRoute, public coreService: CoreService, public confirmService: ConfirmService,
    public viewContainerRef: ViewContainerRef) { }

  ngOnInit() {
    this.getCategories();
    if (this.activatedRoute.snapshot.params['id'] != undefined) {
      this.id = this.activatedRoute.snapshot.params['id'];
      this.getSurveyCategoryDetail(this.id);
    }else{
      
    }
    this.permission=this.coreService.getNonRoutePermission();
  }
  
  // shows add question panel
  addNewQuestion() {
    this.hideAddPanel = true;
    this.addQuestion = { surveyCatID: "", title: "", surveyQuetionnerID: "" };
  }
  
  // hides add question panel
  onCancel() {
    this.category = null;
    this.hideAddPanel = false;
  }

  // saves question
  saveQuestion() {
    this.saveProcessing=true;
    this.disableAddButton=true;
    this.addQuestion = {
      title: this.surveyQuestion,
      surveyCatID: this.category.surveyCatID,
      surveyQuetionnerID: ''
    }

    this.surveyQuestionnareService.addQuestion(this.addQuestion).subscribe(
      data => {
        if (data.code == "200") {
          this.hideAddPanel = false;
          this.category = null;
          this.surveyQuestion = null;
          this.editable = true;
          this.disableDeleteButton = true;
          this.getSurveyCategoryDetail(this.addQuestion.surveyCatID);
          this.saveProcessing=false;
    this.disableAddButton=false;
    
          this.coreService.notify("Successful", data.message, 1)
        } else {
          this.hideAddPanel = false;
          this.category = null;
          this.surveyQuestion = null;
          this.editable = true;
          this.disableDeleteButton = true;
          this.saveProcessing=false;   
          this.disableAddButton=false;    
          this.coreService.notify("Successful", data.message, 2)
        }
      },
      error => {this.saveProcessing=false;this.disableDeleteButton = true;this.disableAddButton=false;this.coreService.notify("Unsuccessful", error.message, 0)},
      () => { }
    )
  }

  // get survey questionnaire categories
  getCategories() {
    this.isProcessing = true;
    this.surveyQuestionnareService.getCategories().subscribe(
      data => {
        if (data.code == "200") {
          this.categories = data.data;
          if (!this.questions) {
            this.questions = this.categories[0];
          }
          this.isProcessing = false;
        }
      },
      error => { this.isProcessing = false; this.coreService.notify("Unsuccessful", error.message, 0) },
      () => { }
    )
  }

  // get survey questionnaire category questions
  getSurveyCategoryDetail(ID) {
    this.selectedIndex = ID;
    this.isListProcessing = true;
    this.surveyQuestionnareService.getCategoryDetails(ID).subscribe(
      data => {
        if (data.code == "200") {
          this.category = data.data;
          this.questions = this.category;
          this.oldQuestions=JSON.parse(JSON.stringify(data.data.questionData));
          this.hideAddPanel = false;
          this.editable = true;
          this.isListProcessing = false;
        }
      },
      error => { this.isListProcessing = false; this.coreService.notify("Unsuccessful", error.message, 0) },
      () => { }
    )
  }

  // selected questions for delete  
  selectQuestionsForDelete(id, e, index) {
    if (e.checked) {
      this.selectedQuestionForDeletion.id = id;
      this.selectedQuestionForDeletion.index.push(id);
      this.disableDeleteButton = false;
    }
    else {
      this.selectedQuestionForDeletion.index = this.selectedQuestionForDeletion.index.filter((item) => {
        if (item != id) return this.selectedQuestionForDeletion.index;
      })
    } if (this.selectedQuestionForDeletion.index.length == 0) {
      this.selectedQuestionForDeletion = { id: "", index: [] }; this.disableDeleteButton = true;
    }
  }

  // delete questions
  deleteQuestion() {
    this.confirmService.confirm(
      this.confirmService.tilte,
      this.confirmService.message,
      this.viewContainerRef
    )
      .subscribe(res => {
        let result = res;
        if (result) {
          this.deleteProcessing = true;
          let deleteParams = { data: this.selectedQuestionForDeletion.index }
          this.surveyQuestionnareService.deleteQuestion(JSON.stringify(deleteParams)).subscribe(
            data => {
              if (data.code == "200") {
                this.disableDeleteButton = true;
                this.selectedQuestionForDeletion = { id: "", index: [] }
                this.deleteProcessing = false;
                this.getSurveyCategoryDetail(this.questions.surveyCatID);
                this.coreService.notify("Successful", data.message, 1)
              }
            },
            error => { this.deleteProcessing = false; this.coreService.notify("Unsuccessful", error.message, 0) },
            () => { }
          )
        }
      })
  }

  onEdit(data) {
    this.editable = false;
  }

  // edit question
  editQuestion(question, categoryID) {
    if(question.title){
    let editQuestion = {
      title: question.title,
      surveyCatID: categoryID,
      surveyQuetionnerID: question.surveyQuetionnerID
    }
    this.surveyQuestionnareService.addQuestion(editQuestion).subscribe(
      data => {
        if (data.code == "200") {
          this.oldQuestions=JSON.parse(JSON.stringify(this.questions.questionData));          
          this.hideAddPanel = false;
          this.category = null;
          this.surveyQuestion = null;
          this.editable = true;
          this.disableDeleteButton = true;
          this.coreService.notify("Successful", data.message, 1)
        }
      },
      error => this.coreService.notify("Unsuccessful", error.message, 0),
      () => { }
    )
  }else{
     for(let q of this.oldQuestions){
       if(question.surveyQuetionnerID==q.surveyQuetionnerID){
         question.title=q.title;
         this.coreService.notify("Unsuccessful","Question is required !",0);
       }
     }            
  }
  }

  editCancel() {
    this.editable = true;
  }
}
