import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QuestionnareDetailComponent } from './questionnare-detail.component';

describe('QuestionnareDetailComponent', () => {
  let component: QuestionnareDetailComponent;
  let fixture: ComponentFixture<QuestionnareDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QuestionnareDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuestionnareDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
