import { Routes, RouterModule }  from '@angular/router';
import { QuestionnairesListComponent } from './questionnaires-list/questionnaires-list.component';
import { QuestionnareDetailComponent } from './questionnare-detail/questionnare-detail.component';
import { AddCategoryComponent } from './add-category/add-category.component';
import { SurveyQuestionnaireGuardService } from './survey-questionnaire-guard.service';

export const SurveyQuestionnarePagesRoutes: Routes = [
    {
        path: '',
        children: [
            { path: 'detail/:id', component: QuestionnareDetailComponent,canActivate:[SurveyQuestionnaireGuardService] }     
        ]
    }
];

export const SurveyQuestionnarePagesRoutingModule = RouterModule.forChild(SurveyQuestionnarePagesRoutes);
