import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { CompanyService } from '../company.service';
import { CoreService } from '../../../shared/services/core.service';

@Component({
  selector: 'app-company-listing',
  templateUrl: './company-listing.component.html',
  styleUrls: ['./company-listing.component.scss']
})
export class CompanyListingComponent implements OnInit {
  public toggleBtn: boolean;
  public companyStatus = {
    active: false,
    inactive: false,
    subcompany: false,
    company: false,
    new: false
  };
  public searchText = '';
  public state = {
    Active: false,
    Inactive: false
  };
  public companyTypeState = {
    subCompany: false,
    company: false,
  };
  public cancelRequest;
  public lastLoginState = {
    "30": false,
    "60": false,
    "90": false,
    "120": false,
    "150": false
  };
  public filters = {
    status: {},
    registration: { "new": false },
    companyType: {},
    lastLogin: {},
  };
  status = [];
  companyType = [];
  lastLogin = [];
  public isProcessing = false;
  public filterArray = [];
  public filterArrayType = [];
  public companies = [];
  public searchLastLoginType = '';
  public searchCompanyType = '';

  constructor(private router: Router, private companyService: CompanyService, private coreService: CoreService) { }

  ngOnInit() {
    this.toggleBtn = false;
    this.status = Object.keys(this.state);
    for (let s of this.status) {
      this.filters.status[s] = false;
    }
    this.companyType = Object.keys(this.companyTypeState);
    for (let c of this.companyType) {
      this.filters.companyType[c] = false;
    }
    this.lastLogin = Object.keys(this.lastLoginState);
    for (let l of this.lastLogin) {
      this.filters.lastLogin[l] = false;
    }
    this.getAllCompanies();
  }

  getAllCompanies() {
    this.isProcessing = true;
    let search = encodeURIComponent(this.searchText);
    let reqBody = JSON.stringify({
      search: search,
      filter: this.filters
    });
    this.cancelRequest = this.companyService.getCompanies(reqBody).subscribe(
      data => {
        if (data.code == 200) {
          this.companies = data.data;
          this.isProcessing = false;
        }
      }, error => {
        this.isProcessing = false;
        this.coreService.notify("Unsuccessful", error.message, 0)
      }
    );
  }

  setFilter(el) {
    let i = this.companyStatus;

    this.filterArray[el.name] = el.checked;

    for (let c in i) {
      if (i[c]) this.filterArray[c] = true;
      else delete this.filterArray[c];
    }
    if (i.active && i.inactive) {
      delete this.filterArray['active'];
      delete this.filterArray['inactive'];
    }
  }

  onFilter() {
    this.cancelRequest.unsubscribe();
    this.getAllCompanies();
  }

  onClick(companyID) {
    this.router.navigate([`/app/super-admin/company/details/${companyID}`]);
  }

}
