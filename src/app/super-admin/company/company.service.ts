import { Injectable } from '@angular/core';
import { RestfulwebService } from '../../shared/services/restfulweb.service'
import { RestfulLaravelService } from '../../shared/services/restful-laravel.service';


@Injectable()
export class CompanyService {

  constructor(
    private restfulWebService: RestfulwebService, 
    private restfulLaravelService : RestfulLaravelService,
  ){}

  editCompany(company){
    return this.restfulLaravelService.put(`admin/company-management-detail-edit`,company);    
  }

  getCompanies(reqBody){
    return this.restfulLaravelService.get(`admin/company-management/${encodeURIComponent(reqBody)}`);     
  }

  getCompanyDetails(reqBody) { 
    return this.restfulLaravelService.get(`admin/company-management-detail/${reqBody}`);         
  }

  deleteCompany(reqBody){
    return this.restfulLaravelService.delete(`admin/company-management-delete/${reqBody}`);             
  }

  getSubcompanies(reqBody){
    return this.restfulWebService.get(`subCompanies/${reqBody}`);         
  }

  getCompanyActivites(reqBody){
     return this.restfulWebService.get(`companieActivities/${reqBody}`);         
  }

  getCompanyNotes(reqBody){
     return this.restfulLaravelService.get(`admin/company-management-note-list/${reqBody}`);         
  }

  addEditCompanyNote(reqBody){
    return this.restfulLaravelService.post(`admin/company-management-add-edit-note`,reqBody);         
  }

  getApiList(){
    return this.restfulWebService.get(`apisList`);            
  }

  getModulesList(reqBody){
    return this.restfulWebService.get(`modulesList/${reqBody}`);                
  }
  
  getCompanyBroadcastTypes(reqBody){
    return this.restfulWebService.get(`companyBroadcastTypes/${reqBody}`);                    
  }

  getCompanyMessageList(reqBody){
    return this.restfulWebService.get(`companyMessagesList/${reqBody}`);                    
  }

  companyPostMessage(reqBody){
    return this.restfulWebService.post(`companyPostMessage`,reqBody);                    
  }

  getMessages(reqBody):any{
    return this.restfulLaravelService.get(`admin/broadcast-message-list/${reqBody}`);
   }
 
   getList(reqBody):any{
     return this.restfulLaravelService.get(`admin/broadcast-message-group/${reqBody}`);
   }
 
   addMessage(message):any{
     return this.restfulLaravelService.post('admin/broadcast-message-add',message);
   }

   uploadFile(reqBody){
    return this.restfulLaravelService.post('upload-file',reqBody);  
  }

   incognito(reqBody){
    return this.restfulLaravelService.get(`admin/login-as-ca/${reqBody}`);   
  }

}