import { Routes, RouterModule }  from '@angular/router';
import { CompanyListingComponent } from './company-listing/company-listing.component';
import { CompanyDetailsComponent } from './company-details/company-details.component';
import { BroadCastComponent } from './broad-cast/broad-cast.component';
import { MessageListingComponent } from './message-listing/message-listing.component';
import { CompanyGuardService } from './company-guard.service';

export const CompanyPagesRoutes: Routes = [
    {
        path: '',
        children: [
            { path: '', component: CompanyListingComponent,canActivate:[CompanyGuardService] },
            { path: 'details/:id', component: CompanyDetailsComponent,canActivate:[CompanyGuardService] },
            { path: 'broadcast/:id', component: BroadCastComponent,canActivate:[CompanyGuardService] },
            { path: 'broadcast/message/:type/:id', component: MessageListingComponent,canActivate:[CompanyGuardService] },
        ]
    }
];

export const CompanyPagesRoutingModule = RouterModule.forChild(CompanyPagesRoutes);
