import { Component, OnInit } from '@angular/core';
import { MdSnackBar, MdDialog, MdDialogRef } from '@angular/material';
import { CompanyService } from '../company.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { CoreService } from '../../../shared/services/core.service';

@Component({
  selector: 'app-message-listing',
  templateUrl: './message-listing.component.html',
  styleUrls: ['./message-listing.component.scss']
})
export class MessageListingComponent implements OnInit {

    type='';
    title='';
    isProcessing=false;
    messageProcessing=false;
    id:'';
   constructor(public snackBar: MdSnackBar, 
                public dialog: MdDialog,
                private activatedRoute: ActivatedRoute,
                private companyService:CompanyService,
                private coreService:CoreService) {}
                public messages=[];
  
  ngOnInit() {
      this.id = this.activatedRoute.snapshot.params['id'];
      this.type=this.activatedRoute.snapshot.params['type'];
      if(this.type=='all') this.title="all";
      else if(this.type=='company-admin') this.title="company admins";
      else if(this.type=='company-manager') this.title="company managers";
      else this.title="company employees";
      
      this.getMessages();
  }


  onTime(time){return new Date(time);}

getMessages(){
    this.isProcessing=true;
  let reqBody = {
      group:this.type,
      companyID:this.id
  };
this.companyService.getMessages(JSON.stringify(reqBody)).subscribe(
    data => {
        if (data.code == "200") {  
            this.messages=data.data;
            this.isProcessing=false;
        //this.coreService.notify('Successful',data.message,0)
        }
    },
    error=>{
        this.isProcessing=false;
        this.coreService.notify('Unsuccessful',error.message,0)
    }
);
}
createNewMessage() {
    let dialogRef = this.dialog.open(CreateNewMessageCompanyDailog, {
        width: '400px'
    });
    dialogRef.componentInstance.type=this.type;
    dialogRef.componentInstance.title=this.title;  
    dialogRef.componentInstance.messages=this.messages;
    dialogRef.componentInstance.id=this.id;    
    dialogRef.afterClosed().subscribe(result => {
        if(result){
        if (result.value=="success") {
          this.coreService.notify('Successful',result.message,1)
        }
        else if (result.value=="failure") this.coreService.notify('Unsuccessful',result.message,0)
        else {}
        }  
    });
}
}

@Component({
    selector: 'create-new-message-dialog',
    template: `<h1 md-dialog-title>Message to: {{title}}</h1>
        <div md-dialog-content style="padding-bottom: 20px;">
         <form #messageForm="ngForm" name="material_signup_form" class="md-form-auth form-validation" >
          
          <md-input-container class="full-width">
            <textarea required rows="4" name="userMessage" [(ngModel)]="userMessage" 
                      #message="ngModel" 
                      mdInput placeholder="Enter your message here"></textarea>
          </md-input-container > 
          <div *ngIf="message.errors && (message.dirty || message.touched)">
                    <div style="color:red" [hidden]="!message.errors.required">
                    Message is required !
                    </div>
                </div>
          </form>            
        </div>
        <md-progress-spinner *ngIf="sendingMessage" mode="indeterminate" color=""></md-progress-spinner>    
        <div md-dialog-actions *ngIf="!sendingMessage">
            <button md-raised-button color="primary" (click)="onSubmit(messageForm.form.valid)">Broadcast message</button>
            <button md-button (click)="dialogRef.close(false,false)">Cancel</button>
        </div>`,
})

export class CreateNewMessageCompanyDailog {
  public userMessage='';
  type;
  messages;
  title;
  id;
  sendingMessage=false;
  public whiteSpaceError=false;
    
    constructor(
        public dialogRef: MdDialogRef<CreateNewMessageCompanyDailog>,
        private companyService:CompanyService,
        private coreService:CoreService) {}

    onSubmit(isvalid){
        this.sendingMessage=true;
        let trimmedMessgae=this.userMessage.trim();
        let valid=(isvalid && (trimmedMessgae!=""))
        if(valid){
            let reqBody = {
                group:this.type,
                message: this.userMessage,
                companyID:this.id
            };
          this.companyService.addMessage(reqBody).subscribe(
              data => {
                  if (data.code == "200") {
                      this.messages.unshift(data.data);
                      this.sendingMessage=false;  
                      this.dialogRef.close({value:"success",message:data.message});
                  }
              },
              error=>{
                this.sendingMessage=false; 
                this.dialogRef.close({value:"failure",message:error.message});                 
              }
          );
        }
      }
   
}
