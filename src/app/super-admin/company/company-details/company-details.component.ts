import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { CompanyService } from '../company.service';
import { MdSnackBar, MdDialog, MdDialogRef } from '@angular/material';
import { CoreService } from '../../../shared/services/core.service';
import { ConfirmService } from '../../../shared/services/confirm.service';
import { Cookie } from 'ng2-cookies/ng2-cookies';
import { LayoutComponent } from '../../../layout/layout.component';
import {Location} from '@angular/common';
// import { AppSidenavMenuComponent } from '../../../layout/sidenav/sidenav-menu/sidenav-menu.component';

@Component({
  selector: 'app-company-details',
  templateUrl: './company-details.component.html',
  styleUrls: ['./company-details.component.scss']
})
export class CompanyDetailsComponent implements OnInit {
public isprocessing=true;
public note={
  noteID:null,
  text:''
};
public notes=[];
public activityLog=[];
public apiList =[];
public editable:Boolean=true;
public hideSaveButton=true;
deleteProcessing=false;
uploadProcessing=false;
company={
    ABN:null,
    address:"",
    adminContactNumber:null,
    adminEmail:"",
    appDownload:"",
    companyAdmin:"",
    companyContactNumber:null,
    companyID:null,
    countryName:"",
    employeeCount:0,
    id:null,
    lastLogin:"",
    lastPayment:"",
    location:"",
    logo:"",
    logoName:null,
    name:"",
    note:null,
    revenue:"",
    sessionTime:"",
    state:"",
    status:"",
    type:"",
    upcomingPayment:'',
    website:null,
    zipCode:''
  };


public subCompanies=[];
public isDetailProcessing=false;
public isNoteProcessing=false;
isSaveProcessing=false;
permission;
loginData;
isIncognitoProcessing;
role;

  constructor(public location:Location,public snackBar: MdSnackBar,private confirmService:ConfirmService,private layoutComponent:LayoutComponent, private companyService: CompanyService,private router: Router,private activatedRoute: ActivatedRoute,private coreService:CoreService, private viewContainerRef:ViewContainerRef) { }
  id;
  ngOnInit() {
    
    this.id = this.activatedRoute.snapshot.params['id'];
     this.getCompanyDetails();
     
    //  this.getSubCompanies(id);
    //  this.companyActivites(id);
      this.getCompanyNotes();
      this.permission=this.coreService.getNonRoutePermission();
    //  this.getApiList();
  }

  getCompanyDetails() {
   // let reqBody=JSON.stringify({"companyid":id});
   this.isDetailProcessing=true;
      this.companyService.getCompanyDetails(this.id).subscribe(
        d => {
            if(d.code=="200"){
              this.company=d.data.companyData;
              this.subCompanies=d.data.subCompanyData;
              this.activityLog = d.data.activityLog?d.data.activityLog:[];
              this.apiList = d.data.apiImplementedData?d.data.apiImplementedData:[];
              this.isDetailProcessing=false;  
            }
        },e=>{
          this.isDetailProcessing=false;
          this.coreService.notify("Unsuccessful",e.message,0)
        }
      );
  }


//   getSubCompanies(id) {
//     let reqBody=JSON.stringify({companyId:id,offset:30,limit:15});
//       this.companyService.getSubcompanies(reqBody).subscribe(
//         d => {
//             if(d.response.code=="200"){
//               console.log(d.response.data)
//               console.log("get sub companies");

//             }
//         },
//         ()=>console.log('a')
//       );
//   }


//   companyActivites(id) {
//      let reqBody=JSON.stringify({companyId:id,offset:30,limit:15});
//       this.companyService.getCompanyActivites(reqBody).subscribe(
//         d => {
//             if(d.response.code=="200"){
//               console.log(d.response.data)
//               console.log("get companies activites");

//             }
//         },
//         ()=>console.log('a')
//       );

//   }

  getCompanyNotes(){
     //let reqBody=JSON.stringify({companyId:id,offset:30,limit:15});
     this.isNoteProcessing=true;
      this.companyService.getCompanyNotes(this.id).subscribe(
        d => {
            if(d.code=="200"){
              this.notes=d.data;
              this.isNoteProcessing=false;
            }
        },e=>{
          this.isNoteProcessing=false;          
          this.coreService.notify("Unsuccessful",e.message,0)          
        }
      );    
  }


// getApiList() {

//     this.companyService.getApiList().subscribe(
//       d => {
//             if(d.response.code=="200"){
//               console.log(d.response.data)
//               console.log("get api list");
//             }
//         },
//         ()=>console.log('a')
//    );
//  }


  onEdit(){
    this.editable=false;
  }

 onSave(isvalid){
   if(isvalid){
    this.isprocessing=false;
      this.companyService.editCompany(this.company).subscribe(
        d=> {     
            if(d.code=="200"){
              this.isprocessing=true;
              this.editable=true;
              this.coreService.notify("Successful",d.message,1)
            }
        },e=>{
          this.isprocessing=true;          
          this.coreService.notify("Unsuccessful",e.message,0)
        }
      );
  }
 }

 onDelete(){
  this.confirmService
  .confirm(
    this.confirmService.tilte,
    this.confirmService.message,
    this.viewContainerRef
  )
  .subscribe(res => {
    if (res) {
      this.deleteProcessing=true;
  this.companyService.deleteCompany(this.id).subscribe(
    d=> {
        if(d.code=="200"){
          this.deleteProcessing=false;
          this.router.navigate(['app/super-admin/company']);
          this.coreService.notify("Successful",d.message,1);
        }
    },e=>{
      this.deleteProcessing=false;
      this.coreService.notify("Unsuccessful",e.message,0)
    }
  );
 }})
 }


 addEditCompanyNote(note) {
   this.isSaveProcessing=true;
   let reqBody = {};
   if (note.noteID) reqBody = { companyID: this.activatedRoute.snapshot.params['id'], noteID: note.noteID, text: note.text };
   else reqBody = { companyID: this.activatedRoute.snapshot.params['id'], noteID: note.noteID, text: note.text };
   this.companyService.addEditCompanyNote(reqBody).subscribe(
     d => {
            if(d.code=="200"){
              this.note={
                noteID:'',
                text:''
              };
              this.getCompanyNotes();
              this.isSaveProcessing=false;
              this.coreService.notify("Successful",d.message,1);
            }
        },e=>{
          this.isSaveProcessing=false;          
          this.coreService.notify("Unsuccessful",e.message,0)          
        }
   );
 }

 

//  getModulesList(apiId){
//      let reqBody=JSON.stringify({apiId:apiId});
//     this.companyService.getModulesList(reqBody).subscribe(
//     d => {
//             if(d.response.code=="200"){
//               console.log(d.response.data)
//               console.log("get module list");
//             }
//         },
//         ()=>console.log('a')
//    );

//  }
 onBroadcastMessage(){
    let id = this.activatedRoute.snapshot.params['id'];
    this.router.navigate([`/app/super-admin/company/broadcast/${id}`]);
 }

 onMessage(){

 }

 handleFileSelect($event) {
  //this.isProcessingImg = true;
  //this.isUploading=true;
  this.uploadProcessing=true;
  const files = $event.target.files || $event.srcElement.files;
  const file = files[0];
  //if(files[0]) this.messageFiles.push({attachmentName:files[0].name})
  const formData = new FormData();
  formData.append('companyLogo', file);
  this.companyService.uploadFile(formData).subscribe(
    data => {
      this.company.logoName=data.data.companyLogo;
      this.uploadProcessing=false;
    },error=>{				
      this.uploadProcessing=false;        
      this.coreService.notify("Unsuccessful",error.message,0);
  });
}

onIncognito(){
  this.companyService.incognito(this.id).subscribe(
    d => {
        if(d.code=="200"){
          let url;
          Cookie.deleteAll();
          localStorage.clear();
          this.loginData=d.data;
          Cookie.set('happyhr-token',d.token);
          this.coreService.setRole(d.data.userRole);
          // Cookie.set('happyhr-role',d.data.userRole);
          Cookie.set('happyhr-userInfo',JSON.stringify(this.loginData));          
             
         this.coreService.setPermission(d.data.modules); 
         Cookie.set('happyhr-permission',JSON.stringify(this.coreService.permission));          
          localStorage.setItem('happyhr-permission',JSON.stringify(this.coreService.permission));
          localStorage.setItem('happyhr-userInfo',JSON.stringify(this.loginData));
          if(d.data.userRole=="companyAdmin"){
            this.role="companyAdmin";
            this.coreService.setRole(this.role);
            url='/app/company-admin';
          }          
          this.coreService.setRole(this.role);
          this.location.replaceState(url);
          window.location.reload();              
      }
    },e=>{          
      this.coreService.notify("Unsuccessful",e.message,0)          
    }
  ); 
}
}
