import { Component, OnInit } from '@angular/core';
import { CompanyService } from '../company.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { CoreService } from '../../../shared/services/core.service';


@Component({
  selector: 'app-broad-cast',
  templateUrl: './broad-cast.component.html',
  styleUrls: ['./broad-cast.component.scss']
})
export class BroadCastComponent implements OnInit {

  constructor(
    private activatedRoute: ActivatedRoute,
    private companyService:CompanyService,
    private coreService:CoreService,
    private router:Router) {}
     
    types=[];
    isProcessing=false;
    id='';
    ngOnInit() {
      this.id = this.activatedRoute.snapshot.params['id'];
      if(this.id) this.getList();    
  }
  
  //get users list
  getList(){
    this.isProcessing=true;
    let reqBody={
      companyID:this.id
    }
  this.companyService.getList(JSON.stringify(reqBody)).subscribe(
      data => {
          if (data.code == "200") { 
              this.types=data.data;
              this.isProcessing=false;
          }
      },
      error=>{
        this.isProcessing=false;
          this.coreService.notify('Unsuccessful',error.message,0)
      }
  );
  }

 // navigates to message list page
  onClick(title){
  if(title=="All(everyone within the company)") this.router.navigate(['/app/super-admin/company/broadcast/message','all',this.id])
  else if(title=="Company admins") this.router.navigate(['/app/super-admin/company/broadcast/message','company-admin',this.id])
  else if(title=="Company managers") this.router.navigate(['/app/super-admin/company/broadcast/message','company-manager',this.id])
  else this.router.navigate(['/app/super-admin/company/broadcast/message/','company-employee',this.id])        
  }

}
