import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from '@angular/material';
import { FormsModule } from '@angular/forms';
import { CompanyPagesRoutingModule } from './company-routing.module';
import { CompanyListingComponent } from './company-listing/company-listing.component';
import {DataTableModule} from "angular2-datatable";
import {StatusPipe} from '../../shared/pipes/status.pipe';
import { CompanyTypePipe } from '../../shared/pipes/company-type.pipe';
import { CompanyDetailsComponent } from './company-details/company-details.component';
import { BroadCastComponent } from './broad-cast/broad-cast.component';
import { MessageListingComponent, CreateNewMessageCompanyDailog } from './message-listing/message-listing.component';
import { CompanyService } from './company.service';
import { SharedModule } from '../../shared/shared.module';
import { CompanyGuardService } from './company-guard.service';
@NgModule({
  imports: [
    DataTableModule,
    CommonModule,
    FormsModule,
    MaterialModule,
    CompanyPagesRoutingModule,
    SharedModule,
  ],
  providers:[CompanyService,CompanyGuardService],
  entryComponents: [
    CreateNewMessageCompanyDailog
    ],
  declarations: [CompanyListingComponent,CreateNewMessageCompanyDailog,StatusPipe,CompanyTypePipe, CompanyDetailsComponent, BroadCastComponent, MessageListingComponent]
})
export class CompanyModule { }
