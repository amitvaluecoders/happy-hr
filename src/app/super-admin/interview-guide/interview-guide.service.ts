import { Injectable } from '@angular/core';
import { RestfulwebService } from '../../shared/services/restfulweb.service'
import { RestfulLaravelService } from '../../shared/services/restful-laravel.service'


@Injectable()
export class InterviewGuideService {

  constructor(private restfulWebService: RestfulwebService,private restfulLaravelService:RestfulLaravelService){}

  getInterviewGuideQuestions(){
    return this.restfulLaravelService.get(`admin/interview-guide-list`);     
  }

  editInterviewGuideQuestions(reqBody){
    return this.restfulLaravelService.put(`admin/update-interview-component`,reqBody);     
  }
 
  deleteInterviewGuideQuestion(reqBody){
    return this.restfulLaravelService.delete(`admin/interview-guide-delete/${reqBody}`);     
  }

  addInterviewGuideQuestion(reqBody){
    return this.restfulLaravelService.post(`admin/add-interview-component`,reqBody); 
  }
}