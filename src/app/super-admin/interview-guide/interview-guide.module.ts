import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from '@angular/material';
import { FormsModule } from '@angular/forms';
import { MyDatePickerModule } from 'mydatepicker';
import { DataTableModule} from "angular2-datatable";
import { InterviewGuideComponent,AddInterviewGuideQuestionDialog } from './interview-guide/interview-guide.component';
import { InterviewGuidePagesRoutingModule } from './interview-guide.routing';
import { InterviewGuideService } from './interview-guide.service';
import { InterviewGuideGuardService } from './interview-guide-guard.service';
@NgModule({
  imports: [
    CommonModule,
    MaterialModule,
    FormsModule,
    DataTableModule,
    MyDatePickerModule,
    InterviewGuidePagesRoutingModule
  ],
  declarations: [InterviewGuideComponent,AddInterviewGuideQuestionDialog],
  providers:[InterviewGuideService,InterviewGuideGuardService],
  entryComponents:[AddInterviewGuideQuestionDialog]
})
export class InterviewGuideModule { }
