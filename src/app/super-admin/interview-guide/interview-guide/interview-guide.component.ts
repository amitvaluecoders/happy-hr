import { Component, OnInit,ViewContainerRef } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { InterviewGuideService } from '../interview-guide.service';
import { CoreService } from '../../../shared/services/core.service';
import { ConfirmService } from '../../../shared/services/confirm.service';
import { MdDialog, MdDialogRef } from '@angular/material';

@Component({
  selector: 'app-interview-guide',
  templateUrl: './interview-guide.component.html',
  styleUrls: ['./interview-guide.component.scss']
})
export class InterviewGuideComponent implements OnInit {

  public interviewGuide={
    attitude:[],
    behavior:[],
    compentancy:[],
    integrity:[]
  }

public loader=false;

public attititudeQuetionArray=[];
public coreCompetencyArray=[];
public integrityArray=[];
public behaviourArray=[];
public interviewGuideArray=[];


public attititudeQuestionEditBox=false;
public coreCompetencyEditBox=false;
public integrityEditBox=false;
public behaviourEditBox=false;
public interviewGuideEditBox=false;

public selectedNumberAttitude;
public questionAttitude='';
public showQuestionAttitude=false;
public attitudeLoader=true;

public selectedNumberCoreCompetency;
public questionCoreCompetency='';
public showQuestionCoreCompetency=false;
public coreCompetencyLoader=true;

public selectedNumberBehaviour;
public questionBehaviour='';
public showQuestionBehaviour=false;
public behaviourLoader=true;

public selectedNumberIntegrity;
public questionIntegrity='';
public showQuestionIntegrity=false;
public integrityLoader=true;

permission;

  constructor(public interviewGuideService:InterviewGuideService,public confirmService: ConfirmService,
    public dialog: MdDialog,public viewContainerRef:ViewContainerRef, public coreService:CoreService) { }

  ngOnInit() {
    this.getInterviewGuideQuestions();
    this.permission=this.coreService.getNonRoutePermission();
  }

  // get question numbers 
  getQuestion(type){
    if(type=='attitude'){
    this.attititudeQuetionArray.filter(item=>{
     if(this.selectedNumberAttitude==item.attitudeID) {this.showQuestionAttitude=true;this.questionAttitude=item.title;}
    })
    }
    if(type=='compentancy'){
      this.coreCompetencyArray.filter(item=>{
    if(this.selectedNumberCoreCompetency==item.compentancyID) {this.showQuestionCoreCompetency=true;this.questionCoreCompetency=item.title;}
    })
    }

   if(type=='behaviour'){
     this.behaviourArray.filter(item=>{
    if(this.selectedNumberBehaviour==item.behaviorID) {this.showQuestionBehaviour=true;this.questionBehaviour=item.title;}
   })
   }

   if(type=='integrity'){
     this.integrityArray.filter(item=>{
   if(this.selectedNumberIntegrity==item.integrityID) {this.showQuestionIntegrity=true;this.questionIntegrity=item.title;}
    })
   }
  }
  
  // get interview guide questions list
  getInterviewGuideQuestions(){
    //this.isProcessing=true;
    this.interviewGuideService.getInterviewGuideQuestions().subscribe(
      d=>{
        if(d.code=="200"){
          this.interviewGuide=d.data;
          this.attititudeQuetionArray= this.interviewGuide.attitude;
          if(this.attititudeQuetionArray) this.attitudeLoader=false;
          
          this.coreCompetencyArray= this.interviewGuide.compentancy;
          if(this.coreCompetencyArray) this.coreCompetencyLoader=false;

          this.behaviourArray= this.interviewGuide.behavior;
          if(this.behaviourArray) this.behaviourLoader=false;

          this.integrityArray= this.interviewGuide.integrity;
          if(this.integrityArray) this.integrityLoader=false;
        }
        else this.coreService.notify("Unsuccessful",d.message,0)
      },
      e=>this.coreService.notify("Unsuccessful",e.message,0))
  }

  // edit interview guide question
  onSave(isvalid,obj,question,questionID,type){
    if(type=="attitude") this.attitudeLoader=true;
    if(type=="compentancy") this.coreCompetencyLoader=true;
    if(type=="behaviour") this.behaviourLoader=true;
    if(type=="integrity") this.integrityLoader=true;    
    let interviewGuideType=type;
    if(interviewGuideType=="coreCompetency") interviewGuideType="compentancy";
    if(interviewGuideType=="behaviour") interviewGuideType="behavior";
    
    let reqParams={
      
      categoryTitle:interviewGuideType,
      questionID:questionID,
      title:question
    }
     this.interviewGuideService.editInterviewGuideQuestions(reqParams).subscribe(
      d=>{
        if(d.code=="200"){
          this.coreService.notify("Successful","Interview guide questions added/updated sucessfully",1)
          this[obj]=false;  
           if(type=="attitude"){this.selectedNumberAttitude='';this.questionAttitude='';this.showQuestionAttitude=false;}
           if(type=="compentancy"){this.selectedNumberCoreCompetency='';this.questionCoreCompetency='';this.showQuestionCoreCompetency=false;}
           if(type=="behaviour"){this.selectedNumberBehaviour='';this.questionBehaviour='';this.showQuestionBehaviour=false;}
           if(type=="integrity"){this.selectedNumberIntegrity='';this.questionIntegrity='';this.showQuestionIntegrity=false;}           
           this.getInterviewGuideQuestions();
        }
        else this.coreService.notify("Unsuccessful","d.message",0)
      },
      e=>this.coreService.notify("Unsuccessful",e.message,0))
  }
  
  // delete interview guide question
  onDelete(questionID,type){
    this.confirmService.confirm(
      this.confirmService.tilte,
      this.confirmService.message,
      this.viewContainerRef
    )
    .subscribe(res => {
      let result = res;
      if (result) {
    if(type=="attitude") this.attitudeLoader=true;
    if(type=="compentancy") this.coreCompetencyLoader=true;
    if(type=="behaviour") this.behaviourLoader=true;
    if(type=="integrity") this.integrityLoader=true;    
    let interviewGuideType=type;
    if(interviewGuideType=="coreCompetency") interviewGuideType="compentancy";
    if(interviewGuideType=="behaviour") interviewGuideType="behavior";
    
    let reqParams={     
      type:interviewGuideType,
      questionID:questionID
    }
     this.interviewGuideService.deleteInterviewGuideQuestion(JSON.stringify(reqParams)).subscribe(
      d=>{
        if(d.code=="200"){
          this.coreService.notify("Successful",d.message,1)
           this.getInterviewGuideQuestions();
        }
        else this.coreService.notify("Unsuccessful",d.message,0)
      },
      e=>{
        this.coreService.notify("Unsuccessful",e.message,0);
    })
    }})
  }
  
  // hides the edit panel
  onCancel(questionID,type){
    //this.ques
    if(type=="attitude"){this.selectedNumberAttitude='';this.questionAttitude='';this.showQuestionAttitude=false; this.attititudeQuestionEditBox=!this.attititudeQuestionEditBox;}
    if(type=="compentancy"){this.selectedNumberCoreCompetency='';this.questionCoreCompetency='';this.showQuestionCoreCompetency=false; this.coreCompetencyEditBox=!this.coreCompetencyEditBox;}    
    if(type=="behaviour"){this.selectedNumberBehaviour='';this.questionBehaviour='';this.showQuestionBehaviour=false;this.behaviourEditBox=!this.behaviourEditBox;}
    if(type=="integrity"){this.selectedNumberIntegrity='';this.questionIntegrity='';this.showQuestionIntegrity=false;this.integrityEditBox=!this.integrityEditBox;}   
  }

  // add interview guide question
  addQuestion(type) {
    let dialogRef = this.dialog.open(AddInterviewGuideQuestionDialog, { width: "400px" });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        let question={
          title:'',
          type:''
        };
        if(type=="attitude") this.attitudeLoader=true;
        if(type=="compentancy") this.coreCompetencyLoader=true;
        if(type=="behaviour") this.behaviourLoader=true;
        if(type=="integrity") this.integrityLoader=true; 
        if(type=="behaviour") type="behavior";
        question.title = result;
        question.type=type;
        this.interviewGuideService.addInterviewGuideQuestion(question).subscribe(
          data => {
            if (data.code == "200") {
              this.getInterviewGuideQuestions();
              this.coreService.notify("Successful", data.message, 1);
            }
          },
          error => {
            if(type=="attitude") this.attitudeLoader=false;
            if(type=="compentancy") this.coreCompetencyLoader=false;
            if(type=="behaviour") this.behaviourLoader=false;
            if(type=="integrity") this.integrityLoader=false; 
            this.coreService.notify("Unsuccessful", error.message, 0);
          },
          () => {}
        );
      }
    });
  }
}

@Component({
  selector: 'add-question-dialog',
  template: `<h1 md-dialog-title>Add question</h1>
      <div md-dialog-content style="padding-bottom: 20px;">
       <form #addQuestionForm="ngForm" name="material_signup_form" class="md-form-auth form-validation" >  
        <md-input-container class="full-width">
        <textarea mdInput required rows="4" name="question_title" [(ngModel)]="questionTitle" #title="ngModel" placeholder="Enter question"></textarea>
        </md-input-container> 
        <div *ngIf="title.errors && (title.dirty || title.touched)">
                  <div style="color:red" [hidden]="!title.errors.required ">
                   Question is required !
                  </div>
              </div>
        </form>       
      </div>
      <div md-dialog-actions>
          <button md-raised-button color="primary" [disabled]="!addQuestionForm.valid" (click)="onSubmit(addQuestionForm.valid);">Add</button>
          <button md-raised-button color="default" (click)="onCancel();">Cancel</button>            
          </div>`,
})
export class AddInterviewGuideQuestionDialog implements OnInit {
  
  questionTitle='';

  constructor(public dialogRef: MdDialogRef<AddInterviewGuideQuestionDialog>) {}
  
ngOnInit(){}

  onSubmit(isvalid){  
    if(this.questionTitle){  
    let trimmedMessgae=this.questionTitle.trim();
    let valid=(isvalid && (trimmedMessgae!=""))
    if(valid){         
      this.dialogRef.close(this.questionTitle);}
    }   
  }
  
  // close dialog box
  onCancel(){
     this.dialogRef.close(null);
  }

}

