import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InterviewGuideComponent } from './interview-guide.component';

describe('InterviewGuideComponent', () => {
  let component: InterviewGuideComponent;
  let fixture: ComponentFixture<InterviewGuideComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InterviewGuideComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InterviewGuideComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
