import { Routes, RouterModule }  from '@angular/router';
import { InterviewGuideComponent } from './interview-guide/interview-guide.component';
import { InterviewGuideGuardService } from './interview-guide-guard.service';

export const InterviewGuidePagesRoutes: Routes = [
    {
        path: '',
        children: [
            { path: '',  component: InterviewGuideComponent,canActivate:[InterviewGuideGuardService] }
        ]
    }
];

export const InterviewGuidePagesRoutingModule = RouterModule.forChild(InterviewGuidePagesRoutes);
