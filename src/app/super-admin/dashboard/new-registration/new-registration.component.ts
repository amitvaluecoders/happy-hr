import { Component, OnInit } from '@angular/core';
import { DashboardService } from '../dashboard.service';
import { CoreService } from '../../../shared/services/core.service';
import { Cookie } from 'ng2-cookies/ng2-cookies';
import { Router, ActivatedRoute, Params } from '@angular/router';


@Component({
  selector: 'app-new-registration',
  templateUrl: './new-registration.component.html',
  styleUrls: ['./new-registration.component.scss']
})
export class NewRegistrationComponent implements OnInit {

  newRegistration=[];
 isProcessing=false;
  constructor(private router: Router,
    private dashboardService: DashboardService,
    private coreService: CoreService) { }

  ngOnInit() {
    this.getCompanyNotLoginInLastThirtyDaysData();
  }

  getCompanyNotLoginInLastThirtyDaysData() {
    this.isProcessing=true;
    this.dashboardService.getCompanyList().subscribe(
      d => {
        if (d.code == "200") {
          this.isProcessing=false;
          this.newRegistration=d.data.newRegistration;
        }
        else {this.isProcessing=false;this.coreService.notify("Unsuccessful", "error while getting contract types", 0);}
      },
      e => {this.isProcessing=false;this.coreService.notify("Unsuccessful", e.message, 0)},
    )
  }

}
