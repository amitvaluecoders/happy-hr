import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from '@angular/material';
import { FormsModule} from '@angular/forms';
import { SharedModule } from '../../shared/shared.module';
import { DashboardService } from './dashboard.service';
import { DashboardComponent } from './dashboard.component';
import { DataTableModule} from "angular2-datatable";
import { DashboardRoutingModule } from './dashboard.routing';
import { ChartsModule } from 'ng2-charts/ng2-charts';
import { ActivityComponent } from './activity/activity.component';
import { NewRegistrationComponent } from './new-registration/new-registration.component';
import { PaymentfailureComponent } from './paymentfailure/paymentfailure.component';
import { CompanyLastLoginComponent } from './company-last-login/company-last-login.component';
import { SupportPackageComponent } from './support-package/support-package.component';

@NgModule({
  imports: [
    SharedModule,
    CommonModule,
    MaterialModule,
    FormsModule,
    DataTableModule,
    DashboardRoutingModule,
    ChartsModule
  ],
  declarations: [DashboardComponent, ActivityComponent, NewRegistrationComponent, PaymentfailureComponent, CompanyLastLoginComponent, SupportPackageComponent],
  providers:[DashboardService]
})
export class DashboardModule { }