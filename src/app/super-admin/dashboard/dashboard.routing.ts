import { Routes, RouterModule }  from '@angular/router';
import { DashboardComponent } from './dashboard.component';
import { ActivityComponent } from './activity/activity.component';
import { SupportPackageComponent } from './support-package/support-package.component';
import { PaymentfailureComponent } from './paymentfailure/paymentfailure.component';
import { NewRegistrationComponent } from './new-registration/new-registration.component';
import { CompanyLastLoginComponent } from './company-last-login/company-last-login.component';

export const DashboardRoutes: Routes = [
    {
        path: '',
        children: [    
          { path: '', component: DashboardComponent },
          { path :'activity',component:ActivityComponent},
          { path :'failedPayment',component:PaymentfailureComponent},
          { path :'supportPackage',component:SupportPackageComponent},
          { path :'shoutout',component:CompanyLastLoginComponent},
          { path :'registration',component:NewRegistrationComponent}
        ]
    }
];

export const DashboardRoutingModule = RouterModule.forChild(DashboardRoutes);
