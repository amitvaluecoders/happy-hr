import { Component, OnInit } from '@angular/core';
import { CHARTCONFIG } from '../../config';
import { DashboardService } from './dashboard.service';
import { CoreService } from '../../shared/services/core.service';
import { Cookie } from 'ng2-cookies/ng2-cookies';
import { Router, ActivatedRoute, Params } from '@angular/router';
import * as moment from 'moment';

@Component({
  selector: 'my-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent {

  isDataAvailable: boolean = false;

  ngOnInit() {
    this.getBarChartData();
  }

  constructor(private router: Router,
    private dashboardService: DashboardService,
    private coreService: CoreService) {
    if (!Cookie.get('happyhr-token')) {
      this.router.navigate(['/user/login']);
    }
  }

  logout() {
    this.router.navigate(['/user/login']);
  }

  public barChartOptions: any = {
    scaleShowVerticalLines: false,
    responsive: true,
    legend: { display: false },
    scales: {
      yAxes: [{
        ticks: {
          beginAtZero: true
        }
      }]
    }
  };
  public barChartLabels: string[] = ['2006', '2007', '2008', '2009', '2010', '2011', '2012'];
  public barChartType: string = 'bar';
  public barChartLegend: boolean = true;

  public barChartData: any[] = [
    { data: [28, 48, 40, 19, 86, 27, 90], label: 'Series B' }
  ];

  // events
  public chartClicked(e: any): void {
   // console.log(e);
  }
  
  public chartHovered(e: any): void {
   // console.log(e);
  }

  barData;
  registeredCompanies = [];
  registeredCompaniesCount = [{ data: [], label: 'Registered' }];
  barChartLabelsRegisteredCompnaies = [];
  totalRegisteredCompnaies = '';

  employees = [];
  employeesCount = [{ data: [], label: 'Employees' }];
  totalEmployees = '';
  barChartLabelsEmployees = [];

  companyRegistration = [];
  companyRegistrationCount = [{ data: [], label: 'Company registration' }]
  totalCompanyRegistration = '';
  barChartLabelsCompanyRegistration = [];

  registeredCompnaiesWithSubcompanies = [];
  registeredCompnaiesWithSubcompaniesCount = [{ data: [], label: 'Sub companies' }]
  totalRegisteredCompnaiesWithSubcompanies = '';
  barChartLabelsRegisteredCompnaiesWithSubcompanies = [];


  activities=[];
  failedPayment=[];
  newRegistration=[];
  notLoginInLastThirtyDays=[];
  packagePurchased=[];

 color = [{
      backgroundColor: '#64c3fb'
    }]
    color1 = [{
      backgroundColor: '#63fa91'
    }]
     color2 = [{
      backgroundColor: '#de63f9'
    }]
     color3 = [{
      backgroundColor: '#f96262'
    }]

 activitiesDayWise={i:[]};   
 activityDays=[]; 
//  formatDate(v){
//   return moment(v).isValid()? moment(v).format('DD MMM YYYY') : 'Not specified';
// }


  getBarChartData() {
    this.dashboardService.getDashboardBarChartData().subscribe(
      d => {
        if (d.code == "200") {
          this.barData = d;
          this.companyRegistration = JSON.parse(d.data.statistics["company-registration-this-week"]["sddData"]);
          this.totalCompanyRegistration = this.companyRegistration["total"];          
          for (let cr of this.companyRegistration["data"]) {
            this.companyRegistrationCount[0].data.push(cr.count); 
            //this.barChartLabelsCompanyRegistration.push(moment(cr.yearMonthDay).format('DD MMM YYYY'));
            this.barChartLabelsCompanyRegistration.push(this.coreService.months[new Date(cr.yearMonthDay).getMonth()]);
          }

          this.employees = JSON.parse(d.data.statistics["total-employee"]["sddData"]);
          this.totalEmployees = this.employees["total"];          
          for (let cr of this.employees["data"]) {
            this.employeesCount[0].data.push(cr.count);
            this.barChartLabelsEmployees.push(this.coreService.months[new Date(cr.yearMonth).getMonth()]);
          }

          this.registeredCompanies = JSON.parse(d.data.statistics["total-registered-company"]["sddData"]);
          this.totalRegisteredCompnaies = this.registeredCompanies["total"];
          for (let cr of this.registeredCompanies["data"]) {
            this.registeredCompaniesCount[0].data.push(cr.count);
            this.barChartLabelsRegisteredCompnaies.push(this.coreService.months[new Date(cr.yearMonth).getMonth()]);
          }

          this.registeredCompnaiesWithSubcompanies = JSON.parse(d.data.statistics["total-registered-company-with-subcompany"]["sddData"]);
          this.totalRegisteredCompnaiesWithSubcompanies = this.registeredCompnaiesWithSubcompanies["total"];          
          for (let cr of this.registeredCompnaiesWithSubcompanies["data"]) {
            this.registeredCompnaiesWithSubcompaniesCount[0].data.push(cr.count);
            this.barChartLabelsRegisteredCompnaiesWithSubcompanies.push(this.coreService.months[new Date(cr.yearMonth).getMonth()]);
          }

          this.activitiesDayWise = d.data.activity;
          // this.activityDays=Object.keys(this.activitiesDayWise);
          
          let date=[];
          for(let day of this.activityDays){
            let i=0;
          for(let activity of this.activitiesDayWise[day]){
            date=(new Date(activity["insertTime"]).toLocaleDateString().split('/'))
          activity[i]=date.reverse().join('-');
          i++; 
          }
          }
          this.failedPayment=d.data.failedPayment;
          this.newRegistration=d.data.newRegistration;
          this.notLoginInLastThirtyDays=d.data.notLoginLast30Days;
          this.packagePurchased=d.data.packagePurchased;       
          this.isDataAvailable = true;

        }
        else this.coreService.notify("Unsuccessful", "error while getting contract types", 0);
      },
      e => this.coreService.notify("Unsuccessful", e.message, 0),
    )
  }
}
