import { Component, OnInit } from '@angular/core';
import { DashboardService } from '../dashboard.service';
import { CoreService } from '../../../shared/services/core.service';
import { Router, ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-activity',
  templateUrl: './activity.component.html',
  styleUrls: ['./activity.component.scss']
})
export class ActivityComponent implements OnInit {

 
  isProcessing: boolean = false;
  activityStream=[];
    ngOnInit() {
      this.getActivityStreamData();
      //this.getBarChartData();
    }
  
    constructor(private router: Router,
      private dashboardService: DashboardService,
      private coreService: CoreService) {}

      activityDays=[];

    getActivityStreamData() {
    this.isProcessing=true;
    this.dashboardService.getActivityStreamData().subscribe(
      d => {
        if (d.code == "200") {
          
          this.activityStream = d.data;
          this.activityDays=Object.keys(this.activityStream);
          
          let date=[];
          for(let day of this.activityDays){
            let i=0;
          for(let activity of this.activityStream[day]){
            date=(new Date(activity["insertTime"]).toLocaleDateString().split('/'))
          activity[i]=date.reverse().join('-');
          i++; 
          this.isProcessing=false;
          }
          }
        }
        else {this.isProcessing=false; this.coreService.notify("Unsuccessful", "error while getting contract types", 0);}
      },
      e => {this.isProcessing=false;this.coreService.notify("Unsuccessful", e.message, 0)},
    )
  }

  onTime(time){return new Date(time);}

}
