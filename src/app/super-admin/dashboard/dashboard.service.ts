import { Injectable } from '@angular/core';
import { RestfulLaravelService } from '../../shared/services/restful-laravel.service'; 

@Injectable()
export class DashboardService {

  constructor (private restfulLaravelService:RestfulLaravelService 
){}

getDashboardBarChartData(){
  return this.restfulLaravelService.get('admin/get-dashboard-data');     
}

getCompanyList(){
  return this.restfulLaravelService.get('admin/get-new-company-data');     
}

getActivityStreamData(){
  return this.restfulLaravelService.get('admin/get-activity-stream');       
}
}
