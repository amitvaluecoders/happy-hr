import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from '@angular/material';
import { FormsModule } from '@angular/forms';
import { MyDatePickerModule } from 'mydatepicker';
import { DataTableModule} from "angular2-datatable";
import { ReferenceDisclaimerComponent } from './reference-disclaimer/reference-disclaimer.component';
import { ReferenceFormComponent,AddFieldsDialog } from './reference-form/reference-form.component';
import { EditReferenceFormComponent } from './edit-reference-form/edit-reference-form.component';
import { ReferenceCheckPagesRoutingModule } from './reference-check.routing';
import { RefrenceCheckService } from './reference-check.service';
import { ReferenceCheckGuardService } from './reference-check-guard.service';

@NgModule({
  imports: [
    CommonModule,
    MaterialModule,
    FormsModule,
    DataTableModule,
    MyDatePickerModule,
    ReferenceCheckPagesRoutingModule
  ],
  declarations: [ReferenceDisclaimerComponent,AddFieldsDialog, ReferenceFormComponent, EditReferenceFormComponent],
  entryComponents:[EditReferenceFormComponent,AddFieldsDialog],
  providers:[RefrenceCheckService,ReferenceCheckGuardService]
})
export class ReferenceCheckModule { }
