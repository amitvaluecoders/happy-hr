import { Injectable } from '@angular/core';
import { RestfulwebService } from '../../shared/services/restfulweb.service';
import { RestfulLaravelService } from '../../shared/services/restful-laravel.service'


@Injectable()
export class RefrenceCheckService {

  constructor(private restfulWebService: RestfulwebService,private restfulLaravelService: RestfulLaravelService){}

  getRefrenceDisclaimer(){
    return this.restfulLaravelService.get('admin/reference-disclaimer-get');     
  }

  editRefrenceDisclaimer(reqBody){
    return this.restfulLaravelService.put(`admin/reference-disclaimer-save`,reqBody);     
  }

  changeRefrenceForm(reqBody){
    return this.restfulLaravelService.post(`admin/create-reference-check`,reqBody);     
  }

  getReferenceFormDetails(){
    return this.restfulLaravelService.get('admin/reference-list');         
  }


  // getReferenceFormDetails(){
  //   return this.restfulWebService.get('refrenceFormDetails');         
  // }




}