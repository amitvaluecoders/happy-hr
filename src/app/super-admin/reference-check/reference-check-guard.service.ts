import { Injectable } from '@angular/core';
import {CanActivate,Router,RouterStateSnapshot,ActivatedRouteSnapshot} from "@angular/router";
import {CoreService} from '../../shared/services/core.service';
import {Observable} from 'rxjs/Rx'; 
import 'rxjs/add/operator/map'

@Injectable()
export class ReferenceCheckGuardService {

  public module = "referenceDocument";
  
        constructor(private _core: CoreService, private router: Router){ }
  
        canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {   
            let url: string = state.url; 
            if(this.checkHavePermission(url)){
              return true;
            }else{
              this.router.navigate(['/extra/unauthorized']);
              return false;
            }
        }
  
        private checkHavePermission(path) {
            switch (path) {
                case "":
                    return this._core.getModulePermission(this.module, 'view');
                case 'form':
                    return this._core.getModulePermission(this.module, 'view');
                case 'form/edit':
                    return this._core.getModulePermission(this.module, 'edit');
                default:
                    return false;
            }
        }

}
