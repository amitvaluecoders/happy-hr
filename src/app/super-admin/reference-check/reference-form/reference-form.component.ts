import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { RefrenceCheckService } from '../reference-check.service';
import { CoreService } from '../../../shared/services/core.service';
import { MdDialog, MdDialogRef } from '@angular/material';
import { ConfirmService } from '../../../shared/services/confirm.service';

@Component({
  selector: 'app-reference-form',
  templateUrl: './reference-form.component.html',
  styleUrls: ['./reference-form.component.scss']
})
export class ReferenceFormComponent implements OnInit {
  public myDatePickerOptions={};

  public loader=false;
  public generalInformationEditBox=false;
  public referenceDetailsEditBox=false;
  public employmentDetailsEditBox=false;
  public applicantDiscriptionEditBox=false;
  public applicantkeyEditBox=false;
  public applicantAbilityEditBox=false;
  public keyCriteriaEditBox=false;
  public roleDiscriptionEditBox=false;
  public reEmployEditBox=false;
  public reEmployRejectionDiscriptionEditBox=false;

  public template={};
  public generalInformation={};
  public referenceDetails={};
  public employmentDetails={};
  public applicantDiscription={};
  public applicantkey={};
  public applicantAbility={};
  public keyCriteria={};
  public roleDiscription={};
  public reEmploy={};
  public reEmployRejectionDiscription={};
  public oldObject={};
  public oldHeading='';
  public addFormFields=[];
  public isProcessing=false;
  public temp={
    generalInformation:{
      heading:"General Information",
      fields:[
        {label:"Employee Conducting Check"},
        {label:"Date"},
        {label:"Candidate Name"},
        {label:"Salary Expectation - As Per Candidate profile"},
        {label:"Client"},
        {label:"Role"}
      ]
    },
    referenceDetails:{
      heading:"Reference details",
      fields:[{label:"Company name"}]
    },
     employmentDetails:{
      heading:"Employement details",
      fields:[{label:"Job title when working with referee"}]
    },
    applicantDiscription:{
      heading:"Please describe the application in relation to their",
      fields:[{label:"Reliabilit"}]
    },
    applicantkey:{
      heading:"What are apllicant's key",
      fields:[{label:"Strengths"}]
    },
    applicantAbility:{
      heading:"What are the applicant's ability to",
      fields:[{label:"Demonstrate company values"}]
    },
    keyCriteria:{
      heading:"Key criteria",
      fields:[{label:"1"}]
    },
    roleDiscription:{
      heading:"Describe the role to the reference abd ask how the candidate perform in this role and Why "
    },
    reEmploy:{
      heading:"Would you re-employee this person ?"
    },
    reEmployRejectionDiscription:{
      heading:"if no, why ?"
    }

  }

  constructor(public route: ActivatedRoute,
  private confirmService:ConfirmService,
  private viewContainerRef:ViewContainerRef,
  public dialog: MdDialog,
                public refrenceCheckService:RefrenceCheckService,
                public coreService:CoreService) { }

  ngOnInit() {
    //this.template=JSON.parse(JSON.stringify(this.temp));
    this.getReferenceFormDetails();
  }

  getReferenceFormDetails(){
    this.isProcessing=true;
    this.refrenceCheckService.getReferenceFormDetails().subscribe(
      d=>{
        if(d.code=="200"){
          console.log("Form DATA::",d.data)
          this.template=d.data;
          for(let i in this.template){
            if(this.template[i].referenceID=='1'){this.generalInformation=this.template[i]}
            if(this.template[i].referenceID=='2'){this.referenceDetails=this.template[i]}
            if(this.template[i].referenceID=='3'){this.employmentDetails=this.template[i]}
            if(this.template[i].referenceID=='4'){this.applicantDiscription=this.template[i]}
            if(this.template[i].referenceID=='5'){this.applicantkey=this.template[i]}
            if(this.template[i].referenceID=='6'){this.applicantAbility=this.template[i]}
            if(this.template[i].referenceID=='7'){this.keyCriteria=this.template[i]}
            if(this.template[i].referenceID=='8'){this.roleDiscription=this.template[i]}
            if(this.template[i].referenceID=='9'){this.reEmploy=this.template[i]}
            if(this.template[i].referenceID=='10'){this.reEmployRejectionDiscription=this.template[i]}
            
          }
          console.log("General",this.generalInformation);
          console.log("Reference",this.referenceDetails);
          this.isProcessing=false;
          
        }
        else {this.isProcessing=false;this.coreService.notify("Unsuccessful",d.message,0);}
      },
      e=>{this.isProcessing=false;this.coreService.notify("Unsuccessful",e.message,0)})
  }

inputType='';
  addFields(obj){
//     let dialogRef = this.dialog.open(AddFieldsDialog, { width: "400px" });
//    // dialogRef.componentInstance.category = this.chars;
//     dialogRef.afterClosed().subscribe(result => {
//       if (result) {
// if(result=='textbox') {this.inputType='textbox'; this.addFormFields.push({referenceKeyID:"",title:""})}
// if(result=='checkbox') {this.inputType='checkbox'; this.addFormFields.push({referenceKeyID:"",title:""})}
// if(result=='radio') {this.inputType='radio'; this.addFormFields.push({referenceKeyID:"",title:""})}        
//       }
        
//     });
    console.log("object",obj);
    this.addFormFields.push({referenceKeyID:"",title:""})
  }

  onCancel(obj,data){
    console.log("Array data",data)
    this.addFormFields=data.data;
    data.heading=this.oldHeading;
    //this.template=JSON.parse(JSON.stringify(this.temp));
    // for(let i in this.template){
    //         if(data.referenceID=='1'){this.generalInformation=this.template[i];this[obj]=false;return}
    //         if(data.referenceID=='3'){this.referenceDetails=this.template[i]}
    //         if(data.referenceID=='4'){this.employmentDetails=this.template[i]}
    //         if(data.referenceID=='5'){this.applicantDiscription=this.template[i]}
    //         if(data.referenceID=='6'){this.applicantkey=this.template[i]}
    //         if(data.referenceID=='7'){this.applicantAbility=this.template[i]}
    //         if(data.referenceID=='8'){this.keyCriteria=this.template[i]}
    //         if(data.referenceID=='9'){this.roleDiscription=this.template[i]}
    //         if(data.referenceID=='10'){this.reEmploy=this.template[i]}
    //         if(data.referenceID=='11'){this.reEmployRejectionDiscription=this.template[i]}            
    //       }  
    this[obj]=false;
  }

  onSave(isvalid,obj,data){
    if(isvalid){
      this.loader=true;
      console.log("send data",data)
     // console.log("template",JSON.stringify(this.template))
      // let reqParams={
      //   //refer
      //   data:[{referenceID: "",title: "Reference data" },{referenceID: "",title: "reference data 2"}]
      // }
      data.data=this.addFormFields;
      this.refrenceCheckService.changeRefrenceForm(data).subscribe(
        d=>{
          if(d.code=="200"){
            //this.getReferenceFormDetails();
            this.coreService.notify("Successful",d.message,1);
            this.loader=false;
            this[obj]=false;
          }
          else this.coreService.notify("Unsuccessful",d.message,0)
        },
        e=>this.coreService.notify("Unsuccessful",e.message,0)
        )
    }
  }

  onEditClick(obj,data){
    this[obj]=true;this.addFormFields=JSON.parse(JSON.stringify(data['data']));
  }


deleteField(index){
    this.confirmService.confirm(this.confirmService.tilte,this.confirmService.message,this.viewContainerRef)
    .subscribe(res=>{
       let result=res;
    if(result){
    this.addFormFields.splice(index,1);
    }})
    // this.contract.subject=this.contract.subject;
  }

}

@Component({
    selector: 'add-edit-award-dialog',
    template: `<h1 md-dialog-title>Choose input type</h1>
        <div md-dialog-content style="padding-bottom: 20px;">
          <md-select [(ngModel)]="inputType" #category="ngModel" name="category" placeholder="Select input type" required>
                                <md-option selected value="textbox">Textbox</md-option>
                                <md-option value="checkbox">Checkbox</md-option>
                                <md-option value="radio">Radio</md-option>                                
                            </md-select>     
        </div>
        <div md-dialog-actions>
            <button md-raised-button color="primary" (click)="onSubmit()">Add</button>
            <button md-raised-button color="default" (click)="onCancel()">Cancel</button>            
            </div>`,
})
export class AddFieldsDialog implements OnInit {
  
  public inputType;
    constructor(public dialogRef: MdDialogRef<AddFieldsDialog>) {}
    
  ngOnInit(){
    }

    onSubmit(){         
        this.dialogRef.close(this.inputType);
      }
    
    onCancel(){
       this.dialogRef.close(null);
    }
}

