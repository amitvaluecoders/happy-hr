import { Routes, RouterModule }  from '@angular/router';
import { ReferenceDisclaimerComponent } from './reference-disclaimer/reference-disclaimer.component';
import { ReferenceFormComponent } from './reference-form/reference-form.component';
import { EditReferenceFormComponent } from './edit-reference-form/edit-reference-form.component';
import { ReferenceCheckGuardService } from './reference-check-guard.service';

export const ReferenceCheckPagesRoutes: Routes = [
    {
        path: '',
        children: [
            // { path: '', redirectTo: '/user/login', pathMatch: 'full' },    
            { path: '',  component: ReferenceDisclaimerComponent,canActivate:[ReferenceCheckGuardService] },
            { path: 'form', component: ReferenceFormComponent,canActivate:[ReferenceCheckGuardService] },
            { path: 'form/edit', component: EditReferenceFormComponent,canActivate:[ReferenceCheckGuardService] }
        ]
    }
];

export const ReferenceCheckPagesRoutingModule = RouterModule.forChild(ReferenceCheckPagesRoutes);
