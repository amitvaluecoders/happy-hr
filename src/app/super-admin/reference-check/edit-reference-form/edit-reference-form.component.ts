import { Component, OnInit } from '@angular/core';
import { MdSnackBar, MdDialog, MdDialogRef } from '@angular/material';
import { RefrenceCheckService } from '../reference-check.service';
import { CoreService } from '../../../shared/services/core.service';


@Component({
  selector: 'app-edit-reference-form',
  templateUrl: './edit-reference-form.component.html',
  styleUrls: ['./edit-reference-form.component.scss']
})
export class EditReferenceFormComponent implements OnInit {

  public refrenceDisclaimer="";
  isProcessing=false;
  accept;
  reject=false;
  constructor(public dialogRef: MdDialogRef<EditReferenceFormComponent>,private coreService:CoreService, private refrenceCheckService:RefrenceCheckService) { }

  ngOnInit() {
    if(!this.accept)
      this.reject=true;
    //else this.reject=true;
  }

  onSave(isvalid){
    if(isvalid){
      this.isProcessing=true;
      let reqBody={
        disclaimer:this.refrenceDisclaimer,
      accept:this.accept      }
      this.refrenceCheckService.editRefrenceDisclaimer(reqBody).subscribe(
        d=>{
        if(d.code=="200"){
        this.dialogRef.close(reqBody);
        this.isProcessing=false;
            this.coreService.notify("Successful",d.message,1)
          }
          else {this.isProcessing=false;this.dialogRef.close(reqBody);this.coreService.notify("Unsuccessful",d.message,0)} 
        },
        e=>{this.isProcessing=false;this.dialogRef.close(reqBody);this.coreService.notify("Unsuccessful",e.message,0)}
        )   
    }
  }

  onCancel(){
    this.dialogRef.close(null);
  }

  acceptDisclaimer(){
    console.log("DDDDD");
    console.log("Accept",this.accept);
    console.log("Reject",this.reject);
    
    if(this.accept==true){
      this.reject=false;
      console.log("AAA")
    }
    if(this.accept==false ){
      this.reject=true;
      console.log("AAA")
    }
   // (this.accept) ?this.reject=false:(this.reject)?this.accept=false:this.accept=false,this.reject=false;
  }

  rejectDisclaimer(){
    console.log("DDDDD");
    console.log("Accept",this.accept);
    console.log("Reject",this.reject);
    if(this.reject==true){
       this.accept=false;
     console.log("RRR");
    }
    if(this.reject==false){
      this.accept=true;
    console.log("RRR");
     }
   // (this.accept) ?this.reject=false:(this.reject)?this.accept=false:this.accept=false,this.reject=false;
  }

}
