import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditReferenceFormComponent } from './edit-reference-form.component';

describe('EditReferenceFormComponent', () => {
  let component: EditReferenceFormComponent;
  let fixture: ComponentFixture<EditReferenceFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditReferenceFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditReferenceFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
