import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReferenceDisclaimerComponent } from './reference-disclaimer.component';

describe('ReferenceDisclaimerComponent', () => {
  let component: ReferenceDisclaimerComponent;
  let fixture: ComponentFixture<ReferenceDisclaimerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReferenceDisclaimerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReferenceDisclaimerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
