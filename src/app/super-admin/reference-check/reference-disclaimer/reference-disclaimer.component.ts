import { Component, OnInit } from '@angular/core';
import { MdSnackBar, MdDialog, MdDialogRef } from '@angular/material';
import { EditReferenceFormComponent } from '../edit-reference-form/edit-reference-form.component';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { RefrenceCheckService } from '../reference-check.service';
import { CoreService } from '../../../shared/services/core.service';


@Component({
  selector: 'app-reference-disclaimer',
  templateUrl: './reference-disclaimer.component.html',
  styleUrls: ['./reference-disclaimer.component.scss']
})
export class ReferenceDisclaimerComponent implements OnInit {
  referenceDisclaimer="";
  accept=true;
  reject=false;
  isProcessing=false;

  constructor(public snackBar: MdSnackBar, 
                public dialog: MdDialog,
                public route: ActivatedRoute,
                public refrenceCheckService:RefrenceCheckService,
                public coreService:CoreService) { }

  ngOnInit() {
    this.getReferenceDisclaimer();
  }

  getReferenceDisclaimer(){
    this.isProcessing=true;
    this.refrenceCheckService.getRefrenceDisclaimer().subscribe(
              d=>{
                if(d.code=="200"){
                  console.log("data",d)
                  this.referenceDisclaimer=d.data.disclaimer;
                  this.accept=d.data.accept;
                  if(this.accept)
                    this.reject=false;
                  if(!this.accept)
                    this.reject=true;
                  // else this.reject=true;
                  this.isProcessing=false;
                }
                else {
                  this.isProcessing=false;
                  this.coreService.notify("Unsuccessful",d.message,1)} 
              },
              e=>{
                this.isProcessing=false;
                this.coreService.notify("Unsuccessful",e.message,0);}
              )          
  }


 onEdit(){
   let dialogRef = this.dialog.open(EditReferenceFormComponent, {
          width: '600px'
      });
      dialogRef.componentInstance.refrenceDisclaimer=this.referenceDisclaimer;
      dialogRef.componentInstance.accept=this.accept;      
      dialogRef.afterClosed().subscribe(result => {
          if (result) {
            this.referenceDisclaimer=result.disclaimer;   
            this.accept=result.accept;
            if(!this.accept)
             this.reject=true;
            if(this.accept)
              this.reject=false;
          }
      });
 }



}
