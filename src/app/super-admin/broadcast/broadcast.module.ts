import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from '@angular/material';
import { DataTableModule} from "angular2-datatable";
import { FormsModule } from '@angular/forms';
import { BroadcastComponent } from './broadcast/broadcast.component';
import { MessageListComponent, CreateNewMessageDailog } from './message-list/message-list.component';
import { BroadcastService } from './broadcast.service';
import { BroadcastPagesRoutingModule } from './broadcast.routing';
import { BroadcastGuardService } from './broadcast-guard.service';
import { SharedModule } from '../../shared/shared.module';

@NgModule({
  imports: [
    SharedModule,
    CommonModule,
    MaterialModule,
    DataTableModule,
    FormsModule,
    BroadcastPagesRoutingModule
  ],
  entryComponents: [
        CreateNewMessageDailog
    ],
  declarations: [BroadcastComponent, MessageListComponent, CreateNewMessageDailog],
  providers:[BroadcastService, BroadcastGuardService]
})
export class BroadcastModule { }
