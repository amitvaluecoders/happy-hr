import { Component, OnInit } from '@angular/core';
import { MdSnackBar, MdDialog, MdDialogRef } from '@angular/material';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { BroadcastService } from '../broadcast.service';
import { CoreService } from '../../../shared/services/core.service';

@Component({
    selector: 'app-message-list',
    templateUrl: './message-list.component.html',
    styleUrls: ['./message-list.component.scss']
})
export class MessageListComponent implements OnInit {

    type = '';
    title = '';
    isProcessing = false;
    messageProcessing = false;
    constructor(public snackBar: MdSnackBar,
        public dialog: MdDialog,
        private activatedRoute: ActivatedRoute,
        private broadcastService: BroadcastService,
        private coreService: CoreService) { }
    public messages = [];

    ngOnInit() {
        this.type = this.activatedRoute.snapshot.params['type'];
        if (this.type == 'all') this.title = "all";
        else if (this.type == 'company-admin') this.title = "all company admins";
        else if (this.type == 'company-manager') this.title = "all company managers";
        else if(this.type=='company-employee'){
            this.type='company-employee-including-manager'
            this.title = "all company employees"
        }
        else this.title = "all company employees";  

        this.getMessages();
    }

    onTime(time){return new Date(time);}

   //get the message list
    getMessages() {
        this.isProcessing = true;
        let reqBody = {
            group: this.type
        };
        this.broadcastService.getMessages(JSON.stringify(reqBody)).subscribe(
            data => {
                if (data.code == "200") {
                    this.messages = data.data;
                    this.isProcessing = false;
                }
            },
            error => {
                this.isProcessing = false;
                this.coreService.notify('Unsuccessful', error.message, 0)
            }
        );
    }

    //opens dialog box to create message 
    createNewMessage() {
        let dialogRef = this.dialog.open(CreateNewMessageDailog, {
            width: '400px'
        });
        dialogRef.componentInstance.type = this.type;
        dialogRef.componentInstance.title = this.title;
        dialogRef.componentInstance.messages = this.messages;
        dialogRef.afterClosed().subscribe(result => {
            if (result) {
                if (result.value == "success") {
                    this.coreService.notify('Successful', result.message, 1)
                }
                else if (result.value == "failure") this.coreService.notify('Unsuccessful', result.message, 0)
                else { }
            }
        });
    }

}

@Component({
    selector: 'create-new-message-dialog',
    template: `<h1 md-dialog-title>Message to: {{title}}</h1>
        <div md-dialog-content style="padding-bottom: 20px;">
         <form #messageForm="ngForm" name="material_signup_form" class="md-form-auth form-validation" >
          
          <md-input-container class="full-width">
            <textarea required rows="4" name="userMessage" [(ngModel)]="userMessage" 
                      #message="ngModel" myNoSpaces
                      mdInput placeholder="Enter your message here"></textarea>
          </md-input-container > 
          <div *ngIf="message.errors && (message.dirty || message.touched)">
                    <div style="color:red" [hidden]="!message.errors.required || !message.errors.whitespace">
                    Message is required !
                    </div>
                </div>
          </form>            
        </div>
        <md-progress-spinner *ngIf="sendingMessage" mode="indeterminate" color=""></md-progress-spinner>    
        <div md-dialog-actions *ngIf="!sendingMessage">
            <button md-raised-button color="primary" [disabled]="messageForm.form.invalid" (click)="onSubmit(messageForm.form.valid)">Broadcast message</button>
            <button md-button (click)="dialogRef.close(false,false)">Cancel</button>
        </div>`,
})

export class CreateNewMessageDailog {
    public userMessage = '';
    type;
    messages;
    title;
    sendingMessage = false;
    public whiteSpaceError = false;

    constructor(
        public dialogRef: MdDialogRef<CreateNewMessageDailog>,
        private broadcastService: BroadcastService,
        private coreService: CoreService) { }

    //broadcast message
    onSubmit(isvalid) {
        this.sendingMessage = true;
        let trimmedMessgae = this.userMessage.trim();
        let valid = (isvalid && (trimmedMessgae != ""))
        if (valid) {
            let reqBody = {
                group: this.type,
                message: this.userMessage
            };
            this.broadcastService.addMessage(reqBody).subscribe(
                data => {
                    if (data.code == "200") {
                        this.messages.unshift(data.data);
                        this.sendingMessage = false;
                        this.dialogRef.close({ value: "success", message: data.message });
                    }
                },
                error => {
                    this.sendingMessage = false;
                    this.dialogRef.close({ value: "failure", message: error.message });
                }
            );
        }
    }
}

