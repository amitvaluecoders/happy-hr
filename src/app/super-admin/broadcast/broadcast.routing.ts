
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule }  from '@angular/router';
import { BroadcastComponent } from './broadcast/broadcast.component';
import { MessageListComponent } from './message-list/message-list.component';
import { BroadcastGuardService }  from './broadcast-guard.service';
export const BroadcastPagesRoutes: Routes = [
    {
        path: '',
        children: [
            { path: '',  component: BroadcastComponent,canActivate:[BroadcastGuardService]},
            { path: 'message/:type', component: MessageListComponent,canActivate:[BroadcastGuardService]},
        ]
    }
];

export const BroadcastPagesRoutingModule = RouterModule.forChild(BroadcastPagesRoutes);
