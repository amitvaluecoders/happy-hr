import { Injectable } from '@angular/core';
import { RestfulLaravelService} from '../../shared/services/restful-laravel.service';
 
@Injectable()
export class BroadcastService {

  constructor(public restfulWebService:RestfulLaravelService) { }
  
    getMessages(reqBody):any{
     return this.restfulWebService.get(`admin/broadcast-message-list/${reqBody}`);
    }
  
    getList():any{
      return this.restfulWebService.get(`admin/broadcast-message-group`);
    }
  
    addMessage(message):any{
      return this.restfulWebService.post('admin/broadcast-message-add',message);
    }

}
