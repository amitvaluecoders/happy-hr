import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { BroadcastService } from '../broadcast.service';
import { CoreService } from '../../../shared/services/core.service';

@Component({
  selector: 'app-broadcast',
  templateUrl: './broadcast.component.html',
  styleUrls: ['./broadcast.component.scss']
})
export class BroadcastComponent implements OnInit {

  constructor(
    private activatedRoute: ActivatedRoute,
    private broadcastService: BroadcastService,
    private coreService: CoreService,
    private router: Router) { }

  types = [];
  isProcessing = false;
  ngOnInit() {
    this.getList();
  }
  
  //gets the list of users to which message can be broadcasted 
  getList() {
    this.isProcessing = true;
    this.broadcastService.getList().subscribe(
      data => {
        if (data.code == "200") {
          this.types = data.data;
          this.isProcessing = false;
        }
      },
      error => {
        this.isProcessing = false;
        this.coreService.notify('Unsuccessful', error.message, 0)
      },
      () => console.log('a')
    );
  }

  //routes to message list that is clicked
  onClick(title) {
    if (title == "All(everyone using the system)") this.router.navigate(['/app/super-admin/broadcast/message', 'all'])
    else if (title == "All company admins") this.router.navigate(['/app/super-admin/broadcast/message', 'company-admin'])
    else if (title == "All company managers") this.router.navigate(['/app/super-admin/broadcast/message', 'company-manager'])
    else this.router.navigate(['/app/super-admin/broadcast/message', 'company-employee'])

  }
}
