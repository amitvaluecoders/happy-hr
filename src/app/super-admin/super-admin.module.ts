import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SuperAdminPagesRoutingModule } from './super-admin-routing.module';
// import { DashboardComponent } from './dashboard/dashboard.component';
import { CompanyService } from './services/company.service';
import { RestfulLaravelService } from '../shared/services/restful-laravel.service';
import { RestfulwebService } from '../shared/services/restfulweb.service';
import {  SuperAdminGuardService  } from './super_admin-guard.service';
import { AwardsGuardService } from './awards/awards-guard.service';
// modules

@NgModule({
  imports: [
    SuperAdminPagesRoutingModule,
    CommonModule
  ],
  providers:[CompanyService,RestfulLaravelService,RestfulwebService,SuperAdminGuardService,AwardsGuardService],
  declarations: []
  
})
export class SuperAdminModule { }
