import { Injectable } from '@angular/core';
import { RestfulLaravelService} from '../../shared/services/restful-laravel.service';
import { RestfulwebService } from '../../shared/services/restfulweb.service';

@Injectable()
export class PositionDescriptionService {

  constructor(public restfulWebService:RestfulLaravelService,public restfulService:RestfulwebService) { }

  getPositionDescriptionsList(pd):any{
   return this.restfulWebService.get(`admin/list-position-description-template/${encodeURIComponent(pd)}`)
  }

  deletePositionDescription(pd):any{
    return this.restfulWebService.delete(`admin/delete-position-description-template/${pd}`);
  }

  addEditPositionDescription(pd):any{
    return this.restfulWebService.post('admin/add-edit-position-description-template',pd);
  }

  getPositionDescription(pd):any{
   return this.restfulWebService.get(`admin/get-position-description-detail/${pd}`);
  }

  getIndustries(industry){
   return this.restfulWebService.get(`admin/list-industry/${encodeURIComponent(industry)}`);
  }

  deleteIndustry(industry){
    return this.restfulWebService.delete(`admin/delete-industry/${industry}`);
  }

  addEditIndustry(industry){
     return this.restfulWebService.post('admin/add-edit-industry',industry);
  }

  getAllIndustries(){
     return this.restfulWebService.get('admin/list-industry');
  }

  getIndustriesForAddEdit(){
     return this.restfulWebService.get('admin/list-industry');
  }

  updateStatus(reqBody){
    return this.restfulWebService.put('admin/update-position-status',reqBody);    
  }
}
