import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { PositionDescriptionService } from '../position-description.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { CoreService } from '../../../shared/services/core.service';
import { ConfirmService } from '../../../shared/services/confirm.service';

@Component({
  selector: 'app-pd-template',
  templateUrl: './pd-template.component.html',
  styleUrls: ['./pd-template.component.scss']
})
export class PdTemplateComponent implements OnInit {

  public positionDescription={
  positionID:'',
  title:"",
  summary:"",
  performanceIndicator:[{
  positionPerformanceIndicatorID:'',
  title:'',
  duration:'',
  measure:''}],
  roleAndResponsibility:"",
  pastExperience:"",
  education:"",
  industryIDs:[],
};

public industries=[{industryID:'',industryTitle:'',isChecked:false}];
public industriesType=[];
public industriesIds=[];
public isProcessing=true;
public isEditDataAvailable=true;
public isButtonClicked=false;
public industrySelected=[];

  positionDescriptions=[];
  id;
  buttonText="Add";
  heading="Create position description template";
  

  constructor(public positionDescriptionService:PositionDescriptionService,private router:Router,private activatedRoute:ActivatedRoute,public coreService:CoreService, private confirmService:ConfirmService, private viewContainerRef:ViewContainerRef) { }

  ngOnInit() {
    this.getIndustries();
    if(this.activatedRoute.snapshot.params['id']!= undefined){
      this.id=this.activatedRoute.snapshot.params['id'];
      this.buttonText="Save";
      this.heading="Edit position description template";
    }    
  }
  
  // get industries
  getIndustries(){
  this.positionDescriptionService.getIndustriesForAddEdit().subscribe(
    data=>{
       if (data.code == "200") {
         let industryTitles={}; 
         this.industries=data.data;
         if(this.activatedRoute.snapshot.params['id']!= undefined){
      this.getPostionDescriptionForUpdate();
    }
          for(let d of data.data){           
            this.positionDescription.industryIDs[d.industryID]=false; 
          }
          this.isProcessing=false;
         if(!this.id) this.isEditDataAvailable=false;
       }
    },
    error=>{this.isProcessing=false;this.coreService.notify("Unsuccessful","Error while getting the data",0);},
    ()=>{}
    )
  }
  
  // add/edit position description
  addEditPositionDescription(){
    this.isButtonClicked=true;
    for(let industry of this.industries){
      if(industry.isChecked==true) this.industrySelected.push({"industryID":industry.industryID}); 
    }
    if(this.industrySelected.length){
     this.positionDescription.industryIDs=this.industrySelected;
    this.positionDescriptionService.addEditPositionDescription(this.positionDescription).subscribe(
      data=>{
        this.isButtonClicked=false;
        this.coreService.notify("Successful",data.message,1),
        this.router.navigate(['/app/super-admin/position-description/pd-list']);        
      },
      error=>{ this.isButtonClicked=false;
        this.coreService.notify("Unsuccessful",error.message,0);},
      ()=>{}
    )
  }else{
    this.isButtonClicked=false;
        this.coreService.notify("Message","Please select atleast one industry",0);
  }
  }

  // get position description details
  getPostionDescriptionForUpdate() {
    this.isEditDataAvailable=true;
    this.positionDescriptionService.getPositionDescription(this.id).subscribe(
      data=>{
            if (data.code == "200") {
            this.positionDescription=data.data;
                let flag;
                for (let i of this.industries){    
                for(let p of data.data.positionindustry){
                if(p.industryID==i.industryID) {
                flag=true; 
                  break;
                  }else{
                    flag=false;
                  }   
              } 
              if(flag){
                    i.isChecked=true;
                  }else i.isChecked=false;
          }
          this.isEditDataAvailable=false;
                }
                
                else {this.isEditDataAvailable=false;this.coreService.notify("Unsuccessful","Error while getting position description details",0);}
      },
      error=>{this.isEditDataAvailable=false;this.coreService.notify("Unsuccessful",error.message,0);},
      ()=>{}
    )
  }

  // add performance indicatior 
  addPerformanceField(){
   this.positionDescription.performanceIndicator.push({
   positionPerformanceIndicatorID:'',
   title:'',
   duration:'',
   measure:''});
  }

  // delete performance indicator
  deletePerformanceField(index){
    this.confirmService.confirm(this.confirmService.tilte,this.confirmService.message,this.viewContainerRef)
    .subscribe(res=>{
       let result=res;
    if(result){
   this.positionDescription.performanceIndicator.splice(index,1);
    }})
  }

  // navigates to position description list page
  onCancel(){
    this.router.navigate(['/app/super-admin/position-description/pd-list']);
  }
}
