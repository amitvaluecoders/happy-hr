import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DataTableModule} from "angular2-datatable";
import { MaterialModule } from '@angular/material';
import { FormsModule } from '@angular/forms';
import { PositionListComponent } from './position-list/position-list.component';
import { PositionDescriptionPagesRoutingModule } from './position-description-routing.module';
import { PdTemplateComponent } from './pd-template/pd-template.component';
import { PositionDescriptionService } from './position-description.service';
import { IndustryManagementComponent,AddEditIndusrtyDialog } from './industry-management/industry-management.component';
// import { SearchPipe } from '../../shared/pipes/search.pipe';
// import { NoWhitespaceDirective } from '../../shared/directives/no-whitespace.directive';
import { SharedModule } from '../../shared/shared.module';
import { RestfulLaravelService } from '../../shared/services/restful-laravel.service';
import { PositionDescriptionDetailsComponent } from './position-description-details/position-description-details.component';
import { QuillModule } from 'ngx-quill';
import { PositionDescriptionGuardService } from './position-description-guard.service';
import { IndustryGuardService } from './industry-guard.service';
@NgModule({
  imports: [
    SharedModule,
    CommonModule,
    FormsModule,
    MaterialModule,
    DataTableModule,
    PositionDescriptionPagesRoutingModule,
    QuillModule
  ],
  entryComponents:[AddEditIndusrtyDialog],
  declarations: [PositionListComponent, PdTemplateComponent, IndustryManagementComponent,AddEditIndusrtyDialog, PositionDescriptionDetailsComponent],
  providers:[PositionDescriptionService,RestfulLaravelService,PositionDescriptionGuardService, IndustryGuardService]
})
export class PositionDescriptionModule { }
