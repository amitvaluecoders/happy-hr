import { Injectable } from '@angular/core';
import {CanActivate,Router,RouterStateSnapshot,ActivatedRouteSnapshot} from "@angular/router";
import {CoreService} from '../../shared/services/core.service';
import {Observable} from 'rxjs/Rx'; 
import 'rxjs/add/operator/map'

@Injectable()
export class PositionDescriptionGuardService {

 public module = "positionDescription";
  
        constructor(private _core: CoreService, private router: Router){ }
  
        canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {  
            let url: string;
            if(route['_routeConfig']['path'])
            url= route['_routeConfig']['path']
            else url = state.url;
            if(this.checkHavePermission(url)){
              return true;
            }else{
              this.router.navigate(['/extra/unauthorized']);
              return false;
            }
        }
  
        private checkHavePermission(path) {
            switch (path) {
                case "add-pd-template":
                    return this._core.getModulePermission(this.module, 'add');
                case 'pd-list':
                    return this._core.getModulePermission(this.module, 'view');
                case 'edit-pd-template/:id':
                    return this._core.getModulePermission(this.module, 'edit');
                case 'details/:id':
                    return this._core.getModulePermission(this.module, 'view');
                default:
                    return false;
            }
        }

}
