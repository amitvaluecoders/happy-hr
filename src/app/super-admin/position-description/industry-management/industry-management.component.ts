import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { MdSnackBar, MdDialog, MdDialogRef } from '@angular/material';
import { PositionDescriptionService } from '../position-description.service';
import { CoreService } from '../../../shared/services/core.service';
import { ConfirmService } from '../../../shared/services/confirm.service';

@Component({
  selector: 'app-industry-management',
  templateUrl: './industry-management.component.html',
  styleUrls: ['./industry-management.component.scss']
})
export class IndustryManagementComponent implements OnInit {

  public industries=[]
  public industry={
    industryTitle:""
  }
  public isProcessing=true;
  public filterProcessing=true;
  public searchText='';
  public cancelRequest;
  permission;
  constructor( public positionDescriptionService:PositionDescriptionService,public dialog: MdDialog,public confirmService:ConfirmService,public coreService:CoreService,public viewContainerRef:ViewContainerRef) { }

  ngOnInit() {
    this.isProcessing=true;
    this.getIndustries();
    this.permission=this.coreService.getNonRoutePermission();
  }

  // get industries list
  getIndustries(){
    this.filterProcessing=true;
    let searchKey='';
    if(this.searchText) searchKey=encodeURIComponent(this.searchText);
    
    let getIndustriesParameters={ 
      offset:"0",
      limit:"1000000",
      search:searchKey}
      this.cancelRequest=this.positionDescriptionService.getIndustries(JSON.stringify(getIndustriesParameters)).subscribe(
      data=>{
       if (data.code == "200") {
       this.industries=data.data;
       this.industries=this.industries.sort(function(a, b){
        a = (a.industryTitle || '').toLowerCase();
        b = (b.industryTitle || '').toLowerCase();   
        return (a > b) ? 1 : ((a < b) ? -1 : 0);
      });
       this.isProcessing=false;
       this.filterProcessing=false;
       }
    },
    error=>{
      this.isProcessing=false;
      this.filterProcessing=false;this.coreService.notify("Unsuccessful",error.message,0)},
    ()=>{}
    )
  }
  
  // opens dialog box to add industry
  onAddIndustry(){
     let dialogRef = this.dialog.open(AddEditIndusrtyDialog, {width:'400px'});     
        dialogRef.afterClosed().subscribe(result => {
          if(result){
            this.isProcessing=true;
            this.industry.industryTitle=result.industryTitle;
            this.positionDescriptionService.addEditIndustry(this.industry).subscribe(
            data=>{
               if (data.code == "200") {
              this.getIndustries();
              this.coreService.notify("Successful",data.message,1);
               }
            },
            error=>{
              this.isProcessing=false;
              this.coreService.notify("Unsuccessful",error.message,0)},
            ()=>{}
            )}
        });   
  }

  // opens dialog box to edit industry
  onEditIndustry(industry){
     let dialogRef = this.dialog.open(AddEditIndusrtyDialog, {width:'400px' });
        dialogRef.componentInstance.industry=industry;
        dialogRef.afterClosed().subscribe(result => {
          if(result){
            this.isProcessing=true;
           this.positionDescriptionService.addEditIndustry(result).subscribe(
            data=>{
              if (data.code == "200") {
              this.getIndustries();
              this.coreService.notify("Successful",data.message,1);
              }
            },
            error=>{this.isProcessing=false;this.coreService.notify("Unsuccessful",error.message,0)},
            ()=>{}
            )}
        });    
  }

  // delete industry
  onDeleteIndustry(industryID,index){
     this.confirmService.confirm(this.confirmService.tilte,this.confirmService.message,this.viewContainerRef)
    .subscribe(res=>{
       let result=res;
    if(result){
    let deleteIndustry={id:"",type:"Industry"};
    this.isProcessing=true;
   this.positionDescriptionService.deleteIndustry(industryID).subscribe(
            data=>{
              if (data.code == "200") {
              this.getIndustries();
              this.coreService.notify("Successful",data.message,1);
              }
            },
            error=>{this.isProcessing=false;this.coreService.notify("Unsuccessful",error.message,0)},
            ()=>{}
            )
    }
    })
  }
  
  // search industry
  onSearch(){
    this.cancelRequest.unsubscribe();
    this.getIndustries();
  }
  
}

@Component({
    selector: 'add-edit-industry-dialog',
    template: `<h1 md-dialog-title>{{title}}</h1>
        <div md-dialog-content style="padding-bottom: 20px;">
         <form #industryForm="ngForm" name="material_signup_form" class="md-form-auth form-validation"  >  
          <md-input-container class="full-width">
          <input mdInput  required rows="4" name="industry_name" (keyup)="onkeyPress(editIndustry.industryTitle)" [(ngModel)]="editIndustry.industryTitle" #industryname="ngModel" placeholder="Enter industry name">
          </md-input-container> 
          <div *ngIf="industryname.errors && (industryname.dirty || industryname.touched)">
                    <span style="color:red">Industry name is required !</span>
                </div>
          </form>       
        </div>
        <div md-dialog-actions>
            <button md-raised-button color="primary" [disabled]="disableEdit" type="submit" (click)="onSubmit(industryForm.form.valid);"style="margin-right:7px;">{{buttonText}}</button>
            <button type="button" md-raised-button color="default" (click)="dialogRef.close(false);">Cancel</button>
        </div>`,
})
export class AddEditIndusrtyDialog implements OnInit {
  public industry={
    industryTitle:"",
    industryID:""
  }
  public editIndustry={
    industryTitle:"",
    industryID:""
  };
  public oldIndustryTitle=''; 
  public title="Add industry";
  public buttonText="Add";
  public whiteSpaceError=false;
  public disableEdit=true;
    constructor(public dialogRef: MdDialogRef<AddEditIndusrtyDialog>) {}
    
    ngOnInit(){
   this.editIndustry=JSON.parse(JSON.stringify(this.industry));
      this.oldIndustryTitle=this.editIndustry.industryTitle; 
      if(this.editIndustry.industryID){
        this.title="Edit industry";
        this.buttonText="Save";
      }
    }

    onSubmit(isvalid){ 
       if(this.oldIndustryTitle==this.editIndustry.industryTitle){
          this.disableEdit=true;
        }else {
      this.disableEdit=false;
      if(this.editIndustry.industryTitle){  
      let trimmedMessgae=this.editIndustry.industryTitle.trim();
      let valid=(isvalid && (trimmedMessgae!=""))
      if(valid){         
        this.dialogRef.close(this.editIndustry);}
      }
        }
    }

    onkeyPress(industryTitle){        
        if(industryTitle!=this.oldIndustryTitle) this.disableEdit=false;
        else this.disableEdit=true;
    }
}
