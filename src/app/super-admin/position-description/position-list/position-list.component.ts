import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { PositionDescriptionService } from '../position-description.service';
import { PostionDescription } from '../position-description';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { CoreService } from '../../../shared/services/core.service';
import { ConfirmService } from '../../../shared/services/confirm.service';

@Component({
  selector: 'app-position-list',
  templateUrl: './position-list.component.html',
  styleUrls: ['./position-list.component.scss']
})
export class PositionListComponent implements OnInit {

  public hightlightStatus: Array<boolean> = [];

  plan={
    one:false,
    two:false,
    three:false
  };

  public keys=[];
  public isProcessing=true;
  public s={
             Active:false,
             Inactive:false
          }
  positionDescriptions=[];
  public searchKey='';
  public limit="10";
  public offset="0-9"
  industryCount=0;
  
  public filters={
  status:{},
  industryIDs:{},
}

public isDeleting=false;
     permission;

  constructor(public positionDescriptionService:PositionDescriptionService,public router:Router, public coreService:CoreService, public confirmService:ConfirmService, public viewContainerRef:ViewContainerRef ) { }

  ngOnInit() {
    this.keys = Object.keys(this.plan);
    this.getAllIndustries();
    this.permission=this.coreService.getNonRoutePermission();
}

  public status =[];
  public industryIDs = [];
  public industriesCount=["1","2","3","4+"];
  public searchKeyStatusType='';
  public searchKeyPDType='';
  public searchKeyIndustryCount='';
  public cancelRequest;
  public isListProcessing=false;

  // get position description list
  getPositionDescriptionsList(){
    this.isListProcessing=true;
    let searchText=encodeURIComponent(this.searchKey);
    let getPositionDescriptionsParameters={
      search:searchText,
      filter:this.filters};
      this.cancelRequest=this.positionDescriptionService.getPositionDescriptionsList(JSON.stringify(getPositionDescriptionsParameters)).subscribe(
      data=>{
         if (data.code == "200") {
       this.positionDescriptions=data.data;
        this.isProcessing=false;
        this.isListProcessing=false;
         }
         else {this.coreService.notify("Unsuccessful","Error while getting PD list",0);this.isProcessing=false;this.isListProcessing=false;}      
    },
    error=>{this.coreService.notify("Unsuccessful","Error while getting PD list",0);this.isProcessing=false;this.isListProcessing=false;},
    ()=>{});
  }

 //get all industries
  getAllIndustries(){
  this.status = Object.keys(this.s);
  this.positionDescriptionService.getAllIndustries().subscribe(
    data=>
    {      
        if(data.code=="200"){
          let industryTitles={};
          for(let d of data.data){
            industryTitles[d.industryTitle]=false;
            this.filters.industryIDs[d.industryID]=false; 
          }
          this.filters.status=this.s;
         this.industryIDs = data.data;
          this.getPositionDescriptionsList();
        } 
         else this.coreService.notify("Unsuccessful","Error while getting industry list",0);
      },
    error=>this.coreService.notify("Unsuccessful",error.message,0),
    ()=>{}
    )
  }
  
  // filter pd list
  onFilter(){
  this.cancelRequest.unsubscribe();
  this.getPositionDescriptionsList();
  }

 // search pd list
 onSearchKeyChange(){
  this.cancelRequest.unsubscribe();
    if(this.searchKey.trim().length != 0) this.getPositionDescriptionsList(); 
  }

  // navigates to pd edit page
  onEdit(item){
   this.router.navigate([`/app/super-admin/position-description/edit-pd-template/${item.positionID}`]);
  }

  // delete position description
  deletePositionDescription(positionID,index) {
    this.hightlightStatus[index]=!this.hightlightStatus[index];
  this.confirmService.confirm(this.confirmService.tilte,this.confirmService.message,this.viewContainerRef)
    .subscribe(res=>{   
    if(res){
      this.isDeleting=true;
    this.positionDescriptionService.deletePositionDescription(positionID).subscribe(
      data => {
        if (data.code == "200") {
          this.hightlightStatus[index]=!this.hightlightStatus[index];
          this.positionDescriptions = this.positionDescriptions.filter(function (i) {
            if (i.positionID == positionID ) return false;
            else return true;
          })
          this.isDeleting=false;
         this.getPositionDescriptionsList();
          this.coreService.notify("Successful","Data deleted successfully",1)
        }
      },
      error =>{this.isProcessing=false;this.coreService.notify("Unsuccessful","Error while deleting the data",0);},
      () => { }
    )
    }
    });  
  }

  // navigates to position description details page
   getPositionDescriptionDetails(positionID){
        this.router.navigate([`app/super-admin/position-description/details/${positionID}`]);
  }
}
