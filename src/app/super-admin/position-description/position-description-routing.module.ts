import { Routes, RouterModule }  from '@angular/router';
import { PositionListComponent } from './position-list/position-list.component';
import { PdTemplateComponent } from './pd-template/pd-template.component';
import { IndustryManagementComponent } from './industry-management/industry-management.component';
import { PositionDescriptionDetailsComponent } from './position-description-details/position-description-details.component';
import { PositionDescriptionGuardService } from './position-description-guard.service';
import { IndustryGuardService } from './industry-guard.service';
export const PositionDescriptionPagesRoutes: Routes = [
    {
        path: '',
        children: [
            { path: '',  component: PositionListComponent,canActivate:[PositionDescriptionGuardService] },
            { path: 'add-pd-template', component: PdTemplateComponent, canActivate:[PositionDescriptionGuardService] },
            { path: 'pd-list', component: PositionListComponent, canActivate:[PositionDescriptionGuardService] },
            { path: 'edit-pd-template/:id', component: PdTemplateComponent, canActivate:[PositionDescriptionGuardService] },
            { path: 'details/:id', component: PositionDescriptionDetailsComponent, canActivate:[PositionDescriptionGuardService] },            
            { path: 'industry-management', component: IndustryManagementComponent, canActivate:[IndustryGuardService] }
        ]
    }
];

export const PositionDescriptionPagesRoutingModule = RouterModule.forChild(PositionDescriptionPagesRoutes);
