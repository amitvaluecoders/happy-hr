import { Component, OnInit } from '@angular/core';
import { PositionDescriptionService } from '../position-description.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { CoreService } from '../../../shared/services/core.service';

@Component({
  selector: 'app-position-description-details',
  templateUrl: './position-description-details.component.html',
  styleUrls: ['./position-description-details.component.scss']
})
export class PositionDescriptionDetailsComponent implements OnInit {
   public positionDescription={
  positionID:'',
  title:"",
  summary:"",
  performanceIndicator:[{
  positionPerformanceIndicatorID:'',
  title:'',
  duration:'',
  measure:''}],
  roleAndResponsibility:"",
  pastExperience:"",
  education:"",
  industryIDs:[],
};

public industries=[];

public isDataAvailable=true;
public isProcessing=false;
id;
color='';
status= "";
permission;
    constructor(public positionDescriptionService:PositionDescriptionService,private router:Router,private activatedRoute:ActivatedRoute,public coreService:CoreService) { }


  ngOnInit() {
    this.getIndustries();
    this.permission=this.coreService.getNonRoutePermission();
  }
  
  // get industries
  getIndustries(){
    this.isProcessing=true;
  this.positionDescriptionService.getIndustriesForAddEdit().subscribe(
    data=>{
       if (data.code == "200") {
         let industryTitles={}; 
         this.industries=data.data;
         if(this.activatedRoute.snapshot.params['id']!= undefined){
      this.id=this.activatedRoute.snapshot.params['id'];
      this.getPostionDescription();
    }
         if(!this.id) this.isDataAvailable=false;
       }
    },
    error=>{this.isProcessing=false;this.coreService.notify("Unsuccessful","Error while getting the data",0);},
    ()=>{}
    )
  }
  
  // get position description details
  getPostionDescription() {
    this.isDataAvailable=true;
    this.positionDescriptionService.getPositionDescription(this.id).subscribe(
      data=>{
            if (data.code == "200") {
            this.positionDescription=data.data;
            this.status=data.data.positionStatus;
            this.color=(this.status == 'Active') ? 'primary' : 'default';
                let flag;
                for (let i of this.industries){    
                for(let p of data.data.positionindustry){
                if(p.industryID==i.industryID) {
                flag=true; 
                  break;
                  }else{
                    flag=false;
                  }   
              } 
              if(flag){
                    i.isChecked=true;
                  }else i.isChecked=false;
          }
          this.isDataAvailable=false;
          this.isProcessing=false;
                }    
                else {this.isProcessing=false;this.coreService.notify("Unsuccessful","Error while getting PD Details",0);}
      },
      error=>{this.isProcessing=false;this.coreService.notify("Unsuccessful","Error while getting the data",0);},
      ()=>{}
    )
  }

  // changes position description status
  statusChange(status) {
    let s = (status == 'Active') ? 'Inactive' : 'Active'
    let reqBody={positionID:this.positionDescription.positionID,status:s}
    this.positionDescriptionService.updateStatus(reqBody).subscribe(
      data => {
        this.status = s;
        this.color = (this.status == 'Active') ? 'primary' : 'default';
        this.coreService.notify("Successful", data.message, 1)
      },
      error => {
        this.coreService.notify("Unsuccessful", error.message, 0)
      }
    )
  }

  // navigates to position description list page
  onCancel(){
    this.router.navigate(['/app/super-admin/position-description/pd-list']);
  }

}
