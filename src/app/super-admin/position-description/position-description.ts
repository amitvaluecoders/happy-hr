export interface PostionDescription{
  id?:String,
  title?: String,
  summary?: String,
  performanceIndicator?: String,
  roleResponsbilities?: String,
  preExp?: String,
  education?: String,
  industryTypeIds?: String,
  status?: String
}