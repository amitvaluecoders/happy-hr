import { Pipe, PipeTransform } from '@angular/core';
import * as _ from "lodash";
@Pipe({
  name: 'multi',
   pure:false
})
export class CompanyTypePipe implements PipeTransform {

    transform(value: any, args?: any): any {
      var tempArgs={};  
      var flag=0;

      var rep=0;
      
      var result = value.filter(function (item) {
        var isAll = false;
        for (let arg in args) {
          
          if (!_.isEmpty(args[arg])) { 
                   
            let io = args[arg];  
            for (let i in io) {
              if (io[i]) {
                rep++;             
                if (item[arg] == i) {
                  flag++;
                  return item;
                } else isAll = true;
              }
            }
          }
        }

      })

      if(!flag) return value;
      else return result;
   }


}
