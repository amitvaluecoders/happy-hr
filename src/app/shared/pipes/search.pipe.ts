import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'search'
})
export class SearchPipe implements PipeTransform {

  transform(value: any, args: string): any {
    if (!value || !args) return value;
    let filter = args.toLocaleLowerCase();
    console.log(value,args)
    console.log("RETURN VALUEUUEUEUE",filter ? value.filter(item => {
      console.log("ITEM ",item.split(/(?=[A-Z])/).join(" ").toLocaleLowerCase().indexOf(filter) != -1)
      item.split(/(?=[A-Z])/).join(" ").toLocaleLowerCase().indexOf(filter) != -1;
    }) : value);
    return value.filter(item => {
      return item.split(/(?=[A-Z])/).join(" ").toLocaleLowerCase().indexOf(filter) != -1;
    });
  }
  // transform(value: any, searchText: any): any {
  //   if(searchText == null) return value;
  //   return value.filter(function(item){
  //     return item.name.toLowerCase().indexOf(searchText.toLowerCase()) > -1;
  //   })
  // }

}

@Pipe({
  name: 'searchBy'
})
export class SearchByPipe implements PipeTransform {

  transform(value: any, prop?: string, args?: string): any {
    if (!value || !prop) return value;
    let filter = args ? args.toLocaleLowerCase() : null;

    if (filter) {
      return value.filter(item => item[prop].split(/(?=[A-Z])/).join(" ").toLocaleLowerCase().indexOf(filter) != -1)
    }
    return value;
  }

}

@Pipe({
  name: 'searchByName'
})
export class SearchByNamePipe implements PipeTransform {

transform(value: any, searchText: any): any {
    if(searchText == null) return value;
    return value.filter(function(item){
       let txt=String(item['name']);
      return txt.toLowerCase().indexOf(searchText.toLowerCase()) > -1;
    })
  }

}

@Pipe({
  name: 'searchByTitle'
})
export class SearchByTitlePipe implements PipeTransform {

transform(value: any, searchText: any): any {
    if(searchText == null) return value;
    return value.filter(function(item){
      return item.title.toLowerCase().indexOf(searchText.toLowerCase()) > -1;
    })
  }

}


@Pipe({
  name: 'searchByKey'
})
export class SearchByKey implements PipeTransform {

transform(value: any, searchText: any,key:any): any {
  if(value){
    if(searchText == null) return value;
    return value.filter(function(item){
      let txt=String(item[key]);
      return txt.toLowerCase().indexOf(searchText.toLowerCase()) > -1;
    })
  }
  else return value;
  }

}
