import { Pipe, PipeTransform } from '@angular/core';
import { DatePipe } from '@angular/common';
import * as moment from "moment";

@Pipe({
  name: 'dateFormat'
})
export class DateFormatPipe extends DatePipe implements PipeTransform {

  transform(value: any, args?: any): any {
    let dateFormat;
    if(value){
        value = value.replace(/-/g, "/");
        switch(args) {
          case 'longDate':
              dateFormat = 'd MMMM, y';
              break;
          case 'shortDate':
              dateFormat = 'd-M-y';
              break;
          case 'shortTime':
              dateFormat = 'h:mm a';
              break;
          default:
            dateFormat = 'd MMMM, y';
        }
  }
    return super.transform(value, dateFormat);
  }

}
