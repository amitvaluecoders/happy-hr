import {Pipe, PipeTransform} from '@angular/core';

@Pipe({name: 'orderBy'})

export class SortPipe {

  transform(array: Array<Object>, args: string): Array<Object> {
    if (array == null) {
      return null;
    }

    array.sort((a: any, b: any) => {
        if (a[args] < b[args] ){
        //a is the Object and args is the orderBy condition (data.likes.count in this case)
            return -1;
        }else if( a[args] > b[args] ){
            return 1;
        }else{
            return 0;
        }
    });
    return array;
  }
}