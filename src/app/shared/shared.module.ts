import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EChartsDirective } from './echarts.directive';
//import { SlimScrollDirective } from './slim-scroll.directive';
import { ConfirmDialogComponent } from './confirm-dialog/confirm-dialog.component';
import { StepsComponent } from './components/steps/steps.component';
import { NoWhitespaceDirective } from './directives/no-whitespace.directive';
import { SearchPipe, SearchByPipe, SearchByNamePipe, SearchByTitlePipe, SearchByKey } from './pipes/search.pipe';
import { MaterialModule } from '@angular/material';
import { SortPipe } from './pipes/sort.pipe';
import { ResponseNoteComponent } from './components/response-note/response-note.component';
import { RequestChangePopupComponent }  from '../user/subcontractor-induction/request-change-popup/request-change-popup.component';
import { MandatoryDocumentsPopupComponent } from './components/mandatory-documents-popup/mandatory-documents-popup.component';
import { FormsModule } from '@angular/forms';
import { SharedService } from './shared.service';
import { DomDetectorDirective } from './directives/dom-detector.directive';
import { DpDatePickerModule } from 'ng2-date-picker';
import { RestfulLaravelService } from './services';
import { MessagesComponent } from './components/messages/messages.component';
import { AngularMultiSelectModule } from 'angular2-multiselect-checkbox-dropdown/angular2-multiselect-dropdown';
import { MessagesService } from './services/messages.service';
import { TinymceModule } from 'angular2-tinymce';
import { SocketServiceService } from './services/socket-service.service';
import { TrophyCommentsPopupComponent } from '../company-admin/company-admin-dashboard/trophy-comments-popup/trophy-comments-popup.component';
import { UploadMendatoryDocsPopupComponent } from '../user/subcontractor-induction/upload-mendatory-docs-popup/upload-mendatory-docs-popup.component';

import "tinymce/plugins/link/plugin.js";
import "tinymce/plugins/paste/plugin.js";
import "tinymce/plugins/advlist/plugin.js";
import "tinymce/plugins/lists/plugin.js";
import "tinymce/plugins/table/plugin.js";
import "tinymce/plugins/preview/plugin.js";
import "tinymce/plugins/image/plugin.js";
import "tinymce/plugins/textcolor/plugin.js";
import "tinymce/plugins/colorpicker/plugin.js";
import "tinymce/plugins/searchreplace/plugin.js";
import "tinymce/plugins/directionality/plugin.js";
import "tinymce/plugins/emoticons/plugin.js";
import "tinymce/plugins/code/plugin.js";
import "tinymce/plugins/insertdatetime/plugin.js";
import "tinymce/plugins/toc/plugin.js";
import "tinymce/plugins/hr/plugin.js";
// import "tinymce/plugins/imagetools/plugin.js";
import { SafePipe } from './pipes/safe.pipe';
import { ActionsOnEmployeePopupComponent } from './components/actions-on-employee-popup/actions-on-employee-popup.component';
import { MyMultiSelectComponent } from './directives/my-multi-select/my-multi-select.component';
import {CalendarComponent} from "ap-angular2-fullcalendar/src/calendar/calendar";
import { EmployeeListingPopupComponent } from "../company-admin/company-module-listing/employee-listing-popup/employee-listing-popup.component";
import { UploadDocumentDialog } from '../company-employee/induction/complete-profile/complete-profile.component';
import { SlimScrollDirective } from './slim-scroll.directive';
import { DateFormatPipe } from './pipes/date.pipe';
// import { TinymceComponent } from './components/tinymce/tinymce.component';
//import { MessagesModule } from './modules/messages/messages.module';

@NgModule({
    imports: [CommonModule, MaterialModule, FormsModule,
        DpDatePickerModule, AngularMultiSelectModule,
        TinymceModule.withConfig({
            skin_url: 'assets/skins/lightgray',
            //baseURL: 'node_modules/tinymce/',
            height: 150,
            resize: false,
            elementpath: false,
            auto_focus: false,
            plugins: [
                'lists link hr searchreplace code insertdatetime textcolor colorpicker table',
                //   'searchreplace wordcount visualblocks visualchars code fullscreen tinymcespellchecker advcode',
                //   'template paste textcolor colorpicker textpattern imagetools codesample toc help emoticons hr'
            ],
            toolbar: 'bold italic underline strikethrough blockquote | undo redo code | forecolor backcolor | formatselect | fontsizeselect | fontselect | link | alignleft aligncenter alignright alignjustify  | numlist bullist | superscript subscript | outdent indent  | table | searchreplace removeformat',
            // textcolor_map: ['000000', 'Black'],
            menubar: false,
            //   setup: editor => {
            //     this.editor = editor;
            //     editor.on('change', () => {
            //       const content = editor.getContent();
            //      // this.onEditorKeyup.emit(content);
            //     });
            //   },
            //   init_instance_callback: (editor: any) => {
            //     editor && this.value && this.editor.setContent(this.value)
            //     }
            // skin_url: 'assets/skins/lightgray',

        }),
    
    ],
    declarations: [
        EChartsDirective,
        //SlimScrollDirective,
        ConfirmDialogComponent,
        StepsComponent,
        UploadDocumentDialog,
        SearchPipe,
        RequestChangePopupComponent,
        CalendarComponent,
        UploadMendatoryDocsPopupComponent,
        SearchByPipe,
        SearchByNamePipe,
        SearchByTitlePipe,
        MandatoryDocumentsPopupComponent,
        TrophyCommentsPopupComponent,
        NoWhitespaceDirective,
        SortPipe,
        ResponseNoteComponent,
        DomDetectorDirective,
        SlimScrollDirective,
        SearchByKey,
        MessagesComponent,
        SafePipe,
        ActionsOnEmployeePopupComponent,
        MyMultiSelectComponent,
        EmployeeListingPopupComponent,
        DateFormatPipe
        // TinymceComponent   
    ],
    exports: [
        UploadDocumentDialog,
        EChartsDirective,
        //SlimScrollDirective,
        StepsComponent,
        SlimScrollDirective,
        CalendarComponent,
        SearchPipe,
        SearchByPipe,
        SearchByNamePipe,
        SearchByTitlePipe,
        NoWhitespaceDirective,
        SortPipe,
        DomDetectorDirective,
        SearchByKey,
        DpDatePickerModule,
        MessagesComponent,
        TinymceModule,
        SafePipe,
        MyMultiSelectComponent,
        EmployeeListingPopupComponent,
        DateFormatPipe
        // TinymceComponent
    ],
    providers: [SharedService, RestfulLaravelService, MessagesService, SocketServiceService],
    entryComponents: [TrophyCommentsPopupComponent,RequestChangePopupComponent,MandatoryDocumentsPopupComponent,UploadMendatoryDocsPopupComponent,ConfirmDialogComponent, StepsComponent, ResponseNoteComponent, ActionsOnEmployeePopupComponent,EmployeeListingPopupComponent,UploadDocumentDialog]
})

export class SharedModule { }
