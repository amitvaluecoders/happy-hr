import { Directive ,ElementRef} from '@angular/core';

@Directive({
  selector: '[appDomDetector]'
})
export class DomDetectorDirective {

  public observer:any;

   constructor(private elRef:ElementRef) {}

  ngAfterViewInit() {
    this.observer = new MutationObserver(mutations => {
      mutations.forEach(function(mutation) {
        console.log(mutation.type);
      });   
    });
    var config = { attributes: true, childList: true, characterData: true };

    this.observer.observe(this.elRef.nativeElement, config);
  }
}
