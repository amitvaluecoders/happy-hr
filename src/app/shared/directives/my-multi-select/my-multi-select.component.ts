import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

export class Item {
  id: any;
  itemName: string;
  imageUrl: string;
  position: string;
  isSelected: boolean;
  external: boolean;
}

@Component({
  selector: 'my-multi-select',
  templateUrl: './my-multi-select.component.html',
  styleUrls: ['./my-multi-select.component.scss']
})
export class MyMultiSelectComponent {
  private _modelName = '';
  private _itemList = Array<Item>(); // {'itemName', 'imageUrl', 'subName'}
  private _selectedItemList = Array<Item>(); // {'itemName', 'imageUrl', 'subName'}
  public isFocused = false;
  public isEnterPressed = false;


  // in HTML
  // <my-multi-select [itemList]="itemList" [modelName]="modelName" (onItemSelected)="onItemSelected()" (onEnterExternalItem)="onEnterExternalItem()"></my-multi-select>

  @Input()
  set itemList(itemList: Item[]) {
    this._itemList = (itemList) ? itemList : [];
  }

  @Input() disabled: boolean;
  // @Input() allowThirdParty: boolean;

  @Input()
  set modelName(modelName: string) {
    console.log('check model ===> ', modelName && typeof modelName == 'string', modelName);
    this._modelName = (modelName && typeof modelName == 'string') ? modelName.trim() : '';
  }

  get itemList(): Item[] { return this._itemList; }
  get modelName(): string { return this._modelName; }

  @Output() onItemSelected = new EventEmitter<any>();
  @Output() onEnterExternalItem = new EventEmitter<boolean>();


  // on adding external third party.
  onSelect(item) {
    item.isSelected = !item.isSelected;

    // this._selectedItemList = this._itemList.filter(i => i.isSelected);
    this.onItemSelected.emit(item);
  }

  // on adding external third party.
  onEnterExternalThirdParty(isvalid) {
    
    let alreadyExist;
    if (this.modelName) {
      this.isEnterPressed = true;
      if (isvalid && this.modelName.trim() != '') {
        for (let c of this._itemList) {
          if (c.itemName == this.modelName) alreadyExist = true;
        }
        if (!alreadyExist) {
          this.onEnterExternalItem.emit(true);
          this._itemList.unshift({
            id: '', isSelected: true, external: true, itemName: this.modelName, position: '',
            imageUrl: "http://happyhrapi.n1.iworklab.com/public/user-profile-image/user.png"
          })
          this.modelName = '';
          this.isEnterPressed = false;
        }
        else {
          this.onEnterExternalItem.emit(false);
        }
      }
    }

  }
}
