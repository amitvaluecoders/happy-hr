import { Component, OnInit ,Input } from '@angular/core';

@Component({
  selector: 'app-steps',
  templateUrl: './steps.component.html',
  styleUrls: ['./steps.component.scss']
})
export class StepsComponent implements OnInit {
  public backgroundColor='';
  public final=''
  @Input() progress;
  // public progress;
  constructor() { }

  ngOnInit() {
  //step 1: #ff8383;
    // step-2{
    //     background-color:#f79e31;
    // }
    // &.step-3{
    //     background-color:#ffcc33;
    // }
    // &.final-step{
    //     background-color:#4cd9aa;
    // }
    // (this.progress['current_step']==2)?this.backgroundColor="red":this.backgroundColor="#fff";    // this.progress=100;
     if(this.progress['total_steps']==this.progress['current_step']){
         this.final=this.progress['current_step'];
     }
       switch (this.progress['current_step']) {
    case 1:
       this.backgroundColor="#ff8383";
        break;
    case this.final :
     this.backgroundColor="#4cd9aa";
        break;
    case 2:
        this.backgroundColor="#f79e31";
        break;
    case 3:
        this.backgroundColor="#ffcc33";
        break;
    case this.progress['current_step']:
        this.backgroundColor="#8a89d9";
        break;
}
     
    
  }

}
