import { Component, OnInit } from '@angular/core';
import { SharedService } from '../../shared.service'
import { CoreService } from '../../../shared/services/core.service';
import { APPCONFIG } from '../../../config';
import { MdSnackBar, MdDialog, MdDialogRef } from '@angular/material';

@Component({
  selector: 'app-response-note',
  templateUrl: './response-note.component.html',
  styleUrls: ['./response-note.component.scss']
})
export class ResponseNoteComponent implements OnInit {

  public isProcessing=false;
  public isButtonClicked=false;
  public noteDetails={
   note:{
     responseNote:''  
   } 
  };

  constructor(public sharedService:SharedService, public coreService: CoreService,
        public dialogRef: MdDialogRef<ResponseNoteComponent>) { }

  ngOnInit() {

  }


// send response note.
  onSubmit(isvalid) {
    this.isButtonClicked=true;
    if(isvalid){
      this.isProcessing=true;
      console.log(this.noteDetails);
      this.noteDetails['responseNote'] = this.noteDetails.note.responseNote;  // will be delete line after assest approve change
       this.sharedService.sendResponseNote(this.noteDetails).subscribe(
          d => {
            if (d.code == "200") {
              this.dialogRef.close(true);
              this.coreService.notify("Successful", d.message, 1);
              this.isProcessing = false;
              // if(d.data && d.data.responseNote)APPCONFIG.tempNote = d.data.responseNote;
            }
            else this.coreService.notify("Unsuccessful", d.message, 0);
          },
          error => this.coreService.notify("Unsuccessful", error.message, 0),
          () => { }
      )
    }
  }

}
