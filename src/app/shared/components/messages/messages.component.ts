import { Component, OnInit, ViewContainerRef  } from '@angular/core';
import { MessagesService } from '../../services/messages.service';
import { CoreService } from '../../services/core.service';
import { APPCONFIG } from '../../../config';
import { ConfirmService } from '../../services/confirm.service';

@Component({
  selector: 'app-messages',
  templateUrl: './messages.component.html',
  styleUrls: ['./messages.component.scss']
})
export class MessagesComponent implements OnInit {
  public filterMenu:boolean;
  filterType='allMessage';
  userWithMessagesList=[];
  userMessages=[];
  messageText='';
  searchKey='';
  showSelectUserDropdown=true;
  userListProcessing=false;
  messagesListProcessing=false;
  filterTitle='All messages';
  selectedIndex="allMessage"
  constructor(private messageService:MessagesService, private coreService:CoreService,public confirmService:ConfirmService, public viewContainerRef:ViewContainerRef) { }

  ngOnInit() {
    APPCONFIG.heading="Messages";
    this.getUsersWithMessages(this.filterType);
    this.getUsers();
    this.filterMenu=false;
  }

  onTime(time){return new Date(time);}

  //list of users having messages 
  getUsersWithMessages(filterType){
    this.selectedIndex=filterType;
    let reqBody={
      filter:filterType
    };
    this.userListProcessing=true;
    this.filterMenu=false;    
    this.messageService.getUsersWithMessages(JSON.stringify(reqBody)).subscribe(
      data=>{
      if(data.code==200){
        this.userWithMessagesList=data.data;
        this.userListProcessing=false;        
      }
      },
      error=>{
        this.userListProcessing=false;        
        this.coreService.notify("Unsuccessful",error.message,0);
      },
      ()=>{}
    )
  }
  
  //list the messages of a user or group
  getUserMessages(user){
    this.selectedUsers=[];
    this.messagesListProcessing=true;
    let reqBody={};
    if(user.isGroup){
      reqBody={
        groupID:user.groupID,isGroup:user.isGroup};
    }else{
      reqBody={
        userID:user.userID,isGroup:user.isGroup};
    }    
    this.messageService.getUserMessages(JSON.stringify(reqBody)).subscribe(
      data=>{
      if(data.code==200){
        this.userMessages=data.data;
        this.showSelectUserDropdown=false;
        this.messagesListProcessing=false;   
        this.selectedUsers=[]; 		
        this.userIDs=[];		
        this.messageFiles=[];      
      }
      },
      error=>{
        this.showSelectUserDropdown=false;
        this.messagesListProcessing=false;                
        this.coreService.notify("Unsuccessful",error.message,0);
      },
      ()=>{}
    )
  }

  //sends message to user or group
  sendMessage(){
    if(this.userIDs.length || (this.userMessages["userID"] || this.userMessages["groupID"])){   
    let reqBody={};
    if(this.userMessages["isGroup"]){
      reqBody={
        messageText:this.messageText,
        groupID: this.userMessages["groupID"],
        attachments: this.messageFiles,
        isGroup: true
      }}else if(!this.userMessages["isGroup"] && !this.userIDs.length){
        reqBody={
          messageText:this.messageText,
          userID: this.userMessages["userID"],
          attachments: this.messageFiles,
          isGroup: false
        }
  }else{
     reqBody={
  messageText: this.messageText,
  userIDs:this.userIDs ,
 attachments: this.messageFiles
}
  }
  if(this.messageText || this.messageFiles.length){
    let m=this.messageText;
    this.sendingMessage=true;
    
  this.messageService.sendUserMessages(reqBody).subscribe(
    data=>{
    if(data.code==200){     
      if(data.data) this.userMessages["conversationList"].push(data.data);
      this.selectedUsers=[]; 
      this.messageFiles=[];
      if(this.userIDs.length) this.getUsersWithMessages(this.filterType);
      this.messageText='';
      this.sendingMessage=false;
    
      this.coreService.notify("Successful",data.message,1);
      this.userIDs=[];
      this.onDeSelectAllUsers(this.userIDs);
    }
    },
    error=>{
      this.sendingMessage=false;      
      this.coreService.notify("Unsuccessful",error.message,0);
    },
    ()=>{}
  )}else{
    this.coreService.notify("Unsuccessful","Please write message",0);
  }
    }else{
      this.coreService.notify("Unsuccessful","No user found to send message",0);
    }
}

// archieve user or group
archiveUser(user){
  this.userListProcessing=true;
  let reqBody={};
  if(user["isGroup"]){
  reqBody={
    isGroup: true,groupID:user.groupID
  }
}else{
  reqBody={
    isGroup: false,userID:user.userID
  }
}
  this.messageService.archieveUser(JSON.stringify(reqBody)).subscribe(
    data=>{
    if(data.code==200){
      //this.userWithMessagesList=data.data;
      this.userListProcessing=false;   
      this.getUsersWithMessages(this.filterType);
      this.showSelectUserDropdown=true; 
      this.coreService.notify("Successful",data.message,1);     
    }
    },
    error=>{
      this.userListProcessing=false;        
      this.coreService.notify("Unsuccessful",error.message,0);
    },
    ()=>{}
  )
}

 public userIDs= [];
 public dropDownListType=[]
 public selectedUsers=[];

// multi select dropdown arrays and setting object.
  public dropDownSettingMultiSelect={
      singleSelection: false,
      text: "Type a name or create group *",
      enableCheckAll:true,
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      enableSearchFilter: true,
      classes: "myclass custom-class"               
  }

   onSelectUser(e){
     //this.userIDs=[];
      this.userIDs.push({userID:e.id});
  }

   onDeSelectUser(e){
    this.userIDs=this.userIDs.filter(item=>item.userID!=e.id);
    //this.trophy.trophyFor=[];
  }

  onSelectAllUsers(e){
    this.userIDs=[];
    for(let user of this.dropDownListType)
  this.userIDs.push({userID:user.id});
}

onDeSelectAllUsers(e){
  this.userIDs=[];
}

public users=[];
  
  //get users to send message 
  getUsers() {
    let i=0;
    this.messageService.getAllUsers().subscribe(
      data => {
        if (data.code == "200") {
          if(data.data){
          this.users = data.data;
          for(let user of this.users){
            this.dropDownListType.push({id:user.userID,itemName:user.name})            
        }
        }
        }
      },
      error =>
        {         
          this.coreService.notify(
          "Unsuccessful",
          error.message,
          0
        )},
      () => {}
    );
  }

public messageFiles=[];

//handle the attachment in message
handleFileSelect($event) {
    this.isUploading=true;
    const files = $event.target.files || $event.srcElement.files;
    const file = files[0];
    const formData = new FormData();
    formData.append('messageAttachments', file);
    this.messageService.uploadFile(formData).subscribe(
      data => {
        this.messageFiles[0]=({"attachmentName":data.data.messageAttachments})
        this.isUploading=false;
      },error=>{		
        this.isUploading=false;		
        this.coreService.notify("Unsuccessful",error.message,0);
    });
  }

public isUploading=false;
public sendingMessage=false;

//filter 
  filterData(){
    let reqBody={};
    this.userListProcessing=true;    
     this.messageService.filterUser(reqBody).subscribe(
      data=>{
      if(data.code==200){
        this.userWithMessagesList=data.data;
        this.userListProcessing=false;        
      }
      },
      error=>{
        this.userListProcessing=false;        
        this.coreService.notify("Unsuccessful",error.message,0);
      },
      ()=>{}
    )
  }

//delete user or group
  deleteUser(user){
    this.confirmService.confirm(this.confirmService.tilte,this.confirmService.message,this.viewContainerRef)		
    .subscribe(res=>{		
       let result=res;		
    if(result){
    let reqBody={};
    if(user["isGroup"]){
    reqBody={
      isGroup: true,groupID:user.groupID
    }
  }else{
    reqBody={
      isGroup: false,userID:user.userID
    }
  }
    this.userListProcessing=true;    
     this.messageService.deleteUser(JSON.stringify(reqBody)).subscribe(
      data=>{
      if(data.code==200){
        this.userListProcessing=false;  
        this.getUsersWithMessages(this.filterType);   
        this.showSelectUserDropdown=true;   
      }
      },
      error=>{
        this.userListProcessing=false;        
        this.coreService.notify("Unsuccessful",error.message,0);
      },
      ()=>{}
    )
  }})
  }

//remove attachment from message
  removeAttachment(){
    this.messageFiles=[];
  }
}
