import { Component, OnInit } from '@angular/core';
import { Router} from '@angular/router';
import { CoreService } from '../../../shared/services/core.service';
import { MdDialogRef, MdDialog, MdSnackBar } from '@angular/material';
@Component({
  selector: 'app-mandatory-documents-popup',
  templateUrl: './mandatory-documents-popup.component.html',
  styleUrls: ['./mandatory-documents-popup.component.scss']
})
export class MandatoryDocumentsPopupComponent implements OnInit {

  isButtonClicked:any;
  document={
    name:''
  };
  isProcessing=false;
  constructor( private coreService:CoreService, private router:Router,public dialogRef: MdDialogRef<MandatoryDocumentsPopupComponent>) { }

  ngOnInit() {
  }

  save(mandatoryDocForm){
     if(mandatoryDocForm.form.valid) this.dialogRef.close(this.document);   
    }
  

}
