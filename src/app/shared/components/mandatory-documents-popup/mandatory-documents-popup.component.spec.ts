import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MandatoryDocumentsPopupComponent } from './mandatory-documents-popup.component';

describe('MandatoryDocumentsPopupComponent', () => {
  let component: MandatoryDocumentsPopupComponent;
  let fixture: ComponentFixture<MandatoryDocumentsPopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MandatoryDocumentsPopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MandatoryDocumentsPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
