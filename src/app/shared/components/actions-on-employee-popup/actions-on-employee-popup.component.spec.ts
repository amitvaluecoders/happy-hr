import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ActionsOnEmployeePopupComponent } from './actions-on-employee-popup.component';

describe('ActionsOnEmployeePopupComponent', () => {
  let component: ActionsOnEmployeePopupComponent;
  let fixture: ComponentFixture<ActionsOnEmployeePopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ActionsOnEmployeePopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActionsOnEmployeePopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
