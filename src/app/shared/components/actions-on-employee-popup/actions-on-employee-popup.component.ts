import { Component, OnInit,ViewContainerRef } from '@angular/core';
import { CoreService } from '../../../shared/services/core.service';
import { MdSnackBar, MdDialog, MdDialogRef } from '@angular/material';
import { SharedService } from '../../shared.service';
import { ConfirmService } from '../../../shared/services/confirm.service';

@Component({
  selector: 'app-actions-on-employee-popup',
  templateUrl: './actions-on-employee-popup.component.html',
  styleUrls: ['./actions-on-employee-popup.component.scss']
})
export class ActionsOnEmployeePopupComponent implements OnInit {

  public actionInfo:any;
  public reqBody:any;
  public isButtonClicked=false;
  public isProcessing=false;
  public isActionProcessing=false;
public actionForm={
  subject:'',
  body:''
}

  constructor(public sharedService:SharedService, public coreService: CoreService,
  public confirmService:ConfirmService,public viewContainerRef:ViewContainerRef,
        public dialogRef: MdDialogRef<ActionsOnEmployeePopupComponent>) { }

  ngOnInit() {
    this.reqBody=JSON.parse(JSON.stringify(this.actionInfo));
    this.getActionData();
  }

// method to get action details. 
  getActionData(){
     this.isProcessing = true;
     this.sharedService.getActionDetails(this.actionInfo).subscribe(
          d => {
            if (d.code == "200") {
              this.isProcessing = false;
              this.reqBody['body']=d.data.body;
              this.reqBody['subject']=d.data.subject;
            }
            else this.coreService.notify("Unsuccessful", d.message, 0);
          },
          error => this.coreService.notify("Unsuccessful", error.message, 0),
          () => { }
      )
  }

  onSave(isvalid) {
    this.isButtonClicked = true;
    if (isvalid) {
      if (this.reqBody.type != "Terminate") this.submitAction();
      else {
        this.confirmService.confirm('Terminate confirmation', "Do you want to terminate this user?", this.viewContainerRef)
          .subscribe(res => {
            let result = res;
            if (result) this.submitAction();
            else  this.dialogRef.close(false);
          })
      }
    }
  }

  submitAction(){
     this.isActionProcessing = true;
        this.sharedService.submitUserAction(this.reqBody).subscribe(
          d => {
            if (d.code == "200") {
              this.coreService.notify("Successful", d.message, 1);
              this.dialogRef.close(true);
            }
            else this.coreService.notify("Unsuccessful", d.message, 0);
          },
          error => this.coreService.notify("Unsuccessful", error.message, 0),
          () => { }
        )
  }

}
