import { Injectable } from "@angular/core";
import { Cookie } from "ng2-cookies/ng2-cookies";
import { NotificationsService } from "angular2-notifications";
import { RestfulLaravelService } from "./restful-laravel.service";
import {
  CanActivate,
  Router,
  RouterStateSnapshot,
  ActivatedRouteSnapshot
} from "@angular/router";

import { APPCONFIG } from "../../config";

@Injectable()
export class CoreService {
    public trophyAndShoutouts:any=[];
  public permission;
  public role;
  public developmentPlan: any;
  public pip: any;
  public module = "";
  public subRole = "";
  // public userID;
  private SiteUnderMaintenanceMessage = "";
  private roles = [
    "superAdmin",
    "companyAdmin",
    "companyManager",
    "companyEmployee"
  ];
  private colors = [
    "assets/images/color-blue.png",
    "assets/images/color-pink.png",
    "assets/images/color-orange.png",
    "assets/images/color-yellow.png",
    "assets/images/color-cyan.png",
    "assets/images/color-skyblue.png",
    "assets/images/color-red.png",
    "assets/images/color-green.png",
    "assets/images/color-grey.png",
    "assets/images/color-purple.png"
  ];
  private awardFor = [
    { id: 1, itemName: "Employee" },
    { id: 2, itemName: "Manager" },
    { id: 3, itemName: "Anyone" }
  ];

  private admin: boolean = false;
  public commonRoute = "";
  public months = [
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December"
  ];
  constructor(
    private router: Router,
    private restfulLaravelService: RestfulLaravelService,
    private _notify: NotificationsService
  ) {
    this.initializeAppConstractor();
  }

  initializeAppConstractor() {
    if (this.getRole()) {
      this.role = this.getRole();
      if (this.role == "superAdmin") this.commonRoute = "/app/super-admin";
    }
    if (Cookie.get("happyhr-permission")) {
      this.permission = JSON.parse(Cookie.get("happyhr-permission"));
    }
    if (localStorage.getItem("happyhr-permission")) {
      this.permission = JSON.parse(localStorage.getItem("happyhr-permission"));
    }

    if (JSON.parse(localStorage.getItem("happyhr-userInfo"))) {
      this.subRole = JSON.parse(
        localStorage.getItem("happyhr-userInfo")
      ).subRole;
    }
  }

  /**
   * copy data from source to matching properties in destination
   * @param destination
   * @param source
   */
  copyObject(destination, source) {
    let des = JSON.parse(JSON.stringify(destination)),
      src = JSON.parse(JSON.stringify(source));
    let keys = Object.keys(des);
    for (let i = 0; i < keys.length; i++) {
      if (src[keys[i]]) {
        des[keys[i]] = src[keys[i]];
      }
    }
    return des;
  }

  getSubPath() {
    const path = {
      companyManager: "company-manager",
      companyAdmin: "company-admin",
      companyEmployee: "employee",
      superAdmin: "super-admin"
    };

    return path[this.role];
  }

  getCompanyLogo() {
    let companyLogo = localStorage.getItem("happyhr-companyLogo");
    return companyLogo;
  }

  getColors() {
    return this.colors;
  }

  // methood to set development plan details.
  setDevelopmentPlan(dp) {
    this.developmentPlan = dp;
  }

  setCanSwitch(value) {
    APPCONFIG.canSwitch = value;
    localStorage.setItem("canSwitch", value);
  }

  getCanSwitch() {
    let canSwitch = JSON.parse(localStorage.getItem("canSwitch"));
    return canSwitch;
  }

  // methood to get development plan details.
  getDevelopmentPlan() {
    return this.developmentPlan;
  }

  // methood to set development plan details.
  setPIP(dp) {
    this.pip = dp;
  }

  // methood to get development plan details.
  getPIP() {
    return this.pip;
  }

  getRoles() {
    return this.roles;
  }
  getAwardForList() {
    return this.awardFor;
  }
  //set role
  setRole(role) {
    this.role = role;
    let user = this.getUser();
    if (user) {
      user.userRole = role;
    }
    localStorage.setItem("happyhr-userInfo", JSON.stringify(user));
    Cookie.set("happyhr-userInfo", JSON.stringify(user));
   
  }

  //get roles
  getRole() {
    let user = this.getUser();
    if (user) return user.userRole;
    return null;
  }

  setLocal(key: string, value) {
    let user = this.getUser();
    if (user) {
      user[key] = value;
    }
    Cookie.set("happyhr-userInfo", JSON.stringify(user));
    localStorage.setItem("happyhr-userInfo", JSON.stringify(user));
  }

  setProfileImage(imageUrl) {
    let user = this.getUser();
    if (user) {
      user.imageUrl = imageUrl;
    }
    Cookie.set("happyhr-userInfo", JSON.stringify(user));
    localStorage.setItem("happyhr-userInfo", JSON.stringify(user));
  }

  getUser() {
    let user: any = {};
    if (localStorage.getItem("happyhr-userInfo")) {
      try {
        user = JSON.parse(localStorage.getItem("happyhr-userInfo"));
      } catch (e) {
        user = {};
      }
    } else if (Cookie.get("happyhr-userInfo")) {
      try {
        user = JSON.parse(Cookie.get("happyhr-userInfo"));
      } catch (e) {
        user = {};
      }
    }
    return user;
  }

  //set permissions
  setPermission(permission) {
    // console.log(permission);
    if (permission) this.permission = permission;
  }

  //get permissions
  getPermission() {
    return this.permission;
  }

  //get non route permission()
  getNonRoutePermission() {
    for (let permission of this.permission) {
      if (permission.module == this.module) {
        return permission.permission;
      }
    }
    return { add: true, edit: true, delete: true, view: true };
  }

  isManager() {
    return this.role == "companyManager";
  }

  setSiteUnderMaintenanceMessage(message) {
    this.SiteUnderMaintenanceMessage = message;
  }

  getSiteUnderMaintenanceMessage() {
    return this.SiteUnderMaintenanceMessage;
  }
  
  isAdmin() {
    // return this.admin;
    return this.role == "companyAdmin";
  }
  isContractor(){
    return this.role =="companyContractor";
  }
  checkRole(role) {
    let isProfileCompleted = false;
    let userInfo = localStorage.getItem("happyhr-userInfo");
    if (userInfo && JSON.parse(userInfo).incompletePage == -1)
      isProfileCompleted = true;

    if (role == "companyAdmin") {
      // isProfileCompleted
      if ((this.role == "companyAdmin" || this.role == "companyManager" || this.role == "companyContractor") && userInfo ) {
            let incompleteCount=JSON.parse(userInfo).incompletePage;
        switch (incompleteCount) {
          case 0:
          this.router.navigate(["/user/company-registration/payment-options"]);
            return true;
          case 1:
          this.router.navigate(["/user/company-registration/questionnaire"]);
          return true;
          case 2:
          this.router.navigate(["/user/company-registration/questionnaire/step/2"]);
          return true;
          case 3:
          this.router.navigate(["/user/company-registration/questionnaire/step/3"]);
          return true;
          case 4:
          this.router.navigate(["/user/company-registration/questionnaire/step/4"]);
          return true;
          default:
            return true;
        } 
      } 
      else return false;
    } else if (this.role == role && isProfileCompleted) {
      return true;
    } else return false;
    // if (this.role == role) { return true; } else return false;
  }

  getModulePermission(mod, submodule) {
    this.module = mod;
    //console.log("permission 111", this.permission)
    var access = JSON.parse(JSON.stringify(this.permission));
    //console.log("Access",access);
    //console.log("module",mod)
    for (let m of access) {
      console.log("module 1111111", m);
      if (m.module == mod) {
        // console.log("ppperrrrrmiiisisisisisiisisis",m["permission"][submodule])
        return m["permission"][submodule];
      }
    }
  }

  notify(title, message, type) {
    this._notify.remove();
    switch (type) {
      case 0:
        this._notify.error(title, message);
        break;
      case 1:
        this._notify.success(title, message);
        break;
      case 2:
        this._notify.info(title, message);
        break;
      case 3:
        this._notify.alert(title, message);
        break;
      default:
        this._notify.success("Notification !", "Task Done");
        break;
    }
  }

  // method to get errors in the form 
  getFormValidationErrors(form) {
    let errors=[];
    Object.keys(form.controls).forEach(key => {
    const controlErrors = form.controls[key].errors;
    if (controlErrors != null) {
          Object.keys(controlErrors).forEach(keyError => {
            let tempKey = this.modifyKeyforError(key);
            switch(keyError) {
                case "minlength":
                    errors.push(tempKey + ' field required atleast '+controlErrors[keyError]['requiredLength'] 
                      + " characters ! You have only entered " + controlErrors[keyError]['actualLength'] + " yet.");
                    break;
                case "whitespace":
                    errors.push(tempKey+ ' field is required') ;
                    break;
                case "required":
                    errors.push(tempKey+ ' field is required') ;
                    break;
                default:
                    errors.push(tempKey+ ' field is required') ;
            }
          });
        }
      });

      return errors;
  }
    getTrophyAndShoutouts(){
    return this.trophyAndShoutouts;
  }

   setTrophyAndShoutouts(trophyAndShoutouts){
    this.trophyAndShoutouts = trophyAndShoutouts;
  }


   modifyKeyforError(key){
    let temp = key.replace(/([A-Z]+)/g, " $1");
    temp = temp.toLocaleLowerCase();
    temp = temp.charAt(0).toUpperCase() + temp.slice(1);
    return temp;
    }
  
}
