import { Injectable } from '@angular/core';
import { RestfulLaravelService} from './restful-laravel.service';
import { CoreService} from './core.service';

@Injectable()
export class MessagesService {
 constructor(private restfulWebService:RestfulLaravelService,private coreService:CoreService) { }
  
  getUsersWithMessages(reqBody):any{
      return this.restfulWebService.get(`admin/get-message-user-list/${reqBody}`)
     }

     getUserMessages(user):any{
      return this.restfulWebService.get(`admin/get-message-conversation/${user}`)
     }

      getAllUsers():any{
      return this.restfulWebService.get('admin/get-all-user-list')
     }

     filterUser(reqBody):any{
      return this.restfulWebService.get(`admin/filter-user-list/${reqBody}`)         
     }

     deleteUser(reqBody):any{
      return this.restfulWebService.delete(`admin/delete-list/${reqBody}`);    
     }

     sendUserMessages(reqBody):any{
        if(reqBody.userIDs) return this.restfulWebService.post('admin/create-message',reqBody);
       else return this.restfulWebService.post('admin/send-message',reqBody);
      }
 
      uploadFile(reqBody){
       return this.restfulWebService.post('admin/file-upload',reqBody);
      }

      archieveUser(reqBody){
      return this.restfulWebService.get(`admin/archive-list/${reqBody}`)         
      }

      getUserUnreadMessagesCount(){
        if(this.coreService.role=="superAdmin")
        return this.restfulWebService.get('admin/get-all-unread-count');
        else
        return this.restfulWebService.get('get-all-unread-count');
      }
 
}
