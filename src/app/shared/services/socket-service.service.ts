import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import * as io from 'socket.io-client'; 
import { Cookie } from 'ng2-cookies/ng2-cookies'; 



// const io = require('socket.io-client');
// const socket = io.connect('http://website.com');

// socket.on('connect', () => {
//   console.log('Successfully connected!');



@Injectable()
export class SocketServiceService {

  constructor() { 
   // this.connectSocket();
  }
  private url = window.location.hostname+':8055';  
  // private url = '10.0.1.47'+':8000';  
  public socket;

  connectSocket(){
    this.socket=io.connect(this.url);
    this.socket.on('connect', () => {
    console.log('Successfully connected!');
    this.addUser();
         });
       
         return this.socket;
  }

  addUser(){
    // console.log("user INFO Socket",JSON.parse(localStorage.getItem('happyhr-userInfo')).userID)
    if(!JSON.parse(localStorage.getItem('happyhr-userInfo'))) return ;
    let socketRequest={
      token:Cookie.get('happyhr-token'),
      userID:JSON.parse(localStorage.getItem('happyhr-userInfo')).userID
    }
     this.socket.emit('add user',socketRequest);
  }
  
  sendMessage(message){
    this.socket.emit('add-message', message);    
  }

  
  
  getMessages() {
    let observable = new Observable(observer => {
      this.socket = io(this.url);
      this.socket.on('message', (data) => {
        observer.next(data);    
      });
      return () => {
        this.socket.disconnect();
      };  
    })     
    return observable;
  }  
}