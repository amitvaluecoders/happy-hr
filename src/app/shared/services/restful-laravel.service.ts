import {Injectable} from '@angular/core';
import {Http, Headers,Response,RequestOptions} from '@angular/http';
import {Observable} from 'rxjs/Rx';
import { Router} from '@angular/router';
import 'rxjs/add/operator/map'
import { Cookie } from 'ng2-cookies/ng2-cookies';

@Injectable()
export class RestfulLaravelService  {
  authHeader:any;
    // baseUrl="http://10.0.1.78/happyhr/public/api/";
    // baseUrl = "http://localhost:8056/";
    // baseUrl = "http://10.0.2.36:8056/";
    // baseUrl="http://happyhrapi.n1.iworklab.com/public/api/";  
    // baseUrl="http://10.0.1.103/happyhr/public/api/";
      baseUrl="https://api.happyhr.com/staging/public/api/";

      //baseUrl="http://10.0.2.46/hhr-local/public/api/"  **when database was change **
  //  baseUrl="https://api.happyhr.com/public/api/";
   //baseUrl="/";

  constructor(private _http: Http,private router:Router) { 
  
  }
  
  getAuthorizationHeader(){
    var jwt = Cookie.get('happyhr-token');
    this.authHeader = new Headers();        
    if(jwt){
        this.authHeader.append('Authorization','Bearer '+jwt);
        this.authHeader.append('Api-Token',jwt);
        this.authHeader.append('enc-id',"true");  // only for testing in development mode`
        
    }
    return {headers:this.authHeader};
  }


  get(url){
     return this._http.get(this.baseUrl+url,this.getAuthorizationHeader())
                .map(this.extractData)
                .catch(this.handleError);
  }

  post(url,itemToPost){
    return this._http.post(this.baseUrl+url,itemToPost,this.getAuthorizationHeader())
                .map(this.extractData)
                .catch(this.handleError);
  }
  
  put(url,itemToPut){
    return this._http.put(this.baseUrl+url,itemToPut,this.getAuthorizationHeader())
                .map(this.extractData)
                .catch(this.handleError);
  }

  delete(url){
    return this._http.delete(this.baseUrl+url,this.getAuthorizationHeader())
                .map(this.extractData)
                .catch(this.handleError);
  }
  
  getBaseUrl(){
    return this.baseUrl;
  } 

  private extractData(res: Response) {
    let body = res.json();
    return body||[];
  }

  private handleError (error: Response | any) {
    // In a real world app, we might use a remote logging infrastructure
    switch (error.status) {
      case 403:
        console.log(403);
        break;
      case 500:
        // error.clearCookie('happyhr-token');
        // this.router.navigate(['/login']);
        console.log(500);
        break;
      case 503:
        // error.clearCookie('happyhr-token');
        break;
      case 0:
        console.log(0);
        break;
    }
    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    console.error(errMsg);
    //return Observable.throw(errMsg);
    return Observable.throw(error.json());
  }

}
