import { Injectable } from '@angular/core';
// import { RestfulwebService } from './restfulweb.service';
import {Http, Headers,Response,RequestOptions} from '@angular/http';
import { RestfulLaravelService } from './restful-laravel.service';

@Injectable()
export class UserService {

  constructor(
    private restfulWebService: RestfulLaravelService, 
    private http:Http
  ){}

  login(user){
    return this.restfulWebService.post(`login`,user);                
  }

  // forgetPassword(user){ 
  //   return this.restfulWebService.post(`forgotPassword`,user);                
  // }

  signup(user){
    return this.restfulWebService.get('signup');
  }

  resetPassword(user){
    return this.restfulWebService.put('reset/password',user);
  }

  addUser(user){
    return this.restfulWebService.post(`user/add`,user);
  }

  editUser(user){
    return this.restfulWebService.put(`user/edit`,user);
  }
  
  deleteUser(user){
    return this.restfulWebService.delete(`user/delete/${user}`);
  }

  editCompany(company){
    return this.restfulWebService.put(`company/edit`,company);    
  }

   getMessages(){
    return this.restfulWebService.get(`all/messages`);     
  }

  addMessages(message) {
    return this.restfulWebService.post(`send/messages`,message);         
  }

  //laravel requests

  //get secuirty questions
  getSecurityQuestions(){
    return this.restfulWebService.get(`get-security-question`);
  }

  //forget password
  forgetPassword(user){
    return this.restfulWebService.post(`forgot-password`,user);   
  }

  //reset password token check
   resetTokenCheck(token){
    return this.restfulWebService.post(`check-password-reset-token`,token);   
  }

  //reset password request
   resetAccPassword(token){
    return this.restfulWebService.post(`reset-password`,token);   
  } 

  subCompanyRegistration(reqBody){
    return this.restfulWebService.post(`update-sub-company-password`,reqBody);   
  }

  userAcceptOffer(token){
    let reqBody ={
      "checkToken":token,"action":"Accept"
    }
   return this.restfulWebService.post(`employee-accepts-offer`,reqBody);      
  }

}