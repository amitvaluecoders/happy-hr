import { Injectable } from '@angular/core';

@Injectable()
export class CountryStateService {

  constructor() { }


  getCountries() {
    return [
      { id: "13", itemName: "Australia" }
    ]

    // , { id: "157", itemName: "New Zealand" }
  }

  getState(countryID) {
    let state = {
      "13": [
        // { "id": "Australian Capital Territory", "itemName": "Australian Capital Territory" },
        // { "id": "Christmas Island", "itemName": "Christmas Island" },
        // { "id": "Jervis Bay Territory", "itemName": "Jervis Bay Territory" },
        // { "id": "Kerguelen", "itemName": "Kerguelen" },
        // { "id": "Kerguelen", "itemName": "Kerguelen" },
        // { "id": "New South Wales", "itemName": "New South Wales" },
        // { "id": "Niue", "itemName": "Niue" },
        // { "id": "Norfolk Island", "itemName": "Norfolk Island" },
        // { "id": "Zhejiang", "itemName": "Zhejiang" },

        { id: "ACT", itemName: "Australian Capital Territory" },
        { id: "New South Wales", itemName: "New South Wales" },
        { id: "Northern Territory", itemName: "Northern Territory" },
        { id: "Queensland", itemName: "Queensland" },
        { id: "South Australia", itemName: "South Australia" },
        { id: "Tasmania", itemName: "Tasmania" },
        { id: "Victoria", itemName: "Victoria" },
        { id: "Western Australia", itemName: "Western Australia" }

      ]
      // ,
      // "157": [
      //   { "id": "Auckland", "itemName": "Auckland" },
      //   { "id": "Bay of Plenty", "itemName": "Bay of Plenty" },
      //   { "id": "Canterbury", "itemName": "Canterbury" },
      //   { "id": "Gisborne", "itemName": "Gisborne" },
      //   { "id": "Hawke's Bay", "itemName": "Hawke's Bay" },
      //   { "id": "Manawatu-Wanganui", "itemName": "Manawatu-Wanganui" },
      //   { "id": "Marlborough", "itemName": "Marlborough" },
      //   { "id": "Nelson", "itemName": "Nelson" },
      //   { "id": "Northland", "itemName": "Northland" },
      //   { "id": "Otago", "itemName": "Otago" },
      //   { "id": "Southland", "itemName": "Southland" },
      //   { "id": "Taranaki", "itemName": "Taranaki" },
      //   { "id": "Tasman", "itemName": "Tasman" },
      //   { "id": "West Coast", "itemName": "West Coast" }
      // ]
    }
    return state[countryID];
  }

}
