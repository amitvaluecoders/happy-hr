import { TestBed, inject } from '@angular/core/testing';

import { RestfulwebService } from './restfulweb.service';

describe('RestfulwebService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [RestfulwebService]
    });
  });

  it('should ...', inject([RestfulwebService], (service: RestfulwebService) => {
    expect(service).toBeTruthy();
  }));
});
