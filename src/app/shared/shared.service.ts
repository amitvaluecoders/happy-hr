import { Injectable } from '@angular/core';
import { RestfulLaravelService} from './services/restful-laravel.service';

@Injectable()
export class SharedService {

  constructor(public restfulWebService:RestfulLaravelService) { }



//method to reply with responseNote .
  sendResponseNote(reqBody):any{
    if(reqBody.method && reqBody.method == "PUT") return this.restfulWebService.put(reqBody.url,reqBody.note);
   return this.restfulWebService.post(reqBody.url,reqBody.note);
  }

  getActionDetails(reqBody):any{
    let url='';
    switch (reqBody.type) {
      case 'Terminate':{
        url='get-terminate-letter/';
        break;
      } 
      case 'Warning':{
        url='get-warning-letter/';
        break;
      } 
      case 'Stand down':{
        url='get-left-letter/';
        break;
      } 
       case 'Serious Disciplinary Action':{
        url='get-serious-misconduct-letter/';
        break;
      } 
      case 'Final Warning':{
        url='get-final-warning-letter/';
        break;
      } 
      case 'createWarning':{
        url='get-warning-letter/';  
      }      
    }
    url+=reqBody.userID;
   return this.restfulWebService.get(url);    
  }

  submitUserAction(reqBody):any{
    return this.restfulWebService.post('send-letter',reqBody);
  }

}
