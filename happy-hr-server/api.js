exports.sendNotification = function(body, app, cb) {
    var ids = body.map(function(v) { return v.userID; });
    var sockets = {};
    app.local.connection.forEach(function(v) {
        if (~ids.indexOf(v.socket.userID)) {
            typeof sockets[v.socket.userID] == 'undefined' ? sockets[v.socket.userID] = [] : null;
            sockets[v.socket.userID].push(v.socket);
        }
    });
    var resValue = [];
    body.forEach(function(v) {
        sockets[v.userID] && sockets[v.userID].forEach(function(socket) {
            socket.emit('recive noti', v);
        });
        resValue.push(v.notificationID);
    });
    cb && cb(resValue);
};


module.exports = function(app) {
    app.post('/send-notification', function(req, res, error) {
        // var body = [{ "message": "Heloo", "userID": 1, "time": "2017-10-15 14:00:00", "notificationID": 3 }, { "message": "Feloo", "userID": 2, "time": "2017-10-15 14:00:00", "notificationID": 4 }];
        var body = req.body;
        if (!body) return res.status(200).send({ 'error': 'empty body' });
        exports.sendNotification(body, app, function(resValue) {
            res.status(200).send({ 'notificationID': resValue })
        });

    });

    app.post('/send-notification', function(req, res, error) {
        // var body = [{ "message": "Heloo", "userID": 1, "time": "2017-10-15 14:00:00", "broadcastID": 3 }, { "message": "Feloo", "userID": 2, "time": "2017-10-15 14:00:00", "broadcastID": 4 }];
        var body = req.body;
        if (!body) return res.status(200).send({ 'error': 'empty body' });
        var ids = body.map(function(v) { return v.userID; });
        var sockets = {};
        app.local.connection.forEach(function(v) {
            if (~ids.indexOf(v.socket.userID)) {
                typeof sockets[v.socket.userID] == 'undefined' ? sockets[v.socket.userID] = [] : null;
                sockets[v.socket.userID].push(v.socket);
            }
        });
        var resValue = [];
        body.forEach(function(v) {
            sockets[v.userID] && sockets[v.userID].forEach(function(socket) {
                socket.emit('recive broad', v);
            });
            resValue.push(v.broadcastID);
        });
        res.status(200).send({ 'broadcastID': resValue })
    });
}