var express = require('express'),
    app = express(),
    server = require('http').createServer(app),
    io = require('socket.io').listen(server, {
        'log level': 2,
        transports: ['websocket', 'polling']
    }),
    request = require('./frontendRequest.js');

    var cors = require('cors');

app.use(cors())
var bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());


app.local = {
    connection: []
};

server.listen(8000);

var connCnt = 0;

require('./api.js')(app);


io.on('connection', function(socket) {
    console.log('connect');
    connCnt++;
    var connIdx = connCnt;
    socket.on('add user', function(usrIdwithToken) {
        socket.userID = usrIdwithToken.userID;
        socket.token = usrIdwithToken.token;
        app.local.connection.push({
            cnt: connCnt,
            socket: socket
        });
        console.log('add', app.local.connection.map(function(v) { return v.socket.userID; }));
    });
    socket.on('send msg', function(req) {
        request.sendMessage(app.local, req, socket);
    });


    socket.on('disconnect', function() {
        for (var i = 0; i < app.local.connection.length; i++) {
            if (app.local.connection[i].cnt == connCnt && app.local.connection[i].socket.userID == socket.userID) {
                app.local.connection.splice(i, 1);
                break;
            }
        }
        console.log('dis', app.local.connection.map(function(v) { return v.socket.userID; }));
    });

});