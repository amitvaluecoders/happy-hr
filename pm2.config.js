module.exports = {
    apps: [{
        name: "socket",
        script: "./happy-hr-server/server.js",
        instances: 4,
        exec_mode: "cluster",
        watch: false,
        merge_logs: true
    }]
}


// name: "angular",
// script: "./app.js",
// instances: 2,
// exec_mode: "cluster",
// watch: false,
// }, {